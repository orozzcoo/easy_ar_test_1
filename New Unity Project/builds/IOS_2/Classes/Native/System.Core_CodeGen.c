﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000005 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000008 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000009 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::Range(System.Int32,System.Int32)
extern void Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C ();
// 0x0000000C System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::RangeIterator(System.Int32,System.Int32)
extern void Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75 ();
// 0x0000000D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000F System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000010 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000011 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000012 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000013 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000014 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000015 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000016 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000018 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000019 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000001A System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001B System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000001C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001D System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000001E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000001F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000020 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000022 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000023 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000024 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000026 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000027 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000028 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000029 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002A System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000002B System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000002C System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000002D System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000002E System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000030 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000031 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000032 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000033 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000035 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000036 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000037 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003A System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000003B System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000003C System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000003D TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000003E System.Void System.Linq.Enumerable_<RangeIterator>d__115::.ctor(System.Int32)
extern void U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228 ();
// 0x0000003F System.Void System.Linq.Enumerable_<RangeIterator>d__115::System.IDisposable.Dispose()
extern void U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032 ();
// 0x00000040 System.Boolean System.Linq.Enumerable_<RangeIterator>d__115::MoveNext()
extern void U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10 ();
// 0x00000041 System.Int32 System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.Generic.IEnumerator<System.Int32>.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D ();
// 0x00000042 System.Void System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerator.Reset()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2 ();
// 0x00000043 System.Object System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerator.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB ();
// 0x00000044 System.Collections.Generic.IEnumerator`1<System.Int32> System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4 ();
// 0x00000045 System.Collections.IEnumerator System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerable.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880 ();
// 0x00000046 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000047 TElement[] System.Linq.Buffer`1::ToArray()
static Il2CppMethodPointer s_methodPointers[71] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C,
	Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228,
	U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032,
	U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[71] = 
{
	0,
	0,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	513,
	513,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	102,
	10,
	23,
	14,
	14,
	14,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[25] = 
{
	{ 0x02000004, { 42, 4 } },
	{ 0x02000005, { 46, 9 } },
	{ 0x02000006, { 57, 7 } },
	{ 0x02000007, { 66, 10 } },
	{ 0x02000008, { 78, 11 } },
	{ 0x02000009, { 92, 9 } },
	{ 0x0200000A, { 104, 12 } },
	{ 0x0200000B, { 119, 1 } },
	{ 0x0200000C, { 120, 2 } },
	{ 0x0200000E, { 122, 4 } },
	{ 0x06000004, { 0, 10 } },
	{ 0x06000005, { 10, 10 } },
	{ 0x06000006, { 20, 5 } },
	{ 0x06000007, { 25, 5 } },
	{ 0x06000008, { 30, 3 } },
	{ 0x06000009, { 33, 2 } },
	{ 0x0600000A, { 35, 3 } },
	{ 0x0600000D, { 38, 1 } },
	{ 0x0600000E, { 39, 3 } },
	{ 0x0600001E, { 55, 2 } },
	{ 0x06000023, { 64, 2 } },
	{ 0x06000028, { 76, 2 } },
	{ 0x0600002E, { 89, 3 } },
	{ 0x06000033, { 101, 3 } },
	{ 0x06000038, { 116, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[126] = 
{
	{ (Il2CppRGCTXDataType)2, 11123 },
	{ (Il2CppRGCTXDataType)3, 5884 },
	{ (Il2CppRGCTXDataType)2, 11124 },
	{ (Il2CppRGCTXDataType)2, 11125 },
	{ (Il2CppRGCTXDataType)3, 5885 },
	{ (Il2CppRGCTXDataType)2, 11126 },
	{ (Il2CppRGCTXDataType)2, 11127 },
	{ (Il2CppRGCTXDataType)3, 5886 },
	{ (Il2CppRGCTXDataType)2, 11128 },
	{ (Il2CppRGCTXDataType)3, 5887 },
	{ (Il2CppRGCTXDataType)2, 11129 },
	{ (Il2CppRGCTXDataType)3, 5888 },
	{ (Il2CppRGCTXDataType)2, 11130 },
	{ (Il2CppRGCTXDataType)2, 11131 },
	{ (Il2CppRGCTXDataType)3, 5889 },
	{ (Il2CppRGCTXDataType)2, 11132 },
	{ (Il2CppRGCTXDataType)2, 11133 },
	{ (Il2CppRGCTXDataType)3, 5890 },
	{ (Il2CppRGCTXDataType)2, 11134 },
	{ (Il2CppRGCTXDataType)3, 5891 },
	{ (Il2CppRGCTXDataType)2, 11135 },
	{ (Il2CppRGCTXDataType)3, 5892 },
	{ (Il2CppRGCTXDataType)3, 5893 },
	{ (Il2CppRGCTXDataType)2, 8605 },
	{ (Il2CppRGCTXDataType)3, 5894 },
	{ (Il2CppRGCTXDataType)2, 11136 },
	{ (Il2CppRGCTXDataType)3, 5895 },
	{ (Il2CppRGCTXDataType)3, 5896 },
	{ (Il2CppRGCTXDataType)2, 8612 },
	{ (Il2CppRGCTXDataType)3, 5897 },
	{ (Il2CppRGCTXDataType)2, 11137 },
	{ (Il2CppRGCTXDataType)3, 5898 },
	{ (Il2CppRGCTXDataType)3, 5899 },
	{ (Il2CppRGCTXDataType)2, 8618 },
	{ (Il2CppRGCTXDataType)3, 5900 },
	{ (Il2CppRGCTXDataType)2, 8619 },
	{ (Il2CppRGCTXDataType)2, 11138 },
	{ (Il2CppRGCTXDataType)3, 5901 },
	{ (Il2CppRGCTXDataType)2, 8622 },
	{ (Il2CppRGCTXDataType)2, 8624 },
	{ (Il2CppRGCTXDataType)2, 11139 },
	{ (Il2CppRGCTXDataType)3, 5902 },
	{ (Il2CppRGCTXDataType)3, 5903 },
	{ (Il2CppRGCTXDataType)3, 5904 },
	{ (Il2CppRGCTXDataType)2, 8629 },
	{ (Il2CppRGCTXDataType)3, 5905 },
	{ (Il2CppRGCTXDataType)3, 5906 },
	{ (Il2CppRGCTXDataType)2, 8641 },
	{ (Il2CppRGCTXDataType)2, 11140 },
	{ (Il2CppRGCTXDataType)3, 5907 },
	{ (Il2CppRGCTXDataType)3, 5908 },
	{ (Il2CppRGCTXDataType)2, 8643 },
	{ (Il2CppRGCTXDataType)2, 11052 },
	{ (Il2CppRGCTXDataType)3, 5909 },
	{ (Il2CppRGCTXDataType)3, 5910 },
	{ (Il2CppRGCTXDataType)2, 11141 },
	{ (Il2CppRGCTXDataType)3, 5911 },
	{ (Il2CppRGCTXDataType)3, 5912 },
	{ (Il2CppRGCTXDataType)2, 8653 },
	{ (Il2CppRGCTXDataType)2, 11142 },
	{ (Il2CppRGCTXDataType)3, 5913 },
	{ (Il2CppRGCTXDataType)3, 5914 },
	{ (Il2CppRGCTXDataType)3, 5553 },
	{ (Il2CppRGCTXDataType)3, 5915 },
	{ (Il2CppRGCTXDataType)2, 11143 },
	{ (Il2CppRGCTXDataType)3, 5916 },
	{ (Il2CppRGCTXDataType)3, 5917 },
	{ (Il2CppRGCTXDataType)2, 8665 },
	{ (Il2CppRGCTXDataType)2, 11144 },
	{ (Il2CppRGCTXDataType)3, 5918 },
	{ (Il2CppRGCTXDataType)3, 5919 },
	{ (Il2CppRGCTXDataType)3, 5920 },
	{ (Il2CppRGCTXDataType)3, 5921 },
	{ (Il2CppRGCTXDataType)3, 5922 },
	{ (Il2CppRGCTXDataType)3, 5559 },
	{ (Il2CppRGCTXDataType)3, 5923 },
	{ (Il2CppRGCTXDataType)2, 11145 },
	{ (Il2CppRGCTXDataType)3, 5924 },
	{ (Il2CppRGCTXDataType)3, 5925 },
	{ (Il2CppRGCTXDataType)2, 8678 },
	{ (Il2CppRGCTXDataType)2, 11146 },
	{ (Il2CppRGCTXDataType)3, 5926 },
	{ (Il2CppRGCTXDataType)3, 5927 },
	{ (Il2CppRGCTXDataType)2, 8680 },
	{ (Il2CppRGCTXDataType)2, 11147 },
	{ (Il2CppRGCTXDataType)3, 5928 },
	{ (Il2CppRGCTXDataType)3, 5929 },
	{ (Il2CppRGCTXDataType)2, 11148 },
	{ (Il2CppRGCTXDataType)3, 5930 },
	{ (Il2CppRGCTXDataType)3, 5931 },
	{ (Il2CppRGCTXDataType)2, 11149 },
	{ (Il2CppRGCTXDataType)3, 5932 },
	{ (Il2CppRGCTXDataType)3, 5933 },
	{ (Il2CppRGCTXDataType)2, 8695 },
	{ (Il2CppRGCTXDataType)2, 11150 },
	{ (Il2CppRGCTXDataType)3, 5934 },
	{ (Il2CppRGCTXDataType)3, 5935 },
	{ (Il2CppRGCTXDataType)3, 5936 },
	{ (Il2CppRGCTXDataType)3, 5570 },
	{ (Il2CppRGCTXDataType)2, 11151 },
	{ (Il2CppRGCTXDataType)3, 5937 },
	{ (Il2CppRGCTXDataType)3, 5938 },
	{ (Il2CppRGCTXDataType)2, 11152 },
	{ (Il2CppRGCTXDataType)3, 5939 },
	{ (Il2CppRGCTXDataType)3, 5940 },
	{ (Il2CppRGCTXDataType)2, 8711 },
	{ (Il2CppRGCTXDataType)2, 11153 },
	{ (Il2CppRGCTXDataType)3, 5941 },
	{ (Il2CppRGCTXDataType)3, 5942 },
	{ (Il2CppRGCTXDataType)3, 5943 },
	{ (Il2CppRGCTXDataType)3, 5944 },
	{ (Il2CppRGCTXDataType)3, 5945 },
	{ (Il2CppRGCTXDataType)3, 5946 },
	{ (Il2CppRGCTXDataType)3, 5576 },
	{ (Il2CppRGCTXDataType)2, 11154 },
	{ (Il2CppRGCTXDataType)3, 5947 },
	{ (Il2CppRGCTXDataType)3, 5948 },
	{ (Il2CppRGCTXDataType)2, 11155 },
	{ (Il2CppRGCTXDataType)3, 5949 },
	{ (Il2CppRGCTXDataType)3, 5950 },
	{ (Il2CppRGCTXDataType)3, 5951 },
	{ (Il2CppRGCTXDataType)3, 5952 },
	{ (Il2CppRGCTXDataType)2, 11156 },
	{ (Il2CppRGCTXDataType)2, 8744 },
	{ (Il2CppRGCTXDataType)2, 8742 },
	{ (Il2CppRGCTXDataType)2, 11157 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	71,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	25,
	s_rgctxIndices,
	126,
	s_rgctxValues,
	NULL,
};
