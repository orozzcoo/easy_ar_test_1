﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// easyar.CameraState
struct  CameraState_t41D0938A1158181E7B0C9E509CF05C61D9CEF658 
{
public:
	// System.Int32 easyar.CameraState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraState_t41D0938A1158181E7B0C9E509CF05C61D9CEF658, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.LogLevel
struct  LogLevel_tE02C77CE6DEB9782E61C3688A9F519AFF1D73CCB 
{
public:
	// System.Int32 easyar.LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_tE02C77CE6DEB9782E61C3688A9F519AFF1D73CCB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.PermissionStatus
struct  PermissionStatus_t41F2835584F927E7DBA46FBDFA60ECA791C96C14 
{
public:
	// System.Int32 easyar.PermissionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PermissionStatus_t41F2835584F927E7DBA46FBDFA60ECA791C96C14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.RecordStatus
struct  RecordStatus_tA225F8E91E6E99FB79D371E8A79891D95C8EC045 
{
public:
	// System.Int32 easyar.RecordStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecordStatus_tA225F8E91E6E99FB79D371E8A79891D95C8EC045, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VideoStatus
struct  VideoStatus_t7B2C801858421B797E970E8C44F1A8FDBD7F9483 
{
public:
	// System.Int32 easyar.VideoStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoStatus_t7B2C801858421B797E970E8C44F1A8FDBD7F9483, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif

extern "C" void DEFAULT_CALL ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m056BCE43FF155AAE872FF7E565F8F72A50D26147(intptr_t ___arg0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoid_func_mF783D5E1A5CC1474AC7371278CABF14761C1737D(intptr_t ___state0, intptr_t* ___exception1);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoid_destroy_m0F5DC9905B8F43E6DC2E844556B0FF56E4BF67B6(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromOutputFrame_func_mB6649CCC85AAE55815A216F223BCCFFE5253FEC0(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromOutputFrame_destroy_mCCA2C33F06C260D5C48CE09D9E03133D4D0CD6DD(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromTargetAndBool_func_mFA51F1E153904EF00496BE23F062C9A12C273793(intptr_t ___state0, intptr_t ___arg01, int8_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromTargetAndBool_destroy_m6AA500E710A687EBB7C96D03D17E131BD94186FA(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCloudRecognizationResult_func_m90154CEE7823B813A64107474E84C9D8E49F9FE8(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mA6894D99F036B445918E01B88C818F63C208B1C1(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromInputFrame_func_mA2CECDF41A63A69813C1E34E46FCE069F47C6427(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromInputFrame_destroy_mB73878C35844CC8471D0EF2A8B4A1FE0D3C6F878(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCameraState_func_m7FCB6EEC40715D367C83D86227157A9E122568C9(intptr_t ___state0, int32_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromCameraState_destroy_mC70C7A72F6DBC20901D045EFF23EA2AF58A5BF41(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromPermissionStatusAndString_func_m61FB5A520BF4BF61618A6036F4C225797196DB4D(intptr_t ___state0, int32_t ___arg01, intptr_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_mFB9A4F27091BABD5B3A3FE933C4003F0478BD7B5(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromLogLevelAndString_func_m53946E55D87E33DA5FA36DC16B26E9B3DB0B439C(intptr_t ___state0, int32_t ___arg01, intptr_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromLogLevelAndString_destroy_mADE83DB6360391E4C933515D59EF3F73E4B0B08E(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromRecordStatusAndString_func_mFA196EC005028814229B1F55F1E24665DF692553(intptr_t ___state0, int32_t ___arg01, intptr_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromRecordStatusAndString_destroy_m051F182F8E827A97DEC26087EDB6CD6616462981(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBool_func_m8C959A83611A4385EE07D73B822F990C10B6EDF6(intptr_t ___state0, int8_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBool_destroy_mC7D4393D0066F06286A66262A3B206C16F9A0A83(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndStringAndString_func_mE08DFB80305F9D293A7D957A1B409629ED9B4396(intptr_t ___state0, int8_t ___arg01, intptr_t ___arg12, intptr_t ___arg23, intptr_t* ___exception4);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_m1D50B3D098976A8F7CDA2AEFDCA989ABDF1FBAD5(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndString_func_m87AE9C0004F64626A5E00CFFD0F1A2F868E29D34(intptr_t ___state0, int8_t ___arg01, intptr_t ___arg12, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndString_destroy_m1C520BDA809AB254063C19A8E92794F7A368E283(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromVideoStatus_func_m75DE2AFDC396D76465BE54C22D32881BE260470F(intptr_t ___state0, int32_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromVideoStatus_destroy_m560F57E8D9CDF6C4CCDD018BE704C492D7C6B550(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromFeedbackFrame_func_mB4288FAC2F09BEFDD8503B7684DDC994C32F8CE7(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___exception2);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfVoidFromFeedbackFrame_destroy_m08DAAB8A98135C1DF4CFDC319727C08E4EF48BF2(intptr_t ____state0);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m458EA310F59A4613F82319C3BE247BE328F9EE19(intptr_t ___state0, intptr_t ___arg01, intptr_t* ___Return2, intptr_t* ___exception3);
extern "C" void CDECL ReversePInvokeWrapper_Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m50A85D540D02D4F023B507E3A9BEF80C6D75D58F(intptr_t ____state0);


extern const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[];
const Il2CppMethodPointer g_ReversePInvokeWrapperPointers[31] = 
{
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_OSSpecificSynchronizationContext_InvocationEntry_m056BCE43FF155AAE872FF7E565F8F72A50D26147),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoid_func_mF783D5E1A5CC1474AC7371278CABF14761C1737D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoid_destroy_m0F5DC9905B8F43E6DC2E844556B0FF56E4BF67B6),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromOutputFrame_func_mB6649CCC85AAE55815A216F223BCCFFE5253FEC0),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromOutputFrame_destroy_mCCA2C33F06C260D5C48CE09D9E03133D4D0CD6DD),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromTargetAndBool_func_mFA51F1E153904EF00496BE23F062C9A12C273793),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromTargetAndBool_destroy_m6AA500E710A687EBB7C96D03D17E131BD94186FA),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCloudRecognizationResult_func_m90154CEE7823B813A64107474E84C9D8E49F9FE8),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mA6894D99F036B445918E01B88C818F63C208B1C1),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromInputFrame_func_mA2CECDF41A63A69813C1E34E46FCE069F47C6427),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromInputFrame_destroy_mB73878C35844CC8471D0EF2A8B4A1FE0D3C6F878),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCameraState_func_m7FCB6EEC40715D367C83D86227157A9E122568C9),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromCameraState_destroy_mC70C7A72F6DBC20901D045EFF23EA2AF58A5BF41),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromPermissionStatusAndString_func_m61FB5A520BF4BF61618A6036F4C225797196DB4D),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_mFB9A4F27091BABD5B3A3FE933C4003F0478BD7B5),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromLogLevelAndString_func_m53946E55D87E33DA5FA36DC16B26E9B3DB0B439C),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromLogLevelAndString_destroy_mADE83DB6360391E4C933515D59EF3F73E4B0B08E),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromRecordStatusAndString_func_mFA196EC005028814229B1F55F1E24665DF692553),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromRecordStatusAndString_destroy_m051F182F8E827A97DEC26087EDB6CD6616462981),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBool_func_m8C959A83611A4385EE07D73B822F990C10B6EDF6),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBool_destroy_mC7D4393D0066F06286A66262A3B206C16F9A0A83),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndStringAndString_func_mE08DFB80305F9D293A7D957A1B409629ED9B4396),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_m1D50B3D098976A8F7CDA2AEFDCA989ABDF1FBAD5),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndString_func_m87AE9C0004F64626A5E00CFFD0F1A2F868E29D34),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromBoolAndString_destroy_m1C520BDA809AB254063C19A8E92794F7A368E283),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromVideoStatus_func_m75DE2AFDC396D76465BE54C22D32881BE260470F),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromVideoStatus_destroy_m560F57E8D9CDF6C4CCDD018BE704C492D7C6B550),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromFeedbackFrame_func_mB4288FAC2F09BEFDD8503B7684DDC994C32F8CE7),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfVoidFromFeedbackFrame_destroy_m08DAAB8A98135C1DF4CFDC319727C08E4EF48BF2),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m458EA310F59A4613F82319C3BE247BE328F9EE19),
	reinterpret_cast<Il2CppMethodPointer>(ReversePInvokeWrapper_Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m50A85D540D02D4F023B507E3A9BEF80C6D75D58F),
};
