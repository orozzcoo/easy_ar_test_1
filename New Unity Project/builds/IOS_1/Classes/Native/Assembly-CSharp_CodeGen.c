﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m50A85D540D02D4F023B507E3A9BEF80C6D75D58F_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m458EA310F59A4613F82319C3BE247BE328F9EE19_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_m1D50B3D098976A8F7CDA2AEFDCA989ABDF1FBAD5_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBoolAndStringAndString_func_mE08DFB80305F9D293A7D957A1B409629ED9B4396_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBoolAndString_destroy_m1C520BDA809AB254063C19A8E92794F7A368E283_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBoolAndString_func_m87AE9C0004F64626A5E00CFFD0F1A2F868E29D34_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBool_destroy_mC7D4393D0066F06286A66262A3B206C16F9A0A83_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromBool_func_m8C959A83611A4385EE07D73B822F990C10B6EDF6_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCameraState_destroy_mC70C7A72F6DBC20901D045EFF23EA2AF58A5BF41_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCameraState_func_m7FCB6EEC40715D367C83D86227157A9E122568C9_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mA6894D99F036B445918E01B88C818F63C208B1C1_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromCloudRecognizationResult_func_m90154CEE7823B813A64107474E84C9D8E49F9FE8_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromFeedbackFrame_destroy_m08DAAB8A98135C1DF4CFDC319727C08E4EF48BF2_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromFeedbackFrame_func_mB4288FAC2F09BEFDD8503B7684DDC994C32F8CE7_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromInputFrame_destroy_mB73878C35844CC8471D0EF2A8B4A1FE0D3C6F878_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromInputFrame_func_mA2CECDF41A63A69813C1E34E46FCE069F47C6427_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromLogLevelAndString_destroy_mADE83DB6360391E4C933515D59EF3F73E4B0B08E_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromLogLevelAndString_func_m53946E55D87E33DA5FA36DC16B26E9B3DB0B439C_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromOutputFrame_destroy_mCCA2C33F06C260D5C48CE09D9E03133D4D0CD6DD_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromOutputFrame_func_mB6649CCC85AAE55815A216F223BCCFFE5253FEC0_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_mFB9A4F27091BABD5B3A3FE933C4003F0478BD7B5_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromPermissionStatusAndString_func_m61FB5A520BF4BF61618A6036F4C225797196DB4D_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromRecordStatusAndString_destroy_m051F182F8E827A97DEC26087EDB6CD6616462981_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromRecordStatusAndString_func_mFA196EC005028814229B1F55F1E24665DF692553_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromTargetAndBool_destroy_m6AA500E710A687EBB7C96D03D17E131BD94186FA_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromTargetAndBool_func_mFA51F1E153904EF00496BE23F062C9A12C273793_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromVideoStatus_destroy_m560F57E8D9CDF6C4CCDD018BE704C492D7C6B550_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoidFromVideoStatus_func_m75DE2AFDC396D76465BE54C22D32881BE260470F_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoid_destroy_m0F5DC9905B8F43E6DC2E844556B0FF56E4BF67B6_RuntimeMethod_var;
extern const RuntimeMethod* Detail_FunctorOfVoid_func_mF783D5E1A5CC1474AC7371278CABF14761C1737D_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void easyar.ARAssembly::Finalize()
extern void ARAssembly_Finalize_m27C7C551F39F7F96F8BB793B150FF22AE4413C8D ();
// 0x00000002 System.Boolean easyar.ARAssembly::get_Ready()
extern void ARAssembly_get_Ready_m8C9799EC0E9B4354D664AD2F88064C79E958DFA2 ();
// 0x00000003 System.Void easyar.ARAssembly::set_Ready(System.Boolean)
extern void ARAssembly_set_Ready_m15799EDE7CED656CF6367A58414625BA680260FD ();
// 0x00000004 System.Boolean easyar.ARAssembly::get_RequireWorldCenter()
extern void ARAssembly_get_RequireWorldCenter_m7226701A762B047BEABDB0B31BC3EBA2D760AF32 ();
// 0x00000005 System.Void easyar.ARAssembly::set_RequireWorldCenter(System.Boolean)
extern void ARAssembly_set_RequireWorldCenter_mC9855DEC2B6BE8A0E43B2AE522E8A5D0428A9F4D ();
// 0x00000006 easyar.Optional`1<easyar.OutputFrame> easyar.ARAssembly::get_OutputFrame()
extern void ARAssembly_get_OutputFrame_mBA7CDF96D6CC14BBD64B7CB14B85B4E638CE11C2 ();
// 0x00000007 System.Void easyar.ARAssembly::Dispose()
extern void ARAssembly_Dispose_m4C2BA0A7290CF733E03BB15933F65D346040EB44 ();
// 0x00000008 System.Void easyar.ARAssembly::Assemble(easyar.ARSession)
extern void ARAssembly_Assemble_m431B3F53B1637AF82FD8AD36E5FC033BEAD065CF ();
// 0x00000009 System.Void easyar.ARAssembly::Break()
extern void ARAssembly_Break_mD8D9F0005804709713CE21C907056E1F76DE6E65 ();
// 0x0000000A System.Void easyar.ARAssembly::Pause()
extern void ARAssembly_Pause_m2D20F95AAA8F57E622E57B3A1DA222DC3EE5A6C7 ();
// 0x0000000B System.Void easyar.ARAssembly::Resume()
extern void ARAssembly_Resume_m62A623CB13D6DD66B017546F8E78527C6D673BC0 ();
// 0x0000000C System.Void easyar.ARAssembly::ResetBufferCapacity()
extern void ARAssembly_ResetBufferCapacity_m597167B49A27D67D7DC8051644A5B19CF0DA665E ();
// 0x0000000D System.Int32 easyar.ARAssembly::GetBufferRequirement()
extern void ARAssembly_GetBufferRequirement_mF7008A6D843E543D9597F1F001F3CC16691D86BD ();
// 0x0000000E System.Int32 easyar.ARAssembly::GetFrameFilterCount()
// 0x0000000F System.Void easyar.ARAssembly::Assemble()
extern void ARAssembly_Assemble_m82AAD532937E2BD5AEE325EF66BFA09A8F8ED4F9 ();
// 0x00000010 System.Void easyar.ARAssembly::DisposeAll()
extern void ARAssembly_DisposeAll_m1F2B46EF8B918A7B26F4D39E6FA7B423EBC7F85F ();
// 0x00000011 System.Void easyar.ARAssembly::.ctor()
extern void ARAssembly__ctor_m04474AAF85E12587B9FA18826FBAD982756B0E9A ();
// 0x00000012 System.Void easyar.ARSession::add_FrameChange(easyar.ARSession_FrameChangeAction)
extern void ARSession_add_FrameChange_m8D60BCDEF23E509A54A3D7D4B3C7E6D9A39EBBCC ();
// 0x00000013 System.Void easyar.ARSession::remove_FrameChange(easyar.ARSession_FrameChangeAction)
extern void ARSession_remove_FrameChange_m0C765DD4CB383AD433FBEFF840D9970E01104C77 ();
// 0x00000014 System.Void easyar.ARSession::add_FrameUpdate(System.Action`1<easyar.OutputFrame>)
extern void ARSession_add_FrameUpdate_m2E435330EEE5150F1301DC39793398FD8E41E03D ();
// 0x00000015 System.Void easyar.ARSession::remove_FrameUpdate(System.Action`1<easyar.OutputFrame>)
extern void ARSession_remove_FrameUpdate_m7DC7A1685EB66C2230442D0E1D23C11B0BFEF357 ();
// 0x00000016 System.Void easyar.ARSession::add_WorldRootChanged(System.Action`1<easyar.WorldRootController>)
extern void ARSession_add_WorldRootChanged_mC6A35B002BC4D65C61D7318A60DC3112965EFABE ();
// 0x00000017 System.Void easyar.ARSession::remove_WorldRootChanged(System.Action`1<easyar.WorldRootController>)
extern void ARSession_remove_WorldRootChanged_m5C3035DAE9668ABFF697149D3DC5439A3A6127CB ();
// 0x00000018 easyar.Optional`1<easyar.CameraParameters> easyar.ARSession::get_FrameCameraParameters()
extern void ARSession_get_FrameCameraParameters_m3668E89DA64EC7B23651FC83C4A1B7A809AA6FC1 ();
// 0x00000019 System.Void easyar.ARSession::set_FrameCameraParameters(easyar.Optional`1<easyar.CameraParameters>)
extern void ARSession_set_FrameCameraParameters_m6EAAD090786BB4FF7F6F743EB16829B87D7CDA45 ();
// 0x0000001A System.Void easyar.ARSession::Start()
extern void ARSession_Start_m22ADF52E8B9BD41EAE623CE6A725C2DF4C9A2ECA ();
// 0x0000001B System.Void easyar.ARSession::Update()
extern void ARSession_Update_mA2E45C5388B0ADB939119E31E90105F64F9045E1 ();
// 0x0000001C System.Void easyar.ARSession::OnDestroy()
extern void ARSession_OnDestroy_mF932EDA3F05AE03929B10F9B5FF284E3C3B595A3 ();
// 0x0000001D System.Void easyar.ARSession::OnFrameUpdate(easyar.OutputFrame,easyar.InputFrame,UnityEngine.Matrix4x4)
extern void ARSession_OnFrameUpdate_mF82C87831C2D956ABA9E875EC7EEBE51ED6668FD ();
// 0x0000001E System.Void easyar.ARSession::OnEmptyFrame()
extern void ARSession_OnEmptyFrame_m8E04D272168C40E48A3FBF2B0BC9D29FCADDB9D1 ();
// 0x0000001F System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.ARSession::DispatchResults(easyar.Optional`1<System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>>>,easyar.Optional`1<easyar.MotionTrackingStatus>)
extern void ARSession_DispatchResults_m71C9C5A43D74397ED101660330F02F25E322BD16 ();
// 0x00000020 System.Void easyar.ARSession::.ctor()
extern void ARSession__ctor_mB3EF86EE2F2FE04DABFC3A36966AD63AA9A4BFEB ();
// 0x00000021 System.Void easyar.CameraImageRenderer::add_OnFrameRenderUpdate(System.Action`2<UnityEngine.Material,UnityEngine.Vector2>)
extern void CameraImageRenderer_add_OnFrameRenderUpdate_m83F3F1454E5099F312BCE87E34C0654C65C548CA ();
// 0x00000022 System.Void easyar.CameraImageRenderer::remove_OnFrameRenderUpdate(System.Action`2<UnityEngine.Material,UnityEngine.Vector2>)
extern void CameraImageRenderer_remove_OnFrameRenderUpdate_mE9612C3D46FEFA37C1C16164CF9DE4F6E5BFDD82 ();
// 0x00000023 System.Void easyar.CameraImageRenderer::add_TargetTextureChange(System.Action`2<UnityEngine.Camera,UnityEngine.RenderTexture>)
extern void CameraImageRenderer_add_TargetTextureChange_mAF629152CBC88480D565C870CAC35DB695AF4B89 ();
// 0x00000024 System.Void easyar.CameraImageRenderer::remove_TargetTextureChange(System.Action`2<UnityEngine.Camera,UnityEngine.RenderTexture>)
extern void CameraImageRenderer_remove_TargetTextureChange_mDB96FEFF9078BDE23131638B1E05EDEE284910D8 ();
// 0x00000025 System.Void easyar.CameraImageRenderer::Awake()
extern void CameraImageRenderer_Awake_m8D3D6A3CF00CED3733847412E8C301B7DD65E717 ();
// 0x00000026 System.Void easyar.CameraImageRenderer::OnEnable()
extern void CameraImageRenderer_OnEnable_m510F923A31E731C534492C9D116BDB67CAE9BC7A ();
// 0x00000027 System.Void easyar.CameraImageRenderer::OnDisable()
extern void CameraImageRenderer_OnDisable_m300A932299E580B7BEEA38490586F0985880F9E1 ();
// 0x00000028 System.Void easyar.CameraImageRenderer::OnDestroy()
extern void CameraImageRenderer_OnDestroy_m186C98B73778E7417D88D06E9051CEF1561F6A6B ();
// 0x00000029 System.Void easyar.CameraImageRenderer::RequestTargetTexture(System.Action`2<UnityEngine.Camera,UnityEngine.RenderTexture>)
extern void CameraImageRenderer_RequestTargetTexture_m845ED8ABBC6DB0E7697D48C4B9A7DE6F58787B27 ();
// 0x0000002A System.Void easyar.CameraImageRenderer::DropTargetTexture(System.Action`2<UnityEngine.Camera,UnityEngine.RenderTexture>)
extern void CameraImageRenderer_DropTargetTexture_mCB5E808B36EEA20489A09FD5667CC4D3E8ABEB3F ();
// 0x0000002B System.Void easyar.CameraImageRenderer::OnAssemble(easyar.ARSession)
extern void CameraImageRenderer_OnAssemble_mFB9D6DD2C29A6B45501315693C15EA85F7A19B7A ();
// 0x0000002C System.Void easyar.CameraImageRenderer::SetHFilp(System.Boolean)
extern void CameraImageRenderer_SetHFilp_mD87E7385E83B902CEAFFCF2E7D7606E499662AD7 ();
// 0x0000002D System.Void easyar.CameraImageRenderer::OnFrameChange(easyar.OutputFrame,UnityEngine.Matrix4x4)
extern void CameraImageRenderer_OnFrameChange_m525EF4BEFFAD33C632828574A5B3EB4FFDEC02DF ();
// 0x0000002E System.Void easyar.CameraImageRenderer::OnFrameUpdate(easyar.OutputFrame)
extern void CameraImageRenderer_OnFrameUpdate_mCCB07378126D84367EC907B2C7432723970728C5 ();
// 0x0000002F System.Void easyar.CameraImageRenderer::UpdateCommandBuffer(UnityEngine.Camera,UnityEngine.Material)
extern void CameraImageRenderer_UpdateCommandBuffer_mB0E7353F02E985845F095917C2EB5A6FFC9AE39A ();
// 0x00000030 System.Void easyar.CameraImageRenderer::RemoveCommandBuffer(UnityEngine.Camera)
extern void CameraImageRenderer_RemoveCommandBuffer_mCF1A6503855B0DDB7EEAC375EA60777666DF9925 ();
// 0x00000031 System.Void easyar.CameraImageRenderer::.ctor()
extern void CameraImageRenderer__ctor_m1BBD4BA70BF67F9008C552860CD826FE61DB1255 ();
// 0x00000032 System.Void easyar.CameraRenderEvent::add_PreRender(System.Action)
extern void CameraRenderEvent_add_PreRender_mA822D872CEDB2FD1605FF0CB594DEAE86B6D6F36 ();
// 0x00000033 System.Void easyar.CameraRenderEvent::remove_PreRender(System.Action)
extern void CameraRenderEvent_remove_PreRender_m1EE3518D666D18CCB9F844D4E10696EDB67709B8 ();
// 0x00000034 System.Void easyar.CameraRenderEvent::add_PostRender(System.Action)
extern void CameraRenderEvent_add_PostRender_mC4A7302F88831C22B863A9B6C7468D07431E13FB ();
// 0x00000035 System.Void easyar.CameraRenderEvent::remove_PostRender(System.Action)
extern void CameraRenderEvent_remove_PostRender_mD2F3AA2F168A6E2EC9B2490739373B724AC4B0CF ();
// 0x00000036 System.Void easyar.CameraRenderEvent::OnPreRender()
extern void CameraRenderEvent_OnPreRender_m6249E885292A55037FB0243E6BA0C5A8CD525F54 ();
// 0x00000037 System.Void easyar.CameraRenderEvent::OnPostRender()
extern void CameraRenderEvent_OnPostRender_mAD0B8A18045F7836636366D5529B48EB8D523580 ();
// 0x00000038 System.Void easyar.CameraRenderEvent::.ctor()
extern void CameraRenderEvent__ctor_m1AE64D6E53AB0B3E849950D9620BE7BE80A2E072 ();
// 0x00000039 System.Int32 easyar.CameraSource::get_BufferCapacity()
extern void CameraSource_get_BufferCapacity_m9E552F2724891337B1083BB2DFDF4131256BFF30 ();
// 0x0000003A System.Void easyar.CameraSource::set_BufferCapacity(System.Int32)
extern void CameraSource_set_BufferCapacity_mC905F6435B3E3F2E298D2EA3BFAE8F2987B1D5A7 ();
// 0x0000003B System.Void easyar.CameraSource::Start()
extern void CameraSource_Start_mA48F5AB0EFFADAF4383D945B5002E4BC76E215B3 ();
// 0x0000003C System.Void easyar.CameraSource::OnDestroy()
extern void CameraSource_OnDestroy_m6F7FF1B9645E5117FAC584DA6557C768E54B4E8C ();
// 0x0000003D System.Void easyar.CameraSource::Open()
// 0x0000003E System.Void easyar.CameraSource::Close()
// 0x0000003F System.Void easyar.CameraSource::.ctor()
extern void CameraSource__ctor_m7F16B069980B861E6F9984C2859BB03B7A1C2A99 ();
// 0x00000040 easyar.CloudRecognizer easyar.CloudRecognizerFrameFilter::get_CloudRecognizer()
extern void CloudRecognizerFrameFilter_get_CloudRecognizer_m87505BF12991E4A1DD2AC9AA9C6C6BDC3C5A1909 ();
// 0x00000041 System.Void easyar.CloudRecognizerFrameFilter::set_CloudRecognizer(easyar.CloudRecognizer)
extern void CloudRecognizerFrameFilter_set_CloudRecognizer_mFCDBC9CE81ED6FDC283D66A0D9BBDFB42DE811B1 ();
// 0x00000042 System.Void easyar.CloudRecognizerFrameFilter::Start()
extern void CloudRecognizerFrameFilter_Start_m927FA2A9088C6BAC1068257717603A5DC3B97BE0 ();
// 0x00000043 System.Void easyar.CloudRecognizerFrameFilter::Resolve(easyar.InputFrame,System.Action`1<easyar.CloudRecognizationResult>)
extern void CloudRecognizerFrameFilter_Resolve_m0F08C9056A5C272338FDFB7E7164B0918C3845C6 ();
// 0x00000044 System.Void easyar.CloudRecognizerFrameFilter::OnDestroy()
extern void CloudRecognizerFrameFilter_OnDestroy_m17BA44B25E328E2E560CF683FD854A433C626D80 ();
// 0x00000045 System.Void easyar.CloudRecognizerFrameFilter::NotifyEmptyConfig(easyar.CloudRecognizerFrameFilter_CloudRecognizerServiceConfig)
extern void CloudRecognizerFrameFilter_NotifyEmptyConfig_m3E3D3D21E09C63DA3E0AEE7241D332B5CB6BF902 ();
// 0x00000046 System.Void easyar.CloudRecognizerFrameFilter::NotifyEmptyPrivateConfig(easyar.CloudRecognizerFrameFilter_PrivateCloudRecognizerServiceConfig)
extern void CloudRecognizerFrameFilter_NotifyEmptyPrivateConfig_m4E4D498A3CC837F87F83551097A319B413FF139C ();
// 0x00000047 System.Void easyar.CloudRecognizerFrameFilter::.ctor()
extern void CloudRecognizerFrameFilter__ctor_m29311C472CCB284DD3838F0808AFD64CC33AD570 ();
// 0x00000048 easyar.BlockInfo easyar.DenseSpatialMapBlockController::get_Info()
extern void DenseSpatialMapBlockController_get_Info_mCAE7B244716DDD5A7F414DEC93D38255DD156A70 ();
// 0x00000049 System.Void easyar.DenseSpatialMapBlockController::set_Info(easyar.BlockInfo)
extern void DenseSpatialMapBlockController_set_Info_mABD08135346B8311E884866C0DCD51BDF4D4B5A4 ();
// 0x0000004A System.Void easyar.DenseSpatialMapBlockController::Awake()
extern void DenseSpatialMapBlockController_Awake_mFAD5245B5620127CDBD7E10A23D8C065385252C6 ();
// 0x0000004B System.Void easyar.DenseSpatialMapBlockController::OnDestroy()
extern void DenseSpatialMapBlockController_OnDestroy_mCCBC3E2379E7EF5A2F45702F150CFB79F301DA2B ();
// 0x0000004C System.Void easyar.DenseSpatialMapBlockController::UpdateData(easyar.BlockInfo,easyar.SceneMesh)
extern void DenseSpatialMapBlockController_UpdateData_mD9F771F64264D5C908081C1BC0BC91620B4D757D ();
// 0x0000004D System.Void easyar.DenseSpatialMapBlockController::UpdateMesh()
extern void DenseSpatialMapBlockController_UpdateMesh_m8492B42C00F84F12431837F636AF9521DEB5FB69 ();
// 0x0000004E System.Void easyar.DenseSpatialMapBlockController::CopyMeshData(easyar.SceneMesh)
extern void DenseSpatialMapBlockController_CopyMeshData_m9C31922C4A8A07EC9A4C1735D03D3BBF351B7C27 ();
// 0x0000004F System.Void easyar.DenseSpatialMapBlockController::.ctor()
extern void DenseSpatialMapBlockController__ctor_m3CB2387BEBAF438477349306D12BA72C59E62536 ();
// 0x00000050 easyar.DenseSpatialMap easyar.DenseSpatialMapBuilderFrameFilter::get_Builder()
extern void DenseSpatialMapBuilderFrameFilter_get_Builder_mF02BE9B80646C6116608FCE0E05319E7B24DB5E8 ();
// 0x00000051 System.Void easyar.DenseSpatialMapBuilderFrameFilter::set_Builder(easyar.DenseSpatialMap)
extern void DenseSpatialMapBuilderFrameFilter_set_Builder_m2D785C31E7F0C39FEDE74DC7ED1A57C430CC1D7D ();
// 0x00000052 System.Void easyar.DenseSpatialMapBuilderFrameFilter::add_MapCreate(System.Action`1<easyar.DenseSpatialMapBlockController>)
extern void DenseSpatialMapBuilderFrameFilter_add_MapCreate_mE689BD3F61B0E43512D7F37C966997D3738BF45B ();
// 0x00000053 System.Void easyar.DenseSpatialMapBuilderFrameFilter::remove_MapCreate(System.Action`1<easyar.DenseSpatialMapBlockController>)
extern void DenseSpatialMapBuilderFrameFilter_remove_MapCreate_mCFE51332A6A791FE17E8E9BABCEB8F9FDF49E482 ();
// 0x00000054 System.Void easyar.DenseSpatialMapBuilderFrameFilter::add_MapUpdate(System.Action`1<System.Collections.Generic.List`1<easyar.DenseSpatialMapBlockController>>)
extern void DenseSpatialMapBuilderFrameFilter_add_MapUpdate_m096C661C5970C927F89104173341B4865875A0FC ();
// 0x00000055 System.Void easyar.DenseSpatialMapBuilderFrameFilter::remove_MapUpdate(System.Action`1<System.Collections.Generic.List`1<easyar.DenseSpatialMapBlockController>>)
extern void DenseSpatialMapBuilderFrameFilter_remove_MapUpdate_m33D3992DECD3BF13AEB413CE97C89842D8C906EF ();
// 0x00000056 System.Int32 easyar.DenseSpatialMapBuilderFrameFilter::get_BufferRequirement()
extern void DenseSpatialMapBuilderFrameFilter_get_BufferRequirement_m2B30546D9D53319D4A2D6AD06C5B2778C48E9DE7 ();
// 0x00000057 easyar.MotionTrackingStatus easyar.DenseSpatialMapBuilderFrameFilter::get_TrackingStatus()
extern void DenseSpatialMapBuilderFrameFilter_get_TrackingStatus_mC83FBD2C2E07474C3A4C320F89E33D0EA7B8AD24 ();
// 0x00000058 System.Void easyar.DenseSpatialMapBuilderFrameFilter::set_TrackingStatus(easyar.MotionTrackingStatus)
extern void DenseSpatialMapBuilderFrameFilter_set_TrackingStatus_m9D0217F76DF14D59E2A0A9D8B88044634D290E0F ();
// 0x00000059 System.Boolean easyar.DenseSpatialMapBuilderFrameFilter::get_RenderMesh()
extern void DenseSpatialMapBuilderFrameFilter_get_RenderMesh_m1020A63260241EBBC026709E0BB87B3603FCF3C3 ();
// 0x0000005A System.Void easyar.DenseSpatialMapBuilderFrameFilter::set_RenderMesh(System.Boolean)
extern void DenseSpatialMapBuilderFrameFilter_set_RenderMesh_mB7928B78D1045D5396F5AACA2C0AED02EF2292EB ();
// 0x0000005B UnityEngine.Color easyar.DenseSpatialMapBuilderFrameFilter::get_MeshColor()
extern void DenseSpatialMapBuilderFrameFilter_get_MeshColor_m7BAFA0F681CBA2FE618897FECF449B038F70254E ();
// 0x0000005C System.Void easyar.DenseSpatialMapBuilderFrameFilter::set_MeshColor(UnityEngine.Color)
extern void DenseSpatialMapBuilderFrameFilter_set_MeshColor_mA75249EC525C8820FB0F2E1F54306C4452BFA3D6 ();
// 0x0000005D System.Collections.Generic.List`1<easyar.DenseSpatialMapBlockController> easyar.DenseSpatialMapBuilderFrameFilter::get_MeshBlocks()
extern void DenseSpatialMapBuilderFrameFilter_get_MeshBlocks_m2314FFCA377A6C9F48AAA7099A530E287F7ACCA4 ();
// 0x0000005E System.Void easyar.DenseSpatialMapBuilderFrameFilter::Awake()
extern void DenseSpatialMapBuilderFrameFilter_Awake_mD0667AEE537C68C8581CC8387B45AAFBEA695A21 ();
// 0x0000005F System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnEnable()
extern void DenseSpatialMapBuilderFrameFilter_OnEnable_mD429BAAC4D28AD8E4743E1F8388D7274D68313BE ();
// 0x00000060 System.Void easyar.DenseSpatialMapBuilderFrameFilter::Start()
extern void DenseSpatialMapBuilderFrameFilter_Start_m306FA3E1AED84BFE90E59C328C0975BBDF52E550 ();
// 0x00000061 System.Void easyar.DenseSpatialMapBuilderFrameFilter::Update()
extern void DenseSpatialMapBuilderFrameFilter_Update_m5FFB8C97B34E0F3A1F9F9C8DE0390A89BF891FA5 ();
// 0x00000062 System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnDisable()
extern void DenseSpatialMapBuilderFrameFilter_OnDisable_m106ECD9F18A8E21DC253AB4927EF927C94D9E4CB ();
// 0x00000063 System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnDestroy()
extern void DenseSpatialMapBuilderFrameFilter_OnDestroy_m56E8749B425BF8A04BAC7516D4792B6899C329EC ();
// 0x00000064 easyar.InputFrameSink easyar.DenseSpatialMapBuilderFrameFilter::InputFrameSink()
extern void DenseSpatialMapBuilderFrameFilter_InputFrameSink_m110512ECC917ABEB5E213CDF5A4FFD88FD3511AE ();
// 0x00000065 System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnTracking(easyar.MotionTrackingStatus)
extern void DenseSpatialMapBuilderFrameFilter_OnTracking_m40AB0A3B409710B1A6F8ECCFD8A96031108829A6 ();
// 0x00000066 System.Void easyar.DenseSpatialMapBuilderFrameFilter::OnAssemble(easyar.ARSession)
extern void DenseSpatialMapBuilderFrameFilter_OnAssemble_m9D3815504DEE342A0DA86EAFBDCD95397ACBA68C ();
// 0x00000067 System.Void easyar.DenseSpatialMapBuilderFrameFilter::.ctor()
extern void DenseSpatialMapBuilderFrameFilter__ctor_m0F7AA39A84FE39B4C1DF1105233002D97BB6D461 ();
// 0x00000068 System.Void easyar.DenseSpatialMapBuilderFrameFilter::<OnAssemble>b__41_0(easyar.WorldRootController)
extern void DenseSpatialMapBuilderFrameFilter_U3COnAssembleU3Eb__41_0_mFF7C3081E15D05E38B031EF79F89EF3079A776E7 ();
// 0x00000069 UnityEngine.Camera easyar.DenseSpatialMapDepthRenderer::get_RenderDepthCamera()
extern void DenseSpatialMapDepthRenderer_get_RenderDepthCamera_m382384798191545BA128C42303D6C377042AFD22 ();
// 0x0000006A System.Void easyar.DenseSpatialMapDepthRenderer::set_RenderDepthCamera(UnityEngine.Camera)
extern void DenseSpatialMapDepthRenderer_set_RenderDepthCamera_mCC59B2C5575C29E0A3744E2DFD2D3836C0FA0393 ();
// 0x0000006B UnityEngine.Material easyar.DenseSpatialMapDepthRenderer::get_MapMeshMaterial()
extern void DenseSpatialMapDepthRenderer_get_MapMeshMaterial_m81069824EE0E3268CA3E5C1B3D5CC0271ABCD861 ();
// 0x0000006C System.Void easyar.DenseSpatialMapDepthRenderer::set_MapMeshMaterial(UnityEngine.Material)
extern void DenseSpatialMapDepthRenderer_set_MapMeshMaterial_mC114EAFF67F95E23CD861C07B22314F66AB608CA ();
// 0x0000006D System.Void easyar.DenseSpatialMapDepthRenderer::LateUpdate()
extern void DenseSpatialMapDepthRenderer_LateUpdate_m13D1B84AAF94857676931AF070191BBCD2DD127C ();
// 0x0000006E System.Void easyar.DenseSpatialMapDepthRenderer::OnDestroy()
extern void DenseSpatialMapDepthRenderer_OnDestroy_m7F89328203B1DE67DE23EE5DE4D8AF351676D323 ();
// 0x0000006F System.Void easyar.DenseSpatialMapDepthRenderer::.ctor()
extern void DenseSpatialMapDepthRenderer__ctor_m0D74AAE79950B9F0720119D59CF1B2D6FE830774 ();
// 0x00000070 easyar.EasyARController easyar.EasyARController::get_Instance()
extern void EasyARController_get_Instance_m250289045EB88AE3CA24A822FD6C39606C44BFCE ();
// 0x00000071 System.Void easyar.EasyARController::set_Instance(easyar.EasyARController)
extern void EasyARController_set_Instance_mC6775667DCB985AD0D7EA34A84BD80F402A435C5 ();
// 0x00000072 System.Boolean easyar.EasyARController::get_Initialized()
extern void EasyARController_get_Initialized_mB1836B5C067B9D5DBBBA82A85277FFA9EDCF893D ();
// 0x00000073 System.Void easyar.EasyARController::set_Initialized(System.Boolean)
extern void EasyARController_set_Initialized_m8FAA0F85A00F298300A9D0EF2EB569806B44A778 ();
// 0x00000074 System.Boolean easyar.EasyARController::get_ARCoreLoadFailed()
extern void EasyARController_get_ARCoreLoadFailed_m38C47AAB7B90EB9C769AAA78CFC0D7BFAA481DD3 ();
// 0x00000075 System.Void easyar.EasyARController::set_ARCoreLoadFailed(System.Boolean)
extern void EasyARController_set_ARCoreLoadFailed_m697E427016E226545C48DA4C1907F3C65001591A ();
// 0x00000076 easyar.DelayedCallbackScheduler easyar.EasyARController::get_Scheduler()
extern void EasyARController_get_Scheduler_m8DAE893DFBB226680525DFDBC5CDFE5FAFD26217 ();
// 0x00000077 System.Void easyar.EasyARController::set_Scheduler(easyar.DelayedCallbackScheduler)
extern void EasyARController_set_Scheduler_mC82E25FDF3EB0F562D02EFE166E035E51CC70BD5 ();
// 0x00000078 easyar.EasyARSettings easyar.EasyARController::get_Settings()
extern void EasyARController_get_Settings_mBC6A0D8058558E6EEFAF0F1E001F40F1441E7847 ();
// 0x00000079 System.String easyar.EasyARController::get_settingsPath()
extern void EasyARController_get_settingsPath_m4D0AFBF86A17FDC40C6F64CA3314A1B63F1E1C2F ();
// 0x0000007A easyar.ThreadWorker easyar.EasyARController::get_Worker()
extern void EasyARController_get_Worker_m7229F90382A04CD8E5479B54308CA1CA99262583 ();
// 0x0000007B System.Void easyar.EasyARController::set_Worker(easyar.ThreadWorker)
extern void EasyARController_set_Worker_m23A5547F9BD71C18E7F55A63D5E4BD499102F1BE ();
// 0x0000007C easyar.Display easyar.EasyARController::get_Display()
extern void EasyARController_get_Display_mDB1CBCB5D424EE49C95A695A2C65AF0332AC847F ();
// 0x0000007D System.Void easyar.EasyARController::set_Display(easyar.Display)
extern void EasyARController_set_Display_mE929E0FBEB29869D3184CC31AFC437C85324E5F8 ();
// 0x0000007E System.Void easyar.EasyARController::GlobalInitialization()
extern void EasyARController_GlobalInitialization_m0F6F13E8AF1C9EBA7F681BDC9678572DFB6725C8 ();
// 0x0000007F System.Void easyar.EasyARController::Awake()
extern void EasyARController_Awake_m9DE44F914646D283C640ABF641CDE0FBE2C57DE0 ();
// 0x00000080 System.Void easyar.EasyARController::Update()
extern void EasyARController_Update_mF21BEAF6AD60A2371E1B72EC76CBAFC8050DCE1C ();
// 0x00000081 System.Void easyar.EasyARController::OnApplicationPause(System.Boolean)
extern void EasyARController_OnApplicationPause_m6FBE6857CD7232F0ABFAF952AA059DBCA7872D91 ();
// 0x00000082 System.Void easyar.EasyARController::OnDestroy()
extern void EasyARController_OnDestroy_m6BE5CB6A91EF6B9EB3F8CFD154C76D3E38504000 ();
// 0x00000083 System.Void easyar.EasyARController::ShowErrorMessage()
extern void EasyARController_ShowErrorMessage_mF4B29F5C418DABF2441D267164FBD579067C1E61 ();
// 0x00000084 System.Void easyar.EasyARController::.ctor()
extern void EasyARController__ctor_mFBE2460C0F0475CCEC34C6A62CB0401F161DB384 ();
// 0x00000085 System.Void easyar.EasyARVersion::.ctor()
extern void EasyARVersion__ctor_m9084F0AD26EEF9D640CE351013E5A0E6F711D4EA ();
// 0x00000086 System.Int32 easyar.FrameFilter::get_BufferRequirement()
// 0x00000087 System.Void easyar.FrameFilter::OnAssemble(easyar.ARSession)
extern void FrameFilter_OnAssemble_m73EDFBEDA054D1CCE44E8DB8C761FC66439BF12F ();
// 0x00000088 System.Void easyar.FrameFilter::SetHFlip(System.Boolean)
extern void FrameFilter_SetHFlip_mA54A462BF41F4F24806BBA573460454D302BA090 ();
// 0x00000089 System.Void easyar.FrameFilter::OnHFlipChange(System.Boolean)
extern void FrameFilter_OnHFlipChange_m66AE1C45BD65CD476D463ED2EE8CDA581967D8C0 ();
// 0x0000008A System.Void easyar.FrameFilter::.ctor()
extern void FrameFilter__ctor_m760903724A2F512FAF1F5979781D96203C554D05 ();
// 0x0000008B System.Boolean easyar.FrameSource::get_HasSpatialInformation()
// 0x0000008C System.Void easyar.FrameSource::OnEnable()
extern void FrameSource_OnEnable_m557927C637CAF8F3E3E9C6B4A6C08BF41CB9AE38 ();
// 0x0000008D System.Void easyar.FrameSource::OnDisable()
extern void FrameSource_OnDisable_m1A333D5EDF00260C87129C5758E40BEE872459E4 ();
// 0x0000008E System.Void easyar.FrameSource::Connect(easyar.InputFrameSink)
extern void FrameSource_Connect_m79FC57A96372B158B6F45B7970775173BDFE74D0 ();
// 0x0000008F System.Void easyar.FrameSource::OnAssemble(easyar.ARSession)
extern void FrameSource_OnAssemble_mF8D2CC6BC4C3B77404EEC5AA9888079393559FD7 ();
// 0x00000090 System.Void easyar.FrameSource::.ctor()
extern void FrameSource__ctor_m77B174515107ACA329FC37974C3BBCC1673315C6 ();
// 0x00000091 easyar.ImageTarget easyar.ImageTargetController::get_Target()
extern void ImageTargetController_get_Target_m8B0AC3E30799AD0EE97C9CFA0E8FE70F5800C556 ();
// 0x00000092 System.Void easyar.ImageTargetController::set_Target(easyar.ImageTarget)
extern void ImageTargetController_set_Target_m8DCE0EDEC9A4F5DDD34835FDB2617CD61DF4EB89 ();
// 0x00000093 System.Void easyar.ImageTargetController::add_TargetAvailable(System.Action)
extern void ImageTargetController_add_TargetAvailable_m61FCAA07FC0D7F491EDAFFD274CD6E6D9BE3BACC ();
// 0x00000094 System.Void easyar.ImageTargetController::remove_TargetAvailable(System.Action)
extern void ImageTargetController_remove_TargetAvailable_mB7FAD81DB044E3B09A4BC336CA2FE6057D99C3DC ();
// 0x00000095 System.Void easyar.ImageTargetController::add_TargetLoad(System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTargetController_add_TargetLoad_m3F9617B73200D95FD67B78356EFA3B680C6BB585 ();
// 0x00000096 System.Void easyar.ImageTargetController::remove_TargetLoad(System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTargetController_remove_TargetLoad_m6B194F9A77DC003894517D8D6067546347979FF8 ();
// 0x00000097 System.Void easyar.ImageTargetController::add_TargetUnload(System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTargetController_add_TargetUnload_m65371B11BF29BCE1015CAE0877A886200FEB3B84 ();
// 0x00000098 System.Void easyar.ImageTargetController::remove_TargetUnload(System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTargetController_remove_TargetUnload_m66574460CECBB9212D1574A62FD1C658B13D87C2 ();
// 0x00000099 easyar.ImageTrackerFrameFilter easyar.ImageTargetController::get_Tracker()
extern void ImageTargetController_get_Tracker_m0668F827BD43B2D9B2B701118103712764815D66 ();
// 0x0000009A System.Void easyar.ImageTargetController::set_Tracker(easyar.ImageTrackerFrameFilter)
extern void ImageTargetController_set_Tracker_mEA5F5C869C0120911C86D5768A3F440C6430E479 ();
// 0x0000009B UnityEngine.Vector2 easyar.ImageTargetController::get_Size()
extern void ImageTargetController_get_Size_mB815DC19252C3209093C310E5D4A5A5529C132DD ();
// 0x0000009C System.Void easyar.ImageTargetController::set_Size(UnityEngine.Vector2)
extern void ImageTargetController_set_Size_m160FBAC46D3BDB4A1F7D58D55BD5C9D62CCE4732 ();
// 0x0000009D System.Void easyar.ImageTargetController::Start()
extern void ImageTargetController_Start_m5C8D158C124FD40B8488207AC5148AFDD73955F5 ();
// 0x0000009E System.Void easyar.ImageTargetController::Update()
extern void ImageTargetController_Update_mCE4F6B2328593E2B974B6C83D1BA0943375C0CF0 ();
// 0x0000009F System.Void easyar.ImageTargetController::OnDestroy()
extern void ImageTargetController_OnDestroy_mD97CB9C3275DCF98E03BED694AA59F7344FE23A8 ();
// 0x000000A0 System.Void easyar.ImageTargetController::OnTracking()
extern void ImageTargetController_OnTracking_m5A44561EE348C461B5044C79C335946D3DF1EB66 ();
// 0x000000A1 System.Void easyar.ImageTargetController::LoadImageFile(easyar.ImageTargetController_ImageFileSourceData)
extern void ImageTargetController_LoadImageFile_m294CB3FB597D036E54ED90808402B4ED6097B7C1 ();
// 0x000000A2 System.Void easyar.ImageTargetController::LoadTargetDataFile(easyar.ImageTargetController_TargetDataFileSourceData)
extern void ImageTargetController_LoadTargetDataFile_m9C798903F31946B61579686C5FCA7C8980352604 ();
// 0x000000A3 System.Void easyar.ImageTargetController::LoadTarget(easyar.ImageTarget)
extern void ImageTargetController_LoadTarget_m8A0FC0C24CD77BED18DD3A89043A5254B27BB147 ();
// 0x000000A4 System.Collections.IEnumerator easyar.ImageTargetController::LoadImageBuffer(easyar.Buffer,easyar.ImageTargetController_ImageFileSourceData)
extern void ImageTargetController_LoadImageBuffer_m2DFFFA1A96BA49257DAFC8B59B5C014D9B704400 ();
// 0x000000A5 System.Collections.IEnumerator easyar.ImageTargetController::LoadTargetDataBuffer(easyar.Buffer)
extern void ImageTargetController_LoadTargetDataBuffer_mC6F727AD7044E17F49CA921AF4B80E25E923C345 ();
// 0x000000A6 System.Void easyar.ImageTargetController::UpdateTargetInTracker()
extern void ImageTargetController_UpdateTargetInTracker_m6260EF9F239468D58D8BBD5F501573C7A22CD8B4 ();
// 0x000000A7 System.Void easyar.ImageTargetController::UpdateScale()
extern void ImageTargetController_UpdateScale_mA1F3E7D8276DD63CCC1B9CF3F4E10388141A639F ();
// 0x000000A8 System.Void easyar.ImageTargetController::CheckScale()
extern void ImageTargetController_CheckScale_m698A32A83BA6365B15141E339CDE7667E0049962 ();
// 0x000000A9 System.Void easyar.ImageTargetController::.ctor()
extern void ImageTargetController__ctor_mE615AE5DA14062F27BAC6FAC5541D57E9FBCFB78 ();
// 0x000000AA System.Void easyar.ImageTargetController::<LoadTargetDataFile>b__35_0(easyar.Buffer)
extern void ImageTargetController_U3CLoadTargetDataFileU3Eb__35_0_mDF99BD172638F248D5CD38A57F30BE818F47BB4F ();
// 0x000000AB System.Void easyar.ImageTargetController::<UpdateTargetInTracker>b__39_0(easyar.Target,System.Boolean)
extern void ImageTargetController_U3CUpdateTargetInTrackerU3Eb__39_0_m5834DF12090C7961854AAE207B97F60371434E61 ();
// 0x000000AC easyar.ImageTracker easyar.ImageTrackerFrameFilter::get_Tracker()
extern void ImageTrackerFrameFilter_get_Tracker_m3D2FFF6BAF9AD9F9E74A0FC049C4980835DF8B0B ();
// 0x000000AD System.Void easyar.ImageTrackerFrameFilter::set_Tracker(easyar.ImageTracker)
extern void ImageTrackerFrameFilter_set_Tracker_m23AA28650516A34FBF421033390014003434F5C2 ();
// 0x000000AE System.Void easyar.ImageTrackerFrameFilter::add_TargetLoad(System.Action`3<easyar.ImageTargetController,easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_add_TargetLoad_mE1ACF6D43360BAB2B79B9C573D25E8E81E8BEE8A ();
// 0x000000AF System.Void easyar.ImageTrackerFrameFilter::remove_TargetLoad(System.Action`3<easyar.ImageTargetController,easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_remove_TargetLoad_m81B83DA297977CB14E0392BD888C5E14BE5896AA ();
// 0x000000B0 System.Void easyar.ImageTrackerFrameFilter::add_TargetUnload(System.Action`3<easyar.ImageTargetController,easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_add_TargetUnload_m98A2AD4ECB81B3AE06C261FA17059C430BCF7BD9 ();
// 0x000000B1 System.Void easyar.ImageTrackerFrameFilter::remove_TargetUnload(System.Action`3<easyar.ImageTargetController,easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_remove_TargetUnload_m483C31750EFE2C8AA216657F002E36491B5EEC81 ();
// 0x000000B2 System.Void easyar.ImageTrackerFrameFilter::add_SimultaneousNumChanged(System.Action)
extern void ImageTrackerFrameFilter_add_SimultaneousNumChanged_m925A65B883FEA8F1AD02F795C484CB8DC0CA29B5 ();
// 0x000000B3 System.Void easyar.ImageTrackerFrameFilter::remove_SimultaneousNumChanged(System.Action)
extern void ImageTrackerFrameFilter_remove_SimultaneousNumChanged_mF091A3A63D08F4864CD1FA8F393965C6522F618D ();
// 0x000000B4 System.Int32 easyar.ImageTrackerFrameFilter::get_BufferRequirement()
extern void ImageTrackerFrameFilter_get_BufferRequirement_m6C19A8699AECB0599863810204006751C08EEE6C ();
// 0x000000B5 System.Int32 easyar.ImageTrackerFrameFilter::get_SimultaneousNum()
extern void ImageTrackerFrameFilter_get_SimultaneousNum_mFA8A36DE26A3F992D6FA776091E85FB8D652702C ();
// 0x000000B6 System.Void easyar.ImageTrackerFrameFilter::set_SimultaneousNum(System.Int32)
extern void ImageTrackerFrameFilter_set_SimultaneousNum_m524A9078C55FFB3E637E37F75D04806A409DA8DB ();
// 0x000000B7 System.Collections.Generic.List`1<easyar.TargetController> easyar.ImageTrackerFrameFilter::get_TargetControllers()
extern void ImageTrackerFrameFilter_get_TargetControllers_m4F818F86065E7B8A6E910025C3C12273F5EEC706 ();
// 0x000000B8 System.Void easyar.ImageTrackerFrameFilter::set_TargetControllers(System.Collections.Generic.List`1<easyar.TargetController>)
extern void ImageTrackerFrameFilter_set_TargetControllers_m05B8FDBE41055195DA550DDFDBB959284BC7B6ED ();
// 0x000000B9 System.Void easyar.ImageTrackerFrameFilter::Awake()
extern void ImageTrackerFrameFilter_Awake_m237BF301F632D9BD47D52D349FD6455EFB5D5BC1 ();
// 0x000000BA System.Void easyar.ImageTrackerFrameFilter::OnEnable()
extern void ImageTrackerFrameFilter_OnEnable_mE17BAB20E63D5091F079D653F9DFA750280482D0 ();
// 0x000000BB System.Void easyar.ImageTrackerFrameFilter::Start()
extern void ImageTrackerFrameFilter_Start_m83DA9C4BC17A0FF4F298F75BD1AC5B3C3778FB4A ();
// 0x000000BC System.Void easyar.ImageTrackerFrameFilter::OnDisable()
extern void ImageTrackerFrameFilter_OnDisable_m29948FDCADC10B086CDFBD08945BC4656AB734E3 ();
// 0x000000BD System.Void easyar.ImageTrackerFrameFilter::OnDestroy()
extern void ImageTrackerFrameFilter_OnDestroy_m633CE8B036F8C43C0F011091FF690E6197F47178 ();
// 0x000000BE System.Void easyar.ImageTrackerFrameFilter::LoadTarget(easyar.ImageTargetController)
extern void ImageTrackerFrameFilter_LoadTarget_mFC0973031EF4D8E865EC3E77BF1388A4CFB6CEEA ();
// 0x000000BF System.Void easyar.ImageTrackerFrameFilter::UnloadTarget(easyar.ImageTargetController)
extern void ImageTrackerFrameFilter_UnloadTarget_m4C415902AFFC709EE07152AFA5E9B91808B8D03C ();
// 0x000000C0 easyar.FeedbackFrameSink easyar.ImageTrackerFrameFilter::FeedbackFrameSink()
extern void ImageTrackerFrameFilter_FeedbackFrameSink_m51AE4F48886529C19205FA9C869E718B2AA3A553 ();
// 0x000000C1 easyar.OutputFrameSource easyar.ImageTrackerFrameFilter::OutputFrameSource()
extern void ImageTrackerFrameFilter_OutputFrameSource_mF6470630AC38434F0F125CDD969B3A79575ED48D ();
// 0x000000C2 System.Void easyar.ImageTrackerFrameFilter::OnAssemble(easyar.ARSession)
extern void ImageTrackerFrameFilter_OnAssemble_mAD25A1052206E0E532A308FAEA998CBBF327B239 ();
// 0x000000C3 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.ImageTrackerFrameFilter::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
extern void ImageTrackerFrameFilter_OnResult_mED52ED5D8C18C3A152A5A27EF726B80E8CC711A9 ();
// 0x000000C4 System.Void easyar.ImageTrackerFrameFilter::LoadImageTarget(easyar.ImageTargetController,System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_LoadImageTarget_m980E0FAB923C9F26E8F7B1D19A2B06A3C6DDDBD8 ();
// 0x000000C5 System.Void easyar.ImageTrackerFrameFilter::UnloadImageTarget(easyar.ImageTargetController,System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTrackerFrameFilter_UnloadImageTarget_m682EF75C6ECE8E903CC1A58E1170420465FC0F76 ();
// 0x000000C6 System.Void easyar.ImageTrackerFrameFilter::OnHFlipChange(System.Boolean)
extern void ImageTrackerFrameFilter_OnHFlipChange_m0B8CCE8F4AD560F4317AF8972DBBC7B98A91F9A4 ();
// 0x000000C7 easyar.TargetController easyar.ImageTrackerFrameFilter::TryGetTargetController(System.Int32)
extern void ImageTrackerFrameFilter_TryGetTargetController_m2E97A328A1190564B5129699A8C6FBD8CF3827A7 ();
// 0x000000C8 System.Void easyar.ImageTrackerFrameFilter::.ctor()
extern void ImageTrackerFrameFilter__ctor_m0C5E64C741355925257847439C2E17881B365C82 ();
// 0x000000C9 easyar.ObjectTarget easyar.ObjectTargetController::get_Target()
extern void ObjectTargetController_get_Target_m16CE294BE63B6194BE261E3F617DD90041AC7470 ();
// 0x000000CA System.Void easyar.ObjectTargetController::set_Target(easyar.ObjectTarget)
extern void ObjectTargetController_set_Target_mF53E78FA16B7FF26D5DF60C3B930EEDF13816660 ();
// 0x000000CB System.Void easyar.ObjectTargetController::add_TargetAvailable(System.Action)
extern void ObjectTargetController_add_TargetAvailable_m1EF5D4C7882EB88D0C9D694F45669E8D1EFF32E7 ();
// 0x000000CC System.Void easyar.ObjectTargetController::remove_TargetAvailable(System.Action)
extern void ObjectTargetController_remove_TargetAvailable_m962874F15DE3DA3E09B7013368065213836E6495 ();
// 0x000000CD System.Void easyar.ObjectTargetController::add_TargetLoad(System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTargetController_add_TargetLoad_mE227DEAE3A2AD9ED188F6C7D24885E1D2BB6CBB9 ();
// 0x000000CE System.Void easyar.ObjectTargetController::remove_TargetLoad(System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTargetController_remove_TargetLoad_m45A771C4438DDC2C526BBE76BF3AE294BC113DC2 ();
// 0x000000CF System.Void easyar.ObjectTargetController::add_TargetUnload(System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTargetController_add_TargetUnload_m780A3151B91A3897FFF4CE70BDD588E6F0F8CCAA ();
// 0x000000D0 System.Void easyar.ObjectTargetController::remove_TargetUnload(System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTargetController_remove_TargetUnload_m1BC458DCA7DAEE1D5554E2BCD8CC500763635BAD ();
// 0x000000D1 easyar.ObjectTrackerFrameFilter easyar.ObjectTargetController::get_Tracker()
extern void ObjectTargetController_get_Tracker_mBC973AEB4F4BC408691E82584F8AD8C7FAD30058 ();
// 0x000000D2 System.Void easyar.ObjectTargetController::set_Tracker(easyar.ObjectTrackerFrameFilter)
extern void ObjectTargetController_set_Tracker_mC56FE6AE07641C0A523AFDA370F17AB54A48255D ();
// 0x000000D3 System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.ObjectTargetController::get_BoundingBox()
extern void ObjectTargetController_get_BoundingBox_m3DA8965C34B9F68907A818BA4FE46A7FEE0D95B6 ();
// 0x000000D4 System.Void easyar.ObjectTargetController::set_BoundingBox(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObjectTargetController_set_BoundingBox_mCB1C618112B490E51BDF8C160FFC45C39434BCFC ();
// 0x000000D5 System.Void easyar.ObjectTargetController::Start()
extern void ObjectTargetController_Start_m8E3C0202B0F5D12D5ADD38CEAEDC80FA0B267D8F ();
// 0x000000D6 System.Void easyar.ObjectTargetController::Update()
extern void ObjectTargetController_Update_mF6BC6494199C27B177DAD977F4086B178B95139F ();
// 0x000000D7 System.Void easyar.ObjectTargetController::OnDestroy()
extern void ObjectTargetController_OnDestroy_m30C21E9254259690FDBC5E6216D4BF23C647827F ();
// 0x000000D8 System.Void easyar.ObjectTargetController::OnTracking()
extern void ObjectTargetController_OnTracking_m3AB79F0D7F9755DB368E2DC4878B6316AFEF83F4 ();
// 0x000000D9 System.Void easyar.ObjectTargetController::LoadObjFile(easyar.ObjectTargetController_ObjFileSourceData)
extern void ObjectTargetController_LoadObjFile_m7F17A33847B51BF936B3C2B363C6A9760E3CBD24 ();
// 0x000000DA System.Void easyar.ObjectTargetController::LoadTarget(easyar.ObjectTarget)
extern void ObjectTargetController_LoadTarget_m13C7B89A46C6AFAB5A3B1EF22348CDB01BA9F0C3 ();
// 0x000000DB System.Collections.IEnumerator easyar.ObjectTargetController::LoadObjFileFromSource(easyar.ObjectTargetController_ObjFileSourceData)
extern void ObjectTargetController_LoadObjFileFromSource_m3A0AB39A41C84BFB96005722409A4DC4CD00872F ();
// 0x000000DC System.Void easyar.ObjectTargetController::UpdateTargetInTracker()
extern void ObjectTargetController_UpdateTargetInTracker_m2E8F83917286436DD63854A71DA69F0F47BF52EB ();
// 0x000000DD System.Void easyar.ObjectTargetController::UpdateScale()
extern void ObjectTargetController_UpdateScale_mBFB128386D5F81259ED4F98E576A09B92EC38704 ();
// 0x000000DE System.Void easyar.ObjectTargetController::CheckScale()
extern void ObjectTargetController_CheckScale_mD726738CA9E526250533C854910294370DA95C29 ();
// 0x000000DF System.Void easyar.ObjectTargetController::.ctor()
extern void ObjectTargetController__ctor_m1C391159D7BCD3D09D6C320EAAF4411656A67906 ();
// 0x000000E0 System.Void easyar.ObjectTargetController::<UpdateTargetInTracker>b__36_0(easyar.Target,System.Boolean)
extern void ObjectTargetController_U3CUpdateTargetInTrackerU3Eb__36_0_m8C1180792D337C877A18944C562FFF5E4EBC0382 ();
// 0x000000E1 easyar.ObjectTracker easyar.ObjectTrackerFrameFilter::get_Tracker()
extern void ObjectTrackerFrameFilter_get_Tracker_mC2D9FFC0B3811F42D982EB401F8498259189A58E ();
// 0x000000E2 System.Void easyar.ObjectTrackerFrameFilter::set_Tracker(easyar.ObjectTracker)
extern void ObjectTrackerFrameFilter_set_Tracker_m188BDC6079AC0E69721BDE529B35871DBB86A900 ();
// 0x000000E3 System.Void easyar.ObjectTrackerFrameFilter::add_TargetLoad(System.Action`3<easyar.ObjectTargetController,easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_add_TargetLoad_m5A2C7969CAF2694AA2032CBD46A2D5B4440FCD09 ();
// 0x000000E4 System.Void easyar.ObjectTrackerFrameFilter::remove_TargetLoad(System.Action`3<easyar.ObjectTargetController,easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_remove_TargetLoad_mFB68ED3A4B3311F8B229C097DE988121D2D4A902 ();
// 0x000000E5 System.Void easyar.ObjectTrackerFrameFilter::add_TargetUnload(System.Action`3<easyar.ObjectTargetController,easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_add_TargetUnload_mDF22F93E75201147273ADBF6163BB292E0CF7C6A ();
// 0x000000E6 System.Void easyar.ObjectTrackerFrameFilter::remove_TargetUnload(System.Action`3<easyar.ObjectTargetController,easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_remove_TargetUnload_m2112DB26C1F8BDA5F1AD00BF09D921781BCB9263 ();
// 0x000000E7 System.Void easyar.ObjectTrackerFrameFilter::add_SimultaneousNumChanged(System.Action)
extern void ObjectTrackerFrameFilter_add_SimultaneousNumChanged_mF7AA2DB4F2C411124F0C7B6FCCE0F0BBAA5DA3A9 ();
// 0x000000E8 System.Void easyar.ObjectTrackerFrameFilter::remove_SimultaneousNumChanged(System.Action)
extern void ObjectTrackerFrameFilter_remove_SimultaneousNumChanged_m273F8B5AFEA5440650E5012FE72972BFD65A9591 ();
// 0x000000E9 System.Int32 easyar.ObjectTrackerFrameFilter::get_BufferRequirement()
extern void ObjectTrackerFrameFilter_get_BufferRequirement_m7A4C6591B13C8F1E4957E3B5BC7AD8AD12B1FA67 ();
// 0x000000EA System.Int32 easyar.ObjectTrackerFrameFilter::get_SimultaneousNum()
extern void ObjectTrackerFrameFilter_get_SimultaneousNum_mF68B05BD48E68601E9FA53004F30D460123AA484 ();
// 0x000000EB System.Void easyar.ObjectTrackerFrameFilter::set_SimultaneousNum(System.Int32)
extern void ObjectTrackerFrameFilter_set_SimultaneousNum_m89366C879DD4345A961CB52CE83D3277FED6DC20 ();
// 0x000000EC System.Collections.Generic.List`1<easyar.TargetController> easyar.ObjectTrackerFrameFilter::get_TargetControllers()
extern void ObjectTrackerFrameFilter_get_TargetControllers_m970DF66E890CBAC3519B7DC88D2FF24454E4D181 ();
// 0x000000ED System.Void easyar.ObjectTrackerFrameFilter::set_TargetControllers(System.Collections.Generic.List`1<easyar.TargetController>)
extern void ObjectTrackerFrameFilter_set_TargetControllers_mC0DA0165575F7BF0FBC0A7C7EA4D122391262D70 ();
// 0x000000EE System.Void easyar.ObjectTrackerFrameFilter::Awake()
extern void ObjectTrackerFrameFilter_Awake_m1D87FEABBA37419A571573174FC7314CFE413521 ();
// 0x000000EF System.Void easyar.ObjectTrackerFrameFilter::OnEnable()
extern void ObjectTrackerFrameFilter_OnEnable_m4E4C7E6EB0D3E92D1806295D8E8442791D68E7E2 ();
// 0x000000F0 System.Void easyar.ObjectTrackerFrameFilter::Start()
extern void ObjectTrackerFrameFilter_Start_m124441769254E7DF369036E0267D9956BB47AA34 ();
// 0x000000F1 System.Void easyar.ObjectTrackerFrameFilter::OnDisable()
extern void ObjectTrackerFrameFilter_OnDisable_mDC6BE8A3BA675036F7642A8EE4EC2479DE505194 ();
// 0x000000F2 System.Void easyar.ObjectTrackerFrameFilter::OnDestroy()
extern void ObjectTrackerFrameFilter_OnDestroy_m0198FE12633C1B23D7C7017BDFBB667AA9F45CD9 ();
// 0x000000F3 System.Void easyar.ObjectTrackerFrameFilter::LoadTarget(easyar.ObjectTargetController)
extern void ObjectTrackerFrameFilter_LoadTarget_mCE2EFE49E8DC7F673DF9A94DBAD094514DF33658 ();
// 0x000000F4 System.Void easyar.ObjectTrackerFrameFilter::UnloadTarget(easyar.ObjectTargetController)
extern void ObjectTrackerFrameFilter_UnloadTarget_m95C90F00445C4211D6EB1A9B30117269088375BE ();
// 0x000000F5 easyar.FeedbackFrameSink easyar.ObjectTrackerFrameFilter::FeedbackFrameSink()
extern void ObjectTrackerFrameFilter_FeedbackFrameSink_m81CE66B6145420E3730CEE878D2CF2267396B55C ();
// 0x000000F6 easyar.OutputFrameSource easyar.ObjectTrackerFrameFilter::OutputFrameSource()
extern void ObjectTrackerFrameFilter_OutputFrameSource_m6F4BE7B63C402E1986B3FC658BE9D17FECFA0776 ();
// 0x000000F7 System.Void easyar.ObjectTrackerFrameFilter::OnAssemble(easyar.ARSession)
extern void ObjectTrackerFrameFilter_OnAssemble_m46FA6E02BCB97D3A98B1AFAF93BA206DD0685815 ();
// 0x000000F8 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.ObjectTrackerFrameFilter::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
extern void ObjectTrackerFrameFilter_OnResult_mA74735CEDF8DBD1D1FAC50FEA95A2958610F01CA ();
// 0x000000F9 System.Void easyar.ObjectTrackerFrameFilter::LoadObjectTarget(easyar.ObjectTargetController,System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_LoadObjectTarget_mF983B406F6009BD2768C99A6153905DBE7649552 ();
// 0x000000FA System.Void easyar.ObjectTrackerFrameFilter::UnloadObjectTarget(easyar.ObjectTargetController,System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTrackerFrameFilter_UnloadObjectTarget_mAA2BF1DB51932A291662593B5A6DBF5F795C6909 ();
// 0x000000FB System.Void easyar.ObjectTrackerFrameFilter::OnHFlipChange(System.Boolean)
extern void ObjectTrackerFrameFilter_OnHFlipChange_m39F6BCF327D2205C21E92A3C81FDE6C03B683E6C ();
// 0x000000FC easyar.TargetController easyar.ObjectTrackerFrameFilter::TryGetTargetController(System.Int32)
extern void ObjectTrackerFrameFilter_TryGetTargetController_m1A646A636C2268590C8D88D6D9B97468573D0C61 ();
// 0x000000FD System.Void easyar.ObjectTrackerFrameFilter::.ctor()
extern void ObjectTrackerFrameFilter__ctor_m8D11E9FB6A82302FA2C7BFBD67A59126ABB84301 ();
// 0x000000FE System.Void easyar.RenderCameraController::OnEnable()
extern void RenderCameraController_OnEnable_m53DA30030D0625E0ACE503EB26C1609A7EA4BAE7 ();
// 0x000000FF System.Void easyar.RenderCameraController::OnDisable()
extern void RenderCameraController_OnDisable_m49C7F48FBA009CB784ED41565F8B2DC5B867B37B ();
// 0x00000100 System.Void easyar.RenderCameraController::OnDestroy()
extern void RenderCameraController_OnDestroy_m2E67508A90E5E02240C673769F4B37D16B64317C ();
// 0x00000101 System.Void easyar.RenderCameraController::OnAssemble(easyar.ARSession)
extern void RenderCameraController_OnAssemble_mD757178B9D7920563E82B5E82E8DCDA414E3B209 ();
// 0x00000102 System.Void easyar.RenderCameraController::SetProjectHFlip(System.Boolean)
extern void RenderCameraController_SetProjectHFlip_m4E0AB184DD077D2C57DC4D71F9CF80D0F14FDA8A ();
// 0x00000103 System.Void easyar.RenderCameraController::SetRenderImageHFilp(System.Boolean)
extern void RenderCameraController_SetRenderImageHFilp_m8C494C3B787A43F214E34A2728C45A09E5BAD2E5 ();
// 0x00000104 System.Void easyar.RenderCameraController::OnFrameChange(easyar.OutputFrame,UnityEngine.Matrix4x4)
extern void RenderCameraController_OnFrameChange_m4A98513039B0D87681CB27E90D9312E4AF17261D ();
// 0x00000105 System.Void easyar.RenderCameraController::OnFrameUpdate(easyar.OutputFrame)
extern void RenderCameraController_OnFrameUpdate_m7741F651B007C8249018D636082FBF4BFF68D0EB ();
// 0x00000106 System.Void easyar.RenderCameraController::.ctor()
extern void RenderCameraController__ctor_mD1DA2801D264D062996388767FF3B24061D53875 ();
// 0x00000107 System.Void easyar.RenderCameraController::<OnFrameUpdate>b__15_0()
extern void RenderCameraController_U3COnFrameUpdateU3Eb__15_0_m9CF747F8D0B80D455064296EE446713E737D9075 ();
// 0x00000108 System.Void easyar.RenderCameraController::<OnFrameUpdate>b__15_1()
extern void RenderCameraController_U3COnFrameUpdateU3Eb__15_1_m19E40B28AEEDA6FC78F91CBCD30773FF90E59F2F ();
// 0x00000109 System.Void easyar.CameraImageMaterial::.ctor()
extern void CameraImageMaterial__ctor_m72BD5A17BE31FB0CC35528EEE02B2B3812C51A4B ();
// 0x0000010A System.Void easyar.CameraImageMaterial::Finalize()
extern void CameraImageMaterial_Finalize_m7BAFC502DD6045B3AF1BC91BC0EFDB84B4FD8315 ();
// 0x0000010B System.Void easyar.CameraImageMaterial::Dispose()
extern void CameraImageMaterial_Dispose_m2BCD8585031CC4B9789E581BD0587295CFDC7DEB ();
// 0x0000010C UnityEngine.Material easyar.CameraImageMaterial::UpdateByImage(easyar.Image)
extern void CameraImageMaterial_UpdateByImage_m720FFB03834C7290957FE1D337794689B8B858C9 ();
// 0x0000010D System.Void easyar.CameraImageMaterial::DisposeResources()
extern void CameraImageMaterial_DisposeResources_mA8FCBB5783A257A07809E72B8A0AFBB2D6FE23C2 ();
// 0x0000010E System.Void easyar.EasyARSettings::.ctor()
extern void EasyARSettings__ctor_m8B67ADD90D834FD829822FA5D724BF5E4B48B5D8 ();
// 0x0000010F System.Void easyar.EasyARShaders::.ctor()
extern void EasyARShaders__ctor_m72997C5757B905E8795B24917DB157E791E13960 ();
// 0x00000110 System.Void easyar.RenderCameraParameters::Finalize()
extern void RenderCameraParameters_Finalize_m14071E54EB673B9F03D16331A0ECEA310FF3B2B7 ();
// 0x00000111 UnityEngine.Matrix4x4 easyar.RenderCameraParameters::get_Transform()
extern void RenderCameraParameters_get_Transform_m554C10D0BEE296CC4B231778F512DDBFAA6AE5FB ();
// 0x00000112 System.Void easyar.RenderCameraParameters::set_Transform(UnityEngine.Matrix4x4)
extern void RenderCameraParameters_set_Transform_m28141E0952C0660A10839219F63F4609B6BEC086 ();
// 0x00000113 easyar.CameraParameters easyar.RenderCameraParameters::get_Parameters()
extern void RenderCameraParameters_get_Parameters_m780DB8B58ECD742C9534AF5B6AF6BFF14AF1BAE8 ();
// 0x00000114 System.Void easyar.RenderCameraParameters::set_Parameters(easyar.CameraParameters)
extern void RenderCameraParameters_set_Parameters_mDB2F0A0974E7BF09410D7ED69748E7F3E4F6D2C9 ();
// 0x00000115 System.Void easyar.RenderCameraParameters::Build(easyar.CameraParameters)
extern void RenderCameraParameters_Build_m403B648D7B010E40679A5B9317FC20405352B3B5 ();
// 0x00000116 System.Void easyar.RenderCameraParameters::Dispose()
extern void RenderCameraParameters_Dispose_mEE431E506129BE66A1BBCE930AC50EF71CD09B4B ();
// 0x00000117 System.Void easyar.RenderCameraParameters::.ctor()
extern void RenderCameraParameters__ctor_m5CDEBEBEED27B2EE8552B31F4F3D7A06F6E7F0E5 ();
// 0x00000118 System.Void easyar.RenderCameraParameters::.cctor()
extern void RenderCameraParameters__cctor_mF0122F584F5394568EF219979FA7941FD9304F5D ();
// 0x00000119 easyar.SparseSpatialMapController_SparseSpatialMapInfo easyar.SparseSpatialMapController::get_MapInfo()
extern void SparseSpatialMapController_get_MapInfo_m1023D0926121715A6E700A64BA025B2CFAC51E76 ();
// 0x0000011A System.Void easyar.SparseSpatialMapController::set_MapInfo(easyar.SparseSpatialMapController_SparseSpatialMapInfo)
extern void SparseSpatialMapController_set_MapInfo_m5F926AB161BD7024F999AD525D0BE41BA0053684 ();
// 0x0000011B System.Void easyar.SparseSpatialMapController::add_MapInfoAvailable(System.Action)
extern void SparseSpatialMapController_add_MapInfoAvailable_mECD14B0BB12A4683D79DF1796E19B88721BDE923 ();
// 0x0000011C System.Void easyar.SparseSpatialMapController::remove_MapInfoAvailable(System.Action)
extern void SparseSpatialMapController_remove_MapInfoAvailable_m6B85B7B15AC9764233A60B184CA144EF7BF1E613 ();
// 0x0000011D System.Void easyar.SparseSpatialMapController::add_MapLocalized(System.Action)
extern void SparseSpatialMapController_add_MapLocalized_m720B7DA81252884CA06448EF190D0742AA0F688D ();
// 0x0000011E System.Void easyar.SparseSpatialMapController::remove_MapLocalized(System.Action)
extern void SparseSpatialMapController_remove_MapLocalized_m0982BF72AA803AAC66EC155CC7BF3EEF7BE69C26 ();
// 0x0000011F System.Void easyar.SparseSpatialMapController::add_MapStopLocalize(System.Action)
extern void SparseSpatialMapController_add_MapStopLocalize_m906D8D831FB93E18F7927E5C15D5B616EF4C1AC0 ();
// 0x00000120 System.Void easyar.SparseSpatialMapController::remove_MapStopLocalize(System.Action)
extern void SparseSpatialMapController_remove_MapStopLocalize_m75ACCFAB5353B9452CDBB26C9D9B7F9DEE4B1111 ();
// 0x00000121 System.Void easyar.SparseSpatialMapController::add_MapLoad(System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_add_MapLoad_m8CB6B0C8E8A08747DD69C4CC4206CBCCE64502B5 ();
// 0x00000122 System.Void easyar.SparseSpatialMapController::remove_MapLoad(System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_remove_MapLoad_m8AADEE5A8C792CB8C2696002BBDCB0DE714D46F8 ();
// 0x00000123 System.Void easyar.SparseSpatialMapController::add_MapUnload(System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_add_MapUnload_mC00E1158C3F95B8FBFC2B040F60228F8737AC5D9 ();
// 0x00000124 System.Void easyar.SparseSpatialMapController::remove_MapUnload(System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_remove_MapUnload_m2A259C65B934350ED6B742D0479F7A631BE41D4F ();
// 0x00000125 System.Void easyar.SparseSpatialMapController::add_MapHost(System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_add_MapHost_m12417AB0C884F7702D392A962196351745BDF052 ();
// 0x00000126 System.Void easyar.SparseSpatialMapController::remove_MapHost(System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapController_remove_MapHost_m4C4CFA9B89E0E5FBF93D5824FC24C900DAD1B716 ();
// 0x00000127 easyar.SparseSpatialMapWorkerFrameFilter easyar.SparseSpatialMapController::get_MapWorker()
extern void SparseSpatialMapController_get_MapWorker_mB5F31F4C3B5BDADFE3C2DDD1AFECEBBEC094EBA2 ();
// 0x00000128 System.Void easyar.SparseSpatialMapController::set_MapWorker(easyar.SparseSpatialMapWorkerFrameFilter)
extern void SparseSpatialMapController_set_MapWorker_m8BC5C0188D493BCB17E1E2469CA5163D60ADF5FB ();
// 0x00000129 easyar.SparseSpatialMapController_ParticleParameter easyar.SparseSpatialMapController::get_PointCloudParticleParameter()
extern void SparseSpatialMapController_get_PointCloudParticleParameter_m1295E02446E4CCE486509B53F31E01ECE543FFB0 ();
// 0x0000012A System.Void easyar.SparseSpatialMapController::set_PointCloudParticleParameter(easyar.SparseSpatialMapController_ParticleParameter)
extern void SparseSpatialMapController_set_PointCloudParticleParameter_m0B161234431AE951E23CC7D7E2F44C0DF59A9E07 ();
// 0x0000012B System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.SparseSpatialMapController::get_PointCloud()
extern void SparseSpatialMapController_get_PointCloud_mF42A58E96D27AC788C6F8005176ADD469D419D42 ();
// 0x0000012C System.Void easyar.SparseSpatialMapController::set_PointCloud(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void SparseSpatialMapController_set_PointCloud_mE0E20F0F338C519F5CFC610B8C80B76B872692A8 ();
// 0x0000012D System.Boolean easyar.SparseSpatialMapController::get_ShowPointCloud()
extern void SparseSpatialMapController_get_ShowPointCloud_mB42B6C17DFDE159FC9447989116E76CAA0C73747 ();
// 0x0000012E System.Void easyar.SparseSpatialMapController::set_ShowPointCloud(System.Boolean)
extern void SparseSpatialMapController_set_ShowPointCloud_mABD291BC846420D37A52DD0486BE3A27F3CCD8D6 ();
// 0x0000012F System.Boolean easyar.SparseSpatialMapController::get_IsLocalizing()
extern void SparseSpatialMapController_get_IsLocalizing_mBF6029A571A786620A7613711DAAB3D91344A6DC ();
// 0x00000130 System.Void easyar.SparseSpatialMapController::set_IsLocalizing(System.Boolean)
extern void SparseSpatialMapController_set_IsLocalizing_m0DB198A14B594FC863B933A81229A5B8EBD77E31 ();
// 0x00000131 System.Void easyar.SparseSpatialMapController::Awake()
extern void SparseSpatialMapController_Awake_m82B0FE21BC940FD15491ED54953513B4352D3740 ();
// 0x00000132 System.Void easyar.SparseSpatialMapController::Start()
extern void SparseSpatialMapController_Start_m3A6C450F6EBA3C4DA6B1643EA76476FC8933BDBB ();
// 0x00000133 System.Void easyar.SparseSpatialMapController::OnDestroy()
extern void SparseSpatialMapController_OnDestroy_m3F1106BD312AFA294F9521BA52DDE1E28F10DE67 ();
// 0x00000134 System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.SparseSpatialMapController::HitTest(UnityEngine.Vector2)
extern void SparseSpatialMapController_HitTest_m2C742DD9287312D12026EFC863A63657351F6C91 ();
// 0x00000135 System.Void easyar.SparseSpatialMapController::Host(System.String,easyar.Optional`1<easyar.Image>)
extern void SparseSpatialMapController_Host_mF76C9C54EEC1ADA414542D35BF93357A528B3115 ();
// 0x00000136 System.Void easyar.SparseSpatialMapController::OnLocalization(System.Boolean)
extern void SparseSpatialMapController_OnLocalization_m5CE084484F638EFFD039FFB0C3739CCE517589DD ();
// 0x00000137 System.Void easyar.SparseSpatialMapController::UpdatePointCloud(easyar.Buffer)
extern void SparseSpatialMapController_UpdatePointCloud_m3F1ECD7C0D78454BDE90C0FF81568DE57F1F2BF7 ();
// 0x00000138 System.Void easyar.SparseSpatialMapController::UpdatePointCloud()
extern void SparseSpatialMapController_UpdatePointCloud_m105DAF4975FC33947A5CD6D5EE88FA6786FCD0ED ();
// 0x00000139 System.Void easyar.SparseSpatialMapController::LoadMapBuilderInfo()
extern void SparseSpatialMapController_LoadMapBuilderInfo_mE37F02F7BDF6A599A36EC3C99626BF09B0500A48 ();
// 0x0000013A System.Void easyar.SparseSpatialMapController::LoadMapManagerInfo(easyar.SparseSpatialMapController_MapManagerSourceData)
extern void SparseSpatialMapController_LoadMapManagerInfo_mDBAA93FBCFA7501588CD2EC41DF4B45684E3B7A3 ();
// 0x0000013B System.Void easyar.SparseSpatialMapController::UpdateMapInLocalizer()
extern void SparseSpatialMapController_UpdateMapInLocalizer_m67B104D8EA76830203F380B6B2AAB9ABF17E744F ();
// 0x0000013C System.Void easyar.SparseSpatialMapController::.ctor()
extern void SparseSpatialMapController__ctor_m178FF50DCE99809AB36FF58BE78727BD112544B9 ();
// 0x0000013D System.Void easyar.SparseSpatialMapController::<Host>b__55_0(easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String)
extern void SparseSpatialMapController_U3CHostU3Eb__55_0_m02DE4F29D715B2329C34F5E502140D039BE5242B ();
// 0x0000013E UnityEngine.ParticleSystem_Particle easyar.SparseSpatialMapController::<UpdatePointCloud>b__58_0(UnityEngine.Vector3)
extern void SparseSpatialMapController_U3CUpdatePointCloudU3Eb__58_0_mCB80C90FCC41AB437D38AE05F2D01F8E108DA12A ();
// 0x0000013F System.Void easyar.SparseSpatialMapController::<UpdateMapInLocalizer>b__61_0(easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String)
extern void SparseSpatialMapController_U3CUpdateMapInLocalizerU3Eb__61_0_m458B2B3FA6E112D111A22712DE5A1D9E8221AF3D ();
// 0x00000140 easyar.SparseSpatialMap easyar.SparseSpatialMapWorkerFrameFilter::get_Builder()
extern void SparseSpatialMapWorkerFrameFilter_get_Builder_m7882C92E1EDF36FF15D0D75059CFABC020FEE7AF ();
// 0x00000141 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_Builder(easyar.SparseSpatialMap)
extern void SparseSpatialMapWorkerFrameFilter_set_Builder_mEB5A848E8FDE14A62372FA1B7E9CF1E92C8BF3D5 ();
// 0x00000142 easyar.SparseSpatialMap easyar.SparseSpatialMapWorkerFrameFilter::get_Localizer()
extern void SparseSpatialMapWorkerFrameFilter_get_Localizer_mF6DC0CB5B1C68042BAEA8D343AC1260B6D0DFD69 ();
// 0x00000143 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_Localizer(easyar.SparseSpatialMap)
extern void SparseSpatialMapWorkerFrameFilter_set_Localizer_mCA48A36CCEE084E1BA05878FFD0492C7A82F89AF ();
// 0x00000144 easyar.SparseSpatialMapManager easyar.SparseSpatialMapWorkerFrameFilter::get_Manager()
extern void SparseSpatialMapWorkerFrameFilter_get_Manager_m2791BB26159DA12D9A5306631273DF815F34BDE6 ();
// 0x00000145 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_Manager(easyar.SparseSpatialMapManager)
extern void SparseSpatialMapWorkerFrameFilter_set_Manager_m1849DF827DF5DDA87B81BE6292E43E7E975F54C4 ();
// 0x00000146 System.Void easyar.SparseSpatialMapWorkerFrameFilter::add_MapLoad(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_add_MapLoad_mA674610F3637B16BC95999C1E2CB5ED3DAA740DD ();
// 0x00000147 System.Void easyar.SparseSpatialMapWorkerFrameFilter::remove_MapLoad(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_remove_MapLoad_m2E221223F109360693E4D8534B12703C512CA5BC ();
// 0x00000148 System.Void easyar.SparseSpatialMapWorkerFrameFilter::add_MapUnload(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_add_MapUnload_m7BC4890DCA95EC5A5CA831DFE739FEC31B918C54 ();
// 0x00000149 System.Void easyar.SparseSpatialMapWorkerFrameFilter::remove_MapUnload(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_remove_MapUnload_m0E15E3CC8069508EFC99DA24DEC8CC3C3C6E168C ();
// 0x0000014A System.Void easyar.SparseSpatialMapWorkerFrameFilter::add_MapHost(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_add_MapHost_m5753CC2BD74613F9118AE7B46EC5C1853BBB511E ();
// 0x0000014B System.Void easyar.SparseSpatialMapWorkerFrameFilter::remove_MapHost(System.Action`4<easyar.SparseSpatialMapController,easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_remove_MapHost_m1DB18C7A03C672402D6A2FC4FB3C3D3A718E509B ();
// 0x0000014C System.Int32 easyar.SparseSpatialMapWorkerFrameFilter::get_BufferRequirement()
extern void SparseSpatialMapWorkerFrameFilter_get_BufferRequirement_mD7BADFFBA4DF406DF0232E5ED56AB448EDE80FC0 ();
// 0x0000014D easyar.MotionTrackingStatus easyar.SparseSpatialMapWorkerFrameFilter::get_TrackingStatus()
extern void SparseSpatialMapWorkerFrameFilter_get_TrackingStatus_mD32B38904C597C5E697B6ABFEA793FF0F7F960E4 ();
// 0x0000014E System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_TrackingStatus(easyar.MotionTrackingStatus)
extern void SparseSpatialMapWorkerFrameFilter_set_TrackingStatus_m7B7280AD476BB94C611BA171DE2059D0082768ED ();
// 0x0000014F easyar.SparseSpatialMapWorkerFrameFilter_Mode easyar.SparseSpatialMapWorkerFrameFilter::get_WorkingMode()
extern void SparseSpatialMapWorkerFrameFilter_get_WorkingMode_m6E3647B64810E2F7AFE24BDEDAB46A1281DC4343 ();
// 0x00000150 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_WorkingMode(easyar.SparseSpatialMapWorkerFrameFilter_Mode)
extern void SparseSpatialMapWorkerFrameFilter_set_WorkingMode_m6A562E5E5BE6992D5EE3A91203ACBC61BE1914E6 ();
// 0x00000151 easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter::get_LocalizedMap()
extern void SparseSpatialMapWorkerFrameFilter_get_LocalizedMap_m596B56F3BFBE04EAACA921CB2940EFE1DE5053EB ();
// 0x00000152 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_LocalizedMap(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_set_LocalizedMap_mD364927A227ACEE987FB2FBD1313A2E008479D7A ();
// 0x00000153 easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter::get_BuilderMapController()
extern void SparseSpatialMapWorkerFrameFilter_get_BuilderMapController_mE5FE5884DE0E7A9BC6E859A946BB691A619F6865 ();
// 0x00000154 System.Void easyar.SparseSpatialMapWorkerFrameFilter::set_BuilderMapController(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_set_BuilderMapController_m66D49306FCF12AAFCDA06940F0A1A042E549D273 ();
// 0x00000155 System.Collections.Generic.List`1<easyar.SparseSpatialMapController> easyar.SparseSpatialMapWorkerFrameFilter::get_MapControllers()
extern void SparseSpatialMapWorkerFrameFilter_get_MapControllers_m7B53B18890749060102996A30392306A9E9EFEDA ();
// 0x00000156 System.Void easyar.SparseSpatialMapWorkerFrameFilter::Awake()
extern void SparseSpatialMapWorkerFrameFilter_Awake_mA51993D61E79B9A3AE874C561844E4F2BC0A3050 ();
// 0x00000157 System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnEnable()
extern void SparseSpatialMapWorkerFrameFilter_OnEnable_mCDBBB1D0F7393EB1AEC7E1995719595FFB614ED5 ();
// 0x00000158 System.Void easyar.SparseSpatialMapWorkerFrameFilter::Start()
extern void SparseSpatialMapWorkerFrameFilter_Start_mD38CE8643906DAB8FB1D0B00839C3BE54673D961 ();
// 0x00000159 System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnDisable()
extern void SparseSpatialMapWorkerFrameFilter_OnDisable_m4602C5FEA7CF3C640AEA012DCA5D95EA3E0C6ABD ();
// 0x0000015A System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnDestroy()
extern void SparseSpatialMapWorkerFrameFilter_OnDestroy_mCA7C39D29104F91B7A3C5C49EB9AEA9AD10E46DD ();
// 0x0000015B System.Void easyar.SparseSpatialMapWorkerFrameFilter::LoadMap(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_LoadMap_m1A63A86323A73AF470BD4EDA8296859250BA3410 ();
// 0x0000015C System.Void easyar.SparseSpatialMapWorkerFrameFilter::UnloadMap(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_UnloadMap_mBD6B44E78C930C5EFA8642146608DC46B781DA3D ();
// 0x0000015D System.Void easyar.SparseSpatialMapWorkerFrameFilter::HostMap(easyar.SparseSpatialMapController,System.String,easyar.Optional`1<easyar.Image>)
extern void SparseSpatialMapWorkerFrameFilter_HostMap_mD7B9530B4F36AF1F0E7DED551232D76CF860B293 ();
// 0x0000015E easyar.InputFrameSink easyar.SparseSpatialMapWorkerFrameFilter::InputFrameSink()
extern void SparseSpatialMapWorkerFrameFilter_InputFrameSink_mF1480C248BFF711E220ED268BEBFC9B89AB87D98 ();
// 0x0000015F easyar.OutputFrameSource easyar.SparseSpatialMapWorkerFrameFilter::OutputFrameSource()
extern void SparseSpatialMapWorkerFrameFilter_OutputFrameSource_m6FA5A59A644D5FA68B92AC5570A4A2526867131E ();
// 0x00000160 System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnTracking(easyar.MotionTrackingStatus)
extern void SparseSpatialMapWorkerFrameFilter_OnTracking_mF8007A28BB7E551E257974D224FBB6F077B5E136 ();
// 0x00000161 System.Void easyar.SparseSpatialMapWorkerFrameFilter::OnAssemble(easyar.ARSession)
extern void SparseSpatialMapWorkerFrameFilter_OnAssemble_mA0009DCC8941D378ECFD6A379E42EAAA9CDF5848 ();
// 0x00000162 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.SparseSpatialMapWorkerFrameFilter::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
extern void SparseSpatialMapWorkerFrameFilter_OnResult_m8555931A0D4E1DB9D58C43E37EC961BDC7E443C1 ();
// 0x00000163 System.Void easyar.SparseSpatialMapWorkerFrameFilter::LoadSparseSpatialMap(easyar.SparseSpatialMapController,System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_LoadSparseSpatialMap_mC958F60621FC7B7A636108A9F1EB8356C2597BE1 ();
// 0x00000164 System.Void easyar.SparseSpatialMapWorkerFrameFilter::UnloadSparseSpatialMap(easyar.SparseSpatialMapController,System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_UnloadSparseSpatialMap_m59901CD8554C447DDB39E15F0404886EF73E1DBA ();
// 0x00000165 System.Void easyar.SparseSpatialMapWorkerFrameFilter::HostSparseSpatialMap(easyar.SparseSpatialMapController,System.String,easyar.Optional`1<easyar.Image>,System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String>)
extern void SparseSpatialMapWorkerFrameFilter_HostSparseSpatialMap_m91ECE225E834BD20FFCF8F3918CE9AE6E78066BF ();
// 0x00000166 System.Void easyar.SparseSpatialMapWorkerFrameFilter::LoadSparseSpatialMapBuild(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_LoadSparseSpatialMapBuild_m33C9AA05FFBCC36F8B84B1D66E707D2E32CF9042 ();
// 0x00000167 System.Void easyar.SparseSpatialMapWorkerFrameFilter::UnloadSparseSpatialMapBuild(easyar.SparseSpatialMapController)
extern void SparseSpatialMapWorkerFrameFilter_UnloadSparseSpatialMapBuild_mBC1F9F42D38B646CA8570B8BC15D946E2D9C4947 ();
// 0x00000168 easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter::TryGetMapController(System.String)
extern void SparseSpatialMapWorkerFrameFilter_TryGetMapController_m6415E8B0C458806359B33027D330F53F1B3D1692 ();
// 0x00000169 System.Void easyar.SparseSpatialMapWorkerFrameFilter::NotifyEmptyConfig(easyar.SparseSpatialMapWorkerFrameFilter_SpatialMapServiceConfig)
extern void SparseSpatialMapWorkerFrameFilter_NotifyEmptyConfig_mE3987B095ACCC605ED0B14B29CD665EA80EE6586 ();
// 0x0000016A System.Void easyar.SparseSpatialMapWorkerFrameFilter::.ctor()
extern void SparseSpatialMapWorkerFrameFilter__ctor_m971448E3E4FB1B51714D52A209E9EB874565EB8A ();
// 0x0000016B System.Void easyar.SparseSpatialMapWorkerFrameFilter::<OnAssemble>b__62_0(easyar.WorldRootController)
extern void SparseSpatialMapWorkerFrameFilter_U3COnAssembleU3Eb__62_0_m988C7E1EF683C39F39EA4BFCE50EF97C030F5A42 ();
// 0x0000016C easyar.SurfaceTracker easyar.SurfaceTrackerFrameFilter::get_Tracker()
extern void SurfaceTrackerFrameFilter_get_Tracker_m5C6AD3EE6DA43304EC472DEE107E620242AA9D6A ();
// 0x0000016D System.Void easyar.SurfaceTrackerFrameFilter::set_Tracker(easyar.SurfaceTracker)
extern void SurfaceTrackerFrameFilter_set_Tracker_mEE859A6937772F269F814E1FF530F9323C9C7E70 ();
// 0x0000016E System.Int32 easyar.SurfaceTrackerFrameFilter::get_BufferRequirement()
extern void SurfaceTrackerFrameFilter_get_BufferRequirement_mBCC9C015C6E1ECB282EF95F98C843CF323A63284 ();
// 0x0000016F System.Void easyar.SurfaceTrackerFrameFilter::Awake()
extern void SurfaceTrackerFrameFilter_Awake_m23FAAA6FF176DCC80A972D7F74D6BA0B9BFEB501 ();
// 0x00000170 System.Void easyar.SurfaceTrackerFrameFilter::OnEnable()
extern void SurfaceTrackerFrameFilter_OnEnable_m17790321336D42658E43955B91E85ACFE033B53D ();
// 0x00000171 System.Void easyar.SurfaceTrackerFrameFilter::Start()
extern void SurfaceTrackerFrameFilter_Start_m1DC3B1190066DE5DB1383B3076D1AFB78925557E ();
// 0x00000172 System.Void easyar.SurfaceTrackerFrameFilter::OnDisable()
extern void SurfaceTrackerFrameFilter_OnDisable_mCE9B087CD21C8A3B8019026FF1C435BF05D3E0F1 ();
// 0x00000173 System.Void easyar.SurfaceTrackerFrameFilter::OnDestroy()
extern void SurfaceTrackerFrameFilter_OnDestroy_m8EC5387EE3A945FCB9C4FFC493F1473057A58FBD ();
// 0x00000174 easyar.InputFrameSink easyar.SurfaceTrackerFrameFilter::InputFrameSink()
extern void SurfaceTrackerFrameFilter_InputFrameSink_mE1405C3197E5CEF771DFF64E6E669B990008DCF5 ();
// 0x00000175 easyar.OutputFrameSource easyar.SurfaceTrackerFrameFilter::OutputFrameSource()
extern void SurfaceTrackerFrameFilter_OutputFrameSource_m28F36AAFC83E84E0A5D0B68D88F8DDCC3EE9365E ();
// 0x00000176 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.SurfaceTrackerFrameFilter::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
extern void SurfaceTrackerFrameFilter_OnResult_m70A9942092AD483ED8DFFAAFCE55BD9CF33DB3DC ();
// 0x00000177 System.Void easyar.SurfaceTrackerFrameFilter::.ctor()
extern void SurfaceTrackerFrameFilter__ctor_m528F28D17051C1BCAE63883C2910AC3FA67D2178 ();
// 0x00000178 System.Void easyar.TargetController::add_TargetFound(System.Action)
extern void TargetController_add_TargetFound_m4127149D8DD008227A384A840D0921EF33341C05 ();
// 0x00000179 System.Void easyar.TargetController::remove_TargetFound(System.Action)
extern void TargetController_remove_TargetFound_m5894D2C3FA2A579FD72DF773E7887BC34B3BA279 ();
// 0x0000017A System.Void easyar.TargetController::add_TargetLost(System.Action)
extern void TargetController_add_TargetLost_mCE77AC87A123425557F396C245C7FB757101378A ();
// 0x0000017B System.Void easyar.TargetController::remove_TargetLost(System.Action)
extern void TargetController_remove_TargetLost_m04CC34D71BA33CDED4966A1895C2DF987E878803 ();
// 0x0000017C System.Boolean easyar.TargetController::get_IsTracked()
extern void TargetController_get_IsTracked_mE495B549B5BFA48F8BC757626AD8E8394941B42F ();
// 0x0000017D System.Void easyar.TargetController::set_IsTracked(System.Boolean)
extern void TargetController_set_IsTracked_mCD24A01BE81D992CBABF29C8ADB724B8FE7EA39C ();
// 0x0000017E System.Boolean easyar.TargetController::get_IsLoaded()
extern void TargetController_get_IsLoaded_m6025FDCD6D83CC1523A7829670AC091B09255CE0 ();
// 0x0000017F System.Void easyar.TargetController::set_IsLoaded(System.Boolean)
extern void TargetController_set_IsLoaded_mB1D22FD919DE48492DBF97F73BE89C253A5619B5 ();
// 0x00000180 System.Void easyar.TargetController::Start()
extern void TargetController_Start_mC720E5BE377FBE4AC6670DC7243C73CA574C7AAB ();
// 0x00000181 System.Void easyar.TargetController::OnTracking(System.Boolean)
extern void TargetController_OnTracking_m96DB86A87FA5725A566913F2D283D4522374F47C ();
// 0x00000182 System.Void easyar.TargetController::OnTracking()
// 0x00000183 System.Void easyar.TargetController::.ctor()
extern void TargetController__ctor_mA9AD9CC87BA8FFE3D24F285A7685B9C535ADA1E0 ();
// 0x00000184 UnityEngine.Matrix4x4 easyar.APIExtend::ToUnityMatrix(easyar.Matrix44F)
extern void APIExtend_ToUnityMatrix_mBE451AEDF08B177882F754B4FBB49AC9A9EEE494 ();
// 0x00000185 easyar.Vec2F easyar.APIExtend::ToEasyARVector(UnityEngine.Vector2)
extern void APIExtend_ToEasyARVector_m437FDBE6E2D4323B5BA97882E5FD783E30DCC580 ();
// 0x00000186 easyar.Vec3F easyar.APIExtend::ToEasyARVector(UnityEngine.Vector3)
extern void APIExtend_ToEasyARVector_m0E65CBACBE7E7625A4920594440430EB04B10DF9 ();
// 0x00000187 UnityEngine.Vector2 easyar.APIExtend::ToUnityVector(easyar.Vec2F)
extern void APIExtend_ToUnityVector_m43F1C17DDE433DA99FDDA072CE293B122166B447 ();
// 0x00000188 UnityEngine.Vector3 easyar.APIExtend::ToUnityVector(easyar.Vec3F)
extern void APIExtend_ToUnityVector_m66DA802A68B830466952E99D67ECB6FDF194663A ();
// 0x00000189 System.Void easyar.Display::.ctor()
extern void Display__ctor_m1AD625F36564FF1E706AFA4997F26F32FCEA9F5C ();
// 0x0000018A System.Void easyar.Display::Finalize()
extern void Display_Finalize_m357A6E5785E241E90831E3BBD269EED1AB5AFCD0 ();
// 0x0000018B System.Int32 easyar.Display::get_Rotation()
extern void Display_get_Rotation_mA63DEB1DB98673A39EA31029CC3B06291A5851C3 ();
// 0x0000018C System.Void easyar.Display::Dispose()
extern void Display_Dispose_m446158E80AF25C4EFC5D2FCC9630C256AE5200ED ();
// 0x0000018D UnityEngine.Matrix4x4 easyar.Display::GetCompensation(easyar.CameraParameters)
extern void Display_GetCompensation_m637617AEF976AB6E50BA52228E8437220F715D20 ();
// 0x0000018E UnityEngine.Vector2 easyar.Display::ImageCoordinatesFromScreenCoordinates(UnityEngine.Vector2,easyar.CameraParameters,UnityEngine.Camera)
extern void Display_ImageCoordinatesFromScreenCoordinates_mD3266FD101F378876A1B6FCD31EF842EE24FEDC9 ();
// 0x0000018F System.Void easyar.Display::InitializeIOS()
extern void Display_InitializeIOS_m7C2AA074D4BB3CB36CE6775138FBF3D41C24575E ();
// 0x00000190 System.Void easyar.Display::InitializeAndroid()
extern void Display_InitializeAndroid_mA8B98E5CA5A4B7F7ED890C61C53C669E964554BA ();
// 0x00000191 System.Void easyar.Display::DeleteAndroidJavaObjects()
extern void Display_DeleteAndroidJavaObjects_m2FEE84D83DD6DBD481AE23B835C81D3683942461 ();
// 0x00000192 System.Collections.IEnumerator easyar.FileUtil::LoadFile(System.String,easyar.PathType,System.Action`1<easyar.Buffer>)
extern void FileUtil_LoadFile_mD43F5F1702DF1DF55D4F2AFB97BA21ABA0190A06 ();
// 0x00000193 System.Collections.IEnumerator easyar.FileUtil::LoadFile(System.String,easyar.PathType,System.Action`1<System.Byte[]>)
extern void FileUtil_LoadFile_mDB0E599D81E9E8D414719BCF8111F12FF5D457C8 ();
// 0x00000194 System.String easyar.FileUtil::PathToUrl(System.String)
extern void FileUtil_PathToUrl_m6E5A9B8E1BD46EF75DCCA78D6B9359EC30CA0C8D ();
// 0x00000195 System.Void easyar.GUIPopup::Start()
extern void GUIPopup_Start_mA2F6B2AAD6D808B1BE7573E7A3B3E4E8E194B84A ();
// 0x00000196 System.Void easyar.GUIPopup::OnDestroy()
extern void GUIPopup_OnDestroy_mCB4785FD7B3C8DCCCE69BB614B5D60699C5048CC ();
// 0x00000197 System.Void easyar.GUIPopup::EnqueueMessage(System.String,System.Single)
extern void GUIPopup_EnqueueMessage_mDFEED6E172639F1823D04AF7FCD63A516DE8716A ();
// 0x00000198 System.Collections.IEnumerator easyar.GUIPopup::ShowMessage()
extern void GUIPopup_ShowMessage_mCFEBD07B2E63FEC66DEED5D544A50A898275305E ();
// 0x00000199 System.Void easyar.GUIPopup::OnGUI()
extern void GUIPopup_OnGUI_m4319587E47ACA5E7919BE8AA5D42186F618D6906 ();
// 0x0000019A System.Void easyar.GUIPopup::.ctor()
extern void GUIPopup__ctor_mA9F14511F21710EBC025312292B61F3E08BA58F6 ();
// 0x0000019B System.Void easyar.UIPopupException::.ctor(System.String,System.Single)
extern void UIPopupException__ctor_m1A92438E8ACA8383DE1CC8CB7453F5752FEE57FC ();
// 0x0000019C System.Void easyar.UIPopupException::.ctor(System.String)
extern void UIPopupException__ctor_m057582E8AF359A569B766600F5739300B215FBE4 ();
// 0x0000019D System.Void easyar.ThreadWorker::Finalize()
extern void ThreadWorker_Finalize_m1C459E226F863CD51CDBE78D8C1292F0A4531FE5 ();
// 0x0000019E System.Void easyar.ThreadWorker::Dispose()
extern void ThreadWorker_Dispose_mF7C5C56EF2141ED9D2BE9C18426ED0B88CF564A8 ();
// 0x0000019F System.Void easyar.ThreadWorker::Run(System.Action)
extern void ThreadWorker_Run_m19DD9ECC471B4210998FADB083A2FD1A043EA337 ();
// 0x000001A0 System.Void easyar.ThreadWorker::CreateThread()
extern void ThreadWorker_CreateThread_m02D76BBB7DF58609DDC6EE4A8AC3DF0A81D9BB09 ();
// 0x000001A1 System.Void easyar.ThreadWorker::Finish()
extern void ThreadWorker_Finish_m6AF3181E13AFC0545E80208E17A77BC3144B091D ();
// 0x000001A2 System.Void easyar.ThreadWorker::.ctor()
extern void ThreadWorker__ctor_m6F7838EB5060A15312AE876039A6FDF586376D11 ();
// 0x000001A3 System.Void easyar.ThreadWorker::<CreateThread>b__6_0()
extern void ThreadWorker_U3CCreateThreadU3Eb__6_0_m0E1686EE64CED61616CD287A32B75E6601B0C161 ();
// 0x000001A4 System.Void easyar.TransformUtil::SetCameraPoseOnCamera(UnityEngine.Transform,easyar.WorldRootController,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetCameraPoseOnCamera_m13D887D57FC7C4B9237ED8C2D1D35F79EDAE3CE1 ();
// 0x000001A5 System.Void easyar.TransformUtil::SetCameraPoseOnWorldRoot(UnityEngine.Transform,easyar.WorldRootController,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetCameraPoseOnWorldRoot_m6FA66D2711D814618B6CF2B2B08D927569AE8909 ();
// 0x000001A6 System.Void easyar.TransformUtil::SetTargetPoseOnCamera(UnityEngine.Transform,easyar.TargetController,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetTargetPoseOnCamera_m604DDC9ACDD7E7A423FBB7A19BB16EDFF9DE1B19 ();
// 0x000001A7 System.Void easyar.TransformUtil::SetTargetPoseOnTarget(UnityEngine.Transform,easyar.TargetController,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetTargetPoseOnTarget_mDC8C81046E396F129CAFDF52594A3415FF8DA9BA ();
// 0x000001A8 System.Void easyar.TransformUtil::SetMatrixOnTransform(UnityEngine.Transform,UnityEngine.Matrix4x4,System.Boolean)
extern void TransformUtil_SetMatrixOnTransform_mF5B2194C1006AE89353FE63EA64301AD32229DC4 ();
// 0x000001A9 System.Void easyar.TransformUtil::SetPoseOnTransform(UnityEngine.Transform,UnityEngine.Transform,easyar.Matrix44F,UnityEngine.Matrix4x4,System.Boolean,System.Boolean,System.Boolean)
extern void TransformUtil_SetPoseOnTransform_m81BF07415D9FAFD5CAEECF6E62ABF40FE2F4B2D2 ();
// 0x000001AA easyar.VIOCameraDeviceUnion_DeviceUnion easyar.VIOCameraDeviceUnion::get_Device()
extern void VIOCameraDeviceUnion_get_Device_m2C42E176D57109D451A3A1BF46BD6202B3654F29 ();
// 0x000001AB System.Void easyar.VIOCameraDeviceUnion::set_Device(easyar.VIOCameraDeviceUnion_DeviceUnion)
extern void VIOCameraDeviceUnion_set_Device_m2A6DEA1412FBB35F64F880C78667BB05DD62D0EA ();
// 0x000001AC System.Void easyar.VIOCameraDeviceUnion::add_DeviceCreated(System.Action)
extern void VIOCameraDeviceUnion_add_DeviceCreated_m34F4B42C2CFBBE30760549CC2A4BC56703FF6DCF ();
// 0x000001AD System.Void easyar.VIOCameraDeviceUnion::remove_DeviceCreated(System.Action)
extern void VIOCameraDeviceUnion_remove_DeviceCreated_m7DF3F58F220B87DB94648F7F740B0364B98230EE ();
// 0x000001AE System.Void easyar.VIOCameraDeviceUnion::add_DeviceOpened(System.Action)
extern void VIOCameraDeviceUnion_add_DeviceOpened_m0BCA0E8F56E2E44323EB64F96D8DC84DCFAA55D4 ();
// 0x000001AF System.Void easyar.VIOCameraDeviceUnion::remove_DeviceOpened(System.Action)
extern void VIOCameraDeviceUnion_remove_DeviceOpened_mEB5C17C89997E2F17FAD59B86AC0627859454672 ();
// 0x000001B0 System.Void easyar.VIOCameraDeviceUnion::add_DeviceClosed(System.Action)
extern void VIOCameraDeviceUnion_add_DeviceClosed_m74113D4216A7B3C5D238B792A56DCAB042103789 ();
// 0x000001B1 System.Void easyar.VIOCameraDeviceUnion::remove_DeviceClosed(System.Action)
extern void VIOCameraDeviceUnion_remove_DeviceClosed_m126CB7D70435537BF80377282724B3F454A6ECA4 ();
// 0x000001B2 System.Int32 easyar.VIOCameraDeviceUnion::get_BufferCapacity()
extern void VIOCameraDeviceUnion_get_BufferCapacity_m36FD16961B9A6FA002E6B77D60780E7DA75EE041 ();
// 0x000001B3 System.Void easyar.VIOCameraDeviceUnion::set_BufferCapacity(System.Int32)
extern void VIOCameraDeviceUnion_set_BufferCapacity_mCC85160780B4C41C064A1523740E77950EF83144 ();
// 0x000001B4 System.Boolean easyar.VIOCameraDeviceUnion::get_HasSpatialInformation()
extern void VIOCameraDeviceUnion_get_HasSpatialInformation_mCDBD63A7EDF527DE2C8727597CD2F9F59911AC58 ();
// 0x000001B5 System.Void easyar.VIOCameraDeviceUnion::OnEnable()
extern void VIOCameraDeviceUnion_OnEnable_mFDFEC774A6852848F52FCEF3E9B344390F27ABE9 ();
// 0x000001B6 System.Void easyar.VIOCameraDeviceUnion::Start()
extern void VIOCameraDeviceUnion_Start_m52D54581297DA27729AFE9AA9E7AB975420CD830 ();
// 0x000001B7 System.Void easyar.VIOCameraDeviceUnion::OnDisable()
extern void VIOCameraDeviceUnion_OnDisable_m6B139E536F34F7AF602512C1E5373EE974C52D73 ();
// 0x000001B8 System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.VIOCameraDeviceUnion::HitTestAgainstHorizontalPlane(UnityEngine.Vector2)
extern void VIOCameraDeviceUnion_HitTestAgainstHorizontalPlane_m232ACE05AD8CA0D1FE332EB5CC2F4D1D38AB9533 ();
// 0x000001B9 System.Collections.Generic.List`1<UnityEngine.Vector3> easyar.VIOCameraDeviceUnion::HitTestAgainstPointCloud(UnityEngine.Vector2)
extern void VIOCameraDeviceUnion_HitTestAgainstPointCloud_mCAB2C0E6C66A1B998D4F4E2258E26AC701677B5A ();
// 0x000001BA System.Void easyar.VIOCameraDeviceUnion::Open()
extern void VIOCameraDeviceUnion_Open_m588140E99F05C3F0A0661781D1AA8E314626874D ();
// 0x000001BB System.Void easyar.VIOCameraDeviceUnion::Close()
extern void VIOCameraDeviceUnion_Close_mD7F1F6A8F5F1AC50721A97615CF7DF9F0F77FA70 ();
// 0x000001BC System.Void easyar.VIOCameraDeviceUnion::Connect(easyar.InputFrameSink)
extern void VIOCameraDeviceUnion_Connect_m8B5FC218B425F462A7E28DB79E0E408664F737DA ();
// 0x000001BD System.Void easyar.VIOCameraDeviceUnion::CreateMotionTrackerCameraDevice()
extern void VIOCameraDeviceUnion_CreateMotionTrackerCameraDevice_mEC4E9D967ECD453576405A396BAC86A184D3EE2D ();
// 0x000001BE System.Void easyar.VIOCameraDeviceUnion::CreateARKitCameraDevice()
extern void VIOCameraDeviceUnion_CreateARKitCameraDevice_m696DD97C8C702CC7FAD0F478C552E105520F5383 ();
// 0x000001BF System.Void easyar.VIOCameraDeviceUnion::CreateARCoreCameraDevice()
extern void VIOCameraDeviceUnion_CreateARCoreCameraDevice_mC5FE3DFD3E2AAFBDFE059618485A2F5F6A231368 ();
// 0x000001C0 System.Boolean easyar.VIOCameraDeviceUnion::CheckARCore()
extern void VIOCameraDeviceUnion_CheckARCore_mE7513936E45F9E1F6DDFFF9DD7510CE956239337 ();
// 0x000001C1 System.Void easyar.VIOCameraDeviceUnion::.ctor()
extern void VIOCameraDeviceUnion__ctor_m78375472B7FE83443423F57400FBF8DC45B7C116 ();
// 0x000001C2 System.Void easyar.VIOCameraDeviceUnion::<Open>b__32_0(easyar.PermissionStatus,System.String)
extern void VIOCameraDeviceUnion_U3COpenU3Eb__32_0_mD7EC69EB3F56C477C87CE7609BD34FBACED2F592 ();
// 0x000001C3 easyar.CameraDevice easyar.VideoCameraDevice::get_Device()
extern void VideoCameraDevice_get_Device_m799A6B482E698A6B53E4477AE38A444935635E28 ();
// 0x000001C4 System.Void easyar.VideoCameraDevice::set_Device(easyar.CameraDevice)
extern void VideoCameraDevice_set_Device_mAE44E8144AFA4B216916404140C65E9C3FF4009D ();
// 0x000001C5 System.Void easyar.VideoCameraDevice::add_DeviceCreated(System.Action)
extern void VideoCameraDevice_add_DeviceCreated_mC9711D3B2DA2B725BE576887245A61AA50F2F2C1 ();
// 0x000001C6 System.Void easyar.VideoCameraDevice::remove_DeviceCreated(System.Action)
extern void VideoCameraDevice_remove_DeviceCreated_mF6F35403C4BAC81D55E444717B709DFBD1C96B87 ();
// 0x000001C7 System.Void easyar.VideoCameraDevice::add_DeviceOpened(System.Action)
extern void VideoCameraDevice_add_DeviceOpened_m3E6E1021C62A3BBC2FE432E220435075A458E9E4 ();
// 0x000001C8 System.Void easyar.VideoCameraDevice::remove_DeviceOpened(System.Action)
extern void VideoCameraDevice_remove_DeviceOpened_mF382A975D6600BAB983E7DF6032008F5CD9F6336 ();
// 0x000001C9 System.Void easyar.VideoCameraDevice::add_DeviceClosed(System.Action)
extern void VideoCameraDevice_add_DeviceClosed_mB33DCB0C932FA014853B2EFF0857C25A068070F6 ();
// 0x000001CA System.Void easyar.VideoCameraDevice::remove_DeviceClosed(System.Action)
extern void VideoCameraDevice_remove_DeviceClosed_mCC212AF7BFBDBC200371B0DF80B526EB010E9FA1 ();
// 0x000001CB System.Int32 easyar.VideoCameraDevice::get_BufferCapacity()
extern void VideoCameraDevice_get_BufferCapacity_m23F8FF409FC7FB23BDC02DEB876B14886CBB5498 ();
// 0x000001CC System.Void easyar.VideoCameraDevice::set_BufferCapacity(System.Int32)
extern void VideoCameraDevice_set_BufferCapacity_m6F1B5D28959F012B470EE8B2A762602EB8FD944E ();
// 0x000001CD System.Boolean easyar.VideoCameraDevice::get_HasSpatialInformation()
extern void VideoCameraDevice_get_HasSpatialInformation_m0501D3A65592ED0A3CE747FBCFE9D2DCFAEE3781 ();
// 0x000001CE easyar.CameraDevicePreference easyar.VideoCameraDevice::get_CameraPreference()
extern void VideoCameraDevice_get_CameraPreference_m5F776D763A3F84E9D0CDF08945DA89C058D708EA ();
// 0x000001CF System.Void easyar.VideoCameraDevice::set_CameraPreference(easyar.CameraDevicePreference)
extern void VideoCameraDevice_set_CameraPreference_m16690F35E68C4D606835C2032C890E5099893778 ();
// 0x000001D0 easyar.CameraParameters easyar.VideoCameraDevice::get_Parameters()
extern void VideoCameraDevice_get_Parameters_m87F8FD49044B04ABAA51B8617FD32A40901ED919 ();
// 0x000001D1 System.Void easyar.VideoCameraDevice::set_Parameters(easyar.CameraParameters)
extern void VideoCameraDevice_set_Parameters_m76ED0A7906776AD8F959CDB4D73D5F7A8145A6EA ();
// 0x000001D2 System.Void easyar.VideoCameraDevice::OnEnable()
extern void VideoCameraDevice_OnEnable_mDB513EE63130DC6EF23D2236A5DB2E6C25762A82 ();
// 0x000001D3 System.Void easyar.VideoCameraDevice::Start()
extern void VideoCameraDevice_Start_m8525AE3EC822D1DA4187A07730666DECEB18D4F2 ();
// 0x000001D4 System.Void easyar.VideoCameraDevice::OnDisable()
extern void VideoCameraDevice_OnDisable_mD57631315AC550F928C1E53573A3F24C96E99B32 ();
// 0x000001D5 System.Void easyar.VideoCameraDevice::Open()
extern void VideoCameraDevice_Open_mEECAB310D7A2B1D07C6386F6A0201FBD118D16FA ();
// 0x000001D6 System.Void easyar.VideoCameraDevice::Close()
extern void VideoCameraDevice_Close_mFA09C7DE6F07D90834433B977516B4D5DE902123 ();
// 0x000001D7 System.Void easyar.VideoCameraDevice::Connect(easyar.InputFrameSink)
extern void VideoCameraDevice_Connect_m9E3C8BA850F9955C9D25FF36FAEBA2254D2DAF5F ();
// 0x000001D8 System.Void easyar.VideoCameraDevice::.ctor()
extern void VideoCameraDevice__ctor_m3DE7A4D58A5B71A0524CCDD6C8669E99B073451C ();
// 0x000001D9 System.Void easyar.VideoCameraDevice::<Open>b__36_0(easyar.PermissionStatus,System.String)
extern void VideoCameraDevice_U3COpenU3Eb__36_0_m7021222C13A6A2F4D3BA8B99BCBAEA75B8E63146 ();
// 0x000001DA System.Void easyar.VideoRecorder::add_StatusUpdate(System.Action`2<easyar.RecordStatus,System.String>)
extern void VideoRecorder_add_StatusUpdate_m6352DE5EAAC3B6751422A911A53A4FD927D70B1E ();
// 0x000001DB System.Void easyar.VideoRecorder::remove_StatusUpdate(System.Action`2<easyar.RecordStatus,System.String>)
extern void VideoRecorder_remove_StatusUpdate_m4B43F05FAA6A79653237314BB4B5DBF14986B116 ();
// 0x000001DC System.Boolean easyar.VideoRecorder::get_IsReady()
extern void VideoRecorder_get_IsReady_m3CE280A2BCDE4D72D4F838D46B2D9082B5743372 ();
// 0x000001DD System.Void easyar.VideoRecorder::set_IsReady(System.Boolean)
extern void VideoRecorder_set_IsReady_mD673ADC59B138F9C4243E63A155F49351A63480B ();
// 0x000001DE System.Void easyar.VideoRecorder::Start()
extern void VideoRecorder_Start_m212AD34167D5914A4BCDB34618EDF1C419C573A3 ();
// 0x000001DF System.Void easyar.VideoRecorder::OnDestroy()
extern void VideoRecorder_OnDestroy_mD39C1C27A7BD34843FC9EF26DF36C868B04B58DA ();
// 0x000001E0 System.Boolean easyar.VideoRecorder::StartRecording()
extern void VideoRecorder_StartRecording_m18BFEA6DBFCED419FABDB0CB82018DD23188E767 ();
// 0x000001E1 System.Boolean easyar.VideoRecorder::StartRecording(easyar.RecorderConfiguration)
extern void VideoRecorder_StartRecording_mC4932CABA354103140AD3313D8E9E32EBE8D25BA ();
// 0x000001E2 System.Boolean easyar.VideoRecorder::StopRecording()
extern void VideoRecorder_StopRecording_m9AF24802471D88A206229D1276DEB25365424729 ();
// 0x000001E3 System.Boolean easyar.VideoRecorder::RecordFrame(UnityEngine.RenderTexture)
extern void VideoRecorder_RecordFrame_m92C5FABB0FE16AB0E80B105437F91D1B96565818 ();
// 0x000001E4 System.Void easyar.VideoRecorder::.ctor()
extern void VideoRecorder__ctor_m90399E134DEBB0AB333556160FB608B31B1583DA ();
// 0x000001E5 System.Void easyar.VideoRecorder::<Start>b__15_0(easyar.PermissionStatus,System.String)
extern void VideoRecorder_U3CStartU3Eb__15_0_m543C30B8D166A76D34B2087705BC5E87596CC960 ();
// 0x000001E6 System.Void easyar.VideoRecorder::<StartRecording>b__18_0(easyar.RecordStatus,System.String)
extern void VideoRecorder_U3CStartRecordingU3Eb__18_0_m4A273ADCB5E5FD4AE84E11A73DBC20294F89E81D ();
// 0x000001E7 System.Void easyar.WorldRootController::add_TrackingStatusChanged(System.Action`1<easyar.MotionTrackingStatus>)
extern void WorldRootController_add_TrackingStatusChanged_m834D8D5BE14FF9E80FDA444984F216E99D6B384F ();
// 0x000001E8 System.Void easyar.WorldRootController::remove_TrackingStatusChanged(System.Action`1<easyar.MotionTrackingStatus>)
extern void WorldRootController_remove_TrackingStatusChanged_m785F5A355D594B741AD2BC4DF9BA9FB62CEF4B81 ();
// 0x000001E9 easyar.MotionTrackingStatus easyar.WorldRootController::get_TrackingStatus()
extern void WorldRootController_get_TrackingStatus_mF4BED702EF437F788F9A051F1E2B7F823BBFF7DE ();
// 0x000001EA System.Void easyar.WorldRootController::set_TrackingStatus(easyar.MotionTrackingStatus)
extern void WorldRootController_set_TrackingStatus_mEB4297697511E6785D0D2162395B38EA8DBF135F ();
// 0x000001EB System.Void easyar.WorldRootController::Start()
extern void WorldRootController_Start_m81BC514715FB4BC5602B70D45F23E490E2219A6C ();
// 0x000001EC System.Void easyar.WorldRootController::OnTracking(easyar.MotionTrackingStatus)
extern void WorldRootController_OnTracking_mDC2D19C2A8F68EC11E517A7BF337478EE4D8DD6D ();
// 0x000001ED System.Void easyar.WorldRootController::.ctor()
extern void WorldRootController__ctor_mA1B08A3360BC9C6C30842CEB196FB209BCA18545 ();
// 0x000001EE System.Void easyar.Detail::easyar_String_from_utf8(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_String_from_utf8_mEC63092B093C81C2AA6A80386483692767D64245 ();
// 0x000001EF System.Void easyar.Detail::easyar_String_from_utf8_begin(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_String_from_utf8_begin_mE06810143D59DF76FBBD28720B81A81F8BAC2484 ();
// 0x000001F0 System.IntPtr easyar.Detail::easyar_String_begin(System.IntPtr)
extern void Detail_easyar_String_begin_m39EEFC5D16AA6E2D997EE721CEBD177006153654 ();
// 0x000001F1 System.IntPtr easyar.Detail::easyar_String_end(System.IntPtr)
extern void Detail_easyar_String_end_m31F3C053C7710D0D8BFFD3E07F7B9A3C8F5AC713 ();
// 0x000001F2 System.Void easyar.Detail::easyar_String_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_String_copy_mE037CE974098567C01C30D2095B49744AB6756E6 ();
// 0x000001F3 System.Void easyar.Detail::easyar_String__dtor(System.IntPtr)
extern void Detail_easyar_String__dtor_m08D4EB43B7A27A2330B8C56132C2F186CD14D802 ();
// 0x000001F4 System.Void easyar.Detail::easyar_ObjectTargetParameters__ctor(System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters__ctor_mB9E53E6603CE5C903B2EBBEDD32DFC7914260F53 ();
// 0x000001F5 System.Void easyar.Detail::easyar_ObjectTargetParameters_bufferDictionary(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_bufferDictionary_m7E3A01AC176652FF2636B053591BDFBF917A1948 ();
// 0x000001F6 System.Void easyar.Detail::easyar_ObjectTargetParameters_setBufferDictionary(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setBufferDictionary_m8FBB22DEC13DFBE457D472C5A30A2CCDDEE95FC6 ();
// 0x000001F7 System.Void easyar.Detail::easyar_ObjectTargetParameters_objPath(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_objPath_m7F64B2432450988DE08130B620DE80588AA35D57 ();
// 0x000001F8 System.Void easyar.Detail::easyar_ObjectTargetParameters_setObjPath(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setObjPath_m304FFEA6EB5BC12AC7A65955CAB172231F7F8803 ();
// 0x000001F9 System.Void easyar.Detail::easyar_ObjectTargetParameters_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_name_mA05DACB9228033F247651EAA033675B046411DAB ();
// 0x000001FA System.Void easyar.Detail::easyar_ObjectTargetParameters_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setName_m68E26990D77DD643EE34DD62816FFCE71EBBD2F4 ();
// 0x000001FB System.Void easyar.Detail::easyar_ObjectTargetParameters_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_uid_m8985E92307412E49B39AF96934D1C4124B36AF55 ();
// 0x000001FC System.Void easyar.Detail::easyar_ObjectTargetParameters_setUid(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setUid_mD59AD3FB2A94B08E7A83FDBA13BBF3067505F678 ();
// 0x000001FD System.Void easyar.Detail::easyar_ObjectTargetParameters_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters_meta_m92B72E57F2906696F1117FDB58E58E3C332BFFFB ();
// 0x000001FE System.Void easyar.Detail::easyar_ObjectTargetParameters_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_setMeta_mAB9102B1B9AFD8AAE2C3CD6F162E67EF009E9501 ();
// 0x000001FF System.Single easyar.Detail::easyar_ObjectTargetParameters_scale(System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters_scale_m6ECAFBFF295DB747E3FDBAE69870D93D0A4EA9B8 ();
// 0x00000200 System.Void easyar.Detail::easyar_ObjectTargetParameters_setScale(System.IntPtr,System.Single)
extern void Detail_easyar_ObjectTargetParameters_setScale_m753395B4F74F4078838FD98008BEA61758D1C23D ();
// 0x00000201 System.Void easyar.Detail::easyar_ObjectTargetParameters__dtor(System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters__dtor_mFFF9183DCFE7E0BFF8E64A92F0029D028AFD6F25 ();
// 0x00000202 System.Void easyar.Detail::easyar_ObjectTargetParameters__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTargetParameters__retain_m182F13A290FF4BC2A61CE46C256A212A432FF600 ();
// 0x00000203 System.IntPtr easyar.Detail::easyar_ObjectTargetParameters__typeName(System.IntPtr)
extern void Detail_easyar_ObjectTargetParameters__typeName_mC6B2A23563EB234F251CCCD290BE40A81B4868D7 ();
// 0x00000204 System.Void easyar.Detail::easyar_ObjectTarget__ctor(System.IntPtr&)
extern void Detail_easyar_ObjectTarget__ctor_mAE2141F8CC6AE55601345AEC63CF75493D93A200 ();
// 0x00000205 System.Void easyar.Detail::easyar_ObjectTarget_createFromParameters(System.IntPtr,easyar.Detail_OptionalOfObjectTarget&)
extern void Detail_easyar_ObjectTarget_createFromParameters_m08F9A8F03C88F038F39D898DB9D4344FB0D3B426 ();
// 0x00000206 System.Void easyar.Detail::easyar_ObjectTarget_createFromObjectFile(System.IntPtr,easyar.StorageType,System.IntPtr,System.IntPtr,System.IntPtr,System.Single,easyar.Detail_OptionalOfObjectTarget&)
extern void Detail_easyar_ObjectTarget_createFromObjectFile_mA3B691058CBD946FB8B6EA3F05DFE8DFAE558B58 ();
// 0x00000207 System.Single easyar.Detail::easyar_ObjectTarget_scale(System.IntPtr)
extern void Detail_easyar_ObjectTarget_scale_mBD53D84323EA11D7EFEB2A171EE86C0AB5ED4420 ();
// 0x00000208 System.Void easyar.Detail::easyar_ObjectTarget_boundingBox(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget_boundingBox_mBD0828D1988D3BC47287A629096D449AE8349731 ();
// 0x00000209 System.Boolean easyar.Detail::easyar_ObjectTarget_setScale(System.IntPtr,System.Single)
extern void Detail_easyar_ObjectTarget_setScale_mD15FF74A935C0D5788D92F417E16E84E9B292466 ();
// 0x0000020A System.Int32 easyar.Detail::easyar_ObjectTarget_runtimeID(System.IntPtr)
extern void Detail_easyar_ObjectTarget_runtimeID_mA399107D7C1D625CB41FCE6DEB7F056F3CED9B0F ();
// 0x0000020B System.Void easyar.Detail::easyar_ObjectTarget_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget_uid_mDEBB9265658BD3A0EA64168F74BF6D8E6824DC0B ();
// 0x0000020C System.Void easyar.Detail::easyar_ObjectTarget_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget_name_mBAF4B3CE888378432BFDE1BCCA419BD7154FBFF0 ();
// 0x0000020D System.Void easyar.Detail::easyar_ObjectTarget_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTarget_setName_m8F55AF7DB366BD7ED2633E0E55473AE3863F494B ();
// 0x0000020E System.Void easyar.Detail::easyar_ObjectTarget_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget_meta_m16E238F12C9EE6955B8FB82F060130F1B846A21B ();
// 0x0000020F System.Void easyar.Detail::easyar_ObjectTarget_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTarget_setMeta_m325AAA73EB2CDDB730AB9D4F8C5B7C08E1AA7783 ();
// 0x00000210 System.Void easyar.Detail::easyar_ObjectTarget__dtor(System.IntPtr)
extern void Detail_easyar_ObjectTarget__dtor_mEBCBC1A4CCA0DE96E4BA1CD24AC27229A7E8BD0F ();
// 0x00000211 System.Void easyar.Detail::easyar_ObjectTarget__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTarget__retain_m0E1D5E118F70A42CD1FEABE941C41D06179A98CC ();
// 0x00000212 System.IntPtr easyar.Detail::easyar_ObjectTarget__typeName(System.IntPtr)
extern void Detail_easyar_ObjectTarget__typeName_mC5D0497CBAE539975C0CD63A72BB79818F76121B ();
// 0x00000213 System.Void easyar.Detail::easyar_castObjectTargetToTarget(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castObjectTargetToTarget_m54039F31B8182449F8A12264B39B6D0BEECEDEE1 ();
// 0x00000214 System.Void easyar.Detail::easyar_tryCastTargetToObjectTarget(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastTargetToObjectTarget_m5F21B243433711899FCFBE34447C5911710EFE43 ();
// 0x00000215 System.Void easyar.Detail::easyar_ObjectTrackerResult_targetInstances(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTrackerResult_targetInstances_m4782F113CF5B6310CD51660614B4C7B00641B521 ();
// 0x00000216 System.Void easyar.Detail::easyar_ObjectTrackerResult_setTargetInstances(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ObjectTrackerResult_setTargetInstances_mB72CB55B5D216A952EAB91C4F9C0D44683DA7B30 ();
// 0x00000217 System.Void easyar.Detail::easyar_ObjectTrackerResult__dtor(System.IntPtr)
extern void Detail_easyar_ObjectTrackerResult__dtor_m68EA011AD90491E1DB2FEE951DA8ECE302A51234 ();
// 0x00000218 System.Void easyar.Detail::easyar_ObjectTrackerResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTrackerResult__retain_m7F765ADE3EFDD2CD6AD2BF5AFBF88BF4099A7EBE ();
// 0x00000219 System.IntPtr easyar.Detail::easyar_ObjectTrackerResult__typeName(System.IntPtr)
extern void Detail_easyar_ObjectTrackerResult__typeName_m69D987D4CECEF33A6D7D3B5145240D767E571F27 ();
// 0x0000021A System.Void easyar.Detail::easyar_castObjectTrackerResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castObjectTrackerResultToFrameFilterResult_mF4E5A4912E8474AB5C146AFD320960518433A3F8 ();
// 0x0000021B System.Void easyar.Detail::easyar_tryCastFrameFilterResultToObjectTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToObjectTrackerResult_m8B8CF2B94DD0B402C462CE44BA9C72BC78382BB5 ();
// 0x0000021C System.Void easyar.Detail::easyar_castObjectTrackerResultToTargetTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castObjectTrackerResultToTargetTrackerResult_m963F7B87CE0D6CCF49886BB53EE76CA38D290408 ();
// 0x0000021D System.Void easyar.Detail::easyar_tryCastTargetTrackerResultToObjectTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastTargetTrackerResultToObjectTrackerResult_mFC426A1F31556A17D97722C075C6E3A42189BA63 ();
// 0x0000021E System.Boolean easyar.Detail::easyar_ObjectTracker_isAvailable()
extern void Detail_easyar_ObjectTracker_isAvailable_m67C847E3784D7532A35C1536D12BB81372F8F4AA ();
// 0x0000021F System.Void easyar.Detail::easyar_ObjectTracker_feedbackFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTracker_feedbackFrameSink_m647FA27212809ACF929CD95EFA6EA828DB7EED03 ();
// 0x00000220 System.Int32 easyar.Detail::easyar_ObjectTracker_bufferRequirement(System.IntPtr)
extern void Detail_easyar_ObjectTracker_bufferRequirement_mFC3BF01AD75D21B6869885E54E8D4EDFACFC45A1 ();
// 0x00000221 System.Void easyar.Detail::easyar_ObjectTracker_outputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTracker_outputFrameSource_m0C68A00F5545D47D3E67667E9633A0654B525707 ();
// 0x00000222 System.Void easyar.Detail::easyar_ObjectTracker_create(System.IntPtr&)
extern void Detail_easyar_ObjectTracker_create_m5F0B0D1E7238E742743BF34ECA1B349158BFB427 ();
// 0x00000223 System.Boolean easyar.Detail::easyar_ObjectTracker_start(System.IntPtr)
extern void Detail_easyar_ObjectTracker_start_m4D999550A948765607B4A16EE93C86BD44EC702C ();
// 0x00000224 System.Void easyar.Detail::easyar_ObjectTracker_stop(System.IntPtr)
extern void Detail_easyar_ObjectTracker_stop_mB13C5FE83A8796170C0A9612F49B217C67C2B383 ();
// 0x00000225 System.Void easyar.Detail::easyar_ObjectTracker_close(System.IntPtr)
extern void Detail_easyar_ObjectTracker_close_mD0C7449847E8A9470BF8D2132ED24797738FD4E3 ();
// 0x00000226 System.Void easyar.Detail::easyar_ObjectTracker_loadTarget(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail_FunctorOfVoidFromTargetAndBool)
extern void Detail_easyar_ObjectTracker_loadTarget_m9C35F6282D552907B42C12CFBF414E906537A120 ();
// 0x00000227 System.Void easyar.Detail::easyar_ObjectTracker_unloadTarget(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail_FunctorOfVoidFromTargetAndBool)
extern void Detail_easyar_ObjectTracker_unloadTarget_mE855574256EC16940D29ECD19C0C621F635099BA ();
// 0x00000228 System.Void easyar.Detail::easyar_ObjectTracker_targets(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTracker_targets_m2DE1E07711B3E806B0D0CA969CB8AA8D884720A9 ();
// 0x00000229 System.Boolean easyar.Detail::easyar_ObjectTracker_setSimultaneousNum(System.IntPtr,System.Int32)
extern void Detail_easyar_ObjectTracker_setSimultaneousNum_mA285B33B7B08AE6BFF15B654029634C9F01FCB60 ();
// 0x0000022A System.Int32 easyar.Detail::easyar_ObjectTracker_simultaneousNum(System.IntPtr)
extern void Detail_easyar_ObjectTracker_simultaneousNum_m6C50C7F3E5FB7AB70360208DA7DCA41AFDEFD712 ();
// 0x0000022B System.Void easyar.Detail::easyar_ObjectTracker__dtor(System.IntPtr)
extern void Detail_easyar_ObjectTracker__dtor_m85BFB9128B6CB91E07BCF8DE5CAF43170897E152 ();
// 0x0000022C System.Void easyar.Detail::easyar_ObjectTracker__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ObjectTracker__retain_m7D83EB0FAC658BA21EEC02000A9F741556207AC6 ();
// 0x0000022D System.IntPtr easyar.Detail::easyar_ObjectTracker__typeName(System.IntPtr)
extern void Detail_easyar_ObjectTracker__typeName_m014FF56637C22408F6F7F434A7A9B3454879DEDC ();
// 0x0000022E easyar.CloudRecognizationStatus easyar.Detail::easyar_CloudRecognizationResult_getStatus(System.IntPtr)
extern void Detail_easyar_CloudRecognizationResult_getStatus_mD974099BDC6FC621B69D0EA90682565FE6934EC8 ();
// 0x0000022F System.Void easyar.Detail::easyar_CloudRecognizationResult_getTarget(System.IntPtr,easyar.Detail_OptionalOfImageTarget&)
extern void Detail_easyar_CloudRecognizationResult_getTarget_m5119FD741F471CA50B84DE079EF4AAF849428ED3 ();
// 0x00000230 System.Void easyar.Detail::easyar_CloudRecognizationResult_getUnknownErrorMessage(System.IntPtr,easyar.Detail_OptionalOfString&)
extern void Detail_easyar_CloudRecognizationResult_getUnknownErrorMessage_mC8FE86A63C3EDA140825353F81D42E5403146CD8 ();
// 0x00000231 System.Void easyar.Detail::easyar_CloudRecognizationResult__dtor(System.IntPtr)
extern void Detail_easyar_CloudRecognizationResult__dtor_m9F43D1264F731C9AB0CFBAFF301458F4C003041E ();
// 0x00000232 System.Void easyar.Detail::easyar_CloudRecognizationResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CloudRecognizationResult__retain_m9DFE4BFA4AF32B50F2E44B24BE82B272BBFEB242 ();
// 0x00000233 System.IntPtr easyar.Detail::easyar_CloudRecognizationResult__typeName(System.IntPtr)
extern void Detail_easyar_CloudRecognizationResult__typeName_mE68DBD216AB91075E2B2A868F2CD53833BDFCB1F ();
// 0x00000234 System.Boolean easyar.Detail::easyar_CloudRecognizer_isAvailable()
extern void Detail_easyar_CloudRecognizer_isAvailable_m201CC9DD52255C6A5804A0855990D317B6811B07 ();
// 0x00000235 System.Void easyar.Detail::easyar_CloudRecognizer_create(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CloudRecognizer_create_m5BB930FD2625FAAAA4637845A534C05FDC36F580 ();
// 0x00000236 System.Void easyar.Detail::easyar_CloudRecognizer_createByCloudSecret(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CloudRecognizer_createByCloudSecret_m63E42BF7539EB4EB2C19670E2A7DD90E7D4ED8CC ();
// 0x00000237 System.Void easyar.Detail::easyar_CloudRecognizer_resolve(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail_FunctorOfVoidFromCloudRecognizationResult)
extern void Detail_easyar_CloudRecognizer_resolve_mC9FFBC16A685A39B2E298DA60589EEFFA5A776F1 ();
// 0x00000238 System.Void easyar.Detail::easyar_CloudRecognizer_close(System.IntPtr)
extern void Detail_easyar_CloudRecognizer_close_m405C348ECC3244615430CCAB700984D2FC710072 ();
// 0x00000239 System.Void easyar.Detail::easyar_CloudRecognizer__dtor(System.IntPtr)
extern void Detail_easyar_CloudRecognizer__dtor_m6A97B9CE132F5326AAFA8C77D252A24EEB8997B4 ();
// 0x0000023A System.Void easyar.Detail::easyar_CloudRecognizer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CloudRecognizer__retain_m3E3D1A8C5B6A55C604380034598E90316F072402 ();
// 0x0000023B System.IntPtr easyar.Detail::easyar_CloudRecognizer__typeName(System.IntPtr)
extern void Detail_easyar_CloudRecognizer__typeName_mCEA5F706CEDD21CBC831994953A824C99AAA415A ();
// 0x0000023C System.Void easyar.Detail::easyar_Buffer_wrap(System.IntPtr,System.Int32,easyar.Detail_FunctorOfVoid,System.IntPtr&)
extern void Detail_easyar_Buffer_wrap_m1BA3620B18E939ECD0965D326F91676F8B62EF58 ();
// 0x0000023D System.Void easyar.Detail::easyar_Buffer_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_Buffer_create_mB21EEBF963BDE6AF4EF6B61F8F709D93AAA6A25F ();
// 0x0000023E System.IntPtr easyar.Detail::easyar_Buffer_data(System.IntPtr)
extern void Detail_easyar_Buffer_data_m4BE3323C222DD1B494C5021C4BADD004B5F3C571 ();
// 0x0000023F System.Int32 easyar.Detail::easyar_Buffer_size(System.IntPtr)
extern void Detail_easyar_Buffer_size_m619FADBC4C831EBC1C885BB1128B53B6CFED85EA ();
// 0x00000240 System.Void easyar.Detail::easyar_Buffer_memoryCopy(System.IntPtr,System.IntPtr,System.Int32)
extern void Detail_easyar_Buffer_memoryCopy_m19FA6A02A9CD7FD667B0CD6092E34336ADFAE1F3 ();
// 0x00000241 System.Boolean easyar.Detail::easyar_Buffer_tryCopyFrom(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void Detail_easyar_Buffer_tryCopyFrom_mC5D8D3C3D69E4F311973DEDE01DEFCF5A034CAE9 ();
// 0x00000242 System.Boolean easyar.Detail::easyar_Buffer_tryCopyTo(System.IntPtr,System.Int32,System.IntPtr,System.Int32,System.Int32)
extern void Detail_easyar_Buffer_tryCopyTo_mC9746452D2931C2EB22A31A386F13553AC8F7D91 ();
// 0x00000243 System.Void easyar.Detail::easyar_Buffer_partition(System.IntPtr,System.Int32,System.Int32,System.IntPtr&)
extern void Detail_easyar_Buffer_partition_m6EC2EEBE6DFB176B7817F836AEBCA8B8A0687DA9 ();
// 0x00000244 System.Void easyar.Detail::easyar_Buffer__dtor(System.IntPtr)
extern void Detail_easyar_Buffer__dtor_m7C4FCF34E3D9F2ED6A208F334425C54D3448C63B ();
// 0x00000245 System.Void easyar.Detail::easyar_Buffer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Buffer__retain_mEB8A08340F3409D2EE5212EE79D9B90CB4B81B7C ();
// 0x00000246 System.IntPtr easyar.Detail::easyar_Buffer__typeName(System.IntPtr)
extern void Detail_easyar_Buffer__typeName_m7F89C1945DF282FC9D6DEC3F6B96A3950E0AB42C ();
// 0x00000247 System.Void easyar.Detail::easyar_BufferDictionary__ctor(System.IntPtr&)
extern void Detail_easyar_BufferDictionary__ctor_mF88BC5DFD31DF23E0610E8758C8F619A70792BB1 ();
// 0x00000248 System.Int32 easyar.Detail::easyar_BufferDictionary_count(System.IntPtr)
extern void Detail_easyar_BufferDictionary_count_mB88F66E4091600690AD888F9E1E67A14D52AC200 ();
// 0x00000249 System.Boolean easyar.Detail::easyar_BufferDictionary_contains(System.IntPtr,System.IntPtr)
extern void Detail_easyar_BufferDictionary_contains_m20A73889356E8C6C3C1D4DC508557A9C18C50615 ();
// 0x0000024A System.Void easyar.Detail::easyar_BufferDictionary_tryGet(System.IntPtr,System.IntPtr,easyar.Detail_OptionalOfBuffer&)
extern void Detail_easyar_BufferDictionary_tryGet_m9DE6E8366441EB9601C2F5BE2164518ABC7EC9F0 ();
// 0x0000024B System.Void easyar.Detail::easyar_BufferDictionary_set(System.IntPtr,System.IntPtr,System.IntPtr)
extern void Detail_easyar_BufferDictionary_set_m1563DB528A311DC892EE52AC045524982A74A4F1 ();
// 0x0000024C System.Boolean easyar.Detail::easyar_BufferDictionary_remove(System.IntPtr,System.IntPtr)
extern void Detail_easyar_BufferDictionary_remove_mA67B74E057062D1E4C4CEFC6C05B7FC94075EA24 ();
// 0x0000024D System.Void easyar.Detail::easyar_BufferDictionary_clear(System.IntPtr)
extern void Detail_easyar_BufferDictionary_clear_m5C605430F796159E777F5FE2F3222E4EBF92B257 ();
// 0x0000024E System.Void easyar.Detail::easyar_BufferDictionary__dtor(System.IntPtr)
extern void Detail_easyar_BufferDictionary__dtor_m4B19C39380DBAF585634F598F6443775A898AF77 ();
// 0x0000024F System.Void easyar.Detail::easyar_BufferDictionary__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_BufferDictionary__retain_mAC84AC741125F3E5AC83E291BA403AC3A2B370C6 ();
// 0x00000250 System.IntPtr easyar.Detail::easyar_BufferDictionary__typeName(System.IntPtr)
extern void Detail_easyar_BufferDictionary__typeName_m476C0B239F7BD25D0982C676B669AFBF345B9C1A ();
// 0x00000251 System.Void easyar.Detail::easyar_BufferPool__ctor(System.Int32,System.Int32,System.IntPtr&)
extern void Detail_easyar_BufferPool__ctor_m1BBCA33373C240A2B3F0F2CE957024BAB5A34365 ();
// 0x00000252 System.Int32 easyar.Detail::easyar_BufferPool_block_size(System.IntPtr)
extern void Detail_easyar_BufferPool_block_size_m373DDA4C36F156047690AFEFB6B2ECD33BFE0EC5 ();
// 0x00000253 System.Int32 easyar.Detail::easyar_BufferPool_capacity(System.IntPtr)
extern void Detail_easyar_BufferPool_capacity_mDB7DA3F41C80142FB7516728994310253069C6C4 ();
// 0x00000254 System.Int32 easyar.Detail::easyar_BufferPool_size(System.IntPtr)
extern void Detail_easyar_BufferPool_size_m52131BB12D573FC59EA284903D5F324060A698FC ();
// 0x00000255 System.Void easyar.Detail::easyar_BufferPool_tryAcquire(System.IntPtr,easyar.Detail_OptionalOfBuffer&)
extern void Detail_easyar_BufferPool_tryAcquire_m1AC4A184D52F644CC96DBEE84E152A948BDB2FCA ();
// 0x00000256 System.Void easyar.Detail::easyar_BufferPool__dtor(System.IntPtr)
extern void Detail_easyar_BufferPool__dtor_m655D1AAB727BD94D751B73D9B6AAA7292D88F292 ();
// 0x00000257 System.Void easyar.Detail::easyar_BufferPool__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_BufferPool__retain_mDB58C5C51D924279D9D892A4CCE886C65903A47A ();
// 0x00000258 System.IntPtr easyar.Detail::easyar_BufferPool__typeName(System.IntPtr)
extern void Detail_easyar_BufferPool__typeName_mB254DE792C79843BE92523B6263F57295A5E4991 ();
// 0x00000259 System.Void easyar.Detail::easyar_CameraParameters__ctor(easyar.Vec2I,easyar.Vec2F,easyar.Vec2F,easyar.CameraDeviceType,System.Int32,System.IntPtr&)
extern void Detail_easyar_CameraParameters__ctor_m897B1280BAE20D82859582F6EB1CAAA73144F549 ();
// 0x0000025A easyar.Vec2I easyar.Detail::easyar_CameraParameters_size(System.IntPtr)
extern void Detail_easyar_CameraParameters_size_mAF44717B69E65A626B9CCA2C238D8A85C7E44C92 ();
// 0x0000025B easyar.Vec2F easyar.Detail::easyar_CameraParameters_focalLength(System.IntPtr)
extern void Detail_easyar_CameraParameters_focalLength_m9E9A9760ACFD81A860F330FCFF894448A37F0C87 ();
// 0x0000025C easyar.Vec2F easyar.Detail::easyar_CameraParameters_principalPoint(System.IntPtr)
extern void Detail_easyar_CameraParameters_principalPoint_mECB6A365CFCA09C8328C9A8F9A5959032E6E4B43 ();
// 0x0000025D easyar.CameraDeviceType easyar.Detail::easyar_CameraParameters_cameraDeviceType(System.IntPtr)
extern void Detail_easyar_CameraParameters_cameraDeviceType_m7A2AF8C40F07909E961D8981A2D5E877259ECC18 ();
// 0x0000025E System.Int32 easyar.Detail::easyar_CameraParameters_cameraOrientation(System.IntPtr)
extern void Detail_easyar_CameraParameters_cameraOrientation_m7BD5C65CEFEBFF51AE19D1C08787267AD0DC7D38 ();
// 0x0000025F System.Void easyar.Detail::easyar_CameraParameters_createWithDefaultIntrinsics(easyar.Vec2I,easyar.CameraDeviceType,System.Int32,System.IntPtr&)
extern void Detail_easyar_CameraParameters_createWithDefaultIntrinsics_m6B900C64227F14B57B9B26E235B497F52DC93963 ();
// 0x00000260 System.Void easyar.Detail::easyar_CameraParameters_getResized(System.IntPtr,easyar.Vec2I,System.IntPtr&)
extern void Detail_easyar_CameraParameters_getResized_m661F13960B6111C5F8FF19A5730A2AB880E04A3B ();
// 0x00000261 System.Int32 easyar.Detail::easyar_CameraParameters_imageOrientation(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraParameters_imageOrientation_mFF3AF26B9955E750F2A11AD73B295D316324C79F ();
// 0x00000262 System.Boolean easyar.Detail::easyar_CameraParameters_imageHorizontalFlip(System.IntPtr,System.Boolean)
extern void Detail_easyar_CameraParameters_imageHorizontalFlip_m9D08E0AD13D22D6A382393D92F48CFF8BECF00DF ();
// 0x00000263 easyar.Matrix44F easyar.Detail::easyar_CameraParameters_projection(System.IntPtr,System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean)
extern void Detail_easyar_CameraParameters_projection_mB661C618AB04E111C143FB8CB32900B345EB78BE ();
// 0x00000264 easyar.Matrix44F easyar.Detail::easyar_CameraParameters_imageProjection(System.IntPtr,System.Single,System.Int32,System.Boolean,System.Boolean)
extern void Detail_easyar_CameraParameters_imageProjection_mED4BD94E869194F2257AB0BFB3F7AD27D0EA8F98 ();
// 0x00000265 easyar.Vec2F easyar.Detail::easyar_CameraParameters_screenCoordinatesFromImageCoordinates(System.IntPtr,System.Single,System.Int32,System.Boolean,System.Boolean,easyar.Vec2F)
extern void Detail_easyar_CameraParameters_screenCoordinatesFromImageCoordinates_mF6EC3C88050FA584197A9EACA6A22DD0A962239F ();
// 0x00000266 easyar.Vec2F easyar.Detail::easyar_CameraParameters_imageCoordinatesFromScreenCoordinates(System.IntPtr,System.Single,System.Int32,System.Boolean,System.Boolean,easyar.Vec2F)
extern void Detail_easyar_CameraParameters_imageCoordinatesFromScreenCoordinates_mB22DF92BA55E7A1F7BF78C86D65F22B78EDEEDDF ();
// 0x00000267 System.Boolean easyar.Detail::easyar_CameraParameters_equalsTo(System.IntPtr,System.IntPtr)
extern void Detail_easyar_CameraParameters_equalsTo_mA4A2255BB0150AAD3251593ACCCD8B3049473CFD ();
// 0x00000268 System.Void easyar.Detail::easyar_CameraParameters__dtor(System.IntPtr)
extern void Detail_easyar_CameraParameters__dtor_m23DD42C18919DF6BBA1BA1C714F6F9DA126FC69A ();
// 0x00000269 System.Void easyar.Detail::easyar_CameraParameters__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CameraParameters__retain_m508245EA4FFBC020B76925C936B8B31E86BFBF6E ();
// 0x0000026A System.IntPtr easyar.Detail::easyar_CameraParameters__typeName(System.IntPtr)
extern void Detail_easyar_CameraParameters__typeName_mC6C10988FB4324EF049A2F380FFDB278A1EE94E1 ();
// 0x0000026B System.Void easyar.Detail::easyar_Image__ctor(System.IntPtr,easyar.PixelFormat,System.Int32,System.Int32,System.IntPtr&)
extern void Detail_easyar_Image__ctor_mDFA08B19001ED6365A1D6DE274691625EC5D515E ();
// 0x0000026C System.Void easyar.Detail::easyar_Image_buffer(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Image_buffer_mE3DA04E2C9AC275A2440277AA1731FFF0EEFC36D ();
// 0x0000026D easyar.PixelFormat easyar.Detail::easyar_Image_format(System.IntPtr)
extern void Detail_easyar_Image_format_m5EB4088D2217F46418ABCD5DAE8B466760BBCF99 ();
// 0x0000026E System.Int32 easyar.Detail::easyar_Image_width(System.IntPtr)
extern void Detail_easyar_Image_width_m5BBB9EAC852E680900861C9EAFA1F3BB675E8AF4 ();
// 0x0000026F System.Int32 easyar.Detail::easyar_Image_height(System.IntPtr)
extern void Detail_easyar_Image_height_mB6E4812EFCDFC6F7DD18D92EF396954D84D58467 ();
// 0x00000270 System.Void easyar.Detail::easyar_Image__dtor(System.IntPtr)
extern void Detail_easyar_Image__dtor_m57538823E226ED2299B5270DD1211FD1996616BA ();
// 0x00000271 System.Void easyar.Detail::easyar_Image__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Image__retain_m47B8B304E7C87040C0ABE952ABA27C42C425394C ();
// 0x00000272 System.IntPtr easyar.Detail::easyar_Image__typeName(System.IntPtr)
extern void Detail_easyar_Image__typeName_m4D23476A4F48B9CF4D236D85BB846F87572819C7 ();
// 0x00000273 System.Boolean easyar.Detail::easyar_DenseSpatialMap_isAvailable()
extern void Detail_easyar_DenseSpatialMap_isAvailable_mD56F4A8FACA03ECEB90252FB29B7B67EC538CDF5 ();
// 0x00000274 System.Void easyar.Detail::easyar_DenseSpatialMap_inputFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_DenseSpatialMap_inputFrameSink_mEBFB835C2A63589BD63E164CF12AF69685C2E78F ();
// 0x00000275 System.Int32 easyar.Detail::easyar_DenseSpatialMap_bufferRequirement(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap_bufferRequirement_mF77C4FAF66FE3B74F8CF8D08A0D6C1FDE19AAF28 ();
// 0x00000276 System.Void easyar.Detail::easyar_DenseSpatialMap_create(System.IntPtr&)
extern void Detail_easyar_DenseSpatialMap_create_m0B006C4A9E1DBEA3285FB4F570800ECC74C115EB ();
// 0x00000277 System.Boolean easyar.Detail::easyar_DenseSpatialMap_start(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap_start_m0EA827F28B96AA9C3FCA471A084E1B29808035BA ();
// 0x00000278 System.Void easyar.Detail::easyar_DenseSpatialMap_stop(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap_stop_m4A989E6ADC1C463D3F1778877DA8216D699B4696 ();
// 0x00000279 System.Void easyar.Detail::easyar_DenseSpatialMap_close(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap_close_mC023723A4F77E20C257BA865BC34F4A47834F167 ();
// 0x0000027A System.Void easyar.Detail::easyar_DenseSpatialMap_getMesh(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_DenseSpatialMap_getMesh_m034341C47604DE6626F3C466A384E4A3949D5A7A ();
// 0x0000027B System.Boolean easyar.Detail::easyar_DenseSpatialMap_updateSceneMesh(System.IntPtr,System.Boolean)
extern void Detail_easyar_DenseSpatialMap_updateSceneMesh_mF8E6462A66B2D0D6D46FB44CFE3025230EB2CA68 ();
// 0x0000027C System.Void easyar.Detail::easyar_DenseSpatialMap__dtor(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap__dtor_mC611B57723849B8C6D6330548597E4F4E2D6EBCE ();
// 0x0000027D System.Void easyar.Detail::easyar_DenseSpatialMap__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_DenseSpatialMap__retain_m3BE4D8B4E2596D9A64235FCE8B48711973EBDD75 ();
// 0x0000027E System.IntPtr easyar.Detail::easyar_DenseSpatialMap__typeName(System.IntPtr)
extern void Detail_easyar_DenseSpatialMap__typeName_m05F8BE0F0FC9AEB4E398BE2F1557DDE26D0C1EA2 ();
// 0x0000027F System.Int32 easyar.Detail::easyar_SceneMesh_getNumOfVertexAll(System.IntPtr)
extern void Detail_easyar_SceneMesh_getNumOfVertexAll_m58CFDE70A0E43BE09D4B3A784EA48F052074D9F3 ();
// 0x00000280 System.Int32 easyar.Detail::easyar_SceneMesh_getNumOfIndexAll(System.IntPtr)
extern void Detail_easyar_SceneMesh_getNumOfIndexAll_m7E70741A16C1BCD80E4DFDD3AA8057D5911231A3 ();
// 0x00000281 System.Void easyar.Detail::easyar_SceneMesh_getVerticesAll(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getVerticesAll_mDFC24613895FE0E63C7F040731E306F94B4EDD10 ();
// 0x00000282 System.Void easyar.Detail::easyar_SceneMesh_getNormalsAll(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getNormalsAll_mA49B7FF6634C23C6A471844095C501990AD30517 ();
// 0x00000283 System.Void easyar.Detail::easyar_SceneMesh_getIndicesAll(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getIndicesAll_m894FE7CBA2752AD36F6EBB6C2EADADAD2F57ECA3 ();
// 0x00000284 System.Int32 easyar.Detail::easyar_SceneMesh_getNumOfVertexIncremental(System.IntPtr)
extern void Detail_easyar_SceneMesh_getNumOfVertexIncremental_m7D871DA397D69511293251866E872E6B9BCA94D6 ();
// 0x00000285 System.Int32 easyar.Detail::easyar_SceneMesh_getNumOfIndexIncremental(System.IntPtr)
extern void Detail_easyar_SceneMesh_getNumOfIndexIncremental_m293D5252D637A88DB34B65E6DA9FCA4FDC2C567F ();
// 0x00000286 System.Void easyar.Detail::easyar_SceneMesh_getVerticesIncremental(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getVerticesIncremental_m3619C833C1F2EED5F7C10F0417C57171C86D4112 ();
// 0x00000287 System.Void easyar.Detail::easyar_SceneMesh_getNormalsIncremental(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getNormalsIncremental_m830B09C760822223E467D7BB9F328B69E4F4C072 ();
// 0x00000288 System.Void easyar.Detail::easyar_SceneMesh_getIndicesIncremental(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getIndicesIncremental_m21450204BAF81B4975B3E14F7DDC30DA1D29DBDA ();
// 0x00000289 System.Void easyar.Detail::easyar_SceneMesh_getBlocksInfoIncremental(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh_getBlocksInfoIncremental_mE2C40FC3C5C8F323C9C11A4F71003B49982DC6A1 ();
// 0x0000028A System.Single easyar.Detail::easyar_SceneMesh_getBlockDimensionInMeters(System.IntPtr)
extern void Detail_easyar_SceneMesh_getBlockDimensionInMeters_m221BAA9A8620C086757BD2A575D77769371576E8 ();
// 0x0000028B System.Void easyar.Detail::easyar_SceneMesh__dtor(System.IntPtr)
extern void Detail_easyar_SceneMesh__dtor_mB73222755F14AFD93A1192601C3A5D7A163D4B74 ();
// 0x0000028C System.Void easyar.Detail::easyar_SceneMesh__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SceneMesh__retain_m699B969FF74965066720A6E1379E07EE1003BCA4 ();
// 0x0000028D System.IntPtr easyar.Detail::easyar_SceneMesh__typeName(System.IntPtr)
extern void Detail_easyar_SceneMesh__typeName_m062970E6BA013F524EFAB10B674E22604A0D384D ();
// 0x0000028E System.Void easyar.Detail::easyar_ARCoreCameraDevice__ctor(System.IntPtr&)
extern void Detail_easyar_ARCoreCameraDevice__ctor_mF041E2358FC8848BD9249A716C836EA3308C9415 ();
// 0x0000028F System.Boolean easyar.Detail::easyar_ARCoreCameraDevice_isAvailable()
extern void Detail_easyar_ARCoreCameraDevice_isAvailable_mDF4BD4ADDCB037F46DEB5D671A84E5B1E8508728 ();
// 0x00000290 System.Int32 easyar.Detail::easyar_ARCoreCameraDevice_bufferCapacity(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice_bufferCapacity_m8876F63E9DAEAE42BC7B5F7541FB4A8EA60F8FA7 ();
// 0x00000291 System.Void easyar.Detail::easyar_ARCoreCameraDevice_setBufferCapacity(System.IntPtr,System.Int32)
extern void Detail_easyar_ARCoreCameraDevice_setBufferCapacity_mC271498B411FD3B5ED0585DDF45B9986F130DB7C ();
// 0x00000292 System.Void easyar.Detail::easyar_ARCoreCameraDevice_inputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ARCoreCameraDevice_inputFrameSource_mBD5A4FBEBC39D52753DC7D3BB5A2A0DDE8D97C48 ();
// 0x00000293 System.Boolean easyar.Detail::easyar_ARCoreCameraDevice_start(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice_start_m6D408345CBF7AE734DA51ECE025524D476778B65 ();
// 0x00000294 System.Void easyar.Detail::easyar_ARCoreCameraDevice_stop(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice_stop_m82518F9ECD903935B0FFD65584DD95E2FC031F38 ();
// 0x00000295 System.Void easyar.Detail::easyar_ARCoreCameraDevice_close(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice_close_m07FD2564070029FA20F2D98C3E46CEF7550FF1F0 ();
// 0x00000296 System.Void easyar.Detail::easyar_ARCoreCameraDevice__dtor(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice__dtor_mC723B273096D8E7263B0C62FDA9D39F7C820CEBD ();
// 0x00000297 System.Void easyar.Detail::easyar_ARCoreCameraDevice__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ARCoreCameraDevice__retain_mA5F8B64EFEDAEC890E4B48F6E74FC1E3FC8F8AAF ();
// 0x00000298 System.IntPtr easyar.Detail::easyar_ARCoreCameraDevice__typeName(System.IntPtr)
extern void Detail_easyar_ARCoreCameraDevice__typeName_m365F944E3E1D9B57CC2C3137B02D8D206789A17D ();
// 0x00000299 System.Void easyar.Detail::easyar_ARKitCameraDevice__ctor(System.IntPtr&)
extern void Detail_easyar_ARKitCameraDevice__ctor_mB005EF0C3884D15DDA0CD1FF87E91E2FB76E565B ();
// 0x0000029A System.Boolean easyar.Detail::easyar_ARKitCameraDevice_isAvailable()
extern void Detail_easyar_ARKitCameraDevice_isAvailable_m728912D71EE6B429E2B0935AF9889518D988A8E1 ();
// 0x0000029B System.Int32 easyar.Detail::easyar_ARKitCameraDevice_bufferCapacity(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice_bufferCapacity_mE13C80476A93018A178DB2B6B9B7E6573B714019 ();
// 0x0000029C System.Void easyar.Detail::easyar_ARKitCameraDevice_setBufferCapacity(System.IntPtr,System.Int32)
extern void Detail_easyar_ARKitCameraDevice_setBufferCapacity_m4FAF19CA35BBB7FC9063AA0E4653E93746A8933F ();
// 0x0000029D System.Void easyar.Detail::easyar_ARKitCameraDevice_inputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ARKitCameraDevice_inputFrameSource_m09BABEAB27CE82AB6D1D8F81C20432EAD0A67C93 ();
// 0x0000029E System.Boolean easyar.Detail::easyar_ARKitCameraDevice_start(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice_start_m50DD4DC91BD795409C96C380D5CB758581B1ED5F ();
// 0x0000029F System.Void easyar.Detail::easyar_ARKitCameraDevice_stop(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice_stop_m2695FB2BA14A33EB3C1AB019B4BF8C730065A001 ();
// 0x000002A0 System.Void easyar.Detail::easyar_ARKitCameraDevice_close(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice_close_m965DA4D2EFA95381ED62742330C096EED9BB5496 ();
// 0x000002A1 System.Void easyar.Detail::easyar_ARKitCameraDevice__dtor(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice__dtor_mA241E782DF73ABDF0C9772F3A1CE918D40B1CEDF ();
// 0x000002A2 System.Void easyar.Detail::easyar_ARKitCameraDevice__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ARKitCameraDevice__retain_mC4F75C91C6EC58A5E5851678392A2E0FDD4C4309 ();
// 0x000002A3 System.IntPtr easyar.Detail::easyar_ARKitCameraDevice__typeName(System.IntPtr)
extern void Detail_easyar_ARKitCameraDevice__typeName_m0930E7FFEE2ADFA99EFBCDC29A444EDCA277EE0E ();
// 0x000002A4 System.Void easyar.Detail::easyar_CameraDevice__ctor(System.IntPtr&)
extern void Detail_easyar_CameraDevice__ctor_mCAEF6546126A26E483A7F86313C384458CFD26D3 ();
// 0x000002A5 System.Boolean easyar.Detail::easyar_CameraDevice_isAvailable()
extern void Detail_easyar_CameraDevice_isAvailable_mE5F6CF9D2FDC6A2130202055596617F21B8C866E ();
// 0x000002A6 easyar.AndroidCameraApiType easyar.Detail::easyar_CameraDevice_androidCameraApiType(System.IntPtr)
extern void Detail_easyar_CameraDevice_androidCameraApiType_mC9B94958D0A9E0CAE5D0657CD8DA0BA1610F9220 ();
// 0x000002A7 System.Void easyar.Detail::easyar_CameraDevice_setAndroidCameraApiType(System.IntPtr,easyar.AndroidCameraApiType)
extern void Detail_easyar_CameraDevice_setAndroidCameraApiType_mBA3FFFEAEAF808AE108F325AA5C93DFA72F5761D ();
// 0x000002A8 System.Int32 easyar.Detail::easyar_CameraDevice_bufferCapacity(System.IntPtr)
extern void Detail_easyar_CameraDevice_bufferCapacity_mC1F425A9870F9910EA3D4F0CB480068468CE98B7 ();
// 0x000002A9 System.Void easyar.Detail::easyar_CameraDevice_setBufferCapacity(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_setBufferCapacity_m63ECF1CA20E8869C114C10B1E6ACB38A0B132AD0 ();
// 0x000002AA System.Void easyar.Detail::easyar_CameraDevice_inputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CameraDevice_inputFrameSource_mC7EFD9F8354193555B7962E50BF00A961AE3EDE3 ();
// 0x000002AB System.Void easyar.Detail::easyar_CameraDevice_setStateChangedCallback(System.IntPtr,System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromCameraState)
extern void Detail_easyar_CameraDevice_setStateChangedCallback_mC67B2368FEEE78BD1C0E26BF3D8C077458615FF5 ();
// 0x000002AC System.Void easyar.Detail::easyar_CameraDevice_requestPermissions(System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString)
extern void Detail_easyar_CameraDevice_requestPermissions_m5CA647FE34E538475F3AEC1F7C652DD8BBDEFBEE ();
// 0x000002AD System.Int32 easyar.Detail::easyar_CameraDevice_cameraCount()
extern void Detail_easyar_CameraDevice_cameraCount_m573F869ADC1664E313B91413B298F66DC1768D3F ();
// 0x000002AE System.Boolean easyar.Detail::easyar_CameraDevice_openWithIndex(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_openWithIndex_mB92C2803491AFCE5B2103E207373850A11542C38 ();
// 0x000002AF System.Boolean easyar.Detail::easyar_CameraDevice_openWithSpecificType(System.IntPtr,easyar.CameraDeviceType)
extern void Detail_easyar_CameraDevice_openWithSpecificType_mD19C4159409021B8B0AF8F293AED9CDBEE4468F7 ();
// 0x000002B0 System.Boolean easyar.Detail::easyar_CameraDevice_openWithPreferredType(System.IntPtr,easyar.CameraDeviceType)
extern void Detail_easyar_CameraDevice_openWithPreferredType_mD11E937D22D02062D189A439A33353A3BC3038CA ();
// 0x000002B1 System.Boolean easyar.Detail::easyar_CameraDevice_start(System.IntPtr)
extern void Detail_easyar_CameraDevice_start_m7E82D941D8794EAB789CE54B377C97F335800C02 ();
// 0x000002B2 System.Void easyar.Detail::easyar_CameraDevice_stop(System.IntPtr)
extern void Detail_easyar_CameraDevice_stop_mC396C33B088B5FF1E812703E9C21428632556A58 ();
// 0x000002B3 System.Void easyar.Detail::easyar_CameraDevice_close(System.IntPtr)
extern void Detail_easyar_CameraDevice_close_mEB3148B502BA623BBCD783A5C86EC49599BC2B61 ();
// 0x000002B4 System.Int32 easyar.Detail::easyar_CameraDevice_index(System.IntPtr)
extern void Detail_easyar_CameraDevice_index_mDF08C8D17C10CB37EC51FE6635BF8919EBDDC911 ();
// 0x000002B5 easyar.CameraDeviceType easyar.Detail::easyar_CameraDevice_type(System.IntPtr)
extern void Detail_easyar_CameraDevice_type_m44D8DCCC8AD226353496833C80F505A4C49BBA71 ();
// 0x000002B6 System.Void easyar.Detail::easyar_CameraDevice_cameraParameters(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CameraDevice_cameraParameters_mC9BCF56B66EACE46F11E2A716A8AE4AD4C82DBF9 ();
// 0x000002B7 System.Void easyar.Detail::easyar_CameraDevice_setCameraParameters(System.IntPtr,System.IntPtr)
extern void Detail_easyar_CameraDevice_setCameraParameters_m639F9E6B2503DE5F01F298F59A56AE8382F383CD ();
// 0x000002B8 easyar.Vec2I easyar.Detail::easyar_CameraDevice_size(System.IntPtr)
extern void Detail_easyar_CameraDevice_size_m63CEC1B0CDFECA410B2DA9D3C3A0D4E2668B73E1 ();
// 0x000002B9 System.Int32 easyar.Detail::easyar_CameraDevice_supportedSizeCount(System.IntPtr)
extern void Detail_easyar_CameraDevice_supportedSizeCount_m29FB184A2ED62E7FE948765915EC6A244CBB02E0 ();
// 0x000002BA easyar.Vec2I easyar.Detail::easyar_CameraDevice_supportedSize(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_supportedSize_m49E1788526919816AA63CD5CCBE699DB53BC1F2D ();
// 0x000002BB System.Boolean easyar.Detail::easyar_CameraDevice_setSize(System.IntPtr,easyar.Vec2I)
extern void Detail_easyar_CameraDevice_setSize_mB2D2138092AAD12752E50D3697C5938B8EC6ABCA ();
// 0x000002BC System.Int32 easyar.Detail::easyar_CameraDevice_supportedFrameRateRangeCount(System.IntPtr)
extern void Detail_easyar_CameraDevice_supportedFrameRateRangeCount_m45953AF89503167511EE1931DABB369A1ED130BB ();
// 0x000002BD System.Single easyar.Detail::easyar_CameraDevice_supportedFrameRateRangeLower(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_supportedFrameRateRangeLower_m6DABBAC14B9DA2C1B0705055AEB725E58C2DE4DF ();
// 0x000002BE System.Single easyar.Detail::easyar_CameraDevice_supportedFrameRateRangeUpper(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_supportedFrameRateRangeUpper_m8B047198AA417DFDA4625577A20C1D995ABA9C43 ();
// 0x000002BF System.Int32 easyar.Detail::easyar_CameraDevice_frameRateRange(System.IntPtr)
extern void Detail_easyar_CameraDevice_frameRateRange_mACAAED8B6F89437B275A0D78DCABE3A55DA95436 ();
// 0x000002C0 System.Boolean easyar.Detail::easyar_CameraDevice_setFrameRateRange(System.IntPtr,System.Int32)
extern void Detail_easyar_CameraDevice_setFrameRateRange_m7209FDB07D9E10779657F4380190D1005A438495 ();
// 0x000002C1 System.Boolean easyar.Detail::easyar_CameraDevice_setFlashTorchMode(System.IntPtr,System.Boolean)
extern void Detail_easyar_CameraDevice_setFlashTorchMode_mEBDF0F54D320E12286419A18AC7DCE3FBE400C27 ();
// 0x000002C2 System.Boolean easyar.Detail::easyar_CameraDevice_setFocusMode(System.IntPtr,easyar.CameraDeviceFocusMode)
extern void Detail_easyar_CameraDevice_setFocusMode_m2BAFC33D60AA711A3D91AEB6BD5CB4855E59CEE2 ();
// 0x000002C3 System.Boolean easyar.Detail::easyar_CameraDevice_autoFocus(System.IntPtr)
extern void Detail_easyar_CameraDevice_autoFocus_mF6C736D44A063B5339B9A844C5F1F6127E783C25 ();
// 0x000002C4 System.Void easyar.Detail::easyar_CameraDevice__dtor(System.IntPtr)
extern void Detail_easyar_CameraDevice__dtor_m319B41AA502EF8ECEAC072BCBBD53BF00233BF89 ();
// 0x000002C5 System.Void easyar.Detail::easyar_CameraDevice__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CameraDevice__retain_m6B1B71563F85CB8E27E21758C2CDB40AF8844FFC ();
// 0x000002C6 System.IntPtr easyar.Detail::easyar_CameraDevice__typeName(System.IntPtr)
extern void Detail_easyar_CameraDevice__typeName_m98899A725F80356175A6D0415339965A7897C140 ();
// 0x000002C7 easyar.AndroidCameraApiType easyar.Detail::easyar_CameraDeviceSelector_getAndroidCameraApiType(easyar.CameraDevicePreference)
extern void Detail_easyar_CameraDeviceSelector_getAndroidCameraApiType_m80C6848F6842A1AF77EAF4D40C1CAD52292F0098 ();
// 0x000002C8 System.Void easyar.Detail::easyar_CameraDeviceSelector_createCameraDevice(easyar.CameraDevicePreference,System.IntPtr&)
extern void Detail_easyar_CameraDeviceSelector_createCameraDevice_m8519645B4F70B544A46F8B2E384D170026F868D0 ();
// 0x000002C9 easyar.Matrix44F easyar.Detail::easyar_SurfaceTrackerResult_transform(System.IntPtr)
extern void Detail_easyar_SurfaceTrackerResult_transform_mB469AF0B3C4C58C777C106629F603A90C2D1D0F5 ();
// 0x000002CA System.Void easyar.Detail::easyar_SurfaceTrackerResult__dtor(System.IntPtr)
extern void Detail_easyar_SurfaceTrackerResult__dtor_mC026985AED62834EBEF664E9219AF72C075181C5 ();
// 0x000002CB System.Void easyar.Detail::easyar_SurfaceTrackerResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SurfaceTrackerResult__retain_m7D75E32E90DFB913B74663611D7AF909A2252E46 ();
// 0x000002CC System.IntPtr easyar.Detail::easyar_SurfaceTrackerResult__typeName(System.IntPtr)
extern void Detail_easyar_SurfaceTrackerResult__typeName_m1618FDE48322CCEF0A3C8AE42BA7B383607F73A6 ();
// 0x000002CD System.Void easyar.Detail::easyar_castSurfaceTrackerResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castSurfaceTrackerResultToFrameFilterResult_m6EE939A2FD70F27A131BE0FC214C11F575CAF980 ();
// 0x000002CE System.Void easyar.Detail::easyar_tryCastFrameFilterResultToSurfaceTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToSurfaceTrackerResult_m64500CA5EE64FC312DB94FF76D008946994D10AF ();
// 0x000002CF System.Boolean easyar.Detail::easyar_SurfaceTracker_isAvailable()
extern void Detail_easyar_SurfaceTracker_isAvailable_m2FF4155CD76139B7A361AE0F0DDD8B34F2BD3088 ();
// 0x000002D0 System.Void easyar.Detail::easyar_SurfaceTracker_inputFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SurfaceTracker_inputFrameSink_m439D99A6C3831F218F1882CC4E9F96924F237CDC ();
// 0x000002D1 System.Int32 easyar.Detail::easyar_SurfaceTracker_bufferRequirement(System.IntPtr)
extern void Detail_easyar_SurfaceTracker_bufferRequirement_m3BC4B6A7F57F089A815DD98BBE381F985E432832 ();
// 0x000002D2 System.Void easyar.Detail::easyar_SurfaceTracker_outputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SurfaceTracker_outputFrameSource_mA89448CB827FF79598A3F699E60EB5667C12151D ();
// 0x000002D3 System.Void easyar.Detail::easyar_SurfaceTracker_create(System.IntPtr&)
extern void Detail_easyar_SurfaceTracker_create_m8798194BE3B3CAD4629B3DA73B8203CD5313E2F5 ();
// 0x000002D4 System.Boolean easyar.Detail::easyar_SurfaceTracker_start(System.IntPtr)
extern void Detail_easyar_SurfaceTracker_start_m3632EE50B010D950B34760ED25DFBE7C8F5DAE56 ();
// 0x000002D5 System.Void easyar.Detail::easyar_SurfaceTracker_stop(System.IntPtr)
extern void Detail_easyar_SurfaceTracker_stop_m00F6BD4D467E3A0C6235A8B2A79D531919B47675 ();
// 0x000002D6 System.Void easyar.Detail::easyar_SurfaceTracker_close(System.IntPtr)
extern void Detail_easyar_SurfaceTracker_close_m9EA9D0C4E5F5C0B153CD1B95BE9975816138842B ();
// 0x000002D7 System.Void easyar.Detail::easyar_SurfaceTracker_alignTargetToCameraImagePoint(System.IntPtr,easyar.Vec2F)
extern void Detail_easyar_SurfaceTracker_alignTargetToCameraImagePoint_m6615E4848857425B9C9880F8BB61DE8AD3D18EEB ();
// 0x000002D8 System.Void easyar.Detail::easyar_SurfaceTracker__dtor(System.IntPtr)
extern void Detail_easyar_SurfaceTracker__dtor_m76A9D8D157910119DC0FED074F163B990612245D ();
// 0x000002D9 System.Void easyar.Detail::easyar_SurfaceTracker__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SurfaceTracker__retain_m01F734604D25093345C39A539DD48921B52C4963 ();
// 0x000002DA System.IntPtr easyar.Detail::easyar_SurfaceTracker__typeName(System.IntPtr)
extern void Detail_easyar_SurfaceTracker__typeName_mC7416FD42A20100AAA590AB22AA36B363861113C ();
// 0x000002DB System.Void easyar.Detail::easyar_MotionTrackerCameraDevice__ctor(System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice__ctor_m727AFE139E44C0F8FA124E4C1DEDD915C8F6144C ();
// 0x000002DC System.Boolean easyar.Detail::easyar_MotionTrackerCameraDevice_isAvailable()
extern void Detail_easyar_MotionTrackerCameraDevice_isAvailable_m11935476BF31F015CC0EBB722CD5DD89DF4F9677 ();
// 0x000002DD System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_setBufferCapacity(System.IntPtr,System.Int32)
extern void Detail_easyar_MotionTrackerCameraDevice_setBufferCapacity_mD6286FC8A82DDC0BC0282DE8EEA3A1C6A2C78CF0 ();
// 0x000002DE System.Int32 easyar.Detail::easyar_MotionTrackerCameraDevice_bufferCapacity(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice_bufferCapacity_mECBFD2CF994CF5AB577D706AB1736BCB4247D2E5 ();
// 0x000002DF System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_inputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice_inputFrameSource_mD02985EF51D4FB5591ADA1451FE23854485E589F ();
// 0x000002E0 System.Boolean easyar.Detail::easyar_MotionTrackerCameraDevice_start(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice_start_m5C753369DD4D5F88A6EE0907D2538A29268632A6 ();
// 0x000002E1 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_stop(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice_stop_m6A6A67C41BB1EC0B66D7582EF76390ADE6BD27E9 ();
// 0x000002E2 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_close(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice_close_mA806BAAF3DC409249442750B7F976A9496F2F237 ();
// 0x000002E3 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_hitTestAgainstPointCloud(System.IntPtr,easyar.Vec2F,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice_hitTestAgainstPointCloud_m2615937C9D4E0FD2A6996BC5A14EB7D3286B7499 ();
// 0x000002E4 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane(System.IntPtr,easyar.Vec2F,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane_mCDCC4A6FF6E05CBEFF28294B66B306EBF0E8504D ();
// 0x000002E5 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice_getLocalPointsCloud(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice_getLocalPointsCloud_m402ADA30ABA8FEF9DDE44F1E4E218DF451DA366A ();
// 0x000002E6 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice__dtor(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice__dtor_m10089B71168DE0A04FA9080B9A878E8389B65FA1 ();
// 0x000002E7 System.Void easyar.Detail::easyar_MotionTrackerCameraDevice__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_MotionTrackerCameraDevice__retain_m3D8DA1CB6BAF93E04FB497C064F6550BC483F24E ();
// 0x000002E8 System.IntPtr easyar.Detail::easyar_MotionTrackerCameraDevice__typeName(System.IntPtr)
extern void Detail_easyar_MotionTrackerCameraDevice__typeName_m1F265E35FDFAAC3082FF00628F527CF5E2DED76C ();
// 0x000002E9 System.Void easyar.Detail::easyar_InputFrameRecorder_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameRecorder_input_m4DF5EA651DD63FF730B46F1515BA67DEB9E3B913 ();
// 0x000002EA System.Int32 easyar.Detail::easyar_InputFrameRecorder_bufferRequirement(System.IntPtr)
extern void Detail_easyar_InputFrameRecorder_bufferRequirement_mB7D13488EA5654EF22F5106D46BDA0326E2B22E1 ();
// 0x000002EB System.Void easyar.Detail::easyar_InputFrameRecorder_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameRecorder_output_m1E58748CDF7F645169098ED99C7CEB424C094A6A ();
// 0x000002EC System.Void easyar.Detail::easyar_InputFrameRecorder_create(System.IntPtr&)
extern void Detail_easyar_InputFrameRecorder_create_mE14DFE318C99B74E1E2601AAC6A4D78EB7D83A63 ();
// 0x000002ED System.Boolean easyar.Detail::easyar_InputFrameRecorder_start(System.IntPtr,System.IntPtr)
extern void Detail_easyar_InputFrameRecorder_start_m129C2DB0BCD918108664A14FBA73BDAD770448A7 ();
// 0x000002EE System.Void easyar.Detail::easyar_InputFrameRecorder_stop(System.IntPtr)
extern void Detail_easyar_InputFrameRecorder_stop_mBBB30DAFE8CC60DB610552C5C9B77EFBD122B26B ();
// 0x000002EF System.Void easyar.Detail::easyar_InputFrameRecorder__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameRecorder__dtor_mA78CC2A380192D4AD7B22FFC728464A88253ADB3 ();
// 0x000002F0 System.Void easyar.Detail::easyar_InputFrameRecorder__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameRecorder__retain_mE64F882B0351DD34AE2DC60F36A89AB7068B410B ();
// 0x000002F1 System.IntPtr easyar.Detail::easyar_InputFrameRecorder__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameRecorder__typeName_m1B0856D5521974DBCDB5DCF5F6C47D680B8D6F6C ();
// 0x000002F2 System.Void easyar.Detail::easyar_InputFramePlayer_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFramePlayer_output_mC15B8AE878D3E41ACE224F83BFC8CB31B31BE72D ();
// 0x000002F3 System.Void easyar.Detail::easyar_InputFramePlayer_create(System.IntPtr&)
extern void Detail_easyar_InputFramePlayer_create_m0088A6280D1CE162ECDCF454A0765CE28E91E78F ();
// 0x000002F4 System.Boolean easyar.Detail::easyar_InputFramePlayer_start(System.IntPtr,System.IntPtr)
extern void Detail_easyar_InputFramePlayer_start_m5D03AB385C83D39ABF3960F7450CFFBF1252B11E ();
// 0x000002F5 System.Void easyar.Detail::easyar_InputFramePlayer_stop(System.IntPtr)
extern void Detail_easyar_InputFramePlayer_stop_m239E9A83AFEA6B2333194545758A46980A1F690E ();
// 0x000002F6 System.Void easyar.Detail::easyar_InputFramePlayer__dtor(System.IntPtr)
extern void Detail_easyar_InputFramePlayer__dtor_mBD411AE1282F018AD0FBB15C1695929BD13AA207 ();
// 0x000002F7 System.Void easyar.Detail::easyar_InputFramePlayer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFramePlayer__retain_mC3DC2B40381EEC24EB7CBE23CBC891AF862A3EB4 ();
// 0x000002F8 System.IntPtr easyar.Detail::easyar_InputFramePlayer__typeName(System.IntPtr)
extern void Detail_easyar_InputFramePlayer__typeName_mB49001916B9F8B326E8E11244BADFD475F5F2C62 ();
// 0x000002F9 System.Void easyar.Detail::easyar_CallbackScheduler__dtor(System.IntPtr)
extern void Detail_easyar_CallbackScheduler__dtor_m39A9A64F0E26372CD4CFA620A15B513A6939ECC4 ();
// 0x000002FA System.Void easyar.Detail::easyar_CallbackScheduler__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_CallbackScheduler__retain_m59574AAE7E5827D420F1BD8CC54B83813853C967 ();
// 0x000002FB System.IntPtr easyar.Detail::easyar_CallbackScheduler__typeName(System.IntPtr)
extern void Detail_easyar_CallbackScheduler__typeName_m589AD2E346AECFD9FE2566543D67C05B23BC212E ();
// 0x000002FC System.Void easyar.Detail::easyar_DelayedCallbackScheduler__ctor(System.IntPtr&)
extern void Detail_easyar_DelayedCallbackScheduler__ctor_mCE7004AAAEC9B2204F4E0A4A7B93BC4D32AC3895 ();
// 0x000002FD System.Boolean easyar.Detail::easyar_DelayedCallbackScheduler_runOne(System.IntPtr)
extern void Detail_easyar_DelayedCallbackScheduler_runOne_m18EB4C83D69AA1FD9FFA85D93DFB67DCEFC205D5 ();
// 0x000002FE System.Void easyar.Detail::easyar_DelayedCallbackScheduler__dtor(System.IntPtr)
extern void Detail_easyar_DelayedCallbackScheduler__dtor_mD3162726C09A6D43155C90A3D3D218FD8BC0C699 ();
// 0x000002FF System.Void easyar.Detail::easyar_DelayedCallbackScheduler__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_DelayedCallbackScheduler__retain_mFF35A479B23047015E74EA22E2640AACA1FCCC29 ();
// 0x00000300 System.IntPtr easyar.Detail::easyar_DelayedCallbackScheduler__typeName(System.IntPtr)
extern void Detail_easyar_DelayedCallbackScheduler__typeName_mD4FB80FDA72EF13B63C0B9AC3EA6DEA6075921A7 ();
// 0x00000301 System.Void easyar.Detail::easyar_castDelayedCallbackSchedulerToCallbackScheduler(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castDelayedCallbackSchedulerToCallbackScheduler_m6BD6EA3BAD85FCA04110F265EAF896E3DF31D8F2 ();
// 0x00000302 System.Void easyar.Detail::easyar_tryCastCallbackSchedulerToDelayedCallbackScheduler(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastCallbackSchedulerToDelayedCallbackScheduler_m3E5C68AB79D6417546ACE3D646841CB988A198B6 ();
// 0x00000303 System.Void easyar.Detail::easyar_ImmediateCallbackScheduler_getDefault(System.IntPtr&)
extern void Detail_easyar_ImmediateCallbackScheduler_getDefault_mB6E326D99CDDE6B0FA24905A904AD3093E05E78F ();
// 0x00000304 System.Void easyar.Detail::easyar_ImmediateCallbackScheduler__dtor(System.IntPtr)
extern void Detail_easyar_ImmediateCallbackScheduler__dtor_mEC04B287FDE94C50A5881DB0EC3A0BC5E1553B5B ();
// 0x00000305 System.Void easyar.Detail::easyar_ImmediateCallbackScheduler__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImmediateCallbackScheduler__retain_m5ACE1BA6B7C5B3B158B9EFB4A915E9EC2108B1EE ();
// 0x00000306 System.IntPtr easyar.Detail::easyar_ImmediateCallbackScheduler__typeName(System.IntPtr)
extern void Detail_easyar_ImmediateCallbackScheduler__typeName_m510E9DB875B1EA0ABA963B9934DBB89CBDEA12E5 ();
// 0x00000307 System.Void easyar.Detail::easyar_castImmediateCallbackSchedulerToCallbackScheduler(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castImmediateCallbackSchedulerToCallbackScheduler_m93B607CC756DD2DF4AEE9C92EBE90EF49C9AA9C3 ();
// 0x00000308 System.Void easyar.Detail::easyar_tryCastCallbackSchedulerToImmediateCallbackScheduler(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastCallbackSchedulerToImmediateCallbackScheduler_m80CAF6FE699552BA8B3D5708706F727CF1F8B99D ();
// 0x00000309 System.Void easyar.Detail::easyar_JniUtility_wrapByteArray(System.IntPtr,System.Boolean,easyar.Detail_FunctorOfVoid,System.IntPtr&)
extern void Detail_easyar_JniUtility_wrapByteArray_mBFB61F02352E4E38F5E03CECCC247E3D39C63009 ();
// 0x0000030A System.Void easyar.Detail::easyar_JniUtility_wrapBuffer(System.IntPtr,easyar.Detail_FunctorOfVoid,System.IntPtr&)
extern void Detail_easyar_JniUtility_wrapBuffer_m23B098C2216F4392A189F3DE60CED83E9C638881 ();
// 0x0000030B System.IntPtr easyar.Detail::easyar_JniUtility_getDirectBufferAddress(System.IntPtr)
extern void Detail_easyar_JniUtility_getDirectBufferAddress_m717A78D004765AD9875C04FB65BB2A027B161DF0 ();
// 0x0000030C System.Void easyar.Detail::easyar_Log_setLogFunc(easyar.Detail_FunctorOfVoidFromLogLevelAndString)
extern void Detail_easyar_Log_setLogFunc_m0F7BEC0E5EC412963E1BF658F5233DEBEAD0413A ();
// 0x0000030D System.Void easyar.Detail::easyar_Log_resetLogFunc()
extern void Detail_easyar_Log_resetLogFunc_mF108666AD1FE27C32762DDCF0F66E9396ED62056 ();
// 0x0000030E System.Void easyar.Detail::easyar_ImageTargetParameters__ctor(System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters__ctor_m07EA26F6A43A4427F50C5D4FEA79D0B7C0286795 ();
// 0x0000030F System.Void easyar.Detail::easyar_ImageTargetParameters_image(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters_image_m18D709A7A9CF42526361A9DAA5877C02BA1F08F6 ();
// 0x00000310 System.Void easyar.Detail::easyar_ImageTargetParameters_setImage(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_setImage_mCDFA374AFB674242968FBD9B3FA234C2204B095B ();
// 0x00000311 System.Void easyar.Detail::easyar_ImageTargetParameters_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters_name_mDBA638B6015EB0EA1509E0316B062F31491D52AB ();
// 0x00000312 System.Void easyar.Detail::easyar_ImageTargetParameters_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_setName_m00A3CCEED833F24E0D7481152F2F1ED1350E071C ();
// 0x00000313 System.Void easyar.Detail::easyar_ImageTargetParameters_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters_uid_mCCDAEF5C225C683736319CECB07BFE4014DC9EE5 ();
// 0x00000314 System.Void easyar.Detail::easyar_ImageTargetParameters_setUid(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_setUid_m7A0FE5BEA22869DD38EEC1DB57F911F06A93A13F ();
// 0x00000315 System.Void easyar.Detail::easyar_ImageTargetParameters_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters_meta_mCA5C4D2E22345C94D47AFE773A9F395687F02B0C ();
// 0x00000316 System.Void easyar.Detail::easyar_ImageTargetParameters_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_setMeta_m097B78195DE1981666D01450AA0C6E38758A8407 ();
// 0x00000317 System.Single easyar.Detail::easyar_ImageTargetParameters_scale(System.IntPtr)
extern void Detail_easyar_ImageTargetParameters_scale_m0F89AC8F65FA8F1842C389FEC472D30DF0101F01 ();
// 0x00000318 System.Void easyar.Detail::easyar_ImageTargetParameters_setScale(System.IntPtr,System.Single)
extern void Detail_easyar_ImageTargetParameters_setScale_m91E9A5B3D1BF05BEFE30DAF95BACDF275BD29D55 ();
// 0x00000319 System.Void easyar.Detail::easyar_ImageTargetParameters__dtor(System.IntPtr)
extern void Detail_easyar_ImageTargetParameters__dtor_m73204E67E13266A6A83DA0FE9368552ED4744ECF ();
// 0x0000031A System.Void easyar.Detail::easyar_ImageTargetParameters__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTargetParameters__retain_mB2257E256B08A3009EE18504CB74D3B9E04D7201 ();
// 0x0000031B System.IntPtr easyar.Detail::easyar_ImageTargetParameters__typeName(System.IntPtr)
extern void Detail_easyar_ImageTargetParameters__typeName_m410E0114BDCFFDF453D822582129F214CE7A9893 ();
// 0x0000031C System.Void easyar.Detail::easyar_ImageTarget__ctor(System.IntPtr&)
extern void Detail_easyar_ImageTarget__ctor_m1D31736328E5CE1B9DA2A719771074734311913A ();
// 0x0000031D System.Void easyar.Detail::easyar_ImageTarget_createFromParameters(System.IntPtr,easyar.Detail_OptionalOfImageTarget&)
extern void Detail_easyar_ImageTarget_createFromParameters_mDA802D0EE55BB2932228D9C8522170A180FE9C36 ();
// 0x0000031E System.Void easyar.Detail::easyar_ImageTarget_createFromTargetFile(System.IntPtr,easyar.StorageType,easyar.Detail_OptionalOfImageTarget&)
extern void Detail_easyar_ImageTarget_createFromTargetFile_mBE0906FD2293EB267293C86375912385F76CDA8C ();
// 0x0000031F System.Void easyar.Detail::easyar_ImageTarget_createFromTargetData(System.IntPtr,easyar.Detail_OptionalOfImageTarget&)
extern void Detail_easyar_ImageTarget_createFromTargetData_mEA4C0D44CDFDCFD96B0AD6CAED28A2732F53D709 ();
// 0x00000320 System.Boolean easyar.Detail::easyar_ImageTarget_save(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTarget_save_m4DBDFEB7F0F7FBEA9B10186C104FF93B89EBB4F7 ();
// 0x00000321 System.Void easyar.Detail::easyar_ImageTarget_createFromImageFile(System.IntPtr,easyar.StorageType,System.IntPtr,System.IntPtr,System.IntPtr,System.Single,easyar.Detail_OptionalOfImageTarget&)
extern void Detail_easyar_ImageTarget_createFromImageFile_m25DBB8BAA15342E04A1A8AB216770A78427E194C ();
// 0x00000322 System.Single easyar.Detail::easyar_ImageTarget_scale(System.IntPtr)
extern void Detail_easyar_ImageTarget_scale_m37DDF62084A00D11B1AA2A435B73E718E27189F0 ();
// 0x00000323 System.Single easyar.Detail::easyar_ImageTarget_aspectRatio(System.IntPtr)
extern void Detail_easyar_ImageTarget_aspectRatio_mFDCAAAA4EB71F5BCADC496AA597F0F6E4C4374A0 ();
// 0x00000324 System.Boolean easyar.Detail::easyar_ImageTarget_setScale(System.IntPtr,System.Single)
extern void Detail_easyar_ImageTarget_setScale_m31DBE7CF282B57FAA330C1AC88ED77A11060E0F9 ();
// 0x00000325 System.Void easyar.Detail::easyar_ImageTarget_images(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget_images_m3065F749142E71E38CA219E241DE792AFBA5255E ();
// 0x00000326 System.Int32 easyar.Detail::easyar_ImageTarget_runtimeID(System.IntPtr)
extern void Detail_easyar_ImageTarget_runtimeID_m231959CD0BD8A442C33FD24836DE1EF35FD5E039 ();
// 0x00000327 System.Void easyar.Detail::easyar_ImageTarget_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget_uid_mC391F9C90A0DC84A1186A93A8D0FB29C8700C730 ();
// 0x00000328 System.Void easyar.Detail::easyar_ImageTarget_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget_name_m52C44A4433B3FD46BCAAFA76C4CAC1D97B5482A3 ();
// 0x00000329 System.Void easyar.Detail::easyar_ImageTarget_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTarget_setName_mC0B17313579AC6982D66109F40450B3A15F76555 ();
// 0x0000032A System.Void easyar.Detail::easyar_ImageTarget_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget_meta_m1DC517F07EE2CCD451FB5472D6F5F343B951092C ();
// 0x0000032B System.Void easyar.Detail::easyar_ImageTarget_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTarget_setMeta_mC47A73EDAA02B64C8D68DB35FE985B206BE546FB ();
// 0x0000032C System.Void easyar.Detail::easyar_ImageTarget__dtor(System.IntPtr)
extern void Detail_easyar_ImageTarget__dtor_m2A9CC00D5D8DCBFFAC4A364C5F36F3A6044C3048 ();
// 0x0000032D System.Void easyar.Detail::easyar_ImageTarget__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTarget__retain_m6969FD3418520D472A8718EC0D9AFBA2E151350C ();
// 0x0000032E System.IntPtr easyar.Detail::easyar_ImageTarget__typeName(System.IntPtr)
extern void Detail_easyar_ImageTarget__typeName_mBABA1715E945AC812765AB36127646F2E40819CB ();
// 0x0000032F System.Void easyar.Detail::easyar_castImageTargetToTarget(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castImageTargetToTarget_mCABFFC495B982E061C104B885357159964227B62 ();
// 0x00000330 System.Void easyar.Detail::easyar_tryCastTargetToImageTarget(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastTargetToImageTarget_m2262F03D8D5A9555DE3B139230F06283A43795B1 ();
// 0x00000331 System.Void easyar.Detail::easyar_ImageTrackerResult_targetInstances(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTrackerResult_targetInstances_mE6517822C58A0479639A0BFCCDFF0615CFB9A38A ();
// 0x00000332 System.Void easyar.Detail::easyar_ImageTrackerResult_setTargetInstances(System.IntPtr,System.IntPtr)
extern void Detail_easyar_ImageTrackerResult_setTargetInstances_mBBE18B81784AEF92052EC6710FBB57116656FEAD ();
// 0x00000333 System.Void easyar.Detail::easyar_ImageTrackerResult__dtor(System.IntPtr)
extern void Detail_easyar_ImageTrackerResult__dtor_m48F0CB7BA0E6FE7E6A7026B24AF0EC75D8D5851D ();
// 0x00000334 System.Void easyar.Detail::easyar_ImageTrackerResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTrackerResult__retain_m4326C7C76054BF057A65B73DECB88A456D0CB99C ();
// 0x00000335 System.IntPtr easyar.Detail::easyar_ImageTrackerResult__typeName(System.IntPtr)
extern void Detail_easyar_ImageTrackerResult__typeName_m570F945451FA52682DF48BB454B4D0315142C191 ();
// 0x00000336 System.Void easyar.Detail::easyar_castImageTrackerResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castImageTrackerResultToFrameFilterResult_mE49F1B4F18207CC75543983BBDB72D6FF579147E ();
// 0x00000337 System.Void easyar.Detail::easyar_tryCastFrameFilterResultToImageTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToImageTrackerResult_mC384349FEA6E77BD805025A7966FCC628DD2E4CB ();
// 0x00000338 System.Void easyar.Detail::easyar_castImageTrackerResultToTargetTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castImageTrackerResultToTargetTrackerResult_mBC8FA0475039B62596EDFF8E6667568DE43CF4EA ();
// 0x00000339 System.Void easyar.Detail::easyar_tryCastTargetTrackerResultToImageTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastTargetTrackerResultToImageTrackerResult_mBC61FD5B5A7DAF8B977F84C291BBC6D504EE8980 ();
// 0x0000033A System.Boolean easyar.Detail::easyar_ImageTracker_isAvailable()
extern void Detail_easyar_ImageTracker_isAvailable_mFDF49609CED26A1192F209314161E12435E111BB ();
// 0x0000033B System.Void easyar.Detail::easyar_ImageTracker_feedbackFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTracker_feedbackFrameSink_mDBE3C6E0E2A62BAB2AD88790FD70336A611F0220 ();
// 0x0000033C System.Int32 easyar.Detail::easyar_ImageTracker_bufferRequirement(System.IntPtr)
extern void Detail_easyar_ImageTracker_bufferRequirement_m7D83DDCEE8C34E6D1F9418E19F7E8A1F470E2536 ();
// 0x0000033D System.Void easyar.Detail::easyar_ImageTracker_outputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTracker_outputFrameSource_mB7B57E3569E4E9BD8135023F761C30B34000FE97 ();
// 0x0000033E System.Void easyar.Detail::easyar_ImageTracker_create(System.IntPtr&)
extern void Detail_easyar_ImageTracker_create_mC17DF89AB686F97795E29063227937B62FCE36D7 ();
// 0x0000033F System.Void easyar.Detail::easyar_ImageTracker_createWithMode(easyar.ImageTrackerMode,System.IntPtr&)
extern void Detail_easyar_ImageTracker_createWithMode_m256BF2CE20BCE0C03340A64638DC12F285C06DE2 ();
// 0x00000340 System.Boolean easyar.Detail::easyar_ImageTracker_start(System.IntPtr)
extern void Detail_easyar_ImageTracker_start_m2CCA8146137AE864AEB2C80A2570E02E7443C167 ();
// 0x00000341 System.Void easyar.Detail::easyar_ImageTracker_stop(System.IntPtr)
extern void Detail_easyar_ImageTracker_stop_m61D194D78336A9DAC86A5E3B73597F2E98E38303 ();
// 0x00000342 System.Void easyar.Detail::easyar_ImageTracker_close(System.IntPtr)
extern void Detail_easyar_ImageTracker_close_mDB08F5DB6C72044E0A81849B147B644353CD2C1D ();
// 0x00000343 System.Void easyar.Detail::easyar_ImageTracker_loadTarget(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail_FunctorOfVoidFromTargetAndBool)
extern void Detail_easyar_ImageTracker_loadTarget_mDD8FFDD025C4A80848B7AC3A054EF4DFE6274954 ();
// 0x00000344 System.Void easyar.Detail::easyar_ImageTracker_unloadTarget(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail_FunctorOfVoidFromTargetAndBool)
extern void Detail_easyar_ImageTracker_unloadTarget_mB22583F78FA5E1CA9806E751C3071145FCB82A8B ();
// 0x00000345 System.Void easyar.Detail::easyar_ImageTracker_targets(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTracker_targets_m17A2856752052EA565275FF125417C5354B07523 ();
// 0x00000346 System.Boolean easyar.Detail::easyar_ImageTracker_setSimultaneousNum(System.IntPtr,System.Int32)
extern void Detail_easyar_ImageTracker_setSimultaneousNum_mAB38DCB8815F6B1E914050496AEF8F1CAD814E91 ();
// 0x00000347 System.Int32 easyar.Detail::easyar_ImageTracker_simultaneousNum(System.IntPtr)
extern void Detail_easyar_ImageTracker_simultaneousNum_mC1A177C42982A322962473728673329D62030F0D ();
// 0x00000348 System.Void easyar.Detail::easyar_ImageTracker__dtor(System.IntPtr)
extern void Detail_easyar_ImageTracker__dtor_m3B1962AFEA49D55ADCC1E3B292401D74D36ECA81 ();
// 0x00000349 System.Void easyar.Detail::easyar_ImageTracker__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ImageTracker__retain_m3B905A373405AFA5BE6B6D9DCB3C2FFC44397607 ();
// 0x0000034A System.IntPtr easyar.Detail::easyar_ImageTracker__typeName(System.IntPtr)
extern void Detail_easyar_ImageTracker__typeName_m487BBB249F87EF8CBB9E93F9FDB7F36FAF15994D ();
// 0x0000034B System.Boolean easyar.Detail::easyar_Recorder_isAvailable()
extern void Detail_easyar_Recorder_isAvailable_mABFBC0B3318CFA1163AC1B0E007F1ECA97249E1A ();
// 0x0000034C System.Void easyar.Detail::easyar_Recorder_requestPermissions(System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString)
extern void Detail_easyar_Recorder_requestPermissions_m0051F92400D7D4470C82CAAE109A767571951B66 ();
// 0x0000034D System.Void easyar.Detail::easyar_Recorder_create(System.IntPtr,System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromRecordStatusAndString,System.IntPtr&)
extern void Detail_easyar_Recorder_create_m5017A3FB6CECB470B58E32B9E742378CB0CA04AB ();
// 0x0000034E System.Void easyar.Detail::easyar_Recorder_start(System.IntPtr)
extern void Detail_easyar_Recorder_start_m5418CCBB8102CE237126E7D15B21A1ECE92C296E ();
// 0x0000034F System.Void easyar.Detail::easyar_Recorder_updateFrame(System.IntPtr,System.IntPtr,System.Int32,System.Int32)
extern void Detail_easyar_Recorder_updateFrame_mDC603271B63D4D2696C8671B56E7282741B3ADF8 ();
// 0x00000350 System.Boolean easyar.Detail::easyar_Recorder_stop(System.IntPtr)
extern void Detail_easyar_Recorder_stop_m3EF9F5327BF0BF4F0378EB50D6705D27E765F897 ();
// 0x00000351 System.Void easyar.Detail::easyar_Recorder__dtor(System.IntPtr)
extern void Detail_easyar_Recorder__dtor_m59E502E1952E26FCEFFDFC800EA4777A6456AD47 ();
// 0x00000352 System.Void easyar.Detail::easyar_Recorder__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Recorder__retain_mDA5DD9E705C7EF4C17FD0E50DD2D91FE99EB90F6 ();
// 0x00000353 System.IntPtr easyar.Detail::easyar_Recorder__typeName(System.IntPtr)
extern void Detail_easyar_Recorder__typeName_mB899C4101DB817E36B295B465FE4563E32F12C83 ();
// 0x00000354 System.Void easyar.Detail::easyar_RecorderConfiguration__ctor(System.IntPtr&)
extern void Detail_easyar_RecorderConfiguration__ctor_m83F7E91103393B1D27E393671466CD4212EF1C13 ();
// 0x00000355 System.Void easyar.Detail::easyar_RecorderConfiguration_setOutputFile(System.IntPtr,System.IntPtr)
extern void Detail_easyar_RecorderConfiguration_setOutputFile_m4039B7C8C3BCD9B9F1B59C3A0689AE2AF68C7757 ();
// 0x00000356 System.Boolean easyar.Detail::easyar_RecorderConfiguration_setProfile(System.IntPtr,easyar.RecordProfile)
extern void Detail_easyar_RecorderConfiguration_setProfile_m737B2C6BDA43EA326D7D8825B72BBBEF9F8B6909 ();
// 0x00000357 System.Void easyar.Detail::easyar_RecorderConfiguration_setVideoSize(System.IntPtr,easyar.RecordVideoSize)
extern void Detail_easyar_RecorderConfiguration_setVideoSize_mB5907CF4CA46DD8FC87FDC2900C35656BFF9EB65 ();
// 0x00000358 System.Void easyar.Detail::easyar_RecorderConfiguration_setVideoBitrate(System.IntPtr,System.Int32)
extern void Detail_easyar_RecorderConfiguration_setVideoBitrate_m84F864E885B9560A73EAAF7E6B903AFD4D1D1D64 ();
// 0x00000359 System.Void easyar.Detail::easyar_RecorderConfiguration_setChannelCount(System.IntPtr,System.Int32)
extern void Detail_easyar_RecorderConfiguration_setChannelCount_m8416CA980C8D37DCA43A98CC61CB32AF72061FB9 ();
// 0x0000035A System.Void easyar.Detail::easyar_RecorderConfiguration_setAudioSampleRate(System.IntPtr,System.Int32)
extern void Detail_easyar_RecorderConfiguration_setAudioSampleRate_mDEC3C82685E330733FA73A53F74EDCED004AF06C ();
// 0x0000035B System.Void easyar.Detail::easyar_RecorderConfiguration_setAudioBitrate(System.IntPtr,System.Int32)
extern void Detail_easyar_RecorderConfiguration_setAudioBitrate_m49F295D9DE19AC92898FAB96217273C99AC37339 ();
// 0x0000035C System.Void easyar.Detail::easyar_RecorderConfiguration_setVideoOrientation(System.IntPtr,easyar.RecordVideoOrientation)
extern void Detail_easyar_RecorderConfiguration_setVideoOrientation_m760ABDFCD7E3C8756AA25175A84CD7BCDCA38C7C ();
// 0x0000035D System.Void easyar.Detail::easyar_RecorderConfiguration_setZoomMode(System.IntPtr,easyar.RecordZoomMode)
extern void Detail_easyar_RecorderConfiguration_setZoomMode_m1A533CAF9BE63D981075A9D6B4465A06B453586A ();
// 0x0000035E System.Void easyar.Detail::easyar_RecorderConfiguration__dtor(System.IntPtr)
extern void Detail_easyar_RecorderConfiguration__dtor_m746F06BA71001E7B7F1727323185B526D5F9A0C3 ();
// 0x0000035F System.Void easyar.Detail::easyar_RecorderConfiguration__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_RecorderConfiguration__retain_m0B635CEAA01D7CCF6CCCE86F82D1C26DF542E7D6 ();
// 0x00000360 System.IntPtr easyar.Detail::easyar_RecorderConfiguration__typeName(System.IntPtr)
extern void Detail_easyar_RecorderConfiguration__typeName_m31F5F037D048C52716EF9F8BC83F42A157982284 ();
// 0x00000361 easyar.MotionTrackingStatus easyar.Detail::easyar_SparseSpatialMapResult_getMotionTrackingStatus(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult_getMotionTrackingStatus_mAB061EE73B15AB2D4E017AB8127E9C2D3D45D699 ();
// 0x00000362 easyar.Detail_OptionalOfMatrix44F easyar.Detail::easyar_SparseSpatialMapResult_getVioPose(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult_getVioPose_m790F1ED09101C2EE9C7E92927EA8D38EB5111E62 ();
// 0x00000363 easyar.Detail_OptionalOfMatrix44F easyar.Detail::easyar_SparseSpatialMapResult_getMapPose(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult_getMapPose_m6FE588A2EED5E126409077CE37F5AEF4E9467FF5 ();
// 0x00000364 System.Boolean easyar.Detail::easyar_SparseSpatialMapResult_getLocalizationStatus(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult_getLocalizationStatus_m78A2568B562BBD8CB0760D9956EBECEB1791C1DB ();
// 0x00000365 System.Void easyar.Detail::easyar_SparseSpatialMapResult_getLocalizationMapID(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapResult_getLocalizationMapID_m4F7CEB84A0078EA54A0A6EA52A1B9DAE0764F041 ();
// 0x00000366 System.Void easyar.Detail::easyar_SparseSpatialMapResult__dtor(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult__dtor_mC64C6F5FABBBA713566E9932A812C23DDD6D7FFF ();
// 0x00000367 System.Void easyar.Detail::easyar_SparseSpatialMapResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapResult__retain_m77A6D8E86722F9A3170293759A26A762EAB8D339 ();
// 0x00000368 System.IntPtr easyar.Detail::easyar_SparseSpatialMapResult__typeName(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapResult__typeName_m50AB472D4C5548D749F11BB4FB8108D82C6619DC ();
// 0x00000369 System.Void easyar.Detail::easyar_castSparseSpatialMapResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castSparseSpatialMapResultToFrameFilterResult_mA0630037A3252A0BE9DF46A8D5B99B7173FAA218 ();
// 0x0000036A System.Void easyar.Detail::easyar_tryCastFrameFilterResultToSparseSpatialMapResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToSparseSpatialMapResult_mE4AAA81F87961B85C46CA4051F101864C6CCCED6 ();
// 0x0000036B System.Void easyar.Detail::easyar_PlaneData__ctor(System.IntPtr&)
extern void Detail_easyar_PlaneData__ctor_mBD260DBC79506DDE92C0FAAC460AC118F0925D9A ();
// 0x0000036C easyar.PlaneType easyar.Detail::easyar_PlaneData_getType(System.IntPtr)
extern void Detail_easyar_PlaneData_getType_m3F35A1CA31353882786C0EAB649F0F0869E0B9FA ();
// 0x0000036D easyar.Matrix44F easyar.Detail::easyar_PlaneData_getPose(System.IntPtr)
extern void Detail_easyar_PlaneData_getPose_mC58DCF2285D99C31F78B519211AC53DEDAD41A62 ();
// 0x0000036E System.Single easyar.Detail::easyar_PlaneData_getExtentX(System.IntPtr)
extern void Detail_easyar_PlaneData_getExtentX_mABADEE0381B9075F498943F5009619E2CABE5823 ();
// 0x0000036F System.Single easyar.Detail::easyar_PlaneData_getExtentZ(System.IntPtr)
extern void Detail_easyar_PlaneData_getExtentZ_m81A27006E91DD8544B0F9108F7B5477711EA8107 ();
// 0x00000370 System.Void easyar.Detail::easyar_PlaneData__dtor(System.IntPtr)
extern void Detail_easyar_PlaneData__dtor_mE8127645EE251FBCCA69FF6A426451B914019674 ();
// 0x00000371 System.Void easyar.Detail::easyar_PlaneData__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_PlaneData__retain_m696401005DF6C6492E0B4BF02C823C365FB6072B ();
// 0x00000372 System.IntPtr easyar.Detail::easyar_PlaneData__typeName(System.IntPtr)
extern void Detail_easyar_PlaneData__typeName_mEBAB29BB70F4409000DE1AF945F7A1A806396300 ();
// 0x00000373 System.Void easyar.Detail::easyar_SparseSpatialMapConfig__ctor(System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapConfig__ctor_m9F3A92613C57D391B1F7E03B5F99D46F17C62679 ();
// 0x00000374 System.Void easyar.Detail::easyar_SparseSpatialMapConfig_setLocalizationMode(System.IntPtr,easyar.LocalizationMode)
extern void Detail_easyar_SparseSpatialMapConfig_setLocalizationMode_m6E74B91D6176F40F4C5622BDA2A0D890D541A64E ();
// 0x00000375 easyar.LocalizationMode easyar.Detail::easyar_SparseSpatialMapConfig_getLocalizationMode(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapConfig_getLocalizationMode_m02FAF2A5160A4FDF325B5DDFE775D7D096C769D1 ();
// 0x00000376 System.Void easyar.Detail::easyar_SparseSpatialMapConfig__dtor(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapConfig__dtor_mB4D3F34C103BC1761AC41AA2C4A6FC75E940E9DE ();
// 0x00000377 System.Void easyar.Detail::easyar_SparseSpatialMapConfig__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapConfig__retain_m5B86733D757F05FCBD0553C604C098F25455CCB0 ();
// 0x00000378 System.IntPtr easyar.Detail::easyar_SparseSpatialMapConfig__typeName(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapConfig__typeName_m671ACDBA92CD2F07547A47B9B7E19DC2F22D7F2D ();
// 0x00000379 System.Boolean easyar.Detail::easyar_SparseSpatialMap_isAvailable()
extern void Detail_easyar_SparseSpatialMap_isAvailable_m9F68231D6439515FA65C28C21D22DE267C10B9ED ();
// 0x0000037A System.Void easyar.Detail::easyar_SparseSpatialMap_inputFrameSink(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_inputFrameSink_m7A365ECD8C7D557B6FA6D06E263C5BFCE5B5757B ();
// 0x0000037B System.Int32 easyar.Detail::easyar_SparseSpatialMap_bufferRequirement(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_bufferRequirement_mD8A35806FCFD18870F30D9694BCE4C90D484AA04 ();
// 0x0000037C System.Void easyar.Detail::easyar_SparseSpatialMap_outputFrameSource(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_outputFrameSource_m88DCC7262DF68FB197C72A692E42FA5B46BEA782 ();
// 0x0000037D System.Void easyar.Detail::easyar_SparseSpatialMap_create(System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_create_mA3956F8DE7E06EEDDBA193363C810D345194D01C ();
// 0x0000037E System.Boolean easyar.Detail::easyar_SparseSpatialMap_start(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_start_mD0E087EAF96D92A7C349EB9C9DABF18D73941ECB ();
// 0x0000037F System.Void easyar.Detail::easyar_SparseSpatialMap_stop(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_stop_mAFAF04DC0B95E1F666B2724F2AA3FA61662F4B43 ();
// 0x00000380 System.Void easyar.Detail::easyar_SparseSpatialMap_close(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_close_m6604C4EE0EF0B619911FFD82D15FAE291940C3A8 ();
// 0x00000381 System.Void easyar.Detail::easyar_SparseSpatialMap_getPointCloudBuffer(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_getPointCloudBuffer_m32AFD1A7D5F0BBAA8E11CA9AAD95C1FD856A9195 ();
// 0x00000382 System.Void easyar.Detail::easyar_SparseSpatialMap_getMapPlanes(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_getMapPlanes_m4274383D525033474BF84B319C25A3C71B3EBA76 ();
// 0x00000383 System.Void easyar.Detail::easyar_SparseSpatialMap_hitTestAgainstPointCloud(System.IntPtr,easyar.Vec2F,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_hitTestAgainstPointCloud_m0BF4FC9A097615A81D0A10F978A8B8F87D926B1A ();
// 0x00000384 System.Void easyar.Detail::easyar_SparseSpatialMap_hitTestAgainstPlanes(System.IntPtr,easyar.Vec2F,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_hitTestAgainstPlanes_mDB1F174000961146617515AA043938ECC80D9290 ();
// 0x00000385 System.Void easyar.Detail::easyar_SparseSpatialMap_getMapVersion(System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_getMapVersion_mEFF3B1EA573D58EA84CA87EBD97433068EB54C26 ();
// 0x00000386 System.Void easyar.Detail::easyar_SparseSpatialMap_unloadMap(System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromBool)
extern void Detail_easyar_SparseSpatialMap_unloadMap_m6ABF26B1DE14E121726C70C83BA75F73EAE19788 ();
// 0x00000387 System.Void easyar.Detail::easyar_SparseSpatialMap_setConfig(System.IntPtr,System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_setConfig_m5924B3AF77BEFFF8C58485D58A3A0B3ACE71015F ();
// 0x00000388 System.Void easyar.Detail::easyar_SparseSpatialMap_getConfig(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap_getConfig_mEEC500AA97B5AFDEBF649D01EA047667F1A7541B ();
// 0x00000389 System.Boolean easyar.Detail::easyar_SparseSpatialMap_startLocalization(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_startLocalization_m0D72783D2F348205EAD1B75C5A4DD34139A3E2F9 ();
// 0x0000038A System.Void easyar.Detail::easyar_SparseSpatialMap_stopLocalization(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap_stopLocalization_m00F4450C3365F672C51B569DE87A89D5E222A5A2 ();
// 0x0000038B System.Void easyar.Detail::easyar_SparseSpatialMap__dtor(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap__dtor_m476B6F81CA292BEF62B626CD36B03AD9DF460542 ();
// 0x0000038C System.Void easyar.Detail::easyar_SparseSpatialMap__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMap__retain_m7B4D289355B3EA1F4407AD6E149C7A3AB11A4213 ();
// 0x0000038D System.IntPtr easyar.Detail::easyar_SparseSpatialMap__typeName(System.IntPtr)
extern void Detail_easyar_SparseSpatialMap__typeName_mBA177B67D9752789818344F687DFDF76F6E2A230 ();
// 0x0000038E System.Boolean easyar.Detail::easyar_SparseSpatialMapManager_isAvailable()
extern void Detail_easyar_SparseSpatialMapManager_isAvailable_m5AED9DA580C9C4A50E9AE38A2FE72A990CEC5163 ();
// 0x0000038F System.Void easyar.Detail::easyar_SparseSpatialMapManager_create(System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapManager_create_mEC6DD3790B7AEA370F794BA74B458929B044669D ();
// 0x00000390 System.Void easyar.Detail::easyar_SparseSpatialMapManager_host(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail_OptionalOfImage,System.IntPtr,easyar.Detail_FunctorOfVoidFromBoolAndStringAndString)
extern void Detail_easyar_SparseSpatialMapManager_host_m3E2A42940E6B9A6E7970E94778FF9796C6E1D210 ();
// 0x00000391 System.Void easyar.Detail::easyar_SparseSpatialMapManager_load(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,easyar.Detail_FunctorOfVoidFromBoolAndString)
extern void Detail_easyar_SparseSpatialMapManager_load_mAC0B59BC8BA33BAB3046BC7A6475FEE1E591021E ();
// 0x00000392 System.Void easyar.Detail::easyar_SparseSpatialMapManager_clear(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapManager_clear_mA778B45891681B199A32BB40C230E021E39C757C ();
// 0x00000393 System.Void easyar.Detail::easyar_SparseSpatialMapManager__dtor(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapManager__dtor_mF098DEA7BEFD6308360A548BD4E64688EEADE9ED ();
// 0x00000394 System.Void easyar.Detail::easyar_SparseSpatialMapManager__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SparseSpatialMapManager__retain_mDEFA6F3B61263FD1B892D03EE5CE1B7365FA7475 ();
// 0x00000395 System.IntPtr easyar.Detail::easyar_SparseSpatialMapManager__typeName(System.IntPtr)
extern void Detail_easyar_SparseSpatialMapManager__typeName_m4B418EC06537315A990E0C72AFF140FB4B023A9A ();
// 0x00000396 System.Int32 easyar.Detail::easyar_Engine_schemaHash()
extern void Detail_easyar_Engine_schemaHash_m3DF9D806291E1AF6E3AFA825D3019C84B6F2D2D9 ();
// 0x00000397 System.Boolean easyar.Detail::easyar_Engine_initialize(System.IntPtr)
extern void Detail_easyar_Engine_initialize_m1B206FCD51B5A2389118D354B5BE97EEAB16A946 ();
// 0x00000398 System.Void easyar.Detail::easyar_Engine_onPause()
extern void Detail_easyar_Engine_onPause_m510DA8D4E5547681E1F52B69117F95459815DEDE ();
// 0x00000399 System.Void easyar.Detail::easyar_Engine_onResume()
extern void Detail_easyar_Engine_onResume_m2DBC1F52423C89E5845B9877B585555E89F993E2 ();
// 0x0000039A System.Void easyar.Detail::easyar_Engine_errorMessage(System.IntPtr&)
extern void Detail_easyar_Engine_errorMessage_m1F7BC10BDBEE1BF954422FAB65B35D3209023897 ();
// 0x0000039B System.Void easyar.Detail::easyar_Engine_versionString(System.IntPtr&)
extern void Detail_easyar_Engine_versionString_m2AFE64E3508110755A5A96A023030FD929DEE7C1 ();
// 0x0000039C System.Void easyar.Detail::easyar_Engine_name(System.IntPtr&)
extern void Detail_easyar_Engine_name_m838240CA3D078DB6AE7E6B499AF66F477970CD4D ();
// 0x0000039D System.Void easyar.Detail::easyar_VideoPlayer__ctor(System.IntPtr&)
extern void Detail_easyar_VideoPlayer__ctor_m8DB6F5060D412234135EE6EB135FEF51BB1A7F99 ();
// 0x0000039E System.Boolean easyar.Detail::easyar_VideoPlayer_isAvailable()
extern void Detail_easyar_VideoPlayer_isAvailable_mBB1E4AD220CB8DA6D99D5A70365B670622FADDC8 ();
// 0x0000039F System.Void easyar.Detail::easyar_VideoPlayer_setVideoType(System.IntPtr,easyar.VideoType)
extern void Detail_easyar_VideoPlayer_setVideoType_mAF86B1646CD8C60F5EC390386204C06A37C4A187 ();
// 0x000003A0 System.Void easyar.Detail::easyar_VideoPlayer_setRenderTexture(System.IntPtr,System.IntPtr)
extern void Detail_easyar_VideoPlayer_setRenderTexture_m2015F5AAB35DB4E98C599AD9DB11DBF8C0102DBA ();
// 0x000003A1 System.Void easyar.Detail::easyar_VideoPlayer_open(System.IntPtr,System.IntPtr,easyar.StorageType,System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromVideoStatus)
extern void Detail_easyar_VideoPlayer_open_m2A01D59651B7C4D1894357513BFD5B1EC5B637D7 ();
// 0x000003A2 System.Void easyar.Detail::easyar_VideoPlayer_close(System.IntPtr)
extern void Detail_easyar_VideoPlayer_close_mFA9DFD14E739BE7B22B5F621B0303C185B7B4438 ();
// 0x000003A3 System.Boolean easyar.Detail::easyar_VideoPlayer_play(System.IntPtr)
extern void Detail_easyar_VideoPlayer_play_mBD6A25586E24FC14B7D2105B52C8456CEEE5991D ();
// 0x000003A4 System.Void easyar.Detail::easyar_VideoPlayer_stop(System.IntPtr)
extern void Detail_easyar_VideoPlayer_stop_m712F76BC80B2CB302A9D595EACF04304D7E0111B ();
// 0x000003A5 System.Void easyar.Detail::easyar_VideoPlayer_pause(System.IntPtr)
extern void Detail_easyar_VideoPlayer_pause_mA535E91E2C2147FD378A78ECA6A60BAEDA8F1BE9 ();
// 0x000003A6 System.Boolean easyar.Detail::easyar_VideoPlayer_isRenderTextureAvailable(System.IntPtr)
extern void Detail_easyar_VideoPlayer_isRenderTextureAvailable_m021A763AF0B8EEEDE57E584800AAD544FB0DCF1C ();
// 0x000003A7 System.Void easyar.Detail::easyar_VideoPlayer_updateFrame(System.IntPtr)
extern void Detail_easyar_VideoPlayer_updateFrame_mEE526A47314268668CDED799A3E1ABC16B8FE613 ();
// 0x000003A8 System.Int32 easyar.Detail::easyar_VideoPlayer_duration(System.IntPtr)
extern void Detail_easyar_VideoPlayer_duration_m7E60163C83CF5DD24AF73904D893DDBCC24DD534 ();
// 0x000003A9 System.Int32 easyar.Detail::easyar_VideoPlayer_currentPosition(System.IntPtr)
extern void Detail_easyar_VideoPlayer_currentPosition_m8338D910A3BA1F32DE27D387ABCD9CBA97EBED51 ();
// 0x000003AA System.Boolean easyar.Detail::easyar_VideoPlayer_seek(System.IntPtr,System.Int32)
extern void Detail_easyar_VideoPlayer_seek_mC4047C70A2A10D0D1A8C49D26636302C8F569306 ();
// 0x000003AB easyar.Vec2I easyar.Detail::easyar_VideoPlayer_size(System.IntPtr)
extern void Detail_easyar_VideoPlayer_size_mAEB8FAB4F17A13C157EC69DA6AF586AF977B91F9 ();
// 0x000003AC System.Single easyar.Detail::easyar_VideoPlayer_volume(System.IntPtr)
extern void Detail_easyar_VideoPlayer_volume_mE5A63DE9CDB2374F511168416AF35C3808CD1787 ();
// 0x000003AD System.Boolean easyar.Detail::easyar_VideoPlayer_setVolume(System.IntPtr,System.Single)
extern void Detail_easyar_VideoPlayer_setVolume_m93A43F5F747F432268155DF7CA05268535042B92 ();
// 0x000003AE System.Void easyar.Detail::easyar_VideoPlayer__dtor(System.IntPtr)
extern void Detail_easyar_VideoPlayer__dtor_m675905043D8942B2DFF58102981551D5EA3ECE75 ();
// 0x000003AF System.Void easyar.Detail::easyar_VideoPlayer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_VideoPlayer__retain_m55D5DAF0EFD54A4407042ED24B3147495D205169 ();
// 0x000003B0 System.IntPtr easyar.Detail::easyar_VideoPlayer__typeName(System.IntPtr)
extern void Detail_easyar_VideoPlayer__typeName_mBFB0DB515A4CF5322209B29B3A859C8307AEA57E ();
// 0x000003B1 System.Void easyar.Detail::easyar_ImageHelper_decode(System.IntPtr,easyar.Detail_OptionalOfImage&)
extern void Detail_easyar_ImageHelper_decode_mB10833518D87B180F6E10E80F2142F347808B1A4 ();
// 0x000003B2 System.Void easyar.Detail::easyar_SignalSink_handle(System.IntPtr)
extern void Detail_easyar_SignalSink_handle_m59C55E4C08E1D9C2A54B926A0703085953733F69 ();
// 0x000003B3 System.Void easyar.Detail::easyar_SignalSink__dtor(System.IntPtr)
extern void Detail_easyar_SignalSink__dtor_m1EAA5EBA1A3FCF410D8453B3442789EE2F52808E ();
// 0x000003B4 System.Void easyar.Detail::easyar_SignalSink__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SignalSink__retain_mDD8ADCFF1BF8DE82C0932E9B95F697C459ACC9E3 ();
// 0x000003B5 System.IntPtr easyar.Detail::easyar_SignalSink__typeName(System.IntPtr)
extern void Detail_easyar_SignalSink__typeName_m085E47ABA9D76616AF51763D74D6C1598E258D47 ();
// 0x000003B6 System.Void easyar.Detail::easyar_SignalSource_setHandler(System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoid)
extern void Detail_easyar_SignalSource_setHandler_m11906B6C65CB059A2DC9149D2AC679A3749448E3 ();
// 0x000003B7 System.Void easyar.Detail::easyar_SignalSource_connect(System.IntPtr,System.IntPtr)
extern void Detail_easyar_SignalSource_connect_mD365AE8AAE4B9B0ACC52347012854E163D0BFE2A ();
// 0x000003B8 System.Void easyar.Detail::easyar_SignalSource_disconnect(System.IntPtr)
extern void Detail_easyar_SignalSource_disconnect_mB76D8B59CA59F2ADC99B3121BBD753C29A634530 ();
// 0x000003B9 System.Void easyar.Detail::easyar_SignalSource__dtor(System.IntPtr)
extern void Detail_easyar_SignalSource__dtor_m1DF8F325175E7DCFCCAD45E262CBDCE9420947A0 ();
// 0x000003BA System.Void easyar.Detail::easyar_SignalSource__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_SignalSource__retain_m44EFF16019E42BD54E6018AB06D0485EA3B3A66F ();
// 0x000003BB System.IntPtr easyar.Detail::easyar_SignalSource__typeName(System.IntPtr)
extern void Detail_easyar_SignalSource__typeName_mE5F6E163FB1A2172D9A23F4B6D05B0D46C2AAAA9 ();
// 0x000003BC System.Void easyar.Detail::easyar_InputFrameSink_handle(System.IntPtr,System.IntPtr)
extern void Detail_easyar_InputFrameSink_handle_m943953C5D8A7EDD0E5EF660A85D23F2C1926A119 ();
// 0x000003BD System.Void easyar.Detail::easyar_InputFrameSink__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameSink__dtor_mAD97B7B952CCE5961FAB12E535E72975CE8C74D5 ();
// 0x000003BE System.Void easyar.Detail::easyar_InputFrameSink__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameSink__retain_m5EA5EB364A37D6790E92CB5DDFD5F02C34AE9890 ();
// 0x000003BF System.IntPtr easyar.Detail::easyar_InputFrameSink__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameSink__typeName_mD355AA8BE0B99293A3EAEF4A00F2B10A4FF40526 ();
// 0x000003C0 System.Void easyar.Detail::easyar_InputFrameSource_setHandler(System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromInputFrame)
extern void Detail_easyar_InputFrameSource_setHandler_mD5B47AA1BF5B21CC60738101DC226EEEE285B2BC ();
// 0x000003C1 System.Void easyar.Detail::easyar_InputFrameSource_connect(System.IntPtr,System.IntPtr)
extern void Detail_easyar_InputFrameSource_connect_m50D337FA30D91967BEF23E271F8E3462192E5B17 ();
// 0x000003C2 System.Void easyar.Detail::easyar_InputFrameSource_disconnect(System.IntPtr)
extern void Detail_easyar_InputFrameSource_disconnect_mD36F901268D2A0C6EAF47F11049EC94C64963B9B ();
// 0x000003C3 System.Void easyar.Detail::easyar_InputFrameSource__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameSource__dtor_mEC6F1C1DC3FBB9E9A91B23EEB8D7A330F2FE4A90 ();
// 0x000003C4 System.Void easyar.Detail::easyar_InputFrameSource__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameSource__retain_mB8BFDCC9A385A57DB9FDC04DC4E588255CD64558 ();
// 0x000003C5 System.IntPtr easyar.Detail::easyar_InputFrameSource__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameSource__typeName_m235448F1D44FC1478AD8AD38A0FF9AF4C6DCB65F ();
// 0x000003C6 System.Void easyar.Detail::easyar_OutputFrameSink_handle(System.IntPtr,System.IntPtr)
extern void Detail_easyar_OutputFrameSink_handle_m61EF117EFA29F64E32F8462D5D53B1F882E263D9 ();
// 0x000003C7 System.Void easyar.Detail::easyar_OutputFrameSink__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameSink__dtor_m2A59348ECE0703732D4F63DFF5AACC1965B1E28C ();
// 0x000003C8 System.Void easyar.Detail::easyar_OutputFrameSink__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameSink__retain_m410F5275AAF508E68062DF49B989F1C5DF2AA8D1 ();
// 0x000003C9 System.IntPtr easyar.Detail::easyar_OutputFrameSink__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameSink__typeName_m777DB5216BF44D0EE4B87413A85290BB2688A832 ();
// 0x000003CA System.Void easyar.Detail::easyar_OutputFrameSource_setHandler(System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromOutputFrame)
extern void Detail_easyar_OutputFrameSource_setHandler_mF916C237048E70CD619E066C53749BA4CAC0F071 ();
// 0x000003CB System.Void easyar.Detail::easyar_OutputFrameSource_connect(System.IntPtr,System.IntPtr)
extern void Detail_easyar_OutputFrameSource_connect_m56A94759FD485ABB1C0EE8B68CF33440A31DD30C ();
// 0x000003CC System.Void easyar.Detail::easyar_OutputFrameSource_disconnect(System.IntPtr)
extern void Detail_easyar_OutputFrameSource_disconnect_mB9D89CC8AB2826F01AD3E78A88FBE5F489441A92 ();
// 0x000003CD System.Void easyar.Detail::easyar_OutputFrameSource__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameSource__dtor_mF26C029B73895B14611A4FB3C7BE96D482A349A4 ();
// 0x000003CE System.Void easyar.Detail::easyar_OutputFrameSource__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameSource__retain_mD0E3AB9A3F3B147048F4812E646E7C7E406EB47D ();
// 0x000003CF System.IntPtr easyar.Detail::easyar_OutputFrameSource__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameSource__typeName_m4620092BB5661AE4361B750595F3ACC8F07CD34C ();
// 0x000003D0 System.Void easyar.Detail::easyar_FeedbackFrameSink_handle(System.IntPtr,System.IntPtr)
extern void Detail_easyar_FeedbackFrameSink_handle_m0982D545506B72DB5100C73CD85C14AFD912AEC4 ();
// 0x000003D1 System.Void easyar.Detail::easyar_FeedbackFrameSink__dtor(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSink__dtor_mCC50B2BA623A2520D00809EC1FBE846983C7E943 ();
// 0x000003D2 System.Void easyar.Detail::easyar_FeedbackFrameSink__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameSink__retain_m94E4FD959D689BD4B4AEE7A117FF56EAA69043AB ();
// 0x000003D3 System.IntPtr easyar.Detail::easyar_FeedbackFrameSink__typeName(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSink__typeName_m6986CA386843FCE1FDD4A953FBAE5868F67626E9 ();
// 0x000003D4 System.Void easyar.Detail::easyar_FeedbackFrameSource_setHandler(System.IntPtr,easyar.Detail_OptionalOfFunctorOfVoidFromFeedbackFrame)
extern void Detail_easyar_FeedbackFrameSource_setHandler_m8A48547729EA48F2342C82205C0AC83F9F87D5B3 ();
// 0x000003D5 System.Void easyar.Detail::easyar_FeedbackFrameSource_connect(System.IntPtr,System.IntPtr)
extern void Detail_easyar_FeedbackFrameSource_connect_mD9C3552E5A6735D8E1E147D8B49D6321F5878C8B ();
// 0x000003D6 System.Void easyar.Detail::easyar_FeedbackFrameSource_disconnect(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSource_disconnect_m19AB8283EFC90B01227D0588805155DD154725CD ();
// 0x000003D7 System.Void easyar.Detail::easyar_FeedbackFrameSource__dtor(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSource__dtor_m82E1278A8C104EA5615A6124797CDB892039D784 ();
// 0x000003D8 System.Void easyar.Detail::easyar_FeedbackFrameSource__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameSource__retain_m9F20F0E00AFE6ECB579A770825B155C13619A6EC ();
// 0x000003D9 System.IntPtr easyar.Detail::easyar_FeedbackFrameSource__typeName(System.IntPtr)
extern void Detail_easyar_FeedbackFrameSource__typeName_mC7A2586731DEE463AD8295AFA5B1E507F8F38D70 ();
// 0x000003DA System.Void easyar.Detail::easyar_InputFrameFork_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameFork_input_m75BC54404C49E9AD3E58CCD20DBEF0CF6EA4CF9D ();
// 0x000003DB System.Void easyar.Detail::easyar_InputFrameFork_output(System.IntPtr,System.Int32,System.IntPtr&)
extern void Detail_easyar_InputFrameFork_output_m429A4BDA180260BD08F3E37A3ED0182A254D63BA ();
// 0x000003DC System.Int32 easyar.Detail::easyar_InputFrameFork_outputCount(System.IntPtr)
extern void Detail_easyar_InputFrameFork_outputCount_m68DE7C184C49D978EA827A33D191B82B8B348A46 ();
// 0x000003DD System.Void easyar.Detail::easyar_InputFrameFork_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_InputFrameFork_create_mB51397DEB0407EE85AB06C0783E25614CE05E6CA ();
// 0x000003DE System.Void easyar.Detail::easyar_InputFrameFork__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameFork__dtor_mB11C14E73CF106599808C81A369E7A05C3CA8CB5 ();
// 0x000003DF System.Void easyar.Detail::easyar_InputFrameFork__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameFork__retain_m3A472A2C7B91F3097EE7604CFA9E932CAB6FF4AB ();
// 0x000003E0 System.IntPtr easyar.Detail::easyar_InputFrameFork__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameFork__typeName_m69749BC507EE390CDC9F807FA4C737CD89139C7D ();
// 0x000003E1 System.Void easyar.Detail::easyar_OutputFrameFork_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameFork_input_mC00F10B4AEB705776080E62C0E94BBB294F4CDA9 ();
// 0x000003E2 System.Void easyar.Detail::easyar_OutputFrameFork_output(System.IntPtr,System.Int32,System.IntPtr&)
extern void Detail_easyar_OutputFrameFork_output_m941EFB0A715D67DE571A0820B94F35DAC01603C6 ();
// 0x000003E3 System.Int32 easyar.Detail::easyar_OutputFrameFork_outputCount(System.IntPtr)
extern void Detail_easyar_OutputFrameFork_outputCount_m63FB584C37220C95D2412C1C3C2F5F391B2CC0EE ();
// 0x000003E4 System.Void easyar.Detail::easyar_OutputFrameFork_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_OutputFrameFork_create_mD82771B6110F9BA1C1497F9C3BC208A5576A6826 ();
// 0x000003E5 System.Void easyar.Detail::easyar_OutputFrameFork__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameFork__dtor_mF16AA7F272B4FCB3E34B481634F60ED1F039D843 ();
// 0x000003E6 System.Void easyar.Detail::easyar_OutputFrameFork__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameFork__retain_m1238873C136FD8944AADE9AA9EAF9B33617E50FE ();
// 0x000003E7 System.IntPtr easyar.Detail::easyar_OutputFrameFork__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameFork__typeName_mE0DFD6B0F498ECE18E0EFE4E08EBFB07EA2A0346 ();
// 0x000003E8 System.Void easyar.Detail::easyar_OutputFrameJoin_input(System.IntPtr,System.Int32,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin_input_m0768708F703E20C97E3D7EC578E8192A91C6888B ();
// 0x000003E9 System.Void easyar.Detail::easyar_OutputFrameJoin_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin_output_m54D5B811DE78F5099A705994FB364DAB61E83471 ();
// 0x000003EA System.Int32 easyar.Detail::easyar_OutputFrameJoin_inputCount(System.IntPtr)
extern void Detail_easyar_OutputFrameJoin_inputCount_m78D1BCB78147ABE386567B33C5BF91C1593DFAE0 ();
// 0x000003EB System.Void easyar.Detail::easyar_OutputFrameJoin_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin_create_mDCAE2C3D9AC5D0531050D779587F1EB9C1E4700F ();
// 0x000003EC System.Void easyar.Detail::easyar_OutputFrameJoin_createWithJoiner(System.Int32,easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin_createWithJoiner_m6D78589C5C7066DA0BCF1002DBBA201A43BCA5F8 ();
// 0x000003ED System.Void easyar.Detail::easyar_OutputFrameJoin__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameJoin__dtor_m4F2D7CBCFD9A941EFC9B0102430C407FA9758278 ();
// 0x000003EE System.Void easyar.Detail::easyar_OutputFrameJoin__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameJoin__retain_m9F4E6F5C3BC95E72296D0D37E7F4994CBC294DBB ();
// 0x000003EF System.IntPtr easyar.Detail::easyar_OutputFrameJoin__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameJoin__typeName_m941521B95BB435C4034C11F6F35D21A6A958C382 ();
// 0x000003F0 System.Void easyar.Detail::easyar_FeedbackFrameFork_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameFork_input_mAC1715B80AE5C0AE1B179C32290457E92A5297E6 ();
// 0x000003F1 System.Void easyar.Detail::easyar_FeedbackFrameFork_output(System.IntPtr,System.Int32,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameFork_output_mB8A98E389A093F8AB8E1FD87C905AAA5F5121D52 ();
// 0x000003F2 System.Int32 easyar.Detail::easyar_FeedbackFrameFork_outputCount(System.IntPtr)
extern void Detail_easyar_FeedbackFrameFork_outputCount_m6678A74BE0DB8C53E2D221CC3F3781835481C9CF ();
// 0x000003F3 System.Void easyar.Detail::easyar_FeedbackFrameFork_create(System.Int32,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameFork_create_m41022ECDD66B9EA57FF05B64DFC0C2EF434EFA36 ();
// 0x000003F4 System.Void easyar.Detail::easyar_FeedbackFrameFork__dtor(System.IntPtr)
extern void Detail_easyar_FeedbackFrameFork__dtor_m1B9151B47EA7ECEF71B9543F4FD4A72E00455A5E ();
// 0x000003F5 System.Void easyar.Detail::easyar_FeedbackFrameFork__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrameFork__retain_m2A1A5C9C82E121361FB8637463016FDF59F1B4E7 ();
// 0x000003F6 System.IntPtr easyar.Detail::easyar_FeedbackFrameFork__typeName(System.IntPtr)
extern void Detail_easyar_FeedbackFrameFork__typeName_mF66F90742986FC0D53449EFF8E78E8107B85E9C3 ();
// 0x000003F7 System.Void easyar.Detail::easyar_InputFrameThrottler_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler_input_m65211FCF9DE14C6D7D7E82F52C0E23F8AD9A990D ();
// 0x000003F8 System.Int32 easyar.Detail::easyar_InputFrameThrottler_bufferRequirement(System.IntPtr)
extern void Detail_easyar_InputFrameThrottler_bufferRequirement_m2E646DB937AB492C9B7DBEC86B2A9644CCF78BD1 ();
// 0x000003F9 System.Void easyar.Detail::easyar_InputFrameThrottler_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler_output_mD6373EC8D5094D0FC5E2A393CD00AD30EF008A0B ();
// 0x000003FA System.Void easyar.Detail::easyar_InputFrameThrottler_signalInput(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler_signalInput_mE5EAA4C5514CD8514E32B7DAE2C5A5BC6261C27E ();
// 0x000003FB System.Void easyar.Detail::easyar_InputFrameThrottler_create(System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler_create_m0B278A373DD4CC90C4CD6631E4994675026E4C13 ();
// 0x000003FC System.Void easyar.Detail::easyar_InputFrameThrottler__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameThrottler__dtor_m7C1FC92C517D18A719681FAC99432845F65AFE30 ();
// 0x000003FD System.Void easyar.Detail::easyar_InputFrameThrottler__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameThrottler__retain_m25FF403486FA01412AA911474D7B319CFC469C98 ();
// 0x000003FE System.IntPtr easyar.Detail::easyar_InputFrameThrottler__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameThrottler__typeName_m283A80FC07D5BB13A50E9863771D5D325280BD91 ();
// 0x000003FF System.Void easyar.Detail::easyar_OutputFrameBuffer_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameBuffer_input_m1B5DD96A983EF14E1EAD138966ABFC225015BC98 ();
// 0x00000400 System.Int32 easyar.Detail::easyar_OutputFrameBuffer_bufferRequirement(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer_bufferRequirement_m3D5C8D2B0129F57D5B495BAB426D9A4A70EE5EFF ();
// 0x00000401 System.Void easyar.Detail::easyar_OutputFrameBuffer_signalOutput(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameBuffer_signalOutput_m71595D5F111E0F7D2250E80F752930D002F33339 ();
// 0x00000402 System.Void easyar.Detail::easyar_OutputFrameBuffer_peek(System.IntPtr,easyar.Detail_OptionalOfOutputFrame&)
extern void Detail_easyar_OutputFrameBuffer_peek_mC50F8A96D5661C0AEF8145600AE536AC24340E22 ();
// 0x00000403 System.Void easyar.Detail::easyar_OutputFrameBuffer_create(System.IntPtr&)
extern void Detail_easyar_OutputFrameBuffer_create_mAC9D3AE6E2AA90D99DFC4C0AF9509C435FB43FB9 ();
// 0x00000404 System.Void easyar.Detail::easyar_OutputFrameBuffer_pause(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer_pause_m408D1C3C0F0BA969B61120E1A27F35D000B7BA38 ();
// 0x00000405 System.Void easyar.Detail::easyar_OutputFrameBuffer_resume(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer_resume_mCCC035DC74DDB95B39F37D55E9B642F15889B459 ();
// 0x00000406 System.Void easyar.Detail::easyar_OutputFrameBuffer__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer__dtor_m10C15480E9086667BCB3B24F8CC76B125BD3B792 ();
// 0x00000407 System.Void easyar.Detail::easyar_OutputFrameBuffer__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrameBuffer__retain_mEDA064591BCFEA3053D7E18EA6F64D9C07C20515 ();
// 0x00000408 System.IntPtr easyar.Detail::easyar_OutputFrameBuffer__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrameBuffer__typeName_m09378AC6E0A12FDDF25C44439D0B949722F21FEB ();
// 0x00000409 System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToOutputFrameAdapter_input_mD970AEE3CCD0E1B15ACEA7FDAA3DD112EB8BF485 ();
// 0x0000040A System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToOutputFrameAdapter_output_m06E289CD367F8A277B9FACC8B3F5FCC1F7601885 ();
// 0x0000040B System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter_create(System.IntPtr&)
extern void Detail_easyar_InputFrameToOutputFrameAdapter_create_m9149D4611D7CDA468D6A3F7357D75B0F563CCCEB ();
// 0x0000040C System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameToOutputFrameAdapter__dtor_mB0D4FDBC03407716A16020E2A82F7951CC25CBAE ();
// 0x0000040D System.Void easyar.Detail::easyar_InputFrameToOutputFrameAdapter__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToOutputFrameAdapter__retain_m9100C567E55CE2BC031D743F19C6C5B62872179A ();
// 0x0000040E System.IntPtr easyar.Detail::easyar_InputFrameToOutputFrameAdapter__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameToOutputFrameAdapter__typeName_m089066F40946FEFF95A7EF5CC525978677D372F9 ();
// 0x0000040F System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_input(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_input_m177440C009DA0005166A9230A5CCBCABD03F21C6 ();
// 0x00000410 System.Int32 easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_bufferRequirement(System.IntPtr)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_bufferRequirement_m6AD41E0AB5B2676E305322C57F5C209D05E61B76 ();
// 0x00000411 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_sideInput(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_sideInput_mEE40448474F4A9986C9E41EA6FC2616B10A0E72F ();
// 0x00000412 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_output(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_output_m5F58F4C79E660F0911DD240E98362E795952B1FE ();
// 0x00000413 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter_create(System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter_create_m106F4F067592A5F43914E086AFAE8213048FA963 ();
// 0x00000414 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter__dtor(System.IntPtr)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter__dtor_m3DC1FB09CAEC44DD294CE4F95CA5037B54880B0F ();
// 0x00000415 System.Void easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter__retain_m079AE236916EEB2881CF3786954740AECE6FB405 ();
// 0x00000416 System.IntPtr easyar.Detail::easyar_InputFrameToFeedbackFrameAdapter__typeName(System.IntPtr)
extern void Detail_easyar_InputFrameToFeedbackFrameAdapter__typeName_mAF7EEFBC5E0F1C40E331F4D987C4C97A35A84A2B ();
// 0x00000417 System.Int32 easyar.Detail::easyar_InputFrame_index(System.IntPtr)
extern void Detail_easyar_InputFrame_index_m3243592DA7D0D75CDDDB3C0637E4E4179BF0424D ();
// 0x00000418 System.Void easyar.Detail::easyar_InputFrame_image(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame_image_m0D214416531DEA5D339DB355C115691C76C5750A ();
// 0x00000419 System.Boolean easyar.Detail::easyar_InputFrame_hasCameraParameters(System.IntPtr)
extern void Detail_easyar_InputFrame_hasCameraParameters_m4F6E196B8FF2D50488110F200AC61FD610994AF3 ();
// 0x0000041A System.Void easyar.Detail::easyar_InputFrame_cameraParameters(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame_cameraParameters_mAB3D9977469E787F1F01561476768A4850145E60 ();
// 0x0000041B System.Boolean easyar.Detail::easyar_InputFrame_hasTemporalInformation(System.IntPtr)
extern void Detail_easyar_InputFrame_hasTemporalInformation_mE48872AD9B6F7DB9838B679315F834BA25ECED0C ();
// 0x0000041C System.Double easyar.Detail::easyar_InputFrame_timestamp(System.IntPtr)
extern void Detail_easyar_InputFrame_timestamp_m78D4C6629D1AB7F480B386C6BB166D666DFC7884 ();
// 0x0000041D System.Boolean easyar.Detail::easyar_InputFrame_hasSpatialInformation(System.IntPtr)
extern void Detail_easyar_InputFrame_hasSpatialInformation_m5ABDD29E52536A4B8B292DA957453B9F00C9ACD7 ();
// 0x0000041E easyar.Matrix44F easyar.Detail::easyar_InputFrame_cameraTransform(System.IntPtr)
extern void Detail_easyar_InputFrame_cameraTransform_mB892D144F251BFEB80717701B7813AF49BFE73F5 ();
// 0x0000041F easyar.MotionTrackingStatus easyar.Detail::easyar_InputFrame_trackingStatus(System.IntPtr)
extern void Detail_easyar_InputFrame_trackingStatus_m37E36FDD400A0F331A119696F220AAF7D63CB61B ();
// 0x00000420 System.Void easyar.Detail::easyar_InputFrame_create(System.IntPtr,System.IntPtr,System.Double,easyar.Matrix44F,easyar.MotionTrackingStatus,System.IntPtr&)
extern void Detail_easyar_InputFrame_create_mDF4B13BB5CB4AB5A494A7F89132980354D0E12E9 ();
// 0x00000421 System.Void easyar.Detail::easyar_InputFrame_createWithImageAndCameraParametersAndTemporal(System.IntPtr,System.IntPtr,System.Double,System.IntPtr&)
extern void Detail_easyar_InputFrame_createWithImageAndCameraParametersAndTemporal_m7D33F69B9F7C163D5B6BFE571A10138526623722 ();
// 0x00000422 System.Void easyar.Detail::easyar_InputFrame_createWithImageAndCameraParameters(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame_createWithImageAndCameraParameters_mF2FCE0AD798A4338CB87AE64AC53D21ED30D12F6 ();
// 0x00000423 System.Void easyar.Detail::easyar_InputFrame_createWithImage(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame_createWithImage_mB317E27D88AF2D7986E60E363922A0BAE2C239CF ();
// 0x00000424 System.Void easyar.Detail::easyar_InputFrame__dtor(System.IntPtr)
extern void Detail_easyar_InputFrame__dtor_m4765F8A2AF09723272BC13ADCD847C4724E74B4C ();
// 0x00000425 System.Void easyar.Detail::easyar_InputFrame__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_InputFrame__retain_m5F8E85D839FC2ED449BAF26D0810311816BE7919 ();
// 0x00000426 System.IntPtr easyar.Detail::easyar_InputFrame__typeName(System.IntPtr)
extern void Detail_easyar_InputFrame__typeName_mA9D7391CFA051BD60C235CAE7EF65B2D6990288D ();
// 0x00000427 System.Void easyar.Detail::easyar_FrameFilterResult__dtor(System.IntPtr)
extern void Detail_easyar_FrameFilterResult__dtor_mED572B57C0D0506958F9826F65B54EA3B9ED1AEC ();
// 0x00000428 System.Void easyar.Detail::easyar_FrameFilterResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FrameFilterResult__retain_m89D8345E6C7934C165F2ACE9C47AD965BFBB304B ();
// 0x00000429 System.IntPtr easyar.Detail::easyar_FrameFilterResult__typeName(System.IntPtr)
extern void Detail_easyar_FrameFilterResult__typeName_mC195CAF147275EE943D5A222AC2C2F81017E553C ();
// 0x0000042A System.Void easyar.Detail::easyar_OutputFrame__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrame__ctor_m63BB4D92CF86C11368301C91F45F4FE7689B7B17 ();
// 0x0000042B System.Int32 easyar.Detail::easyar_OutputFrame_index(System.IntPtr)
extern void Detail_easyar_OutputFrame_index_mFD6B23253DF400D8B71DFCB76FC4F9AF18A455DB ();
// 0x0000042C System.Void easyar.Detail::easyar_OutputFrame_inputFrame(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrame_inputFrame_mA586757588F739273C0DB6AF8E9D085EF92C5BD4 ();
// 0x0000042D System.Void easyar.Detail::easyar_OutputFrame_results(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrame_results_m6ECE0B61B691E9B8BCFF808770DDCC997E3A49DE ();
// 0x0000042E System.Void easyar.Detail::easyar_OutputFrame__dtor(System.IntPtr)
extern void Detail_easyar_OutputFrame__dtor_m4346341071401BF01C2F86D320FFF9A4D14899E5 ();
// 0x0000042F System.Void easyar.Detail::easyar_OutputFrame__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_OutputFrame__retain_m714A8E413CD2BB19F509C80681675E413F8B02CB ();
// 0x00000430 System.IntPtr easyar.Detail::easyar_OutputFrame__typeName(System.IntPtr)
extern void Detail_easyar_OutputFrame__typeName_m71048A27956E9EDA281C922F0307897177181B09 ();
// 0x00000431 System.Void easyar.Detail::easyar_FeedbackFrame__ctor(System.IntPtr,easyar.Detail_OptionalOfOutputFrame,System.IntPtr&)
extern void Detail_easyar_FeedbackFrame__ctor_m711F9FBBA23A5DFC49360DDAFF0240CFF98095AA ();
// 0x00000432 System.Void easyar.Detail::easyar_FeedbackFrame_inputFrame(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrame_inputFrame_m89CAFE94220EF71430922FA4D84DFA1DFAD5717F ();
// 0x00000433 System.Void easyar.Detail::easyar_FeedbackFrame_previousOutputFrame(System.IntPtr,easyar.Detail_OptionalOfOutputFrame&)
extern void Detail_easyar_FeedbackFrame_previousOutputFrame_m857403B6A8564F4187D8353B0428709B5A05B1A8 ();
// 0x00000434 System.Void easyar.Detail::easyar_FeedbackFrame__dtor(System.IntPtr)
extern void Detail_easyar_FeedbackFrame__dtor_m87C04D9F9E1F01A52DE58F5E820D841F43414859 ();
// 0x00000435 System.Void easyar.Detail::easyar_FeedbackFrame__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_FeedbackFrame__retain_mB153A7E0A433D44C04E06FECA57DA0DAEF87BBA5 ();
// 0x00000436 System.IntPtr easyar.Detail::easyar_FeedbackFrame__typeName(System.IntPtr)
extern void Detail_easyar_FeedbackFrame__typeName_mBA75E551CEACE5ED82466EA9194A7F5DB514A83C ();
// 0x00000437 System.Int32 easyar.Detail::easyar_Target_runtimeID(System.IntPtr)
extern void Detail_easyar_Target_runtimeID_m40E148A547C76112516FEF60B5C3AF8C53A80AE1 ();
// 0x00000438 System.Void easyar.Detail::easyar_Target_uid(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Target_uid_m17EC6C81561E460A05F411CD80FBC59F3A395953 ();
// 0x00000439 System.Void easyar.Detail::easyar_Target_name(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Target_name_m4FE0FDB35A58ADFC9293D87E6E007A0C852029DC ();
// 0x0000043A System.Void easyar.Detail::easyar_Target_setName(System.IntPtr,System.IntPtr)
extern void Detail_easyar_Target_setName_m6E510557F2A8F97A8A5FCE6E289FFC41DF0A5EB9 ();
// 0x0000043B System.Void easyar.Detail::easyar_Target_meta(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Target_meta_mD283AC8B5A2B82865A284EDBC38E96371AEF5D64 ();
// 0x0000043C System.Void easyar.Detail::easyar_Target_setMeta(System.IntPtr,System.IntPtr)
extern void Detail_easyar_Target_setMeta_mC8F76141A764417CAA1C00C7C8DD55C26BDE636E ();
// 0x0000043D System.Void easyar.Detail::easyar_Target__dtor(System.IntPtr)
extern void Detail_easyar_Target__dtor_mFB3C215BD0C794B07FD49CD4D6EE46AC9BE87953 ();
// 0x0000043E System.Void easyar.Detail::easyar_Target__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_Target__retain_mDE6A6C16DAEB5E818421159326E125542EEFFB64 ();
// 0x0000043F System.IntPtr easyar.Detail::easyar_Target__typeName(System.IntPtr)
extern void Detail_easyar_Target__typeName_m4C18126ACD072A41BCC350211C910F89DC6A5B40 ();
// 0x00000440 System.Void easyar.Detail::easyar_TargetInstance__ctor(System.IntPtr&)
extern void Detail_easyar_TargetInstance__ctor_m967FB8B3754300B53EDF1DD57CD54CF781CDD479 ();
// 0x00000441 easyar.TargetStatus easyar.Detail::easyar_TargetInstance_status(System.IntPtr)
extern void Detail_easyar_TargetInstance_status_m1089E06D57B2659648A635608B3ED4D74AC28DF1 ();
// 0x00000442 System.Void easyar.Detail::easyar_TargetInstance_target(System.IntPtr,easyar.Detail_OptionalOfTarget&)
extern void Detail_easyar_TargetInstance_target_m6EB1A3D17118A2E6E31C0C99E3F06B9547595CB5 ();
// 0x00000443 easyar.Matrix44F easyar.Detail::easyar_TargetInstance_pose(System.IntPtr)
extern void Detail_easyar_TargetInstance_pose_mE07869F20E8BB11C608A04A901BA0ACE455158A4 ();
// 0x00000444 System.Void easyar.Detail::easyar_TargetInstance__dtor(System.IntPtr)
extern void Detail_easyar_TargetInstance__dtor_m9F9120FE38A4A7D1C396EDA517087306FAC1E9A5 ();
// 0x00000445 System.Void easyar.Detail::easyar_TargetInstance__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TargetInstance__retain_mA9F729F3B08C7C7FA5CC43EC1D267ED75675A524 ();
// 0x00000446 System.IntPtr easyar.Detail::easyar_TargetInstance__typeName(System.IntPtr)
extern void Detail_easyar_TargetInstance__typeName_m6D577900C401C49A66F763E4A065E1ED89602467 ();
// 0x00000447 System.Void easyar.Detail::easyar_TargetTrackerResult_targetInstances(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TargetTrackerResult_targetInstances_m6519E45963F4252050AB694A834D5E2F481179C9 ();
// 0x00000448 System.Void easyar.Detail::easyar_TargetTrackerResult_setTargetInstances(System.IntPtr,System.IntPtr)
extern void Detail_easyar_TargetTrackerResult_setTargetInstances_m7B5A7B7A88A45604AC258A6768244BEC829E5526 ();
// 0x00000449 System.Void easyar.Detail::easyar_TargetTrackerResult__dtor(System.IntPtr)
extern void Detail_easyar_TargetTrackerResult__dtor_m806FA78A2C963364AE27BDE0D0D272BB0F2D01F5 ();
// 0x0000044A System.Void easyar.Detail::easyar_TargetTrackerResult__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TargetTrackerResult__retain_m12C7BC9F07F41167D4E051A590B2A34BB816591F ();
// 0x0000044B System.IntPtr easyar.Detail::easyar_TargetTrackerResult__typeName(System.IntPtr)
extern void Detail_easyar_TargetTrackerResult__typeName_mDFCC2959AF5364459E58FE30A98716CEB7366631 ();
// 0x0000044C System.Void easyar.Detail::easyar_castTargetTrackerResultToFrameFilterResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_castTargetTrackerResultToFrameFilterResult_mA17189C0166007845CB28608B832212CD93BB942 ();
// 0x0000044D System.Void easyar.Detail::easyar_tryCastFrameFilterResultToTargetTrackerResult(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_tryCastFrameFilterResultToTargetTrackerResult_m1304D0AE11AC585574500CCE67E8893B5618231A ();
// 0x0000044E System.Int32 easyar.Detail::easyar_TextureId_getInt(System.IntPtr)
extern void Detail_easyar_TextureId_getInt_mD460B37428437E8A8168671B96868A1DCD980F82 ();
// 0x0000044F System.IntPtr easyar.Detail::easyar_TextureId_getPointer(System.IntPtr)
extern void Detail_easyar_TextureId_getPointer_m2D02947867FA695BD0B9C004C7B1259A8C350BC5 ();
// 0x00000450 System.Void easyar.Detail::easyar_TextureId_fromInt(System.Int32,System.IntPtr&)
extern void Detail_easyar_TextureId_fromInt_m9CB4A21DDACB0FECE177AC970885BFFACB0D3463 ();
// 0x00000451 System.Void easyar.Detail::easyar_TextureId_fromPointer(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TextureId_fromPointer_m5ADB716FEA2AB9D10BC94F671CDA58D51F89F9F4 ();
// 0x00000452 System.Void easyar.Detail::easyar_TextureId__dtor(System.IntPtr)
extern void Detail_easyar_TextureId__dtor_mB9B28EEECC858A7DAB8F99F359EEF2B9E7C60905 ();
// 0x00000453 System.Void easyar.Detail::easyar_TextureId__retain(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_TextureId__retain_m8DE4F3DBB951DE7F7491FA394590BE25DBDDC639 ();
// 0x00000454 System.IntPtr easyar.Detail::easyar_TextureId__typeName(System.IntPtr)
extern void Detail_easyar_TextureId__typeName_mC521B901EB1A153FE537374F63B51C5FD945CC5D ();
// 0x00000455 System.Void easyar.Detail::easyar_ListOfVec3F__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfVec3F__ctor_m2C6264711D3BA58FB267057842F708412FEAE528 ();
// 0x00000456 System.Void easyar.Detail::easyar_ListOfVec3F__dtor(System.IntPtr)
extern void Detail_easyar_ListOfVec3F__dtor_m24B6C30D5D3D61C41B133C24591C5BD75E5AF575 ();
// 0x00000457 System.Void easyar.Detail::easyar_ListOfVec3F_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfVec3F_copy_mE455F41D7DEA42682F4AC9987752C65B9E586CB8 ();
// 0x00000458 System.Int32 easyar.Detail::easyar_ListOfVec3F_size(System.IntPtr)
extern void Detail_easyar_ListOfVec3F_size_m8459FD38D7D5FFAC0DA603E60B5619F0736EDC09 ();
// 0x00000459 easyar.Vec3F easyar.Detail::easyar_ListOfVec3F_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfVec3F_at_mA419D519D12177CCB04FAB19934C7A6C0138E3E8 ();
// 0x0000045A System.Void easyar.Detail::easyar_ListOfTargetInstance__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfTargetInstance__ctor_mB318A85E43B0E5630A7822C18A82C487D7D8ACB4 ();
// 0x0000045B System.Void easyar.Detail::easyar_ListOfTargetInstance__dtor(System.IntPtr)
extern void Detail_easyar_ListOfTargetInstance__dtor_m908DFE44B72C6E366DA386D50DEEECFAFEB29A7D ();
// 0x0000045C System.Void easyar.Detail::easyar_ListOfTargetInstance_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfTargetInstance_copy_m72E7E2564E0C27DFF1F3BD3B432F2B4C23C21FBB ();
// 0x0000045D System.Int32 easyar.Detail::easyar_ListOfTargetInstance_size(System.IntPtr)
extern void Detail_easyar_ListOfTargetInstance_size_mDEBE26E1E430668734F178AE3B92A13A1C5DCA2C ();
// 0x0000045E System.IntPtr easyar.Detail::easyar_ListOfTargetInstance_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfTargetInstance_at_m686D837366AFBD547A38F33A7CE6BFBB78FB6018 ();
// 0x0000045F System.Void easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult__ctor_m5DCA08EF698742F8176C00AA32BC8D88398324E1 ();
// 0x00000460 System.Void easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult__dtor(System.IntPtr)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult__dtor_m164270C7201ED2D4F6D0DE71505D0542035FB854 ();
// 0x00000461 System.Void easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult_copy_m4F6D461B4F5626D14DBB86E23C3DDA335AA4FD9C ();
// 0x00000462 System.Int32 easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult_size(System.IntPtr)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult_size_m36ADBF6235BAC565A63B1DC799D3D677570EAFB9 ();
// 0x00000463 easyar.Detail_OptionalOfFrameFilterResult easyar.Detail::easyar_ListOfOptionalOfFrameFilterResult_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfOptionalOfFrameFilterResult_at_mB2A21F8D8BD97366107C602471406FCF7CD54B15 ();
// 0x00000464 System.Void easyar.Detail::easyar_ListOfTarget__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfTarget__ctor_m1557530754C8F775A43C7C701A9274908A6DE0B0 ();
// 0x00000465 System.Void easyar.Detail::easyar_ListOfTarget__dtor(System.IntPtr)
extern void Detail_easyar_ListOfTarget__dtor_mC27435F886F59BC431C261A654D74F43F9753CC0 ();
// 0x00000466 System.Void easyar.Detail::easyar_ListOfTarget_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfTarget_copy_m4F757586CD7582CEF18875B468BE067C67DFC77B ();
// 0x00000467 System.Int32 easyar.Detail::easyar_ListOfTarget_size(System.IntPtr)
extern void Detail_easyar_ListOfTarget_size_mF8EC23E748EB3BF2CD236CE78496461161FEC5BA ();
// 0x00000468 System.IntPtr easyar.Detail::easyar_ListOfTarget_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfTarget_at_mD130B5A468F1E22C0C9359EA300038CE9F8DC4D8 ();
// 0x00000469 System.Void easyar.Detail::easyar_ListOfImage__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfImage__ctor_mC851D89FB65808B4499B1256230C49ACA76E0B08 ();
// 0x0000046A System.Void easyar.Detail::easyar_ListOfImage__dtor(System.IntPtr)
extern void Detail_easyar_ListOfImage__dtor_m274DA9C1A3A30133DBAF90AF0542300AF0B0709F ();
// 0x0000046B System.Void easyar.Detail::easyar_ListOfImage_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfImage_copy_m8C3E4C8BDCE9852697D7442226A8973101781512 ();
// 0x0000046C System.Int32 easyar.Detail::easyar_ListOfImage_size(System.IntPtr)
extern void Detail_easyar_ListOfImage_size_mD27D9D9386B9D5CD26BBFF18FDD2A427A0B7BDFD ();
// 0x0000046D System.IntPtr easyar.Detail::easyar_ListOfImage_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfImage_at_m883180C6A851A167587A2D934659192C1B3304C8 ();
// 0x0000046E System.Void easyar.Detail::easyar_ListOfBlockInfo__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfBlockInfo__ctor_mB2E363F4C8B8BCA43EF45A64BB9F4AC5F789D7B9 ();
// 0x0000046F System.Void easyar.Detail::easyar_ListOfBlockInfo__dtor(System.IntPtr)
extern void Detail_easyar_ListOfBlockInfo__dtor_m5FB05603BCFA80EBB2F3A287B303DB5455054B98 ();
// 0x00000470 System.Void easyar.Detail::easyar_ListOfBlockInfo_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfBlockInfo_copy_m1DD5151A30DD34D0DD822958343FCF155DFE9D57 ();
// 0x00000471 System.Int32 easyar.Detail::easyar_ListOfBlockInfo_size(System.IntPtr)
extern void Detail_easyar_ListOfBlockInfo_size_mDA47924CD19B92A8700725D24F4685126379431B ();
// 0x00000472 easyar.BlockInfo easyar.Detail::easyar_ListOfBlockInfo_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfBlockInfo_at_m606BC89C9176055146AFA19AB9BF2D60DAD78FFD ();
// 0x00000473 System.Void easyar.Detail::easyar_ListOfPlaneData__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfPlaneData__ctor_m6DBDECE5DE1C8AA5064D1FE5D49E9AF0FB2FD438 ();
// 0x00000474 System.Void easyar.Detail::easyar_ListOfPlaneData__dtor(System.IntPtr)
extern void Detail_easyar_ListOfPlaneData__dtor_mDF452DB0EC1CC1427E85D2B2860ADA63EA43C99C ();
// 0x00000475 System.Void easyar.Detail::easyar_ListOfPlaneData_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfPlaneData_copy_mFE04CE16A4E35C65A6E6FEA3F81EA9918E1C1C79 ();
// 0x00000476 System.Int32 easyar.Detail::easyar_ListOfPlaneData_size(System.IntPtr)
extern void Detail_easyar_ListOfPlaneData_size_m4772B17C49818CF6271663B03129DCBBE3A664B7 ();
// 0x00000477 System.IntPtr easyar.Detail::easyar_ListOfPlaneData_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfPlaneData_at_mB6DF289E5577FDBBA6DA6CE41752175AF41A5FBD ();
// 0x00000478 System.Void easyar.Detail::easyar_ListOfOutputFrame__ctor(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfOutputFrame__ctor_m9E0EC0098FDD4BAE8098AB4A002C55CBFF6B4BED ();
// 0x00000479 System.Void easyar.Detail::easyar_ListOfOutputFrame__dtor(System.IntPtr)
extern void Detail_easyar_ListOfOutputFrame__dtor_m7C25B0C1C59F33F7A3895762ED2FA2EC4E754D85 ();
// 0x0000047A System.Void easyar.Detail::easyar_ListOfOutputFrame_copy(System.IntPtr,System.IntPtr&)
extern void Detail_easyar_ListOfOutputFrame_copy_mDDAE1D429DBD3D5FD63ACD7AECC7E05AE497B877 ();
// 0x0000047B System.Int32 easyar.Detail::easyar_ListOfOutputFrame_size(System.IntPtr)
extern void Detail_easyar_ListOfOutputFrame_size_m89BA841C812446C9CCC393B81B57593D4A3D5AE3 ();
// 0x0000047C System.IntPtr easyar.Detail::easyar_ListOfOutputFrame_at(System.IntPtr,System.Int32)
extern void Detail_easyar_ListOfOutputFrame_at_mFBF82A04B5DC2DE6E0E6057AB91BF78CD5C2C10D ();
// 0x0000047D System.IntPtr easyar.Detail::String_to_c(easyar.Detail_AutoRelease,System.String)
extern void Detail_String_to_c_mEE7B62122F209B1ADA1C5DFDCF2CEFE537725894 ();
// 0x0000047E System.IntPtr easyar.Detail::String_to_c_inner(System.String)
extern void Detail_String_to_c_inner_m6F16B7735B8019203C9639EDBE08F09532940F7D ();
// 0x0000047F System.String easyar.Detail::String_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_String_from_c_mE654BDA106C4698A73E49A481889EDE05C70852D ();
// 0x00000480 System.String easyar.Detail::String_from_cstring(System.IntPtr)
extern void Detail_String_from_cstring_m134BC4E89C40AFF8003E80D40A1F6EECB29BEB0F ();
// 0x00000481 T easyar.Detail::Object_from_c(System.IntPtr,System.Func`2<System.IntPtr,System.IntPtr>)
// 0x00000482 TValue easyar.Detail::map(TKey,System.Func`2<TKey,TValue>)
// 0x00000483 System.Void easyar.Detail::FunctorOfVoid_func(System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoid_func_mF783D5E1A5CC1474AC7371278CABF14761C1737D ();
// 0x00000484 System.Void easyar.Detail::FunctorOfVoid_destroy(System.IntPtr)
extern void Detail_FunctorOfVoid_destroy_m0F5DC9905B8F43E6DC2E844556B0FF56E4BF67B6 ();
// 0x00000485 easyar.Detail_FunctorOfVoid easyar.Detail::FunctorOfVoid_to_c(System.Action)
extern void Detail_FunctorOfVoid_to_c_mC18BFB0946B870EA8CD32DDB8C1FDD7E70B44ED3 ();
// 0x00000486 System.IntPtr easyar.Detail::ListOfVec3F_to_c(easyar.Detail_AutoRelease,System.Collections.Generic.List`1<easyar.Vec3F>)
extern void Detail_ListOfVec3F_to_c_m3FCDAC235A082B1E0A5C31C61A109694172D75F7 ();
// 0x00000487 System.Collections.Generic.List`1<easyar.Vec3F> easyar.Detail::ListOfVec3F_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_ListOfVec3F_from_c_mC344D6C9A436549AA34BCEFB97904D37FBC332A5 ();
// 0x00000488 System.IntPtr easyar.Detail::ListOfTargetInstance_to_c(easyar.Detail_AutoRelease,System.Collections.Generic.List`1<easyar.TargetInstance>)
extern void Detail_ListOfTargetInstance_to_c_mFD579AA8AEA8CB00AFE3CE243C2F7E104F1DABED ();
// 0x00000489 System.Collections.Generic.List`1<easyar.TargetInstance> easyar.Detail::ListOfTargetInstance_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_ListOfTargetInstance_from_c_m50DD73E0D9BA3386FD096E1AB1FCB79676C55EE2 ();
// 0x0000048A System.IntPtr easyar.Detail::ListOfOptionalOfFrameFilterResult_to_c(easyar.Detail_AutoRelease,System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>>)
extern void Detail_ListOfOptionalOfFrameFilterResult_to_c_mD79D5A3D4AF7F60BBF7A97D2C659A32EEBBA07C7 ();
// 0x0000048B System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>> easyar.Detail::ListOfOptionalOfFrameFilterResult_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_ListOfOptionalOfFrameFilterResult_from_c_m3AC82D36FAA42F20BC2884224AC541208F550D15 ();
// 0x0000048C System.Void easyar.Detail::FunctorOfVoidFromOutputFrame_func(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromOutputFrame_func_mB6649CCC85AAE55815A216F223BCCFFE5253FEC0 ();
// 0x0000048D System.Void easyar.Detail::FunctorOfVoidFromOutputFrame_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromOutputFrame_destroy_mCCA2C33F06C260D5C48CE09D9E03133D4D0CD6DD ();
// 0x0000048E easyar.Detail_FunctorOfVoidFromOutputFrame easyar.Detail::FunctorOfVoidFromOutputFrame_to_c(System.Action`1<easyar.OutputFrame>)
extern void Detail_FunctorOfVoidFromOutputFrame_to_c_m79612E87D865E1B035F5BC8111321E60115C4787 ();
// 0x0000048F System.Void easyar.Detail::FunctorOfVoidFromTargetAndBool_func(System.IntPtr,System.IntPtr,System.Boolean,System.IntPtr&)
extern void Detail_FunctorOfVoidFromTargetAndBool_func_mFA51F1E153904EF00496BE23F062C9A12C273793 ();
// 0x00000490 System.Void easyar.Detail::FunctorOfVoidFromTargetAndBool_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromTargetAndBool_destroy_m6AA500E710A687EBB7C96D03D17E131BD94186FA ();
// 0x00000491 easyar.Detail_FunctorOfVoidFromTargetAndBool easyar.Detail::FunctorOfVoidFromTargetAndBool_to_c(System.Action`2<easyar.Target,System.Boolean>)
extern void Detail_FunctorOfVoidFromTargetAndBool_to_c_m10AFC3EDDDEFE2EE6F6DC64B4C6E93170A3F832F ();
// 0x00000492 System.IntPtr easyar.Detail::ListOfTarget_to_c(easyar.Detail_AutoRelease,System.Collections.Generic.List`1<easyar.Target>)
extern void Detail_ListOfTarget_to_c_mED50E60F2824008B97FB60DEC74C8EB086E79C53 ();
// 0x00000493 System.Collections.Generic.List`1<easyar.Target> easyar.Detail::ListOfTarget_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_ListOfTarget_from_c_m9C2827070C7229659B098728B0D80E4F4EDA274A ();
// 0x00000494 System.IntPtr easyar.Detail::ListOfImage_to_c(easyar.Detail_AutoRelease,System.Collections.Generic.List`1<easyar.Image>)
extern void Detail_ListOfImage_to_c_m9FF99F1C9FA7810EBE120B9E569B458B37DB800C ();
// 0x00000495 System.Collections.Generic.List`1<easyar.Image> easyar.Detail::ListOfImage_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_ListOfImage_from_c_m34F1F54F8DCD659A18A3D3D5977D3D5E1CFF6AAA ();
// 0x00000496 System.Void easyar.Detail::FunctorOfVoidFromCloudRecognizationResult_func(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromCloudRecognizationResult_func_m90154CEE7823B813A64107474E84C9D8E49F9FE8 ();
// 0x00000497 System.Void easyar.Detail::FunctorOfVoidFromCloudRecognizationResult_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mA6894D99F036B445918E01B88C818F63C208B1C1 ();
// 0x00000498 easyar.Detail_FunctorOfVoidFromCloudRecognizationResult easyar.Detail::FunctorOfVoidFromCloudRecognizationResult_to_c(System.Action`1<easyar.CloudRecognizationResult>)
extern void Detail_FunctorOfVoidFromCloudRecognizationResult_to_c_m978E39577A697CCE3687060E1690CD7B3F978982 ();
// 0x00000499 System.IntPtr easyar.Detail::ListOfBlockInfo_to_c(easyar.Detail_AutoRelease,System.Collections.Generic.List`1<easyar.BlockInfo>)
extern void Detail_ListOfBlockInfo_to_c_m8AFA65582FD918E3C7AA3709D46B88D9AA07628A ();
// 0x0000049A System.Collections.Generic.List`1<easyar.BlockInfo> easyar.Detail::ListOfBlockInfo_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_ListOfBlockInfo_from_c_m690AC5DA5FA5A91F2D05482C0C812D0E284260FC ();
// 0x0000049B System.Void easyar.Detail::FunctorOfVoidFromInputFrame_func(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromInputFrame_func_mA2CECDF41A63A69813C1E34E46FCE069F47C6427 ();
// 0x0000049C System.Void easyar.Detail::FunctorOfVoidFromInputFrame_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromInputFrame_destroy_mB73878C35844CC8471D0EF2A8B4A1FE0D3C6F878 ();
// 0x0000049D easyar.Detail_FunctorOfVoidFromInputFrame easyar.Detail::FunctorOfVoidFromInputFrame_to_c(System.Action`1<easyar.InputFrame>)
extern void Detail_FunctorOfVoidFromInputFrame_to_c_mF015CC51C5127C1F3858F895C08B32D6718833F8 ();
// 0x0000049E System.Void easyar.Detail::FunctorOfVoidFromCameraState_func(System.IntPtr,easyar.CameraState,System.IntPtr&)
extern void Detail_FunctorOfVoidFromCameraState_func_m7FCB6EEC40715D367C83D86227157A9E122568C9 ();
// 0x0000049F System.Void easyar.Detail::FunctorOfVoidFromCameraState_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromCameraState_destroy_mC70C7A72F6DBC20901D045EFF23EA2AF58A5BF41 ();
// 0x000004A0 easyar.Detail_FunctorOfVoidFromCameraState easyar.Detail::FunctorOfVoidFromCameraState_to_c(System.Action`1<easyar.CameraState>)
extern void Detail_FunctorOfVoidFromCameraState_to_c_mD100AB4BE829B4084A287F004B0953C4BD2F64E0 ();
// 0x000004A1 System.Void easyar.Detail::FunctorOfVoidFromPermissionStatusAndString_func(System.IntPtr,easyar.PermissionStatus,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromPermissionStatusAndString_func_m61FB5A520BF4BF61618A6036F4C225797196DB4D ();
// 0x000004A2 System.Void easyar.Detail::FunctorOfVoidFromPermissionStatusAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_mFB9A4F27091BABD5B3A3FE933C4003F0478BD7B5 ();
// 0x000004A3 easyar.Detail_FunctorOfVoidFromPermissionStatusAndString easyar.Detail::FunctorOfVoidFromPermissionStatusAndString_to_c(System.Action`2<easyar.PermissionStatus,System.String>)
extern void Detail_FunctorOfVoidFromPermissionStatusAndString_to_c_m7D7ED018B419B0711BB71E4680C092780BE15553 ();
// 0x000004A4 System.Void easyar.Detail::FunctorOfVoidFromLogLevelAndString_func(System.IntPtr,easyar.LogLevel,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromLogLevelAndString_func_m53946E55D87E33DA5FA36DC16B26E9B3DB0B439C ();
// 0x000004A5 System.Void easyar.Detail::FunctorOfVoidFromLogLevelAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromLogLevelAndString_destroy_mADE83DB6360391E4C933515D59EF3F73E4B0B08E ();
// 0x000004A6 easyar.Detail_FunctorOfVoidFromLogLevelAndString easyar.Detail::FunctorOfVoidFromLogLevelAndString_to_c(System.Action`2<easyar.LogLevel,System.String>)
extern void Detail_FunctorOfVoidFromLogLevelAndString_to_c_m1F047A303620D5747DE2EB5406CE3D7CBBF0153C ();
// 0x000004A7 System.Void easyar.Detail::FunctorOfVoidFromRecordStatusAndString_func(System.IntPtr,easyar.RecordStatus,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromRecordStatusAndString_func_mFA196EC005028814229B1F55F1E24665DF692553 ();
// 0x000004A8 System.Void easyar.Detail::FunctorOfVoidFromRecordStatusAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromRecordStatusAndString_destroy_m051F182F8E827A97DEC26087EDB6CD6616462981 ();
// 0x000004A9 easyar.Detail_FunctorOfVoidFromRecordStatusAndString easyar.Detail::FunctorOfVoidFromRecordStatusAndString_to_c(System.Action`2<easyar.RecordStatus,System.String>)
extern void Detail_FunctorOfVoidFromRecordStatusAndString_to_c_mF6D4E298A7316095168890C8AF7D59B4054E8961 ();
// 0x000004AA System.IntPtr easyar.Detail::ListOfPlaneData_to_c(easyar.Detail_AutoRelease,System.Collections.Generic.List`1<easyar.PlaneData>)
extern void Detail_ListOfPlaneData_to_c_mF1235606DF4055877099E9690AA242790FB8A1D4 ();
// 0x000004AB System.Collections.Generic.List`1<easyar.PlaneData> easyar.Detail::ListOfPlaneData_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_ListOfPlaneData_from_c_m7797045CAC0D0B89A65AE3BDB60924A61F92A9D7 ();
// 0x000004AC System.Void easyar.Detail::FunctorOfVoidFromBool_func(System.IntPtr,System.Boolean,System.IntPtr&)
extern void Detail_FunctorOfVoidFromBool_func_m8C959A83611A4385EE07D73B822F990C10B6EDF6 ();
// 0x000004AD System.Void easyar.Detail::FunctorOfVoidFromBool_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromBool_destroy_mC7D4393D0066F06286A66262A3B206C16F9A0A83 ();
// 0x000004AE easyar.Detail_FunctorOfVoidFromBool easyar.Detail::FunctorOfVoidFromBool_to_c(System.Action`1<System.Boolean>)
extern void Detail_FunctorOfVoidFromBool_to_c_m7E80875A16F9F2B4B865D8C76BE83CCC5E9559B1 ();
// 0x000004AF System.Void easyar.Detail::FunctorOfVoidFromBoolAndStringAndString_func(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromBoolAndStringAndString_func_mE08DFB80305F9D293A7D957A1B409629ED9B4396 ();
// 0x000004B0 System.Void easyar.Detail::FunctorOfVoidFromBoolAndStringAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_m1D50B3D098976A8F7CDA2AEFDCA989ABDF1FBAD5 ();
// 0x000004B1 easyar.Detail_FunctorOfVoidFromBoolAndStringAndString easyar.Detail::FunctorOfVoidFromBoolAndStringAndString_to_c(System.Action`3<System.Boolean,System.String,System.String>)
extern void Detail_FunctorOfVoidFromBoolAndStringAndString_to_c_m31B7E02DE768C5A2B93AB04508D8D1607A647009 ();
// 0x000004B2 System.Void easyar.Detail::FunctorOfVoidFromBoolAndString_func(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromBoolAndString_func_m87AE9C0004F64626A5E00CFFD0F1A2F868E29D34 ();
// 0x000004B3 System.Void easyar.Detail::FunctorOfVoidFromBoolAndString_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromBoolAndString_destroy_m1C520BDA809AB254063C19A8E92794F7A368E283 ();
// 0x000004B4 easyar.Detail_FunctorOfVoidFromBoolAndString easyar.Detail::FunctorOfVoidFromBoolAndString_to_c(System.Action`2<System.Boolean,System.String>)
extern void Detail_FunctorOfVoidFromBoolAndString_to_c_m3A7CA2C029A0973FC1C64188CC807CFDA90EF6F7 ();
// 0x000004B5 System.Void easyar.Detail::FunctorOfVoidFromVideoStatus_func(System.IntPtr,easyar.VideoStatus,System.IntPtr&)
extern void Detail_FunctorOfVoidFromVideoStatus_func_m75DE2AFDC396D76465BE54C22D32881BE260470F ();
// 0x000004B6 System.Void easyar.Detail::FunctorOfVoidFromVideoStatus_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromVideoStatus_destroy_m560F57E8D9CDF6C4CCDD018BE704C492D7C6B550 ();
// 0x000004B7 easyar.Detail_FunctorOfVoidFromVideoStatus easyar.Detail::FunctorOfVoidFromVideoStatus_to_c(System.Action`1<easyar.VideoStatus>)
extern void Detail_FunctorOfVoidFromVideoStatus_to_c_mE196DE67EFDC99AD2C1A7788604ED69EB068CF3A ();
// 0x000004B8 System.Void easyar.Detail::FunctorOfVoidFromFeedbackFrame_func(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void Detail_FunctorOfVoidFromFeedbackFrame_func_mB4288FAC2F09BEFDD8503B7684DDC994C32F8CE7 ();
// 0x000004B9 System.Void easyar.Detail::FunctorOfVoidFromFeedbackFrame_destroy(System.IntPtr)
extern void Detail_FunctorOfVoidFromFeedbackFrame_destroy_m08DAAB8A98135C1DF4CFDC319727C08E4EF48BF2 ();
// 0x000004BA easyar.Detail_FunctorOfVoidFromFeedbackFrame easyar.Detail::FunctorOfVoidFromFeedbackFrame_to_c(System.Action`1<easyar.FeedbackFrame>)
extern void Detail_FunctorOfVoidFromFeedbackFrame_to_c_m21611EE69D008507BAEA871DA5CB2E9175EDC949 ();
// 0x000004BB System.Void easyar.Detail::FunctorOfOutputFrameFromListOfOutputFrame_func(System.IntPtr,System.IntPtr,System.IntPtr&,System.IntPtr&)
extern void Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m458EA310F59A4613F82319C3BE247BE328F9EE19 ();
// 0x000004BC System.Void easyar.Detail::FunctorOfOutputFrameFromListOfOutputFrame_destroy(System.IntPtr)
extern void Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m50A85D540D02D4F023B507E3A9BEF80C6D75D58F ();
// 0x000004BD easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame easyar.Detail::FunctorOfOutputFrameFromListOfOutputFrame_to_c(System.Func`2<System.Collections.Generic.List`1<easyar.OutputFrame>,easyar.OutputFrame>)
extern void Detail_FunctorOfOutputFrameFromListOfOutputFrame_to_c_mA81A44C76F95D41B1BCCEF51CE3957FBA51665AA ();
// 0x000004BE System.IntPtr easyar.Detail::ListOfOutputFrame_to_c(easyar.Detail_AutoRelease,System.Collections.Generic.List`1<easyar.OutputFrame>)
extern void Detail_ListOfOutputFrame_to_c_mC4B576DD79667CFB0B05BD099664B567850A078B ();
// 0x000004BF System.Collections.Generic.List`1<easyar.OutputFrame> easyar.Detail::ListOfOutputFrame_from_c(easyar.Detail_AutoRelease,System.IntPtr)
extern void Detail_ListOfOutputFrame_from_c_m289AB323D142C3F4D2A020C19A869A3BFE9C60E1 ();
// 0x000004C0 System.Void easyar.Detail::.cctor()
extern void Detail__cctor_m64B4BF6AAABD85F493C928356BC117AA18CFB94F ();
// 0x000004C1 easyar.Optional`1<T> easyar.Optional`1::CreateNone()
// 0x000004C2 easyar.Optional`1<T> easyar.Optional`1::CreateSome(T)
// 0x000004C3 System.Boolean easyar.Optional`1::get_OnNone()
// 0x000004C4 System.Boolean easyar.Optional`1::get_OnSome()
// 0x000004C5 easyar.Optional`1<T> easyar.Optional`1::get_Empty()
// 0x000004C6 easyar.Optional`1<T> easyar.Optional`1::op_Implicit(T)
// 0x000004C7 T easyar.Optional`1::op_Explicit(easyar.Optional`1<T>)
// 0x000004C8 System.Boolean easyar.Optional`1::op_Equality(easyar.Optional`1<T>,easyar.Optional`1<T>)
// 0x000004C9 System.Boolean easyar.Optional`1::op_Inequality(easyar.Optional`1<T>,easyar.Optional`1<T>)
// 0x000004CA System.Boolean easyar.Optional`1::op_Equality(System.Nullable`1<easyar.Optional`1<T>>,System.Nullable`1<easyar.Optional`1<T>>)
// 0x000004CB System.Boolean easyar.Optional`1::op_Inequality(System.Nullable`1<easyar.Optional`1<T>>,System.Nullable`1<easyar.Optional`1<T>>)
// 0x000004CC System.Boolean easyar.Optional`1::Equals(System.Object)
// 0x000004CD System.Int32 easyar.Optional`1::GetHashCode()
// 0x000004CE System.Boolean easyar.Optional`1::Equals(easyar.Optional`1<T>,easyar.Optional`1<T>)
// 0x000004CF System.Boolean easyar.Optional`1::Equals(System.Nullable`1<easyar.Optional`1<T>>,System.Nullable`1<easyar.Optional`1<T>>)
// 0x000004D0 T easyar.Optional`1::get_Value()
// 0x000004D1 T easyar.Optional`1::ValueOrDefault(T)
// 0x000004D2 System.String easyar.Optional`1::ToString()
// 0x000004D3 System.Void easyar.RefBase::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void RefBase__ctor_m91788A7D2CF047C7B85C0DD02445BFE6240E44C4 ();
// 0x000004D4 System.IntPtr easyar.RefBase::get_cdata()
extern void RefBase_get_cdata_mD182E1D48174BA159B5AC842C977D0AA20706B5B ();
// 0x000004D5 System.Void easyar.RefBase::Finalize()
extern void RefBase_Finalize_m05B482B58CFD31F088B8B284179C9F780434994A ();
// 0x000004D6 System.Void easyar.RefBase::Dispose()
extern void RefBase_Dispose_mC5EEE74F2631F48AA8057680984D440B3F6684F5 ();
// 0x000004D7 System.Object easyar.RefBase::CloneObject()
// 0x000004D8 easyar.RefBase easyar.RefBase::Clone()
extern void RefBase_Clone_m1CFD7B5FBA7DA2DF5A4548DDCBC06F291BDBFEFC ();
// 0x000004D9 System.Void easyar.ObjectTargetParameters::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ObjectTargetParameters__ctor_m8639718B1A8466FE107C169FE53894CA087DC679 ();
// 0x000004DA System.Object easyar.ObjectTargetParameters::CloneObject()
extern void ObjectTargetParameters_CloneObject_mB707B1D59C0DB254CB9D55A5A7061C26D8FB108E ();
// 0x000004DB easyar.ObjectTargetParameters easyar.ObjectTargetParameters::Clone()
extern void ObjectTargetParameters_Clone_m074403BF94452C568AB2199C1BE50BA8307B1AF3 ();
// 0x000004DC System.Void easyar.ObjectTargetParameters::.ctor()
extern void ObjectTargetParameters__ctor_m436ACF83AB36FD865F1E6D69DE8E122117FA2969 ();
// 0x000004DD easyar.BufferDictionary easyar.ObjectTargetParameters::bufferDictionary()
extern void ObjectTargetParameters_bufferDictionary_m1A9F9B83A938D283F059A63086F130B751B72F0A ();
// 0x000004DE System.Void easyar.ObjectTargetParameters::setBufferDictionary(easyar.BufferDictionary)
extern void ObjectTargetParameters_setBufferDictionary_mE7DD1B96A4ADE4A3F88F20CD58B013BDDC08A75B ();
// 0x000004DF System.String easyar.ObjectTargetParameters::objPath()
extern void ObjectTargetParameters_objPath_m3160C541889785D07D64462DD0E6645917ECC397 ();
// 0x000004E0 System.Void easyar.ObjectTargetParameters::setObjPath(System.String)
extern void ObjectTargetParameters_setObjPath_m76C44C09E57CF27A6D0DA64095112A640EC22FA5 ();
// 0x000004E1 System.String easyar.ObjectTargetParameters::name()
extern void ObjectTargetParameters_name_m1C646AAADE21448F2718DA98830B6E81E3CA9D98 ();
// 0x000004E2 System.Void easyar.ObjectTargetParameters::setName(System.String)
extern void ObjectTargetParameters_setName_m2E690848398C1CAA5FD4368DBC72397EEB13EC0B ();
// 0x000004E3 System.String easyar.ObjectTargetParameters::uid()
extern void ObjectTargetParameters_uid_m657E16F63D719250FF01D23D97CD4E5827A3262E ();
// 0x000004E4 System.Void easyar.ObjectTargetParameters::setUid(System.String)
extern void ObjectTargetParameters_setUid_m61A40A6E09E5415C5CE959E77EEA1F3AC9CAD83E ();
// 0x000004E5 System.String easyar.ObjectTargetParameters::meta()
extern void ObjectTargetParameters_meta_mF871DCD84C670AD9B4B01194E4EF6C343752C42E ();
// 0x000004E6 System.Void easyar.ObjectTargetParameters::setMeta(System.String)
extern void ObjectTargetParameters_setMeta_m440F5047BE45E1CA2FA86CD34FCA708BA3EA4E06 ();
// 0x000004E7 System.Single easyar.ObjectTargetParameters::scale()
extern void ObjectTargetParameters_scale_m132A590ACC5E2FF6F82038FF61F7F310781BB51B ();
// 0x000004E8 System.Void easyar.ObjectTargetParameters::setScale(System.Single)
extern void ObjectTargetParameters_setScale_mDE32C98CA8E4A7D8C759E35D16F45450E525F2DB ();
// 0x000004E9 System.Void easyar.ObjectTarget::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ObjectTarget__ctor_m924D32853AC861A188E85EBB4A3D4EE1AC7BC181 ();
// 0x000004EA System.Object easyar.ObjectTarget::CloneObject()
extern void ObjectTarget_CloneObject_mB91E2DE038E562E5E6CCDD8DDCDC7BE7A08DAEFA ();
// 0x000004EB easyar.ObjectTarget easyar.ObjectTarget::Clone()
extern void ObjectTarget_Clone_mBC8C4BCB230B7420AA1CB77277B1D8C88DAB1633 ();
// 0x000004EC System.Void easyar.ObjectTarget::.ctor()
extern void ObjectTarget__ctor_m946F072C8262698C034B305E13F7799676D5E7CF ();
// 0x000004ED easyar.Optional`1<easyar.ObjectTarget> easyar.ObjectTarget::createFromParameters(easyar.ObjectTargetParameters)
extern void ObjectTarget_createFromParameters_m1606A56AF84B40B3D8523EFB1A5C5BC37C7FA14F ();
// 0x000004EE easyar.Optional`1<easyar.ObjectTarget> easyar.ObjectTarget::createFromObjectFile(System.String,easyar.StorageType,System.String,System.String,System.String,System.Single)
extern void ObjectTarget_createFromObjectFile_mB9555990BE683CC84EE6B3FA99E5A23F357E20D0 ();
// 0x000004EF System.Single easyar.ObjectTarget::scale()
extern void ObjectTarget_scale_mF3268BA13BF64C433D3542DA638D2A41DD6A8512 ();
// 0x000004F0 System.Collections.Generic.List`1<easyar.Vec3F> easyar.ObjectTarget::boundingBox()
extern void ObjectTarget_boundingBox_mAE9EBCF2AC7271B9A90F33DCD00E8D9870FF5DD0 ();
// 0x000004F1 System.Boolean easyar.ObjectTarget::setScale(System.Single)
extern void ObjectTarget_setScale_m70B0798461799B9CA7C36A5C2B808E31BCB2285F ();
// 0x000004F2 System.Int32 easyar.ObjectTarget::runtimeID()
extern void ObjectTarget_runtimeID_m2ADB716CB3CE10CA83C50D57F341492AB181CF30 ();
// 0x000004F3 System.String easyar.ObjectTarget::uid()
extern void ObjectTarget_uid_m8C97428C7E977E8742FA2D648ADFA8AB00C0F5B1 ();
// 0x000004F4 System.String easyar.ObjectTarget::name()
extern void ObjectTarget_name_m4AA1DE5862EF7C86114C43699F05D54B4CD80A26 ();
// 0x000004F5 System.Void easyar.ObjectTarget::setName(System.String)
extern void ObjectTarget_setName_mC5F602F0A1B97D91871EB0F9BCB91993E850E3AF ();
// 0x000004F6 System.String easyar.ObjectTarget::meta()
extern void ObjectTarget_meta_mB07D7C6922AD59491B80B484F011F069DF77811D ();
// 0x000004F7 System.Void easyar.ObjectTarget::setMeta(System.String)
extern void ObjectTarget_setMeta_mF6DFF9008E5B4535E8E0DB5FBBB884836B6A9E46 ();
// 0x000004F8 System.Void easyar.ObjectTrackerResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ObjectTrackerResult__ctor_mB61219E47B0A872781A4A37762365162199CCA1A ();
// 0x000004F9 System.Object easyar.ObjectTrackerResult::CloneObject()
extern void ObjectTrackerResult_CloneObject_mD0E9F4A237DFDF9676952409A07F4567591D7A57 ();
// 0x000004FA easyar.ObjectTrackerResult easyar.ObjectTrackerResult::Clone()
extern void ObjectTrackerResult_Clone_m5026912103FB9A4F43E6A6AB16CB5F5628B9FFF4 ();
// 0x000004FB System.Collections.Generic.List`1<easyar.TargetInstance> easyar.ObjectTrackerResult::targetInstances()
extern void ObjectTrackerResult_targetInstances_m94DD9F2BE5452736A6AFE883A5A18B5C38F7B18F ();
// 0x000004FC System.Void easyar.ObjectTrackerResult::setTargetInstances(System.Collections.Generic.List`1<easyar.TargetInstance>)
extern void ObjectTrackerResult_setTargetInstances_m1F6BC1E3C5937C2157A4F771A2334C5B7301F9F4 ();
// 0x000004FD System.Void easyar.ObjectTracker::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ObjectTracker__ctor_mA7A9EF4D1B8B074063DD248AAAA5B3345FE2E7B8 ();
// 0x000004FE System.Object easyar.ObjectTracker::CloneObject()
extern void ObjectTracker_CloneObject_mE7BB260FA1FBCD98118B3615C4EEB114145F2FEB ();
// 0x000004FF easyar.ObjectTracker easyar.ObjectTracker::Clone()
extern void ObjectTracker_Clone_mE69E0452C1AAA27D742A2A546B790874345BF801 ();
// 0x00000500 System.Boolean easyar.ObjectTracker::isAvailable()
extern void ObjectTracker_isAvailable_m083D4EFD6B81B341E7F5CDF125924758ED21D226 ();
// 0x00000501 easyar.FeedbackFrameSink easyar.ObjectTracker::feedbackFrameSink()
extern void ObjectTracker_feedbackFrameSink_m4909CAA28D3EC64670932C1F71CD56252FB6761F ();
// 0x00000502 System.Int32 easyar.ObjectTracker::bufferRequirement()
extern void ObjectTracker_bufferRequirement_m7740DD98FD1C606A8576BAD6C37D6C1D949B664D ();
// 0x00000503 easyar.OutputFrameSource easyar.ObjectTracker::outputFrameSource()
extern void ObjectTracker_outputFrameSource_m6D8601B83FAE769B499ECBC35B02C137DDC6948A ();
// 0x00000504 easyar.ObjectTracker easyar.ObjectTracker::create()
extern void ObjectTracker_create_mD209283F841889B26EB8A317DEBD9D2B51BABD6F ();
// 0x00000505 System.Boolean easyar.ObjectTracker::start()
extern void ObjectTracker_start_m17F1D1D22C760D2AEA3F7433D39089EEE701F307 ();
// 0x00000506 System.Void easyar.ObjectTracker::stop()
extern void ObjectTracker_stop_mB3E8E5B0197715DB98FA7DF095FA09FA43CD8941 ();
// 0x00000507 System.Void easyar.ObjectTracker::close()
extern void ObjectTracker_close_mF067D3B2B0BE57ACFB10C6A7ACAECD21F6006994 ();
// 0x00000508 System.Void easyar.ObjectTracker::loadTarget(easyar.Target,easyar.CallbackScheduler,System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTracker_loadTarget_mB16084CB91FB08D7246A81834876AA77C923996C ();
// 0x00000509 System.Void easyar.ObjectTracker::unloadTarget(easyar.Target,easyar.CallbackScheduler,System.Action`2<easyar.Target,System.Boolean>)
extern void ObjectTracker_unloadTarget_m7F2FF728A1AE95EBCEFC31CABBDE12E2BF5F754F ();
// 0x0000050A System.Collections.Generic.List`1<easyar.Target> easyar.ObjectTracker::targets()
extern void ObjectTracker_targets_m2635EBB7016F8E98FA8093B40BF7744B8FBC328F ();
// 0x0000050B System.Boolean easyar.ObjectTracker::setSimultaneousNum(System.Int32)
extern void ObjectTracker_setSimultaneousNum_mA9075A08BBB49358C63463884FC97711122F456B ();
// 0x0000050C System.Int32 easyar.ObjectTracker::simultaneousNum()
extern void ObjectTracker_simultaneousNum_mDE744A781F4C13CB97CB4C6403D5844FBD497D5C ();
// 0x0000050D System.Void easyar.CloudRecognizationResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void CloudRecognizationResult__ctor_m002DB4D3C2D1B071EFCA54B116BB74776666403B ();
// 0x0000050E System.Object easyar.CloudRecognizationResult::CloneObject()
extern void CloudRecognizationResult_CloneObject_mDBD103E163E9C4A1418FB6DFB94BA7C12A1579A0 ();
// 0x0000050F easyar.CloudRecognizationResult easyar.CloudRecognizationResult::Clone()
extern void CloudRecognizationResult_Clone_m429D2B0401C51975AC0CF8F322DFC4508A01ABDA ();
// 0x00000510 easyar.CloudRecognizationStatus easyar.CloudRecognizationResult::getStatus()
extern void CloudRecognizationResult_getStatus_m6E1AACCBCE52E02BF171576D30161E1C72EC3AB4 ();
// 0x00000511 easyar.Optional`1<easyar.ImageTarget> easyar.CloudRecognizationResult::getTarget()
extern void CloudRecognizationResult_getTarget_mE56DDB50BECDE81C1D559728DFA2277155FBEEC5 ();
// 0x00000512 easyar.Optional`1<System.String> easyar.CloudRecognizationResult::getUnknownErrorMessage()
extern void CloudRecognizationResult_getUnknownErrorMessage_m2CC5F7642754D8594E7370FF36D46179ECEAEEC9 ();
// 0x00000513 System.Void easyar.CloudRecognizer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void CloudRecognizer__ctor_m93FFD3B967300233E501FF4341976DACBBFC2BCB ();
// 0x00000514 System.Object easyar.CloudRecognizer::CloneObject()
extern void CloudRecognizer_CloneObject_mD7EE322C5128F8EA0CC8EF2DE9D9C53A8E3746EA ();
// 0x00000515 easyar.CloudRecognizer easyar.CloudRecognizer::Clone()
extern void CloudRecognizer_Clone_m88BAC210481DEA84553D981D53D1E234C572A608 ();
// 0x00000516 System.Boolean easyar.CloudRecognizer::isAvailable()
extern void CloudRecognizer_isAvailable_m4285C37AF079A30076C3873B8B72967D823165D7 ();
// 0x00000517 easyar.CloudRecognizer easyar.CloudRecognizer::create(System.String,System.String,System.String,System.String)
extern void CloudRecognizer_create_m6BB61143CEB61D7E82855704A195F337562F4D8A ();
// 0x00000518 easyar.CloudRecognizer easyar.CloudRecognizer::createByCloudSecret(System.String,System.String,System.String)
extern void CloudRecognizer_createByCloudSecret_mFD6B4D671A6AE058C0686FD7B1C8C452A8ACCCA7 ();
// 0x00000519 System.Void easyar.CloudRecognizer::resolve(easyar.InputFrame,easyar.CallbackScheduler,System.Action`1<easyar.CloudRecognizationResult>)
extern void CloudRecognizer_resolve_m7B2B1A742B0B875C2300248D138FA2552BBEF986 ();
// 0x0000051A System.Void easyar.CloudRecognizer::close()
extern void CloudRecognizer_close_m60640E28D8ECCBA19544C3152DC34CB4735697A1 ();
// 0x0000051B System.Void easyar.Buffer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void Buffer__ctor_mEF3C573C11D612B72D515E1C7F0CF744A341BE1A ();
// 0x0000051C System.Object easyar.Buffer::CloneObject()
extern void Buffer_CloneObject_mD01AEB1D867CE25E230D31DA6DC9F519656751AF ();
// 0x0000051D easyar.Buffer easyar.Buffer::Clone()
extern void Buffer_Clone_mF8012D3BF56109E59E22F70871505B5181147C1E ();
// 0x0000051E easyar.Buffer easyar.Buffer::wrap(System.IntPtr,System.Int32,System.Action)
extern void Buffer_wrap_mB2AB986AF54AD8826E2C10B999BC0A5D0BDB060B ();
// 0x0000051F easyar.Buffer easyar.Buffer::create(System.Int32)
extern void Buffer_create_m1433F8E7DC4CE17B24A54ABCB03406831D49800A ();
// 0x00000520 System.IntPtr easyar.Buffer::data()
extern void Buffer_data_m261BD1BD4753A4F340C64B63D9B645C59096407E ();
// 0x00000521 System.Int32 easyar.Buffer::size()
extern void Buffer_size_mEBC3625F4E332CAE52CF1A77EE87F5FBF7B0BFE3 ();
// 0x00000522 System.Void easyar.Buffer::memoryCopy(System.IntPtr,System.IntPtr,System.Int32)
extern void Buffer_memoryCopy_m6E06ECADF51903161E42F6055D5DE7BE819225C7 ();
// 0x00000523 System.Boolean easyar.Buffer::tryCopyFrom(System.IntPtr,System.Int32,System.Int32,System.Int32)
extern void Buffer_tryCopyFrom_m599D8D5488BC70B016D909E77E7A42FA6988877D ();
// 0x00000524 System.Boolean easyar.Buffer::tryCopyTo(System.Int32,System.IntPtr,System.Int32,System.Int32)
extern void Buffer_tryCopyTo_mE02D6C6BC05007B68EFDE784FC4F43F63451CD8B ();
// 0x00000525 easyar.Buffer easyar.Buffer::partition(System.Int32,System.Int32)
extern void Buffer_partition_mF8CC984BE418E57F8D59659291B0EAD053B50D5B ();
// 0x00000526 easyar.Buffer easyar.Buffer::wrapByteArray(System.Byte[])
extern void Buffer_wrapByteArray_mA2011FD8AF7706362E53D5D95BB8F6DB118586D7 ();
// 0x00000527 easyar.Buffer easyar.Buffer::wrapByteArray(System.Byte[],System.Int32,System.Int32)
extern void Buffer_wrapByteArray_m9F65669B174ECC5CC2EAF34C0E76130164163F1C ();
// 0x00000528 easyar.Buffer easyar.Buffer::wrapByteArray(System.Byte[],System.Int32,System.Int32,System.Action)
extern void Buffer_wrapByteArray_mD921453D5600FCC247D99A166F9FA5360B097ABB ();
// 0x00000529 System.Void easyar.Buffer::copyFromByteArray(System.Byte[])
extern void Buffer_copyFromByteArray_mB626246C15839E8EEA921A3739B78D5BBD6F47D3 ();
// 0x0000052A System.Void easyar.Buffer::copyFromByteArray(System.Byte[],System.Int32,System.Int32,System.Int32)
extern void Buffer_copyFromByteArray_m2C9875A71992BEE709C93A8F136E1101A33051F2 ();
// 0x0000052B System.Void easyar.Buffer::copyToByteArray(System.Byte[])
extern void Buffer_copyToByteArray_mC648B696E1E7E979323D241A2260F4D6DBCEDE32 ();
// 0x0000052C System.Void easyar.Buffer::copyToByteArray(System.Int32,System.Byte[],System.Int32,System.Int32)
extern void Buffer_copyToByteArray_m48502803D78E11061E50CE1B0AA6F586B823EE70 ();
// 0x0000052D System.Void easyar.BufferDictionary::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void BufferDictionary__ctor_m4CF91091E43C1698E3E1FBACFCAF250318C21252 ();
// 0x0000052E System.Object easyar.BufferDictionary::CloneObject()
extern void BufferDictionary_CloneObject_mAD6FA00A4836FEA56255E3FA26C17E2EE6AAC606 ();
// 0x0000052F easyar.BufferDictionary easyar.BufferDictionary::Clone()
extern void BufferDictionary_Clone_m908382D1CF25FEEEBFBAA038C1DFB370CAF52F1A ();
// 0x00000530 System.Void easyar.BufferDictionary::.ctor()
extern void BufferDictionary__ctor_m44FF83DD8756EC1F69D1D3C22317CE8CFE5822C4 ();
// 0x00000531 System.Int32 easyar.BufferDictionary::count()
extern void BufferDictionary_count_m9FA601E3F73807747761943DE41BF9E1D457622B ();
// 0x00000532 System.Boolean easyar.BufferDictionary::contains(System.String)
extern void BufferDictionary_contains_m2B74420924CD26D15FFE5D3A5F1696846517D6F8 ();
// 0x00000533 easyar.Optional`1<easyar.Buffer> easyar.BufferDictionary::tryGet(System.String)
extern void BufferDictionary_tryGet_m666EB4F17C21A6B92E927BCDA8B3A216B5CD487E ();
// 0x00000534 System.Void easyar.BufferDictionary::set(System.String,easyar.Buffer)
extern void BufferDictionary_set_m55DC11FDFE7AEECCA1EE8FE2940E38AF2B9487C8 ();
// 0x00000535 System.Boolean easyar.BufferDictionary::remove(System.String)
extern void BufferDictionary_remove_mE6AAD1B8BEC54300407701B0686CB4DB3F2CD97A ();
// 0x00000536 System.Void easyar.BufferDictionary::clear()
extern void BufferDictionary_clear_m97CB377E4FF2C916216E76BDC3955B981449BA50 ();
// 0x00000537 System.Void easyar.BufferPool::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void BufferPool__ctor_mBE8ABBED71BB3EBB0E3E402FB1871DCCDE2B88D6 ();
// 0x00000538 System.Object easyar.BufferPool::CloneObject()
extern void BufferPool_CloneObject_m1C8252FBD1C2931FA4521FB02A5C3FF14D684814 ();
// 0x00000539 easyar.BufferPool easyar.BufferPool::Clone()
extern void BufferPool_Clone_mB99AF64A70D3C1ACE53080D0AD9982161F03ED72 ();
// 0x0000053A System.Void easyar.BufferPool::.ctor(System.Int32,System.Int32)
extern void BufferPool__ctor_m8C9069485338E307D7DFA1EA8405A50CAD433147 ();
// 0x0000053B System.Int32 easyar.BufferPool::block_size()
extern void BufferPool_block_size_m0977C2CC58C2D7F4C57D360790DB2188741675C4 ();
// 0x0000053C System.Int32 easyar.BufferPool::capacity()
extern void BufferPool_capacity_m142D51E9D7A87D64D076A7D3CEA745CB359E5085 ();
// 0x0000053D System.Int32 easyar.BufferPool::size()
extern void BufferPool_size_mE1934897587752BEBC11BE672447A4BBE4C5ABBC ();
// 0x0000053E easyar.Optional`1<easyar.Buffer> easyar.BufferPool::tryAcquire()
extern void BufferPool_tryAcquire_mF65BB918E3721A933FBBC46444D9D06BC766D95E ();
// 0x0000053F System.Void easyar.CameraParameters::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void CameraParameters__ctor_mCAD7C2A96D99887CFCDC0B88BB6B4BBA25E35D03 ();
// 0x00000540 System.Object easyar.CameraParameters::CloneObject()
extern void CameraParameters_CloneObject_mE5629D6218376CB43C388E929E75D05DE3E73AC0 ();
// 0x00000541 easyar.CameraParameters easyar.CameraParameters::Clone()
extern void CameraParameters_Clone_m5B1FB7B02B1BAEC80C58E51B8A28C741A9CC49AC ();
// 0x00000542 System.Void easyar.CameraParameters::.ctor(easyar.Vec2I,easyar.Vec2F,easyar.Vec2F,easyar.CameraDeviceType,System.Int32)
extern void CameraParameters__ctor_m9B5FB86AC5E02CF08C315CAEB4947DEC1BE2F8DF ();
// 0x00000543 easyar.Vec2I easyar.CameraParameters::size()
extern void CameraParameters_size_mB225D68F6DA3186820C7CB7AAF63380B79FCB2DD ();
// 0x00000544 easyar.Vec2F easyar.CameraParameters::focalLength()
extern void CameraParameters_focalLength_m2FF303C350D10BABDF66C45295B5F9A4AB491096 ();
// 0x00000545 easyar.Vec2F easyar.CameraParameters::principalPoint()
extern void CameraParameters_principalPoint_m6E2B0B61E424A3CB0C437E8A63A3B744B32E7F17 ();
// 0x00000546 easyar.CameraDeviceType easyar.CameraParameters::cameraDeviceType()
extern void CameraParameters_cameraDeviceType_m8C52D12B699798959D87885C9D0AE5A80584530A ();
// 0x00000547 System.Int32 easyar.CameraParameters::cameraOrientation()
extern void CameraParameters_cameraOrientation_mADAB58839A6BAFEC90D0AC5656D7FCF251073FD6 ();
// 0x00000548 easyar.CameraParameters easyar.CameraParameters::createWithDefaultIntrinsics(easyar.Vec2I,easyar.CameraDeviceType,System.Int32)
extern void CameraParameters_createWithDefaultIntrinsics_mAF9EF6182FA49B5542488514C709DD8418D4DC85 ();
// 0x00000549 easyar.CameraParameters easyar.CameraParameters::getResized(easyar.Vec2I)
extern void CameraParameters_getResized_mAED2A3DC77DC2AD22E9ABBAA0649B544940BBC7E ();
// 0x0000054A System.Int32 easyar.CameraParameters::imageOrientation(System.Int32)
extern void CameraParameters_imageOrientation_m8B59CFD2B763044D085C480AC406140262F0845E ();
// 0x0000054B System.Boolean easyar.CameraParameters::imageHorizontalFlip(System.Boolean)
extern void CameraParameters_imageHorizontalFlip_mA797AF69683770220F1904658F11975A6393BAA3 ();
// 0x0000054C easyar.Matrix44F easyar.CameraParameters::projection(System.Single,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean)
extern void CameraParameters_projection_mE54ADBB454957093146903CE2A40DA6844FDD888 ();
// 0x0000054D easyar.Matrix44F easyar.CameraParameters::imageProjection(System.Single,System.Int32,System.Boolean,System.Boolean)
extern void CameraParameters_imageProjection_m9A11DAAAC2241903641A13212FC21355B171059C ();
// 0x0000054E easyar.Vec2F easyar.CameraParameters::screenCoordinatesFromImageCoordinates(System.Single,System.Int32,System.Boolean,System.Boolean,easyar.Vec2F)
extern void CameraParameters_screenCoordinatesFromImageCoordinates_mDBB25316CE26993F1781D00B3487E02F7CA860C1 ();
// 0x0000054F easyar.Vec2F easyar.CameraParameters::imageCoordinatesFromScreenCoordinates(System.Single,System.Int32,System.Boolean,System.Boolean,easyar.Vec2F)
extern void CameraParameters_imageCoordinatesFromScreenCoordinates_m41DB742F00C3182AAE5AC498B528F572EC8F61C5 ();
// 0x00000550 System.Boolean easyar.CameraParameters::equalsTo(easyar.CameraParameters)
extern void CameraParameters_equalsTo_m4CC5CA93CBFB88F2AC2589152FFE03CA781C31DC ();
// 0x00000551 System.Void easyar.Image::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void Image__ctor_mB9C8362D8AD67192ECAE16401923946254297F4A ();
// 0x00000552 System.Object easyar.Image::CloneObject()
extern void Image_CloneObject_m6184971E200120080B008A52F5495390965DB0F3 ();
// 0x00000553 easyar.Image easyar.Image::Clone()
extern void Image_Clone_m65828148EA61961AC68180539D7B7BAD7A177201 ();
// 0x00000554 System.Void easyar.Image::.ctor(easyar.Buffer,easyar.PixelFormat,System.Int32,System.Int32)
extern void Image__ctor_mF60332DD9FAFCA7149DBFA02E8999B866A76DE95 ();
// 0x00000555 easyar.Buffer easyar.Image::buffer()
extern void Image_buffer_m20B6E778C27A4F5921DECA6AA7FE9C68DFC56389 ();
// 0x00000556 easyar.PixelFormat easyar.Image::format()
extern void Image_format_mFA8D4F41659361CC0BCCBEC149E243E6414506A8 ();
// 0x00000557 System.Int32 easyar.Image::width()
extern void Image_width_m4F69246DA2F773F8B32EFC3F0F2511FF1BB9C583 ();
// 0x00000558 System.Int32 easyar.Image::height()
extern void Image_height_m34527BDB15F2496BC06B20272F32CE3AFF8BB45C ();
// 0x00000559 System.Single[] easyar.Matrix44F::get_data()
extern void Matrix44F_get_data_m1DDEAC58B2C140900BF354F3489B9749FF407098_AdjustorThunk ();
// 0x0000055A System.Void easyar.Matrix44F::set_data(System.Single[])
extern void Matrix44F_set_data_mA1983158F080953F2E9E8CF75DE1F1605C4C58EE_AdjustorThunk ();
// 0x0000055B System.Void easyar.Matrix44F::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Matrix44F__ctor_m271F522AB6CAAAEB41F724D5E99D36F489574FFE_AdjustorThunk ();
// 0x0000055C System.Single[] easyar.Matrix33F::get_data()
extern void Matrix33F_get_data_m1527BBB935223A323992B0252569048E0DB1B504_AdjustorThunk ();
// 0x0000055D System.Void easyar.Matrix33F::set_data(System.Single[])
extern void Matrix33F_set_data_m7B9C86C95AC3774CBC63684FD40CE7177EE0F309_AdjustorThunk ();
// 0x0000055E System.Void easyar.Matrix33F::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Matrix33F__ctor_m7E0ECB95E3F54AE58AB0C5A991B7D785D34DDAB5_AdjustorThunk ();
// 0x0000055F System.Single[] easyar.Vec4F::get_data()
extern void Vec4F_get_data_mF51AC023871ABEE4DBD3AD28C4F390CFB273AA30_AdjustorThunk ();
// 0x00000560 System.Void easyar.Vec4F::set_data(System.Single[])
extern void Vec4F_set_data_m0BA7C5536C1701156644686BC4521B1F896689A7_AdjustorThunk ();
// 0x00000561 System.Void easyar.Vec4F::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Vec4F__ctor_m2068732A7C9152EB671CEF3821B8B7BFC25AE18A_AdjustorThunk ();
// 0x00000562 System.Single[] easyar.Vec3F::get_data()
extern void Vec3F_get_data_m90487EB17A649369D3104992E87F591768ECB91D_AdjustorThunk ();
// 0x00000563 System.Void easyar.Vec3F::set_data(System.Single[])
extern void Vec3F_set_data_m5B024D4EE0A5D7106603E0DE33B68CF866A8CCC2_AdjustorThunk ();
// 0x00000564 System.Void easyar.Vec3F::.ctor(System.Single,System.Single,System.Single)
extern void Vec3F__ctor_mF713AA4F3CA14E1AF8871D0E03655932065DE10E_AdjustorThunk ();
// 0x00000565 System.Single[] easyar.Vec2F::get_data()
extern void Vec2F_get_data_m4E526F29C8FC3EE18398AA7D947926AF0092349D_AdjustorThunk ();
// 0x00000566 System.Void easyar.Vec2F::set_data(System.Single[])
extern void Vec2F_set_data_m75C0FC83A5CBE4A65B3B457E15552B503428EE79_AdjustorThunk ();
// 0x00000567 System.Void easyar.Vec2F::.ctor(System.Single,System.Single)
extern void Vec2F__ctor_m1B76E4A69CBFABDA68EA49CE6255960B49C61838_AdjustorThunk ();
// 0x00000568 System.Int32[] easyar.Vec4I::get_data()
extern void Vec4I_get_data_m0B3AABF38F0E33C97BFB07D5656D3DD92C86DD15_AdjustorThunk ();
// 0x00000569 System.Void easyar.Vec4I::set_data(System.Int32[])
extern void Vec4I_set_data_m82AC198017DD058FED3C3EF4FE8C8F45B2223FAB_AdjustorThunk ();
// 0x0000056A System.Void easyar.Vec4I::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Vec4I__ctor_mA483106568863005D3DA0444152E988A6BDA89CC_AdjustorThunk ();
// 0x0000056B System.Int32[] easyar.Vec2I::get_data()
extern void Vec2I_get_data_m4EBB17FC8365344BF8FD08505F8A3B281BDCBDB7_AdjustorThunk ();
// 0x0000056C System.Void easyar.Vec2I::set_data(System.Int32[])
extern void Vec2I_set_data_m04816F71D2D5F8520DA2A2ADEACCB292C9966992_AdjustorThunk ();
// 0x0000056D System.Void easyar.Vec2I::.ctor(System.Int32,System.Int32)
extern void Vec2I__ctor_m1A88208FBBDD30CC030A78F2D0D92385FAFE0666_AdjustorThunk ();
// 0x0000056E System.Void easyar.DenseSpatialMap::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void DenseSpatialMap__ctor_m2815677A18323B8C8E86870368879E3115542224 ();
// 0x0000056F System.Object easyar.DenseSpatialMap::CloneObject()
extern void DenseSpatialMap_CloneObject_m2D3F5503E5F727B7FE548D43BD7F98DFADBCD29C ();
// 0x00000570 easyar.DenseSpatialMap easyar.DenseSpatialMap::Clone()
extern void DenseSpatialMap_Clone_mB8DC4DADD575873C000BF9347EBF56420EB0E880 ();
// 0x00000571 System.Boolean easyar.DenseSpatialMap::isAvailable()
extern void DenseSpatialMap_isAvailable_m8AB15859582EBC1BD44926F8655AC879A2E02563 ();
// 0x00000572 easyar.InputFrameSink easyar.DenseSpatialMap::inputFrameSink()
extern void DenseSpatialMap_inputFrameSink_mF572AF0EF62368F8C4F2B1F7E56661A2E7EB79E4 ();
// 0x00000573 System.Int32 easyar.DenseSpatialMap::bufferRequirement()
extern void DenseSpatialMap_bufferRequirement_mE7A13DD131C096BB0D424BE2E78B5AE78ACFFFA2 ();
// 0x00000574 easyar.DenseSpatialMap easyar.DenseSpatialMap::create()
extern void DenseSpatialMap_create_m3266893950344529E5935822F64314AF990275F6 ();
// 0x00000575 System.Boolean easyar.DenseSpatialMap::start()
extern void DenseSpatialMap_start_m244B31E62F629BA47D8B00E6D64CC160AF735206 ();
// 0x00000576 System.Void easyar.DenseSpatialMap::stop()
extern void DenseSpatialMap_stop_mA46FD9DACAD3FBA9A97408C43474B4D5A9DD13B8 ();
// 0x00000577 System.Void easyar.DenseSpatialMap::close()
extern void DenseSpatialMap_close_mE4B448FA529E993D63C5EFA6FF5AAB5820A6C24C ();
// 0x00000578 easyar.SceneMesh easyar.DenseSpatialMap::getMesh()
extern void DenseSpatialMap_getMesh_mDBEEA6A4E2A6B864DEFE6B39EF9CBFF793AAF033 ();
// 0x00000579 System.Boolean easyar.DenseSpatialMap::updateSceneMesh(System.Boolean)
extern void DenseSpatialMap_updateSceneMesh_mA9CE004FC241C724FB97B171E5E9D0A8106E0C40 ();
// 0x0000057A System.Void easyar.BlockInfo::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void BlockInfo__ctor_mDE82E90CACBCDDDC03D42A9291046B2762876E82_AdjustorThunk ();
// 0x0000057B System.Void easyar.SceneMesh::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SceneMesh__ctor_m73273F2601FF07DDED492EF9EC3FECE8E4A8F21C ();
// 0x0000057C System.Object easyar.SceneMesh::CloneObject()
extern void SceneMesh_CloneObject_mAB7C07480AE30807D24C0F60B958262C076182C1 ();
// 0x0000057D easyar.SceneMesh easyar.SceneMesh::Clone()
extern void SceneMesh_Clone_m038039E8D019D53E4F1A9BC4177B26AA1EA33487 ();
// 0x0000057E System.Int32 easyar.SceneMesh::getNumOfVertexAll()
extern void SceneMesh_getNumOfVertexAll_mC7ABDEE82861B4C506FBF7110C7DAA17A3CD5372 ();
// 0x0000057F System.Int32 easyar.SceneMesh::getNumOfIndexAll()
extern void SceneMesh_getNumOfIndexAll_mDCAE70B9668409A680C0CE8FEFEA9941C5DC2C2B ();
// 0x00000580 easyar.Buffer easyar.SceneMesh::getVerticesAll()
extern void SceneMesh_getVerticesAll_mB68A474BA1BC297CC78DC6D0B1A19B762FFA14CB ();
// 0x00000581 easyar.Buffer easyar.SceneMesh::getNormalsAll()
extern void SceneMesh_getNormalsAll_m6705D6CCF147644A55EB6739AC65F00F9BFB50CF ();
// 0x00000582 easyar.Buffer easyar.SceneMesh::getIndicesAll()
extern void SceneMesh_getIndicesAll_m9A8472D980A3DBD8D1C5D7488B933DAB36207CFD ();
// 0x00000583 System.Int32 easyar.SceneMesh::getNumOfVertexIncremental()
extern void SceneMesh_getNumOfVertexIncremental_m7AE13F0F5D5C8AF91B1E9D9AA407ADC2296F16E7 ();
// 0x00000584 System.Int32 easyar.SceneMesh::getNumOfIndexIncremental()
extern void SceneMesh_getNumOfIndexIncremental_m5443F93EC6A37B08FCFC83DA54C5A0E30B24EEDA ();
// 0x00000585 easyar.Buffer easyar.SceneMesh::getVerticesIncremental()
extern void SceneMesh_getVerticesIncremental_mFB1BF4BA5C98670772D7D9DCA2E7CDB9E0CC33C6 ();
// 0x00000586 easyar.Buffer easyar.SceneMesh::getNormalsIncremental()
extern void SceneMesh_getNormalsIncremental_mEC011B6334049896E9A69DC9F7A6D7FA9AD5E715 ();
// 0x00000587 easyar.Buffer easyar.SceneMesh::getIndicesIncremental()
extern void SceneMesh_getIndicesIncremental_mC2839616893212BC963046D1CC866898F13842A4 ();
// 0x00000588 System.Collections.Generic.List`1<easyar.BlockInfo> easyar.SceneMesh::getBlocksInfoIncremental()
extern void SceneMesh_getBlocksInfoIncremental_mD059A4278AA9D01714E0EAE886E25749B893B6EF ();
// 0x00000589 System.Single easyar.SceneMesh::getBlockDimensionInMeters()
extern void SceneMesh_getBlockDimensionInMeters_mC5181EC4C02044F01286FA7B378FF1AE513630CB ();
// 0x0000058A System.Void easyar.ARCoreCameraDevice::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ARCoreCameraDevice__ctor_m57862F2E4EDC33C0F1945FBC7BC35218D3E31A34 ();
// 0x0000058B System.Object easyar.ARCoreCameraDevice::CloneObject()
extern void ARCoreCameraDevice_CloneObject_mE8611DFBCE3A4EB74267A798922898C85F21F6EF ();
// 0x0000058C easyar.ARCoreCameraDevice easyar.ARCoreCameraDevice::Clone()
extern void ARCoreCameraDevice_Clone_m154E1995D8C0B23A6C919439D3DFE4AD1B385356 ();
// 0x0000058D System.Void easyar.ARCoreCameraDevice::.ctor()
extern void ARCoreCameraDevice__ctor_m97A779D94986EFAF9D2365AEB66709773A20E048 ();
// 0x0000058E System.Boolean easyar.ARCoreCameraDevice::isAvailable()
extern void ARCoreCameraDevice_isAvailable_m4C3E38AE9614E3C6642E10513D9A8A023D169941 ();
// 0x0000058F System.Int32 easyar.ARCoreCameraDevice::bufferCapacity()
extern void ARCoreCameraDevice_bufferCapacity_mABA1E15C2F8C19232A4E5B0A5ACA8615BB050E15 ();
// 0x00000590 System.Void easyar.ARCoreCameraDevice::setBufferCapacity(System.Int32)
extern void ARCoreCameraDevice_setBufferCapacity_m504C5FCAA5DDF61C367BC5819E0AD5DCD999D872 ();
// 0x00000591 easyar.InputFrameSource easyar.ARCoreCameraDevice::inputFrameSource()
extern void ARCoreCameraDevice_inputFrameSource_mFFBE03DE3E9894DDE9191B1DF6CDD23E3A35D8DB ();
// 0x00000592 System.Boolean easyar.ARCoreCameraDevice::start()
extern void ARCoreCameraDevice_start_mAF98A0608B9DF3ED5AD41DFF2E5A0A08119E626D ();
// 0x00000593 System.Void easyar.ARCoreCameraDevice::stop()
extern void ARCoreCameraDevice_stop_mE5942D1D3CAC436C26B19CA645C90060C75A2263 ();
// 0x00000594 System.Void easyar.ARCoreCameraDevice::close()
extern void ARCoreCameraDevice_close_m4746EE08EBFAB18FE243F5FC8D14E2AF64A3A5D0 ();
// 0x00000595 System.Void easyar.ARKitCameraDevice::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ARKitCameraDevice__ctor_mD0EFB0112A261CAFB92BCF50298886DC2DB59699 ();
// 0x00000596 System.Object easyar.ARKitCameraDevice::CloneObject()
extern void ARKitCameraDevice_CloneObject_m6EDF36139DF8EF6373C72A151863287C34E87A35 ();
// 0x00000597 easyar.ARKitCameraDevice easyar.ARKitCameraDevice::Clone()
extern void ARKitCameraDevice_Clone_m1DAE4E44FF0C99BF6404BBC94853F9A41A0A13A1 ();
// 0x00000598 System.Void easyar.ARKitCameraDevice::.ctor()
extern void ARKitCameraDevice__ctor_m124295615BD89B0D2A0AA1C2F3C1EEBDDAA74DD1 ();
// 0x00000599 System.Boolean easyar.ARKitCameraDevice::isAvailable()
extern void ARKitCameraDevice_isAvailable_mBEE5938470583779D88EE88A04A867E9F76CF659 ();
// 0x0000059A System.Int32 easyar.ARKitCameraDevice::bufferCapacity()
extern void ARKitCameraDevice_bufferCapacity_m98D2950D04ACC6197AA8148474B704CC0C10C464 ();
// 0x0000059B System.Void easyar.ARKitCameraDevice::setBufferCapacity(System.Int32)
extern void ARKitCameraDevice_setBufferCapacity_m994843D62AD2EF780C717F46348D61C78F9D9505 ();
// 0x0000059C easyar.InputFrameSource easyar.ARKitCameraDevice::inputFrameSource()
extern void ARKitCameraDevice_inputFrameSource_mFADE99162C50689038FDE4CDA7416A9953837C95 ();
// 0x0000059D System.Boolean easyar.ARKitCameraDevice::start()
extern void ARKitCameraDevice_start_m34CAE9379ADA7CBF535F3904DB74815764167061 ();
// 0x0000059E System.Void easyar.ARKitCameraDevice::stop()
extern void ARKitCameraDevice_stop_mDD176A7C6567B6E1989C3F6C0304E036FAAE8BBF ();
// 0x0000059F System.Void easyar.ARKitCameraDevice::close()
extern void ARKitCameraDevice_close_m1AE33B621A2F1B7681FF446C132366CC6691023D ();
// 0x000005A0 System.Void easyar.CameraDevice::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void CameraDevice__ctor_m1842F47EC62FB0B20307493D830C2C116AE2F038 ();
// 0x000005A1 System.Object easyar.CameraDevice::CloneObject()
extern void CameraDevice_CloneObject_mA4BC78AD457F5D89A08D54ACF7AE8B050E8E4638 ();
// 0x000005A2 easyar.CameraDevice easyar.CameraDevice::Clone()
extern void CameraDevice_Clone_m49EBD72310753FC722F08766D6E24E350DB40B3E ();
// 0x000005A3 System.Void easyar.CameraDevice::.ctor()
extern void CameraDevice__ctor_m2FA90E19434E70B07C24D0AF21104D663E2F1950 ();
// 0x000005A4 System.Boolean easyar.CameraDevice::isAvailable()
extern void CameraDevice_isAvailable_m530C3C01B29037B2C33A29B9A163480051DD085E ();
// 0x000005A5 easyar.AndroidCameraApiType easyar.CameraDevice::androidCameraApiType()
extern void CameraDevice_androidCameraApiType_mE072F58FA31D2E6F6CF5CBF6D9CC7BD6321B1FD7 ();
// 0x000005A6 System.Void easyar.CameraDevice::setAndroidCameraApiType(easyar.AndroidCameraApiType)
extern void CameraDevice_setAndroidCameraApiType_m55F24CBED1BD6D31C3E1589E6EE51E3ECA78EC7C ();
// 0x000005A7 System.Int32 easyar.CameraDevice::bufferCapacity()
extern void CameraDevice_bufferCapacity_mFD4121307C06A0E278B7E00D35FA2BA45E99D4D8 ();
// 0x000005A8 System.Void easyar.CameraDevice::setBufferCapacity(System.Int32)
extern void CameraDevice_setBufferCapacity_m80919A3D1D2D80A03D64A171284BCEB1E119BE78 ();
// 0x000005A9 easyar.InputFrameSource easyar.CameraDevice::inputFrameSource()
extern void CameraDevice_inputFrameSource_m8ED6208393DF0A3DC5AD60915BFAA10594546239 ();
// 0x000005AA System.Void easyar.CameraDevice::setStateChangedCallback(easyar.CallbackScheduler,easyar.Optional`1<System.Action`1<easyar.CameraState>>)
extern void CameraDevice_setStateChangedCallback_mFD90D217DD3B9DEC8DA74660D5417619B89C8362 ();
// 0x000005AB System.Void easyar.CameraDevice::requestPermissions(easyar.CallbackScheduler,easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>)
extern void CameraDevice_requestPermissions_m385B7DEF976F549473CB3296EECDEDBD829BF189 ();
// 0x000005AC System.Int32 easyar.CameraDevice::cameraCount()
extern void CameraDevice_cameraCount_m48AEFEE678EC050FA4EB8C9C6E16F7E649519D56 ();
// 0x000005AD System.Boolean easyar.CameraDevice::openWithIndex(System.Int32)
extern void CameraDevice_openWithIndex_mB70EFF4A8DCFB0637FB0DCEC78980A3174DE37BD ();
// 0x000005AE System.Boolean easyar.CameraDevice::openWithSpecificType(easyar.CameraDeviceType)
extern void CameraDevice_openWithSpecificType_m2B141E413E594595D88FBD9E74BA743A49797169 ();
// 0x000005AF System.Boolean easyar.CameraDevice::openWithPreferredType(easyar.CameraDeviceType)
extern void CameraDevice_openWithPreferredType_mCF3726663592987E7CAFC9E2FC89B07BF91F7DB4 ();
// 0x000005B0 System.Boolean easyar.CameraDevice::start()
extern void CameraDevice_start_m5CAFC134DAD5CB327CED9F74B0F4D8530E8A496B ();
// 0x000005B1 System.Void easyar.CameraDevice::stop()
extern void CameraDevice_stop_m81217BDFB7DF03EC81A2D4CF69D30FAD12321F91 ();
// 0x000005B2 System.Void easyar.CameraDevice::close()
extern void CameraDevice_close_m26E2825C074FB54440C490DE648A76898C8F7212 ();
// 0x000005B3 System.Int32 easyar.CameraDevice::index()
extern void CameraDevice_index_mC31BE8B9B0E6EA7CF28033D8C4FA86ACD0FCBC1E ();
// 0x000005B4 easyar.CameraDeviceType easyar.CameraDevice::type()
extern void CameraDevice_type_m3E8352C6A47182939DDF3401AECDBF4493F7C2CF ();
// 0x000005B5 easyar.CameraParameters easyar.CameraDevice::cameraParameters()
extern void CameraDevice_cameraParameters_mA44C17598E2FB8E6A7142CEEAA0D204581CEC830 ();
// 0x000005B6 System.Void easyar.CameraDevice::setCameraParameters(easyar.CameraParameters)
extern void CameraDevice_setCameraParameters_m0A10FA5A9AB96A06F57BEDD37F2210228854DDF7 ();
// 0x000005B7 easyar.Vec2I easyar.CameraDevice::size()
extern void CameraDevice_size_m3C2FA97FDB64ECE7425366801CE8D4DAFCA23A81 ();
// 0x000005B8 System.Int32 easyar.CameraDevice::supportedSizeCount()
extern void CameraDevice_supportedSizeCount_mC58FB766DCE1CC1042C901FA71217797FE5F3143 ();
// 0x000005B9 easyar.Vec2I easyar.CameraDevice::supportedSize(System.Int32)
extern void CameraDevice_supportedSize_m075A98541DBADE0D29512AF513AC248865816D2D ();
// 0x000005BA System.Boolean easyar.CameraDevice::setSize(easyar.Vec2I)
extern void CameraDevice_setSize_m21926089D451365F41FAE83EC67C4BA256D95E25 ();
// 0x000005BB System.Int32 easyar.CameraDevice::supportedFrameRateRangeCount()
extern void CameraDevice_supportedFrameRateRangeCount_m304D120ABE82081B5B5B5644266A6FB18C5100A2 ();
// 0x000005BC System.Single easyar.CameraDevice::supportedFrameRateRangeLower(System.Int32)
extern void CameraDevice_supportedFrameRateRangeLower_mA073242AC55A29E22612A898D38BB30D0E25C5EB ();
// 0x000005BD System.Single easyar.CameraDevice::supportedFrameRateRangeUpper(System.Int32)
extern void CameraDevice_supportedFrameRateRangeUpper_mFF7615371ADD28916C4A70DF34FFCF37F415C05B ();
// 0x000005BE System.Int32 easyar.CameraDevice::frameRateRange()
extern void CameraDevice_frameRateRange_m8E2244301AC2EA334BCA7D1E11EB793D137A0065 ();
// 0x000005BF System.Boolean easyar.CameraDevice::setFrameRateRange(System.Int32)
extern void CameraDevice_setFrameRateRange_m82B29185FEE6179E8996C98B6D431C824CF9DBD8 ();
// 0x000005C0 System.Boolean easyar.CameraDevice::setFlashTorchMode(System.Boolean)
extern void CameraDevice_setFlashTorchMode_mF2EA25CCE90F281FAFABCC4AC8979F0F65262EB7 ();
// 0x000005C1 System.Boolean easyar.CameraDevice::setFocusMode(easyar.CameraDeviceFocusMode)
extern void CameraDevice_setFocusMode_m6B1965177C3FE08A0D15E0F4A0CDA62E4332CED4 ();
// 0x000005C2 System.Boolean easyar.CameraDevice::autoFocus()
extern void CameraDevice_autoFocus_mAD96E178DDA92847E9BFF2957927802E05D42945 ();
// 0x000005C3 easyar.AndroidCameraApiType easyar.CameraDeviceSelector::getAndroidCameraApiType(easyar.CameraDevicePreference)
extern void CameraDeviceSelector_getAndroidCameraApiType_m84C3FE4160A5A3DDF90FD6F7D9F0288C93C5E31C ();
// 0x000005C4 easyar.CameraDevice easyar.CameraDeviceSelector::createCameraDevice(easyar.CameraDevicePreference)
extern void CameraDeviceSelector_createCameraDevice_mD8DF5A9A0BB0D6C1CEA99490A60EBA3BB02BA6CC ();
// 0x000005C5 System.Void easyar.CameraDeviceSelector::.ctor()
extern void CameraDeviceSelector__ctor_mB5940F284C304E7C909725F60331A90468B5D289 ();
// 0x000005C6 System.Void easyar.SurfaceTrackerResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SurfaceTrackerResult__ctor_m6C768987542EBF5D2D0F5A063DF08E64798534CF ();
// 0x000005C7 System.Object easyar.SurfaceTrackerResult::CloneObject()
extern void SurfaceTrackerResult_CloneObject_m4923C4B7F51DF5D34A5B49CCB7581E5B98C406BC ();
// 0x000005C8 easyar.SurfaceTrackerResult easyar.SurfaceTrackerResult::Clone()
extern void SurfaceTrackerResult_Clone_m361B9E32617F25B0DF171F756448A66C4EEFF5BE ();
// 0x000005C9 easyar.Matrix44F easyar.SurfaceTrackerResult::transform()
extern void SurfaceTrackerResult_transform_m5521BDA6005E280FF45CA891EEBD0A82BA1A9440 ();
// 0x000005CA System.Void easyar.SurfaceTracker::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SurfaceTracker__ctor_mED2AE264CE231828529ACF8C6AF384C06F2B6107 ();
// 0x000005CB System.Object easyar.SurfaceTracker::CloneObject()
extern void SurfaceTracker_CloneObject_m8275D15A247DC0725CCF9A4AC1131D1693B31914 ();
// 0x000005CC easyar.SurfaceTracker easyar.SurfaceTracker::Clone()
extern void SurfaceTracker_Clone_m571A1C2A4EBC0A603744E76352F8693C5A9ADBA7 ();
// 0x000005CD System.Boolean easyar.SurfaceTracker::isAvailable()
extern void SurfaceTracker_isAvailable_m7EA290BF53FC8CC57B12E566D85E3B87DE873E74 ();
// 0x000005CE easyar.InputFrameSink easyar.SurfaceTracker::inputFrameSink()
extern void SurfaceTracker_inputFrameSink_mEF5A5EF815AC8399B6E7AE4661C53F6E561D3204 ();
// 0x000005CF System.Int32 easyar.SurfaceTracker::bufferRequirement()
extern void SurfaceTracker_bufferRequirement_mD181F96A05C50777B019FD4BB80639D1CF0661F4 ();
// 0x000005D0 easyar.OutputFrameSource easyar.SurfaceTracker::outputFrameSource()
extern void SurfaceTracker_outputFrameSource_mFC28738A504820CA1A5F015D2D42C507D9191D7C ();
// 0x000005D1 easyar.SurfaceTracker easyar.SurfaceTracker::create()
extern void SurfaceTracker_create_m0DB6DC130EFB7BA72DE462D893DCFE7C6EE12FAD ();
// 0x000005D2 System.Boolean easyar.SurfaceTracker::start()
extern void SurfaceTracker_start_m0D24241B0A9CEEA9C337E7C099E6D431C7D4C350 ();
// 0x000005D3 System.Void easyar.SurfaceTracker::stop()
extern void SurfaceTracker_stop_mA4C0AA1E5BE654DC35BBBE8CEBEB33C0C19B31D3 ();
// 0x000005D4 System.Void easyar.SurfaceTracker::close()
extern void SurfaceTracker_close_m138B26AA98A03C39235091F376293CA224052117 ();
// 0x000005D5 System.Void easyar.SurfaceTracker::alignTargetToCameraImagePoint(easyar.Vec2F)
extern void SurfaceTracker_alignTargetToCameraImagePoint_m42A9A94CCE72FAA50DEE22257EC812A8A07BCE57 ();
// 0x000005D6 System.Void easyar.MotionTrackerCameraDevice::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void MotionTrackerCameraDevice__ctor_m57B5A7240CC9FE7C81612AA12C5A419C06708589 ();
// 0x000005D7 System.Object easyar.MotionTrackerCameraDevice::CloneObject()
extern void MotionTrackerCameraDevice_CloneObject_mEC2B625CA42C33B1F1ECBA50067DC006BBE08436 ();
// 0x000005D8 easyar.MotionTrackerCameraDevice easyar.MotionTrackerCameraDevice::Clone()
extern void MotionTrackerCameraDevice_Clone_m7EB25CE42FE5634809404EE32A80F9EE2E1F247E ();
// 0x000005D9 System.Void easyar.MotionTrackerCameraDevice::.ctor()
extern void MotionTrackerCameraDevice__ctor_mC8E12CC08516C5CC18A57C3B0E8F5CA427B25494 ();
// 0x000005DA System.Boolean easyar.MotionTrackerCameraDevice::isAvailable()
extern void MotionTrackerCameraDevice_isAvailable_mD600F0E6699F38E9F082D17FD5E7E5F67893066E ();
// 0x000005DB System.Void easyar.MotionTrackerCameraDevice::setBufferCapacity(System.Int32)
extern void MotionTrackerCameraDevice_setBufferCapacity_mC92A34BF2C83A300879AFF22328FDD6DB8DE5277 ();
// 0x000005DC System.Int32 easyar.MotionTrackerCameraDevice::bufferCapacity()
extern void MotionTrackerCameraDevice_bufferCapacity_m28B1EFAADA72D6787D403569D6C35DCC62E45BEC ();
// 0x000005DD easyar.InputFrameSource easyar.MotionTrackerCameraDevice::inputFrameSource()
extern void MotionTrackerCameraDevice_inputFrameSource_m52E4498BB374B41389B82154A3E1DAA5404741F3 ();
// 0x000005DE System.Boolean easyar.MotionTrackerCameraDevice::start()
extern void MotionTrackerCameraDevice_start_m4CFC0000C706FA21752FD405B3CBB82EAE88DEED ();
// 0x000005DF System.Void easyar.MotionTrackerCameraDevice::stop()
extern void MotionTrackerCameraDevice_stop_m4A94DD450862FA09739B1D612056D58E60D54997 ();
// 0x000005E0 System.Void easyar.MotionTrackerCameraDevice::close()
extern void MotionTrackerCameraDevice_close_mDFA3402D5FD04D0264E628957433C22968216FA4 ();
// 0x000005E1 System.Collections.Generic.List`1<easyar.Vec3F> easyar.MotionTrackerCameraDevice::hitTestAgainstPointCloud(easyar.Vec2F)
extern void MotionTrackerCameraDevice_hitTestAgainstPointCloud_m26BBC2693B10BFC7EABA4DE341277D448446B90D ();
// 0x000005E2 System.Collections.Generic.List`1<easyar.Vec3F> easyar.MotionTrackerCameraDevice::hitTestAgainstHorizontalPlane(easyar.Vec2F)
extern void MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane_m072F40350A5A085A7A34B34B4C57EDCBB6075014 ();
// 0x000005E3 System.Collections.Generic.List`1<easyar.Vec3F> easyar.MotionTrackerCameraDevice::getLocalPointsCloud()
extern void MotionTrackerCameraDevice_getLocalPointsCloud_mEBA0DA3DFA8C8A9735FAF4DFB75B36ADE3C84B7A ();
// 0x000005E4 System.Void easyar.InputFrameRecorder::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFrameRecorder__ctor_m43BE1319B97EED39D5E96A35430A1ECDBEC7BB93 ();
// 0x000005E5 System.Object easyar.InputFrameRecorder::CloneObject()
extern void InputFrameRecorder_CloneObject_m44F7EC1E1A08BFAF134ADCEC1264194417BAB102 ();
// 0x000005E6 easyar.InputFrameRecorder easyar.InputFrameRecorder::Clone()
extern void InputFrameRecorder_Clone_mF4ED1C7DC454DD071B56368E057F7CEEB238D24D ();
// 0x000005E7 easyar.InputFrameSink easyar.InputFrameRecorder::input()
extern void InputFrameRecorder_input_m2342B7BC3761D54FD5B868017B4E2D1E86E8CCC1 ();
// 0x000005E8 System.Int32 easyar.InputFrameRecorder::bufferRequirement()
extern void InputFrameRecorder_bufferRequirement_mC3AC54044769739504C0608EB2BCAAB17DC967C7 ();
// 0x000005E9 easyar.InputFrameSource easyar.InputFrameRecorder::output()
extern void InputFrameRecorder_output_m2D2D1282EF809F661E7E92F02FC02F5249D0D07B ();
// 0x000005EA easyar.InputFrameRecorder easyar.InputFrameRecorder::create()
extern void InputFrameRecorder_create_m45FA4818FC9A27A0C29748659819B8100FE3CB35 ();
// 0x000005EB System.Boolean easyar.InputFrameRecorder::start(System.String)
extern void InputFrameRecorder_start_m0439D12F4274989AAFB62F8550652899FA31ACD7 ();
// 0x000005EC System.Void easyar.InputFrameRecorder::stop()
extern void InputFrameRecorder_stop_m24FBBE5EF3FB76235F706AEAC817C563FFBA6EF4 ();
// 0x000005ED System.Void easyar.InputFramePlayer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFramePlayer__ctor_m99BD91151E38B291CC233F90CF1916BF79584F41 ();
// 0x000005EE System.Object easyar.InputFramePlayer::CloneObject()
extern void InputFramePlayer_CloneObject_m8FC2937E2F33AF3485B0A6613319F2D2F96A8F76 ();
// 0x000005EF easyar.InputFramePlayer easyar.InputFramePlayer::Clone()
extern void InputFramePlayer_Clone_m54986064E9B8A24C321EBA60C454E170124E8E02 ();
// 0x000005F0 easyar.InputFrameSource easyar.InputFramePlayer::output()
extern void InputFramePlayer_output_mC104378474F3A04A5214177508BB57586662A487 ();
// 0x000005F1 easyar.InputFramePlayer easyar.InputFramePlayer::create()
extern void InputFramePlayer_create_mDE0AEFE1F079FEA346270845DA4B58716CE8C390 ();
// 0x000005F2 System.Boolean easyar.InputFramePlayer::start(System.String)
extern void InputFramePlayer_start_mF01ECBE35EB399B163D73A3C38A697731271A8E9 ();
// 0x000005F3 System.Void easyar.InputFramePlayer::stop()
extern void InputFramePlayer_stop_m23E87DE21FFE07EEA9C9A28E5C8D7F80FACB7D66 ();
// 0x000005F4 System.Void easyar.CallbackScheduler::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void CallbackScheduler__ctor_m5828981066B852891E3A9DDF0DD34839E41A10B5 ();
// 0x000005F5 System.Object easyar.CallbackScheduler::CloneObject()
extern void CallbackScheduler_CloneObject_mD515F2E036E8DBCC7F2EC3B42524A22C254E8879 ();
// 0x000005F6 easyar.CallbackScheduler easyar.CallbackScheduler::Clone()
extern void CallbackScheduler_Clone_mC584C80A8EDB68EB4B3563DD390C325C19117ECB ();
// 0x000005F7 System.Void easyar.DelayedCallbackScheduler::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void DelayedCallbackScheduler__ctor_mEC400BEE74C0EF24D1A29020FB96A22C784623FC ();
// 0x000005F8 System.Object easyar.DelayedCallbackScheduler::CloneObject()
extern void DelayedCallbackScheduler_CloneObject_m744C039C8B1DBC7524205C16FD689A1EB0768990 ();
// 0x000005F9 easyar.DelayedCallbackScheduler easyar.DelayedCallbackScheduler::Clone()
extern void DelayedCallbackScheduler_Clone_mF957276765CFC926261509EF13C42B75C8D353B1 ();
// 0x000005FA System.Void easyar.DelayedCallbackScheduler::.ctor()
extern void DelayedCallbackScheduler__ctor_m8C635EB11F38F5131DBD050E13B1539E5584187C ();
// 0x000005FB System.Boolean easyar.DelayedCallbackScheduler::runOne()
extern void DelayedCallbackScheduler_runOne_mA003E232CA0E93ED3BCE128CE7672E74535866CA ();
// 0x000005FC System.Void easyar.ImmediateCallbackScheduler::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ImmediateCallbackScheduler__ctor_m4C1B7CD9AB1B4D57A623D179DF1B4AD3DD64EC1D ();
// 0x000005FD System.Object easyar.ImmediateCallbackScheduler::CloneObject()
extern void ImmediateCallbackScheduler_CloneObject_m3E0B3059A719E1E92A7E9515748C177E07C1514C ();
// 0x000005FE easyar.ImmediateCallbackScheduler easyar.ImmediateCallbackScheduler::Clone()
extern void ImmediateCallbackScheduler_Clone_m887060FBFD1470965001F596BCC9605BE3F05997 ();
// 0x000005FF easyar.ImmediateCallbackScheduler easyar.ImmediateCallbackScheduler::getDefault()
extern void ImmediateCallbackScheduler_getDefault_m24BB5A3EE463D63163025DFCE487507C7373BBB4 ();
// 0x00000600 easyar.Buffer easyar.JniUtility::wrapByteArray(System.IntPtr,System.Boolean,System.Action)
extern void JniUtility_wrapByteArray_mF6185FC87B0C7B8C889685810E0B31B7C55D2622 ();
// 0x00000601 easyar.Buffer easyar.JniUtility::wrapBuffer(System.IntPtr,System.Action)
extern void JniUtility_wrapBuffer_m1E9C149CFFD94D7CF96247D8469D57CBCF40A5AD ();
// 0x00000602 System.IntPtr easyar.JniUtility::getDirectBufferAddress(System.IntPtr)
extern void JniUtility_getDirectBufferAddress_mD5B9B192FE7389A473FD13324253E2B37DEEB251 ();
// 0x00000603 System.Void easyar.JniUtility::.ctor()
extern void JniUtility__ctor_mF515E0A8542C84494F29A038D34301498809D74E ();
// 0x00000604 System.Void easyar.Log::setLogFunc(System.Action`2<easyar.LogLevel,System.String>)
extern void Log_setLogFunc_m81B30F66F50C42B99154E24B68FD697D7C5C3B62 ();
// 0x00000605 System.Void easyar.Log::resetLogFunc()
extern void Log_resetLogFunc_m51CBE380CA4BD4277690293A734C73D098C28C4C ();
// 0x00000606 System.Void easyar.Log::.ctor()
extern void Log__ctor_mC9423CFE26EAD59EE4CB3911F79189E176967165 ();
// 0x00000607 System.Void easyar.ImageTargetParameters::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ImageTargetParameters__ctor_m4B2EEF4D080B77098494ED10C7E083EB96CB71F3 ();
// 0x00000608 System.Object easyar.ImageTargetParameters::CloneObject()
extern void ImageTargetParameters_CloneObject_mA32BBAD758D63894972386358ADB1F38FBE1706E ();
// 0x00000609 easyar.ImageTargetParameters easyar.ImageTargetParameters::Clone()
extern void ImageTargetParameters_Clone_m00AA35FD6C2E0DC13A7A05D24444BB2290D832E6 ();
// 0x0000060A System.Void easyar.ImageTargetParameters::.ctor()
extern void ImageTargetParameters__ctor_m5B0F12026B64B604BCA8BA6792625F3CF8ACF574 ();
// 0x0000060B easyar.Image easyar.ImageTargetParameters::image()
extern void ImageTargetParameters_image_m605B7A73AF89D700377406A9DCD4283855E7C5AC ();
// 0x0000060C System.Void easyar.ImageTargetParameters::setImage(easyar.Image)
extern void ImageTargetParameters_setImage_mD70665FDD7BDD1A47037E2DF625373497E935182 ();
// 0x0000060D System.String easyar.ImageTargetParameters::name()
extern void ImageTargetParameters_name_m8C26F413355DCA95BDE11985D49B1C031FF2537A ();
// 0x0000060E System.Void easyar.ImageTargetParameters::setName(System.String)
extern void ImageTargetParameters_setName_mCFE9DCDF8B314B6ECF48B321C0B88A14973069CF ();
// 0x0000060F System.String easyar.ImageTargetParameters::uid()
extern void ImageTargetParameters_uid_mA75CB571167FACE277D523DFBA11A1F31749EEAA ();
// 0x00000610 System.Void easyar.ImageTargetParameters::setUid(System.String)
extern void ImageTargetParameters_setUid_m710956A899A31881C4A4E59DD1EF12EC0B92A4E8 ();
// 0x00000611 System.String easyar.ImageTargetParameters::meta()
extern void ImageTargetParameters_meta_mF9737B4162C835D67D45CB5E6E44258DB207E281 ();
// 0x00000612 System.Void easyar.ImageTargetParameters::setMeta(System.String)
extern void ImageTargetParameters_setMeta_m343B785EAED5DF67642903B216DD1AA7341BE297 ();
// 0x00000613 System.Single easyar.ImageTargetParameters::scale()
extern void ImageTargetParameters_scale_mA7BEAEB95302A9CE9CA75983E0FF3FDAF95454FB ();
// 0x00000614 System.Void easyar.ImageTargetParameters::setScale(System.Single)
extern void ImageTargetParameters_setScale_m576CBFA4531FD6029004AEB72E183041024ACE50 ();
// 0x00000615 System.Void easyar.ImageTarget::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ImageTarget__ctor_m47672761C80BC4CA7A448EE4012F0A5A6D9802B4 ();
// 0x00000616 System.Object easyar.ImageTarget::CloneObject()
extern void ImageTarget_CloneObject_mA66826695A6E4E0982E83659391DBD242338B269 ();
// 0x00000617 easyar.ImageTarget easyar.ImageTarget::Clone()
extern void ImageTarget_Clone_m8F11DFEEDF09754FA10C67532E4D602C2A239077 ();
// 0x00000618 System.Void easyar.ImageTarget::.ctor()
extern void ImageTarget__ctor_m658DB811C59216FCB059EF3B2E0E0618E7A73469 ();
// 0x00000619 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget::createFromParameters(easyar.ImageTargetParameters)
extern void ImageTarget_createFromParameters_m3689AC4639EC0AAAE720F337E04623629DC8853A ();
// 0x0000061A easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget::createFromTargetFile(System.String,easyar.StorageType)
extern void ImageTarget_createFromTargetFile_m9C102DA5FBDF3B51145DA1C1A29658570AC7E758 ();
// 0x0000061B easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget::createFromTargetData(easyar.Buffer)
extern void ImageTarget_createFromTargetData_m5DE83144C00A9A94264430B5DBD9FB684905276C ();
// 0x0000061C System.Boolean easyar.ImageTarget::save(System.String)
extern void ImageTarget_save_m235CF3791319CFBD49DC0E2839C1B9F056C8BCF9 ();
// 0x0000061D easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget::createFromImageFile(System.String,easyar.StorageType,System.String,System.String,System.String,System.Single)
extern void ImageTarget_createFromImageFile_m362E0BE082329B37728E57FD2AC45EA11C656BEC ();
// 0x0000061E System.Single easyar.ImageTarget::scale()
extern void ImageTarget_scale_m8DE7CA1940ED8B4E867137F10BAC799249BD7EF9 ();
// 0x0000061F System.Single easyar.ImageTarget::aspectRatio()
extern void ImageTarget_aspectRatio_mBB18288A2A18753EB105857FFED3FDBAAB356C70 ();
// 0x00000620 System.Boolean easyar.ImageTarget::setScale(System.Single)
extern void ImageTarget_setScale_m4FC8D6296F36C79AEFAAFA6984C6DFC4407244E7 ();
// 0x00000621 System.Collections.Generic.List`1<easyar.Image> easyar.ImageTarget::images()
extern void ImageTarget_images_mF8607048B4D9629F355080F267AAC17D4D712FEB ();
// 0x00000622 System.Int32 easyar.ImageTarget::runtimeID()
extern void ImageTarget_runtimeID_m83A7796F2AD354017DC95B27E4AE616FA440F420 ();
// 0x00000623 System.String easyar.ImageTarget::uid()
extern void ImageTarget_uid_mDB9E38D77A302595B863317991A1A5B2E9CA9881 ();
// 0x00000624 System.String easyar.ImageTarget::name()
extern void ImageTarget_name_m32F4BE54EFC7609B7DFEFBA13015F94EF376FFFB ();
// 0x00000625 System.Void easyar.ImageTarget::setName(System.String)
extern void ImageTarget_setName_mB13762EDB0E4F7315F2827DC8CF7878D7A407CD0 ();
// 0x00000626 System.String easyar.ImageTarget::meta()
extern void ImageTarget_meta_m3390E50ECE00FEC9C770A05982C6A6806CD7E6CD ();
// 0x00000627 System.Void easyar.ImageTarget::setMeta(System.String)
extern void ImageTarget_setMeta_mD7E88D35EA400BF1DBA0B0BE670DDA9AB488D5A3 ();
// 0x00000628 System.Void easyar.ImageTrackerResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ImageTrackerResult__ctor_m07D03C71193714D61F8202FB43573E80C0BB7484 ();
// 0x00000629 System.Object easyar.ImageTrackerResult::CloneObject()
extern void ImageTrackerResult_CloneObject_mAB071B65E9986B1D6C1318D80604D36681A7F96E ();
// 0x0000062A easyar.ImageTrackerResult easyar.ImageTrackerResult::Clone()
extern void ImageTrackerResult_Clone_m901701FCE18B7BBDB8F905A04B184DBEA17C2F51 ();
// 0x0000062B System.Collections.Generic.List`1<easyar.TargetInstance> easyar.ImageTrackerResult::targetInstances()
extern void ImageTrackerResult_targetInstances_m93E0A7439778818844091B82B85407483FF66662 ();
// 0x0000062C System.Void easyar.ImageTrackerResult::setTargetInstances(System.Collections.Generic.List`1<easyar.TargetInstance>)
extern void ImageTrackerResult_setTargetInstances_m8D27851FEB96C2D538B9AA599FBC97E0CDF35EA2 ();
// 0x0000062D System.Void easyar.ImageTracker::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void ImageTracker__ctor_m3006366FE899F8DA2D642CB186B3BD41EDFDE9B7 ();
// 0x0000062E System.Object easyar.ImageTracker::CloneObject()
extern void ImageTracker_CloneObject_m6454933A905756F1754CFDEF0B50C9B5C75006C6 ();
// 0x0000062F easyar.ImageTracker easyar.ImageTracker::Clone()
extern void ImageTracker_Clone_m7AF75A53B97930291D8FCA9D5A7DF8133AD07E40 ();
// 0x00000630 System.Boolean easyar.ImageTracker::isAvailable()
extern void ImageTracker_isAvailable_m74AD1EDAB965AE892A9F4B7C164B246DA6726A32 ();
// 0x00000631 easyar.FeedbackFrameSink easyar.ImageTracker::feedbackFrameSink()
extern void ImageTracker_feedbackFrameSink_m61FDC233D2298EA58F7F94866DC3C045E1C6D08E ();
// 0x00000632 System.Int32 easyar.ImageTracker::bufferRequirement()
extern void ImageTracker_bufferRequirement_mC5D61F7C0D0520A8C5F601A6BD0A353B2E761C05 ();
// 0x00000633 easyar.OutputFrameSource easyar.ImageTracker::outputFrameSource()
extern void ImageTracker_outputFrameSource_m68F1C7B4AACB2DE6C0169770904A7FD86242FD37 ();
// 0x00000634 easyar.ImageTracker easyar.ImageTracker::create()
extern void ImageTracker_create_mF941F588172564074526EF4D9271C6369818F55A ();
// 0x00000635 easyar.ImageTracker easyar.ImageTracker::createWithMode(easyar.ImageTrackerMode)
extern void ImageTracker_createWithMode_mF0A7621A2397E62089AE274F5A4720A98DFCD1A8 ();
// 0x00000636 System.Boolean easyar.ImageTracker::start()
extern void ImageTracker_start_mDAC8D2852D32658AABE0AB3C44E378EEF22B031C ();
// 0x00000637 System.Void easyar.ImageTracker::stop()
extern void ImageTracker_stop_mD26F43D3027BF4FFE002E6ACE6540D67EB11CB41 ();
// 0x00000638 System.Void easyar.ImageTracker::close()
extern void ImageTracker_close_mF9113528B070B6212778E6BAC6C4997766E00C45 ();
// 0x00000639 System.Void easyar.ImageTracker::loadTarget(easyar.Target,easyar.CallbackScheduler,System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTracker_loadTarget_m66A0F12CA832EF99D867D39AEB61C43BF9A5E38E ();
// 0x0000063A System.Void easyar.ImageTracker::unloadTarget(easyar.Target,easyar.CallbackScheduler,System.Action`2<easyar.Target,System.Boolean>)
extern void ImageTracker_unloadTarget_m01A8E69115564BD328B3FA5AFF6D7F1A9E7EABCC ();
// 0x0000063B System.Collections.Generic.List`1<easyar.Target> easyar.ImageTracker::targets()
extern void ImageTracker_targets_m2CB1BBC5BAB16AB626D2D3209E1C4BA811DAF239 ();
// 0x0000063C System.Boolean easyar.ImageTracker::setSimultaneousNum(System.Int32)
extern void ImageTracker_setSimultaneousNum_m544539088BC598808329127B3115C5DE0BB01122 ();
// 0x0000063D System.Int32 easyar.ImageTracker::simultaneousNum()
extern void ImageTracker_simultaneousNum_m20CFA25ED64B0E523945826527FE5E98C0DD92E0 ();
// 0x0000063E System.Void easyar.Recorder::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void Recorder__ctor_m24BBAFD0585A7E34D3A957D313DF9E04B4264C12 ();
// 0x0000063F System.Object easyar.Recorder::CloneObject()
extern void Recorder_CloneObject_m83CCD2759407A0BB009DE1E0C34B676D6F451E6E ();
// 0x00000640 easyar.Recorder easyar.Recorder::Clone()
extern void Recorder_Clone_m0DF5EAEFD2B3AEC96F55A71B3A52B1CB234DBB85 ();
// 0x00000641 System.Boolean easyar.Recorder::isAvailable()
extern void Recorder_isAvailable_mFE1489E8BB02F3C705F89F223A3CC4BAA4987EB8 ();
// 0x00000642 System.Void easyar.Recorder::requestPermissions(easyar.CallbackScheduler,easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>)
extern void Recorder_requestPermissions_m577700A23BEAE10166A5973D221A6C0CEF606498 ();
// 0x00000643 easyar.Recorder easyar.Recorder::create(easyar.RecorderConfiguration,easyar.CallbackScheduler,easyar.Optional`1<System.Action`2<easyar.RecordStatus,System.String>>)
extern void Recorder_create_m2A4B121D0D90BAAD1FB327A2CED247D16FF3C73A ();
// 0x00000644 System.Void easyar.Recorder::start()
extern void Recorder_start_mDDAD6CC4548015B1C9495FAA701BB0CDF77B1374 ();
// 0x00000645 System.Void easyar.Recorder::updateFrame(easyar.TextureId,System.Int32,System.Int32)
extern void Recorder_updateFrame_m6FA968FAA00FDD65BA56ACBA11B2B00CFEFD8E6E ();
// 0x00000646 System.Boolean easyar.Recorder::stop()
extern void Recorder_stop_m1EB17B6A34CADD4C11F1CA960B7826DBD5E17220 ();
// 0x00000647 System.Void easyar.RecorderConfiguration::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void RecorderConfiguration__ctor_mDC7EE1124413E701C3983ECB1282EEFCFB850920 ();
// 0x00000648 System.Object easyar.RecorderConfiguration::CloneObject()
extern void RecorderConfiguration_CloneObject_mF3ADCEDA812096392908E46B5AE7C548CC710C07 ();
// 0x00000649 easyar.RecorderConfiguration easyar.RecorderConfiguration::Clone()
extern void RecorderConfiguration_Clone_m332FA07444B984EA4BF601F54E9A103CE560D5B2 ();
// 0x0000064A System.Void easyar.RecorderConfiguration::.ctor()
extern void RecorderConfiguration__ctor_m3DD2857E8F0704C9B1D851BFF4FB8F2D790AB202 ();
// 0x0000064B System.Void easyar.RecorderConfiguration::setOutputFile(System.String)
extern void RecorderConfiguration_setOutputFile_mA4C04B34F6C45F4DF23D78D8AFE53DF077DAE3DD ();
// 0x0000064C System.Boolean easyar.RecorderConfiguration::setProfile(easyar.RecordProfile)
extern void RecorderConfiguration_setProfile_m8552363569DC1C5D384B06E8CD7B3E29A287E3F0 ();
// 0x0000064D System.Void easyar.RecorderConfiguration::setVideoSize(easyar.RecordVideoSize)
extern void RecorderConfiguration_setVideoSize_m05E9D0429927E863CC61228C913A079248833F4C ();
// 0x0000064E System.Void easyar.RecorderConfiguration::setVideoBitrate(System.Int32)
extern void RecorderConfiguration_setVideoBitrate_m2BEE30F6A7E3049E9B9BF167880F616208E6E9E1 ();
// 0x0000064F System.Void easyar.RecorderConfiguration::setChannelCount(System.Int32)
extern void RecorderConfiguration_setChannelCount_m65360B2BA00C9E7879BA257F78AF3EC6782D318F ();
// 0x00000650 System.Void easyar.RecorderConfiguration::setAudioSampleRate(System.Int32)
extern void RecorderConfiguration_setAudioSampleRate_m76A713DEEC41EE3169BD715970D9E4E90B7A0476 ();
// 0x00000651 System.Void easyar.RecorderConfiguration::setAudioBitrate(System.Int32)
extern void RecorderConfiguration_setAudioBitrate_m33827C31259D8452AE7132A864A68B8634F393A2 ();
// 0x00000652 System.Void easyar.RecorderConfiguration::setVideoOrientation(easyar.RecordVideoOrientation)
extern void RecorderConfiguration_setVideoOrientation_mF4E6F062EAA7293E323F5F2E07FD5AB8C908AD65 ();
// 0x00000653 System.Void easyar.RecorderConfiguration::setZoomMode(easyar.RecordZoomMode)
extern void RecorderConfiguration_setZoomMode_m0EDB4D7114B019AE9D2C07591B908A8544B5857D ();
// 0x00000654 System.Void easyar.SparseSpatialMapResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SparseSpatialMapResult__ctor_mA4BC247C4F5E166125C827FDD37BE166F4E0FE87 ();
// 0x00000655 System.Object easyar.SparseSpatialMapResult::CloneObject()
extern void SparseSpatialMapResult_CloneObject_m00812B106E2844C540BD3877FD9F258EC6A5A6D5 ();
// 0x00000656 easyar.SparseSpatialMapResult easyar.SparseSpatialMapResult::Clone()
extern void SparseSpatialMapResult_Clone_mC008190C1F980CC88CD385C0B3DCBF0FC2491E6A ();
// 0x00000657 easyar.MotionTrackingStatus easyar.SparseSpatialMapResult::getMotionTrackingStatus()
extern void SparseSpatialMapResult_getMotionTrackingStatus_mBA954A35702019855B1EB95C05B3160A3B8A4541 ();
// 0x00000658 easyar.Optional`1<easyar.Matrix44F> easyar.SparseSpatialMapResult::getVioPose()
extern void SparseSpatialMapResult_getVioPose_mB2B5D20D452484D10F16D29BCB2526D87E5BB47E ();
// 0x00000659 easyar.Optional`1<easyar.Matrix44F> easyar.SparseSpatialMapResult::getMapPose()
extern void SparseSpatialMapResult_getMapPose_mED1F7294E40EB7E69327DB5B0EF5229C12D1C3CC ();
// 0x0000065A System.Boolean easyar.SparseSpatialMapResult::getLocalizationStatus()
extern void SparseSpatialMapResult_getLocalizationStatus_m6932D0664FF538839690059CEE0C8106DAD0BA4A ();
// 0x0000065B System.String easyar.SparseSpatialMapResult::getLocalizationMapID()
extern void SparseSpatialMapResult_getLocalizationMapID_m7250B8714B38DB5DA58345A628B59CF2443533DA ();
// 0x0000065C System.Void easyar.PlaneData::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void PlaneData__ctor_m9D4519A545C478A3E36C82BB5FC1EC4C9029D8BF ();
// 0x0000065D System.Object easyar.PlaneData::CloneObject()
extern void PlaneData_CloneObject_m7490952E9D264F0F2F2C1DB75479FD647F575B84 ();
// 0x0000065E easyar.PlaneData easyar.PlaneData::Clone()
extern void PlaneData_Clone_m46275770937516CDE988ABB62A75C420C727A401 ();
// 0x0000065F System.Void easyar.PlaneData::.ctor()
extern void PlaneData__ctor_m2A1DEB44030FD736200F90D8A3F97C2792B58206 ();
// 0x00000660 easyar.PlaneType easyar.PlaneData::getType()
extern void PlaneData_getType_m355402A12E0B86A5A2AFD628BD09DEC4562B9B8E ();
// 0x00000661 easyar.Matrix44F easyar.PlaneData::getPose()
extern void PlaneData_getPose_m16F65C234918F0F37E5131A207664C6B08E1D6B9 ();
// 0x00000662 System.Single easyar.PlaneData::getExtentX()
extern void PlaneData_getExtentX_m2485C8B6D55C8EC98A60663C2641F529830C91BE ();
// 0x00000663 System.Single easyar.PlaneData::getExtentZ()
extern void PlaneData_getExtentZ_mAEF9B0C55FBB4346A9B82461E734CACF8AA924DD ();
// 0x00000664 System.Void easyar.SparseSpatialMapConfig::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SparseSpatialMapConfig__ctor_mBA3BBFCB75C9B959080A7DFB868CABBE16FCBA43 ();
// 0x00000665 System.Object easyar.SparseSpatialMapConfig::CloneObject()
extern void SparseSpatialMapConfig_CloneObject_m9200281DEA7765F2D151B2A9B6E51DA0F6F827EA ();
// 0x00000666 easyar.SparseSpatialMapConfig easyar.SparseSpatialMapConfig::Clone()
extern void SparseSpatialMapConfig_Clone_m629986273F8B3BD9CCD28AE5ACFBA648694BAC8D ();
// 0x00000667 System.Void easyar.SparseSpatialMapConfig::.ctor()
extern void SparseSpatialMapConfig__ctor_m74F38BC5029D2AE098A5B4B3F09C46F2AC2D6EFA ();
// 0x00000668 System.Void easyar.SparseSpatialMapConfig::setLocalizationMode(easyar.LocalizationMode)
extern void SparseSpatialMapConfig_setLocalizationMode_mC92EDD1DC4A8A12252008F0751312CF87A81855B ();
// 0x00000669 easyar.LocalizationMode easyar.SparseSpatialMapConfig::getLocalizationMode()
extern void SparseSpatialMapConfig_getLocalizationMode_mE7CF51AC93F699DB644AD407038ABD161FD826AE ();
// 0x0000066A System.Void easyar.SparseSpatialMap::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SparseSpatialMap__ctor_m66B259AF104470FA4512787F5F139679103A5D22 ();
// 0x0000066B System.Object easyar.SparseSpatialMap::CloneObject()
extern void SparseSpatialMap_CloneObject_mE00EC86831A8EAFD0BC468431E9A6D3D5130AE80 ();
// 0x0000066C easyar.SparseSpatialMap easyar.SparseSpatialMap::Clone()
extern void SparseSpatialMap_Clone_mA9BDB069BC44523B561EC0151295E81893B5D867 ();
// 0x0000066D System.Boolean easyar.SparseSpatialMap::isAvailable()
extern void SparseSpatialMap_isAvailable_m05F7E213365A882F09C801E746DDF7358EA6511D ();
// 0x0000066E easyar.InputFrameSink easyar.SparseSpatialMap::inputFrameSink()
extern void SparseSpatialMap_inputFrameSink_m0609B8ACEFBCFC9E8EB4B8BD897B87D358A9691F ();
// 0x0000066F System.Int32 easyar.SparseSpatialMap::bufferRequirement()
extern void SparseSpatialMap_bufferRequirement_mD5FF3D3866A8EA0BC258A9DD885CF0E6C1A1EFC8 ();
// 0x00000670 easyar.OutputFrameSource easyar.SparseSpatialMap::outputFrameSource()
extern void SparseSpatialMap_outputFrameSource_mC2D376662B0031319B30F1461BE4EDC89990FBF4 ();
// 0x00000671 easyar.SparseSpatialMap easyar.SparseSpatialMap::create()
extern void SparseSpatialMap_create_mD69B1E5269ABE6ABCE868B1152C5758A3B1435E1 ();
// 0x00000672 System.Boolean easyar.SparseSpatialMap::start()
extern void SparseSpatialMap_start_m6CD2210FC9DA80017D66F062F39514D0B3B85915 ();
// 0x00000673 System.Void easyar.SparseSpatialMap::stop()
extern void SparseSpatialMap_stop_m4F45D6DC38B92E8C5CAB92A713EF85C57FAD292D ();
// 0x00000674 System.Void easyar.SparseSpatialMap::close()
extern void SparseSpatialMap_close_mF8031C824F5492ACA91C7A6C5E4C73E54566DA51 ();
// 0x00000675 easyar.Buffer easyar.SparseSpatialMap::getPointCloudBuffer()
extern void SparseSpatialMap_getPointCloudBuffer_m53512408C414B6810263013CB37847926306E6CF ();
// 0x00000676 System.Collections.Generic.List`1<easyar.PlaneData> easyar.SparseSpatialMap::getMapPlanes()
extern void SparseSpatialMap_getMapPlanes_m93C5DCA030AE9D94CE35DBC74CFC9EE50FA3B276 ();
// 0x00000677 System.Collections.Generic.List`1<easyar.Vec3F> easyar.SparseSpatialMap::hitTestAgainstPointCloud(easyar.Vec2F)
extern void SparseSpatialMap_hitTestAgainstPointCloud_m47F18FFB6719E17683DDABE157E8AA8B05DF5631 ();
// 0x00000678 System.Collections.Generic.List`1<easyar.Vec3F> easyar.SparseSpatialMap::hitTestAgainstPlanes(easyar.Vec2F)
extern void SparseSpatialMap_hitTestAgainstPlanes_m3EDA7AE162B3FAEE3F11B2DF1B8695A48D1E7761 ();
// 0x00000679 System.String easyar.SparseSpatialMap::getMapVersion()
extern void SparseSpatialMap_getMapVersion_m4DB7F35B8391E60064C3F2B1EE92DFA8CEB7BF99 ();
// 0x0000067A System.Void easyar.SparseSpatialMap::unloadMap(System.String,easyar.CallbackScheduler,easyar.Optional`1<System.Action`1<System.Boolean>>)
extern void SparseSpatialMap_unloadMap_mAA34DEE45585A73737CBA25DAC9229BE1D42587C ();
// 0x0000067B System.Void easyar.SparseSpatialMap::setConfig(easyar.SparseSpatialMapConfig)
extern void SparseSpatialMap_setConfig_m83A36E83654FDFF6A19624B0CC20C5A45DD9C5BF ();
// 0x0000067C easyar.SparseSpatialMapConfig easyar.SparseSpatialMap::getConfig()
extern void SparseSpatialMap_getConfig_mE7D3F22C28F705D00A28D237CF0C487D89E32EDA ();
// 0x0000067D System.Boolean easyar.SparseSpatialMap::startLocalization()
extern void SparseSpatialMap_startLocalization_m674599FDE04A4CEF1EB94AD835827D6ACE774D5C ();
// 0x0000067E System.Void easyar.SparseSpatialMap::stopLocalization()
extern void SparseSpatialMap_stopLocalization_m60376EECB68CB4E2117E0D5DD2199FAE3E6F3EA2 ();
// 0x0000067F System.Void easyar.SparseSpatialMapManager::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SparseSpatialMapManager__ctor_m3502FC968803032AAC163ADB66A5B55C84254C7B ();
// 0x00000680 System.Object easyar.SparseSpatialMapManager::CloneObject()
extern void SparseSpatialMapManager_CloneObject_m23F60ECF959B993CD423A83002FB4C9A8FEE268C ();
// 0x00000681 easyar.SparseSpatialMapManager easyar.SparseSpatialMapManager::Clone()
extern void SparseSpatialMapManager_Clone_m338B3F03208601E3F7ED757B56F8DF5A4FC72851 ();
// 0x00000682 System.Boolean easyar.SparseSpatialMapManager::isAvailable()
extern void SparseSpatialMapManager_isAvailable_m2D14FBC78D6EA442DF83F84F846817367664098B ();
// 0x00000683 easyar.SparseSpatialMapManager easyar.SparseSpatialMapManager::create()
extern void SparseSpatialMapManager_create_m116CF7A2752EAEE001A7507CC2F0B1EF2BBD2F8A ();
// 0x00000684 System.Void easyar.SparseSpatialMapManager::host(easyar.SparseSpatialMap,System.String,System.String,System.String,System.String,easyar.Optional`1<easyar.Image>,easyar.CallbackScheduler,System.Action`3<System.Boolean,System.String,System.String>)
extern void SparseSpatialMapManager_host_m5A278F9F1E5421C29AC6A7CBC2B84C62FF627696 ();
// 0x00000685 System.Void easyar.SparseSpatialMapManager::load(easyar.SparseSpatialMap,System.String,System.String,System.String,System.String,easyar.CallbackScheduler,System.Action`2<System.Boolean,System.String>)
extern void SparseSpatialMapManager_load_m63E7431E4838246C195B185ECBE58B81F8D85B6A ();
// 0x00000686 System.Void easyar.SparseSpatialMapManager::clear()
extern void SparseSpatialMapManager_clear_m6F50BF5DB09E0B08F65344B6FE7D520CF8BD542E ();
// 0x00000687 System.Int32 easyar.Engine::schemaHash()
extern void Engine_schemaHash_mDE0A9124DB616EE38D83778ADE27D9A754CBE997 ();
// 0x00000688 System.Boolean easyar.Engine::initialize(System.String)
extern void Engine_initialize_m113B4F8EB0D249DD2E54256D2A6A3924EFCA5039 ();
// 0x00000689 System.Void easyar.Engine::onPause()
extern void Engine_onPause_mF9C3B0E7C4F9FB4F65CD0179867490BACCF7E5CD ();
// 0x0000068A System.Void easyar.Engine::onResume()
extern void Engine_onResume_m8B1F26F9B090FB4E53CCF1F00CBE8C4CFADF0D3A ();
// 0x0000068B System.String easyar.Engine::errorMessage()
extern void Engine_errorMessage_m93AAADF726BD4852C6E5C13C330EC6ADB5700C49 ();
// 0x0000068C System.String easyar.Engine::versionString()
extern void Engine_versionString_m96AAB5C7799CDC695A30E9555BA6E5F1C706CBA3 ();
// 0x0000068D System.String easyar.Engine::name()
extern void Engine_name_m44436C2B639646EF4731153EDB43E1986A8DA95E ();
// 0x0000068E System.Void easyar.Engine::.ctor()
extern void Engine__ctor_mB9E0C7F1EA878B92AF299CF834E91D8FAFD40C58 ();
// 0x0000068F System.Void easyar.VideoPlayer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void VideoPlayer__ctor_mA247F342C46FD2341AABB764248D0FD6DC0E119A ();
// 0x00000690 System.Object easyar.VideoPlayer::CloneObject()
extern void VideoPlayer_CloneObject_mD64A210D02B19DB00CC9E7FE69A698B35845DC79 ();
// 0x00000691 easyar.VideoPlayer easyar.VideoPlayer::Clone()
extern void VideoPlayer_Clone_m00CF883D3231692A5ECF904837DA6D4B54A7103E ();
// 0x00000692 System.Void easyar.VideoPlayer::.ctor()
extern void VideoPlayer__ctor_m57B73DC8880F6092B296E53A717B8C8FF4E86328 ();
// 0x00000693 System.Boolean easyar.VideoPlayer::isAvailable()
extern void VideoPlayer_isAvailable_mB909B07FCE8FB30E650EC544E387F263878620CA ();
// 0x00000694 System.Void easyar.VideoPlayer::setVideoType(easyar.VideoType)
extern void VideoPlayer_setVideoType_mAE963160EF021CC62AC151F941C1CAE50E78F32D ();
// 0x00000695 System.Void easyar.VideoPlayer::setRenderTexture(easyar.TextureId)
extern void VideoPlayer_setRenderTexture_mA237E3BBC2743F73DDC3A5424DF4CC239BFB53E1 ();
// 0x00000696 System.Void easyar.VideoPlayer::open(System.String,easyar.StorageType,easyar.CallbackScheduler,easyar.Optional`1<System.Action`1<easyar.VideoStatus>>)
extern void VideoPlayer_open_m0CF5AD174CA080BC29C191D0207DA51EA328B687 ();
// 0x00000697 System.Void easyar.VideoPlayer::close()
extern void VideoPlayer_close_m5B35B8B990E75FADAAD3CFC6527CAAF3245E2CCB ();
// 0x00000698 System.Boolean easyar.VideoPlayer::play()
extern void VideoPlayer_play_m0A00C116D0D9C93BC7481B06442B338C0F5B20AE ();
// 0x00000699 System.Void easyar.VideoPlayer::stop()
extern void VideoPlayer_stop_m611995F8AD20D0352A6124B311109E8145D1F647 ();
// 0x0000069A System.Void easyar.VideoPlayer::pause()
extern void VideoPlayer_pause_mB3A6E9F7FADF6F9DF57F3A0CD07148909505F9BA ();
// 0x0000069B System.Boolean easyar.VideoPlayer::isRenderTextureAvailable()
extern void VideoPlayer_isRenderTextureAvailable_mC05D088961D5DAB705561A4E87C4A2DE19938D21 ();
// 0x0000069C System.Void easyar.VideoPlayer::updateFrame()
extern void VideoPlayer_updateFrame_m0482317223BD922A6241C7B647723642DBABDCD7 ();
// 0x0000069D System.Int32 easyar.VideoPlayer::duration()
extern void VideoPlayer_duration_mBD6AD6373070D675B751D479EF5FCCC1A2FD5961 ();
// 0x0000069E System.Int32 easyar.VideoPlayer::currentPosition()
extern void VideoPlayer_currentPosition_m694972312AE61914F927DD1AC201DAC0B891796C ();
// 0x0000069F System.Boolean easyar.VideoPlayer::seek(System.Int32)
extern void VideoPlayer_seek_m026D4018AD54F8EECA2D76560EED8EEF47CB9C36 ();
// 0x000006A0 easyar.Vec2I easyar.VideoPlayer::size()
extern void VideoPlayer_size_m1D239D3B4223DA17E5B3F329E9DB068B6CFC10BF ();
// 0x000006A1 System.Single easyar.VideoPlayer::volume()
extern void VideoPlayer_volume_m2F0A182CA56F87A7C0145A703882D76E124C8B5F ();
// 0x000006A2 System.Boolean easyar.VideoPlayer::setVolume(System.Single)
extern void VideoPlayer_setVolume_m5FA786ADC0BBBEBAECFA6BD427C37FAE91005B3F ();
// 0x000006A3 easyar.Optional`1<easyar.Image> easyar.ImageHelper::decode(easyar.Buffer)
extern void ImageHelper_decode_m457183EBF0EED91873C4AC25F65976B7652FD6FA ();
// 0x000006A4 System.Void easyar.ImageHelper::.ctor()
extern void ImageHelper__ctor_mF2DC62187A600AA031FF40043006CBCCA8C75DF7 ();
// 0x000006A5 System.Void easyar.SignalSink::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SignalSink__ctor_m4ACEA8E70A1700AC6BA14C892E56545753098C63 ();
// 0x000006A6 System.Object easyar.SignalSink::CloneObject()
extern void SignalSink_CloneObject_m40086FAD4AF9CAF5ACCEE8C18666E356FB54C559 ();
// 0x000006A7 easyar.SignalSink easyar.SignalSink::Clone()
extern void SignalSink_Clone_m247D93B98543F5DCD107C257BD27853BBFF98FA9 ();
// 0x000006A8 System.Void easyar.SignalSink::handle()
extern void SignalSink_handle_m8C188E1F92BE6FE787AE22FFE08AE1FBE6E70066 ();
// 0x000006A9 System.Void easyar.SignalSource::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void SignalSource__ctor_mC07524B197A5868FBDEFA5C5894412355BEAE91A ();
// 0x000006AA System.Object easyar.SignalSource::CloneObject()
extern void SignalSource_CloneObject_mA99E154F82FD574106F33FE624FFE80F7F0210CD ();
// 0x000006AB easyar.SignalSource easyar.SignalSource::Clone()
extern void SignalSource_Clone_mF06117BAA9D740221A79597208D9A569C1EFD1E9 ();
// 0x000006AC System.Void easyar.SignalSource::setHandler(easyar.Optional`1<System.Action>)
extern void SignalSource_setHandler_m67B2FBA103E1EBD03AB957CAEFEA87BEB95F72BC ();
// 0x000006AD System.Void easyar.SignalSource::connect(easyar.SignalSink)
extern void SignalSource_connect_m4EADC28E5EC6A1C5B2F6DB66E57C537E30AE4814 ();
// 0x000006AE System.Void easyar.SignalSource::disconnect()
extern void SignalSource_disconnect_mB3ADF29CF000BBBFC11923B23DD789498383EC33 ();
// 0x000006AF System.Void easyar.InputFrameSink::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFrameSink__ctor_m88632D7ED47C64CCBD6AF4617E002C1B44AE02D4 ();
// 0x000006B0 System.Object easyar.InputFrameSink::CloneObject()
extern void InputFrameSink_CloneObject_mDEA9635D423B3E7E29FFF7658B880CD5342C91C5 ();
// 0x000006B1 easyar.InputFrameSink easyar.InputFrameSink::Clone()
extern void InputFrameSink_Clone_m18993A3807463BB745E1B50176505B682E0EBA86 ();
// 0x000006B2 System.Void easyar.InputFrameSink::handle(easyar.InputFrame)
extern void InputFrameSink_handle_m71E7D7E35DE21A715D98359D4B9F433EB95A667A ();
// 0x000006B3 System.Void easyar.InputFrameSource::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFrameSource__ctor_m9F68CCAB39A195817FC38430C1FBA923AE155D93 ();
// 0x000006B4 System.Object easyar.InputFrameSource::CloneObject()
extern void InputFrameSource_CloneObject_m7F873DAEB20F2EDFBFDB43B97DC25F599F4466F0 ();
// 0x000006B5 easyar.InputFrameSource easyar.InputFrameSource::Clone()
extern void InputFrameSource_Clone_m5F747EE58A8327B3B609087419D4275DFEF289A8 ();
// 0x000006B6 System.Void easyar.InputFrameSource::setHandler(easyar.Optional`1<System.Action`1<easyar.InputFrame>>)
extern void InputFrameSource_setHandler_m6B7EF3939CEB30CD95A18CE665DA02BA6A5BED16 ();
// 0x000006B7 System.Void easyar.InputFrameSource::connect(easyar.InputFrameSink)
extern void InputFrameSource_connect_mCFBD80617E316C9B893A9C12B61DD499277727AD ();
// 0x000006B8 System.Void easyar.InputFrameSource::disconnect()
extern void InputFrameSource_disconnect_mAFD6888A5AD6CAE3297D92196F63B367AD2F8D6D ();
// 0x000006B9 System.Void easyar.OutputFrameSink::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void OutputFrameSink__ctor_m1D615F4665F246F41254AE70204870423C625F0B ();
// 0x000006BA System.Object easyar.OutputFrameSink::CloneObject()
extern void OutputFrameSink_CloneObject_mCC9626A433AAF6193163B2C455934A9DDD040268 ();
// 0x000006BB easyar.OutputFrameSink easyar.OutputFrameSink::Clone()
extern void OutputFrameSink_Clone_m7D796DE18A7EB1948B63DF5859CBD6DE6FF29170 ();
// 0x000006BC System.Void easyar.OutputFrameSink::handle(easyar.OutputFrame)
extern void OutputFrameSink_handle_mD94577A8D567E70EB1B2D8AC0DB018F3C82828D4 ();
// 0x000006BD System.Void easyar.OutputFrameSource::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void OutputFrameSource__ctor_m858251CDCCC4BAFBD7126CCF42046BAA8E1EA72E ();
// 0x000006BE System.Object easyar.OutputFrameSource::CloneObject()
extern void OutputFrameSource_CloneObject_m176D7DC58BCDEC00DAEA1B74FCD5D88C0A50F95A ();
// 0x000006BF easyar.OutputFrameSource easyar.OutputFrameSource::Clone()
extern void OutputFrameSource_Clone_m552D85C438648F2220D46904671246F1A971432D ();
// 0x000006C0 System.Void easyar.OutputFrameSource::setHandler(easyar.Optional`1<System.Action`1<easyar.OutputFrame>>)
extern void OutputFrameSource_setHandler_m21A053591077E76CF71429D1B7828BEC712083C7 ();
// 0x000006C1 System.Void easyar.OutputFrameSource::connect(easyar.OutputFrameSink)
extern void OutputFrameSource_connect_m833158CD37FAEAB7A5F82FB14DA132B7B15624C8 ();
// 0x000006C2 System.Void easyar.OutputFrameSource::disconnect()
extern void OutputFrameSource_disconnect_mD5AE439979D835AC5D7E0704FB408BDF61075592 ();
// 0x000006C3 System.Void easyar.FeedbackFrameSink::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void FeedbackFrameSink__ctor_m509E6A9AF4EBEC6D9659B3F8667E471D89A2787D ();
// 0x000006C4 System.Object easyar.FeedbackFrameSink::CloneObject()
extern void FeedbackFrameSink_CloneObject_mF013BAD27AACBD3B6AD6F4BBC1A6FF727E28F2CA ();
// 0x000006C5 easyar.FeedbackFrameSink easyar.FeedbackFrameSink::Clone()
extern void FeedbackFrameSink_Clone_mEEBA4919A97D24758D6AD7BFA90876E059E744DF ();
// 0x000006C6 System.Void easyar.FeedbackFrameSink::handle(easyar.FeedbackFrame)
extern void FeedbackFrameSink_handle_mD8C9FEC45A81D6698D777B4EF999E50BBE31850A ();
// 0x000006C7 System.Void easyar.FeedbackFrameSource::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void FeedbackFrameSource__ctor_m7D0CBE5E01694294D549099D527A634B8551D42A ();
// 0x000006C8 System.Object easyar.FeedbackFrameSource::CloneObject()
extern void FeedbackFrameSource_CloneObject_m3D82575EA7A7FD691B850567523E59B864CB9936 ();
// 0x000006C9 easyar.FeedbackFrameSource easyar.FeedbackFrameSource::Clone()
extern void FeedbackFrameSource_Clone_m92ADF610D913A4D15AD68AD5F7E62EA7E7E2E716 ();
// 0x000006CA System.Void easyar.FeedbackFrameSource::setHandler(easyar.Optional`1<System.Action`1<easyar.FeedbackFrame>>)
extern void FeedbackFrameSource_setHandler_mC7A8FD386CEEC7D4EED6D8B26C9F17AB5A7D3DD8 ();
// 0x000006CB System.Void easyar.FeedbackFrameSource::connect(easyar.FeedbackFrameSink)
extern void FeedbackFrameSource_connect_m4CECA52212A7D6958907333C1E7068367ACF0F46 ();
// 0x000006CC System.Void easyar.FeedbackFrameSource::disconnect()
extern void FeedbackFrameSource_disconnect_mD547DAA6E9500C4057971889FCC9B8E77F7F811B ();
// 0x000006CD System.Void easyar.InputFrameFork::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFrameFork__ctor_m31C4AF1D64322CE21B8C39748CE00CCAE20E6701 ();
// 0x000006CE System.Object easyar.InputFrameFork::CloneObject()
extern void InputFrameFork_CloneObject_mD29FA3B2CEC0AFFD579EAA8EF258CE411083C67A ();
// 0x000006CF easyar.InputFrameFork easyar.InputFrameFork::Clone()
extern void InputFrameFork_Clone_m5F4849547B1D06CABD38EAC59A7965599677EF80 ();
// 0x000006D0 easyar.InputFrameSink easyar.InputFrameFork::input()
extern void InputFrameFork_input_m4B4F6923AFA19AEC60689C158A114AE34E3F02FF ();
// 0x000006D1 easyar.InputFrameSource easyar.InputFrameFork::output(System.Int32)
extern void InputFrameFork_output_m6E4394E11B9D781D3616972837D054BADF8362A2 ();
// 0x000006D2 System.Int32 easyar.InputFrameFork::outputCount()
extern void InputFrameFork_outputCount_m1A9E823DC07929A3B626AE937FB924A8AA8ACC04 ();
// 0x000006D3 easyar.InputFrameFork easyar.InputFrameFork::create(System.Int32)
extern void InputFrameFork_create_m01A864F8112E0A72C4C236AB919980FB97D038B3 ();
// 0x000006D4 System.Void easyar.OutputFrameFork::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void OutputFrameFork__ctor_m72705BCF50C1D32F45E3595BD6162BB6CE2EC36C ();
// 0x000006D5 System.Object easyar.OutputFrameFork::CloneObject()
extern void OutputFrameFork_CloneObject_m1CBE7616D46D37422697BC02D7527E8769FB9E35 ();
// 0x000006D6 easyar.OutputFrameFork easyar.OutputFrameFork::Clone()
extern void OutputFrameFork_Clone_m26103C12184F36A33DA56892AFF5A004B45B4899 ();
// 0x000006D7 easyar.OutputFrameSink easyar.OutputFrameFork::input()
extern void OutputFrameFork_input_m0F3568EEF1CCA5E5740A03B7356A7081CE354F73 ();
// 0x000006D8 easyar.OutputFrameSource easyar.OutputFrameFork::output(System.Int32)
extern void OutputFrameFork_output_m19087C634351387E1739201BB329CEBD890D71B2 ();
// 0x000006D9 System.Int32 easyar.OutputFrameFork::outputCount()
extern void OutputFrameFork_outputCount_mD5ACC74CA0CF75FB9080CDE757DF4B30BD49823C ();
// 0x000006DA easyar.OutputFrameFork easyar.OutputFrameFork::create(System.Int32)
extern void OutputFrameFork_create_m0075A1F8B9084BBFDBA790397746CF8F306CC6BC ();
// 0x000006DB System.Void easyar.OutputFrameJoin::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void OutputFrameJoin__ctor_m3705478FB9C6BC43EDD59EF89ACEBFF88A111B41 ();
// 0x000006DC System.Object easyar.OutputFrameJoin::CloneObject()
extern void OutputFrameJoin_CloneObject_mE3E34F57FA70822B54C8555E6BDB83AE20E0B972 ();
// 0x000006DD easyar.OutputFrameJoin easyar.OutputFrameJoin::Clone()
extern void OutputFrameJoin_Clone_m79DB8AE555E52ECB572530FF3BEE63194BAD453C ();
// 0x000006DE easyar.OutputFrameSink easyar.OutputFrameJoin::input(System.Int32)
extern void OutputFrameJoin_input_mFB4B939A3F4DBE0C01AD2A6DC483135D97719A3E ();
// 0x000006DF easyar.OutputFrameSource easyar.OutputFrameJoin::output()
extern void OutputFrameJoin_output_mB926DDFEE587647958E6B05CC6EBF7AD320FD70A ();
// 0x000006E0 System.Int32 easyar.OutputFrameJoin::inputCount()
extern void OutputFrameJoin_inputCount_mE6E75F1E10D804FC0E8CF0F58B77274DA70C57D1 ();
// 0x000006E1 easyar.OutputFrameJoin easyar.OutputFrameJoin::create(System.Int32)
extern void OutputFrameJoin_create_m1C16191D0D9720652C32AB917DC5073C608CE5CD ();
// 0x000006E2 easyar.OutputFrameJoin easyar.OutputFrameJoin::createWithJoiner(System.Int32,System.Func`2<System.Collections.Generic.List`1<easyar.OutputFrame>,easyar.OutputFrame>)
extern void OutputFrameJoin_createWithJoiner_mC41C1AFF8490FCBE9195F742BEE4BBB026EE76B7 ();
// 0x000006E3 System.Void easyar.FeedbackFrameFork::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void FeedbackFrameFork__ctor_mD920CC84253F98A412D3355E9DFE23A4011D0E97 ();
// 0x000006E4 System.Object easyar.FeedbackFrameFork::CloneObject()
extern void FeedbackFrameFork_CloneObject_m682E5C21FCF4A5B5708DB2C0B9ABD9FCC87297F7 ();
// 0x000006E5 easyar.FeedbackFrameFork easyar.FeedbackFrameFork::Clone()
extern void FeedbackFrameFork_Clone_m3BD0FDFB83164080A0FDF1E5AE75BD4A1B0C2D3A ();
// 0x000006E6 easyar.FeedbackFrameSink easyar.FeedbackFrameFork::input()
extern void FeedbackFrameFork_input_m49C44522E438A988853410E5D6AAFD42F4B1565D ();
// 0x000006E7 easyar.FeedbackFrameSource easyar.FeedbackFrameFork::output(System.Int32)
extern void FeedbackFrameFork_output_m197271C69F86DB2114D1CAA9F33B00E64742FE85 ();
// 0x000006E8 System.Int32 easyar.FeedbackFrameFork::outputCount()
extern void FeedbackFrameFork_outputCount_m50AA981198269F7B68C40897D11206510A44B058 ();
// 0x000006E9 easyar.FeedbackFrameFork easyar.FeedbackFrameFork::create(System.Int32)
extern void FeedbackFrameFork_create_mB5025ED3F212CD86687201800697ADA34FE26E79 ();
// 0x000006EA System.Void easyar.InputFrameThrottler::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFrameThrottler__ctor_m866265EA8FC6A0060347850E9C13B4D4C3F02BF4 ();
// 0x000006EB System.Object easyar.InputFrameThrottler::CloneObject()
extern void InputFrameThrottler_CloneObject_mA9BF7B67F29BC868E829A1FF9DAE458AB0E1C4C1 ();
// 0x000006EC easyar.InputFrameThrottler easyar.InputFrameThrottler::Clone()
extern void InputFrameThrottler_Clone_m57706C4B958CE9EE89A0C9AA65E25497E9DF0C1B ();
// 0x000006ED easyar.InputFrameSink easyar.InputFrameThrottler::input()
extern void InputFrameThrottler_input_m94D75AF9838C223209885402CCFBD54BC126B920 ();
// 0x000006EE System.Int32 easyar.InputFrameThrottler::bufferRequirement()
extern void InputFrameThrottler_bufferRequirement_m18C83D602C5A0F221C6B1365C3A0F1F72BBA0586 ();
// 0x000006EF easyar.InputFrameSource easyar.InputFrameThrottler::output()
extern void InputFrameThrottler_output_m46A672E64DADE8EB298D47137515941B3B1F7D1C ();
// 0x000006F0 easyar.SignalSink easyar.InputFrameThrottler::signalInput()
extern void InputFrameThrottler_signalInput_mF47FD603C6B5D90748976C87742BCDCC95C34594 ();
// 0x000006F1 easyar.InputFrameThrottler easyar.InputFrameThrottler::create()
extern void InputFrameThrottler_create_m7AD4172BD160BF5CD691A69EE0F51B7EA321CB2E ();
// 0x000006F2 System.Void easyar.OutputFrameBuffer::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void OutputFrameBuffer__ctor_m4803D18081D5D7C715EC38CB25D6471AB1FFED5F ();
// 0x000006F3 System.Object easyar.OutputFrameBuffer::CloneObject()
extern void OutputFrameBuffer_CloneObject_m24EDFB5F8B96A8F2136ADF190E659FB87D0C71E2 ();
// 0x000006F4 easyar.OutputFrameBuffer easyar.OutputFrameBuffer::Clone()
extern void OutputFrameBuffer_Clone_m9C86B35FA309F89FC9898A8566C7F26EF7E3755A ();
// 0x000006F5 easyar.OutputFrameSink easyar.OutputFrameBuffer::input()
extern void OutputFrameBuffer_input_m57EB9D84C15347550C00DE4E93D2DF2D4689B124 ();
// 0x000006F6 System.Int32 easyar.OutputFrameBuffer::bufferRequirement()
extern void OutputFrameBuffer_bufferRequirement_mDA770AFD3C258324C06C2F2CE095AC5798424607 ();
// 0x000006F7 easyar.SignalSource easyar.OutputFrameBuffer::signalOutput()
extern void OutputFrameBuffer_signalOutput_m8F187474874C0E8EF4BA0DC0FEC85368A25DBCBC ();
// 0x000006F8 easyar.Optional`1<easyar.OutputFrame> easyar.OutputFrameBuffer::peek()
extern void OutputFrameBuffer_peek_mB98BBB80C28961E396EA29F631155F6058C0489E ();
// 0x000006F9 easyar.OutputFrameBuffer easyar.OutputFrameBuffer::create()
extern void OutputFrameBuffer_create_m1BBC60443822E75241CE2E5C738A0777F5CF50FF ();
// 0x000006FA System.Void easyar.OutputFrameBuffer::pause()
extern void OutputFrameBuffer_pause_mFDA799F4CB8D2080BA078BFEFB01FA8B00028DD4 ();
// 0x000006FB System.Void easyar.OutputFrameBuffer::resume()
extern void OutputFrameBuffer_resume_mE290E6CCBF4D4B04005EFBDDC0867B980E71241B ();
// 0x000006FC System.Void easyar.InputFrameToOutputFrameAdapter::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFrameToOutputFrameAdapter__ctor_m649E7800AD1615C62B84C840808156EB29A7E8E9 ();
// 0x000006FD System.Object easyar.InputFrameToOutputFrameAdapter::CloneObject()
extern void InputFrameToOutputFrameAdapter_CloneObject_m0AAC88E18811721D296F452DDD2CCE7FD1E26E39 ();
// 0x000006FE easyar.InputFrameToOutputFrameAdapter easyar.InputFrameToOutputFrameAdapter::Clone()
extern void InputFrameToOutputFrameAdapter_Clone_m572753DED81F18A7887F81667A599D19270450B6 ();
// 0x000006FF easyar.InputFrameSink easyar.InputFrameToOutputFrameAdapter::input()
extern void InputFrameToOutputFrameAdapter_input_m90D521E6A36ABC9C1CD1F530C810308EAF1F5941 ();
// 0x00000700 easyar.OutputFrameSource easyar.InputFrameToOutputFrameAdapter::output()
extern void InputFrameToOutputFrameAdapter_output_m32BA43DE8261F264CCC89CB4480F11E00D8A56EF ();
// 0x00000701 easyar.InputFrameToOutputFrameAdapter easyar.InputFrameToOutputFrameAdapter::create()
extern void InputFrameToOutputFrameAdapter_create_mA8E7B372C26C1DEFADD989BAA5754FCA475E03ED ();
// 0x00000702 System.Void easyar.InputFrameToFeedbackFrameAdapter::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFrameToFeedbackFrameAdapter__ctor_mC5D82BC685A97414734B18BDB6B731439DFA95A9 ();
// 0x00000703 System.Object easyar.InputFrameToFeedbackFrameAdapter::CloneObject()
extern void InputFrameToFeedbackFrameAdapter_CloneObject_mC456B6DBCB0DD9BCB0DD0552B1993B62B6EC01B4 ();
// 0x00000704 easyar.InputFrameToFeedbackFrameAdapter easyar.InputFrameToFeedbackFrameAdapter::Clone()
extern void InputFrameToFeedbackFrameAdapter_Clone_m3411C1C9CC9A13F235EF70AE90B90A2339D10EF2 ();
// 0x00000705 easyar.InputFrameSink easyar.InputFrameToFeedbackFrameAdapter::input()
extern void InputFrameToFeedbackFrameAdapter_input_mE4EE78A62225F30255AA91EFCF19EF8F87BF7E6B ();
// 0x00000706 System.Int32 easyar.InputFrameToFeedbackFrameAdapter::bufferRequirement()
extern void InputFrameToFeedbackFrameAdapter_bufferRequirement_m991C4E500C818CC0654FE704D2AC6ACEB7E46FB2 ();
// 0x00000707 easyar.OutputFrameSink easyar.InputFrameToFeedbackFrameAdapter::sideInput()
extern void InputFrameToFeedbackFrameAdapter_sideInput_mF601AA3936E50DB9A2BE90746E50019FA80503DB ();
// 0x00000708 easyar.FeedbackFrameSource easyar.InputFrameToFeedbackFrameAdapter::output()
extern void InputFrameToFeedbackFrameAdapter_output_m819EAA6C737D6077D79CD9920BB4A29BBC0E7EC1 ();
// 0x00000709 easyar.InputFrameToFeedbackFrameAdapter easyar.InputFrameToFeedbackFrameAdapter::create()
extern void InputFrameToFeedbackFrameAdapter_create_m55611E90564970D0D21963BF7EF257C70EFC9968 ();
// 0x0000070A System.Void easyar.InputFrame::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void InputFrame__ctor_m24215B39EF551E7D5775A8FC47A2F79F8CBF5F79 ();
// 0x0000070B System.Object easyar.InputFrame::CloneObject()
extern void InputFrame_CloneObject_mD574189467BA37624D677F66495A7E552E87D19D ();
// 0x0000070C easyar.InputFrame easyar.InputFrame::Clone()
extern void InputFrame_Clone_mECDFF235A5403E4B9B23B66CCC8285EE8944DBE8 ();
// 0x0000070D System.Int32 easyar.InputFrame::index()
extern void InputFrame_index_m31CB99958066C9F0A051CB07F89BB204A97520EC ();
// 0x0000070E easyar.Image easyar.InputFrame::image()
extern void InputFrame_image_m094FB87664E53E65E73A46C11397DE0A8C18AA00 ();
// 0x0000070F System.Boolean easyar.InputFrame::hasCameraParameters()
extern void InputFrame_hasCameraParameters_mBF9E12ED650857216A625C166F0A28D1F839EAB6 ();
// 0x00000710 easyar.CameraParameters easyar.InputFrame::cameraParameters()
extern void InputFrame_cameraParameters_m118EDCBEC3037E10DC6D509F5E64EF18AB83143C ();
// 0x00000711 System.Boolean easyar.InputFrame::hasTemporalInformation()
extern void InputFrame_hasTemporalInformation_m0E8DC0771FE693F4C93E9C8198BCF629F48CCC3A ();
// 0x00000712 System.Double easyar.InputFrame::timestamp()
extern void InputFrame_timestamp_m3AF20E9B958D21087F55219CB175D9AD7E421739 ();
// 0x00000713 System.Boolean easyar.InputFrame::hasSpatialInformation()
extern void InputFrame_hasSpatialInformation_mC6E4D5B56C3D80883824F92C3DCE98A86915EA67 ();
// 0x00000714 easyar.Matrix44F easyar.InputFrame::cameraTransform()
extern void InputFrame_cameraTransform_mDE134D2196AB2530E1FC56B2AED3CEFB40C9FF40 ();
// 0x00000715 easyar.MotionTrackingStatus easyar.InputFrame::trackingStatus()
extern void InputFrame_trackingStatus_m08C798033677F59B5F333376DC587D46C520C9A7 ();
// 0x00000716 easyar.InputFrame easyar.InputFrame::create(easyar.Image,easyar.CameraParameters,System.Double,easyar.Matrix44F,easyar.MotionTrackingStatus)
extern void InputFrame_create_m6462C4D303191D96EA91192DE9170B3939D2AAD4 ();
// 0x00000717 easyar.InputFrame easyar.InputFrame::createWithImageAndCameraParametersAndTemporal(easyar.Image,easyar.CameraParameters,System.Double)
extern void InputFrame_createWithImageAndCameraParametersAndTemporal_m85E729FFB997ED40B6239FD6D3AEF8085B892888 ();
// 0x00000718 easyar.InputFrame easyar.InputFrame::createWithImageAndCameraParameters(easyar.Image,easyar.CameraParameters)
extern void InputFrame_createWithImageAndCameraParameters_m11EF707232413645EED98B531BC1533424CE738C ();
// 0x00000719 easyar.InputFrame easyar.InputFrame::createWithImage(easyar.Image)
extern void InputFrame_createWithImage_mF75B127482902518BBB105202F9FD4D1B8BCA8EA ();
// 0x0000071A System.Void easyar.FrameFilterResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void FrameFilterResult__ctor_m67BAED829C5D417AC9206BE3430BF96A383F6365 ();
// 0x0000071B System.Object easyar.FrameFilterResult::CloneObject()
extern void FrameFilterResult_CloneObject_m4B45B2452E913F1CCD22296DF5E633D39FDFC180 ();
// 0x0000071C easyar.FrameFilterResult easyar.FrameFilterResult::Clone()
extern void FrameFilterResult_Clone_mE77802987BB336E83183D22F5F97E55EB45340C1 ();
// 0x0000071D System.Void easyar.OutputFrame::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void OutputFrame__ctor_m077F6EE0C8A3D6F8288103840C2893106D419C7C ();
// 0x0000071E System.Object easyar.OutputFrame::CloneObject()
extern void OutputFrame_CloneObject_mC09A8BF5F738BA3EB958FC69CA73FB2D77E83D02 ();
// 0x0000071F easyar.OutputFrame easyar.OutputFrame::Clone()
extern void OutputFrame_Clone_mC55E9346749EBFC46A02E555BF90219D24FCC9BF ();
// 0x00000720 System.Void easyar.OutputFrame::.ctor(easyar.InputFrame,System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>>)
extern void OutputFrame__ctor_mCD477CE7E9611D2F670E9CB704B4320482C89CB9 ();
// 0x00000721 System.Int32 easyar.OutputFrame::index()
extern void OutputFrame_index_m3FDA5247D5D319A10C4B407C685730418BFACE64 ();
// 0x00000722 easyar.InputFrame easyar.OutputFrame::inputFrame()
extern void OutputFrame_inputFrame_mD4CB997C9E114F8CD7E0205D9E0CA6070A702B02 ();
// 0x00000723 System.Collections.Generic.List`1<easyar.Optional`1<easyar.FrameFilterResult>> easyar.OutputFrame::results()
extern void OutputFrame_results_m94A9C8E45A36AAC3B27A69EEB19F0B2CA9F70DAA ();
// 0x00000724 System.Void easyar.FeedbackFrame::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void FeedbackFrame__ctor_mB25BFCDD9D2AE705F24F128245ED377741FB5E72 ();
// 0x00000725 System.Object easyar.FeedbackFrame::CloneObject()
extern void FeedbackFrame_CloneObject_m2B159AB5B0D86BBE3D3AF01A1B83D129D4B76A1A ();
// 0x00000726 easyar.FeedbackFrame easyar.FeedbackFrame::Clone()
extern void FeedbackFrame_Clone_m10EA92B244F366F22FB62AA6BDEC97DE34EC24E8 ();
// 0x00000727 System.Void easyar.FeedbackFrame::.ctor(easyar.InputFrame,easyar.Optional`1<easyar.OutputFrame>)
extern void FeedbackFrame__ctor_m3ABB1AF9ED33F3CDC011FABE79C068B003CEC5CE ();
// 0x00000728 easyar.InputFrame easyar.FeedbackFrame::inputFrame()
extern void FeedbackFrame_inputFrame_mF1B52F49D5E9328367D1D5603AE0825546C4EB9C ();
// 0x00000729 easyar.Optional`1<easyar.OutputFrame> easyar.FeedbackFrame::previousOutputFrame()
extern void FeedbackFrame_previousOutputFrame_m60C65489C3837167A187F76612BBA6429B000266 ();
// 0x0000072A System.Void easyar.Target::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void Target__ctor_m6909347F5612EB19FBF339197C6261878A4E143D ();
// 0x0000072B System.Object easyar.Target::CloneObject()
extern void Target_CloneObject_mE204009FF6B55E7D3660D153DCB1977AB105637A ();
// 0x0000072C easyar.Target easyar.Target::Clone()
extern void Target_Clone_m911B0B393CB2D23ED1BBAA9D4E6B5F58422E5CF0 ();
// 0x0000072D System.Int32 easyar.Target::runtimeID()
extern void Target_runtimeID_m1062B1EC916E46C768FFC541F7FD5CB8A4DDC47E ();
// 0x0000072E System.String easyar.Target::uid()
extern void Target_uid_m3DF3EFAE85F84423319F33B41B679E12ECAF78F5 ();
// 0x0000072F System.String easyar.Target::name()
extern void Target_name_m806CF4135F3E2301B5F6878D11182DEAA9DE6936 ();
// 0x00000730 System.Void easyar.Target::setName(System.String)
extern void Target_setName_m38C93F5DA33CF523355FD86C64F35305F392DBED ();
// 0x00000731 System.String easyar.Target::meta()
extern void Target_meta_m6CBED0A45FE25B9CB31B821B031F406DD8794F47 ();
// 0x00000732 System.Void easyar.Target::setMeta(System.String)
extern void Target_setMeta_m0EA586F52FB4C76E58AFB3DF44CE46EF6AECC5E7 ();
// 0x00000733 System.Void easyar.TargetInstance::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void TargetInstance__ctor_mA68F872673E14D569B3B7E9FCE871589C974BEAC ();
// 0x00000734 System.Object easyar.TargetInstance::CloneObject()
extern void TargetInstance_CloneObject_m11086BF4997410BEF1A8EFD6BFACF5671B45DE9D ();
// 0x00000735 easyar.TargetInstance easyar.TargetInstance::Clone()
extern void TargetInstance_Clone_m8D9380A7F505B4A9F9416FDBCDE8EB775DC104DE ();
// 0x00000736 System.Void easyar.TargetInstance::.ctor()
extern void TargetInstance__ctor_m481A589433788D3003E8C5EB4D0310BE1E773EC4 ();
// 0x00000737 easyar.TargetStatus easyar.TargetInstance::status()
extern void TargetInstance_status_mB1DA0DDC9A359392D54B33D60B281E17F09CD02E ();
// 0x00000738 easyar.Optional`1<easyar.Target> easyar.TargetInstance::target()
extern void TargetInstance_target_mB860CB73940F8797CF0B715880571C4333BC6258 ();
// 0x00000739 easyar.Matrix44F easyar.TargetInstance::pose()
extern void TargetInstance_pose_m385428A425AF9D02B5522E9D964A483C3162CE35 ();
// 0x0000073A System.Void easyar.TargetTrackerResult::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void TargetTrackerResult__ctor_mD9A1ADCCF5551328BB265D50F66C02B6A5F90E22 ();
// 0x0000073B System.Object easyar.TargetTrackerResult::CloneObject()
extern void TargetTrackerResult_CloneObject_m0DD8299678C5EC9CE71675C417CF337E64B83466 ();
// 0x0000073C easyar.TargetTrackerResult easyar.TargetTrackerResult::Clone()
extern void TargetTrackerResult_Clone_m1764339BCCCCD38478E1829786B39342D169BA31 ();
// 0x0000073D System.Collections.Generic.List`1<easyar.TargetInstance> easyar.TargetTrackerResult::targetInstances()
extern void TargetTrackerResult_targetInstances_m8569CCC6EB7B5FECF2F0F5964AE8E33F6F821DBE ();
// 0x0000073E System.Void easyar.TargetTrackerResult::setTargetInstances(System.Collections.Generic.List`1<easyar.TargetInstance>)
extern void TargetTrackerResult_setTargetInstances_m83571CA71EFCE300C69612BA409BC7EC6FFA4281 ();
// 0x0000073F System.Void easyar.TextureId::.ctor(System.IntPtr,System.Action`1<System.IntPtr>,easyar.RefBase_Retainer)
extern void TextureId__ctor_m8427277148C404F2719013AA2E9014FFC2968129 ();
// 0x00000740 System.Object easyar.TextureId::CloneObject()
extern void TextureId_CloneObject_m042531FF0C413B50B9375963F2970132C974C640 ();
// 0x00000741 easyar.TextureId easyar.TextureId::Clone()
extern void TextureId_Clone_m948BA0B664D61A72A5039C4AC0F69E4334E0FB12 ();
// 0x00000742 System.Int32 easyar.TextureId::getInt()
extern void TextureId_getInt_m64FD2586D6B254E13E8F4C9A434402D3B459B279 ();
// 0x00000743 System.IntPtr easyar.TextureId::getPointer()
extern void TextureId_getPointer_m3CF5426EAF08F6EFC9BB78D179F2E5513523D3D4 ();
// 0x00000744 easyar.TextureId easyar.TextureId::fromInt(System.Int32)
extern void TextureId_fromInt_mCADFAA01E1B05D47CD1489F08BE956FC9ADBDCC4 ();
// 0x00000745 easyar.TextureId easyar.TextureId::fromPointer(System.IntPtr)
extern void TextureId_fromPointer_m28D7B2AABC3DB5FA10E6736E7C3ADE07705DE98D ();
// 0x00000746 System.Void easyar.ARSession_FrameChangeAction::.ctor(System.Object,System.IntPtr)
extern void FrameChangeAction__ctor_m5A5D389FA4ECE870CD7506021B60145DC962668C ();
// 0x00000747 System.Void easyar.ARSession_FrameChangeAction::Invoke(easyar.OutputFrame,UnityEngine.Matrix4x4)
extern void FrameChangeAction_Invoke_m0839F272E0677DCB5D60261937AA6275EB9CA204 ();
// 0x00000748 System.IAsyncResult easyar.ARSession_FrameChangeAction::BeginInvoke(easyar.OutputFrame,UnityEngine.Matrix4x4,System.AsyncCallback,System.Object)
extern void FrameChangeAction_BeginInvoke_m0E8D139ED5D0695BBBC624A74F8460E14A3D0927 ();
// 0x00000749 System.Void easyar.ARSession_FrameChangeAction::EndInvoke(System.IAsyncResult)
extern void FrameChangeAction_EndInvoke_m8E7F142B40F984159A2ABD2D42B3613FFFE72801 ();
// 0x0000074A System.Void easyar.CameraImageRenderer_UserRequest::Finalize()
extern void UserRequest_Finalize_mCFE8CE1F04790A2FD9AC7DDA5F614224D1B698CD ();
// 0x0000074B System.Void easyar.CameraImageRenderer_UserRequest::Dispose()
extern void UserRequest_Dispose_m54025DE211EA1D9A1A6269EB93632464A5C3818B ();
// 0x0000074C System.Boolean easyar.CameraImageRenderer_UserRequest::UpdateTexture(UnityEngine.Camera,UnityEngine.Material,UnityEngine.RenderTexture&)
extern void UserRequest_UpdateTexture_m23FDF524660408E2E40478795D5E99564DE2F8AC ();
// 0x0000074D System.Void easyar.CameraImageRenderer_UserRequest::UpdateCommandBuffer(UnityEngine.Camera,UnityEngine.Material)
extern void UserRequest_UpdateCommandBuffer_m052857A82C0E2CCE7294BE8676491C9CEE71391F ();
// 0x0000074E System.Void easyar.CameraImageRenderer_UserRequest::RemoveCommandBuffer(UnityEngine.Camera)
extern void UserRequest_RemoveCommandBuffer_m8ADB1878F4C4A040714B7A3A2F6619EC82F95153 ();
// 0x0000074F System.Void easyar.CameraImageRenderer_UserRequest::.ctor()
extern void UserRequest__ctor_mF2CB656506725221414D99675A3542984D199BC7 ();
// 0x00000750 System.Void easyar.CloudRecognizerFrameFilter_CloudRecognizerServiceConfig::.ctor()
extern void CloudRecognizerServiceConfig__ctor_m46BE3366B21BC9246932700C8F61859DD248AC14 ();
// 0x00000751 System.Void easyar.CloudRecognizerFrameFilter_PrivateCloudRecognizerServiceConfig::.ctor()
extern void PrivateCloudRecognizerServiceConfig__ctor_mE345236DDC5FB8A7BFD25759C909FA40D512C7BB ();
// 0x00000752 System.Void easyar.EasyARController_<>c::.cctor()
extern void U3CU3Ec__cctor_m87058F2537A82E466B0B046532D010A061106DA7 ();
// 0x00000753 System.Void easyar.EasyARController_<>c::.ctor()
extern void U3CU3Ec__ctor_mC2D138B103980054FC8FC5E932E1A786D2FEF10A ();
// 0x00000754 System.Void easyar.EasyARController_<>c::<GlobalInitialization>b__30_0(System.Object,System.EventArgs)
extern void U3CU3Ec_U3CGlobalInitializationU3Eb__30_0_mF39DEE2C765DE2F6324F4F49F5CCCA9D070D2DC6 ();
// 0x00000755 System.Void easyar.EasyARController_<>c::<GlobalInitialization>b__30_1(System.Object,System.UnhandledExceptionEventArgs)
extern void U3CU3Ec_U3CGlobalInitializationU3Eb__30_1_m98CDEF5FA28FA903916B3D3C365F3C00A4F475F3 ();
// 0x00000756 easyar.FeedbackFrameSink easyar.FrameFilter_IFeedbackFrameSink::FeedbackFrameSink()
// 0x00000757 easyar.InputFrameSink easyar.FrameFilter_IInputFrameSink::InputFrameSink()
// 0x00000758 System.Void easyar.FrameFilter_IInputFrameSinkDelayConnect::ConnectedTo(easyar.InputFrameSource,System.Action)
// 0x00000759 easyar.OutputFrameSource easyar.FrameFilter_IOutputFrameSource::OutputFrameSource()
// 0x0000075A System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<easyar.Optional`1<easyar.TargetController>,easyar.Matrix44F>> easyar.FrameFilter_IOutputFrameSource::OnResult(easyar.Optional`1<easyar.FrameFilterResult>)
// 0x0000075B easyar.MotionTrackingStatus easyar.FrameFilter_ISpatialInformationSink::get_TrackingStatus()
// 0x0000075C System.Void easyar.FrameFilter_ISpatialInformationSink::OnTracking(easyar.MotionTrackingStatus)
// 0x0000075D System.Void easyar.ImageTargetController_ImageFileSourceData::.ctor()
extern void ImageFileSourceData__ctor_mC26D66CCF1E237088FBD007B784FDA0061EC84A9 ();
// 0x0000075E System.Void easyar.ImageTargetController_TargetDataFileSourceData::.ctor()
extern void TargetDataFileSourceData__ctor_m4604139C142B75EBB36AC281A7FA3ED4620914F7 ();
// 0x0000075F System.Void easyar.ImageTargetController_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m2B1C0AA63060E29BC9A17AD6802935829F350E88 ();
// 0x00000760 System.Void easyar.ImageTargetController_<>c__DisplayClass34_0::<LoadImageFile>b__0(easyar.Buffer)
extern void U3CU3Ec__DisplayClass34_0_U3CLoadImageFileU3Eb__0_m11512DC35C849E198E1358FE0DF31AACE8417901 ();
// 0x00000761 System.Void easyar.ImageTargetController_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m97AB424F7C6585CF151125C03087C695DA33D0A0 ();
// 0x00000762 System.Void easyar.ImageTargetController_<>c__DisplayClass37_1::.ctor()
extern void U3CU3Ec__DisplayClass37_1__ctor_m9CC9B220C66455DAFF684BC8319B23D462E64A6E ();
// 0x00000763 System.Void easyar.ImageTargetController_<>c__DisplayClass37_1::<LoadImageBuffer>b__0()
extern void U3CU3Ec__DisplayClass37_1_U3CLoadImageBufferU3Eb__0_mA5740922DE847CC2BCDF06D0FF915E695E7CCB21 ();
// 0x00000764 System.Void easyar.ImageTargetController_<LoadImageBuffer>d__37::.ctor(System.Int32)
extern void U3CLoadImageBufferU3Ed__37__ctor_mFB4941A9BDCC41E4F3FA2FA1568CDB63BF2A07C4 ();
// 0x00000765 System.Void easyar.ImageTargetController_<LoadImageBuffer>d__37::System.IDisposable.Dispose()
extern void U3CLoadImageBufferU3Ed__37_System_IDisposable_Dispose_mD0A44D8ACFDA059A99A9874657A754520A01BB7A ();
// 0x00000766 System.Boolean easyar.ImageTargetController_<LoadImageBuffer>d__37::MoveNext()
extern void U3CLoadImageBufferU3Ed__37_MoveNext_mEF9F02B4748B2BB1ABAB14BCDC85C487911888A3 ();
// 0x00000767 System.Void easyar.ImageTargetController_<LoadImageBuffer>d__37::<>m__Finally1()
extern void U3CLoadImageBufferU3Ed__37_U3CU3Em__Finally1_mADBB8E93A2D85B2C6AE6CA0DAD7C260EABDD434A ();
// 0x00000768 System.Object easyar.ImageTargetController_<LoadImageBuffer>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadImageBufferU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FBCB20845252D8BF0D6F10DB3A6FDC3253A0F65 ();
// 0x00000769 System.Void easyar.ImageTargetController_<LoadImageBuffer>d__37::System.Collections.IEnumerator.Reset()
extern void U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_Reset_m294E5AA6C40894CF9A1FAF4928CB7E1CB7CDB732 ();
// 0x0000076A System.Object easyar.ImageTargetController_<LoadImageBuffer>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_get_Current_mA8278B5B699B03A7C82406C7B78BCBFA58F981DE ();
// 0x0000076B System.Void easyar.ImageTargetController_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m62C5FC910742E952157DA9D7AB0CCA5AA9221FB3 ();
// 0x0000076C System.Void easyar.ImageTargetController_<>c__DisplayClass38_1::.ctor()
extern void U3CU3Ec__DisplayClass38_1__ctor_m2CBD2D1294748D16AFF5A05F019E2572055506B0 ();
// 0x0000076D System.Void easyar.ImageTargetController_<>c__DisplayClass38_1::<LoadTargetDataBuffer>b__0()
extern void U3CU3Ec__DisplayClass38_1_U3CLoadTargetDataBufferU3Eb__0_m1246457C61690E891639019E91FDCB6489B5C6A6 ();
// 0x0000076E System.Void easyar.ImageTargetController_<LoadTargetDataBuffer>d__38::.ctor(System.Int32)
extern void U3CLoadTargetDataBufferU3Ed__38__ctor_m1BC425137726BE6C6660B61AE070CB69887507CE ();
// 0x0000076F System.Void easyar.ImageTargetController_<LoadTargetDataBuffer>d__38::System.IDisposable.Dispose()
extern void U3CLoadTargetDataBufferU3Ed__38_System_IDisposable_Dispose_m68EC4C6A8A58C76B3E39475CE29A808D6EF2870C ();
// 0x00000770 System.Boolean easyar.ImageTargetController_<LoadTargetDataBuffer>d__38::MoveNext()
extern void U3CLoadTargetDataBufferU3Ed__38_MoveNext_m5614ABC82A7DAE999178F822DCB57B5D1EBF7A56 ();
// 0x00000771 System.Void easyar.ImageTargetController_<LoadTargetDataBuffer>d__38::<>m__Finally1()
extern void U3CLoadTargetDataBufferU3Ed__38_U3CU3Em__Finally1_m9D40EB9155B96731C3867498C68EAA0DAF979E85 ();
// 0x00000772 System.Object easyar.ImageTargetController_<LoadTargetDataBuffer>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadTargetDataBufferU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58F16684447CC75951583491B843C3A117E1C1F1 ();
// 0x00000773 System.Void easyar.ImageTargetController_<LoadTargetDataBuffer>d__38::System.Collections.IEnumerator.Reset()
extern void U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_Reset_m4761BD5C2F12C25EB7DFEEDB9B8E29AB5C500A7E ();
// 0x00000774 System.Object easyar.ImageTargetController_<LoadTargetDataBuffer>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_get_Current_mACCEA7A8EC327E5639E373BA69DCEB49E9FF1CA1 ();
// 0x00000775 System.Void easyar.ImageTargetController_<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m287DA5BABCC5696E18DC6A847353ADC5258E8BE4 ();
// 0x00000776 System.Void easyar.ImageTargetController_<>c__DisplayClass39_0::<UpdateTargetInTracker>b__1(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass39_0_U3CUpdateTargetInTrackerU3Eb__1_mCF0BE3DC27D00B25099707D9097C774C65634961 ();
// 0x00000777 System.Void easyar.ImageTrackerFrameFilter_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mB78C7AC6772E554FA3302D46EA453C331A765384 ();
// 0x00000778 System.Void easyar.ImageTrackerFrameFilter_<>c__DisplayClass37_0::<LoadImageTarget>b__0(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass37_0_U3CLoadImageTargetU3Eb__0_mACE0F309654D62AEFE0E80CA437A2FE68F28162A ();
// 0x00000779 System.Void easyar.ImageTrackerFrameFilter_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m59E8ACC99FACFAFCDD3A9BD2479FE53A9773D42B ();
// 0x0000077A System.Void easyar.ImageTrackerFrameFilter_<>c__DisplayClass38_0::<UnloadImageTarget>b__0(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass38_0_U3CUnloadImageTargetU3Eb__0_m7317F0AD5CD15FCA5C93379742EAC0365B8E6262 ();
// 0x0000077B System.Void easyar.ObjectTargetController_ObjFileSourceData::.ctor()
extern void ObjFileSourceData__ctor_m0C444D0207707000755D227015C3D42A7A654EFB ();
// 0x0000077C System.Void easyar.ObjectTargetController_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m72617F2746AA979F60A8CEE6DC30B07A8512E460 ();
// 0x0000077D System.Void easyar.ObjectTargetController_<>c__DisplayClass35_1::.ctor()
extern void U3CU3Ec__DisplayClass35_1__ctor_m702A7BFDD199B2D4EA28196787034736D2BDEFA7 ();
// 0x0000077E System.Void easyar.ObjectTargetController_<>c__DisplayClass35_1::<LoadObjFileFromSource>b__0(easyar.Buffer)
extern void U3CU3Ec__DisplayClass35_1_U3CLoadObjFileFromSourceU3Eb__0_m783BC0E4CC335B3F26A8843518BD993F25A8B30F ();
// 0x0000077F System.Void easyar.ObjectTargetController_<>c__DisplayClass35_2::.ctor()
extern void U3CU3Ec__DisplayClass35_2__ctor_mD8CAC23DA055755169A21964C702F824EB9C4BC0 ();
// 0x00000780 System.Void easyar.ObjectTargetController_<>c__DisplayClass35_2::<LoadObjFileFromSource>b__1(easyar.Buffer)
extern void U3CU3Ec__DisplayClass35_2_U3CLoadObjFileFromSourceU3Eb__1_m980C6497BA62120F72DB202625D5D33C89392769 ();
// 0x00000781 System.Void easyar.ObjectTargetController_<LoadObjFileFromSource>d__35::.ctor(System.Int32)
extern void U3CLoadObjFileFromSourceU3Ed__35__ctor_mF664C20DDA5FC52850C1A05CFD3A286AA306348D ();
// 0x00000782 System.Void easyar.ObjectTargetController_<LoadObjFileFromSource>d__35::System.IDisposable.Dispose()
extern void U3CLoadObjFileFromSourceU3Ed__35_System_IDisposable_Dispose_mDF8431A500419133E6A0514D8CDE78227EAA95D5 ();
// 0x00000783 System.Boolean easyar.ObjectTargetController_<LoadObjFileFromSource>d__35::MoveNext()
extern void U3CLoadObjFileFromSourceU3Ed__35_MoveNext_m0E915CEAB996A5A057F7D632EC803AF2B2268181 ();
// 0x00000784 System.Void easyar.ObjectTargetController_<LoadObjFileFromSource>d__35::<>m__Finally1()
extern void U3CLoadObjFileFromSourceU3Ed__35_U3CU3Em__Finally1_mE2A4BDD61EEEF9CD182BA203A728E2C67043CA45 ();
// 0x00000785 System.Void easyar.ObjectTargetController_<LoadObjFileFromSource>d__35::<>m__Finally2()
extern void U3CLoadObjFileFromSourceU3Ed__35_U3CU3Em__Finally2_m9E36E823A033AE5B9A179286C0325EEC4E8E2D7E ();
// 0x00000786 System.Object easyar.ObjectTargetController_<LoadObjFileFromSource>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadObjFileFromSourceU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3FEAE033BD4DFA0F146A1F05803B15F0F71E976 ();
// 0x00000787 System.Void easyar.ObjectTargetController_<LoadObjFileFromSource>d__35::System.Collections.IEnumerator.Reset()
extern void U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_Reset_m42E38D7D95C453A7EA50AC722E8F3E06A5F482F2 ();
// 0x00000788 System.Object easyar.ObjectTargetController_<LoadObjFileFromSource>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_get_Current_m763DE9308058541B6FB0299201065D7B22377FDC ();
// 0x00000789 System.Void easyar.ObjectTargetController_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_mDC3CC162BC6BC9E5A540B876F4BA6502B3C99F66 ();
// 0x0000078A System.Void easyar.ObjectTargetController_<>c__DisplayClass36_0::<UpdateTargetInTracker>b__1(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass36_0_U3CUpdateTargetInTrackerU3Eb__1_m8B7755806BC4F0ADA1B3AC6DF35E9FB916BDBFD8 ();
// 0x0000078B System.Void easyar.ObjectTrackerFrameFilter_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m24EC14ABD5BADF5FA98C556354BD094AF538B238 ();
// 0x0000078C System.Void easyar.ObjectTrackerFrameFilter_<>c__DisplayClass36_0::<LoadObjectTarget>b__0(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass36_0_U3CLoadObjectTargetU3Eb__0_m01A22D4A1D295BB65C817D92213EA0A7495C5765 ();
// 0x0000078D System.Void easyar.ObjectTrackerFrameFilter_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mDDC294CBFA9E6C65454FB636AB417483644AFBDF ();
// 0x0000078E System.Void easyar.ObjectTrackerFrameFilter_<>c__DisplayClass37_0::<UnloadObjectTarget>b__0(easyar.Target,System.Boolean)
extern void U3CU3Ec__DisplayClass37_0_U3CUnloadObjectTargetU3Eb__0_mA25CF710ADFBD2CDE8ECEF5789E8DA41B003357A ();
// 0x0000078F System.Void easyar.EasyARSettings_TargetGizmoConfig::.ctor()
extern void TargetGizmoConfig__ctor_m6E4644AA820AC7AD00BF625C9DA36674748D0823 ();
// 0x00000790 System.Void easyar.SparseSpatialMapController_SparseSpatialMapInfo::.ctor()
extern void SparseSpatialMapInfo__ctor_mC9E63EBA31F87A30501A92067A52DFE277F2D559 ();
// 0x00000791 System.Void easyar.SparseSpatialMapController_MapManagerSourceData::.ctor()
extern void MapManagerSourceData__ctor_mF3B009692A5D49D6690D038B3C343D2F0C06094C ();
// 0x00000792 System.Void easyar.SparseSpatialMapController_ParticleParameter::.ctor()
extern void ParticleParameter__ctor_mCE0749CF8471491B063B3844D1BAAD9E876FFBAB ();
// 0x00000793 System.Void easyar.SparseSpatialMapController_<>c__DisplayClass57_0::.ctor()
extern void U3CU3Ec__DisplayClass57_0__ctor_mE8B8F7A4FD86DB5D074C409DF5F90FBE2E51FB6A ();
// 0x00000794 UnityEngine.Vector3 easyar.SparseSpatialMapController_<>c__DisplayClass57_0::<UpdatePointCloud>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass57_0_U3CUpdatePointCloudU3Eb__0_m96210735F38572D86EF08BEADD802AF3773E929A ();
// 0x00000795 System.Void easyar.SparseSpatialMapController_<>c__DisplayClass61_0::.ctor()
extern void U3CU3Ec__DisplayClass61_0__ctor_mED3763669488870BD57D03C1943E6FC4F1A02AB6 ();
// 0x00000796 System.Void easyar.SparseSpatialMapController_<>c__DisplayClass61_0::<UpdateMapInLocalizer>b__1(easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass61_0_U3CUpdateMapInLocalizerU3Eb__1_m0F5ABB7E592D934291CF5E984E5DA69A3CF0E70C ();
// 0x00000797 System.Void easyar.SparseSpatialMapWorkerFrameFilter_MapLocalizerConfig::.ctor()
extern void MapLocalizerConfig__ctor_mB2141AE959D97990BF34B2B988EF1BA7BE9276A4 ();
// 0x00000798 System.Void easyar.SparseSpatialMapWorkerFrameFilter_SpatialMapServiceConfig::.ctor()
extern void SpatialMapServiceConfig__ctor_m6C1E2F582C8D964BF62F7CC0674A4FA0B3BC1C0B ();
// 0x00000799 System.Void easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass64_0::.ctor()
extern void U3CU3Ec__DisplayClass64_0__ctor_m9478A19C24B01D3A5FA2BD7696676646D480A089 ();
// 0x0000079A System.Void easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass64_0::<LoadSparseSpatialMap>b__0(System.Boolean,System.String)
extern void U3CU3Ec__DisplayClass64_0_U3CLoadSparseSpatialMapU3Eb__0_m11115D1269D8460926F8CDCA5A203C4A12259508 ();
// 0x0000079B System.Void easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass65_0::.ctor()
extern void U3CU3Ec__DisplayClass65_0__ctor_mE6E5FC194109FA242F101E16406185FFBE4EFBED ();
// 0x0000079C System.Void easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass65_0::<UnloadSparseSpatialMap>b__0(System.Boolean)
extern void U3CU3Ec__DisplayClass65_0_U3CUnloadSparseSpatialMapU3Eb__0_m82BCE66C7A2A0F349DC8B083D9C5AE79308B7529 ();
// 0x0000079D System.Void easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m049DA0B3BBC1AF189648247D10EC3C15706284AC ();
// 0x0000079E System.Void easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass66_0::<HostSparseSpatialMap>b__0(System.Boolean,System.String,System.String)
extern void U3CU3Ec__DisplayClass66_0_U3CHostSparseSpatialMapU3Eb__0_mBAC3C3CE73A7780013CECDE3F6C25AEE63BE179A ();
// 0x0000079F System.Void easyar.FileUtil_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mDA88F9878FE2C11C409055DE704782C101465D3E ();
// 0x000007A0 System.Void easyar.FileUtil_<>c__DisplayClass0_0::<LoadFile>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass0_0_U3CLoadFileU3Eb__0_m89E7B14ED4DAC4411FBB945CDBEBB9C23C59C9A2 ();
// 0x000007A1 System.Void easyar.FileUtil_<LoadFile>d__1::.ctor(System.Int32)
extern void U3CLoadFileU3Ed__1__ctor_mE13418FCCBD4A43BF00FD5DC750FE442C5EC6AED ();
// 0x000007A2 System.Void easyar.FileUtil_<LoadFile>d__1::System.IDisposable.Dispose()
extern void U3CLoadFileU3Ed__1_System_IDisposable_Dispose_mF648985E400E9511EAAE5045586AE3DC39685860 ();
// 0x000007A3 System.Boolean easyar.FileUtil_<LoadFile>d__1::MoveNext()
extern void U3CLoadFileU3Ed__1_MoveNext_mA3E26032FEF1F819AEDE7DDA0E47A26DCEF827A4 ();
// 0x000007A4 System.Void easyar.FileUtil_<LoadFile>d__1::<>m__Finally1()
extern void U3CLoadFileU3Ed__1_U3CU3Em__Finally1_mE98E113260F26E257234B5C00C2605878BD2FC66 ();
// 0x000007A5 System.Object easyar.FileUtil_<LoadFile>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadFileU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9170B939D633F3684A1972B41C7BDD0727422294 ();
// 0x000007A6 System.Void easyar.FileUtil_<LoadFile>d__1::System.Collections.IEnumerator.Reset()
extern void U3CLoadFileU3Ed__1_System_Collections_IEnumerator_Reset_mEA4B19D80B4CD71AF0345BD61517201CD15DD0DF ();
// 0x000007A7 System.Object easyar.FileUtil_<LoadFile>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CLoadFileU3Ed__1_System_Collections_IEnumerator_get_Current_m4CED7D7A2D78D06F8CABBF30B53F10DEE38AC3EA ();
// 0x000007A8 System.Void easyar.GUIPopup_<ShowMessage>d__8::.ctor(System.Int32)
extern void U3CShowMessageU3Ed__8__ctor_m36B143EF8D98BB904B07FED0B24262AA6BDDE7FB ();
// 0x000007A9 System.Void easyar.GUIPopup_<ShowMessage>d__8::System.IDisposable.Dispose()
extern void U3CShowMessageU3Ed__8_System_IDisposable_Dispose_m337BC0CE33848702389A44B026B54D5FBFB5D9ED ();
// 0x000007AA System.Boolean easyar.GUIPopup_<ShowMessage>d__8::MoveNext()
extern void U3CShowMessageU3Ed__8_MoveNext_mB671DC763D019031F9FEE75FEFE80224915D13DD ();
// 0x000007AB System.Object easyar.GUIPopup_<ShowMessage>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowMessageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64CDAB024F280D891B378F97E3521A6760BF9F27 ();
// 0x000007AC System.Void easyar.GUIPopup_<ShowMessage>d__8::System.Collections.IEnumerator.Reset()
extern void U3CShowMessageU3Ed__8_System_Collections_IEnumerator_Reset_m42D83A290ECB922C83EABCC6F303DAEFBB050A77 ();
// 0x000007AD System.Object easyar.GUIPopup_<ShowMessage>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CShowMessageU3Ed__8_System_Collections_IEnumerator_get_Current_m7ECE6C6E2223DB2AB1FD051EE0B01266548652CC ();
// 0x000007AE System.Void easyar.VIOCameraDeviceUnion_DeviceUnion::.ctor(easyar.MotionTrackerCameraDevice)
extern void DeviceUnion__ctor_m6310C0DCAECE5A1778DBAFE93517A338913C628F ();
// 0x000007AF System.Void easyar.VIOCameraDeviceUnion_DeviceUnion::.ctor(easyar.ARKitCameraDevice)
extern void DeviceUnion__ctor_mF7BFD75FC072F924A0E911D1C00D0F192E2DCA19 ();
// 0x000007B0 System.Void easyar.VIOCameraDeviceUnion_DeviceUnion::.ctor(easyar.ARCoreCameraDevice)
extern void DeviceUnion__ctor_m5826EC18294E9D3A50D13D672EFAA2823B70594B ();
// 0x000007B1 easyar.VIOCameraDeviceUnion_DeviceUnion_VIODeviceType easyar.VIOCameraDeviceUnion_DeviceUnion::get_DeviceType()
extern void DeviceUnion_get_DeviceType_m74381A25C2EDF9BF423CA43C63B7CC2DDC7C0D42 ();
// 0x000007B2 System.Void easyar.VIOCameraDeviceUnion_DeviceUnion::set_DeviceType(easyar.VIOCameraDeviceUnion_DeviceUnion_VIODeviceType)
extern void DeviceUnion_set_DeviceType_m32407C8A0B2A051461CBD0DF567FAB69962400ED ();
// 0x000007B3 easyar.MotionTrackerCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::get_MotionTrackerCameraDevice()
extern void DeviceUnion_get_MotionTrackerCameraDevice_mBAB4C888FB5F767A8FF7C5D845F2FFB97814F9B2 ();
// 0x000007B4 System.Void easyar.VIOCameraDeviceUnion_DeviceUnion::set_MotionTrackerCameraDevice(easyar.MotionTrackerCameraDevice)
extern void DeviceUnion_set_MotionTrackerCameraDevice_m266BD59FD26AF00C372DE661C58E6883A661A8EB ();
// 0x000007B5 easyar.ARKitCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::get_ARKitCameraDevice()
extern void DeviceUnion_get_ARKitCameraDevice_mF3750662F5632D75160E4A2E01CCDE43D7841477 ();
// 0x000007B6 System.Void easyar.VIOCameraDeviceUnion_DeviceUnion::set_ARKitCameraDevice(easyar.ARKitCameraDevice)
extern void DeviceUnion_set_ARKitCameraDevice_mE3DB9D883D9BDE0862CCFF3D7EEDCFFB51BC66CC ();
// 0x000007B7 easyar.ARCoreCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::get_ARCoreCameraDevice()
extern void DeviceUnion_get_ARCoreCameraDevice_m86B0E6B9EE154D770149736DFA5F4CFD83EB1558 ();
// 0x000007B8 System.Void easyar.VIOCameraDeviceUnion_DeviceUnion::set_ARCoreCameraDevice(easyar.ARCoreCameraDevice)
extern void DeviceUnion_set_ARCoreCameraDevice_m3308C8773392C5B96F5FCD860B20A2BF690D2D56 ();
// 0x000007B9 easyar.MotionTrackerCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::op_Explicit(easyar.VIOCameraDeviceUnion_DeviceUnion)
extern void DeviceUnion_op_Explicit_mD4440463B900A4B37E0BA12492E1DF8689AFDF61 ();
// 0x000007BA easyar.ARKitCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::op_Explicit(easyar.VIOCameraDeviceUnion_DeviceUnion)
extern void DeviceUnion_op_Explicit_m449CA26A040CC4397C985B886D619BB75EB47291 ();
// 0x000007BB easyar.ARCoreCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::op_Explicit(easyar.VIOCameraDeviceUnion_DeviceUnion)
extern void DeviceUnion_op_Explicit_m5DD76392883622DB4ABAFE9BC04DD1156A0647F7 ();
// 0x000007BC easyar.VIOCameraDeviceUnion_DeviceUnion easyar.VIOCameraDeviceUnion_DeviceUnion::op_Implicit(easyar.MotionTrackerCameraDevice)
extern void DeviceUnion_op_Implicit_mA9ED1E1375EBE31047347C95F46F7EF9E3324D15 ();
// 0x000007BD easyar.VIOCameraDeviceUnion_DeviceUnion easyar.VIOCameraDeviceUnion_DeviceUnion::op_Implicit(easyar.ARKitCameraDevice)
extern void DeviceUnion_op_Implicit_m36207E4B78ED922B72D33B42D76F241F2753CE6A ();
// 0x000007BE easyar.VIOCameraDeviceUnion_DeviceUnion easyar.VIOCameraDeviceUnion_DeviceUnion::op_Implicit(easyar.ARCoreCameraDevice)
extern void DeviceUnion_op_Implicit_m59C1C7325E063AE41056EEF4CB12732E78350FF1 ();
// 0x000007BF System.Type easyar.VIOCameraDeviceUnion_DeviceUnion::Type()
extern void DeviceUnion_Type_m0AB8A5E98E27F51B7993A4B076983AAB69E7E5BF ();
// 0x000007C0 System.String easyar.VIOCameraDeviceUnion_DeviceUnion::ToString()
extern void DeviceUnion_ToString_m001B6577562B4AFF2DA5165F95C4B1A269DA9A32 ();
// 0x000007C1 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m99F7E4D0CE827BE0F18415AB559A4B3365DE3D08 ();
// 0x000007C2 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0::<CreateMotionTrackerCameraDevice>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__0_m8B5A6794FE0A43A007D120DA965B3CBE10FDD270 ();
// 0x000007C3 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0::<CreateMotionTrackerCameraDevice>b__1()
extern void U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__1_m2EF0F4DCC2C27CF22F8B672A631A5F813D92DEDC ();
// 0x000007C4 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0::<CreateMotionTrackerCameraDevice>b__2()
extern void U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__2_mA2D85802494AFE26CB3942404E813485D0DA04E4 ();
// 0x000007C5 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0::<CreateMotionTrackerCameraDevice>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__3_m3A0510D6D8F5EF8CBF9C61A17FDDFFFFED6B10B3 ();
// 0x000007C6 System.Int32 easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0::<CreateMotionTrackerCameraDevice>b__4()
extern void U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__4_m3D7FE1644F495F50978E9C1D03BFE650C948F1D2 ();
// 0x000007C7 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0::<CreateMotionTrackerCameraDevice>b__5(easyar.InputFrameSink)
extern void U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__5_m0564F7655BE623BA1D038DB6A553A17608F886B0 ();
// 0x000007C8 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m99DD9BC101E969037D1E3F203F6661545DC24F94 ();
// 0x000007C9 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0::<CreateARKitCameraDevice>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__0_mAA44C6A01C2387BEB65A4DB7DEE908B446EE773B ();
// 0x000007CA System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0::<CreateARKitCameraDevice>b__1()
extern void U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__1_mA4149B5859FA68BA6CCE4277030144D00A4355E2 ();
// 0x000007CB System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0::<CreateARKitCameraDevice>b__2()
extern void U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__2_mD9361E9F783A81F508925A2B8BF508F99CA18239 ();
// 0x000007CC System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0::<CreateARKitCameraDevice>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__3_mC7C849DE4085C13F773E527D29D27E9DAC7BFB6C ();
// 0x000007CD System.Int32 easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0::<CreateARKitCameraDevice>b__4()
extern void U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__4_mEB0181C8BD7FE2EED296146C5995AB729EFB0CAB ();
// 0x000007CE System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0::<CreateARKitCameraDevice>b__5(easyar.InputFrameSink)
extern void U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__5_m6A59BA3A9BDF61C8E7B6102037C357AF41402F82 ();
// 0x000007CF System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mA26FA34AA8D1A95F282FF636E4B16118DFB15E67 ();
// 0x000007D0 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0::<CreateARCoreCameraDevice>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__0_mD821C58DC97EDAF33AA2E57345D21CB36377754F ();
// 0x000007D1 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0::<CreateARCoreCameraDevice>b__1()
extern void U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__1_m9AD5578EBC37C71254C1283D30984642A861BD33 ();
// 0x000007D2 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0::<CreateARCoreCameraDevice>b__2()
extern void U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__2_m47E1D988F6EAE2816A8AB275120F0F5D9447C14D ();
// 0x000007D3 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0::<CreateARCoreCameraDevice>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__3_mC65A21FD22B580317D5462BFD5B3195B27D1A0BC ();
// 0x000007D4 System.Int32 easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0::<CreateARCoreCameraDevice>b__4()
extern void U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__4_mF9A87CE0FD9D06CA446E6CAD9DA6ECFFBF361DFB ();
// 0x000007D5 System.Void easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0::<CreateARCoreCameraDevice>b__5(easyar.InputFrameSink)
extern void U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__5_m5C7C5BC2B7C777C41FFEEB57F27A4F26FDDED56F ();
// 0x000007D6 System.Void easyar.Detail_AutoRelease::Add(System.Action)
extern void AutoRelease_Add_mAE4B8D31F19FBE3D2C43C35FC2CBB1FD8A368CF8 ();
// 0x000007D7 T easyar.Detail_AutoRelease::Add(T,System.Action`1<T>)
// 0x000007D8 System.Void easyar.Detail_AutoRelease::Dispose()
extern void AutoRelease_Dispose_m8BE43DC72624325F7EAE73DD38DBFA8A22425745 ();
// 0x000007D9 System.Void easyar.Detail_AutoRelease::.ctor()
extern void AutoRelease__ctor_mD4A2F0D3CEA53F19CB9780F1294269C3820730EF ();
// 0x000007DA System.Boolean easyar.Detail_OptionalOfBuffer::get_has_value()
extern void OptionalOfBuffer_get_has_value_m9D6A59355D2970D878F9515A0F043E56F5D87CCB_AdjustorThunk ();
// 0x000007DB System.Void easyar.Detail_OptionalOfBuffer::set_has_value(System.Boolean)
extern void OptionalOfBuffer_set_has_value_m983B29124CF55FFCD41A9927DB269B7582E091A4_AdjustorThunk ();
// 0x000007DC System.Boolean easyar.Detail_OptionalOfObjectTarget::get_has_value()
extern void OptionalOfObjectTarget_get_has_value_mC647A51D888E6763608BEB804D1A7B3A3904B276_AdjustorThunk ();
// 0x000007DD System.Void easyar.Detail_OptionalOfObjectTarget::set_has_value(System.Boolean)
extern void OptionalOfObjectTarget_set_has_value_mE5F8EEA200FE9B3BF5C56E6C43C96E214C57DB44_AdjustorThunk ();
// 0x000007DE System.Boolean easyar.Detail_OptionalOfTarget::get_has_value()
extern void OptionalOfTarget_get_has_value_m790B50D2027A5C905809232E206494F1F4C91BE7_AdjustorThunk ();
// 0x000007DF System.Void easyar.Detail_OptionalOfTarget::set_has_value(System.Boolean)
extern void OptionalOfTarget_set_has_value_mEBA4B2BBD9E65AA2DCAB70C079E53872730D63F8_AdjustorThunk ();
// 0x000007E0 System.Boolean easyar.Detail_OptionalOfOutputFrame::get_has_value()
extern void OptionalOfOutputFrame_get_has_value_m10A11564384A305155F97AEFBBA72437AD655F29_AdjustorThunk ();
// 0x000007E1 System.Void easyar.Detail_OptionalOfOutputFrame::set_has_value(System.Boolean)
extern void OptionalOfOutputFrame_set_has_value_m9D0E9C612379133C2EAEE95F671153061F79C824_AdjustorThunk ();
// 0x000007E2 System.Boolean easyar.Detail_OptionalOfFrameFilterResult::get_has_value()
extern void OptionalOfFrameFilterResult_get_has_value_mFBECA5EB4063C294A995CDBDEF101DD8F2F6C5EC_AdjustorThunk ();
// 0x000007E3 System.Void easyar.Detail_OptionalOfFrameFilterResult::set_has_value(System.Boolean)
extern void OptionalOfFrameFilterResult_set_has_value_m8ED45EE2429DB6337AB6FE1918A4F56282FA5F2B_AdjustorThunk ();
// 0x000007E4 System.Boolean easyar.Detail_OptionalOfFunctorOfVoidFromOutputFrame::get_has_value()
extern void OptionalOfFunctorOfVoidFromOutputFrame_get_has_value_m662EC61A5F9A7308D525D21DED9760D1857C0DF2_AdjustorThunk ();
// 0x000007E5 System.Void easyar.Detail_OptionalOfFunctorOfVoidFromOutputFrame::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromOutputFrame_set_has_value_m2E510F3094BFDBF761D17C9DAB392203E89FC6AC_AdjustorThunk ();
// 0x000007E6 System.Boolean easyar.Detail_OptionalOfImageTarget::get_has_value()
extern void OptionalOfImageTarget_get_has_value_m42A8C36CCEAAD9ABE06BF3542F6C5ADB8C82FF54_AdjustorThunk ();
// 0x000007E7 System.Void easyar.Detail_OptionalOfImageTarget::set_has_value(System.Boolean)
extern void OptionalOfImageTarget_set_has_value_mF1DD2AFE215A441E298CC7ACA5EEA3687670D7FA_AdjustorThunk ();
// 0x000007E8 System.Boolean easyar.Detail_OptionalOfString::get_has_value()
extern void OptionalOfString_get_has_value_m80AD50BC2F401A100152E0974E13AB1E621EDEAB_AdjustorThunk ();
// 0x000007E9 System.Void easyar.Detail_OptionalOfString::set_has_value(System.Boolean)
extern void OptionalOfString_set_has_value_mCD102F9F525031E72C850FFB20A2A8338FC6CBBA_AdjustorThunk ();
// 0x000007EA System.Boolean easyar.Detail_OptionalOfFunctorOfVoidFromInputFrame::get_has_value()
extern void OptionalOfFunctorOfVoidFromInputFrame_get_has_value_mFDBB48FEFEAFC49D6BC7AD4E78FC811D5D15E67C_AdjustorThunk ();
// 0x000007EB System.Void easyar.Detail_OptionalOfFunctorOfVoidFromInputFrame::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromInputFrame_set_has_value_mEF838067075D602261406DBD08254B78C342F486_AdjustorThunk ();
// 0x000007EC System.Boolean easyar.Detail_OptionalOfFunctorOfVoidFromCameraState::get_has_value()
extern void OptionalOfFunctorOfVoidFromCameraState_get_has_value_mF83C24C0E83A54FA00976906CC8821AB3331E2C4_AdjustorThunk ();
// 0x000007ED System.Void easyar.Detail_OptionalOfFunctorOfVoidFromCameraState::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromCameraState_set_has_value_m2D8B805B1D5DF9FBCDD1988E734A2F47DAEBD7EF_AdjustorThunk ();
// 0x000007EE System.Boolean easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString::get_has_value()
extern void OptionalOfFunctorOfVoidFromPermissionStatusAndString_get_has_value_m54C2C1EAD70D4507DE32FB7FF3FDA020BA38A922_AdjustorThunk ();
// 0x000007EF System.Void easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromPermissionStatusAndString_set_has_value_mFAB6CFFD20AC4B462FFF5D0EFC1AA368AB525AE1_AdjustorThunk ();
// 0x000007F0 System.Boolean easyar.Detail_OptionalOfFunctorOfVoidFromRecordStatusAndString::get_has_value()
extern void OptionalOfFunctorOfVoidFromRecordStatusAndString_get_has_value_m49D7BD70815B7C658D813D65987BEE27B6AAEE58_AdjustorThunk ();
// 0x000007F1 System.Void easyar.Detail_OptionalOfFunctorOfVoidFromRecordStatusAndString::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromRecordStatusAndString_set_has_value_m1D674CF7F28B654FD2621066E2AAE90898DB77BE_AdjustorThunk ();
// 0x000007F2 System.Boolean easyar.Detail_OptionalOfMatrix44F::get_has_value()
extern void OptionalOfMatrix44F_get_has_value_m6CE4342DEF002C6CC751406ACC5702461A5C1079_AdjustorThunk ();
// 0x000007F3 System.Void easyar.Detail_OptionalOfMatrix44F::set_has_value(System.Boolean)
extern void OptionalOfMatrix44F_set_has_value_mBE64ECC5FBD0370BDFE9F0B715B3B2E2EAD0285F_AdjustorThunk ();
// 0x000007F4 System.Boolean easyar.Detail_OptionalOfFunctorOfVoidFromBool::get_has_value()
extern void OptionalOfFunctorOfVoidFromBool_get_has_value_mE800CC81603A472DFE0A9448225FF98C08D698E9_AdjustorThunk ();
// 0x000007F5 System.Void easyar.Detail_OptionalOfFunctorOfVoidFromBool::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromBool_set_has_value_m1F2CD7EC7EC7FF7D8D9E814478AAD9CEBE8E45E8_AdjustorThunk ();
// 0x000007F6 System.Boolean easyar.Detail_OptionalOfImage::get_has_value()
extern void OptionalOfImage_get_has_value_mF6E55AC76F8950B0B9633BCFAD0E4D278C10FCCD_AdjustorThunk ();
// 0x000007F7 System.Void easyar.Detail_OptionalOfImage::set_has_value(System.Boolean)
extern void OptionalOfImage_set_has_value_m4ED800FE9DA50381E49E1A8D3DC583B82CD77980_AdjustorThunk ();
// 0x000007F8 System.Boolean easyar.Detail_OptionalOfFunctorOfVoidFromVideoStatus::get_has_value()
extern void OptionalOfFunctorOfVoidFromVideoStatus_get_has_value_m92C403E87E8C4CC2324557A0582E7F92892FEDDF_AdjustorThunk ();
// 0x000007F9 System.Void easyar.Detail_OptionalOfFunctorOfVoidFromVideoStatus::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromVideoStatus_set_has_value_m05AE6A060EE9BFF950C8F69BF92D50F4CE448865_AdjustorThunk ();
// 0x000007FA System.Boolean easyar.Detail_OptionalOfFunctorOfVoid::get_has_value()
extern void OptionalOfFunctorOfVoid_get_has_value_m75B9606CBBA1854B6BF00D025DCD682000C4BAFB_AdjustorThunk ();
// 0x000007FB System.Void easyar.Detail_OptionalOfFunctorOfVoid::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoid_set_has_value_m6FF72DD8BF8C958D2DB33A82D6405D9A1BF599AB_AdjustorThunk ();
// 0x000007FC System.Boolean easyar.Detail_OptionalOfFunctorOfVoidFromFeedbackFrame::get_has_value()
extern void OptionalOfFunctorOfVoidFromFeedbackFrame_get_has_value_m15BEFDE0C7E92204EE15D0F733D1C1BBDE8F362D_AdjustorThunk ();
// 0x000007FD System.Void easyar.Detail_OptionalOfFunctorOfVoidFromFeedbackFrame::set_has_value(System.Boolean)
extern void OptionalOfFunctorOfVoidFromFeedbackFrame_set_has_value_m74CD0885E8287EC5B4405DA5DAB4729B3D8E4600_AdjustorThunk ();
// 0x000007FE System.Void easyar.Detail_<>c::.cctor()
extern void U3CU3Ec__cctor_m3DC7EC5EEED0D4E244C5688653FEE13108C02AD8 ();
// 0x000007FF System.Void easyar.Detail_<>c::.ctor()
extern void U3CU3Ec__ctor_m0943817FFA14C12B3378D48B3CF247F53F52E95B ();
// 0x00000800 easyar.Vec3F easyar.Detail_<>c::<ListOfVec3F_to_c>b__670_0(easyar.Vec3F)
extern void U3CU3Ec_U3CListOfVec3F_to_cU3Eb__670_0_mB742E1489F5031554C832BEBD7BEE946AD19F7C1 ();
// 0x00000801 System.IntPtr easyar.Detail_<>c::<ListOfTargetInstance_to_c>b__672_0(easyar.TargetInstance)
extern void U3CU3Ec_U3CListOfTargetInstance_to_cU3Eb__672_0_mCB4EDFB4C34657AB33732166BBFBF0947D7718CD ();
// 0x00000802 easyar.Detail_OptionalOfFrameFilterResult easyar.Detail_<>c::<ListOfOptionalOfFrameFilterResult_to_c>b__676_0(easyar.Optional`1<easyar.FrameFilterResult>)
extern void U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_to_cU3Eb__676_0_m2C40E4D867FDFF9D94A32FBF3544B4F920AC85FC ();
// 0x00000803 easyar.Detail_OptionalOfFrameFilterResult easyar.Detail_<>c::<ListOfOptionalOfFrameFilterResult_to_c>b__676_1(easyar.Optional`1<easyar.FrameFilterResult>)
extern void U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_to_cU3Eb__676_1_m33EC9122C276F8F7B6313C05AA306FA26A1058AE ();
// 0x00000804 easyar.Optional`1<easyar.FrameFilterResult> easyar.Detail_<>c::<ListOfOptionalOfFrameFilterResult_from_c>b__677_0(easyar.Detail_OptionalOfFrameFilterResult)
extern void U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_from_cU3Eb__677_0_mA7B7FC3764BEE5703D78BD0E24C58B058A8ACE85 ();
// 0x00000805 System.IntPtr easyar.Detail_<>c::<ListOfTarget_to_c>b__688_0(easyar.Target)
extern void U3CU3Ec_U3CListOfTarget_to_cU3Eb__688_0_mC6DB30933522CA28F378DF1AF82E8D97BB432B03 ();
// 0x00000806 System.IntPtr easyar.Detail_<>c::<ListOfImage_to_c>b__691_0(easyar.Image)
extern void U3CU3Ec_U3CListOfImage_to_cU3Eb__691_0_m3B33F0E19F049AD8FA8F35C41767A82F5458E789 ();
// 0x00000807 easyar.BlockInfo easyar.Detail_<>c::<ListOfBlockInfo_to_c>b__698_0(easyar.BlockInfo)
extern void U3CU3Ec_U3CListOfBlockInfo_to_cU3Eb__698_0_mE78DD82C4A015A7645D604B55F6BD43B4C381362 ();
// 0x00000808 System.IntPtr easyar.Detail_<>c::<ListOfPlaneData_to_c>b__725_0(easyar.PlaneData)
extern void U3CU3Ec_U3CListOfPlaneData_to_cU3Eb__725_0_m9E2E72A598087ADC8ED6358D484077D221268D81 ();
// 0x00000809 System.IntPtr easyar.Detail_<>c::<ListOfOutputFrame_to_c>b__756_0(easyar.OutputFrame)
extern void U3CU3Ec_U3CListOfOutputFrame_to_cU3Eb__756_0_m032F25FBB55F82055199B93801F2F6AF16EC06BA ();
// 0x0000080A easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_0(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_0_mB6531CDE62AC7ECD5BA6CF40D93100DE110F64BE ();
// 0x0000080B easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_1(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_1_m8742C78FC152D3B88EB84234B678B0C5C3D9B49E ();
// 0x0000080C easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_2(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_2_m732B0F7962DC9CF06609FBD1A9909D2C1967C70B ();
// 0x0000080D easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_3(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_3_m76C44DF1147F890829C9DBB56D5D991068136E02 ();
// 0x0000080E easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_4(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_4_m0B9734D2E88C71010508D5C739A3EA645DC6D542 ();
// 0x0000080F easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_5(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_5_mF8B60ABA3D8C428CDF75892E96029679D4D2E0B2 ();
// 0x00000810 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_6(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_6_mBFD23EA5B44B8A438577AFA355709F9BCAF0E593 ();
// 0x00000811 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_7(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_7_m8658C4B5830F812F411386C20546EB49080F5F7E ();
// 0x00000812 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_8(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_8_m26478080CA9B3F1A422946F5F193F047EE8EA952 ();
// 0x00000813 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_9(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_9_mD0E2B09AAA1528D3B6F6C3A12FFA93BC66175921 ();
// 0x00000814 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_10(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_10_mA13202FAA87DD6E34C7EA0AD92CB3DE9544AE96A ();
// 0x00000815 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_11(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_11_mF258A7C4EE78963CAB7D7CB786B3C1C93B372D61 ();
// 0x00000816 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_12(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_12_m4D7CE18FB1AFE894B2565776CB58AA3A59B039E3 ();
// 0x00000817 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_13(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_13_mA94159B5D15381C3B33BA89F71DFA04FF7395783 ();
// 0x00000818 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_14(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_14_mC5B86A953237C767D338101823EA8E8D8BF02EB7 ();
// 0x00000819 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_15(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_15_m3090D8AE5299B14DBE37ED66AFDFCFA92906A7F1 ();
// 0x0000081A easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_16(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_16_m826C9E2E10EA210FD7A5D7283B4A7C60DAC2E9B1 ();
// 0x0000081B easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_17(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_17_m1E65E57ED9461A0BC84571EA8D7D5F7796C0216B ();
// 0x0000081C easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_18(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_18_mF1FD8A2EEF7452F3E70B5F4C15E9CC9719124157 ();
// 0x0000081D easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_19(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_19_m62097AF3D3481C3DF291CB32C13ADE76BC7C69BC ();
// 0x0000081E easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_20(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_20_m1BB65C44681FA483BB94643F2D2AF26193233ACF ();
// 0x0000081F easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_21(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_21_m6EC677055FCF96A594795F59000C8FB209C94006 ();
// 0x00000820 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_22(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_22_m433572B39C741A6A4C87FA667C8BC260B981B802 ();
// 0x00000821 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_23(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_23_mA0FAE16A96CD7A4787C4B9B9F5D350852D44726C ();
// 0x00000822 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_24(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_24_mFB2017ECA151425D22DBC4A481461095199F72F8 ();
// 0x00000823 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_25(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_25_m510A65258DB78EC50AC1E8039338530B8EE62E0A ();
// 0x00000824 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_26(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_26_mE61B3C188E5DCB6857BD574732C80A082DE7F625 ();
// 0x00000825 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_27(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_27_mCC1DA20B82583641485121043759F6244AAC27B8 ();
// 0x00000826 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_28(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_28_m2F676F8600BCB49A04B4B79243A950ADE6B746C7 ();
// 0x00000827 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_29(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_29_m9BD1F5CE466969DFD1E3512F1DB4E8FA3C218598 ();
// 0x00000828 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_30(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_30_mA66050965C76053F038AF1D5F853A33968D34312 ();
// 0x00000829 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_31(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_31_m24E076EBAC672E4A7B181F77D59F81E53C78F775 ();
// 0x0000082A easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_32(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_32_m78730DB8F1DA0C73C90A4FBA8AFF8149DEFB48AC ();
// 0x0000082B easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_33(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_33_mEE86B1F692DA8C5B36770E69ECAF8469D680EBE1 ();
// 0x0000082C easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_34(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_34_m0B48923CB6F7A6115362142623914EDA1A897423 ();
// 0x0000082D easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_35(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_35_m6FD600984067C2AB5AC5CA547FFA0E6A2A151F3D ();
// 0x0000082E easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_36(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_36_m7A65116E6949D51828CA641FC9DC452EFBD8D0BE ();
// 0x0000082F easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_37(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_37_mBA644C9DE0F1BB8B8FA378A886CB8A1F45E34FEF ();
// 0x00000830 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_38(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_38_mE703DED0B1C1B0D27559F3552CCF66AB91CFE077 ();
// 0x00000831 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_39(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_39_mB8555D7F2FEC26CFB04601F6D5227DCEDC440401 ();
// 0x00000832 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_40(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_40_m6EED4438F52C7FE51E8CB6B22D08E519B8A70A95 ();
// 0x00000833 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_41(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_41_m8540468EBCA24CC3E5F911034576AE184F9AD53C ();
// 0x00000834 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_42(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_42_m9C5F39B961FCB4DB2615E0721148C0925347A70D ();
// 0x00000835 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_43(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_43_m7C2C5B4C5100FA89B7FA83451EAD3ECF73EDD019 ();
// 0x00000836 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_44(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_44_mD6C4F75B8CA02E02BB7B5EF176CB3D4BF7580866 ();
// 0x00000837 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_45(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_45_mF686CBFB65364C75FB34CD1A27DE17E98706BAD1 ();
// 0x00000838 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_46(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_46_mEA9B9CD1938D4223ECFA90958EADAB2A1BD7B51A ();
// 0x00000839 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_47(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_47_m6E77968C3729843CBFF5DB41D16158F06A49CD62 ();
// 0x0000083A easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_48(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_48_m95A24EB55B8A6FB04DCA21AB91105A8C00FDA39D ();
// 0x0000083B easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_49(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_49_mEBEAC016D9E5CA848D55CFFA86E8B416039DA2B8 ();
// 0x0000083C easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_50(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_50_mF52309217A0955C2EDFF1296941D8426A1CAB6E0 ();
// 0x0000083D easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_51(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_51_m31FFF619E81548CA3FBD827D0CE20E6CA70084C2 ();
// 0x0000083E easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_52(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_52_m1DDCA0CEA1F3046FE8879ABDEF2E69FE41026528 ();
// 0x0000083F easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_53(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_53_m1C25B67B3881E106069B23B9D680BE5C96CD14B3 ();
// 0x00000840 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_54(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_54_mEC28EF6D4EE3D1719E2CED9C66665F3859AAEA32 ();
// 0x00000841 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_55(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_55_m4791C9E9E4378629C01F7C1B25F4795850536500 ();
// 0x00000842 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_56(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_56_m6AB781470E7B9B4945259B9D6AFBE834FBB2BCDC ();
// 0x00000843 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_57(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_57_m58CC6B94FAB00FA09471E921A1367E1B7262661A ();
// 0x00000844 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_58(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_58_m0A246396DE2F582C2F104577C86FE3D3E4258694 ();
// 0x00000845 easyar.RefBase easyar.Detail_<>c::<.cctor>b__758_59(System.IntPtr)
extern void U3CU3Ec_U3C_cctorU3Eb__758_59_m023390C7F1D99DB30469AB0F863483B4BA990E95 ();
// 0x00000846 System.Void easyar.Detail_<>c__DisplayClass681_0::.ctor()
extern void U3CU3Ec__DisplayClass681_0__ctor_m602E4532ADF4B2FB37BF3CC3C781B67692CECC8F ();
// 0x00000847 System.Void easyar.Detail_<>c__DisplayClass681_0::<FunctorOfVoidFromOutputFrame_func>b__0()
extern void U3CU3Ec__DisplayClass681_0_U3CFunctorOfVoidFromOutputFrame_funcU3Eb__0_mA48DDF43C65854B34F7FC69EBFB43D026EEFC7EF ();
// 0x00000848 System.Void easyar.Detail_<>c__DisplayClass685_0::.ctor()
extern void U3CU3Ec__DisplayClass685_0__ctor_m370CAA00B4245A9F144ECE191BED309189EDCEF8 ();
// 0x00000849 System.Void easyar.Detail_<>c__DisplayClass685_0::<FunctorOfVoidFromTargetAndBool_func>b__0()
extern void U3CU3Ec__DisplayClass685_0_U3CFunctorOfVoidFromTargetAndBool_funcU3Eb__0_mEDBAAA48FD121F3EA83B3222E928E55863248861 ();
// 0x0000084A System.Void easyar.Detail_<>c__DisplayClass695_0::.ctor()
extern void U3CU3Ec__DisplayClass695_0__ctor_m482D8BF13DDAE6592FE71F9A163A5CC1A9A396D4 ();
// 0x0000084B System.Void easyar.Detail_<>c__DisplayClass695_0::<FunctorOfVoidFromCloudRecognizationResult_func>b__0()
extern void U3CU3Ec__DisplayClass695_0_U3CFunctorOfVoidFromCloudRecognizationResult_funcU3Eb__0_m515BA1DCBDD4FFB1D69B5ABBEA922522341CE7C6 ();
// 0x0000084C System.Void easyar.Detail_<>c__DisplayClass702_0::.ctor()
extern void U3CU3Ec__DisplayClass702_0__ctor_m5D8B12D24C8B5468EB9FD35CCC4853C8D40A278D ();
// 0x0000084D System.Void easyar.Detail_<>c__DisplayClass702_0::<FunctorOfVoidFromInputFrame_func>b__0()
extern void U3CU3Ec__DisplayClass702_0_U3CFunctorOfVoidFromInputFrame_funcU3Eb__0_mED474FB77EB85ECBF8474B82E38A6049C2373A34 ();
// 0x0000084E System.Void easyar.Detail_<>c__DisplayClass749_0::.ctor()
extern void U3CU3Ec__DisplayClass749_0__ctor_mEB826B947A67BE418F91196EB9031F746A574580 ();
// 0x0000084F System.Void easyar.Detail_<>c__DisplayClass749_0::<FunctorOfVoidFromFeedbackFrame_func>b__0()
extern void U3CU3Ec__DisplayClass749_0_U3CFunctorOfVoidFromFeedbackFrame_funcU3Eb__0_mD5BF5086117AD10EB440FC6C2D8E7BFAB566B422 ();
// 0x00000850 System.Void easyar.Detail_<>c__DisplayClass753_0::.ctor()
extern void U3CU3Ec__DisplayClass753_0__ctor_mC4A7CE8B4110BBE37D0E1F2FB0E358DE4AABAD23 ();
// 0x00000851 System.Void easyar.Detail_<>c__DisplayClass753_0::<FunctorOfOutputFrameFromListOfOutputFrame_func>b__0(easyar.OutputFrame)
extern void U3CU3Ec__DisplayClass753_0_U3CFunctorOfOutputFrameFromListOfOutputFrame_funcU3Eb__0_m5BA2BBAF4FA9D414A0FD4769CF4DAE7344D4C86C ();
// 0x00000852 System.Void easyar.Detail_<>c__DisplayClass753_1::.ctor()
extern void U3CU3Ec__DisplayClass753_1__ctor_m674E015D47946D9455B1F017871C2C4FFD75C714 ();
// 0x00000853 System.Void easyar.Detail_<>c__DisplayClass753_1::<FunctorOfOutputFrameFromListOfOutputFrame_func>b__1()
extern void U3CU3Ec__DisplayClass753_1_U3CFunctorOfOutputFrameFromListOfOutputFrame_funcU3Eb__1_m87C5C3DE348C86AB721C1EBC74726C5B3743F0E6 ();
// 0x00000854 System.Void easyar.RefBase_Retainer::.ctor(System.Object,System.IntPtr)
extern void Retainer__ctor_m5872905A8AAEDC46956901D23FF5265ADF283918 ();
// 0x00000855 System.Void easyar.RefBase_Retainer::Invoke(System.IntPtr,System.IntPtr&)
extern void Retainer_Invoke_m1695FE0BB2F8417A4132AF5BAF08969A1773A0F3 ();
// 0x00000856 System.IAsyncResult easyar.RefBase_Retainer::BeginInvoke(System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void Retainer_BeginInvoke_m7BF57438B1E42AAC90EF17BB50859EFB56DDA39C ();
// 0x00000857 System.Void easyar.RefBase_Retainer::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void Retainer_EndInvoke_m3C78A070621497EA67822BC148A36F9B23ED6993 ();
// 0x00000858 System.Void easyar.ObjectTarget_<>c::.cctor()
extern void U3CU3Ec__cctor_m0779763872117F69A04F0046A0AABDACE4EB38DE ();
// 0x00000859 System.Void easyar.ObjectTarget_<>c::.ctor()
extern void U3CU3Ec__ctor_m09B49C3ED78AF55B061A7F24CF187215F2A08EDB ();
// 0x0000085A easyar.Optional`1<easyar.ObjectTarget> easyar.ObjectTarget_<>c::<createFromParameters>b__4_0(easyar.Detail_OptionalOfObjectTarget)
extern void U3CU3Ec_U3CcreateFromParametersU3Eb__4_0_m51E2309D5EF2DA96B3F1FD1EBFD1CD0EB045CA85 ();
// 0x0000085B easyar.Optional`1<easyar.ObjectTarget> easyar.ObjectTarget_<>c::<createFromObjectFile>b__5_0(easyar.Detail_OptionalOfObjectTarget)
extern void U3CU3Ec_U3CcreateFromObjectFileU3Eb__5_0_m169692AA05D6E2A0D59A1EAE25773CAB99200ECE ();
// 0x0000085C System.Void easyar.CloudRecognizationResult_<>c::.cctor()
extern void U3CU3Ec__cctor_m72DD153E5338B35AE80FA404593DA48DFBCA831F ();
// 0x0000085D System.Void easyar.CloudRecognizationResult_<>c::.ctor()
extern void U3CU3Ec__ctor_m96115AD764C88912907865EB19A46FC16403322C ();
// 0x0000085E easyar.Optional`1<easyar.ImageTarget> easyar.CloudRecognizationResult_<>c::<getTarget>b__4_0(easyar.Detail_OptionalOfImageTarget)
extern void U3CU3Ec_U3CgetTargetU3Eb__4_0_m68F25BC99DE96CCF5A7ED2D662ED1826C2DA7BF4 ();
// 0x0000085F System.Void easyar.CloudRecognizationResult_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m404BF93F588419F762746DABBC70992680ABC305 ();
// 0x00000860 easyar.Optional`1<System.String> easyar.CloudRecognizationResult_<>c__DisplayClass5_0::<getUnknownErrorMessage>b__0(easyar.Detail_OptionalOfString)
extern void U3CU3Ec__DisplayClass5_0_U3CgetUnknownErrorMessageU3Eb__0_m44EFC3E7F7C3E62F06C2D825CBD154C46612F5EA ();
// 0x00000861 System.Void easyar.Buffer_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m8A9F66B1FD1FE8AABFDA2CF8F3E4B0FE7395A6E7 ();
// 0x00000862 System.Void easyar.Buffer_<>c__DisplayClass11_0::<wrapByteArray>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CwrapByteArrayU3Eb__0_m972D8122BF86C0A71FE820914AF6E5FAD5BCE713 ();
// 0x00000863 System.Void easyar.Buffer_<>c::.cctor()
extern void U3CU3Ec__cctor_m065DA028A543D550A473BCE474064611AB0090BA ();
// 0x00000864 System.Void easyar.Buffer_<>c::.ctor()
extern void U3CU3Ec__ctor_mB17B2AC34CD701BCE875E47593636B377C0DFAA7 ();
// 0x00000865 System.Void easyar.Buffer_<>c::<wrapByteArray>b__12_0()
extern void U3CU3Ec_U3CwrapByteArrayU3Eb__12_0_mC824B59883D481C967DA3C1DBD87F584FC263B94 ();
// 0x00000866 System.Void easyar.Buffer_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m95DF00D0214120504C2E18F822AAE512A00B0C5E ();
// 0x00000867 System.Void easyar.Buffer_<>c__DisplayClass13_0::<wrapByteArray>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CwrapByteArrayU3Eb__0_mCF78844245B8665A8C54566575EBD5D41AACA0DD ();
// 0x00000868 System.Void easyar.BufferDictionary_<>c::.cctor()
extern void U3CU3Ec__cctor_m7653F87AA49A321CC182C0A13854EDAF15902C63 ();
// 0x00000869 System.Void easyar.BufferDictionary_<>c::.ctor()
extern void U3CU3Ec__ctor_m8B266FAF0639DC53F1672DC824921E64A2EFBAC2 ();
// 0x0000086A easyar.Optional`1<easyar.Buffer> easyar.BufferDictionary_<>c::<tryGet>b__6_0(easyar.Detail_OptionalOfBuffer)
extern void U3CU3Ec_U3CtryGetU3Eb__6_0_mA219E836ADB517B00BFD630D7CFDEC40477EDB11 ();
// 0x0000086B System.Void easyar.BufferPool_<>c::.cctor()
extern void U3CU3Ec__cctor_m481DD56D9561C648112205D4408EF59B40E3CCA8 ();
// 0x0000086C System.Void easyar.BufferPool_<>c::.ctor()
extern void U3CU3Ec__ctor_m5BE1BC8C717A47D976E80B0ACED311A92EBC6F64 ();
// 0x0000086D easyar.Optional`1<easyar.Buffer> easyar.BufferPool_<>c::<tryAcquire>b__7_0(easyar.Detail_OptionalOfBuffer)
extern void U3CU3Ec_U3CtryAcquireU3Eb__7_0_m2EA32E5FD6B3A0FA3C868DEEB11B08305E3A417C ();
// 0x0000086E System.Void easyar.CameraDevice_<>c::.cctor()
extern void U3CU3Ec__cctor_mBBB49E8A52B7A8030B281CF67238BB235DD396A5 ();
// 0x0000086F System.Void easyar.CameraDevice_<>c::.ctor()
extern void U3CU3Ec__ctor_m8CB3D683BFC330D0E2E58BB85010598DF3FB6362 ();
// 0x00000870 easyar.Detail_OptionalOfFunctorOfVoidFromCameraState easyar.CameraDevice_<>c::<setStateChangedCallback>b__10_0(easyar.Optional`1<System.Action`1<easyar.CameraState>>)
extern void U3CU3Ec_U3CsetStateChangedCallbackU3Eb__10_0_m8B93FD9E9A7ACA9ED18D693F41F77C2B757D88BC ();
// 0x00000871 easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString easyar.CameraDevice_<>c::<requestPermissions>b__11_0(easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>)
extern void U3CU3Ec_U3CrequestPermissionsU3Eb__11_0_m27BFD8E8AACB63F3F124DA7126C0FC0864CDDBE1 ();
// 0x00000872 System.Void easyar.ImageTarget_<>c::.cctor()
extern void U3CU3Ec__cctor_mD4AB9073E86E5B28DE3659477DF7606FEBF8786D ();
// 0x00000873 System.Void easyar.ImageTarget_<>c::.ctor()
extern void U3CU3Ec__ctor_m6C1AF76F5EE67A785A3CD3B4D432E6C0034C7939 ();
// 0x00000874 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget_<>c::<createFromParameters>b__4_0(easyar.Detail_OptionalOfImageTarget)
extern void U3CU3Ec_U3CcreateFromParametersU3Eb__4_0_mD38A5988BEDAF5A8DDF77F07BD6ADC5E646D8E14 ();
// 0x00000875 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget_<>c::<createFromTargetFile>b__5_0(easyar.Detail_OptionalOfImageTarget)
extern void U3CU3Ec_U3CcreateFromTargetFileU3Eb__5_0_mE8EEECD971F445E1489931F0458643D458210353 ();
// 0x00000876 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget_<>c::<createFromTargetData>b__6_0(easyar.Detail_OptionalOfImageTarget)
extern void U3CU3Ec_U3CcreateFromTargetDataU3Eb__6_0_m45889D88802740119F44F0C929B94D48B55C3EBC ();
// 0x00000877 easyar.Optional`1<easyar.ImageTarget> easyar.ImageTarget_<>c::<createFromImageFile>b__8_0(easyar.Detail_OptionalOfImageTarget)
extern void U3CU3Ec_U3CcreateFromImageFileU3Eb__8_0_m66489C33338E5563DF944E9512027A3BB189BC68 ();
// 0x00000878 System.Void easyar.Recorder_<>c::.cctor()
extern void U3CU3Ec__cctor_m610F49054D1BCB851E0F80AC5A5F67C2F8B87C11 ();
// 0x00000879 System.Void easyar.Recorder_<>c::.ctor()
extern void U3CU3Ec__ctor_m856D0A47BF1E2CDF3060360567C5FE9FB1E92423 ();
// 0x0000087A easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString easyar.Recorder_<>c::<requestPermissions>b__4_0(easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>)
extern void U3CU3Ec_U3CrequestPermissionsU3Eb__4_0_m97EEC5E6A6D583E870950CDDC0A245C24F03557D ();
// 0x0000087B easyar.Detail_OptionalOfFunctorOfVoidFromRecordStatusAndString easyar.Recorder_<>c::<create>b__5_0(easyar.Optional`1<System.Action`2<easyar.RecordStatus,System.String>>)
extern void U3CU3Ec_U3CcreateU3Eb__5_0_mE4525BD21BC15238AADF1D4EF24FE9F715BBB9E5 ();
// 0x0000087C System.Void easyar.SparseSpatialMapResult_<>c::.cctor()
extern void U3CU3Ec__cctor_mCE30D9E439105CDE8BD8C7B6E74383707C483D39 ();
// 0x0000087D System.Void easyar.SparseSpatialMapResult_<>c::.ctor()
extern void U3CU3Ec__ctor_m7AC9347D0F316191C0FFE044E3CA656CFD99B4A1 ();
// 0x0000087E easyar.Optional`1<easyar.Matrix44F> easyar.SparseSpatialMapResult_<>c::<getVioPose>b__4_0(easyar.Detail_OptionalOfMatrix44F)
extern void U3CU3Ec_U3CgetVioPoseU3Eb__4_0_m054AFFE3A55816B9C36DC4D676543B92CCF1A040 ();
// 0x0000087F easyar.Optional`1<easyar.Matrix44F> easyar.SparseSpatialMapResult_<>c::<getMapPose>b__5_0(easyar.Detail_OptionalOfMatrix44F)
extern void U3CU3Ec_U3CgetMapPoseU3Eb__5_0_mC1D0B1B6C0A71E96139E65BDF93DF94145D71728 ();
// 0x00000880 System.Void easyar.SparseSpatialMap_<>c::.cctor()
extern void U3CU3Ec__cctor_mE16731AF5F6D4410C9C8BCCCAF8B84DB8C32AFBE ();
// 0x00000881 System.Void easyar.SparseSpatialMap_<>c::.ctor()
extern void U3CU3Ec__ctor_mBD11BC42157A455694C3447AB2BED62B98F4B964 ();
// 0x00000882 easyar.Detail_OptionalOfFunctorOfVoidFromBool easyar.SparseSpatialMap_<>c::<unloadMap>b__16_0(easyar.Optional`1<System.Action`1<System.Boolean>>)
extern void U3CU3Ec_U3CunloadMapU3Eb__16_0_m5ADEF8E56007CEBF09FC93910AF77876A7292BC0 ();
// 0x00000883 System.Void easyar.SparseSpatialMapManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m2C467F17D39998B965993C04596764AF57815A7B ();
// 0x00000884 System.Void easyar.SparseSpatialMapManager_<>c::.ctor()
extern void U3CU3Ec__ctor_mD5E5993ED8014347554FEDD1D39D61DD463A3129 ();
// 0x00000885 easyar.Detail_OptionalOfImage easyar.SparseSpatialMapManager_<>c::<host>b__5_0(easyar.Optional`1<easyar.Image>)
extern void U3CU3Ec_U3ChostU3Eb__5_0_m36F45650D3152C8F37EC93A6ED734F7625D54D5B ();
// 0x00000886 System.Void easyar.VideoPlayer_<>c::.cctor()
extern void U3CU3Ec__cctor_m530822C857027158CEB3BA5DAE674B49FF328774 ();
// 0x00000887 System.Void easyar.VideoPlayer_<>c::.ctor()
extern void U3CU3Ec__ctor_mFF3C81E2678E31BACC53573FB9476AB427FD166E ();
// 0x00000888 easyar.Detail_OptionalOfFunctorOfVoidFromVideoStatus easyar.VideoPlayer_<>c::<open>b__7_0(easyar.Optional`1<System.Action`1<easyar.VideoStatus>>)
extern void U3CU3Ec_U3CopenU3Eb__7_0_m2A00372C8B19E06E75B185389E6C2EC22D945EC9 ();
// 0x00000889 System.Void easyar.ImageHelper_<>c::.cctor()
extern void U3CU3Ec__cctor_m0F1F8603DF79ED01311A1E068ABAAEE43D7500B9 ();
// 0x0000088A System.Void easyar.ImageHelper_<>c::.ctor()
extern void U3CU3Ec__ctor_m403553CD537569CEFD45CC84FF3AB86EB95FF0B1 ();
// 0x0000088B easyar.Optional`1<easyar.Image> easyar.ImageHelper_<>c::<decode>b__0_0(easyar.Detail_OptionalOfImage)
extern void U3CU3Ec_U3CdecodeU3Eb__0_0_m9A92FA81D1BF151B4261C162DD8BA98ADD63D2CC ();
// 0x0000088C System.Void easyar.SignalSource_<>c::.cctor()
extern void U3CU3Ec__cctor_m7D0A60F95D52D57194A599ABF5DAE83F2DCA43E6 ();
// 0x0000088D System.Void easyar.SignalSource_<>c::.ctor()
extern void U3CU3Ec__ctor_m75E56CA9F1DDE9FF506886F144DD6CEBE5AEAB7F ();
// 0x0000088E easyar.Detail_OptionalOfFunctorOfVoid easyar.SignalSource_<>c::<setHandler>b__3_0(easyar.Optional`1<System.Action>)
extern void U3CU3Ec_U3CsetHandlerU3Eb__3_0_m48B458C910BA126C8F8997AAB4503B19C6AB0A21 ();
// 0x0000088F System.Void easyar.InputFrameSource_<>c::.cctor()
extern void U3CU3Ec__cctor_mB902917647CADF0DAA4030D14299FEE34E851180 ();
// 0x00000890 System.Void easyar.InputFrameSource_<>c::.ctor()
extern void U3CU3Ec__ctor_m72193E86114F81ECC5D7266C46FAC37D5E90B24B ();
// 0x00000891 easyar.Detail_OptionalOfFunctorOfVoidFromInputFrame easyar.InputFrameSource_<>c::<setHandler>b__3_0(easyar.Optional`1<System.Action`1<easyar.InputFrame>>)
extern void U3CU3Ec_U3CsetHandlerU3Eb__3_0_m2463EC0AA9C09C79BA193FB3CC93ED4F664FA23E ();
// 0x00000892 System.Void easyar.OutputFrameSource_<>c::.cctor()
extern void U3CU3Ec__cctor_mD5A59A0815B2F87FA19F2E27F8A0069B694C2F50 ();
// 0x00000893 System.Void easyar.OutputFrameSource_<>c::.ctor()
extern void U3CU3Ec__ctor_m49DEAC095E57442161997DEF2CC9E9BE0E682678 ();
// 0x00000894 easyar.Detail_OptionalOfFunctorOfVoidFromOutputFrame easyar.OutputFrameSource_<>c::<setHandler>b__3_0(easyar.Optional`1<System.Action`1<easyar.OutputFrame>>)
extern void U3CU3Ec_U3CsetHandlerU3Eb__3_0_m237A88462E7E4129455A23453E477D28374DD26B ();
// 0x00000895 System.Void easyar.FeedbackFrameSource_<>c::.cctor()
extern void U3CU3Ec__cctor_m556348CD1B7516FBB54D7AAAF85BCAA760A0C04A ();
// 0x00000896 System.Void easyar.FeedbackFrameSource_<>c::.ctor()
extern void U3CU3Ec__ctor_m42144A8A2B4EFC746BD38F326110D98D6D12A2BD ();
// 0x00000897 easyar.Detail_OptionalOfFunctorOfVoidFromFeedbackFrame easyar.FeedbackFrameSource_<>c::<setHandler>b__3_0(easyar.Optional`1<System.Action`1<easyar.FeedbackFrame>>)
extern void U3CU3Ec_U3CsetHandlerU3Eb__3_0_m7DB6260C930511E1D32A64DEC1F376D6D8463F5D ();
// 0x00000898 System.Void easyar.OutputFrameBuffer_<>c::.cctor()
extern void U3CU3Ec__cctor_mCE9410F6B3C54BE488F6CD6CDCD0C126B37D68FF ();
// 0x00000899 System.Void easyar.OutputFrameBuffer_<>c::.ctor()
extern void U3CU3Ec__ctor_m4A4C25B2529EA02A8A1B9639EF00F7EF9CF061A9 ();
// 0x0000089A easyar.Optional`1<easyar.OutputFrame> easyar.OutputFrameBuffer_<>c::<peek>b__6_0(easyar.Detail_OptionalOfOutputFrame)
extern void U3CU3Ec_U3CpeekU3Eb__6_0_m2482D72EFC273079697355998B459A80952AD25E ();
// 0x0000089B System.Void easyar.FeedbackFrame_<>c::.cctor()
extern void U3CU3Ec__cctor_mEA580CFC164F679F2431C56D70513B99B1D7FF12 ();
// 0x0000089C System.Void easyar.FeedbackFrame_<>c::.ctor()
extern void U3CU3Ec__ctor_m43FB04867CAEE6B9A28E33271AABF0BE694EF244 ();
// 0x0000089D easyar.Detail_OptionalOfOutputFrame easyar.FeedbackFrame_<>c::<.ctor>b__3_0(easyar.Optional`1<easyar.OutputFrame>)
extern void U3CU3Ec_U3C_ctorU3Eb__3_0_m39F087607B43071342D73094A4F25243FD1D78FC ();
// 0x0000089E easyar.Optional`1<easyar.OutputFrame> easyar.FeedbackFrame_<>c::<previousOutputFrame>b__5_0(easyar.Detail_OptionalOfOutputFrame)
extern void U3CU3Ec_U3CpreviousOutputFrameU3Eb__5_0_m22C434C4CBC48FE964AAF40454244D27FE30FA92 ();
// 0x0000089F System.Void easyar.TargetInstance_<>c::.cctor()
extern void U3CU3Ec__cctor_mE718C3EE0D3317D1F474BEB0A8D66238590FE09A ();
// 0x000008A0 System.Void easyar.TargetInstance_<>c::.ctor()
extern void U3CU3Ec__ctor_mAE4C03CFF87E36E27A386A7B541F534EA32DE341 ();
// 0x000008A1 easyar.Optional`1<easyar.Target> easyar.TargetInstance_<>c::<target>b__5_0(easyar.Detail_OptionalOfTarget)
extern void U3CU3Ec_U3CtargetU3Eb__5_0_mFC15353608AF56E4282E90BB75618840B2DEAF36 ();
// 0x000008A2 System.Void easyar.EasyARSettings_TargetGizmoConfig_ImageTargetConfig::.ctor()
extern void ImageTargetConfig__ctor_mA48DE9D56844B4FEB85F759FDA5CB49AFEFE5150 ();
// 0x000008A3 System.Void easyar.EasyARSettings_TargetGizmoConfig_ObjectTargetConfig::.ctor()
extern void ObjectTargetConfig__ctor_mD2153C449CE0521ADC2532F7E1527CA9AD1A3F53 ();
// 0x000008A4 System.Void easyar.Detail_AutoRelease_<>c__DisplayClass2_0`1::.ctor()
// 0x000008A5 System.Void easyar.Detail_AutoRelease_<>c__DisplayClass2_0`1::<Add>b__0()
// 0x000008A6 System.Void easyar.Detail_FunctorOfVoid_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m0E179CC48A7D9461FB57C6C49BC34631E27C67BE ();
// 0x000008A7 System.Void easyar.Detail_FunctorOfVoid_FunctionDelegate::Invoke(System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m11CBC826064BEBD67681C596FA52D95F54AFC850 ();
// 0x000008A8 System.IAsyncResult easyar.Detail_FunctorOfVoid_FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m484E8C5B59358F376332D98933E6340C782E8641 ();
// 0x000008A9 System.Void easyar.Detail_FunctorOfVoid_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mB2C3B0F9862ABCF1AB98399684627227714AF69D ();
// 0x000008AA System.Void easyar.Detail_FunctorOfVoid_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m5A5B0EC452A61C7327BFF4A2A352C9C13810D5B8 ();
// 0x000008AB System.Void easyar.Detail_FunctorOfVoid_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mBE0534CAC55446933DDF052D1CB93F0176201D7F ();
// 0x000008AC System.IAsyncResult easyar.Detail_FunctorOfVoid_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m1338D0F085E1EB5723EB416241271319151526E2 ();
// 0x000008AD System.Void easyar.Detail_FunctorOfVoid_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m279DCA2EE80CB278C0891B5EABC1FDD932750DB3 ();
// 0x000008AE System.Void easyar.Detail_FunctorOfVoidFromOutputFrame_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_mB81B065745F94203D33D966D23B54EA58E50CF1D ();
// 0x000008AF System.Void easyar.Detail_FunctorOfVoidFromOutputFrame_FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m38D31CB633F36CEB5204B85250BB9E6658008BB0 ();
// 0x000008B0 System.IAsyncResult easyar.Detail_FunctorOfVoidFromOutputFrame_FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mB80BAD8AA6A2AE22015DF3F4BFFDD7DE542397C2 ();
// 0x000008B1 System.Void easyar.Detail_FunctorOfVoidFromOutputFrame_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m6429F7AFAFA1C55172F45530C687322977831D11 ();
// 0x000008B2 System.Void easyar.Detail_FunctorOfVoidFromOutputFrame_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m08C129CF2B86E1C321ADE34B126E261B210B62B1 ();
// 0x000008B3 System.Void easyar.Detail_FunctorOfVoidFromOutputFrame_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m190879BB23F2F6BD47FC00450B7E34B22D4CB5E0 ();
// 0x000008B4 System.IAsyncResult easyar.Detail_FunctorOfVoidFromOutputFrame_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mDDAA4C90E46B60A6563E26376A6457A33A5C258B ();
// 0x000008B5 System.Void easyar.Detail_FunctorOfVoidFromOutputFrame_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mA4259DE6CB9B77FF2E76F2D42174CBA5707A5279 ();
// 0x000008B6 System.Void easyar.Detail_FunctorOfVoidFromTargetAndBool_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m8B88DFF446D0523E149824170DB836CE2CDCE6C9 ();
// 0x000008B7 System.Void easyar.Detail_FunctorOfVoidFromTargetAndBool_FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.Boolean,System.IntPtr&)
extern void FunctionDelegate_Invoke_mE19162060CE2C99F97B91A1D61CB08A2E443255D ();
// 0x000008B8 System.IAsyncResult easyar.Detail_FunctorOfVoidFromTargetAndBool_FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.Boolean,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m72DA0B666E659B4BA178EB9127A14620208A8A9B ();
// 0x000008B9 System.Void easyar.Detail_FunctorOfVoidFromTargetAndBool_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m7D411AEA994EBADDDE84DE2648356BAEB34D1F4F ();
// 0x000008BA System.Void easyar.Detail_FunctorOfVoidFromTargetAndBool_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m8369965203E5A6A7C8C3512383D883AC8686703C ();
// 0x000008BB System.Void easyar.Detail_FunctorOfVoidFromTargetAndBool_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m9A7875247CE820E621B04568077BD6EA5A32CA87 ();
// 0x000008BC System.IAsyncResult easyar.Detail_FunctorOfVoidFromTargetAndBool_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m9077DA1C7BFB8D309DE306264C9534E5F4E2077F ();
// 0x000008BD System.Void easyar.Detail_FunctorOfVoidFromTargetAndBool_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mE0132BA9177E49E52578B6BB056FD1EE8414E078 ();
// 0x000008BE System.Void easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_mCCB741ECD1A40B11102E30BC641FC8E681A2A013 ();
// 0x000008BF System.Void easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m6215B3BF341C05ABDD631A917EE2A201D3BD0CCF ();
// 0x000008C0 System.IAsyncResult easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mD21391178ED9C6881DCFF37B877C0C43F5477B69 ();
// 0x000008C1 System.Void easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m93A413BE71100C35974218FB89C54E1C45D985C7 ();
// 0x000008C2 System.Void easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m279204A504638EC677B9FF7A3DB41F1B4D4980AF ();
// 0x000008C3 System.Void easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mE2853AD118B7D6D2E13330412EEE88116334F0A5 ();
// 0x000008C4 System.IAsyncResult easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m3D84E55BBD8AD1286AF553F29D1B2873D823D20E ();
// 0x000008C5 System.Void easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m8FDD30A791BED08E3D09A4CBBF99BF6F926DB621 ();
// 0x000008C6 System.Void easyar.Detail_FunctorOfVoidFromInputFrame_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_mEFF077E9452BDAC8A1C3D04C454F17B950C14237 ();
// 0x000008C7 System.Void easyar.Detail_FunctorOfVoidFromInputFrame_FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_mBA9F0AFCA18452B41B874B25C2A9A4406D5DED73 ();
// 0x000008C8 System.IAsyncResult easyar.Detail_FunctorOfVoidFromInputFrame_FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m3936D2245A8E2EBE3BC4B82936D0CDB3676DB829 ();
// 0x000008C9 System.Void easyar.Detail_FunctorOfVoidFromInputFrame_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mEB7EF6C6E5156143E98DD2CCFB09E29C68D295E7 ();
// 0x000008CA System.Void easyar.Detail_FunctorOfVoidFromInputFrame_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m703805691AFB9D84F4597D131028AEA9EF333971 ();
// 0x000008CB System.Void easyar.Detail_FunctorOfVoidFromInputFrame_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mBC3EF69437E9C30FDB28AC5B3FF7B103E085370A ();
// 0x000008CC System.IAsyncResult easyar.Detail_FunctorOfVoidFromInputFrame_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m8FB0AAF2A01AE3275B6A2C1E7443440FFA51BCA4 ();
// 0x000008CD System.Void easyar.Detail_FunctorOfVoidFromInputFrame_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m900A85690DDE2425F86EF336D0A3258B40A6B508 ();
// 0x000008CE System.Void easyar.Detail_FunctorOfVoidFromCameraState_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m369BCB2D345B3A7E4D6481372DDCFAE6A53752A8 ();
// 0x000008CF System.Void easyar.Detail_FunctorOfVoidFromCameraState_FunctionDelegate::Invoke(System.IntPtr,easyar.CameraState,System.IntPtr&)
extern void FunctionDelegate_Invoke_m460A0AE5767903BA2C5F868727713F8B0356C749 ();
// 0x000008D0 System.IAsyncResult easyar.Detail_FunctorOfVoidFromCameraState_FunctionDelegate::BeginInvoke(System.IntPtr,easyar.CameraState,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m541374B1EB1CB57D1B5F6F63EF88E4ACCC292D9F ();
// 0x000008D1 System.Void easyar.Detail_FunctorOfVoidFromCameraState_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m0438AC21F13BC93CBEB953DBDAC0F524D86B699B ();
// 0x000008D2 System.Void easyar.Detail_FunctorOfVoidFromCameraState_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m0B666DB88CEE6662098F92DFA75F3F750CC60181 ();
// 0x000008D3 System.Void easyar.Detail_FunctorOfVoidFromCameraState_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m31B4BEA54A3E885815BF52F38B9B61A495373B52 ();
// 0x000008D4 System.IAsyncResult easyar.Detail_FunctorOfVoidFromCameraState_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mC932D93F085A30E4CA5A569198B5114C040CC3CD ();
// 0x000008D5 System.Void easyar.Detail_FunctorOfVoidFromCameraState_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mEA623F205FF04E0624D21545D5CE03979C66BFE4 ();
// 0x000008D6 System.Void easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m34BA85E36C22FEF0ED0B32F61441F25F8C09BD26 ();
// 0x000008D7 System.Void easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_FunctionDelegate::Invoke(System.IntPtr,easyar.PermissionStatus,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m46975B50A2AD4847C29D33C0B60DA01A2B09A600 ();
// 0x000008D8 System.IAsyncResult easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_FunctionDelegate::BeginInvoke(System.IntPtr,easyar.PermissionStatus,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mB5EA0823D61F014ECF861CB919A4003CBDF917FA ();
// 0x000008D9 System.Void easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m61CB0BB7CA4938CBA2ADCA086694B74C77411B0C ();
// 0x000008DA System.Void easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m64EB239F30C53386CF3A31337B17990BBAAD10A7 ();
// 0x000008DB System.Void easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m4599B6415700B5306528E5CA67C4C9AEB86E8CF2 ();
// 0x000008DC System.IAsyncResult easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mF9BE803C6F2D278B9932CA2ADCA18427C4B9C0DB ();
// 0x000008DD System.Void easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m0E2EB382E7E15D4931C8775B290C54892614BA23 ();
// 0x000008DE System.Void easyar.Detail_FunctorOfVoidFromLogLevelAndString_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_mA73E1C9EADE192B7538EB09846040F6200AD582D ();
// 0x000008DF System.Void easyar.Detail_FunctorOfVoidFromLogLevelAndString_FunctionDelegate::Invoke(System.IntPtr,easyar.LogLevel,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m71DF6B81BA3C0D4686531C33BE5BA9A0B43CFA29 ();
// 0x000008E0 System.IAsyncResult easyar.Detail_FunctorOfVoidFromLogLevelAndString_FunctionDelegate::BeginInvoke(System.IntPtr,easyar.LogLevel,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m0597D0D11C2ABCDFBC95000854D8716C15AC9E3A ();
// 0x000008E1 System.Void easyar.Detail_FunctorOfVoidFromLogLevelAndString_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mAC386C0F12F8C0286A4EE2342A6BBDC4C9C18542 ();
// 0x000008E2 System.Void easyar.Detail_FunctorOfVoidFromLogLevelAndString_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mD3302A9A25E881C692D88216CD1F7FF03237BB73 ();
// 0x000008E3 System.Void easyar.Detail_FunctorOfVoidFromLogLevelAndString_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m6457C78F3F6004DD3607F062D3A7991FBD8E8F5B ();
// 0x000008E4 System.IAsyncResult easyar.Detail_FunctorOfVoidFromLogLevelAndString_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mFC710DD523019556ECFCF78E6B6DB523161D0FD5 ();
// 0x000008E5 System.Void easyar.Detail_FunctorOfVoidFromLogLevelAndString_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mAE40236DC33186943BB316C8183BF85754CA1F15 ();
// 0x000008E6 System.Void easyar.Detail_FunctorOfVoidFromRecordStatusAndString_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m962110774C99D29F30B2A376ABA22CCE8D085FBD ();
// 0x000008E7 System.Void easyar.Detail_FunctorOfVoidFromRecordStatusAndString_FunctionDelegate::Invoke(System.IntPtr,easyar.RecordStatus,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m96743D50AC11AD80A5BDF8A85C47F8FB5AB44E49 ();
// 0x000008E8 System.IAsyncResult easyar.Detail_FunctorOfVoidFromRecordStatusAndString_FunctionDelegate::BeginInvoke(System.IntPtr,easyar.RecordStatus,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m2805BAC0F418BBA908FCE2C75E8DB8C59935CEC5 ();
// 0x000008E9 System.Void easyar.Detail_FunctorOfVoidFromRecordStatusAndString_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mED2A5B7A393703948C70D2BD6F643A83721A35DB ();
// 0x000008EA System.Void easyar.Detail_FunctorOfVoidFromRecordStatusAndString_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m217130F032ED0FC02AA0ED9FC829E6F4120FB264 ();
// 0x000008EB System.Void easyar.Detail_FunctorOfVoidFromRecordStatusAndString_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mD1EE30263E6A7E4151C77AF4D9B005A63072DC5E ();
// 0x000008EC System.IAsyncResult easyar.Detail_FunctorOfVoidFromRecordStatusAndString_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mD6D9578B0D45CA16B98B1C800025431929AEF4B7 ();
// 0x000008ED System.Void easyar.Detail_FunctorOfVoidFromRecordStatusAndString_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mCC7D09681D6EF9EB4B2A52331A6BD2424A3757D6 ();
// 0x000008EE System.Void easyar.Detail_FunctorOfVoidFromBool_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m953639851CD109DE9833FDB02F3C9C8D926A523B ();
// 0x000008EF System.Void easyar.Detail_FunctorOfVoidFromBool_FunctionDelegate::Invoke(System.IntPtr,System.Boolean,System.IntPtr&)
extern void FunctionDelegate_Invoke_m3E0D7D2E307F4DC0D987C3ABDE4F472B2C087CA0 ();
// 0x000008F0 System.IAsyncResult easyar.Detail_FunctorOfVoidFromBool_FunctionDelegate::BeginInvoke(System.IntPtr,System.Boolean,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m62930ED2301620A2EC2193A628337AA7BD9E6DDB ();
// 0x000008F1 System.Void easyar.Detail_FunctorOfVoidFromBool_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mB196424F97E936ADE48868828E0189C2DBE8EA7E ();
// 0x000008F2 System.Void easyar.Detail_FunctorOfVoidFromBool_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mF9C5D467A494890326241ECE09E41F10FC448E25 ();
// 0x000008F3 System.Void easyar.Detail_FunctorOfVoidFromBool_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m4A095EC1D34F39204D55C5071CA22F16042225A6 ();
// 0x000008F4 System.IAsyncResult easyar.Detail_FunctorOfVoidFromBool_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mE08FD9953A2A7E73F824F5BDC692FD213E47139C ();
// 0x000008F5 System.Void easyar.Detail_FunctorOfVoidFromBool_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m989C6C497353C4640860922F848174624CC6C521 ();
// 0x000008F6 System.Void easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m1AAC4B3448BF523974B135FDBA507FE57F3EAA5E ();
// 0x000008F7 System.Void easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_FunctionDelegate::Invoke(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m8B7840C7616102C92E1F636016B9555FB4572A76 ();
// 0x000008F8 System.IAsyncResult easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_FunctionDelegate::BeginInvoke(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m6E2F466FA5FED38F44E7DDA67389D5AF5E9B9364 ();
// 0x000008F9 System.Void easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m97CE29629D7CE8DEA1E4BED43BC506C3CD6DD45B ();
// 0x000008FA System.Void easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mCFBACA0F014A85733CDC536881A374D66C15E014 ();
// 0x000008FB System.Void easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mD7401B99FC044B5A5248C3D4C5A4B833032943C3 ();
// 0x000008FC System.IAsyncResult easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m04DE88BE65985E3F8AE8BCB091C202838788D2A7 ();
// 0x000008FD System.Void easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m980A750F8B2C42A85DF9FB740BE0F7AEF616DA8B ();
// 0x000008FE System.Void easyar.Detail_FunctorOfVoidFromBoolAndString_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m58EA7F64246E9D934EDD585032AE43CFD185BE16 ();
// 0x000008FF System.Void easyar.Detail_FunctorOfVoidFromBoolAndString_FunctionDelegate::Invoke(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_mE72D7F213BE1E727C5047288263D189822C3C82A ();
// 0x00000900 System.IAsyncResult easyar.Detail_FunctorOfVoidFromBoolAndString_FunctionDelegate::BeginInvoke(System.IntPtr,System.Boolean,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mD6BD38D154115E190CDE7F903AE2F3F77DD66335 ();
// 0x00000901 System.Void easyar.Detail_FunctorOfVoidFromBoolAndString_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_m897DBF59F58D3E555BB77FD1230E9A7FCB256420 ();
// 0x00000902 System.Void easyar.Detail_FunctorOfVoidFromBoolAndString_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m22D84636E41362B44A6D7DF5E2F55C7CB2E1CC18 ();
// 0x00000903 System.Void easyar.Detail_FunctorOfVoidFromBoolAndString_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m350FA9D187D9C0A8A83ACCFC3782AA43B49A2D9D ();
// 0x00000904 System.IAsyncResult easyar.Detail_FunctorOfVoidFromBoolAndString_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m3A394D12A05CFBA8B94739939499552195719F8D ();
// 0x00000905 System.Void easyar.Detail_FunctorOfVoidFromBoolAndString_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mD56D504AA16C004D7757538E631E60165B8BED96 ();
// 0x00000906 System.Void easyar.Detail_FunctorOfVoidFromVideoStatus_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_mA14C9DE827C2DB1C26EAE2276BB086B68C8DCFC2 ();
// 0x00000907 System.Void easyar.Detail_FunctorOfVoidFromVideoStatus_FunctionDelegate::Invoke(System.IntPtr,easyar.VideoStatus,System.IntPtr&)
extern void FunctionDelegate_Invoke_m34F44155B50DB126560343A5EBE5CB108C856F8C ();
// 0x00000908 System.IAsyncResult easyar.Detail_FunctorOfVoidFromVideoStatus_FunctionDelegate::BeginInvoke(System.IntPtr,easyar.VideoStatus,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m361C882225D7D001B497B6A53C72C40320B4C774 ();
// 0x00000909 System.Void easyar.Detail_FunctorOfVoidFromVideoStatus_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mA2D391F2CA9D21522AEFD2BE83ECC65C41E4E763 ();
// 0x0000090A System.Void easyar.Detail_FunctorOfVoidFromVideoStatus_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_mF04E1863AEDA755055052A13C2CDC1A228FF22B3 ();
// 0x0000090B System.Void easyar.Detail_FunctorOfVoidFromVideoStatus_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m8219C68504316A20EFE28A87D28F050F6AC094B6 ();
// 0x0000090C System.IAsyncResult easyar.Detail_FunctorOfVoidFromVideoStatus_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_mF53F10F9891F7FFCA2CB6E50B0D0D2F2D15C3D34 ();
// 0x0000090D System.Void easyar.Detail_FunctorOfVoidFromVideoStatus_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_mA11950B6F44360E08F38257F709F752F99DAA7BB ();
// 0x0000090E System.Void easyar.Detail_FunctorOfVoidFromFeedbackFrame_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m8001F661E65F5F972A7696DA603EA430473D8441 ();
// 0x0000090F System.Void easyar.Detail_FunctorOfVoidFromFeedbackFrame_FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&)
extern void FunctionDelegate_Invoke_m5CF011F127F04DF9C8B274D29AE591829DAA2AFF ();
// 0x00000910 System.IAsyncResult easyar.Detail_FunctorOfVoidFromFeedbackFrame_FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_m80BCCF53B42AFE698A463B7D92EC9DA6007A86E9 ();
// 0x00000911 System.Void easyar.Detail_FunctorOfVoidFromFeedbackFrame_FunctionDelegate::EndInvoke(System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mC247B7CBCE1356376F2E6BDBED636B46AF21A01C ();
// 0x00000912 System.Void easyar.Detail_FunctorOfVoidFromFeedbackFrame_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m220E794C85DCF1D6CE09F57EA9FD245D6FD689C8 ();
// 0x00000913 System.Void easyar.Detail_FunctorOfVoidFromFeedbackFrame_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_m0A4BF8BB80A27181C593682E11CD41D85E7B6A93 ();
// 0x00000914 System.IAsyncResult easyar.Detail_FunctorOfVoidFromFeedbackFrame_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m209DC89E07980BD36BB26CC30C2B28E8E2302A3F ();
// 0x00000915 System.Void easyar.Detail_FunctorOfVoidFromFeedbackFrame_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m8989D1652133EE1D8DB3F4803EB6D1B3924DA73C ();
// 0x00000916 System.Void easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_FunctionDelegate::.ctor(System.Object,System.IntPtr)
extern void FunctionDelegate__ctor_m3BDA778FE9964EE66A5906214062377E6EA81389 ();
// 0x00000917 System.Void easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_FunctionDelegate::Invoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.IntPtr&)
extern void FunctionDelegate_Invoke_m1FAC7DD6CCBB7DDE856B367304A1C0EBE47FD7EA ();
// 0x00000918 System.IAsyncResult easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_FunctionDelegate::BeginInvoke(System.IntPtr,System.IntPtr,System.IntPtr&,System.IntPtr&,System.AsyncCallback,System.Object)
extern void FunctionDelegate_BeginInvoke_mF46C7DBF268493C3827653376AEF543E481DE3AC ();
// 0x00000919 System.Void easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_FunctionDelegate::EndInvoke(System.IntPtr&,System.IntPtr&,System.IAsyncResult)
extern void FunctionDelegate_EndInvoke_mEDBCF561F392A390B2E7347016355838B43AF473 ();
// 0x0000091A System.Void easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern void DestroyDelegate__ctor_m302C7A97E6F9EAA0C80F2C7C7BFF78FD35D94C74 ();
// 0x0000091B System.Void easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_DestroyDelegate::Invoke(System.IntPtr)
extern void DestroyDelegate_Invoke_mAC57037483DA9B5E7D7E405305E5A07288325D21 ();
// 0x0000091C System.IAsyncResult easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_DestroyDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
extern void DestroyDelegate_BeginInvoke_m8C00228FE8AB67D1B89B30C531A4024392C2F012 ();
// 0x0000091D System.Void easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_DestroyDelegate::EndInvoke(System.IAsyncResult)
extern void DestroyDelegate_EndInvoke_m626FF344FDB87816A01FFE926B91D72E0DFB5057 ();
static Il2CppMethodPointer s_methodPointers[2333] = 
{
	ARAssembly_Finalize_m27C7C551F39F7F96F8BB793B150FF22AE4413C8D,
	ARAssembly_get_Ready_m8C9799EC0E9B4354D664AD2F88064C79E958DFA2,
	ARAssembly_set_Ready_m15799EDE7CED656CF6367A58414625BA680260FD,
	ARAssembly_get_RequireWorldCenter_m7226701A762B047BEABDB0B31BC3EBA2D760AF32,
	ARAssembly_set_RequireWorldCenter_mC9855DEC2B6BE8A0E43B2AE522E8A5D0428A9F4D,
	ARAssembly_get_OutputFrame_mBA7CDF96D6CC14BBD64B7CB14B85B4E638CE11C2,
	ARAssembly_Dispose_m4C2BA0A7290CF733E03BB15933F65D346040EB44,
	ARAssembly_Assemble_m431B3F53B1637AF82FD8AD36E5FC033BEAD065CF,
	ARAssembly_Break_mD8D9F0005804709713CE21C907056E1F76DE6E65,
	ARAssembly_Pause_m2D20F95AAA8F57E622E57B3A1DA222DC3EE5A6C7,
	ARAssembly_Resume_m62A623CB13D6DD66B017546F8E78527C6D673BC0,
	ARAssembly_ResetBufferCapacity_m597167B49A27D67D7DC8051644A5B19CF0DA665E,
	ARAssembly_GetBufferRequirement_mF7008A6D843E543D9597F1F001F3CC16691D86BD,
	NULL,
	ARAssembly_Assemble_m82AAD532937E2BD5AEE325EF66BFA09A8F8ED4F9,
	ARAssembly_DisposeAll_m1F2B46EF8B918A7B26F4D39E6FA7B423EBC7F85F,
	ARAssembly__ctor_m04474AAF85E12587B9FA18826FBAD982756B0E9A,
	ARSession_add_FrameChange_m8D60BCDEF23E509A54A3D7D4B3C7E6D9A39EBBCC,
	ARSession_remove_FrameChange_m0C765DD4CB383AD433FBEFF840D9970E01104C77,
	ARSession_add_FrameUpdate_m2E435330EEE5150F1301DC39793398FD8E41E03D,
	ARSession_remove_FrameUpdate_m7DC7A1685EB66C2230442D0E1D23C11B0BFEF357,
	ARSession_add_WorldRootChanged_mC6A35B002BC4D65C61D7318A60DC3112965EFABE,
	ARSession_remove_WorldRootChanged_m5C3035DAE9668ABFF697149D3DC5439A3A6127CB,
	ARSession_get_FrameCameraParameters_m3668E89DA64EC7B23651FC83C4A1B7A809AA6FC1,
	ARSession_set_FrameCameraParameters_m6EAAD090786BB4FF7F6F743EB16829B87D7CDA45,
	ARSession_Start_m22ADF52E8B9BD41EAE623CE6A725C2DF4C9A2ECA,
	ARSession_Update_mA2E45C5388B0ADB939119E31E90105F64F9045E1,
	ARSession_OnDestroy_mF932EDA3F05AE03929B10F9B5FF284E3C3B595A3,
	ARSession_OnFrameUpdate_mF82C87831C2D956ABA9E875EC7EEBE51ED6668FD,
	ARSession_OnEmptyFrame_m8E04D272168C40E48A3FBF2B0BC9D29FCADDB9D1,
	ARSession_DispatchResults_m71C9C5A43D74397ED101660330F02F25E322BD16,
	ARSession__ctor_mB3EF86EE2F2FE04DABFC3A36966AD63AA9A4BFEB,
	CameraImageRenderer_add_OnFrameRenderUpdate_m83F3F1454E5099F312BCE87E34C0654C65C548CA,
	CameraImageRenderer_remove_OnFrameRenderUpdate_mE9612C3D46FEFA37C1C16164CF9DE4F6E5BFDD82,
	CameraImageRenderer_add_TargetTextureChange_mAF629152CBC88480D565C870CAC35DB695AF4B89,
	CameraImageRenderer_remove_TargetTextureChange_mDB96FEFF9078BDE23131638B1E05EDEE284910D8,
	CameraImageRenderer_Awake_m8D3D6A3CF00CED3733847412E8C301B7DD65E717,
	CameraImageRenderer_OnEnable_m510F923A31E731C534492C9D116BDB67CAE9BC7A,
	CameraImageRenderer_OnDisable_m300A932299E580B7BEEA38490586F0985880F9E1,
	CameraImageRenderer_OnDestroy_m186C98B73778E7417D88D06E9051CEF1561F6A6B,
	CameraImageRenderer_RequestTargetTexture_m845ED8ABBC6DB0E7697D48C4B9A7DE6F58787B27,
	CameraImageRenderer_DropTargetTexture_mCB5E808B36EEA20489A09FD5667CC4D3E8ABEB3F,
	CameraImageRenderer_OnAssemble_mFB9D6DD2C29A6B45501315693C15EA85F7A19B7A,
	CameraImageRenderer_SetHFilp_mD87E7385E83B902CEAFFCF2E7D7606E499662AD7,
	CameraImageRenderer_OnFrameChange_m525EF4BEFFAD33C632828574A5B3EB4FFDEC02DF,
	CameraImageRenderer_OnFrameUpdate_mCCB07378126D84367EC907B2C7432723970728C5,
	CameraImageRenderer_UpdateCommandBuffer_mB0E7353F02E985845F095917C2EB5A6FFC9AE39A,
	CameraImageRenderer_RemoveCommandBuffer_mCF1A6503855B0DDB7EEAC375EA60777666DF9925,
	CameraImageRenderer__ctor_m1BBD4BA70BF67F9008C552860CD826FE61DB1255,
	CameraRenderEvent_add_PreRender_mA822D872CEDB2FD1605FF0CB594DEAE86B6D6F36,
	CameraRenderEvent_remove_PreRender_m1EE3518D666D18CCB9F844D4E10696EDB67709B8,
	CameraRenderEvent_add_PostRender_mC4A7302F88831C22B863A9B6C7468D07431E13FB,
	CameraRenderEvent_remove_PostRender_mD2F3AA2F168A6E2EC9B2490739373B724AC4B0CF,
	CameraRenderEvent_OnPreRender_m6249E885292A55037FB0243E6BA0C5A8CD525F54,
	CameraRenderEvent_OnPostRender_mAD0B8A18045F7836636366D5529B48EB8D523580,
	CameraRenderEvent__ctor_m1AE64D6E53AB0B3E849950D9620BE7BE80A2E072,
	CameraSource_get_BufferCapacity_m9E552F2724891337B1083BB2DFDF4131256BFF30,
	CameraSource_set_BufferCapacity_mC905F6435B3E3F2E298D2EA3BFAE8F2987B1D5A7,
	CameraSource_Start_mA48F5AB0EFFADAF4383D945B5002E4BC76E215B3,
	CameraSource_OnDestroy_m6F7FF1B9645E5117FAC584DA6557C768E54B4E8C,
	NULL,
	NULL,
	CameraSource__ctor_m7F16B069980B861E6F9984C2859BB03B7A1C2A99,
	CloudRecognizerFrameFilter_get_CloudRecognizer_m87505BF12991E4A1DD2AC9AA9C6C6BDC3C5A1909,
	CloudRecognizerFrameFilter_set_CloudRecognizer_mFCDBC9CE81ED6FDC283D66A0D9BBDFB42DE811B1,
	CloudRecognizerFrameFilter_Start_m927FA2A9088C6BAC1068257717603A5DC3B97BE0,
	CloudRecognizerFrameFilter_Resolve_m0F08C9056A5C272338FDFB7E7164B0918C3845C6,
	CloudRecognizerFrameFilter_OnDestroy_m17BA44B25E328E2E560CF683FD854A433C626D80,
	CloudRecognizerFrameFilter_NotifyEmptyConfig_m3E3D3D21E09C63DA3E0AEE7241D332B5CB6BF902,
	CloudRecognizerFrameFilter_NotifyEmptyPrivateConfig_m4E4D498A3CC837F87F83551097A319B413FF139C,
	CloudRecognizerFrameFilter__ctor_m29311C472CCB284DD3838F0808AFD64CC33AD570,
	DenseSpatialMapBlockController_get_Info_mCAE7B244716DDD5A7F414DEC93D38255DD156A70,
	DenseSpatialMapBlockController_set_Info_mABD08135346B8311E884866C0DCD51BDF4D4B5A4,
	DenseSpatialMapBlockController_Awake_mFAD5245B5620127CDBD7E10A23D8C065385252C6,
	DenseSpatialMapBlockController_OnDestroy_mCCBC3E2379E7EF5A2F45702F150CFB79F301DA2B,
	DenseSpatialMapBlockController_UpdateData_mD9F771F64264D5C908081C1BC0BC91620B4D757D,
	DenseSpatialMapBlockController_UpdateMesh_m8492B42C00F84F12431837F636AF9521DEB5FB69,
	DenseSpatialMapBlockController_CopyMeshData_m9C31922C4A8A07EC9A4C1735D03D3BBF351B7C27,
	DenseSpatialMapBlockController__ctor_m3CB2387BEBAF438477349306D12BA72C59E62536,
	DenseSpatialMapBuilderFrameFilter_get_Builder_mF02BE9B80646C6116608FCE0E05319E7B24DB5E8,
	DenseSpatialMapBuilderFrameFilter_set_Builder_m2D785C31E7F0C39FEDE74DC7ED1A57C430CC1D7D,
	DenseSpatialMapBuilderFrameFilter_add_MapCreate_mE689BD3F61B0E43512D7F37C966997D3738BF45B,
	DenseSpatialMapBuilderFrameFilter_remove_MapCreate_mCFE51332A6A791FE17E8E9BABCEB8F9FDF49E482,
	DenseSpatialMapBuilderFrameFilter_add_MapUpdate_m096C661C5970C927F89104173341B4865875A0FC,
	DenseSpatialMapBuilderFrameFilter_remove_MapUpdate_m33D3992DECD3BF13AEB413CE97C89842D8C906EF,
	DenseSpatialMapBuilderFrameFilter_get_BufferRequirement_m2B30546D9D53319D4A2D6AD06C5B2778C48E9DE7,
	DenseSpatialMapBuilderFrameFilter_get_TrackingStatus_mC83FBD2C2E07474C3A4C320F89E33D0EA7B8AD24,
	DenseSpatialMapBuilderFrameFilter_set_TrackingStatus_m9D0217F76DF14D59E2A0A9D8B88044634D290E0F,
	DenseSpatialMapBuilderFrameFilter_get_RenderMesh_m1020A63260241EBBC026709E0BB87B3603FCF3C3,
	DenseSpatialMapBuilderFrameFilter_set_RenderMesh_mB7928B78D1045D5396F5AACA2C0AED02EF2292EB,
	DenseSpatialMapBuilderFrameFilter_get_MeshColor_m7BAFA0F681CBA2FE618897FECF449B038F70254E,
	DenseSpatialMapBuilderFrameFilter_set_MeshColor_mA75249EC525C8820FB0F2E1F54306C4452BFA3D6,
	DenseSpatialMapBuilderFrameFilter_get_MeshBlocks_m2314FFCA377A6C9F48AAA7099A530E287F7ACCA4,
	DenseSpatialMapBuilderFrameFilter_Awake_mD0667AEE537C68C8581CC8387B45AAFBEA695A21,
	DenseSpatialMapBuilderFrameFilter_OnEnable_mD429BAAC4D28AD8E4743E1F8388D7274D68313BE,
	DenseSpatialMapBuilderFrameFilter_Start_m306FA3E1AED84BFE90E59C328C0975BBDF52E550,
	DenseSpatialMapBuilderFrameFilter_Update_m5FFB8C97B34E0F3A1F9F9C8DE0390A89BF891FA5,
	DenseSpatialMapBuilderFrameFilter_OnDisable_m106ECD9F18A8E21DC253AB4927EF927C94D9E4CB,
	DenseSpatialMapBuilderFrameFilter_OnDestroy_m56E8749B425BF8A04BAC7516D4792B6899C329EC,
	DenseSpatialMapBuilderFrameFilter_InputFrameSink_m110512ECC917ABEB5E213CDF5A4FFD88FD3511AE,
	DenseSpatialMapBuilderFrameFilter_OnTracking_m40AB0A3B409710B1A6F8ECCFD8A96031108829A6,
	DenseSpatialMapBuilderFrameFilter_OnAssemble_m9D3815504DEE342A0DA86EAFBDCD95397ACBA68C,
	DenseSpatialMapBuilderFrameFilter__ctor_m0F7AA39A84FE39B4C1DF1105233002D97BB6D461,
	DenseSpatialMapBuilderFrameFilter_U3COnAssembleU3Eb__41_0_mFF7C3081E15D05E38B031EF79F89EF3079A776E7,
	DenseSpatialMapDepthRenderer_get_RenderDepthCamera_m382384798191545BA128C42303D6C377042AFD22,
	DenseSpatialMapDepthRenderer_set_RenderDepthCamera_mCC59B2C5575C29E0A3744E2DFD2D3836C0FA0393,
	DenseSpatialMapDepthRenderer_get_MapMeshMaterial_m81069824EE0E3268CA3E5C1B3D5CC0271ABCD861,
	DenseSpatialMapDepthRenderer_set_MapMeshMaterial_mC114EAFF67F95E23CD861C07B22314F66AB608CA,
	DenseSpatialMapDepthRenderer_LateUpdate_m13D1B84AAF94857676931AF070191BBCD2DD127C,
	DenseSpatialMapDepthRenderer_OnDestroy_m7F89328203B1DE67DE23EE5DE4D8AF351676D323,
	DenseSpatialMapDepthRenderer__ctor_m0D74AAE79950B9F0720119D59CF1B2D6FE830774,
	EasyARController_get_Instance_m250289045EB88AE3CA24A822FD6C39606C44BFCE,
	EasyARController_set_Instance_mC6775667DCB985AD0D7EA34A84BD80F402A435C5,
	EasyARController_get_Initialized_mB1836B5C067B9D5DBBBA82A85277FFA9EDCF893D,
	EasyARController_set_Initialized_m8FAA0F85A00F298300A9D0EF2EB569806B44A778,
	EasyARController_get_ARCoreLoadFailed_m38C47AAB7B90EB9C769AAA78CFC0D7BFAA481DD3,
	EasyARController_set_ARCoreLoadFailed_m697E427016E226545C48DA4C1907F3C65001591A,
	EasyARController_get_Scheduler_m8DAE893DFBB226680525DFDBC5CDFE5FAFD26217,
	EasyARController_set_Scheduler_mC82E25FDF3EB0F562D02EFE166E035E51CC70BD5,
	EasyARController_get_Settings_mBC6A0D8058558E6EEFAF0F1E001F40F1441E7847,
	EasyARController_get_settingsPath_m4D0AFBF86A17FDC40C6F64CA3314A1B63F1E1C2F,
	EasyARController_get_Worker_m7229F90382A04CD8E5479B54308CA1CA99262583,
	EasyARController_set_Worker_m23A5547F9BD71C18E7F55A63D5E4BD499102F1BE,
	EasyARController_get_Display_mDB1CBCB5D424EE49C95A695A2C65AF0332AC847F,
	EasyARController_set_Display_mE929E0FBEB29869D3184CC31AFC437C85324E5F8,
	EasyARController_GlobalInitialization_m0F6F13E8AF1C9EBA7F681BDC9678572DFB6725C8,
	EasyARController_Awake_m9DE44F914646D283C640ABF641CDE0FBE2C57DE0,
	EasyARController_Update_mF21BEAF6AD60A2371E1B72EC76CBAFC8050DCE1C,
	EasyARController_OnApplicationPause_m6FBE6857CD7232F0ABFAF952AA059DBCA7872D91,
	EasyARController_OnDestroy_m6BE5CB6A91EF6B9EB3F8CFD154C76D3E38504000,
	EasyARController_ShowErrorMessage_mF4B29F5C418DABF2441D267164FBD579067C1E61,
	EasyARController__ctor_mFBE2460C0F0475CCEC34C6A62CB0401F161DB384,
	EasyARVersion__ctor_m9084F0AD26EEF9D640CE351013E5A0E6F711D4EA,
	NULL,
	FrameFilter_OnAssemble_m73EDFBEDA054D1CCE44E8DB8C761FC66439BF12F,
	FrameFilter_SetHFlip_mA54A462BF41F4F24806BBA573460454D302BA090,
	FrameFilter_OnHFlipChange_m66AE1C45BD65CD476D463ED2EE8CDA581967D8C0,
	FrameFilter__ctor_m760903724A2F512FAF1F5979781D96203C554D05,
	NULL,
	FrameSource_OnEnable_m557927C637CAF8F3E3E9C6B4A6C08BF41CB9AE38,
	FrameSource_OnDisable_m1A333D5EDF00260C87129C5758E40BEE872459E4,
	FrameSource_Connect_m79FC57A96372B158B6F45B7970775173BDFE74D0,
	FrameSource_OnAssemble_mF8D2CC6BC4C3B77404EEC5AA9888079393559FD7,
	FrameSource__ctor_m77B174515107ACA329FC37974C3BBCC1673315C6,
	ImageTargetController_get_Target_m8B0AC3E30799AD0EE97C9CFA0E8FE70F5800C556,
	ImageTargetController_set_Target_m8DCE0EDEC9A4F5DDD34835FDB2617CD61DF4EB89,
	ImageTargetController_add_TargetAvailable_m61FCAA07FC0D7F491EDAFFD274CD6E6D9BE3BACC,
	ImageTargetController_remove_TargetAvailable_mB7FAD81DB044E3B09A4BC336CA2FE6057D99C3DC,
	ImageTargetController_add_TargetLoad_m3F9617B73200D95FD67B78356EFA3B680C6BB585,
	ImageTargetController_remove_TargetLoad_m6B194F9A77DC003894517D8D6067546347979FF8,
	ImageTargetController_add_TargetUnload_m65371B11BF29BCE1015CAE0877A886200FEB3B84,
	ImageTargetController_remove_TargetUnload_m66574460CECBB9212D1574A62FD1C658B13D87C2,
	ImageTargetController_get_Tracker_m0668F827BD43B2D9B2B701118103712764815D66,
	ImageTargetController_set_Tracker_mEA5F5C869C0120911C86D5768A3F440C6430E479,
	ImageTargetController_get_Size_mB815DC19252C3209093C310E5D4A5A5529C132DD,
	ImageTargetController_set_Size_m160FBAC46D3BDB4A1F7D58D55BD5C9D62CCE4732,
	ImageTargetController_Start_m5C8D158C124FD40B8488207AC5148AFDD73955F5,
	ImageTargetController_Update_mCE4F6B2328593E2B974B6C83D1BA0943375C0CF0,
	ImageTargetController_OnDestroy_mD97CB9C3275DCF98E03BED694AA59F7344FE23A8,
	ImageTargetController_OnTracking_m5A44561EE348C461B5044C79C335946D3DF1EB66,
	ImageTargetController_LoadImageFile_m294CB3FB597D036E54ED90808402B4ED6097B7C1,
	ImageTargetController_LoadTargetDataFile_m9C798903F31946B61579686C5FCA7C8980352604,
	ImageTargetController_LoadTarget_m8A0FC0C24CD77BED18DD3A89043A5254B27BB147,
	ImageTargetController_LoadImageBuffer_m2DFFFA1A96BA49257DAFC8B59B5C014D9B704400,
	ImageTargetController_LoadTargetDataBuffer_mC6F727AD7044E17F49CA921AF4B80E25E923C345,
	ImageTargetController_UpdateTargetInTracker_m6260EF9F239468D58D8BBD5F501573C7A22CD8B4,
	ImageTargetController_UpdateScale_mA1F3E7D8276DD63CCC1B9CF3F4E10388141A639F,
	ImageTargetController_CheckScale_m698A32A83BA6365B15141E339CDE7667E0049962,
	ImageTargetController__ctor_mE615AE5DA14062F27BAC6FAC5541D57E9FBCFB78,
	ImageTargetController_U3CLoadTargetDataFileU3Eb__35_0_mDF99BD172638F248D5CD38A57F30BE818F47BB4F,
	ImageTargetController_U3CUpdateTargetInTrackerU3Eb__39_0_m5834DF12090C7961854AAE207B97F60371434E61,
	ImageTrackerFrameFilter_get_Tracker_m3D2FFF6BAF9AD9F9E74A0FC049C4980835DF8B0B,
	ImageTrackerFrameFilter_set_Tracker_m23AA28650516A34FBF421033390014003434F5C2,
	ImageTrackerFrameFilter_add_TargetLoad_mE1ACF6D43360BAB2B79B9C573D25E8E81E8BEE8A,
	ImageTrackerFrameFilter_remove_TargetLoad_m81B83DA297977CB14E0392BD888C5E14BE5896AA,
	ImageTrackerFrameFilter_add_TargetUnload_m98A2AD4ECB81B3AE06C261FA17059C430BCF7BD9,
	ImageTrackerFrameFilter_remove_TargetUnload_m483C31750EFE2C8AA216657F002E36491B5EEC81,
	ImageTrackerFrameFilter_add_SimultaneousNumChanged_m925A65B883FEA8F1AD02F795C484CB8DC0CA29B5,
	ImageTrackerFrameFilter_remove_SimultaneousNumChanged_mF091A3A63D08F4864CD1FA8F393965C6522F618D,
	ImageTrackerFrameFilter_get_BufferRequirement_m6C19A8699AECB0599863810204006751C08EEE6C,
	ImageTrackerFrameFilter_get_SimultaneousNum_mFA8A36DE26A3F992D6FA776091E85FB8D652702C,
	ImageTrackerFrameFilter_set_SimultaneousNum_m524A9078C55FFB3E637E37F75D04806A409DA8DB,
	ImageTrackerFrameFilter_get_TargetControllers_m4F818F86065E7B8A6E910025C3C12273F5EEC706,
	ImageTrackerFrameFilter_set_TargetControllers_m05B8FDBE41055195DA550DDFDBB959284BC7B6ED,
	ImageTrackerFrameFilter_Awake_m237BF301F632D9BD47D52D349FD6455EFB5D5BC1,
	ImageTrackerFrameFilter_OnEnable_mE17BAB20E63D5091F079D653F9DFA750280482D0,
	ImageTrackerFrameFilter_Start_m83DA9C4BC17A0FF4F298F75BD1AC5B3C3778FB4A,
	ImageTrackerFrameFilter_OnDisable_m29948FDCADC10B086CDFBD08945BC4656AB734E3,
	ImageTrackerFrameFilter_OnDestroy_m633CE8B036F8C43C0F011091FF690E6197F47178,
	ImageTrackerFrameFilter_LoadTarget_mFC0973031EF4D8E865EC3E77BF1388A4CFB6CEEA,
	ImageTrackerFrameFilter_UnloadTarget_m4C415902AFFC709EE07152AFA5E9B91808B8D03C,
	ImageTrackerFrameFilter_FeedbackFrameSink_m51AE4F48886529C19205FA9C869E718B2AA3A553,
	ImageTrackerFrameFilter_OutputFrameSource_mF6470630AC38434F0F125CDD969B3A79575ED48D,
	ImageTrackerFrameFilter_OnAssemble_mAD25A1052206E0E532A308FAEA998CBBF327B239,
	ImageTrackerFrameFilter_OnResult_mED52ED5D8C18C3A152A5A27EF726B80E8CC711A9,
	ImageTrackerFrameFilter_LoadImageTarget_m980E0FAB923C9F26E8F7B1D19A2B06A3C6DDDBD8,
	ImageTrackerFrameFilter_UnloadImageTarget_m682EF75C6ECE8E903CC1A58E1170420465FC0F76,
	ImageTrackerFrameFilter_OnHFlipChange_m0B8CCE8F4AD560F4317AF8972DBBC7B98A91F9A4,
	ImageTrackerFrameFilter_TryGetTargetController_m2E97A328A1190564B5129699A8C6FBD8CF3827A7,
	ImageTrackerFrameFilter__ctor_m0C5E64C741355925257847439C2E17881B365C82,
	ObjectTargetController_get_Target_m16CE294BE63B6194BE261E3F617DD90041AC7470,
	ObjectTargetController_set_Target_mF53E78FA16B7FF26D5DF60C3B930EEDF13816660,
	ObjectTargetController_add_TargetAvailable_m1EF5D4C7882EB88D0C9D694F45669E8D1EFF32E7,
	ObjectTargetController_remove_TargetAvailable_m962874F15DE3DA3E09B7013368065213836E6495,
	ObjectTargetController_add_TargetLoad_mE227DEAE3A2AD9ED188F6C7D24885E1D2BB6CBB9,
	ObjectTargetController_remove_TargetLoad_m45A771C4438DDC2C526BBE76BF3AE294BC113DC2,
	ObjectTargetController_add_TargetUnload_m780A3151B91A3897FFF4CE70BDD588E6F0F8CCAA,
	ObjectTargetController_remove_TargetUnload_m1BC458DCA7DAEE1D5554E2BCD8CC500763635BAD,
	ObjectTargetController_get_Tracker_mBC973AEB4F4BC408691E82584F8AD8C7FAD30058,
	ObjectTargetController_set_Tracker_mC56FE6AE07641C0A523AFDA370F17AB54A48255D,
	ObjectTargetController_get_BoundingBox_m3DA8965C34B9F68907A818BA4FE46A7FEE0D95B6,
	ObjectTargetController_set_BoundingBox_mCB1C618112B490E51BDF8C160FFC45C39434BCFC,
	ObjectTargetController_Start_m8E3C0202B0F5D12D5ADD38CEAEDC80FA0B267D8F,
	ObjectTargetController_Update_mF6BC6494199C27B177DAD977F4086B178B95139F,
	ObjectTargetController_OnDestroy_m30C21E9254259690FDBC5E6216D4BF23C647827F,
	ObjectTargetController_OnTracking_m3AB79F0D7F9755DB368E2DC4878B6316AFEF83F4,
	ObjectTargetController_LoadObjFile_m7F17A33847B51BF936B3C2B363C6A9760E3CBD24,
	ObjectTargetController_LoadTarget_m13C7B89A46C6AFAB5A3B1EF22348CDB01BA9F0C3,
	ObjectTargetController_LoadObjFileFromSource_m3A0AB39A41C84BFB96005722409A4DC4CD00872F,
	ObjectTargetController_UpdateTargetInTracker_m2E8F83917286436DD63854A71DA69F0F47BF52EB,
	ObjectTargetController_UpdateScale_mBFB128386D5F81259ED4F98E576A09B92EC38704,
	ObjectTargetController_CheckScale_mD726738CA9E526250533C854910294370DA95C29,
	ObjectTargetController__ctor_m1C391159D7BCD3D09D6C320EAAF4411656A67906,
	ObjectTargetController_U3CUpdateTargetInTrackerU3Eb__36_0_m8C1180792D337C877A18944C562FFF5E4EBC0382,
	ObjectTrackerFrameFilter_get_Tracker_mC2D9FFC0B3811F42D982EB401F8498259189A58E,
	ObjectTrackerFrameFilter_set_Tracker_m188BDC6079AC0E69721BDE529B35871DBB86A900,
	ObjectTrackerFrameFilter_add_TargetLoad_m5A2C7969CAF2694AA2032CBD46A2D5B4440FCD09,
	ObjectTrackerFrameFilter_remove_TargetLoad_mFB68ED3A4B3311F8B229C097DE988121D2D4A902,
	ObjectTrackerFrameFilter_add_TargetUnload_mDF22F93E75201147273ADBF6163BB292E0CF7C6A,
	ObjectTrackerFrameFilter_remove_TargetUnload_m2112DB26C1F8BDA5F1AD00BF09D921781BCB9263,
	ObjectTrackerFrameFilter_add_SimultaneousNumChanged_mF7AA2DB4F2C411124F0C7B6FCCE0F0BBAA5DA3A9,
	ObjectTrackerFrameFilter_remove_SimultaneousNumChanged_m273F8B5AFEA5440650E5012FE72972BFD65A9591,
	ObjectTrackerFrameFilter_get_BufferRequirement_m7A4C6591B13C8F1E4957E3B5BC7AD8AD12B1FA67,
	ObjectTrackerFrameFilter_get_SimultaneousNum_mF68B05BD48E68601E9FA53004F30D460123AA484,
	ObjectTrackerFrameFilter_set_SimultaneousNum_m89366C879DD4345A961CB52CE83D3277FED6DC20,
	ObjectTrackerFrameFilter_get_TargetControllers_m970DF66E890CBAC3519B7DC88D2FF24454E4D181,
	ObjectTrackerFrameFilter_set_TargetControllers_mC0DA0165575F7BF0FBC0A7C7EA4D122391262D70,
	ObjectTrackerFrameFilter_Awake_m1D87FEABBA37419A571573174FC7314CFE413521,
	ObjectTrackerFrameFilter_OnEnable_m4E4C7E6EB0D3E92D1806295D8E8442791D68E7E2,
	ObjectTrackerFrameFilter_Start_m124441769254E7DF369036E0267D9956BB47AA34,
	ObjectTrackerFrameFilter_OnDisable_mDC6BE8A3BA675036F7642A8EE4EC2479DE505194,
	ObjectTrackerFrameFilter_OnDestroy_m0198FE12633C1B23D7C7017BDFBB667AA9F45CD9,
	ObjectTrackerFrameFilter_LoadTarget_mCE2EFE49E8DC7F673DF9A94DBAD094514DF33658,
	ObjectTrackerFrameFilter_UnloadTarget_m95C90F00445C4211D6EB1A9B30117269088375BE,
	ObjectTrackerFrameFilter_FeedbackFrameSink_m81CE66B6145420E3730CEE878D2CF2267396B55C,
	ObjectTrackerFrameFilter_OutputFrameSource_m6F4BE7B63C402E1986B3FC658BE9D17FECFA0776,
	ObjectTrackerFrameFilter_OnAssemble_m46FA6E02BCB97D3A98B1AFAF93BA206DD0685815,
	ObjectTrackerFrameFilter_OnResult_mA74735CEDF8DBD1D1FAC50FEA95A2958610F01CA,
	ObjectTrackerFrameFilter_LoadObjectTarget_mF983B406F6009BD2768C99A6153905DBE7649552,
	ObjectTrackerFrameFilter_UnloadObjectTarget_mAA2BF1DB51932A291662593B5A6DBF5F795C6909,
	ObjectTrackerFrameFilter_OnHFlipChange_m39F6BCF327D2205C21E92A3C81FDE6C03B683E6C,
	ObjectTrackerFrameFilter_TryGetTargetController_m1A646A636C2268590C8D88D6D9B97468573D0C61,
	ObjectTrackerFrameFilter__ctor_m8D11E9FB6A82302FA2C7BFBD67A59126ABB84301,
	RenderCameraController_OnEnable_m53DA30030D0625E0ACE503EB26C1609A7EA4BAE7,
	RenderCameraController_OnDisable_m49C7F48FBA009CB784ED41565F8B2DC5B867B37B,
	RenderCameraController_OnDestroy_m2E67508A90E5E02240C673769F4B37D16B64317C,
	RenderCameraController_OnAssemble_mD757178B9D7920563E82B5E82E8DCDA414E3B209,
	RenderCameraController_SetProjectHFlip_m4E0AB184DD077D2C57DC4D71F9CF80D0F14FDA8A,
	RenderCameraController_SetRenderImageHFilp_m8C494C3B787A43F214E34A2728C45A09E5BAD2E5,
	RenderCameraController_OnFrameChange_m4A98513039B0D87681CB27E90D9312E4AF17261D,
	RenderCameraController_OnFrameUpdate_m7741F651B007C8249018D636082FBF4BFF68D0EB,
	RenderCameraController__ctor_mD1DA2801D264D062996388767FF3B24061D53875,
	RenderCameraController_U3COnFrameUpdateU3Eb__15_0_m9CF747F8D0B80D455064296EE446713E737D9075,
	RenderCameraController_U3COnFrameUpdateU3Eb__15_1_m19E40B28AEEDA6FC78F91CBCD30773FF90E59F2F,
	CameraImageMaterial__ctor_m72BD5A17BE31FB0CC35528EEE02B2B3812C51A4B,
	CameraImageMaterial_Finalize_m7BAFC502DD6045B3AF1BC91BC0EFDB84B4FD8315,
	CameraImageMaterial_Dispose_m2BCD8585031CC4B9789E581BD0587295CFDC7DEB,
	CameraImageMaterial_UpdateByImage_m720FFB03834C7290957FE1D337794689B8B858C9,
	CameraImageMaterial_DisposeResources_mA8FCBB5783A257A07809E72B8A0AFBB2D6FE23C2,
	EasyARSettings__ctor_m8B67ADD90D834FD829822FA5D724BF5E4B48B5D8,
	EasyARShaders__ctor_m72997C5757B905E8795B24917DB157E791E13960,
	RenderCameraParameters_Finalize_m14071E54EB673B9F03D16331A0ECEA310FF3B2B7,
	RenderCameraParameters_get_Transform_m554C10D0BEE296CC4B231778F512DDBFAA6AE5FB,
	RenderCameraParameters_set_Transform_m28141E0952C0660A10839219F63F4609B6BEC086,
	RenderCameraParameters_get_Parameters_m780DB8B58ECD742C9534AF5B6AF6BFF14AF1BAE8,
	RenderCameraParameters_set_Parameters_mDB2F0A0974E7BF09410D7ED69748E7F3E4F6D2C9,
	RenderCameraParameters_Build_m403B648D7B010E40679A5B9317FC20405352B3B5,
	RenderCameraParameters_Dispose_mEE431E506129BE66A1BBCE930AC50EF71CD09B4B,
	RenderCameraParameters__ctor_m5CDEBEBEED27B2EE8552B31F4F3D7A06F6E7F0E5,
	RenderCameraParameters__cctor_mF0122F584F5394568EF219979FA7941FD9304F5D,
	SparseSpatialMapController_get_MapInfo_m1023D0926121715A6E700A64BA025B2CFAC51E76,
	SparseSpatialMapController_set_MapInfo_m5F926AB161BD7024F999AD525D0BE41BA0053684,
	SparseSpatialMapController_add_MapInfoAvailable_mECD14B0BB12A4683D79DF1796E19B88721BDE923,
	SparseSpatialMapController_remove_MapInfoAvailable_m6B85B7B15AC9764233A60B184CA144EF7BF1E613,
	SparseSpatialMapController_add_MapLocalized_m720B7DA81252884CA06448EF190D0742AA0F688D,
	SparseSpatialMapController_remove_MapLocalized_m0982BF72AA803AAC66EC155CC7BF3EEF7BE69C26,
	SparseSpatialMapController_add_MapStopLocalize_m906D8D831FB93E18F7927E5C15D5B616EF4C1AC0,
	SparseSpatialMapController_remove_MapStopLocalize_m75ACCFAB5353B9452CDBB26C9D9B7F9DEE4B1111,
	SparseSpatialMapController_add_MapLoad_m8CB6B0C8E8A08747DD69C4CC4206CBCCE64502B5,
	SparseSpatialMapController_remove_MapLoad_m8AADEE5A8C792CB8C2696002BBDCB0DE714D46F8,
	SparseSpatialMapController_add_MapUnload_mC00E1158C3F95B8FBFC2B040F60228F8737AC5D9,
	SparseSpatialMapController_remove_MapUnload_m2A259C65B934350ED6B742D0479F7A631BE41D4F,
	SparseSpatialMapController_add_MapHost_m12417AB0C884F7702D392A962196351745BDF052,
	SparseSpatialMapController_remove_MapHost_m4C4CFA9B89E0E5FBF93D5824FC24C900DAD1B716,
	SparseSpatialMapController_get_MapWorker_mB5F31F4C3B5BDADFE3C2DDD1AFECEBBEC094EBA2,
	SparseSpatialMapController_set_MapWorker_m8BC5C0188D493BCB17E1E2469CA5163D60ADF5FB,
	SparseSpatialMapController_get_PointCloudParticleParameter_m1295E02446E4CCE486509B53F31E01ECE543FFB0,
	SparseSpatialMapController_set_PointCloudParticleParameter_m0B161234431AE951E23CC7D7E2F44C0DF59A9E07,
	SparseSpatialMapController_get_PointCloud_mF42A58E96D27AC788C6F8005176ADD469D419D42,
	SparseSpatialMapController_set_PointCloud_mE0E20F0F338C519F5CFC610B8C80B76B872692A8,
	SparseSpatialMapController_get_ShowPointCloud_mB42B6C17DFDE159FC9447989116E76CAA0C73747,
	SparseSpatialMapController_set_ShowPointCloud_mABD291BC846420D37A52DD0486BE3A27F3CCD8D6,
	SparseSpatialMapController_get_IsLocalizing_mBF6029A571A786620A7613711DAAB3D91344A6DC,
	SparseSpatialMapController_set_IsLocalizing_m0DB198A14B594FC863B933A81229A5B8EBD77E31,
	SparseSpatialMapController_Awake_m82B0FE21BC940FD15491ED54953513B4352D3740,
	SparseSpatialMapController_Start_m3A6C450F6EBA3C4DA6B1643EA76476FC8933BDBB,
	SparseSpatialMapController_OnDestroy_m3F1106BD312AFA294F9521BA52DDE1E28F10DE67,
	SparseSpatialMapController_HitTest_m2C742DD9287312D12026EFC863A63657351F6C91,
	SparseSpatialMapController_Host_mF76C9C54EEC1ADA414542D35BF93357A528B3115,
	SparseSpatialMapController_OnLocalization_m5CE084484F638EFFD039FFB0C3739CCE517589DD,
	SparseSpatialMapController_UpdatePointCloud_m3F1ECD7C0D78454BDE90C0FF81568DE57F1F2BF7,
	SparseSpatialMapController_UpdatePointCloud_m105DAF4975FC33947A5CD6D5EE88FA6786FCD0ED,
	SparseSpatialMapController_LoadMapBuilderInfo_mE37F02F7BDF6A599A36EC3C99626BF09B0500A48,
	SparseSpatialMapController_LoadMapManagerInfo_mDBAA93FBCFA7501588CD2EC41DF4B45684E3B7A3,
	SparseSpatialMapController_UpdateMapInLocalizer_m67B104D8EA76830203F380B6B2AAB9ABF17E744F,
	SparseSpatialMapController__ctor_m178FF50DCE99809AB36FF58BE78727BD112544B9,
	SparseSpatialMapController_U3CHostU3Eb__55_0_m02DE4F29D715B2329C34F5E502140D039BE5242B,
	SparseSpatialMapController_U3CUpdatePointCloudU3Eb__58_0_mCB80C90FCC41AB437D38AE05F2D01F8E108DA12A,
	SparseSpatialMapController_U3CUpdateMapInLocalizerU3Eb__61_0_m458B2B3FA6E112D111A22712DE5A1D9E8221AF3D,
	SparseSpatialMapWorkerFrameFilter_get_Builder_m7882C92E1EDF36FF15D0D75059CFABC020FEE7AF,
	SparseSpatialMapWorkerFrameFilter_set_Builder_mEB5A848E8FDE14A62372FA1B7E9CF1E92C8BF3D5,
	SparseSpatialMapWorkerFrameFilter_get_Localizer_mF6DC0CB5B1C68042BAEA8D343AC1260B6D0DFD69,
	SparseSpatialMapWorkerFrameFilter_set_Localizer_mCA48A36CCEE084E1BA05878FFD0492C7A82F89AF,
	SparseSpatialMapWorkerFrameFilter_get_Manager_m2791BB26159DA12D9A5306631273DF815F34BDE6,
	SparseSpatialMapWorkerFrameFilter_set_Manager_m1849DF827DF5DDA87B81BE6292E43E7E975F54C4,
	SparseSpatialMapWorkerFrameFilter_add_MapLoad_mA674610F3637B16BC95999C1E2CB5ED3DAA740DD,
	SparseSpatialMapWorkerFrameFilter_remove_MapLoad_m2E221223F109360693E4D8534B12703C512CA5BC,
	SparseSpatialMapWorkerFrameFilter_add_MapUnload_m7BC4890DCA95EC5A5CA831DFE739FEC31B918C54,
	SparseSpatialMapWorkerFrameFilter_remove_MapUnload_m0E15E3CC8069508EFC99DA24DEC8CC3C3C6E168C,
	SparseSpatialMapWorkerFrameFilter_add_MapHost_m5753CC2BD74613F9118AE7B46EC5C1853BBB511E,
	SparseSpatialMapWorkerFrameFilter_remove_MapHost_m1DB18C7A03C672402D6A2FC4FB3C3D3A718E509B,
	SparseSpatialMapWorkerFrameFilter_get_BufferRequirement_mD7BADFFBA4DF406DF0232E5ED56AB448EDE80FC0,
	SparseSpatialMapWorkerFrameFilter_get_TrackingStatus_mD32B38904C597C5E697B6ABFEA793FF0F7F960E4,
	SparseSpatialMapWorkerFrameFilter_set_TrackingStatus_m7B7280AD476BB94C611BA171DE2059D0082768ED,
	SparseSpatialMapWorkerFrameFilter_get_WorkingMode_m6E3647B64810E2F7AFE24BDEDAB46A1281DC4343,
	SparseSpatialMapWorkerFrameFilter_set_WorkingMode_m6A562E5E5BE6992D5EE3A91203ACBC61BE1914E6,
	SparseSpatialMapWorkerFrameFilter_get_LocalizedMap_m596B56F3BFBE04EAACA921CB2940EFE1DE5053EB,
	SparseSpatialMapWorkerFrameFilter_set_LocalizedMap_mD364927A227ACEE987FB2FBD1313A2E008479D7A,
	SparseSpatialMapWorkerFrameFilter_get_BuilderMapController_mE5FE5884DE0E7A9BC6E859A946BB691A619F6865,
	SparseSpatialMapWorkerFrameFilter_set_BuilderMapController_m66D49306FCF12AAFCDA06940F0A1A042E549D273,
	SparseSpatialMapWorkerFrameFilter_get_MapControllers_m7B53B18890749060102996A30392306A9E9EFEDA,
	SparseSpatialMapWorkerFrameFilter_Awake_mA51993D61E79B9A3AE874C561844E4F2BC0A3050,
	SparseSpatialMapWorkerFrameFilter_OnEnable_mCDBBB1D0F7393EB1AEC7E1995719595FFB614ED5,
	SparseSpatialMapWorkerFrameFilter_Start_mD38CE8643906DAB8FB1D0B00839C3BE54673D961,
	SparseSpatialMapWorkerFrameFilter_OnDisable_m4602C5FEA7CF3C640AEA012DCA5D95EA3E0C6ABD,
	SparseSpatialMapWorkerFrameFilter_OnDestroy_mCA7C39D29104F91B7A3C5C49EB9AEA9AD10E46DD,
	SparseSpatialMapWorkerFrameFilter_LoadMap_m1A63A86323A73AF470BD4EDA8296859250BA3410,
	SparseSpatialMapWorkerFrameFilter_UnloadMap_mBD6B44E78C930C5EFA8642146608DC46B781DA3D,
	SparseSpatialMapWorkerFrameFilter_HostMap_mD7B9530B4F36AF1F0E7DED551232D76CF860B293,
	SparseSpatialMapWorkerFrameFilter_InputFrameSink_mF1480C248BFF711E220ED268BEBFC9B89AB87D98,
	SparseSpatialMapWorkerFrameFilter_OutputFrameSource_m6FA5A59A644D5FA68B92AC5570A4A2526867131E,
	SparseSpatialMapWorkerFrameFilter_OnTracking_mF8007A28BB7E551E257974D224FBB6F077B5E136,
	SparseSpatialMapWorkerFrameFilter_OnAssemble_mA0009DCC8941D378ECFD6A379E42EAAA9CDF5848,
	SparseSpatialMapWorkerFrameFilter_OnResult_m8555931A0D4E1DB9D58C43E37EC961BDC7E443C1,
	SparseSpatialMapWorkerFrameFilter_LoadSparseSpatialMap_mC958F60621FC7B7A636108A9F1EB8356C2597BE1,
	SparseSpatialMapWorkerFrameFilter_UnloadSparseSpatialMap_m59901CD8554C447DDB39E15F0404886EF73E1DBA,
	SparseSpatialMapWorkerFrameFilter_HostSparseSpatialMap_m91ECE225E834BD20FFCF8F3918CE9AE6E78066BF,
	SparseSpatialMapWorkerFrameFilter_LoadSparseSpatialMapBuild_m33C9AA05FFBCC36F8B84B1D66E707D2E32CF9042,
	SparseSpatialMapWorkerFrameFilter_UnloadSparseSpatialMapBuild_mBC1F9F42D38B646CA8570B8BC15D946E2D9C4947,
	SparseSpatialMapWorkerFrameFilter_TryGetMapController_m6415E8B0C458806359B33027D330F53F1B3D1692,
	SparseSpatialMapWorkerFrameFilter_NotifyEmptyConfig_mE3987B095ACCC605ED0B14B29CD665EA80EE6586,
	SparseSpatialMapWorkerFrameFilter__ctor_m971448E3E4FB1B51714D52A209E9EB874565EB8A,
	SparseSpatialMapWorkerFrameFilter_U3COnAssembleU3Eb__62_0_m988C7E1EF683C39F39EA4BFCE50EF97C030F5A42,
	SurfaceTrackerFrameFilter_get_Tracker_m5C6AD3EE6DA43304EC472DEE107E620242AA9D6A,
	SurfaceTrackerFrameFilter_set_Tracker_mEE859A6937772F269F814E1FF530F9323C9C7E70,
	SurfaceTrackerFrameFilter_get_BufferRequirement_mBCC9C015C6E1ECB282EF95F98C843CF323A63284,
	SurfaceTrackerFrameFilter_Awake_m23FAAA6FF176DCC80A972D7F74D6BA0B9BFEB501,
	SurfaceTrackerFrameFilter_OnEnable_m17790321336D42658E43955B91E85ACFE033B53D,
	SurfaceTrackerFrameFilter_Start_m1DC3B1190066DE5DB1383B3076D1AFB78925557E,
	SurfaceTrackerFrameFilter_OnDisable_mCE9B087CD21C8A3B8019026FF1C435BF05D3E0F1,
	SurfaceTrackerFrameFilter_OnDestroy_m8EC5387EE3A945FCB9C4FFC493F1473057A58FBD,
	SurfaceTrackerFrameFilter_InputFrameSink_mE1405C3197E5CEF771DFF64E6E669B990008DCF5,
	SurfaceTrackerFrameFilter_OutputFrameSource_m28F36AAFC83E84E0A5D0B68D88F8DDCC3EE9365E,
	SurfaceTrackerFrameFilter_OnResult_m70A9942092AD483ED8DFFAAFCE55BD9CF33DB3DC,
	SurfaceTrackerFrameFilter__ctor_m528F28D17051C1BCAE63883C2910AC3FA67D2178,
	TargetController_add_TargetFound_m4127149D8DD008227A384A840D0921EF33341C05,
	TargetController_remove_TargetFound_m5894D2C3FA2A579FD72DF773E7887BC34B3BA279,
	TargetController_add_TargetLost_mCE77AC87A123425557F396C245C7FB757101378A,
	TargetController_remove_TargetLost_m04CC34D71BA33CDED4966A1895C2DF987E878803,
	TargetController_get_IsTracked_mE495B549B5BFA48F8BC757626AD8E8394941B42F,
	TargetController_set_IsTracked_mCD24A01BE81D992CBABF29C8ADB724B8FE7EA39C,
	TargetController_get_IsLoaded_m6025FDCD6D83CC1523A7829670AC091B09255CE0,
	TargetController_set_IsLoaded_mB1D22FD919DE48492DBF97F73BE89C253A5619B5,
	TargetController_Start_mC720E5BE377FBE4AC6670DC7243C73CA574C7AAB,
	TargetController_OnTracking_m96DB86A87FA5725A566913F2D283D4522374F47C,
	NULL,
	TargetController__ctor_mA9AD9CC87BA8FFE3D24F285A7685B9C535ADA1E0,
	APIExtend_ToUnityMatrix_mBE451AEDF08B177882F754B4FBB49AC9A9EEE494,
	APIExtend_ToEasyARVector_m437FDBE6E2D4323B5BA97882E5FD783E30DCC580,
	APIExtend_ToEasyARVector_m0E65CBACBE7E7625A4920594440430EB04B10DF9,
	APIExtend_ToUnityVector_m43F1C17DDE433DA99FDDA072CE293B122166B447,
	APIExtend_ToUnityVector_m66DA802A68B830466952E99D67ECB6FDF194663A,
	Display__ctor_m1AD625F36564FF1E706AFA4997F26F32FCEA9F5C,
	Display_Finalize_m357A6E5785E241E90831E3BBD269EED1AB5AFCD0,
	Display_get_Rotation_mA63DEB1DB98673A39EA31029CC3B06291A5851C3,
	Display_Dispose_m446158E80AF25C4EFC5D2FCC9630C256AE5200ED,
	Display_GetCompensation_m637617AEF976AB6E50BA52228E8437220F715D20,
	Display_ImageCoordinatesFromScreenCoordinates_mD3266FD101F378876A1B6FCD31EF842EE24FEDC9,
	Display_InitializeIOS_m7C2AA074D4BB3CB36CE6775138FBF3D41C24575E,
	Display_InitializeAndroid_mA8B98E5CA5A4B7F7ED890C61C53C669E964554BA,
	Display_DeleteAndroidJavaObjects_m2FEE84D83DD6DBD481AE23B835C81D3683942461,
	FileUtil_LoadFile_mD43F5F1702DF1DF55D4F2AFB97BA21ABA0190A06,
	FileUtil_LoadFile_mDB0E599D81E9E8D414719BCF8111F12FF5D457C8,
	FileUtil_PathToUrl_m6E5A9B8E1BD46EF75DCCA78D6B9359EC30CA0C8D,
	GUIPopup_Start_mA2F6B2AAD6D808B1BE7573E7A3B3E4E8E194B84A,
	GUIPopup_OnDestroy_mCB4785FD7B3C8DCCCE69BB614B5D60699C5048CC,
	GUIPopup_EnqueueMessage_mDFEED6E172639F1823D04AF7FCD63A516DE8716A,
	GUIPopup_ShowMessage_mCFEBD07B2E63FEC66DEED5D544A50A898275305E,
	GUIPopup_OnGUI_m4319587E47ACA5E7919BE8AA5D42186F618D6906,
	GUIPopup__ctor_mA9F14511F21710EBC025312292B61F3E08BA58F6,
	UIPopupException__ctor_m1A92438E8ACA8383DE1CC8CB7453F5752FEE57FC,
	UIPopupException__ctor_m057582E8AF359A569B766600F5739300B215FBE4,
	ThreadWorker_Finalize_m1C459E226F863CD51CDBE78D8C1292F0A4531FE5,
	ThreadWorker_Dispose_mF7C5C56EF2141ED9D2BE9C18426ED0B88CF564A8,
	ThreadWorker_Run_m19DD9ECC471B4210998FADB083A2FD1A043EA337,
	ThreadWorker_CreateThread_m02D76BBB7DF58609DDC6EE4A8AC3DF0A81D9BB09,
	ThreadWorker_Finish_m6AF3181E13AFC0545E80208E17A77BC3144B091D,
	ThreadWorker__ctor_m6F7838EB5060A15312AE876039A6FDF586376D11,
	ThreadWorker_U3CCreateThreadU3Eb__6_0_m0E1686EE64CED61616CD287A32B75E6601B0C161,
	TransformUtil_SetCameraPoseOnCamera_m13D887D57FC7C4B9237ED8C2D1D35F79EDAE3CE1,
	TransformUtil_SetCameraPoseOnWorldRoot_m6FA66D2711D814618B6CF2B2B08D927569AE8909,
	TransformUtil_SetTargetPoseOnCamera_m604DDC9ACDD7E7A423FBB7A19BB16EDFF9DE1B19,
	TransformUtil_SetTargetPoseOnTarget_mDC8C81046E396F129CAFDF52594A3415FF8DA9BA,
	TransformUtil_SetMatrixOnTransform_mF5B2194C1006AE89353FE63EA64301AD32229DC4,
	TransformUtil_SetPoseOnTransform_m81BF07415D9FAFD5CAEECF6E62ABF40FE2F4B2D2,
	VIOCameraDeviceUnion_get_Device_m2C42E176D57109D451A3A1BF46BD6202B3654F29,
	VIOCameraDeviceUnion_set_Device_m2A6DEA1412FBB35F64F880C78667BB05DD62D0EA,
	VIOCameraDeviceUnion_add_DeviceCreated_m34F4B42C2CFBBE30760549CC2A4BC56703FF6DCF,
	VIOCameraDeviceUnion_remove_DeviceCreated_m7DF3F58F220B87DB94648F7F740B0364B98230EE,
	VIOCameraDeviceUnion_add_DeviceOpened_m0BCA0E8F56E2E44323EB64F96D8DC84DCFAA55D4,
	VIOCameraDeviceUnion_remove_DeviceOpened_mEB5C17C89997E2F17FAD59B86AC0627859454672,
	VIOCameraDeviceUnion_add_DeviceClosed_m74113D4216A7B3C5D238B792A56DCAB042103789,
	VIOCameraDeviceUnion_remove_DeviceClosed_m126CB7D70435537BF80377282724B3F454A6ECA4,
	VIOCameraDeviceUnion_get_BufferCapacity_m36FD16961B9A6FA002E6B77D60780E7DA75EE041,
	VIOCameraDeviceUnion_set_BufferCapacity_mCC85160780B4C41C064A1523740E77950EF83144,
	VIOCameraDeviceUnion_get_HasSpatialInformation_mCDBD63A7EDF527DE2C8727597CD2F9F59911AC58,
	VIOCameraDeviceUnion_OnEnable_mFDFEC774A6852848F52FCEF3E9B344390F27ABE9,
	VIOCameraDeviceUnion_Start_m52D54581297DA27729AFE9AA9E7AB975420CD830,
	VIOCameraDeviceUnion_OnDisable_m6B139E536F34F7AF602512C1E5373EE974C52D73,
	VIOCameraDeviceUnion_HitTestAgainstHorizontalPlane_m232ACE05AD8CA0D1FE332EB5CC2F4D1D38AB9533,
	VIOCameraDeviceUnion_HitTestAgainstPointCloud_mCAB2C0E6C66A1B998D4F4E2258E26AC701677B5A,
	VIOCameraDeviceUnion_Open_m588140E99F05C3F0A0661781D1AA8E314626874D,
	VIOCameraDeviceUnion_Close_mD7F1F6A8F5F1AC50721A97615CF7DF9F0F77FA70,
	VIOCameraDeviceUnion_Connect_m8B5FC218B425F462A7E28DB79E0E408664F737DA,
	VIOCameraDeviceUnion_CreateMotionTrackerCameraDevice_mEC4E9D967ECD453576405A396BAC86A184D3EE2D,
	VIOCameraDeviceUnion_CreateARKitCameraDevice_m696DD97C8C702CC7FAD0F478C552E105520F5383,
	VIOCameraDeviceUnion_CreateARCoreCameraDevice_mC5FE3DFD3E2AAFBDFE059618485A2F5F6A231368,
	VIOCameraDeviceUnion_CheckARCore_mE7513936E45F9E1F6DDFFF9DD7510CE956239337,
	VIOCameraDeviceUnion__ctor_m78375472B7FE83443423F57400FBF8DC45B7C116,
	VIOCameraDeviceUnion_U3COpenU3Eb__32_0_mD7EC69EB3F56C477C87CE7609BD34FBACED2F592,
	VideoCameraDevice_get_Device_m799A6B482E698A6B53E4477AE38A444935635E28,
	VideoCameraDevice_set_Device_mAE44E8144AFA4B216916404140C65E9C3FF4009D,
	VideoCameraDevice_add_DeviceCreated_mC9711D3B2DA2B725BE576887245A61AA50F2F2C1,
	VideoCameraDevice_remove_DeviceCreated_mF6F35403C4BAC81D55E444717B709DFBD1C96B87,
	VideoCameraDevice_add_DeviceOpened_m3E6E1021C62A3BBC2FE432E220435075A458E9E4,
	VideoCameraDevice_remove_DeviceOpened_mF382A975D6600BAB983E7DF6032008F5CD9F6336,
	VideoCameraDevice_add_DeviceClosed_mB33DCB0C932FA014853B2EFF0857C25A068070F6,
	VideoCameraDevice_remove_DeviceClosed_mCC212AF7BFBDBC200371B0DF80B526EB010E9FA1,
	VideoCameraDevice_get_BufferCapacity_m23F8FF409FC7FB23BDC02DEB876B14886CBB5498,
	VideoCameraDevice_set_BufferCapacity_m6F1B5D28959F012B470EE8B2A762602EB8FD944E,
	VideoCameraDevice_get_HasSpatialInformation_m0501D3A65592ED0A3CE747FBCFE9D2DCFAEE3781,
	VideoCameraDevice_get_CameraPreference_m5F776D763A3F84E9D0CDF08945DA89C058D708EA,
	VideoCameraDevice_set_CameraPreference_m16690F35E68C4D606835C2032C890E5099893778,
	VideoCameraDevice_get_Parameters_m87F8FD49044B04ABAA51B8617FD32A40901ED919,
	VideoCameraDevice_set_Parameters_m76ED0A7906776AD8F959CDB4D73D5F7A8145A6EA,
	VideoCameraDevice_OnEnable_mDB513EE63130DC6EF23D2236A5DB2E6C25762A82,
	VideoCameraDevice_Start_m8525AE3EC822D1DA4187A07730666DECEB18D4F2,
	VideoCameraDevice_OnDisable_mD57631315AC550F928C1E53573A3F24C96E99B32,
	VideoCameraDevice_Open_mEECAB310D7A2B1D07C6386F6A0201FBD118D16FA,
	VideoCameraDevice_Close_mFA09C7DE6F07D90834433B977516B4D5DE902123,
	VideoCameraDevice_Connect_m9E3C8BA850F9955C9D25FF36FAEBA2254D2DAF5F,
	VideoCameraDevice__ctor_m3DE7A4D58A5B71A0524CCDD6C8669E99B073451C,
	VideoCameraDevice_U3COpenU3Eb__36_0_m7021222C13A6A2F4D3BA8B99BCBAEA75B8E63146,
	VideoRecorder_add_StatusUpdate_m6352DE5EAAC3B6751422A911A53A4FD927D70B1E,
	VideoRecorder_remove_StatusUpdate_m4B43F05FAA6A79653237314BB4B5DBF14986B116,
	VideoRecorder_get_IsReady_m3CE280A2BCDE4D72D4F838D46B2D9082B5743372,
	VideoRecorder_set_IsReady_mD673ADC59B138F9C4243E63A155F49351A63480B,
	VideoRecorder_Start_m212AD34167D5914A4BCDB34618EDF1C419C573A3,
	VideoRecorder_OnDestroy_mD39C1C27A7BD34843FC9EF26DF36C868B04B58DA,
	VideoRecorder_StartRecording_m18BFEA6DBFCED419FABDB0CB82018DD23188E767,
	VideoRecorder_StartRecording_mC4932CABA354103140AD3313D8E9E32EBE8D25BA,
	VideoRecorder_StopRecording_m9AF24802471D88A206229D1276DEB25365424729,
	VideoRecorder_RecordFrame_m92C5FABB0FE16AB0E80B105437F91D1B96565818,
	VideoRecorder__ctor_m90399E134DEBB0AB333556160FB608B31B1583DA,
	VideoRecorder_U3CStartU3Eb__15_0_m543C30B8D166A76D34B2087705BC5E87596CC960,
	VideoRecorder_U3CStartRecordingU3Eb__18_0_m4A273ADCB5E5FD4AE84E11A73DBC20294F89E81D,
	WorldRootController_add_TrackingStatusChanged_m834D8D5BE14FF9E80FDA444984F216E99D6B384F,
	WorldRootController_remove_TrackingStatusChanged_m785F5A355D594B741AD2BC4DF9BA9FB62CEF4B81,
	WorldRootController_get_TrackingStatus_mF4BED702EF437F788F9A051F1E2B7F823BBFF7DE,
	WorldRootController_set_TrackingStatus_mEB4297697511E6785D0D2162395B38EA8DBF135F,
	WorldRootController_Start_m81BC514715FB4BC5602B70D45F23E490E2219A6C,
	WorldRootController_OnTracking_mDC2D19C2A8F68EC11E517A7BF337478EE4D8DD6D,
	WorldRootController__ctor_mA1B08A3360BC9C6C30842CEB196FB209BCA18545,
	Detail_easyar_String_from_utf8_mEC63092B093C81C2AA6A80386483692767D64245,
	Detail_easyar_String_from_utf8_begin_mE06810143D59DF76FBBD28720B81A81F8BAC2484,
	Detail_easyar_String_begin_m39EEFC5D16AA6E2D997EE721CEBD177006153654,
	Detail_easyar_String_end_m31F3C053C7710D0D8BFFD3E07F7B9A3C8F5AC713,
	Detail_easyar_String_copy_mE037CE974098567C01C30D2095B49744AB6756E6,
	Detail_easyar_String__dtor_m08D4EB43B7A27A2330B8C56132C2F186CD14D802,
	Detail_easyar_ObjectTargetParameters__ctor_mB9E53E6603CE5C903B2EBBEDD32DFC7914260F53,
	Detail_easyar_ObjectTargetParameters_bufferDictionary_m7E3A01AC176652FF2636B053591BDFBF917A1948,
	Detail_easyar_ObjectTargetParameters_setBufferDictionary_m8FBB22DEC13DFBE457D472C5A30A2CCDDEE95FC6,
	Detail_easyar_ObjectTargetParameters_objPath_m7F64B2432450988DE08130B620DE80588AA35D57,
	Detail_easyar_ObjectTargetParameters_setObjPath_m304FFEA6EB5BC12AC7A65955CAB172231F7F8803,
	Detail_easyar_ObjectTargetParameters_name_mA05DACB9228033F247651EAA033675B046411DAB,
	Detail_easyar_ObjectTargetParameters_setName_m68E26990D77DD643EE34DD62816FFCE71EBBD2F4,
	Detail_easyar_ObjectTargetParameters_uid_m8985E92307412E49B39AF96934D1C4124B36AF55,
	Detail_easyar_ObjectTargetParameters_setUid_mD59AD3FB2A94B08E7A83FDBA13BBF3067505F678,
	Detail_easyar_ObjectTargetParameters_meta_m92B72E57F2906696F1117FDB58E58E3C332BFFFB,
	Detail_easyar_ObjectTargetParameters_setMeta_mAB9102B1B9AFD8AAE2C3CD6F162E67EF009E9501,
	Detail_easyar_ObjectTargetParameters_scale_m6ECAFBFF295DB747E3FDBAE69870D93D0A4EA9B8,
	Detail_easyar_ObjectTargetParameters_setScale_m753395B4F74F4078838FD98008BEA61758D1C23D,
	Detail_easyar_ObjectTargetParameters__dtor_mFFF9183DCFE7E0BFF8E64A92F0029D028AFD6F25,
	Detail_easyar_ObjectTargetParameters__retain_m182F13A290FF4BC2A61CE46C256A212A432FF600,
	Detail_easyar_ObjectTargetParameters__typeName_mC6B2A23563EB234F251CCCD290BE40A81B4868D7,
	Detail_easyar_ObjectTarget__ctor_mAE2141F8CC6AE55601345AEC63CF75493D93A200,
	Detail_easyar_ObjectTarget_createFromParameters_m08F9A8F03C88F038F39D898DB9D4344FB0D3B426,
	Detail_easyar_ObjectTarget_createFromObjectFile_mA3B691058CBD946FB8B6EA3F05DFE8DFAE558B58,
	Detail_easyar_ObjectTarget_scale_mBD53D84323EA11D7EFEB2A171EE86C0AB5ED4420,
	Detail_easyar_ObjectTarget_boundingBox_mBD0828D1988D3BC47287A629096D449AE8349731,
	Detail_easyar_ObjectTarget_setScale_mD15FF74A935C0D5788D92F417E16E84E9B292466,
	Detail_easyar_ObjectTarget_runtimeID_mA399107D7C1D625CB41FCE6DEB7F056F3CED9B0F,
	Detail_easyar_ObjectTarget_uid_mDEBB9265658BD3A0EA64168F74BF6D8E6824DC0B,
	Detail_easyar_ObjectTarget_name_mBAF4B3CE888378432BFDE1BCCA419BD7154FBFF0,
	Detail_easyar_ObjectTarget_setName_m8F55AF7DB366BD7ED2633E0E55473AE3863F494B,
	Detail_easyar_ObjectTarget_meta_m16E238F12C9EE6955B8FB82F060130F1B846A21B,
	Detail_easyar_ObjectTarget_setMeta_m325AAA73EB2CDDB730AB9D4F8C5B7C08E1AA7783,
	Detail_easyar_ObjectTarget__dtor_mEBCBC1A4CCA0DE96E4BA1CD24AC27229A7E8BD0F,
	Detail_easyar_ObjectTarget__retain_m0E1D5E118F70A42CD1FEABE941C41D06179A98CC,
	Detail_easyar_ObjectTarget__typeName_mC5D0497CBAE539975C0CD63A72BB79818F76121B,
	Detail_easyar_castObjectTargetToTarget_m54039F31B8182449F8A12264B39B6D0BEECEDEE1,
	Detail_easyar_tryCastTargetToObjectTarget_m5F21B243433711899FCFBE34447C5911710EFE43,
	Detail_easyar_ObjectTrackerResult_targetInstances_m4782F113CF5B6310CD51660614B4C7B00641B521,
	Detail_easyar_ObjectTrackerResult_setTargetInstances_mB72CB55B5D216A952EAB91C4F9C0D44683DA7B30,
	Detail_easyar_ObjectTrackerResult__dtor_m68EA011AD90491E1DB2FEE951DA8ECE302A51234,
	Detail_easyar_ObjectTrackerResult__retain_m7F765ADE3EFDD2CD6AD2BF5AFBF88BF4099A7EBE,
	Detail_easyar_ObjectTrackerResult__typeName_m69D987D4CECEF33A6D7D3B5145240D767E571F27,
	Detail_easyar_castObjectTrackerResultToFrameFilterResult_mF4E5A4912E8474AB5C146AFD320960518433A3F8,
	Detail_easyar_tryCastFrameFilterResultToObjectTrackerResult_m8B8CF2B94DD0B402C462CE44BA9C72BC78382BB5,
	Detail_easyar_castObjectTrackerResultToTargetTrackerResult_m963F7B87CE0D6CCF49886BB53EE76CA38D290408,
	Detail_easyar_tryCastTargetTrackerResultToObjectTrackerResult_mFC426A1F31556A17D97722C075C6E3A42189BA63,
	Detail_easyar_ObjectTracker_isAvailable_m67C847E3784D7532A35C1536D12BB81372F8F4AA,
	Detail_easyar_ObjectTracker_feedbackFrameSink_m647FA27212809ACF929CD95EFA6EA828DB7EED03,
	Detail_easyar_ObjectTracker_bufferRequirement_mFC3BF01AD75D21B6869885E54E8D4EDFACFC45A1,
	Detail_easyar_ObjectTracker_outputFrameSource_m0C68A00F5545D47D3E67667E9633A0654B525707,
	Detail_easyar_ObjectTracker_create_m5F0B0D1E7238E742743BF34ECA1B349158BFB427,
	Detail_easyar_ObjectTracker_start_m4D999550A948765607B4A16EE93C86BD44EC702C,
	Detail_easyar_ObjectTracker_stop_mB13C5FE83A8796170C0A9612F49B217C67C2B383,
	Detail_easyar_ObjectTracker_close_mD0C7449847E8A9470BF8D2132ED24797738FD4E3,
	Detail_easyar_ObjectTracker_loadTarget_m9C35F6282D552907B42C12CFBF414E906537A120,
	Detail_easyar_ObjectTracker_unloadTarget_mE855574256EC16940D29ECD19C0C621F635099BA,
	Detail_easyar_ObjectTracker_targets_m2DE1E07711B3E806B0D0CA969CB8AA8D884720A9,
	Detail_easyar_ObjectTracker_setSimultaneousNum_mA285B33B7B08AE6BFF15B654029634C9F01FCB60,
	Detail_easyar_ObjectTracker_simultaneousNum_m6C50C7F3E5FB7AB70360208DA7DCA41AFDEFD712,
	Detail_easyar_ObjectTracker__dtor_m85BFB9128B6CB91E07BCF8DE5CAF43170897E152,
	Detail_easyar_ObjectTracker__retain_m7D83EB0FAC658BA21EEC02000A9F741556207AC6,
	Detail_easyar_ObjectTracker__typeName_m014FF56637C22408F6F7F434A7A9B3454879DEDC,
	Detail_easyar_CloudRecognizationResult_getStatus_mD974099BDC6FC621B69D0EA90682565FE6934EC8,
	Detail_easyar_CloudRecognizationResult_getTarget_m5119FD741F471CA50B84DE079EF4AAF849428ED3,
	Detail_easyar_CloudRecognizationResult_getUnknownErrorMessage_mC8FE86A63C3EDA140825353F81D42E5403146CD8,
	Detail_easyar_CloudRecognizationResult__dtor_m9F43D1264F731C9AB0CFBAFF301458F4C003041E,
	Detail_easyar_CloudRecognizationResult__retain_m9DFE4BFA4AF32B50F2E44B24BE82B272BBFEB242,
	Detail_easyar_CloudRecognizationResult__typeName_mE68DBD216AB91075E2B2A868F2CD53833BDFCB1F,
	Detail_easyar_CloudRecognizer_isAvailable_m201CC9DD52255C6A5804A0855990D317B6811B07,
	Detail_easyar_CloudRecognizer_create_m5BB930FD2625FAAAA4637845A534C05FDC36F580,
	Detail_easyar_CloudRecognizer_createByCloudSecret_m63E42BF7539EB4EB2C19670E2A7DD90E7D4ED8CC,
	Detail_easyar_CloudRecognizer_resolve_mC9FFBC16A685A39B2E298DA60589EEFFA5A776F1,
	Detail_easyar_CloudRecognizer_close_m405C348ECC3244615430CCAB700984D2FC710072,
	Detail_easyar_CloudRecognizer__dtor_m6A97B9CE132F5326AAFA8C77D252A24EEB8997B4,
	Detail_easyar_CloudRecognizer__retain_m3E3D1A8C5B6A55C604380034598E90316F072402,
	Detail_easyar_CloudRecognizer__typeName_mCEA5F706CEDD21CBC831994953A824C99AAA415A,
	Detail_easyar_Buffer_wrap_m1BA3620B18E939ECD0965D326F91676F8B62EF58,
	Detail_easyar_Buffer_create_mB21EEBF963BDE6AF4EF6B61F8F709D93AAA6A25F,
	Detail_easyar_Buffer_data_m4BE3323C222DD1B494C5021C4BADD004B5F3C571,
	Detail_easyar_Buffer_size_m619FADBC4C831EBC1C885BB1128B53B6CFED85EA,
	Detail_easyar_Buffer_memoryCopy_m19FA6A02A9CD7FD667B0CD6092E34336ADFAE1F3,
	Detail_easyar_Buffer_tryCopyFrom_mC5D8D3C3D69E4F311973DEDE01DEFCF5A034CAE9,
	Detail_easyar_Buffer_tryCopyTo_mC9746452D2931C2EB22A31A386F13553AC8F7D91,
	Detail_easyar_Buffer_partition_m6EC2EEBE6DFB176B7817F836AEBCA8B8A0687DA9,
	Detail_easyar_Buffer__dtor_m7C4FCF34E3D9F2ED6A208F334425C54D3448C63B,
	Detail_easyar_Buffer__retain_mEB8A08340F3409D2EE5212EE79D9B90CB4B81B7C,
	Detail_easyar_Buffer__typeName_m7F89C1945DF282FC9D6DEC3F6B96A3950E0AB42C,
	Detail_easyar_BufferDictionary__ctor_mF88BC5DFD31DF23E0610E8758C8F619A70792BB1,
	Detail_easyar_BufferDictionary_count_mB88F66E4091600690AD888F9E1E67A14D52AC200,
	Detail_easyar_BufferDictionary_contains_m20A73889356E8C6C3C1D4DC508557A9C18C50615,
	Detail_easyar_BufferDictionary_tryGet_m9DE6E8366441EB9601C2F5BE2164518ABC7EC9F0,
	Detail_easyar_BufferDictionary_set_m1563DB528A311DC892EE52AC045524982A74A4F1,
	Detail_easyar_BufferDictionary_remove_mA67B74E057062D1E4C4CEFC6C05B7FC94075EA24,
	Detail_easyar_BufferDictionary_clear_m5C605430F796159E777F5FE2F3222E4EBF92B257,
	Detail_easyar_BufferDictionary__dtor_m4B19C39380DBAF585634F598F6443775A898AF77,
	Detail_easyar_BufferDictionary__retain_mAC84AC741125F3E5AC83E291BA403AC3A2B370C6,
	Detail_easyar_BufferDictionary__typeName_m476C0B239F7BD25D0982C676B669AFBF345B9C1A,
	Detail_easyar_BufferPool__ctor_m1BBCA33373C240A2B3F0F2CE957024BAB5A34365,
	Detail_easyar_BufferPool_block_size_m373DDA4C36F156047690AFEFB6B2ECD33BFE0EC5,
	Detail_easyar_BufferPool_capacity_mDB7DA3F41C80142FB7516728994310253069C6C4,
	Detail_easyar_BufferPool_size_m52131BB12D573FC59EA284903D5F324060A698FC,
	Detail_easyar_BufferPool_tryAcquire_m1AC4A184D52F644CC96DBEE84E152A948BDB2FCA,
	Detail_easyar_BufferPool__dtor_m655D1AAB727BD94D751B73D9B6AAA7292D88F292,
	Detail_easyar_BufferPool__retain_mDB58C5C51D924279D9D892A4CCE886C65903A47A,
	Detail_easyar_BufferPool__typeName_mB254DE792C79843BE92523B6263F57295A5E4991,
	Detail_easyar_CameraParameters__ctor_m897B1280BAE20D82859582F6EB1CAAA73144F549,
	Detail_easyar_CameraParameters_size_mAF44717B69E65A626B9CCA2C238D8A85C7E44C92,
	Detail_easyar_CameraParameters_focalLength_m9E9A9760ACFD81A860F330FCFF894448A37F0C87,
	Detail_easyar_CameraParameters_principalPoint_mECB6A365CFCA09C8328C9A8F9A5959032E6E4B43,
	Detail_easyar_CameraParameters_cameraDeviceType_m7A2AF8C40F07909E961D8981A2D5E877259ECC18,
	Detail_easyar_CameraParameters_cameraOrientation_m7BD5C65CEFEBFF51AE19D1C08787267AD0DC7D38,
	Detail_easyar_CameraParameters_createWithDefaultIntrinsics_m6B900C64227F14B57B9B26E235B497F52DC93963,
	Detail_easyar_CameraParameters_getResized_m661F13960B6111C5F8FF19A5730A2AB880E04A3B,
	Detail_easyar_CameraParameters_imageOrientation_mFF3AF26B9955E750F2A11AD73B295D316324C79F,
	Detail_easyar_CameraParameters_imageHorizontalFlip_m9D08E0AD13D22D6A382393D92F48CFF8BECF00DF,
	Detail_easyar_CameraParameters_projection_mB661C618AB04E111C143FB8CB32900B345EB78BE,
	Detail_easyar_CameraParameters_imageProjection_mED4BD94E869194F2257AB0BFB3F7AD27D0EA8F98,
	Detail_easyar_CameraParameters_screenCoordinatesFromImageCoordinates_mF6EC3C88050FA584197A9EACA6A22DD0A962239F,
	Detail_easyar_CameraParameters_imageCoordinatesFromScreenCoordinates_mB22DF92BA55E7A1F7BF78C86D65F22B78EDEEDDF,
	Detail_easyar_CameraParameters_equalsTo_mA4A2255BB0150AAD3251593ACCCD8B3049473CFD,
	Detail_easyar_CameraParameters__dtor_m23DD42C18919DF6BBA1BA1C714F6F9DA126FC69A,
	Detail_easyar_CameraParameters__retain_m508245EA4FFBC020B76925C936B8B31E86BFBF6E,
	Detail_easyar_CameraParameters__typeName_mC6C10988FB4324EF049A2F380FFDB278A1EE94E1,
	Detail_easyar_Image__ctor_mDFA08B19001ED6365A1D6DE274691625EC5D515E,
	Detail_easyar_Image_buffer_mE3DA04E2C9AC275A2440277AA1731FFF0EEFC36D,
	Detail_easyar_Image_format_m5EB4088D2217F46418ABCD5DAE8B466760BBCF99,
	Detail_easyar_Image_width_m5BBB9EAC852E680900861C9EAFA1F3BB675E8AF4,
	Detail_easyar_Image_height_mB6E4812EFCDFC6F7DD18D92EF396954D84D58467,
	Detail_easyar_Image__dtor_m57538823E226ED2299B5270DD1211FD1996616BA,
	Detail_easyar_Image__retain_m47B8B304E7C87040C0ABE952ABA27C42C425394C,
	Detail_easyar_Image__typeName_m4D23476A4F48B9CF4D236D85BB846F87572819C7,
	Detail_easyar_DenseSpatialMap_isAvailable_mD56F4A8FACA03ECEB90252FB29B7B67EC538CDF5,
	Detail_easyar_DenseSpatialMap_inputFrameSink_mEBFB835C2A63589BD63E164CF12AF69685C2E78F,
	Detail_easyar_DenseSpatialMap_bufferRequirement_mF77C4FAF66FE3B74F8CF8D08A0D6C1FDE19AAF28,
	Detail_easyar_DenseSpatialMap_create_m0B006C4A9E1DBEA3285FB4F570800ECC74C115EB,
	Detail_easyar_DenseSpatialMap_start_m0EA827F28B96AA9C3FCA471A084E1B29808035BA,
	Detail_easyar_DenseSpatialMap_stop_m4A989E6ADC1C463D3F1778877DA8216D699B4696,
	Detail_easyar_DenseSpatialMap_close_mC023723A4F77E20C257BA865BC34F4A47834F167,
	Detail_easyar_DenseSpatialMap_getMesh_m034341C47604DE6626F3C466A384E4A3949D5A7A,
	Detail_easyar_DenseSpatialMap_updateSceneMesh_mF8E6462A66B2D0D6D46FB44CFE3025230EB2CA68,
	Detail_easyar_DenseSpatialMap__dtor_mC611B57723849B8C6D6330548597E4F4E2D6EBCE,
	Detail_easyar_DenseSpatialMap__retain_m3BE4D8B4E2596D9A64235FCE8B48711973EBDD75,
	Detail_easyar_DenseSpatialMap__typeName_m05F8BE0F0FC9AEB4E398BE2F1557DDE26D0C1EA2,
	Detail_easyar_SceneMesh_getNumOfVertexAll_m58CFDE70A0E43BE09D4B3A784EA48F052074D9F3,
	Detail_easyar_SceneMesh_getNumOfIndexAll_m7E70741A16C1BCD80E4DFDD3AA8057D5911231A3,
	Detail_easyar_SceneMesh_getVerticesAll_mDFC24613895FE0E63C7F040731E306F94B4EDD10,
	Detail_easyar_SceneMesh_getNormalsAll_mA49B7FF6634C23C6A471844095C501990AD30517,
	Detail_easyar_SceneMesh_getIndicesAll_m894FE7CBA2752AD36F6EBB6C2EADADAD2F57ECA3,
	Detail_easyar_SceneMesh_getNumOfVertexIncremental_m7D871DA397D69511293251866E872E6B9BCA94D6,
	Detail_easyar_SceneMesh_getNumOfIndexIncremental_m293D5252D637A88DB34B65E6DA9FCA4FDC2C567F,
	Detail_easyar_SceneMesh_getVerticesIncremental_m3619C833C1F2EED5F7C10F0417C57171C86D4112,
	Detail_easyar_SceneMesh_getNormalsIncremental_m830B09C760822223E467D7BB9F328B69E4F4C072,
	Detail_easyar_SceneMesh_getIndicesIncremental_m21450204BAF81B4975B3E14F7DDC30DA1D29DBDA,
	Detail_easyar_SceneMesh_getBlocksInfoIncremental_mE2C40FC3C5C8F323C9C11A4F71003B49982DC6A1,
	Detail_easyar_SceneMesh_getBlockDimensionInMeters_m221BAA9A8620C086757BD2A575D77769371576E8,
	Detail_easyar_SceneMesh__dtor_mB73222755F14AFD93A1192601C3A5D7A163D4B74,
	Detail_easyar_SceneMesh__retain_m699B969FF74965066720A6E1379E07EE1003BCA4,
	Detail_easyar_SceneMesh__typeName_m062970E6BA013F524EFAB10B674E22604A0D384D,
	Detail_easyar_ARCoreCameraDevice__ctor_mF041E2358FC8848BD9249A716C836EA3308C9415,
	Detail_easyar_ARCoreCameraDevice_isAvailable_mDF4BD4ADDCB037F46DEB5D671A84E5B1E8508728,
	Detail_easyar_ARCoreCameraDevice_bufferCapacity_m8876F63E9DAEAE42BC7B5F7541FB4A8EA60F8FA7,
	Detail_easyar_ARCoreCameraDevice_setBufferCapacity_mC271498B411FD3B5ED0585DDF45B9986F130DB7C,
	Detail_easyar_ARCoreCameraDevice_inputFrameSource_mBD5A4FBEBC39D52753DC7D3BB5A2A0DDE8D97C48,
	Detail_easyar_ARCoreCameraDevice_start_m6D408345CBF7AE734DA51ECE025524D476778B65,
	Detail_easyar_ARCoreCameraDevice_stop_m82518F9ECD903935B0FFD65584DD95E2FC031F38,
	Detail_easyar_ARCoreCameraDevice_close_m07FD2564070029FA20F2D98C3E46CEF7550FF1F0,
	Detail_easyar_ARCoreCameraDevice__dtor_mC723B273096D8E7263B0C62FDA9D39F7C820CEBD,
	Detail_easyar_ARCoreCameraDevice__retain_mA5F8B64EFEDAEC890E4B48F6E74FC1E3FC8F8AAF,
	Detail_easyar_ARCoreCameraDevice__typeName_m365F944E3E1D9B57CC2C3137B02D8D206789A17D,
	Detail_easyar_ARKitCameraDevice__ctor_mB005EF0C3884D15DDA0CD1FF87E91E2FB76E565B,
	Detail_easyar_ARKitCameraDevice_isAvailable_m728912D71EE6B429E2B0935AF9889518D988A8E1,
	Detail_easyar_ARKitCameraDevice_bufferCapacity_mE13C80476A93018A178DB2B6B9B7E6573B714019,
	Detail_easyar_ARKitCameraDevice_setBufferCapacity_m4FAF19CA35BBB7FC9063AA0E4653E93746A8933F,
	Detail_easyar_ARKitCameraDevice_inputFrameSource_m09BABEAB27CE82AB6D1D8F81C20432EAD0A67C93,
	Detail_easyar_ARKitCameraDevice_start_m50DD4DC91BD795409C96C380D5CB758581B1ED5F,
	Detail_easyar_ARKitCameraDevice_stop_m2695FB2BA14A33EB3C1AB019B4BF8C730065A001,
	Detail_easyar_ARKitCameraDevice_close_m965DA4D2EFA95381ED62742330C096EED9BB5496,
	Detail_easyar_ARKitCameraDevice__dtor_mA241E782DF73ABDF0C9772F3A1CE918D40B1CEDF,
	Detail_easyar_ARKitCameraDevice__retain_mC4F75C91C6EC58A5E5851678392A2E0FDD4C4309,
	Detail_easyar_ARKitCameraDevice__typeName_m0930E7FFEE2ADFA99EFBCDC29A444EDCA277EE0E,
	Detail_easyar_CameraDevice__ctor_mCAEF6546126A26E483A7F86313C384458CFD26D3,
	Detail_easyar_CameraDevice_isAvailable_mE5F6CF9D2FDC6A2130202055596617F21B8C866E,
	Detail_easyar_CameraDevice_androidCameraApiType_mC9B94958D0A9E0CAE5D0657CD8DA0BA1610F9220,
	Detail_easyar_CameraDevice_setAndroidCameraApiType_mBA3FFFEAEAF808AE108F325AA5C93DFA72F5761D,
	Detail_easyar_CameraDevice_bufferCapacity_mC1F425A9870F9910EA3D4F0CB480068468CE98B7,
	Detail_easyar_CameraDevice_setBufferCapacity_m63ECF1CA20E8869C114C10B1E6ACB38A0B132AD0,
	Detail_easyar_CameraDevice_inputFrameSource_mC7EFD9F8354193555B7962E50BF00A961AE3EDE3,
	Detail_easyar_CameraDevice_setStateChangedCallback_mC67B2368FEEE78BD1C0E26BF3D8C077458615FF5,
	Detail_easyar_CameraDevice_requestPermissions_m5CA647FE34E538475F3AEC1F7C652DD8BBDEFBEE,
	Detail_easyar_CameraDevice_cameraCount_m573F869ADC1664E313B91413B298F66DC1768D3F,
	Detail_easyar_CameraDevice_openWithIndex_mB92C2803491AFCE5B2103E207373850A11542C38,
	Detail_easyar_CameraDevice_openWithSpecificType_mD19C4159409021B8B0AF8F293AED9CDBEE4468F7,
	Detail_easyar_CameraDevice_openWithPreferredType_mD11E937D22D02062D189A439A33353A3BC3038CA,
	Detail_easyar_CameraDevice_start_m7E82D941D8794EAB789CE54B377C97F335800C02,
	Detail_easyar_CameraDevice_stop_mC396C33B088B5FF1E812703E9C21428632556A58,
	Detail_easyar_CameraDevice_close_mEB3148B502BA623BBCD783A5C86EC49599BC2B61,
	Detail_easyar_CameraDevice_index_mDF08C8D17C10CB37EC51FE6635BF8919EBDDC911,
	Detail_easyar_CameraDevice_type_m44D8DCCC8AD226353496833C80F505A4C49BBA71,
	Detail_easyar_CameraDevice_cameraParameters_mC9BCF56B66EACE46F11E2A716A8AE4AD4C82DBF9,
	Detail_easyar_CameraDevice_setCameraParameters_m639F9E6B2503DE5F01F298F59A56AE8382F383CD,
	Detail_easyar_CameraDevice_size_m63CEC1B0CDFECA410B2DA9D3C3A0D4E2668B73E1,
	Detail_easyar_CameraDevice_supportedSizeCount_m29FB184A2ED62E7FE948765915EC6A244CBB02E0,
	Detail_easyar_CameraDevice_supportedSize_m49E1788526919816AA63CD5CCBE699DB53BC1F2D,
	Detail_easyar_CameraDevice_setSize_mB2D2138092AAD12752E50D3697C5938B8EC6ABCA,
	Detail_easyar_CameraDevice_supportedFrameRateRangeCount_m45953AF89503167511EE1931DABB369A1ED130BB,
	Detail_easyar_CameraDevice_supportedFrameRateRangeLower_m6DABBAC14B9DA2C1B0705055AEB725E58C2DE4DF,
	Detail_easyar_CameraDevice_supportedFrameRateRangeUpper_m8B047198AA417DFDA4625577A20C1D995ABA9C43,
	Detail_easyar_CameraDevice_frameRateRange_mACAAED8B6F89437B275A0D78DCABE3A55DA95436,
	Detail_easyar_CameraDevice_setFrameRateRange_m7209FDB07D9E10779657F4380190D1005A438495,
	Detail_easyar_CameraDevice_setFlashTorchMode_mEBDF0F54D320E12286419A18AC7DCE3FBE400C27,
	Detail_easyar_CameraDevice_setFocusMode_m2BAFC33D60AA711A3D91AEB6BD5CB4855E59CEE2,
	Detail_easyar_CameraDevice_autoFocus_mF6C736D44A063B5339B9A844C5F1F6127E783C25,
	Detail_easyar_CameraDevice__dtor_m319B41AA502EF8ECEAC072BCBBD53BF00233BF89,
	Detail_easyar_CameraDevice__retain_m6B1B71563F85CB8E27E21758C2CDB40AF8844FFC,
	Detail_easyar_CameraDevice__typeName_m98899A725F80356175A6D0415339965A7897C140,
	Detail_easyar_CameraDeviceSelector_getAndroidCameraApiType_m80C6848F6842A1AF77EAF4D40C1CAD52292F0098,
	Detail_easyar_CameraDeviceSelector_createCameraDevice_m8519645B4F70B544A46F8B2E384D170026F868D0,
	Detail_easyar_SurfaceTrackerResult_transform_mB469AF0B3C4C58C777C106629F603A90C2D1D0F5,
	Detail_easyar_SurfaceTrackerResult__dtor_mC026985AED62834EBEF664E9219AF72C075181C5,
	Detail_easyar_SurfaceTrackerResult__retain_m7D75E32E90DFB913B74663611D7AF909A2252E46,
	Detail_easyar_SurfaceTrackerResult__typeName_m1618FDE48322CCEF0A3C8AE42BA7B383607F73A6,
	Detail_easyar_castSurfaceTrackerResultToFrameFilterResult_m6EE939A2FD70F27A131BE0FC214C11F575CAF980,
	Detail_easyar_tryCastFrameFilterResultToSurfaceTrackerResult_m64500CA5EE64FC312DB94FF76D008946994D10AF,
	Detail_easyar_SurfaceTracker_isAvailable_m2FF4155CD76139B7A361AE0F0DDD8B34F2BD3088,
	Detail_easyar_SurfaceTracker_inputFrameSink_m439D99A6C3831F218F1882CC4E9F96924F237CDC,
	Detail_easyar_SurfaceTracker_bufferRequirement_m3BC4B6A7F57F089A815DD98BBE381F985E432832,
	Detail_easyar_SurfaceTracker_outputFrameSource_mA89448CB827FF79598A3F699E60EB5667C12151D,
	Detail_easyar_SurfaceTracker_create_m8798194BE3B3CAD4629B3DA73B8203CD5313E2F5,
	Detail_easyar_SurfaceTracker_start_m3632EE50B010D950B34760ED25DFBE7C8F5DAE56,
	Detail_easyar_SurfaceTracker_stop_m00F6BD4D467E3A0C6235A8B2A79D531919B47675,
	Detail_easyar_SurfaceTracker_close_m9EA9D0C4E5F5C0B153CD1B95BE9975816138842B,
	Detail_easyar_SurfaceTracker_alignTargetToCameraImagePoint_m6615E4848857425B9C9880F8BB61DE8AD3D18EEB,
	Detail_easyar_SurfaceTracker__dtor_m76A9D8D157910119DC0FED074F163B990612245D,
	Detail_easyar_SurfaceTracker__retain_m01F734604D25093345C39A539DD48921B52C4963,
	Detail_easyar_SurfaceTracker__typeName_mC7416FD42A20100AAA590AB22AA36B363861113C,
	Detail_easyar_MotionTrackerCameraDevice__ctor_m727AFE139E44C0F8FA124E4C1DEDD915C8F6144C,
	Detail_easyar_MotionTrackerCameraDevice_isAvailable_m11935476BF31F015CC0EBB722CD5DD89DF4F9677,
	Detail_easyar_MotionTrackerCameraDevice_setBufferCapacity_mD6286FC8A82DDC0BC0282DE8EEA3A1C6A2C78CF0,
	Detail_easyar_MotionTrackerCameraDevice_bufferCapacity_mECBFD2CF994CF5AB577D706AB1736BCB4247D2E5,
	Detail_easyar_MotionTrackerCameraDevice_inputFrameSource_mD02985EF51D4FB5591ADA1451FE23854485E589F,
	Detail_easyar_MotionTrackerCameraDevice_start_m5C753369DD4D5F88A6EE0907D2538A29268632A6,
	Detail_easyar_MotionTrackerCameraDevice_stop_m6A6A67C41BB1EC0B66D7582EF76390ADE6BD27E9,
	Detail_easyar_MotionTrackerCameraDevice_close_mA806BAAF3DC409249442750B7F976A9496F2F237,
	Detail_easyar_MotionTrackerCameraDevice_hitTestAgainstPointCloud_m2615937C9D4E0FD2A6996BC5A14EB7D3286B7499,
	Detail_easyar_MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane_mCDCC4A6FF6E05CBEFF28294B66B306EBF0E8504D,
	Detail_easyar_MotionTrackerCameraDevice_getLocalPointsCloud_m402ADA30ABA8FEF9DDE44F1E4E218DF451DA366A,
	Detail_easyar_MotionTrackerCameraDevice__dtor_m10089B71168DE0A04FA9080B9A878E8389B65FA1,
	Detail_easyar_MotionTrackerCameraDevice__retain_m3D8DA1CB6BAF93E04FB497C064F6550BC483F24E,
	Detail_easyar_MotionTrackerCameraDevice__typeName_m1F265E35FDFAAC3082FF00628F527CF5E2DED76C,
	Detail_easyar_InputFrameRecorder_input_m4DF5EA651DD63FF730B46F1515BA67DEB9E3B913,
	Detail_easyar_InputFrameRecorder_bufferRequirement_mB7D13488EA5654EF22F5106D46BDA0326E2B22E1,
	Detail_easyar_InputFrameRecorder_output_m1E58748CDF7F645169098ED99C7CEB424C094A6A,
	Detail_easyar_InputFrameRecorder_create_mE14DFE318C99B74E1E2601AAC6A4D78EB7D83A63,
	Detail_easyar_InputFrameRecorder_start_m129C2DB0BCD918108664A14FBA73BDAD770448A7,
	Detail_easyar_InputFrameRecorder_stop_mBBB30DAFE8CC60DB610552C5C9B77EFBD122B26B,
	Detail_easyar_InputFrameRecorder__dtor_mA78CC2A380192D4AD7B22FFC728464A88253ADB3,
	Detail_easyar_InputFrameRecorder__retain_mE64F882B0351DD34AE2DC60F36A89AB7068B410B,
	Detail_easyar_InputFrameRecorder__typeName_m1B0856D5521974DBCDB5DCF5F6C47D680B8D6F6C,
	Detail_easyar_InputFramePlayer_output_mC15B8AE878D3E41ACE224F83BFC8CB31B31BE72D,
	Detail_easyar_InputFramePlayer_create_m0088A6280D1CE162ECDCF454A0765CE28E91E78F,
	Detail_easyar_InputFramePlayer_start_m5D03AB385C83D39ABF3960F7450CFFBF1252B11E,
	Detail_easyar_InputFramePlayer_stop_m239E9A83AFEA6B2333194545758A46980A1F690E,
	Detail_easyar_InputFramePlayer__dtor_mBD411AE1282F018AD0FBB15C1695929BD13AA207,
	Detail_easyar_InputFramePlayer__retain_mC3DC2B40381EEC24EB7CBE23CBC891AF862A3EB4,
	Detail_easyar_InputFramePlayer__typeName_mB49001916B9F8B326E8E11244BADFD475F5F2C62,
	Detail_easyar_CallbackScheduler__dtor_m39A9A64F0E26372CD4CFA620A15B513A6939ECC4,
	Detail_easyar_CallbackScheduler__retain_m59574AAE7E5827D420F1BD8CC54B83813853C967,
	Detail_easyar_CallbackScheduler__typeName_m589AD2E346AECFD9FE2566543D67C05B23BC212E,
	Detail_easyar_DelayedCallbackScheduler__ctor_mCE7004AAAEC9B2204F4E0A4A7B93BC4D32AC3895,
	Detail_easyar_DelayedCallbackScheduler_runOne_m18EB4C83D69AA1FD9FFA85D93DFB67DCEFC205D5,
	Detail_easyar_DelayedCallbackScheduler__dtor_mD3162726C09A6D43155C90A3D3D218FD8BC0C699,
	Detail_easyar_DelayedCallbackScheduler__retain_mFF35A479B23047015E74EA22E2640AACA1FCCC29,
	Detail_easyar_DelayedCallbackScheduler__typeName_mD4FB80FDA72EF13B63C0B9AC3EA6DEA6075921A7,
	Detail_easyar_castDelayedCallbackSchedulerToCallbackScheduler_m6BD6EA3BAD85FCA04110F265EAF896E3DF31D8F2,
	Detail_easyar_tryCastCallbackSchedulerToDelayedCallbackScheduler_m3E5C68AB79D6417546ACE3D646841CB988A198B6,
	Detail_easyar_ImmediateCallbackScheduler_getDefault_mB6E326D99CDDE6B0FA24905A904AD3093E05E78F,
	Detail_easyar_ImmediateCallbackScheduler__dtor_mEC04B287FDE94C50A5881DB0EC3A0BC5E1553B5B,
	Detail_easyar_ImmediateCallbackScheduler__retain_m5ACE1BA6B7C5B3B158B9EFB4A915E9EC2108B1EE,
	Detail_easyar_ImmediateCallbackScheduler__typeName_m510E9DB875B1EA0ABA963B9934DBB89CBDEA12E5,
	Detail_easyar_castImmediateCallbackSchedulerToCallbackScheduler_m93B607CC756DD2DF4AEE9C92EBE90EF49C9AA9C3,
	Detail_easyar_tryCastCallbackSchedulerToImmediateCallbackScheduler_m80CAF6FE699552BA8B3D5708706F727CF1F8B99D,
	Detail_easyar_JniUtility_wrapByteArray_mBFB61F02352E4E38F5E03CECCC247E3D39C63009,
	Detail_easyar_JniUtility_wrapBuffer_m23B098C2216F4392A189F3DE60CED83E9C638881,
	Detail_easyar_JniUtility_getDirectBufferAddress_m717A78D004765AD9875C04FB65BB2A027B161DF0,
	Detail_easyar_Log_setLogFunc_m0F7BEC0E5EC412963E1BF658F5233DEBEAD0413A,
	Detail_easyar_Log_resetLogFunc_mF108666AD1FE27C32762DDCF0F66E9396ED62056,
	Detail_easyar_ImageTargetParameters__ctor_m07EA26F6A43A4427F50C5D4FEA79D0B7C0286795,
	Detail_easyar_ImageTargetParameters_image_m18D709A7A9CF42526361A9DAA5877C02BA1F08F6,
	Detail_easyar_ImageTargetParameters_setImage_mCDFA374AFB674242968FBD9B3FA234C2204B095B,
	Detail_easyar_ImageTargetParameters_name_mDBA638B6015EB0EA1509E0316B062F31491D52AB,
	Detail_easyar_ImageTargetParameters_setName_m00A3CCEED833F24E0D7481152F2F1ED1350E071C,
	Detail_easyar_ImageTargetParameters_uid_mCCDAEF5C225C683736319CECB07BFE4014DC9EE5,
	Detail_easyar_ImageTargetParameters_setUid_m7A0FE5BEA22869DD38EEC1DB57F911F06A93A13F,
	Detail_easyar_ImageTargetParameters_meta_mCA5C4D2E22345C94D47AFE773A9F395687F02B0C,
	Detail_easyar_ImageTargetParameters_setMeta_m097B78195DE1981666D01450AA0C6E38758A8407,
	Detail_easyar_ImageTargetParameters_scale_m0F89AC8F65FA8F1842C389FEC472D30DF0101F01,
	Detail_easyar_ImageTargetParameters_setScale_m91E9A5B3D1BF05BEFE30DAF95BACDF275BD29D55,
	Detail_easyar_ImageTargetParameters__dtor_m73204E67E13266A6A83DA0FE9368552ED4744ECF,
	Detail_easyar_ImageTargetParameters__retain_mB2257E256B08A3009EE18504CB74D3B9E04D7201,
	Detail_easyar_ImageTargetParameters__typeName_m410E0114BDCFFDF453D822582129F214CE7A9893,
	Detail_easyar_ImageTarget__ctor_m1D31736328E5CE1B9DA2A719771074734311913A,
	Detail_easyar_ImageTarget_createFromParameters_mDA802D0EE55BB2932228D9C8522170A180FE9C36,
	Detail_easyar_ImageTarget_createFromTargetFile_mBE0906FD2293EB267293C86375912385F76CDA8C,
	Detail_easyar_ImageTarget_createFromTargetData_mEA4C0D44CDFDCFD96B0AD6CAED28A2732F53D709,
	Detail_easyar_ImageTarget_save_m4DBDFEB7F0F7FBEA9B10186C104FF93B89EBB4F7,
	Detail_easyar_ImageTarget_createFromImageFile_m25DBB8BAA15342E04A1A8AB216770A78427E194C,
	Detail_easyar_ImageTarget_scale_m37DDF62084A00D11B1AA2A435B73E718E27189F0,
	Detail_easyar_ImageTarget_aspectRatio_mFDCAAAA4EB71F5BCADC496AA597F0F6E4C4374A0,
	Detail_easyar_ImageTarget_setScale_m31DBE7CF282B57FAA330C1AC88ED77A11060E0F9,
	Detail_easyar_ImageTarget_images_m3065F749142E71E38CA219E241DE792AFBA5255E,
	Detail_easyar_ImageTarget_runtimeID_m231959CD0BD8A442C33FD24836DE1EF35FD5E039,
	Detail_easyar_ImageTarget_uid_mC391F9C90A0DC84A1186A93A8D0FB29C8700C730,
	Detail_easyar_ImageTarget_name_m52C44A4433B3FD46BCAAFA76C4CAC1D97B5482A3,
	Detail_easyar_ImageTarget_setName_mC0B17313579AC6982D66109F40450B3A15F76555,
	Detail_easyar_ImageTarget_meta_m1DC517F07EE2CCD451FB5472D6F5F343B951092C,
	Detail_easyar_ImageTarget_setMeta_mC47A73EDAA02B64C8D68DB35FE985B206BE546FB,
	Detail_easyar_ImageTarget__dtor_m2A9CC00D5D8DCBFFAC4A364C5F36F3A6044C3048,
	Detail_easyar_ImageTarget__retain_m6969FD3418520D472A8718EC0D9AFBA2E151350C,
	Detail_easyar_ImageTarget__typeName_mBABA1715E945AC812765AB36127646F2E40819CB,
	Detail_easyar_castImageTargetToTarget_mCABFFC495B982E061C104B885357159964227B62,
	Detail_easyar_tryCastTargetToImageTarget_m2262F03D8D5A9555DE3B139230F06283A43795B1,
	Detail_easyar_ImageTrackerResult_targetInstances_mE6517822C58A0479639A0BFCCDFF0615CFB9A38A,
	Detail_easyar_ImageTrackerResult_setTargetInstances_mBBE18B81784AEF92052EC6710FBB57116656FEAD,
	Detail_easyar_ImageTrackerResult__dtor_m48F0CB7BA0E6FE7E6A7026B24AF0EC75D8D5851D,
	Detail_easyar_ImageTrackerResult__retain_m4326C7C76054BF057A65B73DECB88A456D0CB99C,
	Detail_easyar_ImageTrackerResult__typeName_m570F945451FA52682DF48BB454B4D0315142C191,
	Detail_easyar_castImageTrackerResultToFrameFilterResult_mE49F1B4F18207CC75543983BBDB72D6FF579147E,
	Detail_easyar_tryCastFrameFilterResultToImageTrackerResult_mC384349FEA6E77BD805025A7966FCC628DD2E4CB,
	Detail_easyar_castImageTrackerResultToTargetTrackerResult_mBC8FA0475039B62596EDFF8E6667568DE43CF4EA,
	Detail_easyar_tryCastTargetTrackerResultToImageTrackerResult_mBC61FD5B5A7DAF8B977F84C291BBC6D504EE8980,
	Detail_easyar_ImageTracker_isAvailable_mFDF49609CED26A1192F209314161E12435E111BB,
	Detail_easyar_ImageTracker_feedbackFrameSink_mDBE3C6E0E2A62BAB2AD88790FD70336A611F0220,
	Detail_easyar_ImageTracker_bufferRequirement_m7D83DDCEE8C34E6D1F9418E19F7E8A1F470E2536,
	Detail_easyar_ImageTracker_outputFrameSource_mB7B57E3569E4E9BD8135023F761C30B34000FE97,
	Detail_easyar_ImageTracker_create_mC17DF89AB686F97795E29063227937B62FCE36D7,
	Detail_easyar_ImageTracker_createWithMode_m256BF2CE20BCE0C03340A64638DC12F285C06DE2,
	Detail_easyar_ImageTracker_start_m2CCA8146137AE864AEB2C80A2570E02E7443C167,
	Detail_easyar_ImageTracker_stop_m61D194D78336A9DAC86A5E3B73597F2E98E38303,
	Detail_easyar_ImageTracker_close_mDB08F5DB6C72044E0A81849B147B644353CD2C1D,
	Detail_easyar_ImageTracker_loadTarget_mDD8FFDD025C4A80848B7AC3A054EF4DFE6274954,
	Detail_easyar_ImageTracker_unloadTarget_mB22583F78FA5E1CA9806E751C3071145FCB82A8B,
	Detail_easyar_ImageTracker_targets_m17A2856752052EA565275FF125417C5354B07523,
	Detail_easyar_ImageTracker_setSimultaneousNum_mAB38DCB8815F6B1E914050496AEF8F1CAD814E91,
	Detail_easyar_ImageTracker_simultaneousNum_mC1A177C42982A322962473728673329D62030F0D,
	Detail_easyar_ImageTracker__dtor_m3B1962AFEA49D55ADCC1E3B292401D74D36ECA81,
	Detail_easyar_ImageTracker__retain_m3B905A373405AFA5BE6B6D9DCB3C2FFC44397607,
	Detail_easyar_ImageTracker__typeName_m487BBB249F87EF8CBB9E93F9FDB7F36FAF15994D,
	Detail_easyar_Recorder_isAvailable_mABFBC0B3318CFA1163AC1B0E007F1ECA97249E1A,
	Detail_easyar_Recorder_requestPermissions_m0051F92400D7D4470C82CAAE109A767571951B66,
	Detail_easyar_Recorder_create_m5017A3FB6CECB470B58E32B9E742378CB0CA04AB,
	Detail_easyar_Recorder_start_m5418CCBB8102CE237126E7D15B21A1ECE92C296E,
	Detail_easyar_Recorder_updateFrame_mDC603271B63D4D2696C8671B56E7282741B3ADF8,
	Detail_easyar_Recorder_stop_m3EF9F5327BF0BF4F0378EB50D6705D27E765F897,
	Detail_easyar_Recorder__dtor_m59E502E1952E26FCEFFDFC800EA4777A6456AD47,
	Detail_easyar_Recorder__retain_mDA5DD9E705C7EF4C17FD0E50DD2D91FE99EB90F6,
	Detail_easyar_Recorder__typeName_mB899C4101DB817E36B295B465FE4563E32F12C83,
	Detail_easyar_RecorderConfiguration__ctor_m83F7E91103393B1D27E393671466CD4212EF1C13,
	Detail_easyar_RecorderConfiguration_setOutputFile_m4039B7C8C3BCD9B9F1B59C3A0689AE2AF68C7757,
	Detail_easyar_RecorderConfiguration_setProfile_m737B2C6BDA43EA326D7D8825B72BBBEF9F8B6909,
	Detail_easyar_RecorderConfiguration_setVideoSize_mB5907CF4CA46DD8FC87FDC2900C35656BFF9EB65,
	Detail_easyar_RecorderConfiguration_setVideoBitrate_m84F864E885B9560A73EAAF7E6B903AFD4D1D1D64,
	Detail_easyar_RecorderConfiguration_setChannelCount_m8416CA980C8D37DCA43A98CC61CB32AF72061FB9,
	Detail_easyar_RecorderConfiguration_setAudioSampleRate_mDEC3C82685E330733FA73A53F74EDCED004AF06C,
	Detail_easyar_RecorderConfiguration_setAudioBitrate_m49F295D9DE19AC92898FAB96217273C99AC37339,
	Detail_easyar_RecorderConfiguration_setVideoOrientation_m760ABDFCD7E3C8756AA25175A84CD7BCDCA38C7C,
	Detail_easyar_RecorderConfiguration_setZoomMode_m1A533CAF9BE63D981075A9D6B4465A06B453586A,
	Detail_easyar_RecorderConfiguration__dtor_m746F06BA71001E7B7F1727323185B526D5F9A0C3,
	Detail_easyar_RecorderConfiguration__retain_m0B635CEAA01D7CCF6CCCE86F82D1C26DF542E7D6,
	Detail_easyar_RecorderConfiguration__typeName_m31F5F037D048C52716EF9F8BC83F42A157982284,
	Detail_easyar_SparseSpatialMapResult_getMotionTrackingStatus_mAB061EE73B15AB2D4E017AB8127E9C2D3D45D699,
	Detail_easyar_SparseSpatialMapResult_getVioPose_m790F1ED09101C2EE9C7E92927EA8D38EB5111E62,
	Detail_easyar_SparseSpatialMapResult_getMapPose_m6FE588A2EED5E126409077CE37F5AEF4E9467FF5,
	Detail_easyar_SparseSpatialMapResult_getLocalizationStatus_m78A2568B562BBD8CB0760D9956EBECEB1791C1DB,
	Detail_easyar_SparseSpatialMapResult_getLocalizationMapID_m4F7CEB84A0078EA54A0A6EA52A1B9DAE0764F041,
	Detail_easyar_SparseSpatialMapResult__dtor_mC64C6F5FABBBA713566E9932A812C23DDD6D7FFF,
	Detail_easyar_SparseSpatialMapResult__retain_m77A6D8E86722F9A3170293759A26A762EAB8D339,
	Detail_easyar_SparseSpatialMapResult__typeName_m50AB472D4C5548D749F11BB4FB8108D82C6619DC,
	Detail_easyar_castSparseSpatialMapResultToFrameFilterResult_mA0630037A3252A0BE9DF46A8D5B99B7173FAA218,
	Detail_easyar_tryCastFrameFilterResultToSparseSpatialMapResult_mE4AAA81F87961B85C46CA4051F101864C6CCCED6,
	Detail_easyar_PlaneData__ctor_mBD260DBC79506DDE92C0FAAC460AC118F0925D9A,
	Detail_easyar_PlaneData_getType_m3F35A1CA31353882786C0EAB649F0F0869E0B9FA,
	Detail_easyar_PlaneData_getPose_mC58DCF2285D99C31F78B519211AC53DEDAD41A62,
	Detail_easyar_PlaneData_getExtentX_mABADEE0381B9075F498943F5009619E2CABE5823,
	Detail_easyar_PlaneData_getExtentZ_m81A27006E91DD8544B0F9108F7B5477711EA8107,
	Detail_easyar_PlaneData__dtor_mE8127645EE251FBCCA69FF6A426451B914019674,
	Detail_easyar_PlaneData__retain_m696401005DF6C6492E0B4BF02C823C365FB6072B,
	Detail_easyar_PlaneData__typeName_mEBAB29BB70F4409000DE1AF945F7A1A806396300,
	Detail_easyar_SparseSpatialMapConfig__ctor_m9F3A92613C57D391B1F7E03B5F99D46F17C62679,
	Detail_easyar_SparseSpatialMapConfig_setLocalizationMode_m6E74B91D6176F40F4C5622BDA2A0D890D541A64E,
	Detail_easyar_SparseSpatialMapConfig_getLocalizationMode_m02FAF2A5160A4FDF325B5DDFE775D7D096C769D1,
	Detail_easyar_SparseSpatialMapConfig__dtor_mB4D3F34C103BC1761AC41AA2C4A6FC75E940E9DE,
	Detail_easyar_SparseSpatialMapConfig__retain_m5B86733D757F05FCBD0553C604C098F25455CCB0,
	Detail_easyar_SparseSpatialMapConfig__typeName_m671ACDBA92CD2F07547A47B9B7E19DC2F22D7F2D,
	Detail_easyar_SparseSpatialMap_isAvailable_m9F68231D6439515FA65C28C21D22DE267C10B9ED,
	Detail_easyar_SparseSpatialMap_inputFrameSink_m7A365ECD8C7D557B6FA6D06E263C5BFCE5B5757B,
	Detail_easyar_SparseSpatialMap_bufferRequirement_mD8A35806FCFD18870F30D9694BCE4C90D484AA04,
	Detail_easyar_SparseSpatialMap_outputFrameSource_m88DCC7262DF68FB197C72A692E42FA5B46BEA782,
	Detail_easyar_SparseSpatialMap_create_mA3956F8DE7E06EEDDBA193363C810D345194D01C,
	Detail_easyar_SparseSpatialMap_start_mD0E087EAF96D92A7C349EB9C9DABF18D73941ECB,
	Detail_easyar_SparseSpatialMap_stop_mAFAF04DC0B95E1F666B2724F2AA3FA61662F4B43,
	Detail_easyar_SparseSpatialMap_close_m6604C4EE0EF0B619911FFD82D15FAE291940C3A8,
	Detail_easyar_SparseSpatialMap_getPointCloudBuffer_m32AFD1A7D5F0BBAA8E11CA9AAD95C1FD856A9195,
	Detail_easyar_SparseSpatialMap_getMapPlanes_m4274383D525033474BF84B319C25A3C71B3EBA76,
	Detail_easyar_SparseSpatialMap_hitTestAgainstPointCloud_m0BF4FC9A097615A81D0A10F978A8B8F87D926B1A,
	Detail_easyar_SparseSpatialMap_hitTestAgainstPlanes_mDB1F174000961146617515AA043938ECC80D9290,
	Detail_easyar_SparseSpatialMap_getMapVersion_mEFF3B1EA573D58EA84CA87EBD97433068EB54C26,
	Detail_easyar_SparseSpatialMap_unloadMap_m6ABF26B1DE14E121726C70C83BA75F73EAE19788,
	Detail_easyar_SparseSpatialMap_setConfig_m5924B3AF77BEFFF8C58485D58A3A0B3ACE71015F,
	Detail_easyar_SparseSpatialMap_getConfig_mEEC500AA97B5AFDEBF649D01EA047667F1A7541B,
	Detail_easyar_SparseSpatialMap_startLocalization_m0D72783D2F348205EAD1B75C5A4DD34139A3E2F9,
	Detail_easyar_SparseSpatialMap_stopLocalization_m00F4450C3365F672C51B569DE87A89D5E222A5A2,
	Detail_easyar_SparseSpatialMap__dtor_m476B6F81CA292BEF62B626CD36B03AD9DF460542,
	Detail_easyar_SparseSpatialMap__retain_m7B4D289355B3EA1F4407AD6E149C7A3AB11A4213,
	Detail_easyar_SparseSpatialMap__typeName_mBA177B67D9752789818344F687DFDF76F6E2A230,
	Detail_easyar_SparseSpatialMapManager_isAvailable_m5AED9DA580C9C4A50E9AE38A2FE72A990CEC5163,
	Detail_easyar_SparseSpatialMapManager_create_mEC6DD3790B7AEA370F794BA74B458929B044669D,
	Detail_easyar_SparseSpatialMapManager_host_m3E2A42940E6B9A6E7970E94778FF9796C6E1D210,
	Detail_easyar_SparseSpatialMapManager_load_mAC0B59BC8BA33BAB3046BC7A6475FEE1E591021E,
	Detail_easyar_SparseSpatialMapManager_clear_mA778B45891681B199A32BB40C230E021E39C757C,
	Detail_easyar_SparseSpatialMapManager__dtor_mF098DEA7BEFD6308360A548BD4E64688EEADE9ED,
	Detail_easyar_SparseSpatialMapManager__retain_mDEFA6F3B61263FD1B892D03EE5CE1B7365FA7475,
	Detail_easyar_SparseSpatialMapManager__typeName_m4B418EC06537315A990E0C72AFF140FB4B023A9A,
	Detail_easyar_Engine_schemaHash_m3DF9D806291E1AF6E3AFA825D3019C84B6F2D2D9,
	Detail_easyar_Engine_initialize_m1B206FCD51B5A2389118D354B5BE97EEAB16A946,
	Detail_easyar_Engine_onPause_m510DA8D4E5547681E1F52B69117F95459815DEDE,
	Detail_easyar_Engine_onResume_m2DBC1F52423C89E5845B9877B585555E89F993E2,
	Detail_easyar_Engine_errorMessage_m1F7BC10BDBEE1BF954422FAB65B35D3209023897,
	Detail_easyar_Engine_versionString_m2AFE64E3508110755A5A96A023030FD929DEE7C1,
	Detail_easyar_Engine_name_m838240CA3D078DB6AE7E6B499AF66F477970CD4D,
	Detail_easyar_VideoPlayer__ctor_m8DB6F5060D412234135EE6EB135FEF51BB1A7F99,
	Detail_easyar_VideoPlayer_isAvailable_mBB1E4AD220CB8DA6D99D5A70365B670622FADDC8,
	Detail_easyar_VideoPlayer_setVideoType_mAF86B1646CD8C60F5EC390386204C06A37C4A187,
	Detail_easyar_VideoPlayer_setRenderTexture_m2015F5AAB35DB4E98C599AD9DB11DBF8C0102DBA,
	Detail_easyar_VideoPlayer_open_m2A01D59651B7C4D1894357513BFD5B1EC5B637D7,
	Detail_easyar_VideoPlayer_close_mFA9DFD14E739BE7B22B5F621B0303C185B7B4438,
	Detail_easyar_VideoPlayer_play_mBD6A25586E24FC14B7D2105B52C8456CEEE5991D,
	Detail_easyar_VideoPlayer_stop_m712F76BC80B2CB302A9D595EACF04304D7E0111B,
	Detail_easyar_VideoPlayer_pause_mA535E91E2C2147FD378A78ECA6A60BAEDA8F1BE9,
	Detail_easyar_VideoPlayer_isRenderTextureAvailable_m021A763AF0B8EEEDE57E584800AAD544FB0DCF1C,
	Detail_easyar_VideoPlayer_updateFrame_mEE526A47314268668CDED799A3E1ABC16B8FE613,
	Detail_easyar_VideoPlayer_duration_m7E60163C83CF5DD24AF73904D893DDBCC24DD534,
	Detail_easyar_VideoPlayer_currentPosition_m8338D910A3BA1F32DE27D387ABCD9CBA97EBED51,
	Detail_easyar_VideoPlayer_seek_mC4047C70A2A10D0D1A8C49D26636302C8F569306,
	Detail_easyar_VideoPlayer_size_mAEB8FAB4F17A13C157EC69DA6AF586AF977B91F9,
	Detail_easyar_VideoPlayer_volume_mE5A63DE9CDB2374F511168416AF35C3808CD1787,
	Detail_easyar_VideoPlayer_setVolume_m93A43F5F747F432268155DF7CA05268535042B92,
	Detail_easyar_VideoPlayer__dtor_m675905043D8942B2DFF58102981551D5EA3ECE75,
	Detail_easyar_VideoPlayer__retain_m55D5DAF0EFD54A4407042ED24B3147495D205169,
	Detail_easyar_VideoPlayer__typeName_mBFB0DB515A4CF5322209B29B3A859C8307AEA57E,
	Detail_easyar_ImageHelper_decode_mB10833518D87B180F6E10E80F2142F347808B1A4,
	Detail_easyar_SignalSink_handle_m59C55E4C08E1D9C2A54B926A0703085953733F69,
	Detail_easyar_SignalSink__dtor_m1EAA5EBA1A3FCF410D8453B3442789EE2F52808E,
	Detail_easyar_SignalSink__retain_mDD8ADCFF1BF8DE82C0932E9B95F697C459ACC9E3,
	Detail_easyar_SignalSink__typeName_m085E47ABA9D76616AF51763D74D6C1598E258D47,
	Detail_easyar_SignalSource_setHandler_m11906B6C65CB059A2DC9149D2AC679A3749448E3,
	Detail_easyar_SignalSource_connect_mD365AE8AAE4B9B0ACC52347012854E163D0BFE2A,
	Detail_easyar_SignalSource_disconnect_mB76D8B59CA59F2ADC99B3121BBD753C29A634530,
	Detail_easyar_SignalSource__dtor_m1DF8F325175E7DCFCCAD45E262CBDCE9420947A0,
	Detail_easyar_SignalSource__retain_m44EFF16019E42BD54E6018AB06D0485EA3B3A66F,
	Detail_easyar_SignalSource__typeName_mE5F6E163FB1A2172D9A23F4B6D05B0D46C2AAAA9,
	Detail_easyar_InputFrameSink_handle_m943953C5D8A7EDD0E5EF660A85D23F2C1926A119,
	Detail_easyar_InputFrameSink__dtor_mAD97B7B952CCE5961FAB12E535E72975CE8C74D5,
	Detail_easyar_InputFrameSink__retain_m5EA5EB364A37D6790E92CB5DDFD5F02C34AE9890,
	Detail_easyar_InputFrameSink__typeName_mD355AA8BE0B99293A3EAEF4A00F2B10A4FF40526,
	Detail_easyar_InputFrameSource_setHandler_mD5B47AA1BF5B21CC60738101DC226EEEE285B2BC,
	Detail_easyar_InputFrameSource_connect_m50D337FA30D91967BEF23E271F8E3462192E5B17,
	Detail_easyar_InputFrameSource_disconnect_mD36F901268D2A0C6EAF47F11049EC94C64963B9B,
	Detail_easyar_InputFrameSource__dtor_mEC6F1C1DC3FBB9E9A91B23EEB8D7A330F2FE4A90,
	Detail_easyar_InputFrameSource__retain_mB8BFDCC9A385A57DB9FDC04DC4E588255CD64558,
	Detail_easyar_InputFrameSource__typeName_m235448F1D44FC1478AD8AD38A0FF9AF4C6DCB65F,
	Detail_easyar_OutputFrameSink_handle_m61EF117EFA29F64E32F8462D5D53B1F882E263D9,
	Detail_easyar_OutputFrameSink__dtor_m2A59348ECE0703732D4F63DFF5AACC1965B1E28C,
	Detail_easyar_OutputFrameSink__retain_m410F5275AAF508E68062DF49B989F1C5DF2AA8D1,
	Detail_easyar_OutputFrameSink__typeName_m777DB5216BF44D0EE4B87413A85290BB2688A832,
	Detail_easyar_OutputFrameSource_setHandler_mF916C237048E70CD619E066C53749BA4CAC0F071,
	Detail_easyar_OutputFrameSource_connect_m56A94759FD485ABB1C0EE8B68CF33440A31DD30C,
	Detail_easyar_OutputFrameSource_disconnect_mB9D89CC8AB2826F01AD3E78A88FBE5F489441A92,
	Detail_easyar_OutputFrameSource__dtor_mF26C029B73895B14611A4FB3C7BE96D482A349A4,
	Detail_easyar_OutputFrameSource__retain_mD0E3AB9A3F3B147048F4812E646E7C7E406EB47D,
	Detail_easyar_OutputFrameSource__typeName_m4620092BB5661AE4361B750595F3ACC8F07CD34C,
	Detail_easyar_FeedbackFrameSink_handle_m0982D545506B72DB5100C73CD85C14AFD912AEC4,
	Detail_easyar_FeedbackFrameSink__dtor_mCC50B2BA623A2520D00809EC1FBE846983C7E943,
	Detail_easyar_FeedbackFrameSink__retain_m94E4FD959D689BD4B4AEE7A117FF56EAA69043AB,
	Detail_easyar_FeedbackFrameSink__typeName_m6986CA386843FCE1FDD4A953FBAE5868F67626E9,
	Detail_easyar_FeedbackFrameSource_setHandler_m8A48547729EA48F2342C82205C0AC83F9F87D5B3,
	Detail_easyar_FeedbackFrameSource_connect_mD9C3552E5A6735D8E1E147D8B49D6321F5878C8B,
	Detail_easyar_FeedbackFrameSource_disconnect_m19AB8283EFC90B01227D0588805155DD154725CD,
	Detail_easyar_FeedbackFrameSource__dtor_m82E1278A8C104EA5615A6124797CDB892039D784,
	Detail_easyar_FeedbackFrameSource__retain_m9F20F0E00AFE6ECB579A770825B155C13619A6EC,
	Detail_easyar_FeedbackFrameSource__typeName_mC7A2586731DEE463AD8295AFA5B1E507F8F38D70,
	Detail_easyar_InputFrameFork_input_m75BC54404C49E9AD3E58CCD20DBEF0CF6EA4CF9D,
	Detail_easyar_InputFrameFork_output_m429A4BDA180260BD08F3E37A3ED0182A254D63BA,
	Detail_easyar_InputFrameFork_outputCount_m68DE7C184C49D978EA827A33D191B82B8B348A46,
	Detail_easyar_InputFrameFork_create_mB51397DEB0407EE85AB06C0783E25614CE05E6CA,
	Detail_easyar_InputFrameFork__dtor_mB11C14E73CF106599808C81A369E7A05C3CA8CB5,
	Detail_easyar_InputFrameFork__retain_m3A472A2C7B91F3097EE7604CFA9E932CAB6FF4AB,
	Detail_easyar_InputFrameFork__typeName_m69749BC507EE390CDC9F807FA4C737CD89139C7D,
	Detail_easyar_OutputFrameFork_input_mC00F10B4AEB705776080E62C0E94BBB294F4CDA9,
	Detail_easyar_OutputFrameFork_output_m941EFB0A715D67DE571A0820B94F35DAC01603C6,
	Detail_easyar_OutputFrameFork_outputCount_m63FB584C37220C95D2412C1C3C2F5F391B2CC0EE,
	Detail_easyar_OutputFrameFork_create_mD82771B6110F9BA1C1497F9C3BC208A5576A6826,
	Detail_easyar_OutputFrameFork__dtor_mF16AA7F272B4FCB3E34B481634F60ED1F039D843,
	Detail_easyar_OutputFrameFork__retain_m1238873C136FD8944AADE9AA9EAF9B33617E50FE,
	Detail_easyar_OutputFrameFork__typeName_mE0DFD6B0F498ECE18E0EFE4E08EBFB07EA2A0346,
	Detail_easyar_OutputFrameJoin_input_m0768708F703E20C97E3D7EC578E8192A91C6888B,
	Detail_easyar_OutputFrameJoin_output_m54D5B811DE78F5099A705994FB364DAB61E83471,
	Detail_easyar_OutputFrameJoin_inputCount_m78D1BCB78147ABE386567B33C5BF91C1593DFAE0,
	Detail_easyar_OutputFrameJoin_create_mDCAE2C3D9AC5D0531050D779587F1EB9C1E4700F,
	Detail_easyar_OutputFrameJoin_createWithJoiner_m6D78589C5C7066DA0BCF1002DBBA201A43BCA5F8,
	Detail_easyar_OutputFrameJoin__dtor_m4F2D7CBCFD9A941EFC9B0102430C407FA9758278,
	Detail_easyar_OutputFrameJoin__retain_m9F4E6F5C3BC95E72296D0D37E7F4994CBC294DBB,
	Detail_easyar_OutputFrameJoin__typeName_m941521B95BB435C4034C11F6F35D21A6A958C382,
	Detail_easyar_FeedbackFrameFork_input_mAC1715B80AE5C0AE1B179C32290457E92A5297E6,
	Detail_easyar_FeedbackFrameFork_output_mB8A98E389A093F8AB8E1FD87C905AAA5F5121D52,
	Detail_easyar_FeedbackFrameFork_outputCount_m6678A74BE0DB8C53E2D221CC3F3781835481C9CF,
	Detail_easyar_FeedbackFrameFork_create_m41022ECDD66B9EA57FF05B64DFC0C2EF434EFA36,
	Detail_easyar_FeedbackFrameFork__dtor_m1B9151B47EA7ECEF71B9543F4FD4A72E00455A5E,
	Detail_easyar_FeedbackFrameFork__retain_m2A1A5C9C82E121361FB8637463016FDF59F1B4E7,
	Detail_easyar_FeedbackFrameFork__typeName_mF66F90742986FC0D53449EFF8E78E8107B85E9C3,
	Detail_easyar_InputFrameThrottler_input_m65211FCF9DE14C6D7D7E82F52C0E23F8AD9A990D,
	Detail_easyar_InputFrameThrottler_bufferRequirement_m2E646DB937AB492C9B7DBEC86B2A9644CCF78BD1,
	Detail_easyar_InputFrameThrottler_output_mD6373EC8D5094D0FC5E2A393CD00AD30EF008A0B,
	Detail_easyar_InputFrameThrottler_signalInput_mE5EAA4C5514CD8514E32B7DAE2C5A5BC6261C27E,
	Detail_easyar_InputFrameThrottler_create_m0B278A373DD4CC90C4CD6631E4994675026E4C13,
	Detail_easyar_InputFrameThrottler__dtor_m7C1FC92C517D18A719681FAC99432845F65AFE30,
	Detail_easyar_InputFrameThrottler__retain_m25FF403486FA01412AA911474D7B319CFC469C98,
	Detail_easyar_InputFrameThrottler__typeName_m283A80FC07D5BB13A50E9863771D5D325280BD91,
	Detail_easyar_OutputFrameBuffer_input_m1B5DD96A983EF14E1EAD138966ABFC225015BC98,
	Detail_easyar_OutputFrameBuffer_bufferRequirement_m3D5C8D2B0129F57D5B495BAB426D9A4A70EE5EFF,
	Detail_easyar_OutputFrameBuffer_signalOutput_m71595D5F111E0F7D2250E80F752930D002F33339,
	Detail_easyar_OutputFrameBuffer_peek_mC50F8A96D5661C0AEF8145600AE536AC24340E22,
	Detail_easyar_OutputFrameBuffer_create_mAC9D3AE6E2AA90D99DFC4C0AF9509C435FB43FB9,
	Detail_easyar_OutputFrameBuffer_pause_m408D1C3C0F0BA969B61120E1A27F35D000B7BA38,
	Detail_easyar_OutputFrameBuffer_resume_mCCC035DC74DDB95B39F37D55E9B642F15889B459,
	Detail_easyar_OutputFrameBuffer__dtor_m10C15480E9086667BCB3B24F8CC76B125BD3B792,
	Detail_easyar_OutputFrameBuffer__retain_mEDA064591BCFEA3053D7E18EA6F64D9C07C20515,
	Detail_easyar_OutputFrameBuffer__typeName_m09378AC6E0A12FDDF25C44439D0B949722F21FEB,
	Detail_easyar_InputFrameToOutputFrameAdapter_input_mD970AEE3CCD0E1B15ACEA7FDAA3DD112EB8BF485,
	Detail_easyar_InputFrameToOutputFrameAdapter_output_m06E289CD367F8A277B9FACC8B3F5FCC1F7601885,
	Detail_easyar_InputFrameToOutputFrameAdapter_create_m9149D4611D7CDA468D6A3F7357D75B0F563CCCEB,
	Detail_easyar_InputFrameToOutputFrameAdapter__dtor_mB0D4FDBC03407716A16020E2A82F7951CC25CBAE,
	Detail_easyar_InputFrameToOutputFrameAdapter__retain_m9100C567E55CE2BC031D743F19C6C5B62872179A,
	Detail_easyar_InputFrameToOutputFrameAdapter__typeName_m089066F40946FEFF95A7EF5CC525978677D372F9,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_input_m177440C009DA0005166A9230A5CCBCABD03F21C6,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_bufferRequirement_m6AD41E0AB5B2676E305322C57F5C209D05E61B76,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_sideInput_mEE40448474F4A9986C9E41EA6FC2616B10A0E72F,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_output_m5F58F4C79E660F0911DD240E98362E795952B1FE,
	Detail_easyar_InputFrameToFeedbackFrameAdapter_create_m106F4F067592A5F43914E086AFAE8213048FA963,
	Detail_easyar_InputFrameToFeedbackFrameAdapter__dtor_m3DC1FB09CAEC44DD294CE4F95CA5037B54880B0F,
	Detail_easyar_InputFrameToFeedbackFrameAdapter__retain_m079AE236916EEB2881CF3786954740AECE6FB405,
	Detail_easyar_InputFrameToFeedbackFrameAdapter__typeName_mAF7EEFBC5E0F1C40E331F4D987C4C97A35A84A2B,
	Detail_easyar_InputFrame_index_m3243592DA7D0D75CDDDB3C0637E4E4179BF0424D,
	Detail_easyar_InputFrame_image_m0D214416531DEA5D339DB355C115691C76C5750A,
	Detail_easyar_InputFrame_hasCameraParameters_m4F6E196B8FF2D50488110F200AC61FD610994AF3,
	Detail_easyar_InputFrame_cameraParameters_mAB3D9977469E787F1F01561476768A4850145E60,
	Detail_easyar_InputFrame_hasTemporalInformation_mE48872AD9B6F7DB9838B679315F834BA25ECED0C,
	Detail_easyar_InputFrame_timestamp_m78D4C6629D1AB7F480B386C6BB166D666DFC7884,
	Detail_easyar_InputFrame_hasSpatialInformation_m5ABDD29E52536A4B8B292DA957453B9F00C9ACD7,
	Detail_easyar_InputFrame_cameraTransform_mB892D144F251BFEB80717701B7813AF49BFE73F5,
	Detail_easyar_InputFrame_trackingStatus_m37E36FDD400A0F331A119696F220AAF7D63CB61B,
	Detail_easyar_InputFrame_create_mDF4B13BB5CB4AB5A494A7F89132980354D0E12E9,
	Detail_easyar_InputFrame_createWithImageAndCameraParametersAndTemporal_m7D33F69B9F7C163D5B6BFE571A10138526623722,
	Detail_easyar_InputFrame_createWithImageAndCameraParameters_mF2FCE0AD798A4338CB87AE64AC53D21ED30D12F6,
	Detail_easyar_InputFrame_createWithImage_mB317E27D88AF2D7986E60E363922A0BAE2C239CF,
	Detail_easyar_InputFrame__dtor_m4765F8A2AF09723272BC13ADCD847C4724E74B4C,
	Detail_easyar_InputFrame__retain_m5F8E85D839FC2ED449BAF26D0810311816BE7919,
	Detail_easyar_InputFrame__typeName_mA9D7391CFA051BD60C235CAE7EF65B2D6990288D,
	Detail_easyar_FrameFilterResult__dtor_mED572B57C0D0506958F9826F65B54EA3B9ED1AEC,
	Detail_easyar_FrameFilterResult__retain_m89D8345E6C7934C165F2ACE9C47AD965BFBB304B,
	Detail_easyar_FrameFilterResult__typeName_mC195CAF147275EE943D5A222AC2C2F81017E553C,
	Detail_easyar_OutputFrame__ctor_m63BB4D92CF86C11368301C91F45F4FE7689B7B17,
	Detail_easyar_OutputFrame_index_mFD6B23253DF400D8B71DFCB76FC4F9AF18A455DB,
	Detail_easyar_OutputFrame_inputFrame_mA586757588F739273C0DB6AF8E9D085EF92C5BD4,
	Detail_easyar_OutputFrame_results_m6ECE0B61B691E9B8BCFF808770DDCC997E3A49DE,
	Detail_easyar_OutputFrame__dtor_m4346341071401BF01C2F86D320FFF9A4D14899E5,
	Detail_easyar_OutputFrame__retain_m714A8E413CD2BB19F509C80681675E413F8B02CB,
	Detail_easyar_OutputFrame__typeName_m71048A27956E9EDA281C922F0307897177181B09,
	Detail_easyar_FeedbackFrame__ctor_m711F9FBBA23A5DFC49360DDAFF0240CFF98095AA,
	Detail_easyar_FeedbackFrame_inputFrame_m89CAFE94220EF71430922FA4D84DFA1DFAD5717F,
	Detail_easyar_FeedbackFrame_previousOutputFrame_m857403B6A8564F4187D8353B0428709B5A05B1A8,
	Detail_easyar_FeedbackFrame__dtor_m87C04D9F9E1F01A52DE58F5E820D841F43414859,
	Detail_easyar_FeedbackFrame__retain_mB153A7E0A433D44C04E06FECA57DA0DAEF87BBA5,
	Detail_easyar_FeedbackFrame__typeName_mBA75E551CEACE5ED82466EA9194A7F5DB514A83C,
	Detail_easyar_Target_runtimeID_m40E148A547C76112516FEF60B5C3AF8C53A80AE1,
	Detail_easyar_Target_uid_m17EC6C81561E460A05F411CD80FBC59F3A395953,
	Detail_easyar_Target_name_m4FE0FDB35A58ADFC9293D87E6E007A0C852029DC,
	Detail_easyar_Target_setName_m6E510557F2A8F97A8A5FCE6E289FFC41DF0A5EB9,
	Detail_easyar_Target_meta_mD283AC8B5A2B82865A284EDBC38E96371AEF5D64,
	Detail_easyar_Target_setMeta_mC8F76141A764417CAA1C00C7C8DD55C26BDE636E,
	Detail_easyar_Target__dtor_mFB3C215BD0C794B07FD49CD4D6EE46AC9BE87953,
	Detail_easyar_Target__retain_mDE6A6C16DAEB5E818421159326E125542EEFFB64,
	Detail_easyar_Target__typeName_m4C18126ACD072A41BCC350211C910F89DC6A5B40,
	Detail_easyar_TargetInstance__ctor_m967FB8B3754300B53EDF1DD57CD54CF781CDD479,
	Detail_easyar_TargetInstance_status_m1089E06D57B2659648A635608B3ED4D74AC28DF1,
	Detail_easyar_TargetInstance_target_m6EB1A3D17118A2E6E31C0C99E3F06B9547595CB5,
	Detail_easyar_TargetInstance_pose_mE07869F20E8BB11C608A04A901BA0ACE455158A4,
	Detail_easyar_TargetInstance__dtor_m9F9120FE38A4A7D1C396EDA517087306FAC1E9A5,
	Detail_easyar_TargetInstance__retain_mA9F729F3B08C7C7FA5CC43EC1D267ED75675A524,
	Detail_easyar_TargetInstance__typeName_m6D577900C401C49A66F763E4A065E1ED89602467,
	Detail_easyar_TargetTrackerResult_targetInstances_m6519E45963F4252050AB694A834D5E2F481179C9,
	Detail_easyar_TargetTrackerResult_setTargetInstances_m7B5A7B7A88A45604AC258A6768244BEC829E5526,
	Detail_easyar_TargetTrackerResult__dtor_m806FA78A2C963364AE27BDE0D0D272BB0F2D01F5,
	Detail_easyar_TargetTrackerResult__retain_m12C7BC9F07F41167D4E051A590B2A34BB816591F,
	Detail_easyar_TargetTrackerResult__typeName_mDFCC2959AF5364459E58FE30A98716CEB7366631,
	Detail_easyar_castTargetTrackerResultToFrameFilterResult_mA17189C0166007845CB28608B832212CD93BB942,
	Detail_easyar_tryCastFrameFilterResultToTargetTrackerResult_m1304D0AE11AC585574500CCE67E8893B5618231A,
	Detail_easyar_TextureId_getInt_mD460B37428437E8A8168671B96868A1DCD980F82,
	Detail_easyar_TextureId_getPointer_m2D02947867FA695BD0B9C004C7B1259A8C350BC5,
	Detail_easyar_TextureId_fromInt_m9CB4A21DDACB0FECE177AC970885BFFACB0D3463,
	Detail_easyar_TextureId_fromPointer_m5ADB716FEA2AB9D10BC94F671CDA58D51F89F9F4,
	Detail_easyar_TextureId__dtor_mB9B28EEECC858A7DAB8F99F359EEF2B9E7C60905,
	Detail_easyar_TextureId__retain_m8DE4F3DBB951DE7F7491FA394590BE25DBDDC639,
	Detail_easyar_TextureId__typeName_mC521B901EB1A153FE537374F63B51C5FD945CC5D,
	Detail_easyar_ListOfVec3F__ctor_m2C6264711D3BA58FB267057842F708412FEAE528,
	Detail_easyar_ListOfVec3F__dtor_m24B6C30D5D3D61C41B133C24591C5BD75E5AF575,
	Detail_easyar_ListOfVec3F_copy_mE455F41D7DEA42682F4AC9987752C65B9E586CB8,
	Detail_easyar_ListOfVec3F_size_m8459FD38D7D5FFAC0DA603E60B5619F0736EDC09,
	Detail_easyar_ListOfVec3F_at_mA419D519D12177CCB04FAB19934C7A6C0138E3E8,
	Detail_easyar_ListOfTargetInstance__ctor_mB318A85E43B0E5630A7822C18A82C487D7D8ACB4,
	Detail_easyar_ListOfTargetInstance__dtor_m908DFE44B72C6E366DA386D50DEEECFAFEB29A7D,
	Detail_easyar_ListOfTargetInstance_copy_m72E7E2564E0C27DFF1F3BD3B432F2B4C23C21FBB,
	Detail_easyar_ListOfTargetInstance_size_mDEBE26E1E430668734F178AE3B92A13A1C5DCA2C,
	Detail_easyar_ListOfTargetInstance_at_m686D837366AFBD547A38F33A7CE6BFBB78FB6018,
	Detail_easyar_ListOfOptionalOfFrameFilterResult__ctor_m5DCA08EF698742F8176C00AA32BC8D88398324E1,
	Detail_easyar_ListOfOptionalOfFrameFilterResult__dtor_m164270C7201ED2D4F6D0DE71505D0542035FB854,
	Detail_easyar_ListOfOptionalOfFrameFilterResult_copy_m4F6D461B4F5626D14DBB86E23C3DDA335AA4FD9C,
	Detail_easyar_ListOfOptionalOfFrameFilterResult_size_m36ADBF6235BAC565A63B1DC799D3D677570EAFB9,
	Detail_easyar_ListOfOptionalOfFrameFilterResult_at_mB2A21F8D8BD97366107C602471406FCF7CD54B15,
	Detail_easyar_ListOfTarget__ctor_m1557530754C8F775A43C7C701A9274908A6DE0B0,
	Detail_easyar_ListOfTarget__dtor_mC27435F886F59BC431C261A654D74F43F9753CC0,
	Detail_easyar_ListOfTarget_copy_m4F757586CD7582CEF18875B468BE067C67DFC77B,
	Detail_easyar_ListOfTarget_size_mF8EC23E748EB3BF2CD236CE78496461161FEC5BA,
	Detail_easyar_ListOfTarget_at_mD130B5A468F1E22C0C9359EA300038CE9F8DC4D8,
	Detail_easyar_ListOfImage__ctor_mC851D89FB65808B4499B1256230C49ACA76E0B08,
	Detail_easyar_ListOfImage__dtor_m274DA9C1A3A30133DBAF90AF0542300AF0B0709F,
	Detail_easyar_ListOfImage_copy_m8C3E4C8BDCE9852697D7442226A8973101781512,
	Detail_easyar_ListOfImage_size_mD27D9D9386B9D5CD26BBFF18FDD2A427A0B7BDFD,
	Detail_easyar_ListOfImage_at_m883180C6A851A167587A2D934659192C1B3304C8,
	Detail_easyar_ListOfBlockInfo__ctor_mB2E363F4C8B8BCA43EF45A64BB9F4AC5F789D7B9,
	Detail_easyar_ListOfBlockInfo__dtor_m5FB05603BCFA80EBB2F3A287B303DB5455054B98,
	Detail_easyar_ListOfBlockInfo_copy_m1DD5151A30DD34D0DD822958343FCF155DFE9D57,
	Detail_easyar_ListOfBlockInfo_size_mDA47924CD19B92A8700725D24F4685126379431B,
	Detail_easyar_ListOfBlockInfo_at_m606BC89C9176055146AFA19AB9BF2D60DAD78FFD,
	Detail_easyar_ListOfPlaneData__ctor_m6DBDECE5DE1C8AA5064D1FE5D49E9AF0FB2FD438,
	Detail_easyar_ListOfPlaneData__dtor_mDF452DB0EC1CC1427E85D2B2860ADA63EA43C99C,
	Detail_easyar_ListOfPlaneData_copy_mFE04CE16A4E35C65A6E6FEA3F81EA9918E1C1C79,
	Detail_easyar_ListOfPlaneData_size_m4772B17C49818CF6271663B03129DCBBE3A664B7,
	Detail_easyar_ListOfPlaneData_at_mB6DF289E5577FDBBA6DA6CE41752175AF41A5FBD,
	Detail_easyar_ListOfOutputFrame__ctor_m9E0EC0098FDD4BAE8098AB4A002C55CBFF6B4BED,
	Detail_easyar_ListOfOutputFrame__dtor_m7C25B0C1C59F33F7A3895762ED2FA2EC4E754D85,
	Detail_easyar_ListOfOutputFrame_copy_mDDAE1D429DBD3D5FD63ACD7AECC7E05AE497B877,
	Detail_easyar_ListOfOutputFrame_size_m89BA841C812446C9CCC393B81B57593D4A3D5AE3,
	Detail_easyar_ListOfOutputFrame_at_mFBF82A04B5DC2DE6E0E6057AB91BF78CD5C2C10D,
	Detail_String_to_c_mEE7B62122F209B1ADA1C5DFDCF2CEFE537725894,
	Detail_String_to_c_inner_m6F16B7735B8019203C9639EDBE08F09532940F7D,
	Detail_String_from_c_mE654BDA106C4698A73E49A481889EDE05C70852D,
	Detail_String_from_cstring_m134BC4E89C40AFF8003E80D40A1F6EECB29BEB0F,
	NULL,
	NULL,
	Detail_FunctorOfVoid_func_mF783D5E1A5CC1474AC7371278CABF14761C1737D,
	Detail_FunctorOfVoid_destroy_m0F5DC9905B8F43E6DC2E844556B0FF56E4BF67B6,
	Detail_FunctorOfVoid_to_c_mC18BFB0946B870EA8CD32DDB8C1FDD7E70B44ED3,
	Detail_ListOfVec3F_to_c_m3FCDAC235A082B1E0A5C31C61A109694172D75F7,
	Detail_ListOfVec3F_from_c_mC344D6C9A436549AA34BCEFB97904D37FBC332A5,
	Detail_ListOfTargetInstance_to_c_mFD579AA8AEA8CB00AFE3CE243C2F7E104F1DABED,
	Detail_ListOfTargetInstance_from_c_m50DD73E0D9BA3386FD096E1AB1FCB79676C55EE2,
	Detail_ListOfOptionalOfFrameFilterResult_to_c_mD79D5A3D4AF7F60BBF7A97D2C659A32EEBBA07C7,
	Detail_ListOfOptionalOfFrameFilterResult_from_c_m3AC82D36FAA42F20BC2884224AC541208F550D15,
	Detail_FunctorOfVoidFromOutputFrame_func_mB6649CCC85AAE55815A216F223BCCFFE5253FEC0,
	Detail_FunctorOfVoidFromOutputFrame_destroy_mCCA2C33F06C260D5C48CE09D9E03133D4D0CD6DD,
	Detail_FunctorOfVoidFromOutputFrame_to_c_m79612E87D865E1B035F5BC8111321E60115C4787,
	Detail_FunctorOfVoidFromTargetAndBool_func_mFA51F1E153904EF00496BE23F062C9A12C273793,
	Detail_FunctorOfVoidFromTargetAndBool_destroy_m6AA500E710A687EBB7C96D03D17E131BD94186FA,
	Detail_FunctorOfVoidFromTargetAndBool_to_c_m10AFC3EDDDEFE2EE6F6DC64B4C6E93170A3F832F,
	Detail_ListOfTarget_to_c_mED50E60F2824008B97FB60DEC74C8EB086E79C53,
	Detail_ListOfTarget_from_c_m9C2827070C7229659B098728B0D80E4F4EDA274A,
	Detail_ListOfImage_to_c_m9FF99F1C9FA7810EBE120B9E569B458B37DB800C,
	Detail_ListOfImage_from_c_m34F1F54F8DCD659A18A3D3D5977D3D5E1CFF6AAA,
	Detail_FunctorOfVoidFromCloudRecognizationResult_func_m90154CEE7823B813A64107474E84C9D8E49F9FE8,
	Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mA6894D99F036B445918E01B88C818F63C208B1C1,
	Detail_FunctorOfVoidFromCloudRecognizationResult_to_c_m978E39577A697CCE3687060E1690CD7B3F978982,
	Detail_ListOfBlockInfo_to_c_m8AFA65582FD918E3C7AA3709D46B88D9AA07628A,
	Detail_ListOfBlockInfo_from_c_m690AC5DA5FA5A91F2D05482C0C812D0E284260FC,
	Detail_FunctorOfVoidFromInputFrame_func_mA2CECDF41A63A69813C1E34E46FCE069F47C6427,
	Detail_FunctorOfVoidFromInputFrame_destroy_mB73878C35844CC8471D0EF2A8B4A1FE0D3C6F878,
	Detail_FunctorOfVoidFromInputFrame_to_c_mF015CC51C5127C1F3858F895C08B32D6718833F8,
	Detail_FunctorOfVoidFromCameraState_func_m7FCB6EEC40715D367C83D86227157A9E122568C9,
	Detail_FunctorOfVoidFromCameraState_destroy_mC70C7A72F6DBC20901D045EFF23EA2AF58A5BF41,
	Detail_FunctorOfVoidFromCameraState_to_c_mD100AB4BE829B4084A287F004B0953C4BD2F64E0,
	Detail_FunctorOfVoidFromPermissionStatusAndString_func_m61FB5A520BF4BF61618A6036F4C225797196DB4D,
	Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_mFB9A4F27091BABD5B3A3FE933C4003F0478BD7B5,
	Detail_FunctorOfVoidFromPermissionStatusAndString_to_c_m7D7ED018B419B0711BB71E4680C092780BE15553,
	Detail_FunctorOfVoidFromLogLevelAndString_func_m53946E55D87E33DA5FA36DC16B26E9B3DB0B439C,
	Detail_FunctorOfVoidFromLogLevelAndString_destroy_mADE83DB6360391E4C933515D59EF3F73E4B0B08E,
	Detail_FunctorOfVoidFromLogLevelAndString_to_c_m1F047A303620D5747DE2EB5406CE3D7CBBF0153C,
	Detail_FunctorOfVoidFromRecordStatusAndString_func_mFA196EC005028814229B1F55F1E24665DF692553,
	Detail_FunctorOfVoidFromRecordStatusAndString_destroy_m051F182F8E827A97DEC26087EDB6CD6616462981,
	Detail_FunctorOfVoidFromRecordStatusAndString_to_c_mF6D4E298A7316095168890C8AF7D59B4054E8961,
	Detail_ListOfPlaneData_to_c_mF1235606DF4055877099E9690AA242790FB8A1D4,
	Detail_ListOfPlaneData_from_c_m7797045CAC0D0B89A65AE3BDB60924A61F92A9D7,
	Detail_FunctorOfVoidFromBool_func_m8C959A83611A4385EE07D73B822F990C10B6EDF6,
	Detail_FunctorOfVoidFromBool_destroy_mC7D4393D0066F06286A66262A3B206C16F9A0A83,
	Detail_FunctorOfVoidFromBool_to_c_m7E80875A16F9F2B4B865D8C76BE83CCC5E9559B1,
	Detail_FunctorOfVoidFromBoolAndStringAndString_func_mE08DFB80305F9D293A7D957A1B409629ED9B4396,
	Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_m1D50B3D098976A8F7CDA2AEFDCA989ABDF1FBAD5,
	Detail_FunctorOfVoidFromBoolAndStringAndString_to_c_m31B7E02DE768C5A2B93AB04508D8D1607A647009,
	Detail_FunctorOfVoidFromBoolAndString_func_m87AE9C0004F64626A5E00CFFD0F1A2F868E29D34,
	Detail_FunctorOfVoidFromBoolAndString_destroy_m1C520BDA809AB254063C19A8E92794F7A368E283,
	Detail_FunctorOfVoidFromBoolAndString_to_c_m3A7CA2C029A0973FC1C64188CC807CFDA90EF6F7,
	Detail_FunctorOfVoidFromVideoStatus_func_m75DE2AFDC396D76465BE54C22D32881BE260470F,
	Detail_FunctorOfVoidFromVideoStatus_destroy_m560F57E8D9CDF6C4CCDD018BE704C492D7C6B550,
	Detail_FunctorOfVoidFromVideoStatus_to_c_mE196DE67EFDC99AD2C1A7788604ED69EB068CF3A,
	Detail_FunctorOfVoidFromFeedbackFrame_func_mB4288FAC2F09BEFDD8503B7684DDC994C32F8CE7,
	Detail_FunctorOfVoidFromFeedbackFrame_destroy_m08DAAB8A98135C1DF4CFDC319727C08E4EF48BF2,
	Detail_FunctorOfVoidFromFeedbackFrame_to_c_m21611EE69D008507BAEA871DA5CB2E9175EDC949,
	Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m458EA310F59A4613F82319C3BE247BE328F9EE19,
	Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m50A85D540D02D4F023B507E3A9BEF80C6D75D58F,
	Detail_FunctorOfOutputFrameFromListOfOutputFrame_to_c_mA81A44C76F95D41B1BCCEF51CE3957FBA51665AA,
	Detail_ListOfOutputFrame_to_c_mC4B576DD79667CFB0B05BD099664B567850A078B,
	Detail_ListOfOutputFrame_from_c_m289AB323D142C3F4D2A020C19A869A3BFE9C60E1,
	Detail__cctor_m64B4BF6AAABD85F493C928356BC117AA18CFB94F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RefBase__ctor_m91788A7D2CF047C7B85C0DD02445BFE6240E44C4,
	RefBase_get_cdata_mD182E1D48174BA159B5AC842C977D0AA20706B5B,
	RefBase_Finalize_m05B482B58CFD31F088B8B284179C9F780434994A,
	RefBase_Dispose_mC5EEE74F2631F48AA8057680984D440B3F6684F5,
	NULL,
	RefBase_Clone_m1CFD7B5FBA7DA2DF5A4548DDCBC06F291BDBFEFC,
	ObjectTargetParameters__ctor_m8639718B1A8466FE107C169FE53894CA087DC679,
	ObjectTargetParameters_CloneObject_mB707B1D59C0DB254CB9D55A5A7061C26D8FB108E,
	ObjectTargetParameters_Clone_m074403BF94452C568AB2199C1BE50BA8307B1AF3,
	ObjectTargetParameters__ctor_m436ACF83AB36FD865F1E6D69DE8E122117FA2969,
	ObjectTargetParameters_bufferDictionary_m1A9F9B83A938D283F059A63086F130B751B72F0A,
	ObjectTargetParameters_setBufferDictionary_mE7DD1B96A4ADE4A3F88F20CD58B013BDDC08A75B,
	ObjectTargetParameters_objPath_m3160C541889785D07D64462DD0E6645917ECC397,
	ObjectTargetParameters_setObjPath_m76C44C09E57CF27A6D0DA64095112A640EC22FA5,
	ObjectTargetParameters_name_m1C646AAADE21448F2718DA98830B6E81E3CA9D98,
	ObjectTargetParameters_setName_m2E690848398C1CAA5FD4368DBC72397EEB13EC0B,
	ObjectTargetParameters_uid_m657E16F63D719250FF01D23D97CD4E5827A3262E,
	ObjectTargetParameters_setUid_m61A40A6E09E5415C5CE959E77EEA1F3AC9CAD83E,
	ObjectTargetParameters_meta_mF871DCD84C670AD9B4B01194E4EF6C343752C42E,
	ObjectTargetParameters_setMeta_m440F5047BE45E1CA2FA86CD34FCA708BA3EA4E06,
	ObjectTargetParameters_scale_m132A590ACC5E2FF6F82038FF61F7F310781BB51B,
	ObjectTargetParameters_setScale_mDE32C98CA8E4A7D8C759E35D16F45450E525F2DB,
	ObjectTarget__ctor_m924D32853AC861A188E85EBB4A3D4EE1AC7BC181,
	ObjectTarget_CloneObject_mB91E2DE038E562E5E6CCDD8DDCDC7BE7A08DAEFA,
	ObjectTarget_Clone_mBC8C4BCB230B7420AA1CB77277B1D8C88DAB1633,
	ObjectTarget__ctor_m946F072C8262698C034B305E13F7799676D5E7CF,
	ObjectTarget_createFromParameters_m1606A56AF84B40B3D8523EFB1A5C5BC37C7FA14F,
	ObjectTarget_createFromObjectFile_mB9555990BE683CC84EE6B3FA99E5A23F357E20D0,
	ObjectTarget_scale_mF3268BA13BF64C433D3542DA638D2A41DD6A8512,
	ObjectTarget_boundingBox_mAE9EBCF2AC7271B9A90F33DCD00E8D9870FF5DD0,
	ObjectTarget_setScale_m70B0798461799B9CA7C36A5C2B808E31BCB2285F,
	ObjectTarget_runtimeID_m2ADB716CB3CE10CA83C50D57F341492AB181CF30,
	ObjectTarget_uid_m8C97428C7E977E8742FA2D648ADFA8AB00C0F5B1,
	ObjectTarget_name_m4AA1DE5862EF7C86114C43699F05D54B4CD80A26,
	ObjectTarget_setName_mC5F602F0A1B97D91871EB0F9BCB91993E850E3AF,
	ObjectTarget_meta_mB07D7C6922AD59491B80B484F011F069DF77811D,
	ObjectTarget_setMeta_mF6DFF9008E5B4535E8E0DB5FBBB884836B6A9E46,
	ObjectTrackerResult__ctor_mB61219E47B0A872781A4A37762365162199CCA1A,
	ObjectTrackerResult_CloneObject_mD0E9F4A237DFDF9676952409A07F4567591D7A57,
	ObjectTrackerResult_Clone_m5026912103FB9A4F43E6A6AB16CB5F5628B9FFF4,
	ObjectTrackerResult_targetInstances_m94DD9F2BE5452736A6AFE883A5A18B5C38F7B18F,
	ObjectTrackerResult_setTargetInstances_m1F6BC1E3C5937C2157A4F771A2334C5B7301F9F4,
	ObjectTracker__ctor_mA7A9EF4D1B8B074063DD248AAAA5B3345FE2E7B8,
	ObjectTracker_CloneObject_mE7BB260FA1FBCD98118B3615C4EEB114145F2FEB,
	ObjectTracker_Clone_mE69E0452C1AAA27D742A2A546B790874345BF801,
	ObjectTracker_isAvailable_m083D4EFD6B81B341E7F5CDF125924758ED21D226,
	ObjectTracker_feedbackFrameSink_m4909CAA28D3EC64670932C1F71CD56252FB6761F,
	ObjectTracker_bufferRequirement_m7740DD98FD1C606A8576BAD6C37D6C1D949B664D,
	ObjectTracker_outputFrameSource_m6D8601B83FAE769B499ECBC35B02C137DDC6948A,
	ObjectTracker_create_mD209283F841889B26EB8A317DEBD9D2B51BABD6F,
	ObjectTracker_start_m17F1D1D22C760D2AEA3F7433D39089EEE701F307,
	ObjectTracker_stop_mB3E8E5B0197715DB98FA7DF095FA09FA43CD8941,
	ObjectTracker_close_mF067D3B2B0BE57ACFB10C6A7ACAECD21F6006994,
	ObjectTracker_loadTarget_mB16084CB91FB08D7246A81834876AA77C923996C,
	ObjectTracker_unloadTarget_m7F2FF728A1AE95EBCEFC31CABBDE12E2BF5F754F,
	ObjectTracker_targets_m2635EBB7016F8E98FA8093B40BF7744B8FBC328F,
	ObjectTracker_setSimultaneousNum_mA9075A08BBB49358C63463884FC97711122F456B,
	ObjectTracker_simultaneousNum_mDE744A781F4C13CB97CB4C6403D5844FBD497D5C,
	CloudRecognizationResult__ctor_m002DB4D3C2D1B071EFCA54B116BB74776666403B,
	CloudRecognizationResult_CloneObject_mDBD103E163E9C4A1418FB6DFB94BA7C12A1579A0,
	CloudRecognizationResult_Clone_m429D2B0401C51975AC0CF8F322DFC4508A01ABDA,
	CloudRecognizationResult_getStatus_m6E1AACCBCE52E02BF171576D30161E1C72EC3AB4,
	CloudRecognizationResult_getTarget_mE56DDB50BECDE81C1D559728DFA2277155FBEEC5,
	CloudRecognizationResult_getUnknownErrorMessage_m2CC5F7642754D8594E7370FF36D46179ECEAEEC9,
	CloudRecognizer__ctor_m93FFD3B967300233E501FF4341976DACBBFC2BCB,
	CloudRecognizer_CloneObject_mD7EE322C5128F8EA0CC8EF2DE9D9C53A8E3746EA,
	CloudRecognizer_Clone_m88BAC210481DEA84553D981D53D1E234C572A608,
	CloudRecognizer_isAvailable_m4285C37AF079A30076C3873B8B72967D823165D7,
	CloudRecognizer_create_m6BB61143CEB61D7E82855704A195F337562F4D8A,
	CloudRecognizer_createByCloudSecret_mFD6B4D671A6AE058C0686FD7B1C8C452A8ACCCA7,
	CloudRecognizer_resolve_m7B2B1A742B0B875C2300248D138FA2552BBEF986,
	CloudRecognizer_close_m60640E28D8ECCBA19544C3152DC34CB4735697A1,
	Buffer__ctor_mEF3C573C11D612B72D515E1C7F0CF744A341BE1A,
	Buffer_CloneObject_mD01AEB1D867CE25E230D31DA6DC9F519656751AF,
	Buffer_Clone_mF8012D3BF56109E59E22F70871505B5181147C1E,
	Buffer_wrap_mB2AB986AF54AD8826E2C10B999BC0A5D0BDB060B,
	Buffer_create_m1433F8E7DC4CE17B24A54ABCB03406831D49800A,
	Buffer_data_m261BD1BD4753A4F340C64B63D9B645C59096407E,
	Buffer_size_mEBC3625F4E332CAE52CF1A77EE87F5FBF7B0BFE3,
	Buffer_memoryCopy_m6E06ECADF51903161E42F6055D5DE7BE819225C7,
	Buffer_tryCopyFrom_m599D8D5488BC70B016D909E77E7A42FA6988877D,
	Buffer_tryCopyTo_mE02D6C6BC05007B68EFDE784FC4F43F63451CD8B,
	Buffer_partition_mF8CC984BE418E57F8D59659291B0EAD053B50D5B,
	Buffer_wrapByteArray_mA2011FD8AF7706362E53D5D95BB8F6DB118586D7,
	Buffer_wrapByteArray_m9F65669B174ECC5CC2EAF34C0E76130164163F1C,
	Buffer_wrapByteArray_mD921453D5600FCC247D99A166F9FA5360B097ABB,
	Buffer_copyFromByteArray_mB626246C15839E8EEA921A3739B78D5BBD6F47D3,
	Buffer_copyFromByteArray_m2C9875A71992BEE709C93A8F136E1101A33051F2,
	Buffer_copyToByteArray_mC648B696E1E7E979323D241A2260F4D6DBCEDE32,
	Buffer_copyToByteArray_m48502803D78E11061E50CE1B0AA6F586B823EE70,
	BufferDictionary__ctor_m4CF91091E43C1698E3E1FBACFCAF250318C21252,
	BufferDictionary_CloneObject_mAD6FA00A4836FEA56255E3FA26C17E2EE6AAC606,
	BufferDictionary_Clone_m908382D1CF25FEEEBFBAA038C1DFB370CAF52F1A,
	BufferDictionary__ctor_m44FF83DD8756EC1F69D1D3C22317CE8CFE5822C4,
	BufferDictionary_count_m9FA601E3F73807747761943DE41BF9E1D457622B,
	BufferDictionary_contains_m2B74420924CD26D15FFE5D3A5F1696846517D6F8,
	BufferDictionary_tryGet_m666EB4F17C21A6B92E927BCDA8B3A216B5CD487E,
	BufferDictionary_set_m55DC11FDFE7AEECCA1EE8FE2940E38AF2B9487C8,
	BufferDictionary_remove_mE6AAD1B8BEC54300407701B0686CB4DB3F2CD97A,
	BufferDictionary_clear_m97CB377E4FF2C916216E76BDC3955B981449BA50,
	BufferPool__ctor_mBE8ABBED71BB3EBB0E3E402FB1871DCCDE2B88D6,
	BufferPool_CloneObject_m1C8252FBD1C2931FA4521FB02A5C3FF14D684814,
	BufferPool_Clone_mB99AF64A70D3C1ACE53080D0AD9982161F03ED72,
	BufferPool__ctor_m8C9069485338E307D7DFA1EA8405A50CAD433147,
	BufferPool_block_size_m0977C2CC58C2D7F4C57D360790DB2188741675C4,
	BufferPool_capacity_m142D51E9D7A87D64D076A7D3CEA745CB359E5085,
	BufferPool_size_mE1934897587752BEBC11BE672447A4BBE4C5ABBC,
	BufferPool_tryAcquire_mF65BB918E3721A933FBBC46444D9D06BC766D95E,
	CameraParameters__ctor_mCAD7C2A96D99887CFCDC0B88BB6B4BBA25E35D03,
	CameraParameters_CloneObject_mE5629D6218376CB43C388E929E75D05DE3E73AC0,
	CameraParameters_Clone_m5B1FB7B02B1BAEC80C58E51B8A28C741A9CC49AC,
	CameraParameters__ctor_m9B5FB86AC5E02CF08C315CAEB4947DEC1BE2F8DF,
	CameraParameters_size_mB225D68F6DA3186820C7CB7AAF63380B79FCB2DD,
	CameraParameters_focalLength_m2FF303C350D10BABDF66C45295B5F9A4AB491096,
	CameraParameters_principalPoint_m6E2B0B61E424A3CB0C437E8A63A3B744B32E7F17,
	CameraParameters_cameraDeviceType_m8C52D12B699798959D87885C9D0AE5A80584530A,
	CameraParameters_cameraOrientation_mADAB58839A6BAFEC90D0AC5656D7FCF251073FD6,
	CameraParameters_createWithDefaultIntrinsics_mAF9EF6182FA49B5542488514C709DD8418D4DC85,
	CameraParameters_getResized_mAED2A3DC77DC2AD22E9ABBAA0649B544940BBC7E,
	CameraParameters_imageOrientation_m8B59CFD2B763044D085C480AC406140262F0845E,
	CameraParameters_imageHorizontalFlip_mA797AF69683770220F1904658F11975A6393BAA3,
	CameraParameters_projection_mE54ADBB454957093146903CE2A40DA6844FDD888,
	CameraParameters_imageProjection_m9A11DAAAC2241903641A13212FC21355B171059C,
	CameraParameters_screenCoordinatesFromImageCoordinates_mDBB25316CE26993F1781D00B3487E02F7CA860C1,
	CameraParameters_imageCoordinatesFromScreenCoordinates_m41DB742F00C3182AAE5AC498B528F572EC8F61C5,
	CameraParameters_equalsTo_m4CC5CA93CBFB88F2AC2589152FFE03CA781C31DC,
	Image__ctor_mB9C8362D8AD67192ECAE16401923946254297F4A,
	Image_CloneObject_m6184971E200120080B008A52F5495390965DB0F3,
	Image_Clone_m65828148EA61961AC68180539D7B7BAD7A177201,
	Image__ctor_mF60332DD9FAFCA7149DBFA02E8999B866A76DE95,
	Image_buffer_m20B6E778C27A4F5921DECA6AA7FE9C68DFC56389,
	Image_format_mFA8D4F41659361CC0BCCBEC149E243E6414506A8,
	Image_width_m4F69246DA2F773F8B32EFC3F0F2511FF1BB9C583,
	Image_height_m34527BDB15F2496BC06B20272F32CE3AFF8BB45C,
	Matrix44F_get_data_m1DDEAC58B2C140900BF354F3489B9749FF407098_AdjustorThunk,
	Matrix44F_set_data_mA1983158F080953F2E9E8CF75DE1F1605C4C58EE_AdjustorThunk,
	Matrix44F__ctor_m271F522AB6CAAAEB41F724D5E99D36F489574FFE_AdjustorThunk,
	Matrix33F_get_data_m1527BBB935223A323992B0252569048E0DB1B504_AdjustorThunk,
	Matrix33F_set_data_m7B9C86C95AC3774CBC63684FD40CE7177EE0F309_AdjustorThunk,
	Matrix33F__ctor_m7E0ECB95E3F54AE58AB0C5A991B7D785D34DDAB5_AdjustorThunk,
	Vec4F_get_data_mF51AC023871ABEE4DBD3AD28C4F390CFB273AA30_AdjustorThunk,
	Vec4F_set_data_m0BA7C5536C1701156644686BC4521B1F896689A7_AdjustorThunk,
	Vec4F__ctor_m2068732A7C9152EB671CEF3821B8B7BFC25AE18A_AdjustorThunk,
	Vec3F_get_data_m90487EB17A649369D3104992E87F591768ECB91D_AdjustorThunk,
	Vec3F_set_data_m5B024D4EE0A5D7106603E0DE33B68CF866A8CCC2_AdjustorThunk,
	Vec3F__ctor_mF713AA4F3CA14E1AF8871D0E03655932065DE10E_AdjustorThunk,
	Vec2F_get_data_m4E526F29C8FC3EE18398AA7D947926AF0092349D_AdjustorThunk,
	Vec2F_set_data_m75C0FC83A5CBE4A65B3B457E15552B503428EE79_AdjustorThunk,
	Vec2F__ctor_m1B76E4A69CBFABDA68EA49CE6255960B49C61838_AdjustorThunk,
	Vec4I_get_data_m0B3AABF38F0E33C97BFB07D5656D3DD92C86DD15_AdjustorThunk,
	Vec4I_set_data_m82AC198017DD058FED3C3EF4FE8C8F45B2223FAB_AdjustorThunk,
	Vec4I__ctor_mA483106568863005D3DA0444152E988A6BDA89CC_AdjustorThunk,
	Vec2I_get_data_m4EBB17FC8365344BF8FD08505F8A3B281BDCBDB7_AdjustorThunk,
	Vec2I_set_data_m04816F71D2D5F8520DA2A2ADEACCB292C9966992_AdjustorThunk,
	Vec2I__ctor_m1A88208FBBDD30CC030A78F2D0D92385FAFE0666_AdjustorThunk,
	DenseSpatialMap__ctor_m2815677A18323B8C8E86870368879E3115542224,
	DenseSpatialMap_CloneObject_m2D3F5503E5F727B7FE548D43BD7F98DFADBCD29C,
	DenseSpatialMap_Clone_mB8DC4DADD575873C000BF9347EBF56420EB0E880,
	DenseSpatialMap_isAvailable_m8AB15859582EBC1BD44926F8655AC879A2E02563,
	DenseSpatialMap_inputFrameSink_mF572AF0EF62368F8C4F2B1F7E56661A2E7EB79E4,
	DenseSpatialMap_bufferRequirement_mE7A13DD131C096BB0D424BE2E78B5AE78ACFFFA2,
	DenseSpatialMap_create_m3266893950344529E5935822F64314AF990275F6,
	DenseSpatialMap_start_m244B31E62F629BA47D8B00E6D64CC160AF735206,
	DenseSpatialMap_stop_mA46FD9DACAD3FBA9A97408C43474B4D5A9DD13B8,
	DenseSpatialMap_close_mE4B448FA529E993D63C5EFA6FF5AAB5820A6C24C,
	DenseSpatialMap_getMesh_mDBEEA6A4E2A6B864DEFE6B39EF9CBFF793AAF033,
	DenseSpatialMap_updateSceneMesh_mA9CE004FC241C724FB97B171E5E9D0A8106E0C40,
	BlockInfo__ctor_mDE82E90CACBCDDDC03D42A9291046B2762876E82_AdjustorThunk,
	SceneMesh__ctor_m73273F2601FF07DDED492EF9EC3FECE8E4A8F21C,
	SceneMesh_CloneObject_mAB7C07480AE30807D24C0F60B958262C076182C1,
	SceneMesh_Clone_m038039E8D019D53E4F1A9BC4177B26AA1EA33487,
	SceneMesh_getNumOfVertexAll_mC7ABDEE82861B4C506FBF7110C7DAA17A3CD5372,
	SceneMesh_getNumOfIndexAll_mDCAE70B9668409A680C0CE8FEFEA9941C5DC2C2B,
	SceneMesh_getVerticesAll_mB68A474BA1BC297CC78DC6D0B1A19B762FFA14CB,
	SceneMesh_getNormalsAll_m6705D6CCF147644A55EB6739AC65F00F9BFB50CF,
	SceneMesh_getIndicesAll_m9A8472D980A3DBD8D1C5D7488B933DAB36207CFD,
	SceneMesh_getNumOfVertexIncremental_m7AE13F0F5D5C8AF91B1E9D9AA407ADC2296F16E7,
	SceneMesh_getNumOfIndexIncremental_m5443F93EC6A37B08FCFC83DA54C5A0E30B24EEDA,
	SceneMesh_getVerticesIncremental_mFB1BF4BA5C98670772D7D9DCA2E7CDB9E0CC33C6,
	SceneMesh_getNormalsIncremental_mEC011B6334049896E9A69DC9F7A6D7FA9AD5E715,
	SceneMesh_getIndicesIncremental_mC2839616893212BC963046D1CC866898F13842A4,
	SceneMesh_getBlocksInfoIncremental_mD059A4278AA9D01714E0EAE886E25749B893B6EF,
	SceneMesh_getBlockDimensionInMeters_mC5181EC4C02044F01286FA7B378FF1AE513630CB,
	ARCoreCameraDevice__ctor_m57862F2E4EDC33C0F1945FBC7BC35218D3E31A34,
	ARCoreCameraDevice_CloneObject_mE8611DFBCE3A4EB74267A798922898C85F21F6EF,
	ARCoreCameraDevice_Clone_m154E1995D8C0B23A6C919439D3DFE4AD1B385356,
	ARCoreCameraDevice__ctor_m97A779D94986EFAF9D2365AEB66709773A20E048,
	ARCoreCameraDevice_isAvailable_m4C3E38AE9614E3C6642E10513D9A8A023D169941,
	ARCoreCameraDevice_bufferCapacity_mABA1E15C2F8C19232A4E5B0A5ACA8615BB050E15,
	ARCoreCameraDevice_setBufferCapacity_m504C5FCAA5DDF61C367BC5819E0AD5DCD999D872,
	ARCoreCameraDevice_inputFrameSource_mFFBE03DE3E9894DDE9191B1DF6CDD23E3A35D8DB,
	ARCoreCameraDevice_start_mAF98A0608B9DF3ED5AD41DFF2E5A0A08119E626D,
	ARCoreCameraDevice_stop_mE5942D1D3CAC436C26B19CA645C90060C75A2263,
	ARCoreCameraDevice_close_m4746EE08EBFAB18FE243F5FC8D14E2AF64A3A5D0,
	ARKitCameraDevice__ctor_mD0EFB0112A261CAFB92BCF50298886DC2DB59699,
	ARKitCameraDevice_CloneObject_m6EDF36139DF8EF6373C72A151863287C34E87A35,
	ARKitCameraDevice_Clone_m1DAE4E44FF0C99BF6404BBC94853F9A41A0A13A1,
	ARKitCameraDevice__ctor_m124295615BD89B0D2A0AA1C2F3C1EEBDDAA74DD1,
	ARKitCameraDevice_isAvailable_mBEE5938470583779D88EE88A04A867E9F76CF659,
	ARKitCameraDevice_bufferCapacity_m98D2950D04ACC6197AA8148474B704CC0C10C464,
	ARKitCameraDevice_setBufferCapacity_m994843D62AD2EF780C717F46348D61C78F9D9505,
	ARKitCameraDevice_inputFrameSource_mFADE99162C50689038FDE4CDA7416A9953837C95,
	ARKitCameraDevice_start_m34CAE9379ADA7CBF535F3904DB74815764167061,
	ARKitCameraDevice_stop_mDD176A7C6567B6E1989C3F6C0304E036FAAE8BBF,
	ARKitCameraDevice_close_m1AE33B621A2F1B7681FF446C132366CC6691023D,
	CameraDevice__ctor_m1842F47EC62FB0B20307493D830C2C116AE2F038,
	CameraDevice_CloneObject_mA4BC78AD457F5D89A08D54ACF7AE8B050E8E4638,
	CameraDevice_Clone_m49EBD72310753FC722F08766D6E24E350DB40B3E,
	CameraDevice__ctor_m2FA90E19434E70B07C24D0AF21104D663E2F1950,
	CameraDevice_isAvailable_m530C3C01B29037B2C33A29B9A163480051DD085E,
	CameraDevice_androidCameraApiType_mE072F58FA31D2E6F6CF5CBF6D9CC7BD6321B1FD7,
	CameraDevice_setAndroidCameraApiType_m55F24CBED1BD6D31C3E1589E6EE51E3ECA78EC7C,
	CameraDevice_bufferCapacity_mFD4121307C06A0E278B7E00D35FA2BA45E99D4D8,
	CameraDevice_setBufferCapacity_m80919A3D1D2D80A03D64A171284BCEB1E119BE78,
	CameraDevice_inputFrameSource_m8ED6208393DF0A3DC5AD60915BFAA10594546239,
	CameraDevice_setStateChangedCallback_mFD90D217DD3B9DEC8DA74660D5417619B89C8362,
	CameraDevice_requestPermissions_m385B7DEF976F549473CB3296EECDEDBD829BF189,
	CameraDevice_cameraCount_m48AEFEE678EC050FA4EB8C9C6E16F7E649519D56,
	CameraDevice_openWithIndex_mB70EFF4A8DCFB0637FB0DCEC78980A3174DE37BD,
	CameraDevice_openWithSpecificType_m2B141E413E594595D88FBD9E74BA743A49797169,
	CameraDevice_openWithPreferredType_mCF3726663592987E7CAFC9E2FC89B07BF91F7DB4,
	CameraDevice_start_m5CAFC134DAD5CB327CED9F74B0F4D8530E8A496B,
	CameraDevice_stop_m81217BDFB7DF03EC81A2D4CF69D30FAD12321F91,
	CameraDevice_close_m26E2825C074FB54440C490DE648A76898C8F7212,
	CameraDevice_index_mC31BE8B9B0E6EA7CF28033D8C4FA86ACD0FCBC1E,
	CameraDevice_type_m3E8352C6A47182939DDF3401AECDBF4493F7C2CF,
	CameraDevice_cameraParameters_mA44C17598E2FB8E6A7142CEEAA0D204581CEC830,
	CameraDevice_setCameraParameters_m0A10FA5A9AB96A06F57BEDD37F2210228854DDF7,
	CameraDevice_size_m3C2FA97FDB64ECE7425366801CE8D4DAFCA23A81,
	CameraDevice_supportedSizeCount_mC58FB766DCE1CC1042C901FA71217797FE5F3143,
	CameraDevice_supportedSize_m075A98541DBADE0D29512AF513AC248865816D2D,
	CameraDevice_setSize_m21926089D451365F41FAE83EC67C4BA256D95E25,
	CameraDevice_supportedFrameRateRangeCount_m304D120ABE82081B5B5B5644266A6FB18C5100A2,
	CameraDevice_supportedFrameRateRangeLower_mA073242AC55A29E22612A898D38BB30D0E25C5EB,
	CameraDevice_supportedFrameRateRangeUpper_mFF7615371ADD28916C4A70DF34FFCF37F415C05B,
	CameraDevice_frameRateRange_m8E2244301AC2EA334BCA7D1E11EB793D137A0065,
	CameraDevice_setFrameRateRange_m82B29185FEE6179E8996C98B6D431C824CF9DBD8,
	CameraDevice_setFlashTorchMode_mF2EA25CCE90F281FAFABCC4AC8979F0F65262EB7,
	CameraDevice_setFocusMode_m6B1965177C3FE08A0D15E0F4A0CDA62E4332CED4,
	CameraDevice_autoFocus_mAD96E178DDA92847E9BFF2957927802E05D42945,
	CameraDeviceSelector_getAndroidCameraApiType_m84C3FE4160A5A3DDF90FD6F7D9F0288C93C5E31C,
	CameraDeviceSelector_createCameraDevice_mD8DF5A9A0BB0D6C1CEA99490A60EBA3BB02BA6CC,
	CameraDeviceSelector__ctor_mB5940F284C304E7C909725F60331A90468B5D289,
	SurfaceTrackerResult__ctor_m6C768987542EBF5D2D0F5A063DF08E64798534CF,
	SurfaceTrackerResult_CloneObject_m4923C4B7F51DF5D34A5B49CCB7581E5B98C406BC,
	SurfaceTrackerResult_Clone_m361B9E32617F25B0DF171F756448A66C4EEFF5BE,
	SurfaceTrackerResult_transform_m5521BDA6005E280FF45CA891EEBD0A82BA1A9440,
	SurfaceTracker__ctor_mED2AE264CE231828529ACF8C6AF384C06F2B6107,
	SurfaceTracker_CloneObject_m8275D15A247DC0725CCF9A4AC1131D1693B31914,
	SurfaceTracker_Clone_m571A1C2A4EBC0A603744E76352F8693C5A9ADBA7,
	SurfaceTracker_isAvailable_m7EA290BF53FC8CC57B12E566D85E3B87DE873E74,
	SurfaceTracker_inputFrameSink_mEF5A5EF815AC8399B6E7AE4661C53F6E561D3204,
	SurfaceTracker_bufferRequirement_mD181F96A05C50777B019FD4BB80639D1CF0661F4,
	SurfaceTracker_outputFrameSource_mFC28738A504820CA1A5F015D2D42C507D9191D7C,
	SurfaceTracker_create_m0DB6DC130EFB7BA72DE462D893DCFE7C6EE12FAD,
	SurfaceTracker_start_m0D24241B0A9CEEA9C337E7C099E6D431C7D4C350,
	SurfaceTracker_stop_mA4C0AA1E5BE654DC35BBBE8CEBEB33C0C19B31D3,
	SurfaceTracker_close_m138B26AA98A03C39235091F376293CA224052117,
	SurfaceTracker_alignTargetToCameraImagePoint_m42A9A94CCE72FAA50DEE22257EC812A8A07BCE57,
	MotionTrackerCameraDevice__ctor_m57B5A7240CC9FE7C81612AA12C5A419C06708589,
	MotionTrackerCameraDevice_CloneObject_mEC2B625CA42C33B1F1ECBA50067DC006BBE08436,
	MotionTrackerCameraDevice_Clone_m7EB25CE42FE5634809404EE32A80F9EE2E1F247E,
	MotionTrackerCameraDevice__ctor_mC8E12CC08516C5CC18A57C3B0E8F5CA427B25494,
	MotionTrackerCameraDevice_isAvailable_mD600F0E6699F38E9F082D17FD5E7E5F67893066E,
	MotionTrackerCameraDevice_setBufferCapacity_mC92A34BF2C83A300879AFF22328FDD6DB8DE5277,
	MotionTrackerCameraDevice_bufferCapacity_m28B1EFAADA72D6787D403569D6C35DCC62E45BEC,
	MotionTrackerCameraDevice_inputFrameSource_m52E4498BB374B41389B82154A3E1DAA5404741F3,
	MotionTrackerCameraDevice_start_m4CFC0000C706FA21752FD405B3CBB82EAE88DEED,
	MotionTrackerCameraDevice_stop_m4A94DD450862FA09739B1D612056D58E60D54997,
	MotionTrackerCameraDevice_close_mDFA3402D5FD04D0264E628957433C22968216FA4,
	MotionTrackerCameraDevice_hitTestAgainstPointCloud_m26BBC2693B10BFC7EABA4DE341277D448446B90D,
	MotionTrackerCameraDevice_hitTestAgainstHorizontalPlane_m072F40350A5A085A7A34B34B4C57EDCBB6075014,
	MotionTrackerCameraDevice_getLocalPointsCloud_mEBA0DA3DFA8C8A9735FAF4DFB75B36ADE3C84B7A,
	InputFrameRecorder__ctor_m43BE1319B97EED39D5E96A35430A1ECDBEC7BB93,
	InputFrameRecorder_CloneObject_m44F7EC1E1A08BFAF134ADCEC1264194417BAB102,
	InputFrameRecorder_Clone_mF4ED1C7DC454DD071B56368E057F7CEEB238D24D,
	InputFrameRecorder_input_m2342B7BC3761D54FD5B868017B4E2D1E86E8CCC1,
	InputFrameRecorder_bufferRequirement_mC3AC54044769739504C0608EB2BCAAB17DC967C7,
	InputFrameRecorder_output_m2D2D1282EF809F661E7E92F02FC02F5249D0D07B,
	InputFrameRecorder_create_m45FA4818FC9A27A0C29748659819B8100FE3CB35,
	InputFrameRecorder_start_m0439D12F4274989AAFB62F8550652899FA31ACD7,
	InputFrameRecorder_stop_m24FBBE5EF3FB76235F706AEAC817C563FFBA6EF4,
	InputFramePlayer__ctor_m99BD91151E38B291CC233F90CF1916BF79584F41,
	InputFramePlayer_CloneObject_m8FC2937E2F33AF3485B0A6613319F2D2F96A8F76,
	InputFramePlayer_Clone_m54986064E9B8A24C321EBA60C454E170124E8E02,
	InputFramePlayer_output_mC104378474F3A04A5214177508BB57586662A487,
	InputFramePlayer_create_mDE0AEFE1F079FEA346270845DA4B58716CE8C390,
	InputFramePlayer_start_mF01ECBE35EB399B163D73A3C38A697731271A8E9,
	InputFramePlayer_stop_m23E87DE21FFE07EEA9C9A28E5C8D7F80FACB7D66,
	CallbackScheduler__ctor_m5828981066B852891E3A9DDF0DD34839E41A10B5,
	CallbackScheduler_CloneObject_mD515F2E036E8DBCC7F2EC3B42524A22C254E8879,
	CallbackScheduler_Clone_mC584C80A8EDB68EB4B3563DD390C325C19117ECB,
	DelayedCallbackScheduler__ctor_mEC400BEE74C0EF24D1A29020FB96A22C784623FC,
	DelayedCallbackScheduler_CloneObject_m744C039C8B1DBC7524205C16FD689A1EB0768990,
	DelayedCallbackScheduler_Clone_mF957276765CFC926261509EF13C42B75C8D353B1,
	DelayedCallbackScheduler__ctor_m8C635EB11F38F5131DBD050E13B1539E5584187C,
	DelayedCallbackScheduler_runOne_mA003E232CA0E93ED3BCE128CE7672E74535866CA,
	ImmediateCallbackScheduler__ctor_m4C1B7CD9AB1B4D57A623D179DF1B4AD3DD64EC1D,
	ImmediateCallbackScheduler_CloneObject_m3E0B3059A719E1E92A7E9515748C177E07C1514C,
	ImmediateCallbackScheduler_Clone_m887060FBFD1470965001F596BCC9605BE3F05997,
	ImmediateCallbackScheduler_getDefault_m24BB5A3EE463D63163025DFCE487507C7373BBB4,
	JniUtility_wrapByteArray_mF6185FC87B0C7B8C889685810E0B31B7C55D2622,
	JniUtility_wrapBuffer_m1E9C149CFFD94D7CF96247D8469D57CBCF40A5AD,
	JniUtility_getDirectBufferAddress_mD5B9B192FE7389A473FD13324253E2B37DEEB251,
	JniUtility__ctor_mF515E0A8542C84494F29A038D34301498809D74E,
	Log_setLogFunc_m81B30F66F50C42B99154E24B68FD697D7C5C3B62,
	Log_resetLogFunc_m51CBE380CA4BD4277690293A734C73D098C28C4C,
	Log__ctor_mC9423CFE26EAD59EE4CB3911F79189E176967165,
	ImageTargetParameters__ctor_m4B2EEF4D080B77098494ED10C7E083EB96CB71F3,
	ImageTargetParameters_CloneObject_mA32BBAD758D63894972386358ADB1F38FBE1706E,
	ImageTargetParameters_Clone_m00AA35FD6C2E0DC13A7A05D24444BB2290D832E6,
	ImageTargetParameters__ctor_m5B0F12026B64B604BCA8BA6792625F3CF8ACF574,
	ImageTargetParameters_image_m605B7A73AF89D700377406A9DCD4283855E7C5AC,
	ImageTargetParameters_setImage_mD70665FDD7BDD1A47037E2DF625373497E935182,
	ImageTargetParameters_name_m8C26F413355DCA95BDE11985D49B1C031FF2537A,
	ImageTargetParameters_setName_mCFE9DCDF8B314B6ECF48B321C0B88A14973069CF,
	ImageTargetParameters_uid_mA75CB571167FACE277D523DFBA11A1F31749EEAA,
	ImageTargetParameters_setUid_m710956A899A31881C4A4E59DD1EF12EC0B92A4E8,
	ImageTargetParameters_meta_mF9737B4162C835D67D45CB5E6E44258DB207E281,
	ImageTargetParameters_setMeta_m343B785EAED5DF67642903B216DD1AA7341BE297,
	ImageTargetParameters_scale_mA7BEAEB95302A9CE9CA75983E0FF3FDAF95454FB,
	ImageTargetParameters_setScale_m576CBFA4531FD6029004AEB72E183041024ACE50,
	ImageTarget__ctor_m47672761C80BC4CA7A448EE4012F0A5A6D9802B4,
	ImageTarget_CloneObject_mA66826695A6E4E0982E83659391DBD242338B269,
	ImageTarget_Clone_m8F11DFEEDF09754FA10C67532E4D602C2A239077,
	ImageTarget__ctor_m658DB811C59216FCB059EF3B2E0E0618E7A73469,
	ImageTarget_createFromParameters_m3689AC4639EC0AAAE720F337E04623629DC8853A,
	ImageTarget_createFromTargetFile_m9C102DA5FBDF3B51145DA1C1A29658570AC7E758,
	ImageTarget_createFromTargetData_m5DE83144C00A9A94264430B5DBD9FB684905276C,
	ImageTarget_save_m235CF3791319CFBD49DC0E2839C1B9F056C8BCF9,
	ImageTarget_createFromImageFile_m362E0BE082329B37728E57FD2AC45EA11C656BEC,
	ImageTarget_scale_m8DE7CA1940ED8B4E867137F10BAC799249BD7EF9,
	ImageTarget_aspectRatio_mBB18288A2A18753EB105857FFED3FDBAAB356C70,
	ImageTarget_setScale_m4FC8D6296F36C79AEFAAFA6984C6DFC4407244E7,
	ImageTarget_images_mF8607048B4D9629F355080F267AAC17D4D712FEB,
	ImageTarget_runtimeID_m83A7796F2AD354017DC95B27E4AE616FA440F420,
	ImageTarget_uid_mDB9E38D77A302595B863317991A1A5B2E9CA9881,
	ImageTarget_name_m32F4BE54EFC7609B7DFEFBA13015F94EF376FFFB,
	ImageTarget_setName_mB13762EDB0E4F7315F2827DC8CF7878D7A407CD0,
	ImageTarget_meta_m3390E50ECE00FEC9C770A05982C6A6806CD7E6CD,
	ImageTarget_setMeta_mD7E88D35EA400BF1DBA0B0BE670DDA9AB488D5A3,
	ImageTrackerResult__ctor_m07D03C71193714D61F8202FB43573E80C0BB7484,
	ImageTrackerResult_CloneObject_mAB071B65E9986B1D6C1318D80604D36681A7F96E,
	ImageTrackerResult_Clone_m901701FCE18B7BBDB8F905A04B184DBEA17C2F51,
	ImageTrackerResult_targetInstances_m93E0A7439778818844091B82B85407483FF66662,
	ImageTrackerResult_setTargetInstances_m8D27851FEB96C2D538B9AA599FBC97E0CDF35EA2,
	ImageTracker__ctor_m3006366FE899F8DA2D642CB186B3BD41EDFDE9B7,
	ImageTracker_CloneObject_m6454933A905756F1754CFDEF0B50C9B5C75006C6,
	ImageTracker_Clone_m7AF75A53B97930291D8FCA9D5A7DF8133AD07E40,
	ImageTracker_isAvailable_m74AD1EDAB965AE892A9F4B7C164B246DA6726A32,
	ImageTracker_feedbackFrameSink_m61FDC233D2298EA58F7F94866DC3C045E1C6D08E,
	ImageTracker_bufferRequirement_mC5D61F7C0D0520A8C5F601A6BD0A353B2E761C05,
	ImageTracker_outputFrameSource_m68F1C7B4AACB2DE6C0169770904A7FD86242FD37,
	ImageTracker_create_mF941F588172564074526EF4D9271C6369818F55A,
	ImageTracker_createWithMode_mF0A7621A2397E62089AE274F5A4720A98DFCD1A8,
	ImageTracker_start_mDAC8D2852D32658AABE0AB3C44E378EEF22B031C,
	ImageTracker_stop_mD26F43D3027BF4FFE002E6ACE6540D67EB11CB41,
	ImageTracker_close_mF9113528B070B6212778E6BAC6C4997766E00C45,
	ImageTracker_loadTarget_m66A0F12CA832EF99D867D39AEB61C43BF9A5E38E,
	ImageTracker_unloadTarget_m01A8E69115564BD328B3FA5AFF6D7F1A9E7EABCC,
	ImageTracker_targets_m2CB1BBC5BAB16AB626D2D3209E1C4BA811DAF239,
	ImageTracker_setSimultaneousNum_m544539088BC598808329127B3115C5DE0BB01122,
	ImageTracker_simultaneousNum_m20CFA25ED64B0E523945826527FE5E98C0DD92E0,
	Recorder__ctor_m24BBAFD0585A7E34D3A957D313DF9E04B4264C12,
	Recorder_CloneObject_m83CCD2759407A0BB009DE1E0C34B676D6F451E6E,
	Recorder_Clone_m0DF5EAEFD2B3AEC96F55A71B3A52B1CB234DBB85,
	Recorder_isAvailable_mFE1489E8BB02F3C705F89F223A3CC4BAA4987EB8,
	Recorder_requestPermissions_m577700A23BEAE10166A5973D221A6C0CEF606498,
	Recorder_create_m2A4B121D0D90BAAD1FB327A2CED247D16FF3C73A,
	Recorder_start_mDDAD6CC4548015B1C9495FAA701BB0CDF77B1374,
	Recorder_updateFrame_m6FA968FAA00FDD65BA56ACBA11B2B00CFEFD8E6E,
	Recorder_stop_m1EB17B6A34CADD4C11F1CA960B7826DBD5E17220,
	RecorderConfiguration__ctor_mDC7EE1124413E701C3983ECB1282EEFCFB850920,
	RecorderConfiguration_CloneObject_mF3ADCEDA812096392908E46B5AE7C548CC710C07,
	RecorderConfiguration_Clone_m332FA07444B984EA4BF601F54E9A103CE560D5B2,
	RecorderConfiguration__ctor_m3DD2857E8F0704C9B1D851BFF4FB8F2D790AB202,
	RecorderConfiguration_setOutputFile_mA4C04B34F6C45F4DF23D78D8AFE53DF077DAE3DD,
	RecorderConfiguration_setProfile_m8552363569DC1C5D384B06E8CD7B3E29A287E3F0,
	RecorderConfiguration_setVideoSize_m05E9D0429927E863CC61228C913A079248833F4C,
	RecorderConfiguration_setVideoBitrate_m2BEE30F6A7E3049E9B9BF167880F616208E6E9E1,
	RecorderConfiguration_setChannelCount_m65360B2BA00C9E7879BA257F78AF3EC6782D318F,
	RecorderConfiguration_setAudioSampleRate_m76A713DEEC41EE3169BD715970D9E4E90B7A0476,
	RecorderConfiguration_setAudioBitrate_m33827C31259D8452AE7132A864A68B8634F393A2,
	RecorderConfiguration_setVideoOrientation_mF4E6F062EAA7293E323F5F2E07FD5AB8C908AD65,
	RecorderConfiguration_setZoomMode_m0EDB4D7114B019AE9D2C07591B908A8544B5857D,
	SparseSpatialMapResult__ctor_mA4BC247C4F5E166125C827FDD37BE166F4E0FE87,
	SparseSpatialMapResult_CloneObject_m00812B106E2844C540BD3877FD9F258EC6A5A6D5,
	SparseSpatialMapResult_Clone_mC008190C1F980CC88CD385C0B3DCBF0FC2491E6A,
	SparseSpatialMapResult_getMotionTrackingStatus_mBA954A35702019855B1EB95C05B3160A3B8A4541,
	SparseSpatialMapResult_getVioPose_mB2B5D20D452484D10F16D29BCB2526D87E5BB47E,
	SparseSpatialMapResult_getMapPose_mED1F7294E40EB7E69327DB5B0EF5229C12D1C3CC,
	SparseSpatialMapResult_getLocalizationStatus_m6932D0664FF538839690059CEE0C8106DAD0BA4A,
	SparseSpatialMapResult_getLocalizationMapID_m7250B8714B38DB5DA58345A628B59CF2443533DA,
	PlaneData__ctor_m9D4519A545C478A3E36C82BB5FC1EC4C9029D8BF,
	PlaneData_CloneObject_m7490952E9D264F0F2F2C1DB75479FD647F575B84,
	PlaneData_Clone_m46275770937516CDE988ABB62A75C420C727A401,
	PlaneData__ctor_m2A1DEB44030FD736200F90D8A3F97C2792B58206,
	PlaneData_getType_m355402A12E0B86A5A2AFD628BD09DEC4562B9B8E,
	PlaneData_getPose_m16F65C234918F0F37E5131A207664C6B08E1D6B9,
	PlaneData_getExtentX_m2485C8B6D55C8EC98A60663C2641F529830C91BE,
	PlaneData_getExtentZ_mAEF9B0C55FBB4346A9B82461E734CACF8AA924DD,
	SparseSpatialMapConfig__ctor_mBA3BBFCB75C9B959080A7DFB868CABBE16FCBA43,
	SparseSpatialMapConfig_CloneObject_m9200281DEA7765F2D151B2A9B6E51DA0F6F827EA,
	SparseSpatialMapConfig_Clone_m629986273F8B3BD9CCD28AE5ACFBA648694BAC8D,
	SparseSpatialMapConfig__ctor_m74F38BC5029D2AE098A5B4B3F09C46F2AC2D6EFA,
	SparseSpatialMapConfig_setLocalizationMode_mC92EDD1DC4A8A12252008F0751312CF87A81855B,
	SparseSpatialMapConfig_getLocalizationMode_mE7CF51AC93F699DB644AD407038ABD161FD826AE,
	SparseSpatialMap__ctor_m66B259AF104470FA4512787F5F139679103A5D22,
	SparseSpatialMap_CloneObject_mE00EC86831A8EAFD0BC468431E9A6D3D5130AE80,
	SparseSpatialMap_Clone_mA9BDB069BC44523B561EC0151295E81893B5D867,
	SparseSpatialMap_isAvailable_m05F7E213365A882F09C801E746DDF7358EA6511D,
	SparseSpatialMap_inputFrameSink_m0609B8ACEFBCFC9E8EB4B8BD897B87D358A9691F,
	SparseSpatialMap_bufferRequirement_mD5FF3D3866A8EA0BC258A9DD885CF0E6C1A1EFC8,
	SparseSpatialMap_outputFrameSource_mC2D376662B0031319B30F1461BE4EDC89990FBF4,
	SparseSpatialMap_create_mD69B1E5269ABE6ABCE868B1152C5758A3B1435E1,
	SparseSpatialMap_start_m6CD2210FC9DA80017D66F062F39514D0B3B85915,
	SparseSpatialMap_stop_m4F45D6DC38B92E8C5CAB92A713EF85C57FAD292D,
	SparseSpatialMap_close_mF8031C824F5492ACA91C7A6C5E4C73E54566DA51,
	SparseSpatialMap_getPointCloudBuffer_m53512408C414B6810263013CB37847926306E6CF,
	SparseSpatialMap_getMapPlanes_m93C5DCA030AE9D94CE35DBC74CFC9EE50FA3B276,
	SparseSpatialMap_hitTestAgainstPointCloud_m47F18FFB6719E17683DDABE157E8AA8B05DF5631,
	SparseSpatialMap_hitTestAgainstPlanes_m3EDA7AE162B3FAEE3F11B2DF1B8695A48D1E7761,
	SparseSpatialMap_getMapVersion_m4DB7F35B8391E60064C3F2B1EE92DFA8CEB7BF99,
	SparseSpatialMap_unloadMap_mAA34DEE45585A73737CBA25DAC9229BE1D42587C,
	SparseSpatialMap_setConfig_m83A36E83654FDFF6A19624B0CC20C5A45DD9C5BF,
	SparseSpatialMap_getConfig_mE7D3F22C28F705D00A28D237CF0C487D89E32EDA,
	SparseSpatialMap_startLocalization_m674599FDE04A4CEF1EB94AD835827D6ACE774D5C,
	SparseSpatialMap_stopLocalization_m60376EECB68CB4E2117E0D5DD2199FAE3E6F3EA2,
	SparseSpatialMapManager__ctor_m3502FC968803032AAC163ADB66A5B55C84254C7B,
	SparseSpatialMapManager_CloneObject_m23F60ECF959B993CD423A83002FB4C9A8FEE268C,
	SparseSpatialMapManager_Clone_m338B3F03208601E3F7ED757B56F8DF5A4FC72851,
	SparseSpatialMapManager_isAvailable_m2D14FBC78D6EA442DF83F84F846817367664098B,
	SparseSpatialMapManager_create_m116CF7A2752EAEE001A7507CC2F0B1EF2BBD2F8A,
	SparseSpatialMapManager_host_m5A278F9F1E5421C29AC6A7CBC2B84C62FF627696,
	SparseSpatialMapManager_load_m63E7431E4838246C195B185ECBE58B81F8D85B6A,
	SparseSpatialMapManager_clear_m6F50BF5DB09E0B08F65344B6FE7D520CF8BD542E,
	Engine_schemaHash_mDE0A9124DB616EE38D83778ADE27D9A754CBE997,
	Engine_initialize_m113B4F8EB0D249DD2E54256D2A6A3924EFCA5039,
	Engine_onPause_mF9C3B0E7C4F9FB4F65CD0179867490BACCF7E5CD,
	Engine_onResume_m8B1F26F9B090FB4E53CCF1F00CBE8C4CFADF0D3A,
	Engine_errorMessage_m93AAADF726BD4852C6E5C13C330EC6ADB5700C49,
	Engine_versionString_m96AAB5C7799CDC695A30E9555BA6E5F1C706CBA3,
	Engine_name_m44436C2B639646EF4731153EDB43E1986A8DA95E,
	Engine__ctor_mB9E0C7F1EA878B92AF299CF834E91D8FAFD40C58,
	VideoPlayer__ctor_mA247F342C46FD2341AABB764248D0FD6DC0E119A,
	VideoPlayer_CloneObject_mD64A210D02B19DB00CC9E7FE69A698B35845DC79,
	VideoPlayer_Clone_m00CF883D3231692A5ECF904837DA6D4B54A7103E,
	VideoPlayer__ctor_m57B73DC8880F6092B296E53A717B8C8FF4E86328,
	VideoPlayer_isAvailable_mB909B07FCE8FB30E650EC544E387F263878620CA,
	VideoPlayer_setVideoType_mAE963160EF021CC62AC151F941C1CAE50E78F32D,
	VideoPlayer_setRenderTexture_mA237E3BBC2743F73DDC3A5424DF4CC239BFB53E1,
	VideoPlayer_open_m0CF5AD174CA080BC29C191D0207DA51EA328B687,
	VideoPlayer_close_m5B35B8B990E75FADAAD3CFC6527CAAF3245E2CCB,
	VideoPlayer_play_m0A00C116D0D9C93BC7481B06442B338C0F5B20AE,
	VideoPlayer_stop_m611995F8AD20D0352A6124B311109E8145D1F647,
	VideoPlayer_pause_mB3A6E9F7FADF6F9DF57F3A0CD07148909505F9BA,
	VideoPlayer_isRenderTextureAvailable_mC05D088961D5DAB705561A4E87C4A2DE19938D21,
	VideoPlayer_updateFrame_m0482317223BD922A6241C7B647723642DBABDCD7,
	VideoPlayer_duration_mBD6AD6373070D675B751D479EF5FCCC1A2FD5961,
	VideoPlayer_currentPosition_m694972312AE61914F927DD1AC201DAC0B891796C,
	VideoPlayer_seek_m026D4018AD54F8EECA2D76560EED8EEF47CB9C36,
	VideoPlayer_size_m1D239D3B4223DA17E5B3F329E9DB068B6CFC10BF,
	VideoPlayer_volume_m2F0A182CA56F87A7C0145A703882D76E124C8B5F,
	VideoPlayer_setVolume_m5FA786ADC0BBBEBAECFA6BD427C37FAE91005B3F,
	ImageHelper_decode_m457183EBF0EED91873C4AC25F65976B7652FD6FA,
	ImageHelper__ctor_mF2DC62187A600AA031FF40043006CBCCA8C75DF7,
	SignalSink__ctor_m4ACEA8E70A1700AC6BA14C892E56545753098C63,
	SignalSink_CloneObject_m40086FAD4AF9CAF5ACCEE8C18666E356FB54C559,
	SignalSink_Clone_m247D93B98543F5DCD107C257BD27853BBFF98FA9,
	SignalSink_handle_m8C188E1F92BE6FE787AE22FFE08AE1FBE6E70066,
	SignalSource__ctor_mC07524B197A5868FBDEFA5C5894412355BEAE91A,
	SignalSource_CloneObject_mA99E154F82FD574106F33FE624FFE80F7F0210CD,
	SignalSource_Clone_mF06117BAA9D740221A79597208D9A569C1EFD1E9,
	SignalSource_setHandler_m67B2FBA103E1EBD03AB957CAEFEA87BEB95F72BC,
	SignalSource_connect_m4EADC28E5EC6A1C5B2F6DB66E57C537E30AE4814,
	SignalSource_disconnect_mB3ADF29CF000BBBFC11923B23DD789498383EC33,
	InputFrameSink__ctor_m88632D7ED47C64CCBD6AF4617E002C1B44AE02D4,
	InputFrameSink_CloneObject_mDEA9635D423B3E7E29FFF7658B880CD5342C91C5,
	InputFrameSink_Clone_m18993A3807463BB745E1B50176505B682E0EBA86,
	InputFrameSink_handle_m71E7D7E35DE21A715D98359D4B9F433EB95A667A,
	InputFrameSource__ctor_m9F68CCAB39A195817FC38430C1FBA923AE155D93,
	InputFrameSource_CloneObject_m7F873DAEB20F2EDFBFDB43B97DC25F599F4466F0,
	InputFrameSource_Clone_m5F747EE58A8327B3B609087419D4275DFEF289A8,
	InputFrameSource_setHandler_m6B7EF3939CEB30CD95A18CE665DA02BA6A5BED16,
	InputFrameSource_connect_mCFBD80617E316C9B893A9C12B61DD499277727AD,
	InputFrameSource_disconnect_mAFD6888A5AD6CAE3297D92196F63B367AD2F8D6D,
	OutputFrameSink__ctor_m1D615F4665F246F41254AE70204870423C625F0B,
	OutputFrameSink_CloneObject_mCC9626A433AAF6193163B2C455934A9DDD040268,
	OutputFrameSink_Clone_m7D796DE18A7EB1948B63DF5859CBD6DE6FF29170,
	OutputFrameSink_handle_mD94577A8D567E70EB1B2D8AC0DB018F3C82828D4,
	OutputFrameSource__ctor_m858251CDCCC4BAFBD7126CCF42046BAA8E1EA72E,
	OutputFrameSource_CloneObject_m176D7DC58BCDEC00DAEA1B74FCD5D88C0A50F95A,
	OutputFrameSource_Clone_m552D85C438648F2220D46904671246F1A971432D,
	OutputFrameSource_setHandler_m21A053591077E76CF71429D1B7828BEC712083C7,
	OutputFrameSource_connect_m833158CD37FAEAB7A5F82FB14DA132B7B15624C8,
	OutputFrameSource_disconnect_mD5AE439979D835AC5D7E0704FB408BDF61075592,
	FeedbackFrameSink__ctor_m509E6A9AF4EBEC6D9659B3F8667E471D89A2787D,
	FeedbackFrameSink_CloneObject_mF013BAD27AACBD3B6AD6F4BBC1A6FF727E28F2CA,
	FeedbackFrameSink_Clone_mEEBA4919A97D24758D6AD7BFA90876E059E744DF,
	FeedbackFrameSink_handle_mD8C9FEC45A81D6698D777B4EF999E50BBE31850A,
	FeedbackFrameSource__ctor_m7D0CBE5E01694294D549099D527A634B8551D42A,
	FeedbackFrameSource_CloneObject_m3D82575EA7A7FD691B850567523E59B864CB9936,
	FeedbackFrameSource_Clone_m92ADF610D913A4D15AD68AD5F7E62EA7E7E2E716,
	FeedbackFrameSource_setHandler_mC7A8FD386CEEC7D4EED6D8B26C9F17AB5A7D3DD8,
	FeedbackFrameSource_connect_m4CECA52212A7D6958907333C1E7068367ACF0F46,
	FeedbackFrameSource_disconnect_mD547DAA6E9500C4057971889FCC9B8E77F7F811B,
	InputFrameFork__ctor_m31C4AF1D64322CE21B8C39748CE00CCAE20E6701,
	InputFrameFork_CloneObject_mD29FA3B2CEC0AFFD579EAA8EF258CE411083C67A,
	InputFrameFork_Clone_m5F4849547B1D06CABD38EAC59A7965599677EF80,
	InputFrameFork_input_m4B4F6923AFA19AEC60689C158A114AE34E3F02FF,
	InputFrameFork_output_m6E4394E11B9D781D3616972837D054BADF8362A2,
	InputFrameFork_outputCount_m1A9E823DC07929A3B626AE937FB924A8AA8ACC04,
	InputFrameFork_create_m01A864F8112E0A72C4C236AB919980FB97D038B3,
	OutputFrameFork__ctor_m72705BCF50C1D32F45E3595BD6162BB6CE2EC36C,
	OutputFrameFork_CloneObject_m1CBE7616D46D37422697BC02D7527E8769FB9E35,
	OutputFrameFork_Clone_m26103C12184F36A33DA56892AFF5A004B45B4899,
	OutputFrameFork_input_m0F3568EEF1CCA5E5740A03B7356A7081CE354F73,
	OutputFrameFork_output_m19087C634351387E1739201BB329CEBD890D71B2,
	OutputFrameFork_outputCount_mD5ACC74CA0CF75FB9080CDE757DF4B30BD49823C,
	OutputFrameFork_create_m0075A1F8B9084BBFDBA790397746CF8F306CC6BC,
	OutputFrameJoin__ctor_m3705478FB9C6BC43EDD59EF89ACEBFF88A111B41,
	OutputFrameJoin_CloneObject_mE3E34F57FA70822B54C8555E6BDB83AE20E0B972,
	OutputFrameJoin_Clone_m79DB8AE555E52ECB572530FF3BEE63194BAD453C,
	OutputFrameJoin_input_mFB4B939A3F4DBE0C01AD2A6DC483135D97719A3E,
	OutputFrameJoin_output_mB926DDFEE587647958E6B05CC6EBF7AD320FD70A,
	OutputFrameJoin_inputCount_mE6E75F1E10D804FC0E8CF0F58B77274DA70C57D1,
	OutputFrameJoin_create_m1C16191D0D9720652C32AB917DC5073C608CE5CD,
	OutputFrameJoin_createWithJoiner_mC41C1AFF8490FCBE9195F742BEE4BBB026EE76B7,
	FeedbackFrameFork__ctor_mD920CC84253F98A412D3355E9DFE23A4011D0E97,
	FeedbackFrameFork_CloneObject_m682E5C21FCF4A5B5708DB2C0B9ABD9FCC87297F7,
	FeedbackFrameFork_Clone_m3BD0FDFB83164080A0FDF1E5AE75BD4A1B0C2D3A,
	FeedbackFrameFork_input_m49C44522E438A988853410E5D6AAFD42F4B1565D,
	FeedbackFrameFork_output_m197271C69F86DB2114D1CAA9F33B00E64742FE85,
	FeedbackFrameFork_outputCount_m50AA981198269F7B68C40897D11206510A44B058,
	FeedbackFrameFork_create_mB5025ED3F212CD86687201800697ADA34FE26E79,
	InputFrameThrottler__ctor_m866265EA8FC6A0060347850E9C13B4D4C3F02BF4,
	InputFrameThrottler_CloneObject_mA9BF7B67F29BC868E829A1FF9DAE458AB0E1C4C1,
	InputFrameThrottler_Clone_m57706C4B958CE9EE89A0C9AA65E25497E9DF0C1B,
	InputFrameThrottler_input_m94D75AF9838C223209885402CCFBD54BC126B920,
	InputFrameThrottler_bufferRequirement_m18C83D602C5A0F221C6B1365C3A0F1F72BBA0586,
	InputFrameThrottler_output_m46A672E64DADE8EB298D47137515941B3B1F7D1C,
	InputFrameThrottler_signalInput_mF47FD603C6B5D90748976C87742BCDCC95C34594,
	InputFrameThrottler_create_m7AD4172BD160BF5CD691A69EE0F51B7EA321CB2E,
	OutputFrameBuffer__ctor_m4803D18081D5D7C715EC38CB25D6471AB1FFED5F,
	OutputFrameBuffer_CloneObject_m24EDFB5F8B96A8F2136ADF190E659FB87D0C71E2,
	OutputFrameBuffer_Clone_m9C86B35FA309F89FC9898A8566C7F26EF7E3755A,
	OutputFrameBuffer_input_m57EB9D84C15347550C00DE4E93D2DF2D4689B124,
	OutputFrameBuffer_bufferRequirement_mDA770AFD3C258324C06C2F2CE095AC5798424607,
	OutputFrameBuffer_signalOutput_m8F187474874C0E8EF4BA0DC0FEC85368A25DBCBC,
	OutputFrameBuffer_peek_mB98BBB80C28961E396EA29F631155F6058C0489E,
	OutputFrameBuffer_create_m1BBC60443822E75241CE2E5C738A0777F5CF50FF,
	OutputFrameBuffer_pause_mFDA799F4CB8D2080BA078BFEFB01FA8B00028DD4,
	OutputFrameBuffer_resume_mE290E6CCBF4D4B04005EFBDDC0867B980E71241B,
	InputFrameToOutputFrameAdapter__ctor_m649E7800AD1615C62B84C840808156EB29A7E8E9,
	InputFrameToOutputFrameAdapter_CloneObject_m0AAC88E18811721D296F452DDD2CCE7FD1E26E39,
	InputFrameToOutputFrameAdapter_Clone_m572753DED81F18A7887F81667A599D19270450B6,
	InputFrameToOutputFrameAdapter_input_m90D521E6A36ABC9C1CD1F530C810308EAF1F5941,
	InputFrameToOutputFrameAdapter_output_m32BA43DE8261F264CCC89CB4480F11E00D8A56EF,
	InputFrameToOutputFrameAdapter_create_mA8E7B372C26C1DEFADD989BAA5754FCA475E03ED,
	InputFrameToFeedbackFrameAdapter__ctor_mC5D82BC685A97414734B18BDB6B731439DFA95A9,
	InputFrameToFeedbackFrameAdapter_CloneObject_mC456B6DBCB0DD9BCB0DD0552B1993B62B6EC01B4,
	InputFrameToFeedbackFrameAdapter_Clone_m3411C1C9CC9A13F235EF70AE90B90A2339D10EF2,
	InputFrameToFeedbackFrameAdapter_input_mE4EE78A62225F30255AA91EFCF19EF8F87BF7E6B,
	InputFrameToFeedbackFrameAdapter_bufferRequirement_m991C4E500C818CC0654FE704D2AC6ACEB7E46FB2,
	InputFrameToFeedbackFrameAdapter_sideInput_mF601AA3936E50DB9A2BE90746E50019FA80503DB,
	InputFrameToFeedbackFrameAdapter_output_m819EAA6C737D6077D79CD9920BB4A29BBC0E7EC1,
	InputFrameToFeedbackFrameAdapter_create_m55611E90564970D0D21963BF7EF257C70EFC9968,
	InputFrame__ctor_m24215B39EF551E7D5775A8FC47A2F79F8CBF5F79,
	InputFrame_CloneObject_mD574189467BA37624D677F66495A7E552E87D19D,
	InputFrame_Clone_mECDFF235A5403E4B9B23B66CCC8285EE8944DBE8,
	InputFrame_index_m31CB99958066C9F0A051CB07F89BB204A97520EC,
	InputFrame_image_m094FB87664E53E65E73A46C11397DE0A8C18AA00,
	InputFrame_hasCameraParameters_mBF9E12ED650857216A625C166F0A28D1F839EAB6,
	InputFrame_cameraParameters_m118EDCBEC3037E10DC6D509F5E64EF18AB83143C,
	InputFrame_hasTemporalInformation_m0E8DC0771FE693F4C93E9C8198BCF629F48CCC3A,
	InputFrame_timestamp_m3AF20E9B958D21087F55219CB175D9AD7E421739,
	InputFrame_hasSpatialInformation_mC6E4D5B56C3D80883824F92C3DCE98A86915EA67,
	InputFrame_cameraTransform_mDE134D2196AB2530E1FC56B2AED3CEFB40C9FF40,
	InputFrame_trackingStatus_m08C798033677F59B5F333376DC587D46C520C9A7,
	InputFrame_create_m6462C4D303191D96EA91192DE9170B3939D2AAD4,
	InputFrame_createWithImageAndCameraParametersAndTemporal_m85E729FFB997ED40B6239FD6D3AEF8085B892888,
	InputFrame_createWithImageAndCameraParameters_m11EF707232413645EED98B531BC1533424CE738C,
	InputFrame_createWithImage_mF75B127482902518BBB105202F9FD4D1B8BCA8EA,
	FrameFilterResult__ctor_m67BAED829C5D417AC9206BE3430BF96A383F6365,
	FrameFilterResult_CloneObject_m4B45B2452E913F1CCD22296DF5E633D39FDFC180,
	FrameFilterResult_Clone_mE77802987BB336E83183D22F5F97E55EB45340C1,
	OutputFrame__ctor_m077F6EE0C8A3D6F8288103840C2893106D419C7C,
	OutputFrame_CloneObject_mC09A8BF5F738BA3EB958FC69CA73FB2D77E83D02,
	OutputFrame_Clone_mC55E9346749EBFC46A02E555BF90219D24FCC9BF,
	OutputFrame__ctor_mCD477CE7E9611D2F670E9CB704B4320482C89CB9,
	OutputFrame_index_m3FDA5247D5D319A10C4B407C685730418BFACE64,
	OutputFrame_inputFrame_mD4CB997C9E114F8CD7E0205D9E0CA6070A702B02,
	OutputFrame_results_m94A9C8E45A36AAC3B27A69EEB19F0B2CA9F70DAA,
	FeedbackFrame__ctor_mB25BFCDD9D2AE705F24F128245ED377741FB5E72,
	FeedbackFrame_CloneObject_m2B159AB5B0D86BBE3D3AF01A1B83D129D4B76A1A,
	FeedbackFrame_Clone_m10EA92B244F366F22FB62AA6BDEC97DE34EC24E8,
	FeedbackFrame__ctor_m3ABB1AF9ED33F3CDC011FABE79C068B003CEC5CE,
	FeedbackFrame_inputFrame_mF1B52F49D5E9328367D1D5603AE0825546C4EB9C,
	FeedbackFrame_previousOutputFrame_m60C65489C3837167A187F76612BBA6429B000266,
	Target__ctor_m6909347F5612EB19FBF339197C6261878A4E143D,
	Target_CloneObject_mE204009FF6B55E7D3660D153DCB1977AB105637A,
	Target_Clone_m911B0B393CB2D23ED1BBAA9D4E6B5F58422E5CF0,
	Target_runtimeID_m1062B1EC916E46C768FFC541F7FD5CB8A4DDC47E,
	Target_uid_m3DF3EFAE85F84423319F33B41B679E12ECAF78F5,
	Target_name_m806CF4135F3E2301B5F6878D11182DEAA9DE6936,
	Target_setName_m38C93F5DA33CF523355FD86C64F35305F392DBED,
	Target_meta_m6CBED0A45FE25B9CB31B821B031F406DD8794F47,
	Target_setMeta_m0EA586F52FB4C76E58AFB3DF44CE46EF6AECC5E7,
	TargetInstance__ctor_mA68F872673E14D569B3B7E9FCE871589C974BEAC,
	TargetInstance_CloneObject_m11086BF4997410BEF1A8EFD6BFACF5671B45DE9D,
	TargetInstance_Clone_m8D9380A7F505B4A9F9416FDBCDE8EB775DC104DE,
	TargetInstance__ctor_m481A589433788D3003E8C5EB4D0310BE1E773EC4,
	TargetInstance_status_mB1DA0DDC9A359392D54B33D60B281E17F09CD02E,
	TargetInstance_target_mB860CB73940F8797CF0B715880571C4333BC6258,
	TargetInstance_pose_m385428A425AF9D02B5522E9D964A483C3162CE35,
	TargetTrackerResult__ctor_mD9A1ADCCF5551328BB265D50F66C02B6A5F90E22,
	TargetTrackerResult_CloneObject_m0DD8299678C5EC9CE71675C417CF337E64B83466,
	TargetTrackerResult_Clone_m1764339BCCCCD38478E1829786B39342D169BA31,
	TargetTrackerResult_targetInstances_m8569CCC6EB7B5FECF2F0F5964AE8E33F6F821DBE,
	TargetTrackerResult_setTargetInstances_m83571CA71EFCE300C69612BA409BC7EC6FFA4281,
	TextureId__ctor_m8427277148C404F2719013AA2E9014FFC2968129,
	TextureId_CloneObject_m042531FF0C413B50B9375963F2970132C974C640,
	TextureId_Clone_m948BA0B664D61A72A5039C4AC0F69E4334E0FB12,
	TextureId_getInt_m64FD2586D6B254E13E8F4C9A434402D3B459B279,
	TextureId_getPointer_m3CF5426EAF08F6EFC9BB78D179F2E5513523D3D4,
	TextureId_fromInt_mCADFAA01E1B05D47CD1489F08BE956FC9ADBDCC4,
	TextureId_fromPointer_m28D7B2AABC3DB5FA10E6736E7C3ADE07705DE98D,
	FrameChangeAction__ctor_m5A5D389FA4ECE870CD7506021B60145DC962668C,
	FrameChangeAction_Invoke_m0839F272E0677DCB5D60261937AA6275EB9CA204,
	FrameChangeAction_BeginInvoke_m0E8D139ED5D0695BBBC624A74F8460E14A3D0927,
	FrameChangeAction_EndInvoke_m8E7F142B40F984159A2ABD2D42B3613FFFE72801,
	UserRequest_Finalize_mCFE8CE1F04790A2FD9AC7DDA5F614224D1B698CD,
	UserRequest_Dispose_m54025DE211EA1D9A1A6269EB93632464A5C3818B,
	UserRequest_UpdateTexture_m23FDF524660408E2E40478795D5E99564DE2F8AC,
	UserRequest_UpdateCommandBuffer_m052857A82C0E2CCE7294BE8676491C9CEE71391F,
	UserRequest_RemoveCommandBuffer_m8ADB1878F4C4A040714B7A3A2F6619EC82F95153,
	UserRequest__ctor_mF2CB656506725221414D99675A3542984D199BC7,
	CloudRecognizerServiceConfig__ctor_m46BE3366B21BC9246932700C8F61859DD248AC14,
	PrivateCloudRecognizerServiceConfig__ctor_mE345236DDC5FB8A7BFD25759C909FA40D512C7BB,
	U3CU3Ec__cctor_m87058F2537A82E466B0B046532D010A061106DA7,
	U3CU3Ec__ctor_mC2D138B103980054FC8FC5E932E1A786D2FEF10A,
	U3CU3Ec_U3CGlobalInitializationU3Eb__30_0_mF39DEE2C765DE2F6324F4F49F5CCCA9D070D2DC6,
	U3CU3Ec_U3CGlobalInitializationU3Eb__30_1_m98CDEF5FA28FA903916B3D3C365F3C00A4F475F3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ImageFileSourceData__ctor_mC26D66CCF1E237088FBD007B784FDA0061EC84A9,
	TargetDataFileSourceData__ctor_m4604139C142B75EBB36AC281A7FA3ED4620914F7,
	U3CU3Ec__DisplayClass34_0__ctor_m2B1C0AA63060E29BC9A17AD6802935829F350E88,
	U3CU3Ec__DisplayClass34_0_U3CLoadImageFileU3Eb__0_m11512DC35C849E198E1358FE0DF31AACE8417901,
	U3CU3Ec__DisplayClass37_0__ctor_m97AB424F7C6585CF151125C03087C695DA33D0A0,
	U3CU3Ec__DisplayClass37_1__ctor_m9CC9B220C66455DAFF684BC8319B23D462E64A6E,
	U3CU3Ec__DisplayClass37_1_U3CLoadImageBufferU3Eb__0_mA5740922DE847CC2BCDF06D0FF915E695E7CCB21,
	U3CLoadImageBufferU3Ed__37__ctor_mFB4941A9BDCC41E4F3FA2FA1568CDB63BF2A07C4,
	U3CLoadImageBufferU3Ed__37_System_IDisposable_Dispose_mD0A44D8ACFDA059A99A9874657A754520A01BB7A,
	U3CLoadImageBufferU3Ed__37_MoveNext_mEF9F02B4748B2BB1ABAB14BCDC85C487911888A3,
	U3CLoadImageBufferU3Ed__37_U3CU3Em__Finally1_mADBB8E93A2D85B2C6AE6CA0DAD7C260EABDD434A,
	U3CLoadImageBufferU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FBCB20845252D8BF0D6F10DB3A6FDC3253A0F65,
	U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_Reset_m294E5AA6C40894CF9A1FAF4928CB7E1CB7CDB732,
	U3CLoadImageBufferU3Ed__37_System_Collections_IEnumerator_get_Current_mA8278B5B699B03A7C82406C7B78BCBFA58F981DE,
	U3CU3Ec__DisplayClass38_0__ctor_m62C5FC910742E952157DA9D7AB0CCA5AA9221FB3,
	U3CU3Ec__DisplayClass38_1__ctor_m2CBD2D1294748D16AFF5A05F019E2572055506B0,
	U3CU3Ec__DisplayClass38_1_U3CLoadTargetDataBufferU3Eb__0_m1246457C61690E891639019E91FDCB6489B5C6A6,
	U3CLoadTargetDataBufferU3Ed__38__ctor_m1BC425137726BE6C6660B61AE070CB69887507CE,
	U3CLoadTargetDataBufferU3Ed__38_System_IDisposable_Dispose_m68EC4C6A8A58C76B3E39475CE29A808D6EF2870C,
	U3CLoadTargetDataBufferU3Ed__38_MoveNext_m5614ABC82A7DAE999178F822DCB57B5D1EBF7A56,
	U3CLoadTargetDataBufferU3Ed__38_U3CU3Em__Finally1_m9D40EB9155B96731C3867498C68EAA0DAF979E85,
	U3CLoadTargetDataBufferU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58F16684447CC75951583491B843C3A117E1C1F1,
	U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_Reset_m4761BD5C2F12C25EB7DFEEDB9B8E29AB5C500A7E,
	U3CLoadTargetDataBufferU3Ed__38_System_Collections_IEnumerator_get_Current_mACCEA7A8EC327E5639E373BA69DCEB49E9FF1CA1,
	U3CU3Ec__DisplayClass39_0__ctor_m287DA5BABCC5696E18DC6A847353ADC5258E8BE4,
	U3CU3Ec__DisplayClass39_0_U3CUpdateTargetInTrackerU3Eb__1_mCF0BE3DC27D00B25099707D9097C774C65634961,
	U3CU3Ec__DisplayClass37_0__ctor_mB78C7AC6772E554FA3302D46EA453C331A765384,
	U3CU3Ec__DisplayClass37_0_U3CLoadImageTargetU3Eb__0_mACE0F309654D62AEFE0E80CA437A2FE68F28162A,
	U3CU3Ec__DisplayClass38_0__ctor_m59E8ACC99FACFAFCDD3A9BD2479FE53A9773D42B,
	U3CU3Ec__DisplayClass38_0_U3CUnloadImageTargetU3Eb__0_m7317F0AD5CD15FCA5C93379742EAC0365B8E6262,
	ObjFileSourceData__ctor_m0C444D0207707000755D227015C3D42A7A654EFB,
	U3CU3Ec__DisplayClass35_0__ctor_m72617F2746AA979F60A8CEE6DC30B07A8512E460,
	U3CU3Ec__DisplayClass35_1__ctor_m702A7BFDD199B2D4EA28196787034736D2BDEFA7,
	U3CU3Ec__DisplayClass35_1_U3CLoadObjFileFromSourceU3Eb__0_m783BC0E4CC335B3F26A8843518BD993F25A8B30F,
	U3CU3Ec__DisplayClass35_2__ctor_mD8CAC23DA055755169A21964C702F824EB9C4BC0,
	U3CU3Ec__DisplayClass35_2_U3CLoadObjFileFromSourceU3Eb__1_m980C6497BA62120F72DB202625D5D33C89392769,
	U3CLoadObjFileFromSourceU3Ed__35__ctor_mF664C20DDA5FC52850C1A05CFD3A286AA306348D,
	U3CLoadObjFileFromSourceU3Ed__35_System_IDisposable_Dispose_mDF8431A500419133E6A0514D8CDE78227EAA95D5,
	U3CLoadObjFileFromSourceU3Ed__35_MoveNext_m0E915CEAB996A5A057F7D632EC803AF2B2268181,
	U3CLoadObjFileFromSourceU3Ed__35_U3CU3Em__Finally1_mE2A4BDD61EEEF9CD182BA203A728E2C67043CA45,
	U3CLoadObjFileFromSourceU3Ed__35_U3CU3Em__Finally2_m9E36E823A033AE5B9A179286C0325EEC4E8E2D7E,
	U3CLoadObjFileFromSourceU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3FEAE033BD4DFA0F146A1F05803B15F0F71E976,
	U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_Reset_m42E38D7D95C453A7EA50AC722E8F3E06A5F482F2,
	U3CLoadObjFileFromSourceU3Ed__35_System_Collections_IEnumerator_get_Current_m763DE9308058541B6FB0299201065D7B22377FDC,
	U3CU3Ec__DisplayClass36_0__ctor_mDC3CC162BC6BC9E5A540B876F4BA6502B3C99F66,
	U3CU3Ec__DisplayClass36_0_U3CUpdateTargetInTrackerU3Eb__1_m8B7755806BC4F0ADA1B3AC6DF35E9FB916BDBFD8,
	U3CU3Ec__DisplayClass36_0__ctor_m24EC14ABD5BADF5FA98C556354BD094AF538B238,
	U3CU3Ec__DisplayClass36_0_U3CLoadObjectTargetU3Eb__0_m01A22D4A1D295BB65C817D92213EA0A7495C5765,
	U3CU3Ec__DisplayClass37_0__ctor_mDDC294CBFA9E6C65454FB636AB417483644AFBDF,
	U3CU3Ec__DisplayClass37_0_U3CUnloadObjectTargetU3Eb__0_mA25CF710ADFBD2CDE8ECEF5789E8DA41B003357A,
	TargetGizmoConfig__ctor_m6E4644AA820AC7AD00BF625C9DA36674748D0823,
	SparseSpatialMapInfo__ctor_mC9E63EBA31F87A30501A92067A52DFE277F2D559,
	MapManagerSourceData__ctor_mF3B009692A5D49D6690D038B3C343D2F0C06094C,
	ParticleParameter__ctor_mCE0749CF8471491B063B3844D1BAAD9E876FFBAB,
	U3CU3Ec__DisplayClass57_0__ctor_mE8B8F7A4FD86DB5D074C409DF5F90FBE2E51FB6A,
	U3CU3Ec__DisplayClass57_0_U3CUpdatePointCloudU3Eb__0_m96210735F38572D86EF08BEADD802AF3773E929A,
	U3CU3Ec__DisplayClass61_0__ctor_mED3763669488870BD57D03C1943E6FC4F1A02AB6,
	U3CU3Ec__DisplayClass61_0_U3CUpdateMapInLocalizerU3Eb__1_m0F5ABB7E592D934291CF5E984E5DA69A3CF0E70C,
	MapLocalizerConfig__ctor_mB2141AE959D97990BF34B2B988EF1BA7BE9276A4,
	SpatialMapServiceConfig__ctor_m6C1E2F582C8D964BF62F7CC0674A4FA0B3BC1C0B,
	U3CU3Ec__DisplayClass64_0__ctor_m9478A19C24B01D3A5FA2BD7696676646D480A089,
	U3CU3Ec__DisplayClass64_0_U3CLoadSparseSpatialMapU3Eb__0_m11115D1269D8460926F8CDCA5A203C4A12259508,
	U3CU3Ec__DisplayClass65_0__ctor_mE6E5FC194109FA242F101E16406185FFBE4EFBED,
	U3CU3Ec__DisplayClass65_0_U3CUnloadSparseSpatialMapU3Eb__0_m82BCE66C7A2A0F349DC8B083D9C5AE79308B7529,
	U3CU3Ec__DisplayClass66_0__ctor_m049DA0B3BBC1AF189648247D10EC3C15706284AC,
	U3CU3Ec__DisplayClass66_0_U3CHostSparseSpatialMapU3Eb__0_mBAC3C3CE73A7780013CECDE3F6C25AEE63BE179A,
	U3CU3Ec__DisplayClass0_0__ctor_mDA88F9878FE2C11C409055DE704782C101465D3E,
	U3CU3Ec__DisplayClass0_0_U3CLoadFileU3Eb__0_m89E7B14ED4DAC4411FBB945CDBEBB9C23C59C9A2,
	U3CLoadFileU3Ed__1__ctor_mE13418FCCBD4A43BF00FD5DC750FE442C5EC6AED,
	U3CLoadFileU3Ed__1_System_IDisposable_Dispose_mF648985E400E9511EAAE5045586AE3DC39685860,
	U3CLoadFileU3Ed__1_MoveNext_mA3E26032FEF1F819AEDE7DDA0E47A26DCEF827A4,
	U3CLoadFileU3Ed__1_U3CU3Em__Finally1_mE98E113260F26E257234B5C00C2605878BD2FC66,
	U3CLoadFileU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9170B939D633F3684A1972B41C7BDD0727422294,
	U3CLoadFileU3Ed__1_System_Collections_IEnumerator_Reset_mEA4B19D80B4CD71AF0345BD61517201CD15DD0DF,
	U3CLoadFileU3Ed__1_System_Collections_IEnumerator_get_Current_m4CED7D7A2D78D06F8CABBF30B53F10DEE38AC3EA,
	U3CShowMessageU3Ed__8__ctor_m36B143EF8D98BB904B07FED0B24262AA6BDDE7FB,
	U3CShowMessageU3Ed__8_System_IDisposable_Dispose_m337BC0CE33848702389A44B026B54D5FBFB5D9ED,
	U3CShowMessageU3Ed__8_MoveNext_mB671DC763D019031F9FEE75FEFE80224915D13DD,
	U3CShowMessageU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64CDAB024F280D891B378F97E3521A6760BF9F27,
	U3CShowMessageU3Ed__8_System_Collections_IEnumerator_Reset_m42D83A290ECB922C83EABCC6F303DAEFBB050A77,
	U3CShowMessageU3Ed__8_System_Collections_IEnumerator_get_Current_m7ECE6C6E2223DB2AB1FD051EE0B01266548652CC,
	DeviceUnion__ctor_m6310C0DCAECE5A1778DBAFE93517A338913C628F,
	DeviceUnion__ctor_mF7BFD75FC072F924A0E911D1C00D0F192E2DCA19,
	DeviceUnion__ctor_m5826EC18294E9D3A50D13D672EFAA2823B70594B,
	DeviceUnion_get_DeviceType_m74381A25C2EDF9BF423CA43C63B7CC2DDC7C0D42,
	DeviceUnion_set_DeviceType_m32407C8A0B2A051461CBD0DF567FAB69962400ED,
	DeviceUnion_get_MotionTrackerCameraDevice_mBAB4C888FB5F767A8FF7C5D845F2FFB97814F9B2,
	DeviceUnion_set_MotionTrackerCameraDevice_m266BD59FD26AF00C372DE661C58E6883A661A8EB,
	DeviceUnion_get_ARKitCameraDevice_mF3750662F5632D75160E4A2E01CCDE43D7841477,
	DeviceUnion_set_ARKitCameraDevice_mE3DB9D883D9BDE0862CCFF3D7EEDCFFB51BC66CC,
	DeviceUnion_get_ARCoreCameraDevice_m86B0E6B9EE154D770149736DFA5F4CFD83EB1558,
	DeviceUnion_set_ARCoreCameraDevice_m3308C8773392C5B96F5FCD860B20A2BF690D2D56,
	DeviceUnion_op_Explicit_mD4440463B900A4B37E0BA12492E1DF8689AFDF61,
	DeviceUnion_op_Explicit_m449CA26A040CC4397C985B886D619BB75EB47291,
	DeviceUnion_op_Explicit_m5DD76392883622DB4ABAFE9BC04DD1156A0647F7,
	DeviceUnion_op_Implicit_mA9ED1E1375EBE31047347C95F46F7EF9E3324D15,
	DeviceUnion_op_Implicit_m36207E4B78ED922B72D33B42D76F241F2753CE6A,
	DeviceUnion_op_Implicit_m59C1C7325E063AE41056EEF4CB12732E78350FF1,
	DeviceUnion_Type_m0AB8A5E98E27F51B7993A4B076983AAB69E7E5BF,
	DeviceUnion_ToString_m001B6577562B4AFF2DA5165F95C4B1A269DA9A32,
	U3CU3Ec__DisplayClass35_0__ctor_m99F7E4D0CE827BE0F18415AB559A4B3365DE3D08,
	U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__0_m8B5A6794FE0A43A007D120DA965B3CBE10FDD270,
	U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__1_m2EF0F4DCC2C27CF22F8B672A631A5F813D92DEDC,
	U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__2_mA2D85802494AFE26CB3942404E813485D0DA04E4,
	U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__3_m3A0510D6D8F5EF8CBF9C61A17FDDFFFFED6B10B3,
	U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__4_m3D7FE1644F495F50978E9C1D03BFE650C948F1D2,
	U3CU3Ec__DisplayClass35_0_U3CCreateMotionTrackerCameraDeviceU3Eb__5_m0564F7655BE623BA1D038DB6A553A17608F886B0,
	U3CU3Ec__DisplayClass36_0__ctor_m99DD9BC101E969037D1E3F203F6661545DC24F94,
	U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__0_mAA44C6A01C2387BEB65A4DB7DEE908B446EE773B,
	U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__1_mA4149B5859FA68BA6CCE4277030144D00A4355E2,
	U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__2_mD9361E9F783A81F508925A2B8BF508F99CA18239,
	U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__3_mC7C849DE4085C13F773E527D29D27E9DAC7BFB6C,
	U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__4_mEB0181C8BD7FE2EED296146C5995AB729EFB0CAB,
	U3CU3Ec__DisplayClass36_0_U3CCreateARKitCameraDeviceU3Eb__5_m6A59BA3A9BDF61C8E7B6102037C357AF41402F82,
	U3CU3Ec__DisplayClass37_0__ctor_mA26FA34AA8D1A95F282FF636E4B16118DFB15E67,
	U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__0_mD821C58DC97EDAF33AA2E57345D21CB36377754F,
	U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__1_m9AD5578EBC37C71254C1283D30984642A861BD33,
	U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__2_m47E1D988F6EAE2816A8AB275120F0F5D9447C14D,
	U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__3_mC65A21FD22B580317D5462BFD5B3195B27D1A0BC,
	U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__4_mF9A87CE0FD9D06CA446E6CAD9DA6ECFFBF361DFB,
	U3CU3Ec__DisplayClass37_0_U3CCreateARCoreCameraDeviceU3Eb__5_m5C7C5BC2B7C777C41FFEEB57F27A4F26FDDED56F,
	AutoRelease_Add_mAE4B8D31F19FBE3D2C43C35FC2CBB1FD8A368CF8,
	NULL,
	AutoRelease_Dispose_m8BE43DC72624325F7EAE73DD38DBFA8A22425745,
	AutoRelease__ctor_mD4A2F0D3CEA53F19CB9780F1294269C3820730EF,
	OptionalOfBuffer_get_has_value_m9D6A59355D2970D878F9515A0F043E56F5D87CCB_AdjustorThunk,
	OptionalOfBuffer_set_has_value_m983B29124CF55FFCD41A9927DB269B7582E091A4_AdjustorThunk,
	OptionalOfObjectTarget_get_has_value_mC647A51D888E6763608BEB804D1A7B3A3904B276_AdjustorThunk,
	OptionalOfObjectTarget_set_has_value_mE5F8EEA200FE9B3BF5C56E6C43C96E214C57DB44_AdjustorThunk,
	OptionalOfTarget_get_has_value_m790B50D2027A5C905809232E206494F1F4C91BE7_AdjustorThunk,
	OptionalOfTarget_set_has_value_mEBA4B2BBD9E65AA2DCAB70C079E53872730D63F8_AdjustorThunk,
	OptionalOfOutputFrame_get_has_value_m10A11564384A305155F97AEFBBA72437AD655F29_AdjustorThunk,
	OptionalOfOutputFrame_set_has_value_m9D0E9C612379133C2EAEE95F671153061F79C824_AdjustorThunk,
	OptionalOfFrameFilterResult_get_has_value_mFBECA5EB4063C294A995CDBDEF101DD8F2F6C5EC_AdjustorThunk,
	OptionalOfFrameFilterResult_set_has_value_m8ED45EE2429DB6337AB6FE1918A4F56282FA5F2B_AdjustorThunk,
	OptionalOfFunctorOfVoidFromOutputFrame_get_has_value_m662EC61A5F9A7308D525D21DED9760D1857C0DF2_AdjustorThunk,
	OptionalOfFunctorOfVoidFromOutputFrame_set_has_value_m2E510F3094BFDBF761D17C9DAB392203E89FC6AC_AdjustorThunk,
	OptionalOfImageTarget_get_has_value_m42A8C36CCEAAD9ABE06BF3542F6C5ADB8C82FF54_AdjustorThunk,
	OptionalOfImageTarget_set_has_value_mF1DD2AFE215A441E298CC7ACA5EEA3687670D7FA_AdjustorThunk,
	OptionalOfString_get_has_value_m80AD50BC2F401A100152E0974E13AB1E621EDEAB_AdjustorThunk,
	OptionalOfString_set_has_value_mCD102F9F525031E72C850FFB20A2A8338FC6CBBA_AdjustorThunk,
	OptionalOfFunctorOfVoidFromInputFrame_get_has_value_mFDBB48FEFEAFC49D6BC7AD4E78FC811D5D15E67C_AdjustorThunk,
	OptionalOfFunctorOfVoidFromInputFrame_set_has_value_mEF838067075D602261406DBD08254B78C342F486_AdjustorThunk,
	OptionalOfFunctorOfVoidFromCameraState_get_has_value_mF83C24C0E83A54FA00976906CC8821AB3331E2C4_AdjustorThunk,
	OptionalOfFunctorOfVoidFromCameraState_set_has_value_m2D8B805B1D5DF9FBCDD1988E734A2F47DAEBD7EF_AdjustorThunk,
	OptionalOfFunctorOfVoidFromPermissionStatusAndString_get_has_value_m54C2C1EAD70D4507DE32FB7FF3FDA020BA38A922_AdjustorThunk,
	OptionalOfFunctorOfVoidFromPermissionStatusAndString_set_has_value_mFAB6CFFD20AC4B462FFF5D0EFC1AA368AB525AE1_AdjustorThunk,
	OptionalOfFunctorOfVoidFromRecordStatusAndString_get_has_value_m49D7BD70815B7C658D813D65987BEE27B6AAEE58_AdjustorThunk,
	OptionalOfFunctorOfVoidFromRecordStatusAndString_set_has_value_m1D674CF7F28B654FD2621066E2AAE90898DB77BE_AdjustorThunk,
	OptionalOfMatrix44F_get_has_value_m6CE4342DEF002C6CC751406ACC5702461A5C1079_AdjustorThunk,
	OptionalOfMatrix44F_set_has_value_mBE64ECC5FBD0370BDFE9F0B715B3B2E2EAD0285F_AdjustorThunk,
	OptionalOfFunctorOfVoidFromBool_get_has_value_mE800CC81603A472DFE0A9448225FF98C08D698E9_AdjustorThunk,
	OptionalOfFunctorOfVoidFromBool_set_has_value_m1F2CD7EC7EC7FF7D8D9E814478AAD9CEBE8E45E8_AdjustorThunk,
	OptionalOfImage_get_has_value_mF6E55AC76F8950B0B9633BCFAD0E4D278C10FCCD_AdjustorThunk,
	OptionalOfImage_set_has_value_m4ED800FE9DA50381E49E1A8D3DC583B82CD77980_AdjustorThunk,
	OptionalOfFunctorOfVoidFromVideoStatus_get_has_value_m92C403E87E8C4CC2324557A0582E7F92892FEDDF_AdjustorThunk,
	OptionalOfFunctorOfVoidFromVideoStatus_set_has_value_m05AE6A060EE9BFF950C8F69BF92D50F4CE448865_AdjustorThunk,
	OptionalOfFunctorOfVoid_get_has_value_m75B9606CBBA1854B6BF00D025DCD682000C4BAFB_AdjustorThunk,
	OptionalOfFunctorOfVoid_set_has_value_m6FF72DD8BF8C958D2DB33A82D6405D9A1BF599AB_AdjustorThunk,
	OptionalOfFunctorOfVoidFromFeedbackFrame_get_has_value_m15BEFDE0C7E92204EE15D0F733D1C1BBDE8F362D_AdjustorThunk,
	OptionalOfFunctorOfVoidFromFeedbackFrame_set_has_value_m74CD0885E8287EC5B4405DA5DAB4729B3D8E4600_AdjustorThunk,
	U3CU3Ec__cctor_m3DC7EC5EEED0D4E244C5688653FEE13108C02AD8,
	U3CU3Ec__ctor_m0943817FFA14C12B3378D48B3CF247F53F52E95B,
	U3CU3Ec_U3CListOfVec3F_to_cU3Eb__670_0_mB742E1489F5031554C832BEBD7BEE946AD19F7C1,
	U3CU3Ec_U3CListOfTargetInstance_to_cU3Eb__672_0_mCB4EDFB4C34657AB33732166BBFBF0947D7718CD,
	U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_to_cU3Eb__676_0_m2C40E4D867FDFF9D94A32FBF3544B4F920AC85FC,
	U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_to_cU3Eb__676_1_m33EC9122C276F8F7B6313C05AA306FA26A1058AE,
	U3CU3Ec_U3CListOfOptionalOfFrameFilterResult_from_cU3Eb__677_0_mA7B7FC3764BEE5703D78BD0E24C58B058A8ACE85,
	U3CU3Ec_U3CListOfTarget_to_cU3Eb__688_0_mC6DB30933522CA28F378DF1AF82E8D97BB432B03,
	U3CU3Ec_U3CListOfImage_to_cU3Eb__691_0_m3B33F0E19F049AD8FA8F35C41767A82F5458E789,
	U3CU3Ec_U3CListOfBlockInfo_to_cU3Eb__698_0_mE78DD82C4A015A7645D604B55F6BD43B4C381362,
	U3CU3Ec_U3CListOfPlaneData_to_cU3Eb__725_0_m9E2E72A598087ADC8ED6358D484077D221268D81,
	U3CU3Ec_U3CListOfOutputFrame_to_cU3Eb__756_0_m032F25FBB55F82055199B93801F2F6AF16EC06BA,
	U3CU3Ec_U3C_cctorU3Eb__758_0_mB6531CDE62AC7ECD5BA6CF40D93100DE110F64BE,
	U3CU3Ec_U3C_cctorU3Eb__758_1_m8742C78FC152D3B88EB84234B678B0C5C3D9B49E,
	U3CU3Ec_U3C_cctorU3Eb__758_2_m732B0F7962DC9CF06609FBD1A9909D2C1967C70B,
	U3CU3Ec_U3C_cctorU3Eb__758_3_m76C44DF1147F890829C9DBB56D5D991068136E02,
	U3CU3Ec_U3C_cctorU3Eb__758_4_m0B9734D2E88C71010508D5C739A3EA645DC6D542,
	U3CU3Ec_U3C_cctorU3Eb__758_5_mF8B60ABA3D8C428CDF75892E96029679D4D2E0B2,
	U3CU3Ec_U3C_cctorU3Eb__758_6_mBFD23EA5B44B8A438577AFA355709F9BCAF0E593,
	U3CU3Ec_U3C_cctorU3Eb__758_7_m8658C4B5830F812F411386C20546EB49080F5F7E,
	U3CU3Ec_U3C_cctorU3Eb__758_8_m26478080CA9B3F1A422946F5F193F047EE8EA952,
	U3CU3Ec_U3C_cctorU3Eb__758_9_mD0E2B09AAA1528D3B6F6C3A12FFA93BC66175921,
	U3CU3Ec_U3C_cctorU3Eb__758_10_mA13202FAA87DD6E34C7EA0AD92CB3DE9544AE96A,
	U3CU3Ec_U3C_cctorU3Eb__758_11_mF258A7C4EE78963CAB7D7CB786B3C1C93B372D61,
	U3CU3Ec_U3C_cctorU3Eb__758_12_m4D7CE18FB1AFE894B2565776CB58AA3A59B039E3,
	U3CU3Ec_U3C_cctorU3Eb__758_13_mA94159B5D15381C3B33BA89F71DFA04FF7395783,
	U3CU3Ec_U3C_cctorU3Eb__758_14_mC5B86A953237C767D338101823EA8E8D8BF02EB7,
	U3CU3Ec_U3C_cctorU3Eb__758_15_m3090D8AE5299B14DBE37ED66AFDFCFA92906A7F1,
	U3CU3Ec_U3C_cctorU3Eb__758_16_m826C9E2E10EA210FD7A5D7283B4A7C60DAC2E9B1,
	U3CU3Ec_U3C_cctorU3Eb__758_17_m1E65E57ED9461A0BC84571EA8D7D5F7796C0216B,
	U3CU3Ec_U3C_cctorU3Eb__758_18_mF1FD8A2EEF7452F3E70B5F4C15E9CC9719124157,
	U3CU3Ec_U3C_cctorU3Eb__758_19_m62097AF3D3481C3DF291CB32C13ADE76BC7C69BC,
	U3CU3Ec_U3C_cctorU3Eb__758_20_m1BB65C44681FA483BB94643F2D2AF26193233ACF,
	U3CU3Ec_U3C_cctorU3Eb__758_21_m6EC677055FCF96A594795F59000C8FB209C94006,
	U3CU3Ec_U3C_cctorU3Eb__758_22_m433572B39C741A6A4C87FA667C8BC260B981B802,
	U3CU3Ec_U3C_cctorU3Eb__758_23_mA0FAE16A96CD7A4787C4B9B9F5D350852D44726C,
	U3CU3Ec_U3C_cctorU3Eb__758_24_mFB2017ECA151425D22DBC4A481461095199F72F8,
	U3CU3Ec_U3C_cctorU3Eb__758_25_m510A65258DB78EC50AC1E8039338530B8EE62E0A,
	U3CU3Ec_U3C_cctorU3Eb__758_26_mE61B3C188E5DCB6857BD574732C80A082DE7F625,
	U3CU3Ec_U3C_cctorU3Eb__758_27_mCC1DA20B82583641485121043759F6244AAC27B8,
	U3CU3Ec_U3C_cctorU3Eb__758_28_m2F676F8600BCB49A04B4B79243A950ADE6B746C7,
	U3CU3Ec_U3C_cctorU3Eb__758_29_m9BD1F5CE466969DFD1E3512F1DB4E8FA3C218598,
	U3CU3Ec_U3C_cctorU3Eb__758_30_mA66050965C76053F038AF1D5F853A33968D34312,
	U3CU3Ec_U3C_cctorU3Eb__758_31_m24E076EBAC672E4A7B181F77D59F81E53C78F775,
	U3CU3Ec_U3C_cctorU3Eb__758_32_m78730DB8F1DA0C73C90A4FBA8AFF8149DEFB48AC,
	U3CU3Ec_U3C_cctorU3Eb__758_33_mEE86B1F692DA8C5B36770E69ECAF8469D680EBE1,
	U3CU3Ec_U3C_cctorU3Eb__758_34_m0B48923CB6F7A6115362142623914EDA1A897423,
	U3CU3Ec_U3C_cctorU3Eb__758_35_m6FD600984067C2AB5AC5CA547FFA0E6A2A151F3D,
	U3CU3Ec_U3C_cctorU3Eb__758_36_m7A65116E6949D51828CA641FC9DC452EFBD8D0BE,
	U3CU3Ec_U3C_cctorU3Eb__758_37_mBA644C9DE0F1BB8B8FA378A886CB8A1F45E34FEF,
	U3CU3Ec_U3C_cctorU3Eb__758_38_mE703DED0B1C1B0D27559F3552CCF66AB91CFE077,
	U3CU3Ec_U3C_cctorU3Eb__758_39_mB8555D7F2FEC26CFB04601F6D5227DCEDC440401,
	U3CU3Ec_U3C_cctorU3Eb__758_40_m6EED4438F52C7FE51E8CB6B22D08E519B8A70A95,
	U3CU3Ec_U3C_cctorU3Eb__758_41_m8540468EBCA24CC3E5F911034576AE184F9AD53C,
	U3CU3Ec_U3C_cctorU3Eb__758_42_m9C5F39B961FCB4DB2615E0721148C0925347A70D,
	U3CU3Ec_U3C_cctorU3Eb__758_43_m7C2C5B4C5100FA89B7FA83451EAD3ECF73EDD019,
	U3CU3Ec_U3C_cctorU3Eb__758_44_mD6C4F75B8CA02E02BB7B5EF176CB3D4BF7580866,
	U3CU3Ec_U3C_cctorU3Eb__758_45_mF686CBFB65364C75FB34CD1A27DE17E98706BAD1,
	U3CU3Ec_U3C_cctorU3Eb__758_46_mEA9B9CD1938D4223ECFA90958EADAB2A1BD7B51A,
	U3CU3Ec_U3C_cctorU3Eb__758_47_m6E77968C3729843CBFF5DB41D16158F06A49CD62,
	U3CU3Ec_U3C_cctorU3Eb__758_48_m95A24EB55B8A6FB04DCA21AB91105A8C00FDA39D,
	U3CU3Ec_U3C_cctorU3Eb__758_49_mEBEAC016D9E5CA848D55CFFA86E8B416039DA2B8,
	U3CU3Ec_U3C_cctorU3Eb__758_50_mF52309217A0955C2EDFF1296941D8426A1CAB6E0,
	U3CU3Ec_U3C_cctorU3Eb__758_51_m31FFF619E81548CA3FBD827D0CE20E6CA70084C2,
	U3CU3Ec_U3C_cctorU3Eb__758_52_m1DDCA0CEA1F3046FE8879ABDEF2E69FE41026528,
	U3CU3Ec_U3C_cctorU3Eb__758_53_m1C25B67B3881E106069B23B9D680BE5C96CD14B3,
	U3CU3Ec_U3C_cctorU3Eb__758_54_mEC28EF6D4EE3D1719E2CED9C66665F3859AAEA32,
	U3CU3Ec_U3C_cctorU3Eb__758_55_m4791C9E9E4378629C01F7C1B25F4795850536500,
	U3CU3Ec_U3C_cctorU3Eb__758_56_m6AB781470E7B9B4945259B9D6AFBE834FBB2BCDC,
	U3CU3Ec_U3C_cctorU3Eb__758_57_m58CC6B94FAB00FA09471E921A1367E1B7262661A,
	U3CU3Ec_U3C_cctorU3Eb__758_58_m0A246396DE2F582C2F104577C86FE3D3E4258694,
	U3CU3Ec_U3C_cctorU3Eb__758_59_m023390C7F1D99DB30469AB0F863483B4BA990E95,
	U3CU3Ec__DisplayClass681_0__ctor_m602E4532ADF4B2FB37BF3CC3C781B67692CECC8F,
	U3CU3Ec__DisplayClass681_0_U3CFunctorOfVoidFromOutputFrame_funcU3Eb__0_mA48DDF43C65854B34F7FC69EBFB43D026EEFC7EF,
	U3CU3Ec__DisplayClass685_0__ctor_m370CAA00B4245A9F144ECE191BED309189EDCEF8,
	U3CU3Ec__DisplayClass685_0_U3CFunctorOfVoidFromTargetAndBool_funcU3Eb__0_mEDBAAA48FD121F3EA83B3222E928E55863248861,
	U3CU3Ec__DisplayClass695_0__ctor_m482D8BF13DDAE6592FE71F9A163A5CC1A9A396D4,
	U3CU3Ec__DisplayClass695_0_U3CFunctorOfVoidFromCloudRecognizationResult_funcU3Eb__0_m515BA1DCBDD4FFB1D69B5ABBEA922522341CE7C6,
	U3CU3Ec__DisplayClass702_0__ctor_m5D8B12D24C8B5468EB9FD35CCC4853C8D40A278D,
	U3CU3Ec__DisplayClass702_0_U3CFunctorOfVoidFromInputFrame_funcU3Eb__0_mED474FB77EB85ECBF8474B82E38A6049C2373A34,
	U3CU3Ec__DisplayClass749_0__ctor_mEB826B947A67BE418F91196EB9031F746A574580,
	U3CU3Ec__DisplayClass749_0_U3CFunctorOfVoidFromFeedbackFrame_funcU3Eb__0_mD5BF5086117AD10EB440FC6C2D8E7BFAB566B422,
	U3CU3Ec__DisplayClass753_0__ctor_mC4A7CE8B4110BBE37D0E1F2FB0E358DE4AABAD23,
	U3CU3Ec__DisplayClass753_0_U3CFunctorOfOutputFrameFromListOfOutputFrame_funcU3Eb__0_m5BA2BBAF4FA9D414A0FD4769CF4DAE7344D4C86C,
	U3CU3Ec__DisplayClass753_1__ctor_m674E015D47946D9455B1F017871C2C4FFD75C714,
	U3CU3Ec__DisplayClass753_1_U3CFunctorOfOutputFrameFromListOfOutputFrame_funcU3Eb__1_m87C5C3DE348C86AB721C1EBC74726C5B3743F0E6,
	Retainer__ctor_m5872905A8AAEDC46956901D23FF5265ADF283918,
	Retainer_Invoke_m1695FE0BB2F8417A4132AF5BAF08969A1773A0F3,
	Retainer_BeginInvoke_m7BF57438B1E42AAC90EF17BB50859EFB56DDA39C,
	Retainer_EndInvoke_m3C78A070621497EA67822BC148A36F9B23ED6993,
	U3CU3Ec__cctor_m0779763872117F69A04F0046A0AABDACE4EB38DE,
	U3CU3Ec__ctor_m09B49C3ED78AF55B061A7F24CF187215F2A08EDB,
	U3CU3Ec_U3CcreateFromParametersU3Eb__4_0_m51E2309D5EF2DA96B3F1FD1EBFD1CD0EB045CA85,
	U3CU3Ec_U3CcreateFromObjectFileU3Eb__5_0_m169692AA05D6E2A0D59A1EAE25773CAB99200ECE,
	U3CU3Ec__cctor_m72DD153E5338B35AE80FA404593DA48DFBCA831F,
	U3CU3Ec__ctor_m96115AD764C88912907865EB19A46FC16403322C,
	U3CU3Ec_U3CgetTargetU3Eb__4_0_m68F25BC99DE96CCF5A7ED2D662ED1826C2DA7BF4,
	U3CU3Ec__DisplayClass5_0__ctor_m404BF93F588419F762746DABBC70992680ABC305,
	U3CU3Ec__DisplayClass5_0_U3CgetUnknownErrorMessageU3Eb__0_m44EFC3E7F7C3E62F06C2D825CBD154C46612F5EA,
	U3CU3Ec__DisplayClass11_0__ctor_m8A9F66B1FD1FE8AABFDA2CF8F3E4B0FE7395A6E7,
	U3CU3Ec__DisplayClass11_0_U3CwrapByteArrayU3Eb__0_m972D8122BF86C0A71FE820914AF6E5FAD5BCE713,
	U3CU3Ec__cctor_m065DA028A543D550A473BCE474064611AB0090BA,
	U3CU3Ec__ctor_mB17B2AC34CD701BCE875E47593636B377C0DFAA7,
	U3CU3Ec_U3CwrapByteArrayU3Eb__12_0_mC824B59883D481C967DA3C1DBD87F584FC263B94,
	U3CU3Ec__DisplayClass13_0__ctor_m95DF00D0214120504C2E18F822AAE512A00B0C5E,
	U3CU3Ec__DisplayClass13_0_U3CwrapByteArrayU3Eb__0_mCF78844245B8665A8C54566575EBD5D41AACA0DD,
	U3CU3Ec__cctor_m7653F87AA49A321CC182C0A13854EDAF15902C63,
	U3CU3Ec__ctor_m8B266FAF0639DC53F1672DC824921E64A2EFBAC2,
	U3CU3Ec_U3CtryGetU3Eb__6_0_mA219E836ADB517B00BFD630D7CFDEC40477EDB11,
	U3CU3Ec__cctor_m481DD56D9561C648112205D4408EF59B40E3CCA8,
	U3CU3Ec__ctor_m5BE1BC8C717A47D976E80B0ACED311A92EBC6F64,
	U3CU3Ec_U3CtryAcquireU3Eb__7_0_m2EA32E5FD6B3A0FA3C868DEEB11B08305E3A417C,
	U3CU3Ec__cctor_mBBB49E8A52B7A8030B281CF67238BB235DD396A5,
	U3CU3Ec__ctor_m8CB3D683BFC330D0E2E58BB85010598DF3FB6362,
	U3CU3Ec_U3CsetStateChangedCallbackU3Eb__10_0_m8B93FD9E9A7ACA9ED18D693F41F77C2B757D88BC,
	U3CU3Ec_U3CrequestPermissionsU3Eb__11_0_m27BFD8E8AACB63F3F124DA7126C0FC0864CDDBE1,
	U3CU3Ec__cctor_mD4AB9073E86E5B28DE3659477DF7606FEBF8786D,
	U3CU3Ec__ctor_m6C1AF76F5EE67A785A3CD3B4D432E6C0034C7939,
	U3CU3Ec_U3CcreateFromParametersU3Eb__4_0_mD38A5988BEDAF5A8DDF77F07BD6ADC5E646D8E14,
	U3CU3Ec_U3CcreateFromTargetFileU3Eb__5_0_mE8EEECD971F445E1489931F0458643D458210353,
	U3CU3Ec_U3CcreateFromTargetDataU3Eb__6_0_m45889D88802740119F44F0C929B94D48B55C3EBC,
	U3CU3Ec_U3CcreateFromImageFileU3Eb__8_0_m66489C33338E5563DF944E9512027A3BB189BC68,
	U3CU3Ec__cctor_m610F49054D1BCB851E0F80AC5A5F67C2F8B87C11,
	U3CU3Ec__ctor_m856D0A47BF1E2CDF3060360567C5FE9FB1E92423,
	U3CU3Ec_U3CrequestPermissionsU3Eb__4_0_m97EEC5E6A6D583E870950CDDC0A245C24F03557D,
	U3CU3Ec_U3CcreateU3Eb__5_0_mE4525BD21BC15238AADF1D4EF24FE9F715BBB9E5,
	U3CU3Ec__cctor_mCE30D9E439105CDE8BD8C7B6E74383707C483D39,
	U3CU3Ec__ctor_m7AC9347D0F316191C0FFE044E3CA656CFD99B4A1,
	U3CU3Ec_U3CgetVioPoseU3Eb__4_0_m054AFFE3A55816B9C36DC4D676543B92CCF1A040,
	U3CU3Ec_U3CgetMapPoseU3Eb__5_0_mC1D0B1B6C0A71E96139E65BDF93DF94145D71728,
	U3CU3Ec__cctor_mE16731AF5F6D4410C9C8BCCCAF8B84DB8C32AFBE,
	U3CU3Ec__ctor_mBD11BC42157A455694C3447AB2BED62B98F4B964,
	U3CU3Ec_U3CunloadMapU3Eb__16_0_m5ADEF8E56007CEBF09FC93910AF77876A7292BC0,
	U3CU3Ec__cctor_m2C467F17D39998B965993C04596764AF57815A7B,
	U3CU3Ec__ctor_mD5E5993ED8014347554FEDD1D39D61DD463A3129,
	U3CU3Ec_U3ChostU3Eb__5_0_m36F45650D3152C8F37EC93A6ED734F7625D54D5B,
	U3CU3Ec__cctor_m530822C857027158CEB3BA5DAE674B49FF328774,
	U3CU3Ec__ctor_mFF3C81E2678E31BACC53573FB9476AB427FD166E,
	U3CU3Ec_U3CopenU3Eb__7_0_m2A00372C8B19E06E75B185389E6C2EC22D945EC9,
	U3CU3Ec__cctor_m0F1F8603DF79ED01311A1E068ABAAEE43D7500B9,
	U3CU3Ec__ctor_m403553CD537569CEFD45CC84FF3AB86EB95FF0B1,
	U3CU3Ec_U3CdecodeU3Eb__0_0_m9A92FA81D1BF151B4261C162DD8BA98ADD63D2CC,
	U3CU3Ec__cctor_m7D0A60F95D52D57194A599ABF5DAE83F2DCA43E6,
	U3CU3Ec__ctor_m75E56CA9F1DDE9FF506886F144DD6CEBE5AEAB7F,
	U3CU3Ec_U3CsetHandlerU3Eb__3_0_m48B458C910BA126C8F8997AAB4503B19C6AB0A21,
	U3CU3Ec__cctor_mB902917647CADF0DAA4030D14299FEE34E851180,
	U3CU3Ec__ctor_m72193E86114F81ECC5D7266C46FAC37D5E90B24B,
	U3CU3Ec_U3CsetHandlerU3Eb__3_0_m2463EC0AA9C09C79BA193FB3CC93ED4F664FA23E,
	U3CU3Ec__cctor_mD5A59A0815B2F87FA19F2E27F8A0069B694C2F50,
	U3CU3Ec__ctor_m49DEAC095E57442161997DEF2CC9E9BE0E682678,
	U3CU3Ec_U3CsetHandlerU3Eb__3_0_m237A88462E7E4129455A23453E477D28374DD26B,
	U3CU3Ec__cctor_m556348CD1B7516FBB54D7AAAF85BCAA760A0C04A,
	U3CU3Ec__ctor_m42144A8A2B4EFC746BD38F326110D98D6D12A2BD,
	U3CU3Ec_U3CsetHandlerU3Eb__3_0_m7DB6260C930511E1D32A64DEC1F376D6D8463F5D,
	U3CU3Ec__cctor_mCE9410F6B3C54BE488F6CD6CDCD0C126B37D68FF,
	U3CU3Ec__ctor_m4A4C25B2529EA02A8A1B9639EF00F7EF9CF061A9,
	U3CU3Ec_U3CpeekU3Eb__6_0_m2482D72EFC273079697355998B459A80952AD25E,
	U3CU3Ec__cctor_mEA580CFC164F679F2431C56D70513B99B1D7FF12,
	U3CU3Ec__ctor_m43FB04867CAEE6B9A28E33271AABF0BE694EF244,
	U3CU3Ec_U3C_ctorU3Eb__3_0_m39F087607B43071342D73094A4F25243FD1D78FC,
	U3CU3Ec_U3CpreviousOutputFrameU3Eb__5_0_m22C434C4CBC48FE964AAF40454244D27FE30FA92,
	U3CU3Ec__cctor_mE718C3EE0D3317D1F474BEB0A8D66238590FE09A,
	U3CU3Ec__ctor_mAE4C03CFF87E36E27A386A7B541F534EA32DE341,
	U3CU3Ec_U3CtargetU3Eb__5_0_mFC15353608AF56E4282E90BB75618840B2DEAF36,
	ImageTargetConfig__ctor_mA48DE9D56844B4FEB85F759FDA5CB49AFEFE5150,
	ObjectTargetConfig__ctor_mD2153C449CE0521ADC2532F7E1527CA9AD1A3F53,
	NULL,
	NULL,
	FunctionDelegate__ctor_m0E179CC48A7D9461FB57C6C49BC34631E27C67BE,
	FunctionDelegate_Invoke_m11CBC826064BEBD67681C596FA52D95F54AFC850,
	FunctionDelegate_BeginInvoke_m484E8C5B59358F376332D98933E6340C782E8641,
	FunctionDelegate_EndInvoke_mB2C3B0F9862ABCF1AB98399684627227714AF69D,
	DestroyDelegate__ctor_m5A5B0EC452A61C7327BFF4A2A352C9C13810D5B8,
	DestroyDelegate_Invoke_mBE0534CAC55446933DDF052D1CB93F0176201D7F,
	DestroyDelegate_BeginInvoke_m1338D0F085E1EB5723EB416241271319151526E2,
	DestroyDelegate_EndInvoke_m279DCA2EE80CB278C0891B5EABC1FDD932750DB3,
	FunctionDelegate__ctor_mB81B065745F94203D33D966D23B54EA58E50CF1D,
	FunctionDelegate_Invoke_m38D31CB633F36CEB5204B85250BB9E6658008BB0,
	FunctionDelegate_BeginInvoke_mB80BAD8AA6A2AE22015DF3F4BFFDD7DE542397C2,
	FunctionDelegate_EndInvoke_m6429F7AFAFA1C55172F45530C687322977831D11,
	DestroyDelegate__ctor_m08C129CF2B86E1C321ADE34B126E261B210B62B1,
	DestroyDelegate_Invoke_m190879BB23F2F6BD47FC00450B7E34B22D4CB5E0,
	DestroyDelegate_BeginInvoke_mDDAA4C90E46B60A6563E26376A6457A33A5C258B,
	DestroyDelegate_EndInvoke_mA4259DE6CB9B77FF2E76F2D42174CBA5707A5279,
	FunctionDelegate__ctor_m8B88DFF446D0523E149824170DB836CE2CDCE6C9,
	FunctionDelegate_Invoke_mE19162060CE2C99F97B91A1D61CB08A2E443255D,
	FunctionDelegate_BeginInvoke_m72DA0B666E659B4BA178EB9127A14620208A8A9B,
	FunctionDelegate_EndInvoke_m7D411AEA994EBADDDE84DE2648356BAEB34D1F4F,
	DestroyDelegate__ctor_m8369965203E5A6A7C8C3512383D883AC8686703C,
	DestroyDelegate_Invoke_m9A7875247CE820E621B04568077BD6EA5A32CA87,
	DestroyDelegate_BeginInvoke_m9077DA1C7BFB8D309DE306264C9534E5F4E2077F,
	DestroyDelegate_EndInvoke_mE0132BA9177E49E52578B6BB056FD1EE8414E078,
	FunctionDelegate__ctor_mCCB741ECD1A40B11102E30BC641FC8E681A2A013,
	FunctionDelegate_Invoke_m6215B3BF341C05ABDD631A917EE2A201D3BD0CCF,
	FunctionDelegate_BeginInvoke_mD21391178ED9C6881DCFF37B877C0C43F5477B69,
	FunctionDelegate_EndInvoke_m93A413BE71100C35974218FB89C54E1C45D985C7,
	DestroyDelegate__ctor_m279204A504638EC677B9FF7A3DB41F1B4D4980AF,
	DestroyDelegate_Invoke_mE2853AD118B7D6D2E13330412EEE88116334F0A5,
	DestroyDelegate_BeginInvoke_m3D84E55BBD8AD1286AF553F29D1B2873D823D20E,
	DestroyDelegate_EndInvoke_m8FDD30A791BED08E3D09A4CBBF99BF6F926DB621,
	FunctionDelegate__ctor_mEFF077E9452BDAC8A1C3D04C454F17B950C14237,
	FunctionDelegate_Invoke_mBA9F0AFCA18452B41B874B25C2A9A4406D5DED73,
	FunctionDelegate_BeginInvoke_m3936D2245A8E2EBE3BC4B82936D0CDB3676DB829,
	FunctionDelegate_EndInvoke_mEB7EF6C6E5156143E98DD2CCFB09E29C68D295E7,
	DestroyDelegate__ctor_m703805691AFB9D84F4597D131028AEA9EF333971,
	DestroyDelegate_Invoke_mBC3EF69437E9C30FDB28AC5B3FF7B103E085370A,
	DestroyDelegate_BeginInvoke_m8FB0AAF2A01AE3275B6A2C1E7443440FFA51BCA4,
	DestroyDelegate_EndInvoke_m900A85690DDE2425F86EF336D0A3258B40A6B508,
	FunctionDelegate__ctor_m369BCB2D345B3A7E4D6481372DDCFAE6A53752A8,
	FunctionDelegate_Invoke_m460A0AE5767903BA2C5F868727713F8B0356C749,
	FunctionDelegate_BeginInvoke_m541374B1EB1CB57D1B5F6F63EF88E4ACCC292D9F,
	FunctionDelegate_EndInvoke_m0438AC21F13BC93CBEB953DBDAC0F524D86B699B,
	DestroyDelegate__ctor_m0B666DB88CEE6662098F92DFA75F3F750CC60181,
	DestroyDelegate_Invoke_m31B4BEA54A3E885815BF52F38B9B61A495373B52,
	DestroyDelegate_BeginInvoke_mC932D93F085A30E4CA5A569198B5114C040CC3CD,
	DestroyDelegate_EndInvoke_mEA623F205FF04E0624D21545D5CE03979C66BFE4,
	FunctionDelegate__ctor_m34BA85E36C22FEF0ED0B32F61441F25F8C09BD26,
	FunctionDelegate_Invoke_m46975B50A2AD4847C29D33C0B60DA01A2B09A600,
	FunctionDelegate_BeginInvoke_mB5EA0823D61F014ECF861CB919A4003CBDF917FA,
	FunctionDelegate_EndInvoke_m61CB0BB7CA4938CBA2ADCA086694B74C77411B0C,
	DestroyDelegate__ctor_m64EB239F30C53386CF3A31337B17990BBAAD10A7,
	DestroyDelegate_Invoke_m4599B6415700B5306528E5CA67C4C9AEB86E8CF2,
	DestroyDelegate_BeginInvoke_mF9BE803C6F2D278B9932CA2ADCA18427C4B9C0DB,
	DestroyDelegate_EndInvoke_m0E2EB382E7E15D4931C8775B290C54892614BA23,
	FunctionDelegate__ctor_mA73E1C9EADE192B7538EB09846040F6200AD582D,
	FunctionDelegate_Invoke_m71DF6B81BA3C0D4686531C33BE5BA9A0B43CFA29,
	FunctionDelegate_BeginInvoke_m0597D0D11C2ABCDFBC95000854D8716C15AC9E3A,
	FunctionDelegate_EndInvoke_mAC386C0F12F8C0286A4EE2342A6BBDC4C9C18542,
	DestroyDelegate__ctor_mD3302A9A25E881C692D88216CD1F7FF03237BB73,
	DestroyDelegate_Invoke_m6457C78F3F6004DD3607F062D3A7991FBD8E8F5B,
	DestroyDelegate_BeginInvoke_mFC710DD523019556ECFCF78E6B6DB523161D0FD5,
	DestroyDelegate_EndInvoke_mAE40236DC33186943BB316C8183BF85754CA1F15,
	FunctionDelegate__ctor_m962110774C99D29F30B2A376ABA22CCE8D085FBD,
	FunctionDelegate_Invoke_m96743D50AC11AD80A5BDF8A85C47F8FB5AB44E49,
	FunctionDelegate_BeginInvoke_m2805BAC0F418BBA908FCE2C75E8DB8C59935CEC5,
	FunctionDelegate_EndInvoke_mED2A5B7A393703948C70D2BD6F643A83721A35DB,
	DestroyDelegate__ctor_m217130F032ED0FC02AA0ED9FC829E6F4120FB264,
	DestroyDelegate_Invoke_mD1EE30263E6A7E4151C77AF4D9B005A63072DC5E,
	DestroyDelegate_BeginInvoke_mD6D9578B0D45CA16B98B1C800025431929AEF4B7,
	DestroyDelegate_EndInvoke_mCC7D09681D6EF9EB4B2A52331A6BD2424A3757D6,
	FunctionDelegate__ctor_m953639851CD109DE9833FDB02F3C9C8D926A523B,
	FunctionDelegate_Invoke_m3E0D7D2E307F4DC0D987C3ABDE4F472B2C087CA0,
	FunctionDelegate_BeginInvoke_m62930ED2301620A2EC2193A628337AA7BD9E6DDB,
	FunctionDelegate_EndInvoke_mB196424F97E936ADE48868828E0189C2DBE8EA7E,
	DestroyDelegate__ctor_mF9C5D467A494890326241ECE09E41F10FC448E25,
	DestroyDelegate_Invoke_m4A095EC1D34F39204D55C5071CA22F16042225A6,
	DestroyDelegate_BeginInvoke_mE08FD9953A2A7E73F824F5BDC692FD213E47139C,
	DestroyDelegate_EndInvoke_m989C6C497353C4640860922F848174624CC6C521,
	FunctionDelegate__ctor_m1AAC4B3448BF523974B135FDBA507FE57F3EAA5E,
	FunctionDelegate_Invoke_m8B7840C7616102C92E1F636016B9555FB4572A76,
	FunctionDelegate_BeginInvoke_m6E2F466FA5FED38F44E7DDA67389D5AF5E9B9364,
	FunctionDelegate_EndInvoke_m97CE29629D7CE8DEA1E4BED43BC506C3CD6DD45B,
	DestroyDelegate__ctor_mCFBACA0F014A85733CDC536881A374D66C15E014,
	DestroyDelegate_Invoke_mD7401B99FC044B5A5248C3D4C5A4B833032943C3,
	DestroyDelegate_BeginInvoke_m04DE88BE65985E3F8AE8BCB091C202838788D2A7,
	DestroyDelegate_EndInvoke_m980A750F8B2C42A85DF9FB740BE0F7AEF616DA8B,
	FunctionDelegate__ctor_m58EA7F64246E9D934EDD585032AE43CFD185BE16,
	FunctionDelegate_Invoke_mE72D7F213BE1E727C5047288263D189822C3C82A,
	FunctionDelegate_BeginInvoke_mD6BD38D154115E190CDE7F903AE2F3F77DD66335,
	FunctionDelegate_EndInvoke_m897DBF59F58D3E555BB77FD1230E9A7FCB256420,
	DestroyDelegate__ctor_m22D84636E41362B44A6D7DF5E2F55C7CB2E1CC18,
	DestroyDelegate_Invoke_m350FA9D187D9C0A8A83ACCFC3782AA43B49A2D9D,
	DestroyDelegate_BeginInvoke_m3A394D12A05CFBA8B94739939499552195719F8D,
	DestroyDelegate_EndInvoke_mD56D504AA16C004D7757538E631E60165B8BED96,
	FunctionDelegate__ctor_mA14C9DE827C2DB1C26EAE2276BB086B68C8DCFC2,
	FunctionDelegate_Invoke_m34F44155B50DB126560343A5EBE5CB108C856F8C,
	FunctionDelegate_BeginInvoke_m361C882225D7D001B497B6A53C72C40320B4C774,
	FunctionDelegate_EndInvoke_mA2D391F2CA9D21522AEFD2BE83ECC65C41E4E763,
	DestroyDelegate__ctor_mF04E1863AEDA755055052A13C2CDC1A228FF22B3,
	DestroyDelegate_Invoke_m8219C68504316A20EFE28A87D28F050F6AC094B6,
	DestroyDelegate_BeginInvoke_mF53F10F9891F7FFCA2CB6E50B0D0D2F2D15C3D34,
	DestroyDelegate_EndInvoke_mA11950B6F44360E08F38257F709F752F99DAA7BB,
	FunctionDelegate__ctor_m8001F661E65F5F972A7696DA603EA430473D8441,
	FunctionDelegate_Invoke_m5CF011F127F04DF9C8B274D29AE591829DAA2AFF,
	FunctionDelegate_BeginInvoke_m80BCCF53B42AFE698A463B7D92EC9DA6007A86E9,
	FunctionDelegate_EndInvoke_mC247B7CBCE1356376F2E6BDBED636B46AF21A01C,
	DestroyDelegate__ctor_m220E794C85DCF1D6CE09F57EA9FD245D6FD689C8,
	DestroyDelegate_Invoke_m0A4BF8BB80A27181C593682E11CD41D85E7B6A93,
	DestroyDelegate_BeginInvoke_m209DC89E07980BD36BB26CC30C2B28E8E2302A3F,
	DestroyDelegate_EndInvoke_m8989D1652133EE1D8DB3F4803EB6D1B3924DA73C,
	FunctionDelegate__ctor_m3BDA778FE9964EE66A5906214062377E6EA81389,
	FunctionDelegate_Invoke_m1FAC7DD6CCBB7DDE856B367304A1C0EBE47FD7EA,
	FunctionDelegate_BeginInvoke_mF46C7DBF268493C3827653376AEF543E481DE3AC,
	FunctionDelegate_EndInvoke_mEDBCF561F392A390B2E7347016355838B43AF473,
	DestroyDelegate__ctor_m302C7A97E6F9EAA0C80F2C7C7BFF78FD35D94C74,
	DestroyDelegate_Invoke_mAC57037483DA9B5E7D7E405305E5A07288325D21,
	DestroyDelegate_BeginInvoke_m8C00228FE8AB67D1B89B30C531A4024392C2F012,
	DestroyDelegate_EndInvoke_m626FF344FDB87816A01FFE926B91D72E0DFB5057,
};
static const int32_t s_InvokerIndices[2333] = 
{
	23,
	102,
	31,
	102,
	31,
	1241,
	23,
	26,
	23,
	23,
	23,
	23,
	10,
	-1,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	1242,
	1243,
	23,
	23,
	23,
	1244,
	23,
	1245,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	31,
	1015,
	26,
	27,
	26,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	27,
	23,
	26,
	26,
	23,
	1248,
	1249,
	23,
	23,
	1250,
	23,
	26,
	23,
	14,
	26,
	26,
	26,
	26,
	26,
	10,
	10,
	32,
	102,
	31,
	1009,
	1010,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	32,
	26,
	23,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	4,
	111,
	49,
	763,
	49,
	763,
	4,
	111,
	4,
	4,
	14,
	26,
	14,
	26,
	3,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	10,
	26,
	31,
	31,
	23,
	102,
	23,
	23,
	26,
	26,
	23,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	26,
	1170,
	1252,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	101,
	28,
	23,
	23,
	23,
	23,
	26,
	381,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	10,
	10,
	32,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	14,
	14,
	26,
	1251,
	27,
	27,
	31,
	34,
	23,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	26,
	26,
	28,
	23,
	23,
	23,
	23,
	381,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	10,
	10,
	32,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	14,
	14,
	26,
	1251,
	27,
	27,
	31,
	34,
	23,
	23,
	23,
	23,
	26,
	31,
	31,
	1015,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	1049,
	989,
	14,
	26,
	26,
	23,
	23,
	3,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	102,
	31,
	102,
	31,
	23,
	23,
	23,
	1253,
	1254,
	31,
	26,
	23,
	23,
	26,
	23,
	23,
	663,
	1255,
	663,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	10,
	10,
	32,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	1257,
	14,
	14,
	32,
	26,
	1251,
	27,
	27,
	1258,
	26,
	26,
	28,
	26,
	23,
	26,
	14,
	26,
	10,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	1251,
	23,
	26,
	26,
	26,
	26,
	102,
	31,
	102,
	31,
	23,
	31,
	23,
	23,
	1260,
	1261,
	1262,
	1263,
	1264,
	23,
	23,
	10,
	23,
	1265,
	1266,
	23,
	23,
	23,
	205,
	205,
	0,
	23,
	23,
	1089,
	14,
	23,
	23,
	826,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	1267,
	1267,
	1267,
	1267,
	1268,
	1269,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	10,
	32,
	102,
	23,
	23,
	23,
	1253,
	1253,
	23,
	23,
	26,
	23,
	23,
	23,
	102,
	23,
	62,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	10,
	32,
	102,
	10,
	32,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	62,
	26,
	26,
	102,
	31,
	23,
	23,
	102,
	9,
	102,
	9,
	23,
	62,
	62,
	26,
	26,
	10,
	32,
	23,
	32,
	23,
	1270,
	643,
	1271,
	1271,
	643,
	25,
	17,
	643,
	1272,
	643,
	1272,
	643,
	1272,
	643,
	1272,
	643,
	1272,
	1273,
	1274,
	25,
	643,
	1271,
	17,
	643,
	1275,
	1273,
	643,
	1276,
	112,
	643,
	643,
	1272,
	643,
	1272,
	25,
	643,
	1271,
	643,
	643,
	643,
	1272,
	25,
	643,
	1271,
	643,
	643,
	643,
	643,
	49,
	643,
	112,
	643,
	17,
	523,
	25,
	25,
	1277,
	1277,
	643,
	897,
	112,
	25,
	643,
	1271,
	112,
	643,
	643,
	25,
	643,
	1271,
	49,
	1278,
	1279,
	1280,
	25,
	25,
	643,
	1271,
	1281,
	1177,
	1271,
	112,
	1282,
	1283,
	1284,
	1285,
	25,
	643,
	1271,
	17,
	112,
	529,
	1270,
	1286,
	529,
	25,
	25,
	643,
	1271,
	1287,
	112,
	112,
	112,
	643,
	25,
	643,
	1271,
	1288,
	1289,
	1290,
	1290,
	112,
	112,
	1291,
	1292,
	1293,
	1294,
	1295,
	1296,
	1297,
	1297,
	529,
	25,
	643,
	1271,
	1298,
	643,
	112,
	112,
	112,
	25,
	643,
	1271,
	49,
	643,
	112,
	17,
	523,
	25,
	25,
	643,
	1294,
	25,
	643,
	1271,
	112,
	112,
	643,
	643,
	643,
	112,
	112,
	643,
	643,
	643,
	643,
	1273,
	25,
	643,
	1271,
	17,
	49,
	112,
	1299,
	643,
	523,
	25,
	25,
	25,
	643,
	1271,
	17,
	49,
	112,
	1299,
	643,
	523,
	25,
	25,
	25,
	643,
	1271,
	17,
	49,
	112,
	1299,
	112,
	1299,
	643,
	1300,
	1301,
	512,
	897,
	897,
	897,
	523,
	25,
	25,
	112,
	112,
	643,
	1272,
	1289,
	112,
	1302,
	1303,
	112,
	1304,
	1304,
	112,
	897,
	1294,
	897,
	523,
	25,
	643,
	1271,
	21,
	1177,
	1305,
	25,
	643,
	1271,
	643,
	643,
	49,
	643,
	112,
	643,
	17,
	523,
	25,
	25,
	1306,
	25,
	643,
	1271,
	17,
	49,
	1299,
	112,
	643,
	523,
	25,
	25,
	1307,
	1307,
	643,
	25,
	643,
	1271,
	643,
	112,
	643,
	17,
	529,
	25,
	25,
	643,
	1271,
	643,
	17,
	529,
	25,
	25,
	643,
	1271,
	25,
	643,
	1271,
	17,
	523,
	25,
	643,
	1271,
	643,
	643,
	17,
	25,
	643,
	1271,
	643,
	643,
	1308,
	1309,
	1271,
	1310,
	3,
	17,
	643,
	1272,
	643,
	1272,
	643,
	1272,
	643,
	1272,
	1273,
	1274,
	25,
	643,
	1271,
	17,
	643,
	1311,
	643,
	529,
	1275,
	1273,
	1273,
	1276,
	643,
	112,
	643,
	643,
	1272,
	643,
	1272,
	25,
	643,
	1271,
	643,
	643,
	643,
	1272,
	25,
	643,
	1271,
	643,
	643,
	643,
	643,
	49,
	643,
	112,
	643,
	17,
	1177,
	523,
	25,
	25,
	1277,
	1277,
	643,
	897,
	112,
	25,
	643,
	1271,
	49,
	1301,
	1312,
	25,
	1313,
	523,
	25,
	643,
	1271,
	17,
	1272,
	897,
	1299,
	1299,
	1299,
	1299,
	1299,
	1299,
	1299,
	25,
	643,
	1271,
	112,
	1314,
	1314,
	523,
	643,
	25,
	643,
	1271,
	643,
	643,
	17,
	112,
	1305,
	1273,
	1273,
	25,
	643,
	1271,
	17,
	1299,
	112,
	25,
	643,
	1271,
	49,
	643,
	112,
	643,
	17,
	523,
	25,
	25,
	643,
	643,
	1307,
	1307,
	17,
	1315,
	1272,
	643,
	523,
	25,
	25,
	643,
	1271,
	49,
	17,
	1316,
	1317,
	25,
	25,
	643,
	1271,
	512,
	523,
	3,
	3,
	17,
	17,
	17,
	17,
	49,
	1299,
	1272,
	1318,
	25,
	523,
	25,
	25,
	523,
	25,
	112,
	112,
	897,
	1289,
	1273,
	1276,
	25,
	643,
	1271,
	643,
	25,
	25,
	643,
	1271,
	1319,
	1272,
	25,
	25,
	643,
	1271,
	1272,
	25,
	643,
	1271,
	1320,
	1272,
	25,
	25,
	643,
	1271,
	1272,
	25,
	643,
	1271,
	1321,
	1272,
	25,
	25,
	643,
	1271,
	1272,
	25,
	643,
	1271,
	1322,
	1272,
	25,
	25,
	643,
	1271,
	643,
	1311,
	112,
	1177,
	25,
	643,
	1271,
	643,
	1311,
	112,
	1177,
	25,
	643,
	1271,
	1311,
	643,
	112,
	1177,
	1323,
	25,
	643,
	1271,
	643,
	1311,
	112,
	1177,
	25,
	643,
	1271,
	643,
	112,
	643,
	643,
	17,
	25,
	643,
	1271,
	643,
	112,
	643,
	643,
	17,
	25,
	25,
	25,
	643,
	1271,
	643,
	643,
	17,
	25,
	643,
	1271,
	643,
	112,
	643,
	643,
	17,
	25,
	643,
	1271,
	112,
	643,
	523,
	643,
	523,
	1324,
	523,
	1305,
	112,
	1325,
	1326,
	1270,
	643,
	25,
	643,
	1271,
	25,
	643,
	1271,
	1270,
	112,
	643,
	643,
	25,
	643,
	1271,
	1327,
	643,
	643,
	25,
	643,
	1271,
	112,
	643,
	643,
	1272,
	643,
	1272,
	25,
	643,
	1271,
	17,
	112,
	643,
	1305,
	25,
	643,
	1271,
	643,
	1272,
	25,
	643,
	1271,
	643,
	643,
	112,
	1271,
	1177,
	643,
	25,
	643,
	1271,
	1270,
	25,
	643,
	112,
	1328,
	1270,
	25,
	643,
	112,
	1329,
	1270,
	25,
	643,
	112,
	1330,
	1270,
	25,
	643,
	112,
	1329,
	1270,
	25,
	643,
	112,
	1329,
	1270,
	25,
	643,
	112,
	1331,
	1270,
	25,
	643,
	112,
	1329,
	1270,
	25,
	643,
	112,
	1329,
	1332,
	24,
	1178,
	18,
	-1,
	-1,
	643,
	25,
	1333,
	1332,
	1178,
	1332,
	1178,
	1332,
	1178,
	1270,
	25,
	1334,
	1335,
	25,
	1336,
	1332,
	1178,
	1332,
	1178,
	1270,
	25,
	1337,
	1332,
	1178,
	1270,
	25,
	1338,
	1311,
	25,
	1339,
	1340,
	25,
	1341,
	1340,
	25,
	1342,
	1340,
	25,
	1343,
	1332,
	1178,
	1344,
	25,
	1345,
	1346,
	25,
	1347,
	1348,
	25,
	1349,
	1311,
	25,
	1350,
	1270,
	25,
	1351,
	1352,
	25,
	1353,
	1332,
	1178,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1376,
	15,
	23,
	23,
	14,
	14,
	1376,
	14,
	14,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	652,
	275,
	1376,
	14,
	14,
	23,
	1377,
	1378,
	652,
	14,
	413,
	10,
	14,
	14,
	26,
	14,
	26,
	1376,
	14,
	14,
	14,
	26,
	1376,
	14,
	14,
	49,
	14,
	10,
	14,
	4,
	102,
	23,
	23,
	156,
	156,
	14,
	30,
	10,
	1376,
	14,
	14,
	10,
	1380,
	1381,
	1376,
	14,
	14,
	49,
	293,
	2,
	156,
	23,
	1376,
	14,
	14,
	1384,
	43,
	15,
	10,
	1282,
	1385,
	1386,
	148,
	0,
	151,
	484,
	26,
	150,
	26,
	417,
	1376,
	14,
	14,
	23,
	10,
	9,
	1387,
	27,
	9,
	23,
	1376,
	14,
	14,
	157,
	10,
	10,
	10,
	1389,
	1376,
	14,
	14,
	1390,
	1391,
	1392,
	1392,
	10,
	10,
	1393,
	1394,
	37,
	176,
	1395,
	1396,
	1397,
	1397,
	9,
	1376,
	14,
	14,
	150,
	14,
	10,
	10,
	10,
	14,
	26,
	1398,
	14,
	26,
	1399,
	14,
	26,
	1001,
	14,
	26,
	1042,
	14,
	26,
	1074,
	14,
	26,
	279,
	14,
	26,
	157,
	1376,
	14,
	14,
	49,
	14,
	10,
	4,
	102,
	23,
	23,
	14,
	176,
	1400,
	1376,
	14,
	14,
	10,
	10,
	14,
	14,
	14,
	10,
	10,
	14,
	14,
	14,
	14,
	652,
	1376,
	14,
	14,
	23,
	49,
	10,
	32,
	14,
	102,
	23,
	23,
	1376,
	14,
	14,
	23,
	49,
	10,
	32,
	14,
	102,
	23,
	23,
	1376,
	14,
	14,
	23,
	49,
	10,
	32,
	10,
	32,
	14,
	1401,
	1402,
	512,
	30,
	30,
	30,
	102,
	23,
	23,
	10,
	10,
	14,
	26,
	1391,
	10,
	1403,
	1404,
	10,
	1053,
	1053,
	10,
	30,
	176,
	30,
	102,
	21,
	43,
	23,
	1376,
	14,
	14,
	1407,
	1376,
	14,
	14,
	49,
	14,
	10,
	14,
	4,
	102,
	23,
	23,
	1408,
	1376,
	14,
	14,
	23,
	49,
	32,
	10,
	14,
	102,
	23,
	23,
	1409,
	1409,
	14,
	1376,
	14,
	14,
	14,
	10,
	14,
	4,
	9,
	23,
	1376,
	14,
	14,
	14,
	4,
	9,
	23,
	1376,
	14,
	14,
	1376,
	14,
	14,
	23,
	102,
	1376,
	14,
	14,
	4,
	1410,
	645,
	1271,
	23,
	111,
	3,
	23,
	1376,
	14,
	14,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	652,
	275,
	1376,
	14,
	14,
	23,
	1411,
	1412,
	1411,
	9,
	1413,
	652,
	652,
	413,
	14,
	10,
	14,
	14,
	26,
	14,
	26,
	1376,
	14,
	14,
	14,
	26,
	1376,
	14,
	14,
	49,
	14,
	10,
	14,
	4,
	43,
	102,
	23,
	23,
	156,
	156,
	14,
	30,
	10,
	1376,
	14,
	14,
	49,
	1402,
	1414,
	23,
	35,
	102,
	1376,
	14,
	14,
	23,
	26,
	30,
	32,
	32,
	32,
	32,
	32,
	32,
	32,
	1376,
	14,
	14,
	10,
	1416,
	1416,
	102,
	14,
	1376,
	14,
	14,
	23,
	10,
	1407,
	652,
	652,
	1376,
	14,
	14,
	23,
	32,
	10,
	1376,
	14,
	14,
	49,
	14,
	10,
	14,
	4,
	102,
	23,
	23,
	14,
	14,
	1409,
	1409,
	4,
	1418,
	26,
	14,
	102,
	23,
	1376,
	14,
	14,
	49,
	4,
	1420,
	863,
	23,
	512,
	109,
	3,
	3,
	4,
	4,
	4,
	23,
	1376,
	14,
	14,
	23,
	49,
	32,
	26,
	1422,
	23,
	102,
	23,
	23,
	102,
	23,
	10,
	10,
	30,
	1391,
	652,
	413,
	1424,
	23,
	1376,
	14,
	14,
	23,
	1376,
	14,
	14,
	1426,
	26,
	23,
	1376,
	14,
	14,
	26,
	1376,
	14,
	14,
	1428,
	26,
	23,
	1376,
	14,
	14,
	26,
	1376,
	14,
	14,
	1430,
	26,
	23,
	1376,
	14,
	14,
	26,
	1376,
	14,
	14,
	1432,
	26,
	23,
	1376,
	14,
	14,
	14,
	34,
	10,
	43,
	1376,
	14,
	14,
	14,
	34,
	10,
	43,
	1376,
	14,
	14,
	34,
	14,
	10,
	43,
	250,
	1376,
	14,
	14,
	14,
	34,
	10,
	43,
	1376,
	14,
	14,
	14,
	10,
	14,
	14,
	4,
	1376,
	14,
	14,
	14,
	10,
	14,
	1241,
	4,
	23,
	23,
	1376,
	14,
	14,
	14,
	14,
	4,
	1376,
	14,
	14,
	14,
	10,
	14,
	14,
	4,
	1376,
	14,
	14,
	10,
	14,
	102,
	14,
	102,
	385,
	102,
	1407,
	10,
	1435,
	1436,
	1,
	0,
	1376,
	14,
	14,
	1376,
	14,
	14,
	27,
	10,
	14,
	14,
	1376,
	14,
	14,
	1437,
	14,
	1241,
	1376,
	14,
	14,
	10,
	14,
	14,
	26,
	14,
	26,
	1376,
	14,
	14,
	23,
	10,
	1439,
	1407,
	1376,
	14,
	14,
	14,
	26,
	1376,
	14,
	14,
	10,
	15,
	43,
	18,
	163,
	1015,
	1246,
	26,
	23,
	23,
	1247,
	27,
	26,
	23,
	23,
	23,
	3,
	23,
	27,
	27,
	14,
	14,
	27,
	14,
	1251,
	10,
	32,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	32,
	23,
	102,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	32,
	23,
	102,
	23,
	14,
	23,
	14,
	23,
	381,
	23,
	381,
	23,
	381,
	23,
	23,
	23,
	26,
	23,
	26,
	32,
	23,
	102,
	23,
	23,
	14,
	23,
	14,
	23,
	381,
	23,
	381,
	23,
	381,
	23,
	23,
	23,
	23,
	23,
	1256,
	23,
	663,
	23,
	23,
	23,
	906,
	23,
	31,
	23,
	1259,
	23,
	26,
	32,
	23,
	102,
	23,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	26,
	26,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	0,
	0,
	0,
	0,
	0,
	0,
	14,
	14,
	23,
	23,
	23,
	23,
	32,
	10,
	26,
	23,
	23,
	23,
	23,
	32,
	10,
	26,
	23,
	23,
	23,
	23,
	32,
	10,
	26,
	26,
	-1,
	23,
	23,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	102,
	31,
	3,
	23,
	1372,
	96,
	1373,
	1373,
	1374,
	96,
	96,
	1375,
	96,
	96,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	888,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	163,
	1354,
	1355,
	292,
	3,
	23,
	1379,
	1379,
	3,
	23,
	1382,
	23,
	1383,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	3,
	23,
	1388,
	3,
	23,
	1388,
	3,
	23,
	1405,
	1406,
	3,
	23,
	1382,
	1382,
	1382,
	1382,
	3,
	23,
	1406,
	1415,
	3,
	23,
	1417,
	1417,
	3,
	23,
	1419,
	3,
	23,
	1421,
	3,
	23,
	1423,
	3,
	23,
	1425,
	3,
	23,
	1427,
	3,
	23,
	1429,
	3,
	23,
	1431,
	3,
	23,
	1433,
	3,
	23,
	1434,
	3,
	23,
	1438,
	1434,
	3,
	23,
	1440,
	23,
	23,
	-1,
	-1,
	163,
	1354,
	1355,
	292,
	163,
	7,
	750,
	26,
	163,
	1356,
	1357,
	292,
	163,
	7,
	750,
	26,
	163,
	1358,
	1359,
	292,
	163,
	7,
	750,
	26,
	163,
	1356,
	1357,
	292,
	163,
	7,
	750,
	26,
	163,
	1356,
	1357,
	292,
	163,
	7,
	750,
	26,
	163,
	1360,
	1361,
	292,
	163,
	7,
	750,
	26,
	163,
	1362,
	1363,
	292,
	163,
	7,
	750,
	26,
	163,
	1362,
	1363,
	292,
	163,
	7,
	750,
	26,
	163,
	1362,
	1363,
	292,
	163,
	7,
	750,
	26,
	163,
	1364,
	1365,
	292,
	163,
	7,
	750,
	26,
	163,
	1366,
	1367,
	292,
	163,
	7,
	750,
	26,
	163,
	1368,
	1369,
	292,
	163,
	7,
	750,
	26,
	163,
	1360,
	1361,
	292,
	163,
	7,
	750,
	26,
	163,
	1356,
	1357,
	292,
	163,
	7,
	750,
	26,
	163,
	1370,
	1371,
	341,
	163,
	7,
	750,
	26,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[30] = 
{
	{ 0x06000483, 1,  (void**)&Detail_FunctorOfVoid_func_mF783D5E1A5CC1474AC7371278CABF14761C1737D_RuntimeMethod_var, 0 },
	{ 0x06000484, 2,  (void**)&Detail_FunctorOfVoid_destroy_m0F5DC9905B8F43E6DC2E844556B0FF56E4BF67B6_RuntimeMethod_var, 0 },
	{ 0x0600048C, 3,  (void**)&Detail_FunctorOfVoidFromOutputFrame_func_mB6649CCC85AAE55815A216F223BCCFFE5253FEC0_RuntimeMethod_var, 0 },
	{ 0x0600048D, 4,  (void**)&Detail_FunctorOfVoidFromOutputFrame_destroy_mCCA2C33F06C260D5C48CE09D9E03133D4D0CD6DD_RuntimeMethod_var, 0 },
	{ 0x0600048F, 5,  (void**)&Detail_FunctorOfVoidFromTargetAndBool_func_mFA51F1E153904EF00496BE23F062C9A12C273793_RuntimeMethod_var, 0 },
	{ 0x06000490, 6,  (void**)&Detail_FunctorOfVoidFromTargetAndBool_destroy_m6AA500E710A687EBB7C96D03D17E131BD94186FA_RuntimeMethod_var, 0 },
	{ 0x06000496, 7,  (void**)&Detail_FunctorOfVoidFromCloudRecognizationResult_func_m90154CEE7823B813A64107474E84C9D8E49F9FE8_RuntimeMethod_var, 0 },
	{ 0x06000497, 8,  (void**)&Detail_FunctorOfVoidFromCloudRecognizationResult_destroy_mA6894D99F036B445918E01B88C818F63C208B1C1_RuntimeMethod_var, 0 },
	{ 0x0600049B, 9,  (void**)&Detail_FunctorOfVoidFromInputFrame_func_mA2CECDF41A63A69813C1E34E46FCE069F47C6427_RuntimeMethod_var, 0 },
	{ 0x0600049C, 10,  (void**)&Detail_FunctorOfVoidFromInputFrame_destroy_mB73878C35844CC8471D0EF2A8B4A1FE0D3C6F878_RuntimeMethod_var, 0 },
	{ 0x0600049E, 11,  (void**)&Detail_FunctorOfVoidFromCameraState_func_m7FCB6EEC40715D367C83D86227157A9E122568C9_RuntimeMethod_var, 0 },
	{ 0x0600049F, 12,  (void**)&Detail_FunctorOfVoidFromCameraState_destroy_mC70C7A72F6DBC20901D045EFF23EA2AF58A5BF41_RuntimeMethod_var, 0 },
	{ 0x060004A1, 13,  (void**)&Detail_FunctorOfVoidFromPermissionStatusAndString_func_m61FB5A520BF4BF61618A6036F4C225797196DB4D_RuntimeMethod_var, 0 },
	{ 0x060004A2, 14,  (void**)&Detail_FunctorOfVoidFromPermissionStatusAndString_destroy_mFB9A4F27091BABD5B3A3FE933C4003F0478BD7B5_RuntimeMethod_var, 0 },
	{ 0x060004A4, 15,  (void**)&Detail_FunctorOfVoidFromLogLevelAndString_func_m53946E55D87E33DA5FA36DC16B26E9B3DB0B439C_RuntimeMethod_var, 0 },
	{ 0x060004A5, 16,  (void**)&Detail_FunctorOfVoidFromLogLevelAndString_destroy_mADE83DB6360391E4C933515D59EF3F73E4B0B08E_RuntimeMethod_var, 0 },
	{ 0x060004A7, 17,  (void**)&Detail_FunctorOfVoidFromRecordStatusAndString_func_mFA196EC005028814229B1F55F1E24665DF692553_RuntimeMethod_var, 0 },
	{ 0x060004A8, 18,  (void**)&Detail_FunctorOfVoidFromRecordStatusAndString_destroy_m051F182F8E827A97DEC26087EDB6CD6616462981_RuntimeMethod_var, 0 },
	{ 0x060004AC, 19,  (void**)&Detail_FunctorOfVoidFromBool_func_m8C959A83611A4385EE07D73B822F990C10B6EDF6_RuntimeMethod_var, 0 },
	{ 0x060004AD, 20,  (void**)&Detail_FunctorOfVoidFromBool_destroy_mC7D4393D0066F06286A66262A3B206C16F9A0A83_RuntimeMethod_var, 0 },
	{ 0x060004AF, 21,  (void**)&Detail_FunctorOfVoidFromBoolAndStringAndString_func_mE08DFB80305F9D293A7D957A1B409629ED9B4396_RuntimeMethod_var, 0 },
	{ 0x060004B0, 22,  (void**)&Detail_FunctorOfVoidFromBoolAndStringAndString_destroy_m1D50B3D098976A8F7CDA2AEFDCA989ABDF1FBAD5_RuntimeMethod_var, 0 },
	{ 0x060004B2, 23,  (void**)&Detail_FunctorOfVoidFromBoolAndString_func_m87AE9C0004F64626A5E00CFFD0F1A2F868E29D34_RuntimeMethod_var, 0 },
	{ 0x060004B3, 24,  (void**)&Detail_FunctorOfVoidFromBoolAndString_destroy_m1C520BDA809AB254063C19A8E92794F7A368E283_RuntimeMethod_var, 0 },
	{ 0x060004B5, 25,  (void**)&Detail_FunctorOfVoidFromVideoStatus_func_m75DE2AFDC396D76465BE54C22D32881BE260470F_RuntimeMethod_var, 0 },
	{ 0x060004B6, 26,  (void**)&Detail_FunctorOfVoidFromVideoStatus_destroy_m560F57E8D9CDF6C4CCDD018BE704C492D7C6B550_RuntimeMethod_var, 0 },
	{ 0x060004B8, 27,  (void**)&Detail_FunctorOfVoidFromFeedbackFrame_func_mB4288FAC2F09BEFDD8503B7684DDC994C32F8CE7_RuntimeMethod_var, 0 },
	{ 0x060004B9, 28,  (void**)&Detail_FunctorOfVoidFromFeedbackFrame_destroy_m08DAAB8A98135C1DF4CFDC319727C08E4EF48BF2_RuntimeMethod_var, 0 },
	{ 0x060004BB, 29,  (void**)&Detail_FunctorOfOutputFrameFromListOfOutputFrame_func_m458EA310F59A4613F82319C3BE247BE328F9EE19_RuntimeMethod_var, 0 },
	{ 0x060004BC, 30,  (void**)&Detail_FunctorOfOutputFrameFromListOfOutputFrame_destroy_m50A85D540D02D4F023B507E3A9BEF80C6D75D58F_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x0200002B, { 8, 14 } },
	{ 0x02000111, { 7, 1 } },
	{ 0x0600000E, { 0, 1 } },
	{ 0x06000481, { 1, 1 } },
	{ 0x06000482, { 2, 1 } },
	{ 0x060007D7, { 3, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[22] = 
{
	{ (Il2CppRGCTXDataType)2, 11168 },
	{ (Il2CppRGCTXDataType)2, 10580 },
	{ (Il2CppRGCTXDataType)3, 5990 },
	{ (Il2CppRGCTXDataType)2, 11169 },
	{ (Il2CppRGCTXDataType)3, 5991 },
	{ (Il2CppRGCTXDataType)2, 10594 },
	{ (Il2CppRGCTXDataType)3, 5992 },
	{ (Il2CppRGCTXDataType)3, 5993 },
	{ (Il2CppRGCTXDataType)3, 5994 },
	{ (Il2CppRGCTXDataType)2, 10774 },
	{ (Il2CppRGCTXDataType)2, 10775 },
	{ (Il2CppRGCTXDataType)3, 5995 },
	{ (Il2CppRGCTXDataType)3, 5996 },
	{ (Il2CppRGCTXDataType)3, 5997 },
	{ (Il2CppRGCTXDataType)3, 5998 },
	{ (Il2CppRGCTXDataType)2, 10776 },
	{ (Il2CppRGCTXDataType)3, 5999 },
	{ (Il2CppRGCTXDataType)1, 10774 },
	{ (Il2CppRGCTXDataType)2, 10774 },
	{ (Il2CppRGCTXDataType)3, 6000 },
	{ (Il2CppRGCTXDataType)3, 6001 },
	{ (Il2CppRGCTXDataType)3, 6002 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	2333,
	s_methodPointers,
	s_InvokerIndices,
	30,
	s_reversePInvokeIndices,
	6,
	s_rgctxIndices,
	22,
	s_rgctxValues,
	NULL,
};
