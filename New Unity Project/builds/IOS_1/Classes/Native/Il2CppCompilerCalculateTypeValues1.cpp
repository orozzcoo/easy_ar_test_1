﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Byte[]>
struct Action_1_t98224925EBFF210B057632BF4539B3B7AED96DC7;
// System.Action`1<System.Int32>
struct Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134;
// System.Action`1<System.IntPtr>
struct Action_1_t34F8CB5772B9DDE7B1E197366ED54E43CD61052B;
// System.Action`1<easyar.Buffer>
struct Action_1_t4C74FE5E10F7ABDE836FBE69DC43A3160984F6C9;
// System.Action`1<easyar.InputFrameSink>
struct Action_1_tF1A7104D18700C90ACE10E78CF50AE21367A5D3B;
// System.Action`1<easyar.MotionTrackingStatus>
struct Action_1_tBCED0B9AD0B042D7EA7FC7C908253547CF7D9FB6;
// System.Action`2<easyar.RecordStatus,System.String>
struct Action_2_t637B3D6E59757721381E91C325A4FF48667ED83B;
// System.Action`3<easyar.SparseSpatialMapController/SparseSpatialMapInfo,System.Boolean,System.String>
struct Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<System.IntPtr,easyar.RefBase>>
struct Dictionary_2_t3155200ECFC6EDA6629C50379F9451DFFD48380A;
// System.Collections.Generic.List`1<System.Action>
struct List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA;
// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Single>>
struct Queue_1_tF033B03B6B4B6A87650DC344F9DB27113C444AFF;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`1<System.Int32>
struct Func_1_t3ACD5BFE4C52F1FE4426CAE02589E0F6F4ABCB40;
// System.Func`2<easyar.BlockInfo,easyar.BlockInfo>
struct Func_2_t3932EEDF33848B6D0059776C1807816B0F53A516;
// System.Func`2<easyar.Detail/OptionalOfBuffer,easyar.Optional`1<easyar.Buffer>>
struct Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B;
// System.Func`2<easyar.Detail/OptionalOfFrameFilterResult,easyar.Optional`1<easyar.FrameFilterResult>>
struct Func_2_t91A89FAC01AB2000E10EA4D1B1947E74141A91E4;
// System.Func`2<easyar.Detail/OptionalOfImage,easyar.Optional`1<easyar.Image>>
struct Func_2_tA26069AA84D78243CEFE625126A002164E349E8A;
// System.Func`2<easyar.Detail/OptionalOfImageTarget,easyar.Optional`1<easyar.ImageTarget>>
struct Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C;
// System.Func`2<easyar.Detail/OptionalOfMatrix44F,easyar.Optional`1<easyar.Matrix44F>>
struct Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED;
// System.Func`2<easyar.Detail/OptionalOfObjectTarget,easyar.Optional`1<easyar.ObjectTarget>>
struct Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82;
// System.Func`2<easyar.Detail/OptionalOfOutputFrame,easyar.Optional`1<easyar.OutputFrame>>
struct Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330;
// System.Func`2<easyar.Detail/OptionalOfTarget,easyar.Optional`1<easyar.Target>>
struct Func_2_tA927FA06240E2CFE3D8BA1F253E1693D64430ADE;
// System.Func`2<easyar.Image,System.IntPtr>
struct Func_2_t128FD1D4AF99C698E168A005C9FE9ACEF3B4C16B;
// System.Func`2<easyar.Optional`1<System.Action>,easyar.Detail/OptionalOfFunctorOfVoid>
struct Func_2_tC5720BE95F5F8FFEFE5DCCBC02D0392230E319FF;
// System.Func`2<easyar.Optional`1<System.Action`1<System.Boolean>>,easyar.Detail/OptionalOfFunctorOfVoidFromBool>
struct Func_2_t249266CF8DDC010337C524A573C25806D31F6483;
// System.Func`2<easyar.Optional`1<System.Action`1<easyar.CameraState>>,easyar.Detail/OptionalOfFunctorOfVoidFromCameraState>
struct Func_2_t77B75ED6C3AA58D0A3906C94F04E7ADB2A5C92BE;
// System.Func`2<easyar.Optional`1<System.Action`1<easyar.FeedbackFrame>>,easyar.Detail/OptionalOfFunctorOfVoidFromFeedbackFrame>
struct Func_2_t6B4666EBC0C6C5F361CF821367A233859EB87279;
// System.Func`2<easyar.Optional`1<System.Action`1<easyar.InputFrame>>,easyar.Detail/OptionalOfFunctorOfVoidFromInputFrame>
struct Func_2_t03CADFE7F24FC1F062EBA452BFC797B8DDF983CF;
// System.Func`2<easyar.Optional`1<System.Action`1<easyar.OutputFrame>>,easyar.Detail/OptionalOfFunctorOfVoidFromOutputFrame>
struct Func_2_t5F6E82F687DEC12367484BFC6023B94FD12DBFAA;
// System.Func`2<easyar.Optional`1<System.Action`1<easyar.VideoStatus>>,easyar.Detail/OptionalOfFunctorOfVoidFromVideoStatus>
struct Func_2_t40E4EDC6BFBEFEC19B5C117FBC3332302594CEF9;
// System.Func`2<easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>,easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString>
struct Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF;
// System.Func`2<easyar.Optional`1<System.Action`2<easyar.RecordStatus,System.String>>,easyar.Detail/OptionalOfFunctorOfVoidFromRecordStatusAndString>
struct Func_2_tD595369A831C627956085D61BDC735B3D26BFE60;
// System.Func`2<easyar.Optional`1<easyar.FrameFilterResult>,easyar.Detail/OptionalOfFrameFilterResult>
struct Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2;
// System.Func`2<easyar.Optional`1<easyar.Image>,easyar.Detail/OptionalOfImage>
struct Func_2_t0E7435CE8A654E992ED211D2F4DF73F6D322EFB6;
// System.Func`2<easyar.Optional`1<easyar.OutputFrame>,easyar.Detail/OptionalOfOutputFrame>
struct Func_2_tA78DEA62605325A06771D9B6764A353A6592CB89;
// System.Func`2<easyar.OutputFrame,System.IntPtr>
struct Func_2_tF6B69639701D8D81DDFC3C2880A469D2E27A84D6;
// System.Func`2<easyar.PlaneData,System.IntPtr>
struct Func_2_tC9FFF171350E390C0B9D74958853BF4ED44A6CB7;
// System.Func`2<easyar.Target,System.IntPtr>
struct Func_2_tC802123A9271AF3B68242C579341CE984D130EEE;
// System.Func`2<easyar.TargetInstance,System.IntPtr>
struct Func_2_tFB15A7B1ABE7F3EA8789ABF7B3FB05195CFFDEAD;
// System.Func`2<easyar.Vec3F,easyar.Vec3F>
struct Func_2_tC52FADD23EFF4B7E99BB6489D91FE29FCFE034FA;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Threading.Thread
struct Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GUISkin
struct GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7;
// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// easyar.ARCoreCameraDevice
struct ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49;
// easyar.ARKitCameraDevice
struct ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239;
// easyar.ARSession
struct ARSession_tB5970654DC42C1495E6D09E2904603951CCEB03C;
// easyar.CameraDevice
struct CameraDevice_t93B3250A54F0E0FE23EB922E2AF6D94942F84470;
// easyar.CameraParameters
struct CameraParameters_t6C1E79E04BA6188556FE8F1E2B46360A8C3B2BD5;
// easyar.CloudRecognizationResult
struct CloudRecognizationResult_t6494AC886F8FA4C74D70EA0CCA6BAB766525C4FB;
// easyar.Detail/AutoRelease
struct AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67;
// easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/DestroyDelegate
struct DestroyDelegate_t763D40EFF45F320D973DBBDBC2091903407DE9B4;
// easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame/FunctionDelegate
struct FunctionDelegate_t090C1C63C0751FF32A4D0F8A7C02814712221080;
// easyar.Detail/FunctorOfVoid/DestroyDelegate
struct DestroyDelegate_t3AA1949EC75C7221AB90BB3B883229E0CB7C31B2;
// easyar.Detail/FunctorOfVoid/FunctionDelegate
struct FunctionDelegate_t8848D5E47212289B58FA62C1BF4A6181B20EA54F;
// easyar.Detail/FunctorOfVoidFromBool/DestroyDelegate
struct DestroyDelegate_t57CB1B601122C024033C7AD2F46AA139B5E75986;
// easyar.Detail/FunctorOfVoidFromBool/FunctionDelegate
struct FunctionDelegate_t2CFD5626EBF6B782585676A1D9BFD20E0BE31BB4;
// easyar.Detail/FunctorOfVoidFromBoolAndString/DestroyDelegate
struct DestroyDelegate_tFF2989B8C9F4C2E6577E10F17BC39501BEB0737D;
// easyar.Detail/FunctorOfVoidFromBoolAndString/FunctionDelegate
struct FunctionDelegate_tC9991BEBFE57DA2C93AE24E31B226FA0DBBCCBC4;
// easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/DestroyDelegate
struct DestroyDelegate_tF0DF87394419C98CD7EEFD04AF8127B379FA62C6;
// easyar.Detail/FunctorOfVoidFromBoolAndStringAndString/FunctionDelegate
struct FunctionDelegate_tCA3602C4951133C4B97C064574C5508752B54395;
// easyar.Detail/FunctorOfVoidFromCameraState/DestroyDelegate
struct DestroyDelegate_t758A0260C125658E88D189A318456AD9E62ECF32;
// easyar.Detail/FunctorOfVoidFromCameraState/FunctionDelegate
struct FunctionDelegate_tAE9F5819A2ED199C1C375161C8A71F04A69FB01D;
// easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/DestroyDelegate
struct DestroyDelegate_t1AD3EDFB2E4036786B440B416474CDE85F082E26;
// easyar.Detail/FunctorOfVoidFromCloudRecognizationResult/FunctionDelegate
struct FunctionDelegate_tE7D7F32814FA87DC91820A195D9678EB48FC9F81;
// easyar.Detail/FunctorOfVoidFromFeedbackFrame/DestroyDelegate
struct DestroyDelegate_t35BEB98D61989D586C4B902FB7A2F58C6B3AD351;
// easyar.Detail/FunctorOfVoidFromFeedbackFrame/FunctionDelegate
struct FunctionDelegate_t95EA6B135721C887565B9A7C3A9D979FB575D769;
// easyar.Detail/FunctorOfVoidFromInputFrame/DestroyDelegate
struct DestroyDelegate_tA95D741A4ECD162A9E3EC7D8085B5589D738C740;
// easyar.Detail/FunctorOfVoidFromInputFrame/FunctionDelegate
struct FunctionDelegate_t1E48CF3290061A12253AC07FEC9B5A2C0865FC19;
// easyar.Detail/FunctorOfVoidFromLogLevelAndString/DestroyDelegate
struct DestroyDelegate_t2188B56261B93F5F389858154388FF5F297B0033;
// easyar.Detail/FunctorOfVoidFromLogLevelAndString/FunctionDelegate
struct FunctionDelegate_t0E05B7C0F77F94A09651021DEA1DB8377A70C448;
// easyar.Detail/FunctorOfVoidFromOutputFrame/DestroyDelegate
struct DestroyDelegate_tFFAFFA1AA25A9A49F60E9F7B12BB4B41D8B015E3;
// easyar.Detail/FunctorOfVoidFromOutputFrame/FunctionDelegate
struct FunctionDelegate_tE89DEAA79C244DC519F4FF518D34772907DDBB9B;
// easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/DestroyDelegate
struct DestroyDelegate_tE504B96EE209B931BDC1BF72BF91B6A87A772209;
// easyar.Detail/FunctorOfVoidFromPermissionStatusAndString/FunctionDelegate
struct FunctionDelegate_tF4D41B79691A226FEA22151FB99FEA4EC1726836;
// easyar.Detail/FunctorOfVoidFromRecordStatusAndString/DestroyDelegate
struct DestroyDelegate_t3C0897F2F93EC30F5D876405102810B918CF7790;
// easyar.Detail/FunctorOfVoidFromRecordStatusAndString/FunctionDelegate
struct FunctionDelegate_tC303954CF881FABFE27597445CFEB8CEDCF13765;
// easyar.Detail/FunctorOfVoidFromTargetAndBool/DestroyDelegate
struct DestroyDelegate_t14935C2A4187F63C334BF87A911FBF20F1F1BFA5;
// easyar.Detail/FunctorOfVoidFromTargetAndBool/FunctionDelegate
struct FunctionDelegate_t990035A4EB0D6A4E74D65EE5C45F3AF438D69119;
// easyar.Detail/FunctorOfVoidFromVideoStatus/DestroyDelegate
struct DestroyDelegate_t46368E6813BD066929E2C8CB5BF24211E30D5EFD;
// easyar.Detail/FunctorOfVoidFromVideoStatus/FunctionDelegate
struct FunctionDelegate_tF44FA25582324437CAD80F657DCC3970F650DE99;
// easyar.FeedbackFrame
struct FeedbackFrame_tEB60455EDD504A079A1780150E58F46F9EA0DB8E;
// easyar.GUIPopup
struct GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7;
// easyar.InputFrame
struct InputFrame_tCADC80282B08673BD5A63A4B37A662368204000D;
// easyar.InputFrameSink
struct InputFrameSink_t97BD9624A3E94450FC4D84DFC4A54035A82DC1D5;
// easyar.MotionTrackerCameraDevice
struct MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69;
// easyar.OutputFrame
struct OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6;
// easyar.Recorder
struct Recorder_t8323228F81AFAA312EF2F050D621026784AA7A75;
// easyar.RefBase/Retainer
struct Retainer_tF3060567DC74FEC906B36A0E3352EF3955BC7EFC;
// easyar.SparseSpatialMapController
struct SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218;
// easyar.SparseSpatialMapWorkerFrameFilter
struct SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F;
// easyar.SurfaceTracker
struct SurfaceTracker_t5700DA0E1842A81A3E4FD2DBE1887C69B51A2BF0;
// easyar.Target
struct Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46;
// easyar.VIOCameraDeviceUnion/DeviceUnion
struct DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// easyar.APIExtend
struct  APIExtend_t5293A6E69A0F431427FA960410D09AC2F130610C  : public RuntimeObject
{
public:

public:
};


// easyar.Buffer_<>c
struct  U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44_StaticFields
{
public:
	// easyar.Buffer_<>c easyar.Buffer_<>c::<>9
	U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44 * ___U3CU3E9_0;
	// System.Action easyar.Buffer_<>c::<>9__12_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__12_0_1), (void*)value);
	}
};


// easyar.BufferDictionary_<>c
struct  U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804_StaticFields
{
public:
	// easyar.BufferDictionary_<>c easyar.BufferDictionary_<>c::<>9
	U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804 * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfBuffer,easyar.Optional`1<easyar.Buffer>> easyar.BufferDictionary_<>c::<>9__6_0
	Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_0_1), (void*)value);
	}
};


// easyar.BufferPool_<>c
struct  U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914_StaticFields
{
public:
	// easyar.BufferPool_<>c easyar.BufferPool_<>c::<>9
	U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914 * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfBuffer,easyar.Optional`1<easyar.Buffer>> easyar.BufferPool_<>c::<>9__7_0
	Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_t23F3D1CD8290B37442FF38EA01387D4806DB1F1B * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__7_0_1), (void*)value);
	}
};


// easyar.CameraDevice_<>c
struct  U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50_StaticFields
{
public:
	// easyar.CameraDevice_<>c easyar.CameraDevice_<>c::<>9
	U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50 * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<System.Action`1<easyar.CameraState>>,easyar.Detail_OptionalOfFunctorOfVoidFromCameraState> easyar.CameraDevice_<>c::<>9__10_0
	Func_2_t77B75ED6C3AA58D0A3906C94F04E7ADB2A5C92BE * ___U3CU3E9__10_0_1;
	// System.Func`2<easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>,easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString> easyar.CameraDevice_<>c::<>9__11_0
	Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF * ___U3CU3E9__11_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50_StaticFields, ___U3CU3E9__10_0_1)); }
	inline Func_2_t77B75ED6C3AA58D0A3906C94F04E7ADB2A5C92BE * get_U3CU3E9__10_0_1() const { return ___U3CU3E9__10_0_1; }
	inline Func_2_t77B75ED6C3AA58D0A3906C94F04E7ADB2A5C92BE ** get_address_of_U3CU3E9__10_0_1() { return &___U3CU3E9__10_0_1; }
	inline void set_U3CU3E9__10_0_1(Func_2_t77B75ED6C3AA58D0A3906C94F04E7ADB2A5C92BE * value)
	{
		___U3CU3E9__10_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__10_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50_StaticFields, ___U3CU3E9__11_0_2)); }
	inline Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF * get_U3CU3E9__11_0_2() const { return ___U3CU3E9__11_0_2; }
	inline Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF ** get_address_of_U3CU3E9__11_0_2() { return &___U3CU3E9__11_0_2; }
	inline void set_U3CU3E9__11_0_2(Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF * value)
	{
		___U3CU3E9__11_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__11_0_2), (void*)value);
	}
};


// easyar.CameraDeviceSelector
struct  CameraDeviceSelector_t49ED72EBEF29AA08FD21A7D7EF9EEC7832118F62  : public RuntimeObject
{
public:

public:
};


// easyar.CloudRecognizationResult_<>c
struct  U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC_StaticFields
{
public:
	// easyar.CloudRecognizationResult_<>c easyar.CloudRecognizationResult_<>c::<>9
	U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfImageTarget,easyar.Optional`1<easyar.ImageTarget>> easyar.CloudRecognizationResult_<>c::<>9__4_0
	Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}
};


// easyar.CloudRecognizationResult_<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t215BB4C54B46D7533B4EF30FD3E40FAA89BBB0DE  : public RuntimeObject
{
public:
	// easyar.Detail_AutoRelease easyar.CloudRecognizationResult_<>c__DisplayClass5_0::ar
	AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67 * ___ar_0;

public:
	inline static int32_t get_offset_of_ar_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t215BB4C54B46D7533B4EF30FD3E40FAA89BBB0DE, ___ar_0)); }
	inline AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67 * get_ar_0() const { return ___ar_0; }
	inline AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67 ** get_address_of_ar_0() { return &___ar_0; }
	inline void set_ar_0(AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67 * value)
	{
		___ar_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ar_0), (void*)value);
	}
};


// easyar.Detail
struct  Detail_t747CF47D738753813EA98188F036B1FDF753ECA5  : public RuntimeObject
{
public:

public:
};

struct Detail_t747CF47D738753813EA98188F036B1FDF753ECA5_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`2<System.IntPtr,easyar.RefBase>> easyar.Detail::TypeNameToConstructor
	Dictionary_2_t3155200ECFC6EDA6629C50379F9451DFFD48380A * ___TypeNameToConstructor_1;

public:
	inline static int32_t get_offset_of_TypeNameToConstructor_1() { return static_cast<int32_t>(offsetof(Detail_t747CF47D738753813EA98188F036B1FDF753ECA5_StaticFields, ___TypeNameToConstructor_1)); }
	inline Dictionary_2_t3155200ECFC6EDA6629C50379F9451DFFD48380A * get_TypeNameToConstructor_1() const { return ___TypeNameToConstructor_1; }
	inline Dictionary_2_t3155200ECFC6EDA6629C50379F9451DFFD48380A ** get_address_of_TypeNameToConstructor_1() { return &___TypeNameToConstructor_1; }
	inline void set_TypeNameToConstructor_1(Dictionary_2_t3155200ECFC6EDA6629C50379F9451DFFD48380A * value)
	{
		___TypeNameToConstructor_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TypeNameToConstructor_1), (void*)value);
	}
};


// easyar.Detail_<>c
struct  U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields
{
public:
	// easyar.Detail_<>c easyar.Detail_<>c::<>9
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5 * ___U3CU3E9_0;
	// System.Func`2<easyar.Vec3F,easyar.Vec3F> easyar.Detail_<>c::<>9__670_0
	Func_2_tC52FADD23EFF4B7E99BB6489D91FE29FCFE034FA * ___U3CU3E9__670_0_1;
	// System.Func`2<easyar.TargetInstance,System.IntPtr> easyar.Detail_<>c::<>9__672_0
	Func_2_tFB15A7B1ABE7F3EA8789ABF7B3FB05195CFFDEAD * ___U3CU3E9__672_0_2;
	// System.Func`2<easyar.Optional`1<easyar.FrameFilterResult>,easyar.Detail_OptionalOfFrameFilterResult> easyar.Detail_<>c::<>9__676_1
	Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2 * ___U3CU3E9__676_1_3;
	// System.Func`2<easyar.Optional`1<easyar.FrameFilterResult>,easyar.Detail_OptionalOfFrameFilterResult> easyar.Detail_<>c::<>9__676_0
	Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2 * ___U3CU3E9__676_0_4;
	// System.Func`2<easyar.Detail_OptionalOfFrameFilterResult,easyar.Optional`1<easyar.FrameFilterResult>> easyar.Detail_<>c::<>9__677_0
	Func_2_t91A89FAC01AB2000E10EA4D1B1947E74141A91E4 * ___U3CU3E9__677_0_5;
	// System.Func`2<easyar.Target,System.IntPtr> easyar.Detail_<>c::<>9__688_0
	Func_2_tC802123A9271AF3B68242C579341CE984D130EEE * ___U3CU3E9__688_0_6;
	// System.Func`2<easyar.Image,System.IntPtr> easyar.Detail_<>c::<>9__691_0
	Func_2_t128FD1D4AF99C698E168A005C9FE9ACEF3B4C16B * ___U3CU3E9__691_0_7;
	// System.Func`2<easyar.BlockInfo,easyar.BlockInfo> easyar.Detail_<>c::<>9__698_0
	Func_2_t3932EEDF33848B6D0059776C1807816B0F53A516 * ___U3CU3E9__698_0_8;
	// System.Func`2<easyar.PlaneData,System.IntPtr> easyar.Detail_<>c::<>9__725_0
	Func_2_tC9FFF171350E390C0B9D74958853BF4ED44A6CB7 * ___U3CU3E9__725_0_9;
	// System.Func`2<easyar.OutputFrame,System.IntPtr> easyar.Detail_<>c::<>9__756_0
	Func_2_tF6B69639701D8D81DDFC3C2880A469D2E27A84D6 * ___U3CU3E9__756_0_10;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__670_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__670_0_1)); }
	inline Func_2_tC52FADD23EFF4B7E99BB6489D91FE29FCFE034FA * get_U3CU3E9__670_0_1() const { return ___U3CU3E9__670_0_1; }
	inline Func_2_tC52FADD23EFF4B7E99BB6489D91FE29FCFE034FA ** get_address_of_U3CU3E9__670_0_1() { return &___U3CU3E9__670_0_1; }
	inline void set_U3CU3E9__670_0_1(Func_2_tC52FADD23EFF4B7E99BB6489D91FE29FCFE034FA * value)
	{
		___U3CU3E9__670_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__670_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__672_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__672_0_2)); }
	inline Func_2_tFB15A7B1ABE7F3EA8789ABF7B3FB05195CFFDEAD * get_U3CU3E9__672_0_2() const { return ___U3CU3E9__672_0_2; }
	inline Func_2_tFB15A7B1ABE7F3EA8789ABF7B3FB05195CFFDEAD ** get_address_of_U3CU3E9__672_0_2() { return &___U3CU3E9__672_0_2; }
	inline void set_U3CU3E9__672_0_2(Func_2_tFB15A7B1ABE7F3EA8789ABF7B3FB05195CFFDEAD * value)
	{
		___U3CU3E9__672_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__672_0_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__676_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__676_1_3)); }
	inline Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2 * get_U3CU3E9__676_1_3() const { return ___U3CU3E9__676_1_3; }
	inline Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2 ** get_address_of_U3CU3E9__676_1_3() { return &___U3CU3E9__676_1_3; }
	inline void set_U3CU3E9__676_1_3(Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2 * value)
	{
		___U3CU3E9__676_1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__676_1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__676_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__676_0_4)); }
	inline Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2 * get_U3CU3E9__676_0_4() const { return ___U3CU3E9__676_0_4; }
	inline Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2 ** get_address_of_U3CU3E9__676_0_4() { return &___U3CU3E9__676_0_4; }
	inline void set_U3CU3E9__676_0_4(Func_2_t0576E1EBAA1562DDF82C28E539F0E280268B2AB2 * value)
	{
		___U3CU3E9__676_0_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__676_0_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__677_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__677_0_5)); }
	inline Func_2_t91A89FAC01AB2000E10EA4D1B1947E74141A91E4 * get_U3CU3E9__677_0_5() const { return ___U3CU3E9__677_0_5; }
	inline Func_2_t91A89FAC01AB2000E10EA4D1B1947E74141A91E4 ** get_address_of_U3CU3E9__677_0_5() { return &___U3CU3E9__677_0_5; }
	inline void set_U3CU3E9__677_0_5(Func_2_t91A89FAC01AB2000E10EA4D1B1947E74141A91E4 * value)
	{
		___U3CU3E9__677_0_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__677_0_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__688_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__688_0_6)); }
	inline Func_2_tC802123A9271AF3B68242C579341CE984D130EEE * get_U3CU3E9__688_0_6() const { return ___U3CU3E9__688_0_6; }
	inline Func_2_tC802123A9271AF3B68242C579341CE984D130EEE ** get_address_of_U3CU3E9__688_0_6() { return &___U3CU3E9__688_0_6; }
	inline void set_U3CU3E9__688_0_6(Func_2_tC802123A9271AF3B68242C579341CE984D130EEE * value)
	{
		___U3CU3E9__688_0_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__688_0_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__691_0_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__691_0_7)); }
	inline Func_2_t128FD1D4AF99C698E168A005C9FE9ACEF3B4C16B * get_U3CU3E9__691_0_7() const { return ___U3CU3E9__691_0_7; }
	inline Func_2_t128FD1D4AF99C698E168A005C9FE9ACEF3B4C16B ** get_address_of_U3CU3E9__691_0_7() { return &___U3CU3E9__691_0_7; }
	inline void set_U3CU3E9__691_0_7(Func_2_t128FD1D4AF99C698E168A005C9FE9ACEF3B4C16B * value)
	{
		___U3CU3E9__691_0_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__691_0_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__698_0_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__698_0_8)); }
	inline Func_2_t3932EEDF33848B6D0059776C1807816B0F53A516 * get_U3CU3E9__698_0_8() const { return ___U3CU3E9__698_0_8; }
	inline Func_2_t3932EEDF33848B6D0059776C1807816B0F53A516 ** get_address_of_U3CU3E9__698_0_8() { return &___U3CU3E9__698_0_8; }
	inline void set_U3CU3E9__698_0_8(Func_2_t3932EEDF33848B6D0059776C1807816B0F53A516 * value)
	{
		___U3CU3E9__698_0_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__698_0_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__725_0_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__725_0_9)); }
	inline Func_2_tC9FFF171350E390C0B9D74958853BF4ED44A6CB7 * get_U3CU3E9__725_0_9() const { return ___U3CU3E9__725_0_9; }
	inline Func_2_tC9FFF171350E390C0B9D74958853BF4ED44A6CB7 ** get_address_of_U3CU3E9__725_0_9() { return &___U3CU3E9__725_0_9; }
	inline void set_U3CU3E9__725_0_9(Func_2_tC9FFF171350E390C0B9D74958853BF4ED44A6CB7 * value)
	{
		___U3CU3E9__725_0_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__725_0_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__756_0_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields, ___U3CU3E9__756_0_10)); }
	inline Func_2_tF6B69639701D8D81DDFC3C2880A469D2E27A84D6 * get_U3CU3E9__756_0_10() const { return ___U3CU3E9__756_0_10; }
	inline Func_2_tF6B69639701D8D81DDFC3C2880A469D2E27A84D6 ** get_address_of_U3CU3E9__756_0_10() { return &___U3CU3E9__756_0_10; }
	inline void set_U3CU3E9__756_0_10(Func_2_tF6B69639701D8D81DDFC3C2880A469D2E27A84D6 * value)
	{
		___U3CU3E9__756_0_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__756_0_10), (void*)value);
	}
};


// easyar.Detail_<>c__DisplayClass681_0
struct  U3CU3Ec__DisplayClass681_0_tE8CF45153C0065CE9D9B6A53DF2774AB870DD61F  : public RuntimeObject
{
public:
	// easyar.OutputFrame easyar.Detail_<>c__DisplayClass681_0::sarg0
	OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6 * ___sarg0_0;

public:
	inline static int32_t get_offset_of_sarg0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass681_0_tE8CF45153C0065CE9D9B6A53DF2774AB870DD61F, ___sarg0_0)); }
	inline OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6 * get_sarg0_0() const { return ___sarg0_0; }
	inline OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6 ** get_address_of_sarg0_0() { return &___sarg0_0; }
	inline void set_sarg0_0(OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6 * value)
	{
		___sarg0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sarg0_0), (void*)value);
	}
};


// easyar.Detail_<>c__DisplayClass685_0
struct  U3CU3Ec__DisplayClass685_0_t6C9AC89FAADD442EE9AB2EF01DA23B308931B1F8  : public RuntimeObject
{
public:
	// easyar.Target easyar.Detail_<>c__DisplayClass685_0::sarg0
	Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46 * ___sarg0_0;

public:
	inline static int32_t get_offset_of_sarg0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass685_0_t6C9AC89FAADD442EE9AB2EF01DA23B308931B1F8, ___sarg0_0)); }
	inline Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46 * get_sarg0_0() const { return ___sarg0_0; }
	inline Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46 ** get_address_of_sarg0_0() { return &___sarg0_0; }
	inline void set_sarg0_0(Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46 * value)
	{
		___sarg0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sarg0_0), (void*)value);
	}
};


// easyar.Detail_<>c__DisplayClass695_0
struct  U3CU3Ec__DisplayClass695_0_t447BC46E3E8D5F5B62EF132843402E6000A6435B  : public RuntimeObject
{
public:
	// easyar.CloudRecognizationResult easyar.Detail_<>c__DisplayClass695_0::sarg0
	CloudRecognizationResult_t6494AC886F8FA4C74D70EA0CCA6BAB766525C4FB * ___sarg0_0;

public:
	inline static int32_t get_offset_of_sarg0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass695_0_t447BC46E3E8D5F5B62EF132843402E6000A6435B, ___sarg0_0)); }
	inline CloudRecognizationResult_t6494AC886F8FA4C74D70EA0CCA6BAB766525C4FB * get_sarg0_0() const { return ___sarg0_0; }
	inline CloudRecognizationResult_t6494AC886F8FA4C74D70EA0CCA6BAB766525C4FB ** get_address_of_sarg0_0() { return &___sarg0_0; }
	inline void set_sarg0_0(CloudRecognizationResult_t6494AC886F8FA4C74D70EA0CCA6BAB766525C4FB * value)
	{
		___sarg0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sarg0_0), (void*)value);
	}
};


// easyar.Detail_<>c__DisplayClass702_0
struct  U3CU3Ec__DisplayClass702_0_tB19BD97E688E04F6F33F27FC3A4BFCE771702D8F  : public RuntimeObject
{
public:
	// easyar.InputFrame easyar.Detail_<>c__DisplayClass702_0::sarg0
	InputFrame_tCADC80282B08673BD5A63A4B37A662368204000D * ___sarg0_0;

public:
	inline static int32_t get_offset_of_sarg0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass702_0_tB19BD97E688E04F6F33F27FC3A4BFCE771702D8F, ___sarg0_0)); }
	inline InputFrame_tCADC80282B08673BD5A63A4B37A662368204000D * get_sarg0_0() const { return ___sarg0_0; }
	inline InputFrame_tCADC80282B08673BD5A63A4B37A662368204000D ** get_address_of_sarg0_0() { return &___sarg0_0; }
	inline void set_sarg0_0(InputFrame_tCADC80282B08673BD5A63A4B37A662368204000D * value)
	{
		___sarg0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sarg0_0), (void*)value);
	}
};


// easyar.Detail_<>c__DisplayClass749_0
struct  U3CU3Ec__DisplayClass749_0_t7190C5F90C0EE6A77F2435983A3474FFE5A4BD07  : public RuntimeObject
{
public:
	// easyar.FeedbackFrame easyar.Detail_<>c__DisplayClass749_0::sarg0
	FeedbackFrame_tEB60455EDD504A079A1780150E58F46F9EA0DB8E * ___sarg0_0;

public:
	inline static int32_t get_offset_of_sarg0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass749_0_t7190C5F90C0EE6A77F2435983A3474FFE5A4BD07, ___sarg0_0)); }
	inline FeedbackFrame_tEB60455EDD504A079A1780150E58F46F9EA0DB8E * get_sarg0_0() const { return ___sarg0_0; }
	inline FeedbackFrame_tEB60455EDD504A079A1780150E58F46F9EA0DB8E ** get_address_of_sarg0_0() { return &___sarg0_0; }
	inline void set_sarg0_0(FeedbackFrame_tEB60455EDD504A079A1780150E58F46F9EA0DB8E * value)
	{
		___sarg0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sarg0_0), (void*)value);
	}
};


// easyar.Detail_<>c__DisplayClass753_0
struct  U3CU3Ec__DisplayClass753_0_t91BFD741FB69E6370FA8F9D21B31FB47940BDA48  : public RuntimeObject
{
public:
	// easyar.Detail_AutoRelease easyar.Detail_<>c__DisplayClass753_0::ar
	AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67 * ___ar_0;

public:
	inline static int32_t get_offset_of_ar_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass753_0_t91BFD741FB69E6370FA8F9D21B31FB47940BDA48, ___ar_0)); }
	inline AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67 * get_ar_0() const { return ___ar_0; }
	inline AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67 ** get_address_of_ar_0() { return &___ar_0; }
	inline void set_ar_0(AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67 * value)
	{
		___ar_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ar_0), (void*)value);
	}
};


// easyar.Detail_<>c__DisplayClass753_1
struct  U3CU3Ec__DisplayClass753_1_t1C1253CE6211B9425815D04D5B9CE6EF986FBCDF  : public RuntimeObject
{
public:
	// easyar.OutputFrame easyar.Detail_<>c__DisplayClass753_1::_v0_
	OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6 * ____v0__0;

public:
	inline static int32_t get_offset_of__v0__0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass753_1_t1C1253CE6211B9425815D04D5B9CE6EF986FBCDF, ____v0__0)); }
	inline OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6 * get__v0__0() const { return ____v0__0; }
	inline OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6 ** get_address_of__v0__0() { return &____v0__0; }
	inline void set__v0__0(OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6 * value)
	{
		____v0__0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____v0__0), (void*)value);
	}
};


// easyar.Detail_AutoRelease
struct  AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Action> easyar.Detail_AutoRelease::actions
	List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * ___actions_0;

public:
	inline static int32_t get_offset_of_actions_0() { return static_cast<int32_t>(offsetof(AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67, ___actions_0)); }
	inline List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * get_actions_0() const { return ___actions_0; }
	inline List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 ** get_address_of_actions_0() { return &___actions_0; }
	inline void set_actions_0(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * value)
	{
		___actions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actions_0), (void*)value);
	}
};


// easyar.Display
struct  Display_tEA466D953E94BF739413BEBF395FA0F3913A32C5  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> easyar.Display::rotations
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___rotations_0;

public:
	inline static int32_t get_offset_of_rotations_0() { return static_cast<int32_t>(offsetof(Display_tEA466D953E94BF739413BEBF395FA0F3913A32C5, ___rotations_0)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_rotations_0() const { return ___rotations_0; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_rotations_0() { return &___rotations_0; }
	inline void set_rotations_0(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___rotations_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rotations_0), (void*)value);
	}
};


// easyar.Engine
struct  Engine_t935A20D4E1FBBCBBAED27EA5C43F42152E77F67E  : public RuntimeObject
{
public:

public:
};


// easyar.FeedbackFrame_<>c
struct  U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79_StaticFields
{
public:
	// easyar.FeedbackFrame_<>c easyar.FeedbackFrame_<>c::<>9
	U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79 * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<easyar.OutputFrame>,easyar.Detail_OptionalOfOutputFrame> easyar.FeedbackFrame_<>c::<>9__3_0
	Func_2_tA78DEA62605325A06771D9B6764A353A6592CB89 * ___U3CU3E9__3_0_1;
	// System.Func`2<easyar.Detail_OptionalOfOutputFrame,easyar.Optional`1<easyar.OutputFrame>> easyar.FeedbackFrame_<>c::<>9__5_0
	Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330 * ___U3CU3E9__5_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_tA78DEA62605325A06771D9B6764A353A6592CB89 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_tA78DEA62605325A06771D9B6764A353A6592CB89 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_tA78DEA62605325A06771D9B6764A353A6592CB89 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330 * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330 ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330 * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__5_0_2), (void*)value);
	}
};


// easyar.FeedbackFrameSource_<>c
struct  U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905_StaticFields
{
public:
	// easyar.FeedbackFrameSource_<>c easyar.FeedbackFrameSource_<>c::<>9
	U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905 * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<System.Action`1<easyar.FeedbackFrame>>,easyar.Detail_OptionalOfFunctorOfVoidFromFeedbackFrame> easyar.FeedbackFrameSource_<>c::<>9__3_0
	Func_2_t6B4666EBC0C6C5F361CF821367A233859EB87279 * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_t6B4666EBC0C6C5F361CF821367A233859EB87279 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_t6B4666EBC0C6C5F361CF821367A233859EB87279 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_t6B4666EBC0C6C5F361CF821367A233859EB87279 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}
};


// easyar.FileUtil
struct  FileUtil_tB84023BE2B43EE962475F97316CF60D0B83D89B4  : public RuntimeObject
{
public:

public:
};


// easyar.FileUtil_<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t9FC5F8D2234B3AC9254C75A9E79027A839C6021B  : public RuntimeObject
{
public:
	// System.Action`1<easyar.Buffer> easyar.FileUtil_<>c__DisplayClass0_0::onLoad
	Action_1_t4C74FE5E10F7ABDE836FBE69DC43A3160984F6C9 * ___onLoad_0;

public:
	inline static int32_t get_offset_of_onLoad_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t9FC5F8D2234B3AC9254C75A9E79027A839C6021B, ___onLoad_0)); }
	inline Action_1_t4C74FE5E10F7ABDE836FBE69DC43A3160984F6C9 * get_onLoad_0() const { return ___onLoad_0; }
	inline Action_1_t4C74FE5E10F7ABDE836FBE69DC43A3160984F6C9 ** get_address_of_onLoad_0() { return &___onLoad_0; }
	inline void set_onLoad_0(Action_1_t4C74FE5E10F7ABDE836FBE69DC43A3160984F6C9 * value)
	{
		___onLoad_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onLoad_0), (void*)value);
	}
};


// easyar.GUIPopup_<ShowMessage>d__8
struct  U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1  : public RuntimeObject
{
public:
	// System.Int32 easyar.GUIPopup_<ShowMessage>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object easyar.GUIPopup_<ShowMessage>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// easyar.GUIPopup easyar.GUIPopup_<ShowMessage>d__8::<>4__this
	GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7 * ___U3CU3E4__this_2;
	// System.Single easyar.GUIPopup_<ShowMessage>d__8::<time>5__2
	float ___U3CtimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1, ___U3CU3E4__this_2)); }
	inline GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1, ___U3CtimeU3E5__2_3)); }
	inline float get_U3CtimeU3E5__2_3() const { return ___U3CtimeU3E5__2_3; }
	inline float* get_address_of_U3CtimeU3E5__2_3() { return &___U3CtimeU3E5__2_3; }
	inline void set_U3CtimeU3E5__2_3(float value)
	{
		___U3CtimeU3E5__2_3 = value;
	}
};


// easyar.ImageHelper
struct  ImageHelper_t30C212450B16C4C02A3EA93BB7801CEB459E9E5A  : public RuntimeObject
{
public:

public:
};


// easyar.ImageHelper_<>c
struct  U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD_StaticFields
{
public:
	// easyar.ImageHelper_<>c easyar.ImageHelper_<>c::<>9
	U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfImage,easyar.Optional`1<easyar.Image>> easyar.ImageHelper_<>c::<>9__0_0
	Func_2_tA26069AA84D78243CEFE625126A002164E349E8A * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_tA26069AA84D78243CEFE625126A002164E349E8A * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_tA26069AA84D78243CEFE625126A002164E349E8A ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_tA26069AA84D78243CEFE625126A002164E349E8A * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_0_1), (void*)value);
	}
};


// easyar.ImageTarget_<>c
struct  U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields
{
public:
	// easyar.ImageTarget_<>c easyar.ImageTarget_<>c::<>9
	U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6 * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfImageTarget,easyar.Optional`1<easyar.ImageTarget>> easyar.ImageTarget_<>c::<>9__4_0
	Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * ___U3CU3E9__4_0_1;
	// System.Func`2<easyar.Detail_OptionalOfImageTarget,easyar.Optional`1<easyar.ImageTarget>> easyar.ImageTarget_<>c::<>9__5_0
	Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * ___U3CU3E9__5_0_2;
	// System.Func`2<easyar.Detail_OptionalOfImageTarget,easyar.Optional`1<easyar.ImageTarget>> easyar.ImageTarget_<>c::<>9__6_0
	Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * ___U3CU3E9__6_0_3;
	// System.Func`2<easyar.Detail_OptionalOfImageTarget,easyar.Optional`1<easyar.ImageTarget>> easyar.ImageTarget_<>c::<>9__8_0
	Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * ___U3CU3E9__8_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__5_0_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields, ___U3CU3E9__6_0_3)); }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * get_U3CU3E9__6_0_3() const { return ___U3CU3E9__6_0_3; }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C ** get_address_of_U3CU3E9__6_0_3() { return &___U3CU3E9__6_0_3; }
	inline void set_U3CU3E9__6_0_3(Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * value)
	{
		___U3CU3E9__6_0_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_0_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields, ___U3CU3E9__8_0_4)); }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * get_U3CU3E9__8_0_4() const { return ___U3CU3E9__8_0_4; }
	inline Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C ** get_address_of_U3CU3E9__8_0_4() { return &___U3CU3E9__8_0_4; }
	inline void set_U3CU3E9__8_0_4(Func_2_t509B2C0C7EA100B0D8B49777F2A39E97A4B5D42C * value)
	{
		___U3CU3E9__8_0_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__8_0_4), (void*)value);
	}
};


// easyar.InputFrameSource_<>c
struct  U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1_StaticFields
{
public:
	// easyar.InputFrameSource_<>c easyar.InputFrameSource_<>c::<>9
	U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1 * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<System.Action`1<easyar.InputFrame>>,easyar.Detail_OptionalOfFunctorOfVoidFromInputFrame> easyar.InputFrameSource_<>c::<>9__3_0
	Func_2_t03CADFE7F24FC1F062EBA452BFC797B8DDF983CF * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_t03CADFE7F24FC1F062EBA452BFC797B8DDF983CF * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_t03CADFE7F24FC1F062EBA452BFC797B8DDF983CF ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_t03CADFE7F24FC1F062EBA452BFC797B8DDF983CF * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}
};


// easyar.JniUtility
struct  JniUtility_t368D91DE5D5E59933210A9B9C9985CEC9115AD1D  : public RuntimeObject
{
public:

public:
};


// easyar.Log
struct  Log_t04DE32FFC173FCC192BE8481C75B27AF85CCBC0A  : public RuntimeObject
{
public:

public:
};


// easyar.ObjectTarget_<>c
struct  U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921_StaticFields
{
public:
	// easyar.ObjectTarget_<>c easyar.ObjectTarget_<>c::<>9
	U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921 * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfObjectTarget,easyar.Optional`1<easyar.ObjectTarget>> easyar.ObjectTarget_<>c::<>9__4_0
	Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82 * ___U3CU3E9__4_0_1;
	// System.Func`2<easyar.Detail_OptionalOfObjectTarget,easyar.Optional`1<easyar.ObjectTarget>> easyar.ObjectTarget_<>c::<>9__5_0
	Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82 * ___U3CU3E9__5_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82 * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82 ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_tC562B64A4DA5C5295D52712F8C4A7131AA2D7D82 * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__5_0_2), (void*)value);
	}
};


// easyar.OutputFrameBuffer_<>c
struct  U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4_StaticFields
{
public:
	// easyar.OutputFrameBuffer_<>c easyar.OutputFrameBuffer_<>c::<>9
	U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4 * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfOutputFrame,easyar.Optional`1<easyar.OutputFrame>> easyar.OutputFrameBuffer_<>c::<>9__6_0
	Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_tEB9279A26852903D095EEBAEF973C4C4EA436330 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_0_1), (void*)value);
	}
};


// easyar.OutputFrameSource_<>c
struct  U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27_StaticFields
{
public:
	// easyar.OutputFrameSource_<>c easyar.OutputFrameSource_<>c::<>9
	U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27 * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<System.Action`1<easyar.OutputFrame>>,easyar.Detail_OptionalOfFunctorOfVoidFromOutputFrame> easyar.OutputFrameSource_<>c::<>9__3_0
	Func_2_t5F6E82F687DEC12367484BFC6023B94FD12DBFAA * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_t5F6E82F687DEC12367484BFC6023B94FD12DBFAA * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_t5F6E82F687DEC12367484BFC6023B94FD12DBFAA ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_t5F6E82F687DEC12367484BFC6023B94FD12DBFAA * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}
};


// easyar.Recorder_<>c
struct  U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247_StaticFields
{
public:
	// easyar.Recorder_<>c easyar.Recorder_<>c::<>9
	U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247 * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<System.Action`2<easyar.PermissionStatus,System.String>>,easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString> easyar.Recorder_<>c::<>9__4_0
	Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF * ___U3CU3E9__4_0_1;
	// System.Func`2<easyar.Optional`1<System.Action`2<easyar.RecordStatus,System.String>>,easyar.Detail_OptionalOfFunctorOfVoidFromRecordStatusAndString> easyar.Recorder_<>c::<>9__5_0
	Func_2_tD595369A831C627956085D61BDC735B3D26BFE60 * ___U3CU3E9__5_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t82AD4DCA4752C04FF99FBEB37CD7C9C88CB87DFF * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_tD595369A831C627956085D61BDC735B3D26BFE60 * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_tD595369A831C627956085D61BDC735B3D26BFE60 ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_tD595369A831C627956085D61BDC735B3D26BFE60 * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__5_0_2), (void*)value);
	}
};


// easyar.SignalSource_<>c
struct  U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3_StaticFields
{
public:
	// easyar.SignalSource_<>c easyar.SignalSource_<>c::<>9
	U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3 * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<System.Action>,easyar.Detail_OptionalOfFunctorOfVoid> easyar.SignalSource_<>c::<>9__3_0
	Func_2_tC5720BE95F5F8FFEFE5DCCBC02D0392230E319FF * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Func_2_tC5720BE95F5F8FFEFE5DCCBC02D0392230E319FF * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Func_2_tC5720BE95F5F8FFEFE5DCCBC02D0392230E319FF ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Func_2_tC5720BE95F5F8FFEFE5DCCBC02D0392230E319FF * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}
};


// easyar.SparseSpatialMap_<>c
struct  U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007_StaticFields
{
public:
	// easyar.SparseSpatialMap_<>c easyar.SparseSpatialMap_<>c::<>9
	U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007 * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<System.Action`1<System.Boolean>>,easyar.Detail_OptionalOfFunctorOfVoidFromBool> easyar.SparseSpatialMap_<>c::<>9__16_0
	Func_2_t249266CF8DDC010337C524A573C25806D31F6483 * ___U3CU3E9__16_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007_StaticFields, ___U3CU3E9__16_0_1)); }
	inline Func_2_t249266CF8DDC010337C524A573C25806D31F6483 * get_U3CU3E9__16_0_1() const { return ___U3CU3E9__16_0_1; }
	inline Func_2_t249266CF8DDC010337C524A573C25806D31F6483 ** get_address_of_U3CU3E9__16_0_1() { return &___U3CU3E9__16_0_1; }
	inline void set_U3CU3E9__16_0_1(Func_2_t249266CF8DDC010337C524A573C25806D31F6483 * value)
	{
		___U3CU3E9__16_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_0_1), (void*)value);
	}
};


// easyar.SparseSpatialMapManager_<>c
struct  U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D_StaticFields
{
public:
	// easyar.SparseSpatialMapManager_<>c easyar.SparseSpatialMapManager_<>c::<>9
	U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<easyar.Image>,easyar.Detail_OptionalOfImage> easyar.SparseSpatialMapManager_<>c::<>9__5_0
	Func_2_t0E7435CE8A654E992ED211D2F4DF73F6D322EFB6 * ___U3CU3E9__5_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Func_2_t0E7435CE8A654E992ED211D2F4DF73F6D322EFB6 * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Func_2_t0E7435CE8A654E992ED211D2F4DF73F6D322EFB6 ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Func_2_t0E7435CE8A654E992ED211D2F4DF73F6D322EFB6 * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__5_0_1), (void*)value);
	}
};


// easyar.SparseSpatialMapResult_<>c
struct  U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982_StaticFields
{
public:
	// easyar.SparseSpatialMapResult_<>c easyar.SparseSpatialMapResult_<>c::<>9
	U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982 * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfMatrix44F,easyar.Optional`1<easyar.Matrix44F>> easyar.SparseSpatialMapResult_<>c::<>9__4_0
	Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED * ___U3CU3E9__4_0_1;
	// System.Func`2<easyar.Detail_OptionalOfMatrix44F,easyar.Optional`1<easyar.Matrix44F>> easyar.SparseSpatialMapResult_<>c::<>9__5_0
	Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED * ___U3CU3E9__5_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_tC7879335E82C033C3B52D93266B95BA8E27563ED * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__5_0_2), (void*)value);
	}
};


// easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass64_0
struct  U3CU3Ec__DisplayClass64_0_t2447ADF55FF732F7A04F5897C33974CBFB472F55  : public RuntimeObject
{
public:
	// easyar.SparseSpatialMapWorkerFrameFilter easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass64_0::<>4__this
	SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * ___U3CU3E4__this_0;
	// easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass64_0::controller
	SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * ___controller_1;
	// System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String> easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass64_0::callback
	Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * ___callback_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass64_0_t2447ADF55FF732F7A04F5897C33974CBFB472F55, ___U3CU3E4__this_0)); }
	inline SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_controller_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass64_0_t2447ADF55FF732F7A04F5897C33974CBFB472F55, ___controller_1)); }
	inline SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * get_controller_1() const { return ___controller_1; }
	inline SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 ** get_address_of_controller_1() { return &___controller_1; }
	inline void set_controller_1(SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * value)
	{
		___controller_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controller_1), (void*)value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass64_0_t2447ADF55FF732F7A04F5897C33974CBFB472F55, ___callback_2)); }
	inline Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * get_callback_2() const { return ___callback_2; }
	inline Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_2), (void*)value);
	}
};


// easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass65_0
struct  U3CU3Ec__DisplayClass65_0_t8B0FAECA16A92C91E17B2A15748D81263ED2045C  : public RuntimeObject
{
public:
	// easyar.SparseSpatialMapWorkerFrameFilter easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass65_0::<>4__this
	SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * ___U3CU3E4__this_0;
	// easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass65_0::controller
	SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * ___controller_1;
	// System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String> easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass65_0::callback
	Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * ___callback_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t8B0FAECA16A92C91E17B2A15748D81263ED2045C, ___U3CU3E4__this_0)); }
	inline SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_controller_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t8B0FAECA16A92C91E17B2A15748D81263ED2045C, ___controller_1)); }
	inline SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * get_controller_1() const { return ___controller_1; }
	inline SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 ** get_address_of_controller_1() { return &___controller_1; }
	inline void set_controller_1(SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * value)
	{
		___controller_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controller_1), (void*)value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t8B0FAECA16A92C91E17B2A15748D81263ED2045C, ___callback_2)); }
	inline Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * get_callback_2() const { return ___callback_2; }
	inline Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_2), (void*)value);
	}
};


// easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass66_0
struct  U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC  : public RuntimeObject
{
public:
	// System.String easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass66_0::name
	String_t* ___name_0;
	// easyar.SparseSpatialMapWorkerFrameFilter easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass66_0::<>4__this
	SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * ___U3CU3E4__this_1;
	// easyar.SparseSpatialMapController easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass66_0::controller
	SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * ___controller_2;
	// System.Action`3<easyar.SparseSpatialMapController_SparseSpatialMapInfo,System.Boolean,System.String> easyar.SparseSpatialMapWorkerFrameFilter_<>c__DisplayClass66_0::callback
	Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * ___callback_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC, ___U3CU3E4__this_1)); }
	inline SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SparseSpatialMapWorkerFrameFilter_tFCE7C549CEF376605CCDF096A53866DC3585482F * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}

	inline static int32_t get_offset_of_controller_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC, ___controller_2)); }
	inline SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * get_controller_2() const { return ___controller_2; }
	inline SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 ** get_address_of_controller_2() { return &___controller_2; }
	inline void set_controller_2(SparseSpatialMapController_t6175C0273C6B1864F6EC9195D830DA6C4C309218 * value)
	{
		___controller_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___controller_2), (void*)value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC, ___callback_3)); }
	inline Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * get_callback_3() const { return ___callback_3; }
	inline Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_3_tE9023222B4CC73788A7F2C9F4FFB0C6D4A2AAE39 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_3), (void*)value);
	}
};


// easyar.TargetInstance_<>c
struct  U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA_StaticFields
{
public:
	// easyar.TargetInstance_<>c easyar.TargetInstance_<>c::<>9
	U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA * ___U3CU3E9_0;
	// System.Func`2<easyar.Detail_OptionalOfTarget,easyar.Optional`1<easyar.Target>> easyar.TargetInstance_<>c::<>9__5_0
	Func_2_tA927FA06240E2CFE3D8BA1F253E1693D64430ADE * ___U3CU3E9__5_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA_StaticFields, ___U3CU3E9__5_0_1)); }
	inline Func_2_tA927FA06240E2CFE3D8BA1F253E1693D64430ADE * get_U3CU3E9__5_0_1() const { return ___U3CU3E9__5_0_1; }
	inline Func_2_tA927FA06240E2CFE3D8BA1F253E1693D64430ADE ** get_address_of_U3CU3E9__5_0_1() { return &___U3CU3E9__5_0_1; }
	inline void set_U3CU3E9__5_0_1(Func_2_tA927FA06240E2CFE3D8BA1F253E1693D64430ADE * value)
	{
		___U3CU3E9__5_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__5_0_1), (void*)value);
	}
};


// easyar.ThreadWorker
struct  ThreadWorker_t884565929A86E905E099E91E3ADF13BA90CD7C9E  : public RuntimeObject
{
public:
	// System.Threading.Thread easyar.ThreadWorker::thread
	Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * ___thread_0;
	// System.Boolean easyar.ThreadWorker::finished
	bool ___finished_1;
	// System.Collections.Generic.Queue`1<System.Action> easyar.ThreadWorker::queue
	Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * ___queue_2;

public:
	inline static int32_t get_offset_of_thread_0() { return static_cast<int32_t>(offsetof(ThreadWorker_t884565929A86E905E099E91E3ADF13BA90CD7C9E, ___thread_0)); }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * get_thread_0() const { return ___thread_0; }
	inline Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 ** get_address_of_thread_0() { return &___thread_0; }
	inline void set_thread_0(Thread_tF60E0A146CD3B5480CB65FF9B6016E84C5460CC7 * value)
	{
		___thread_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thread_0), (void*)value);
	}

	inline static int32_t get_offset_of_finished_1() { return static_cast<int32_t>(offsetof(ThreadWorker_t884565929A86E905E099E91E3ADF13BA90CD7C9E, ___finished_1)); }
	inline bool get_finished_1() const { return ___finished_1; }
	inline bool* get_address_of_finished_1() { return &___finished_1; }
	inline void set_finished_1(bool value)
	{
		___finished_1 = value;
	}

	inline static int32_t get_offset_of_queue_2() { return static_cast<int32_t>(offsetof(ThreadWorker_t884565929A86E905E099E91E3ADF13BA90CD7C9E, ___queue_2)); }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * get_queue_2() const { return ___queue_2; }
	inline Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA ** get_address_of_queue_2() { return &___queue_2; }
	inline void set_queue_2(Queue_1_t17C991E61C7592E9EA90E7A1A497EF1D7E7B88AA * value)
	{
		___queue_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___queue_2), (void*)value);
	}
};


// easyar.TransformUtil
struct  TransformUtil_t1E80C9C7DBA8482ECD9F374EFDB0771D77C496BB  : public RuntimeObject
{
public:

public:
};


// easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t3E7F517A55F291899B812B3B126710ECBD47D6C1  : public RuntimeObject
{
public:
	// easyar.MotionTrackerCameraDevice easyar.VIOCameraDeviceUnion_<>c__DisplayClass35_0::device
	MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69 * ___device_0;

public:
	inline static int32_t get_offset_of_device_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t3E7F517A55F291899B812B3B126710ECBD47D6C1, ___device_0)); }
	inline MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69 * get_device_0() const { return ___device_0; }
	inline MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69 ** get_address_of_device_0() { return &___device_0; }
	inline void set_device_0(MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69 * value)
	{
		___device_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___device_0), (void*)value);
	}
};


// easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t51241EBB9A57AC8C14A97E8AD7C0E4A042093B77  : public RuntimeObject
{
public:
	// easyar.ARKitCameraDevice easyar.VIOCameraDeviceUnion_<>c__DisplayClass36_0::device
	ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239 * ___device_0;

public:
	inline static int32_t get_offset_of_device_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t51241EBB9A57AC8C14A97E8AD7C0E4A042093B77, ___device_0)); }
	inline ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239 * get_device_0() const { return ___device_0; }
	inline ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239 ** get_address_of_device_0() { return &___device_0; }
	inline void set_device_0(ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239 * value)
	{
		___device_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___device_0), (void*)value);
	}
};


// easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t99023F1F073B643F7F59F61C460EA2A2319F5F5C  : public RuntimeObject
{
public:
	// easyar.ARCoreCameraDevice easyar.VIOCameraDeviceUnion_<>c__DisplayClass37_0::device
	ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49 * ___device_0;

public:
	inline static int32_t get_offset_of_device_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t99023F1F073B643F7F59F61C460EA2A2319F5F5C, ___device_0)); }
	inline ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49 * get_device_0() const { return ___device_0; }
	inline ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49 ** get_address_of_device_0() { return &___device_0; }
	inline void set_device_0(ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49 * value)
	{
		___device_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___device_0), (void*)value);
	}
};


// easyar.VideoPlayer_<>c
struct  U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C_StaticFields
{
public:
	// easyar.VideoPlayer_<>c easyar.VideoPlayer_<>c::<>9
	U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C * ___U3CU3E9_0;
	// System.Func`2<easyar.Optional`1<System.Action`1<easyar.VideoStatus>>,easyar.Detail_OptionalOfFunctorOfVoidFromVideoStatus> easyar.VideoPlayer_<>c::<>9__7_0
	Func_2_t40E4EDC6BFBEFEC19B5C117FBC3332302594CEF9 * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_t40E4EDC6BFBEFEC19B5C117FBC3332302594CEF9 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_t40E4EDC6BFBEFEC19B5C117FBC3332302594CEF9 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_t40E4EDC6BFBEFEC19B5C117FBC3332302594CEF9 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__7_0_1), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// easyar.BlockInfo
struct  BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09 
{
public:
	// System.Int32 easyar.BlockInfo::x
	int32_t ___x_0;
	// System.Int32 easyar.BlockInfo::y
	int32_t ___y_1;
	// System.Int32 easyar.BlockInfo::z
	int32_t ___z_2;
	// System.Int32 easyar.BlockInfo::numOfVertex
	int32_t ___numOfVertex_3;
	// System.Int32 easyar.BlockInfo::startPointOfVertex
	int32_t ___startPointOfVertex_4;
	// System.Int32 easyar.BlockInfo::numOfIndex
	int32_t ___numOfIndex_5;
	// System.Int32 easyar.BlockInfo::startPointOfIndex
	int32_t ___startPointOfIndex_6;
	// System.Int32 easyar.BlockInfo::version
	int32_t ___version_7;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_numOfVertex_3() { return static_cast<int32_t>(offsetof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09, ___numOfVertex_3)); }
	inline int32_t get_numOfVertex_3() const { return ___numOfVertex_3; }
	inline int32_t* get_address_of_numOfVertex_3() { return &___numOfVertex_3; }
	inline void set_numOfVertex_3(int32_t value)
	{
		___numOfVertex_3 = value;
	}

	inline static int32_t get_offset_of_startPointOfVertex_4() { return static_cast<int32_t>(offsetof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09, ___startPointOfVertex_4)); }
	inline int32_t get_startPointOfVertex_4() const { return ___startPointOfVertex_4; }
	inline int32_t* get_address_of_startPointOfVertex_4() { return &___startPointOfVertex_4; }
	inline void set_startPointOfVertex_4(int32_t value)
	{
		___startPointOfVertex_4 = value;
	}

	inline static int32_t get_offset_of_numOfIndex_5() { return static_cast<int32_t>(offsetof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09, ___numOfIndex_5)); }
	inline int32_t get_numOfIndex_5() const { return ___numOfIndex_5; }
	inline int32_t* get_address_of_numOfIndex_5() { return &___numOfIndex_5; }
	inline void set_numOfIndex_5(int32_t value)
	{
		___numOfIndex_5 = value;
	}

	inline static int32_t get_offset_of_startPointOfIndex_6() { return static_cast<int32_t>(offsetof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09, ___startPointOfIndex_6)); }
	inline int32_t get_startPointOfIndex_6() const { return ___startPointOfIndex_6; }
	inline int32_t* get_address_of_startPointOfIndex_6() { return &___startPointOfIndex_6; }
	inline void set_startPointOfIndex_6(int32_t value)
	{
		___startPointOfIndex_6 = value;
	}

	inline static int32_t get_offset_of_version_7() { return static_cast<int32_t>(offsetof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09, ___version_7)); }
	inline int32_t get_version_7() const { return ___version_7; }
	inline int32_t* get_address_of_version_7() { return &___version_7; }
	inline void set_version_7(int32_t value)
	{
		___version_7 = value;
	}
};


// easyar.Matrix33F
struct  Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634 
{
public:
	// System.Single easyar.Matrix33F::data_0
	float ___data_0_0;
	// System.Single easyar.Matrix33F::data_1
	float ___data_1_1;
	// System.Single easyar.Matrix33F::data_2
	float ___data_2_2;
	// System.Single easyar.Matrix33F::data_3
	float ___data_3_3;
	// System.Single easyar.Matrix33F::data_4
	float ___data_4_4;
	// System.Single easyar.Matrix33F::data_5
	float ___data_5_5;
	// System.Single easyar.Matrix33F::data_6
	float ___data_6_6;
	// System.Single easyar.Matrix33F::data_7
	float ___data_7_7;
	// System.Single easyar.Matrix33F::data_8
	float ___data_8_8;

public:
	inline static int32_t get_offset_of_data_0_0() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_0_0)); }
	inline float get_data_0_0() const { return ___data_0_0; }
	inline float* get_address_of_data_0_0() { return &___data_0_0; }
	inline void set_data_0_0(float value)
	{
		___data_0_0 = value;
	}

	inline static int32_t get_offset_of_data_1_1() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_1_1)); }
	inline float get_data_1_1() const { return ___data_1_1; }
	inline float* get_address_of_data_1_1() { return &___data_1_1; }
	inline void set_data_1_1(float value)
	{
		___data_1_1 = value;
	}

	inline static int32_t get_offset_of_data_2_2() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_2_2)); }
	inline float get_data_2_2() const { return ___data_2_2; }
	inline float* get_address_of_data_2_2() { return &___data_2_2; }
	inline void set_data_2_2(float value)
	{
		___data_2_2 = value;
	}

	inline static int32_t get_offset_of_data_3_3() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_3_3)); }
	inline float get_data_3_3() const { return ___data_3_3; }
	inline float* get_address_of_data_3_3() { return &___data_3_3; }
	inline void set_data_3_3(float value)
	{
		___data_3_3 = value;
	}

	inline static int32_t get_offset_of_data_4_4() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_4_4)); }
	inline float get_data_4_4() const { return ___data_4_4; }
	inline float* get_address_of_data_4_4() { return &___data_4_4; }
	inline void set_data_4_4(float value)
	{
		___data_4_4 = value;
	}

	inline static int32_t get_offset_of_data_5_5() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_5_5)); }
	inline float get_data_5_5() const { return ___data_5_5; }
	inline float* get_address_of_data_5_5() { return &___data_5_5; }
	inline void set_data_5_5(float value)
	{
		___data_5_5 = value;
	}

	inline static int32_t get_offset_of_data_6_6() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_6_6)); }
	inline float get_data_6_6() const { return ___data_6_6; }
	inline float* get_address_of_data_6_6() { return &___data_6_6; }
	inline void set_data_6_6(float value)
	{
		___data_6_6 = value;
	}

	inline static int32_t get_offset_of_data_7_7() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_7_7)); }
	inline float get_data_7_7() const { return ___data_7_7; }
	inline float* get_address_of_data_7_7() { return &___data_7_7; }
	inline void set_data_7_7(float value)
	{
		___data_7_7 = value;
	}

	inline static int32_t get_offset_of_data_8_8() { return static_cast<int32_t>(offsetof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634, ___data_8_8)); }
	inline float get_data_8_8() const { return ___data_8_8; }
	inline float* get_address_of_data_8_8() { return &___data_8_8; }
	inline void set_data_8_8(float value)
	{
		___data_8_8 = value;
	}
};


// easyar.Matrix44F
struct  Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3 
{
public:
	// System.Single easyar.Matrix44F::data_0
	float ___data_0_0;
	// System.Single easyar.Matrix44F::data_1
	float ___data_1_1;
	// System.Single easyar.Matrix44F::data_2
	float ___data_2_2;
	// System.Single easyar.Matrix44F::data_3
	float ___data_3_3;
	// System.Single easyar.Matrix44F::data_4
	float ___data_4_4;
	// System.Single easyar.Matrix44F::data_5
	float ___data_5_5;
	// System.Single easyar.Matrix44F::data_6
	float ___data_6_6;
	// System.Single easyar.Matrix44F::data_7
	float ___data_7_7;
	// System.Single easyar.Matrix44F::data_8
	float ___data_8_8;
	// System.Single easyar.Matrix44F::data_9
	float ___data_9_9;
	// System.Single easyar.Matrix44F::data_10
	float ___data_10_10;
	// System.Single easyar.Matrix44F::data_11
	float ___data_11_11;
	// System.Single easyar.Matrix44F::data_12
	float ___data_12_12;
	// System.Single easyar.Matrix44F::data_13
	float ___data_13_13;
	// System.Single easyar.Matrix44F::data_14
	float ___data_14_14;
	// System.Single easyar.Matrix44F::data_15
	float ___data_15_15;

public:
	inline static int32_t get_offset_of_data_0_0() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_0_0)); }
	inline float get_data_0_0() const { return ___data_0_0; }
	inline float* get_address_of_data_0_0() { return &___data_0_0; }
	inline void set_data_0_0(float value)
	{
		___data_0_0 = value;
	}

	inline static int32_t get_offset_of_data_1_1() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_1_1)); }
	inline float get_data_1_1() const { return ___data_1_1; }
	inline float* get_address_of_data_1_1() { return &___data_1_1; }
	inline void set_data_1_1(float value)
	{
		___data_1_1 = value;
	}

	inline static int32_t get_offset_of_data_2_2() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_2_2)); }
	inline float get_data_2_2() const { return ___data_2_2; }
	inline float* get_address_of_data_2_2() { return &___data_2_2; }
	inline void set_data_2_2(float value)
	{
		___data_2_2 = value;
	}

	inline static int32_t get_offset_of_data_3_3() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_3_3)); }
	inline float get_data_3_3() const { return ___data_3_3; }
	inline float* get_address_of_data_3_3() { return &___data_3_3; }
	inline void set_data_3_3(float value)
	{
		___data_3_3 = value;
	}

	inline static int32_t get_offset_of_data_4_4() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_4_4)); }
	inline float get_data_4_4() const { return ___data_4_4; }
	inline float* get_address_of_data_4_4() { return &___data_4_4; }
	inline void set_data_4_4(float value)
	{
		___data_4_4 = value;
	}

	inline static int32_t get_offset_of_data_5_5() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_5_5)); }
	inline float get_data_5_5() const { return ___data_5_5; }
	inline float* get_address_of_data_5_5() { return &___data_5_5; }
	inline void set_data_5_5(float value)
	{
		___data_5_5 = value;
	}

	inline static int32_t get_offset_of_data_6_6() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_6_6)); }
	inline float get_data_6_6() const { return ___data_6_6; }
	inline float* get_address_of_data_6_6() { return &___data_6_6; }
	inline void set_data_6_6(float value)
	{
		___data_6_6 = value;
	}

	inline static int32_t get_offset_of_data_7_7() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_7_7)); }
	inline float get_data_7_7() const { return ___data_7_7; }
	inline float* get_address_of_data_7_7() { return &___data_7_7; }
	inline void set_data_7_7(float value)
	{
		___data_7_7 = value;
	}

	inline static int32_t get_offset_of_data_8_8() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_8_8)); }
	inline float get_data_8_8() const { return ___data_8_8; }
	inline float* get_address_of_data_8_8() { return &___data_8_8; }
	inline void set_data_8_8(float value)
	{
		___data_8_8 = value;
	}

	inline static int32_t get_offset_of_data_9_9() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_9_9)); }
	inline float get_data_9_9() const { return ___data_9_9; }
	inline float* get_address_of_data_9_9() { return &___data_9_9; }
	inline void set_data_9_9(float value)
	{
		___data_9_9 = value;
	}

	inline static int32_t get_offset_of_data_10_10() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_10_10)); }
	inline float get_data_10_10() const { return ___data_10_10; }
	inline float* get_address_of_data_10_10() { return &___data_10_10; }
	inline void set_data_10_10(float value)
	{
		___data_10_10 = value;
	}

	inline static int32_t get_offset_of_data_11_11() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_11_11)); }
	inline float get_data_11_11() const { return ___data_11_11; }
	inline float* get_address_of_data_11_11() { return &___data_11_11; }
	inline void set_data_11_11(float value)
	{
		___data_11_11 = value;
	}

	inline static int32_t get_offset_of_data_12_12() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_12_12)); }
	inline float get_data_12_12() const { return ___data_12_12; }
	inline float* get_address_of_data_12_12() { return &___data_12_12; }
	inline void set_data_12_12(float value)
	{
		___data_12_12 = value;
	}

	inline static int32_t get_offset_of_data_13_13() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_13_13)); }
	inline float get_data_13_13() const { return ___data_13_13; }
	inline float* get_address_of_data_13_13() { return &___data_13_13; }
	inline void set_data_13_13(float value)
	{
		___data_13_13 = value;
	}

	inline static int32_t get_offset_of_data_14_14() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_14_14)); }
	inline float get_data_14_14() const { return ___data_14_14; }
	inline float* get_address_of_data_14_14() { return &___data_14_14; }
	inline void set_data_14_14(float value)
	{
		___data_14_14 = value;
	}

	inline static int32_t get_offset_of_data_15_15() { return static_cast<int32_t>(offsetof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3, ___data_15_15)); }
	inline float get_data_15_15() const { return ___data_15_15; }
	inline float* get_address_of_data_15_15() { return &___data_15_15; }
	inline void set_data_15_15(float value)
	{
		___data_15_15 = value;
	}
};


// easyar.Unit
struct  Unit_t3673CF84D960209286D3322DFEFA8817003B66A0 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Unit_t3673CF84D960209286D3322DFEFA8817003B66A0__padding[1];
	};

public:
};


// easyar.Vec2F
struct  Vec2F_tBB4EB623E12BF4D1A4027EBAEA291CF8D8037227 
{
public:
	// System.Single easyar.Vec2F::data_0
	float ___data_0_0;
	// System.Single easyar.Vec2F::data_1
	float ___data_1_1;

public:
	inline static int32_t get_offset_of_data_0_0() { return static_cast<int32_t>(offsetof(Vec2F_tBB4EB623E12BF4D1A4027EBAEA291CF8D8037227, ___data_0_0)); }
	inline float get_data_0_0() const { return ___data_0_0; }
	inline float* get_address_of_data_0_0() { return &___data_0_0; }
	inline void set_data_0_0(float value)
	{
		___data_0_0 = value;
	}

	inline static int32_t get_offset_of_data_1_1() { return static_cast<int32_t>(offsetof(Vec2F_tBB4EB623E12BF4D1A4027EBAEA291CF8D8037227, ___data_1_1)); }
	inline float get_data_1_1() const { return ___data_1_1; }
	inline float* get_address_of_data_1_1() { return &___data_1_1; }
	inline void set_data_1_1(float value)
	{
		___data_1_1 = value;
	}
};


// easyar.Vec2I
struct  Vec2I_t01C0457CB2DB321F0AB349CAC888D0FD6E73BBD2 
{
public:
	// System.Int32 easyar.Vec2I::data_0
	int32_t ___data_0_0;
	// System.Int32 easyar.Vec2I::data_1
	int32_t ___data_1_1;

public:
	inline static int32_t get_offset_of_data_0_0() { return static_cast<int32_t>(offsetof(Vec2I_t01C0457CB2DB321F0AB349CAC888D0FD6E73BBD2, ___data_0_0)); }
	inline int32_t get_data_0_0() const { return ___data_0_0; }
	inline int32_t* get_address_of_data_0_0() { return &___data_0_0; }
	inline void set_data_0_0(int32_t value)
	{
		___data_0_0 = value;
	}

	inline static int32_t get_offset_of_data_1_1() { return static_cast<int32_t>(offsetof(Vec2I_t01C0457CB2DB321F0AB349CAC888D0FD6E73BBD2, ___data_1_1)); }
	inline int32_t get_data_1_1() const { return ___data_1_1; }
	inline int32_t* get_address_of_data_1_1() { return &___data_1_1; }
	inline void set_data_1_1(int32_t value)
	{
		___data_1_1 = value;
	}
};


// easyar.Vec3F
struct  Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E 
{
public:
	// System.Single easyar.Vec3F::data_0
	float ___data_0_0;
	// System.Single easyar.Vec3F::data_1
	float ___data_1_1;
	// System.Single easyar.Vec3F::data_2
	float ___data_2_2;

public:
	inline static int32_t get_offset_of_data_0_0() { return static_cast<int32_t>(offsetof(Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E, ___data_0_0)); }
	inline float get_data_0_0() const { return ___data_0_0; }
	inline float* get_address_of_data_0_0() { return &___data_0_0; }
	inline void set_data_0_0(float value)
	{
		___data_0_0 = value;
	}

	inline static int32_t get_offset_of_data_1_1() { return static_cast<int32_t>(offsetof(Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E, ___data_1_1)); }
	inline float get_data_1_1() const { return ___data_1_1; }
	inline float* get_address_of_data_1_1() { return &___data_1_1; }
	inline void set_data_1_1(float value)
	{
		___data_1_1 = value;
	}

	inline static int32_t get_offset_of_data_2_2() { return static_cast<int32_t>(offsetof(Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E, ___data_2_2)); }
	inline float get_data_2_2() const { return ___data_2_2; }
	inline float* get_address_of_data_2_2() { return &___data_2_2; }
	inline void set_data_2_2(float value)
	{
		___data_2_2 = value;
	}
};


// easyar.Vec4F
struct  Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612 
{
public:
	// System.Single easyar.Vec4F::data_0
	float ___data_0_0;
	// System.Single easyar.Vec4F::data_1
	float ___data_1_1;
	// System.Single easyar.Vec4F::data_2
	float ___data_2_2;
	// System.Single easyar.Vec4F::data_3
	float ___data_3_3;

public:
	inline static int32_t get_offset_of_data_0_0() { return static_cast<int32_t>(offsetof(Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612, ___data_0_0)); }
	inline float get_data_0_0() const { return ___data_0_0; }
	inline float* get_address_of_data_0_0() { return &___data_0_0; }
	inline void set_data_0_0(float value)
	{
		___data_0_0 = value;
	}

	inline static int32_t get_offset_of_data_1_1() { return static_cast<int32_t>(offsetof(Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612, ___data_1_1)); }
	inline float get_data_1_1() const { return ___data_1_1; }
	inline float* get_address_of_data_1_1() { return &___data_1_1; }
	inline void set_data_1_1(float value)
	{
		___data_1_1 = value;
	}

	inline static int32_t get_offset_of_data_2_2() { return static_cast<int32_t>(offsetof(Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612, ___data_2_2)); }
	inline float get_data_2_2() const { return ___data_2_2; }
	inline float* get_address_of_data_2_2() { return &___data_2_2; }
	inline void set_data_2_2(float value)
	{
		___data_2_2 = value;
	}

	inline static int32_t get_offset_of_data_3_3() { return static_cast<int32_t>(offsetof(Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612, ___data_3_3)); }
	inline float get_data_3_3() const { return ___data_3_3; }
	inline float* get_address_of_data_3_3() { return &___data_3_3; }
	inline void set_data_3_3(float value)
	{
		___data_3_3 = value;
	}
};


// easyar.Vec4I
struct  Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B 
{
public:
	// System.Int32 easyar.Vec4I::data_0
	int32_t ___data_0_0;
	// System.Int32 easyar.Vec4I::data_1
	int32_t ___data_1_1;
	// System.Int32 easyar.Vec4I::data_2
	int32_t ___data_2_2;
	// System.Int32 easyar.Vec4I::data_3
	int32_t ___data_3_3;

public:
	inline static int32_t get_offset_of_data_0_0() { return static_cast<int32_t>(offsetof(Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B, ___data_0_0)); }
	inline int32_t get_data_0_0() const { return ___data_0_0; }
	inline int32_t* get_address_of_data_0_0() { return &___data_0_0; }
	inline void set_data_0_0(int32_t value)
	{
		___data_0_0 = value;
	}

	inline static int32_t get_offset_of_data_1_1() { return static_cast<int32_t>(offsetof(Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B, ___data_1_1)); }
	inline int32_t get_data_1_1() const { return ___data_1_1; }
	inline int32_t* get_address_of_data_1_1() { return &___data_1_1; }
	inline void set_data_1_1(int32_t value)
	{
		___data_1_1 = value;
	}

	inline static int32_t get_offset_of_data_2_2() { return static_cast<int32_t>(offsetof(Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B, ___data_2_2)); }
	inline int32_t get_data_2_2() const { return ___data_2_2; }
	inline int32_t* get_address_of_data_2_2() { return &___data_2_2; }
	inline void set_data_2_2(int32_t value)
	{
		___data_2_2 = value;
	}

	inline static int32_t get_offset_of_data_3_3() { return static_cast<int32_t>(offsetof(Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B, ___data_3_3)); }
	inline int32_t get_data_3_3() const { return ___data_3_3; }
	inline int32_t* get_address_of_data_3_3() { return &___data_3_3; }
	inline void set_data_3_3(int32_t value)
	{
		___data_3_3 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// easyar.AndroidCameraApiType
struct  AndroidCameraApiType_t4131D8FB0C923BC130B759AC51AC12A27BD36325 
{
public:
	// System.Int32 easyar.AndroidCameraApiType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AndroidCameraApiType_t4131D8FB0C923BC130B759AC51AC12A27BD36325, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.Buffer_<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t34C2759F7AAD82551ABA6DBF020B91CFC379C568  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.GCHandle easyar.Buffer_<>c__DisplayClass11_0::h
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___h_0;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t34C2759F7AAD82551ABA6DBF020B91CFC379C568, ___h_0)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get_h_0() const { return ___h_0; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		___h_0 = value;
	}
};


// easyar.Buffer_<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t1FD40E9A43A2BD116FC4ED588F635D5A4493425A  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.GCHandle easyar.Buffer_<>c__DisplayClass13_0::h
	GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  ___h_0;
	// System.Action easyar.Buffer_<>c__DisplayClass13_0::deleter
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___deleter_1;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t1FD40E9A43A2BD116FC4ED588F635D5A4493425A, ___h_0)); }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  get_h_0() const { return ___h_0; }
	inline GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3 * get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(GCHandle_t39FAEE3EA592432C93B574A31DD83B87F1847DE3  value)
	{
		___h_0 = value;
	}

	inline static int32_t get_offset_of_deleter_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t1FD40E9A43A2BD116FC4ED588F635D5A4493425A, ___deleter_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_deleter_1() const { return ___deleter_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_deleter_1() { return &___deleter_1; }
	inline void set_deleter_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___deleter_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deleter_1), (void*)value);
	}
};


// easyar.CameraDeviceFocusMode
struct  CameraDeviceFocusMode_t7CB13E84AF8DE3D7D946A1DBCAA2105F4952953E 
{
public:
	// System.Int32 easyar.CameraDeviceFocusMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraDeviceFocusMode_t7CB13E84AF8DE3D7D946A1DBCAA2105F4952953E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.CameraDevicePreference
struct  CameraDevicePreference_t4BEF9FCA55102557DAA1A9F6BE552CE11AD049AB 
{
public:
	// System.Int32 easyar.CameraDevicePreference::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraDevicePreference_t4BEF9FCA55102557DAA1A9F6BE552CE11AD049AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.CameraDevicePresetProfile
struct  CameraDevicePresetProfile_tC5D271617B3555C299330A1AEE5FF46A59374F9C 
{
public:
	// System.Int32 easyar.CameraDevicePresetProfile::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraDevicePresetProfile_tC5D271617B3555C299330A1AEE5FF46A59374F9C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.CameraDeviceType
struct  CameraDeviceType_t3085F22241377C20BE28D68740E0D32EB4B6ADFA 
{
public:
	// System.Int32 easyar.CameraDeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraDeviceType_t3085F22241377C20BE28D68740E0D32EB4B6ADFA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.CameraState
struct  CameraState_t41D0938A1158181E7B0C9E509CF05C61D9CEF658 
{
public:
	// System.Int32 easyar.CameraState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraState_t41D0938A1158181E7B0C9E509CF05C61D9CEF658, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.CloudRecognizationStatus
struct  CloudRecognizationStatus_tCCBA3D4F61D18616929D1E995BD5CECF4B861CD8 
{
public:
	// System.Int32 easyar.CloudRecognizationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CloudRecognizationStatus_tCCBA3D4F61D18616929D1E995BD5CECF4B861CD8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame
struct  FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_FunctionDelegate easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame::_func
	FunctionDelegate_t090C1C63C0751FF32A4D0F8A7C02814712221080 * ____func_1;
	// easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_DestroyDelegate easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame::_destroy
	DestroyDelegate_t763D40EFF45F320D973DBBDBC2091903407DE9B4 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D, ____func_1)); }
	inline FunctionDelegate_t090C1C63C0751FF32A4D0F8A7C02814712221080 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_t090C1C63C0751FF32A4D0F8A7C02814712221080 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_t090C1C63C0751FF32A4D0F8A7C02814712221080 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D, ____destroy_2)); }
	inline DestroyDelegate_t763D40EFF45F320D973DBBDBC2091903407DE9B4 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t763D40EFF45F320D973DBBDBC2091903407DE9B4 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t763D40EFF45F320D973DBBDBC2091903407DE9B4 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame
struct FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfOutputFrameFromListOfOutputFrame
struct FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoid
struct  FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoid::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoid_FunctionDelegate easyar.Detail_FunctorOfVoid::_func
	FunctionDelegate_t8848D5E47212289B58FA62C1BF4A6181B20EA54F * ____func_1;
	// easyar.Detail_FunctorOfVoid_DestroyDelegate easyar.Detail_FunctorOfVoid::_destroy
	DestroyDelegate_t3AA1949EC75C7221AB90BB3B883229E0CB7C31B2 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3, ____func_1)); }
	inline FunctionDelegate_t8848D5E47212289B58FA62C1BF4A6181B20EA54F * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_t8848D5E47212289B58FA62C1BF4A6181B20EA54F ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_t8848D5E47212289B58FA62C1BF4A6181B20EA54F * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3, ____destroy_2)); }
	inline DestroyDelegate_t3AA1949EC75C7221AB90BB3B883229E0CB7C31B2 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t3AA1949EC75C7221AB90BB3B883229E0CB7C31B2 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t3AA1949EC75C7221AB90BB3B883229E0CB7C31B2 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoid
struct FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoid
struct FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromBool
struct  FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromBool::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromBool_FunctionDelegate easyar.Detail_FunctorOfVoidFromBool::_func
	FunctionDelegate_t2CFD5626EBF6B782585676A1D9BFD20E0BE31BB4 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromBool_DestroyDelegate easyar.Detail_FunctorOfVoidFromBool::_destroy
	DestroyDelegate_t57CB1B601122C024033C7AD2F46AA139B5E75986 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6, ____func_1)); }
	inline FunctionDelegate_t2CFD5626EBF6B782585676A1D9BFD20E0BE31BB4 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_t2CFD5626EBF6B782585676A1D9BFD20E0BE31BB4 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_t2CFD5626EBF6B782585676A1D9BFD20E0BE31BB4 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6, ____destroy_2)); }
	inline DestroyDelegate_t57CB1B601122C024033C7AD2F46AA139B5E75986 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t57CB1B601122C024033C7AD2F46AA139B5E75986 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t57CB1B601122C024033C7AD2F46AA139B5E75986 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromBool
struct FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromBool
struct FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromBoolAndString
struct  FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromBoolAndString::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromBoolAndString_FunctionDelegate easyar.Detail_FunctorOfVoidFromBoolAndString::_func
	FunctionDelegate_tC9991BEBFE57DA2C93AE24E31B226FA0DBBCCBC4 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromBoolAndString_DestroyDelegate easyar.Detail_FunctorOfVoidFromBoolAndString::_destroy
	DestroyDelegate_tFF2989B8C9F4C2E6577E10F17BC39501BEB0737D * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC, ____func_1)); }
	inline FunctionDelegate_tC9991BEBFE57DA2C93AE24E31B226FA0DBBCCBC4 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_tC9991BEBFE57DA2C93AE24E31B226FA0DBBCCBC4 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_tC9991BEBFE57DA2C93AE24E31B226FA0DBBCCBC4 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC, ____destroy_2)); }
	inline DestroyDelegate_tFF2989B8C9F4C2E6577E10F17BC39501BEB0737D * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_tFF2989B8C9F4C2E6577E10F17BC39501BEB0737D ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_tFF2989B8C9F4C2E6577E10F17BC39501BEB0737D * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromBoolAndString
struct FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromBoolAndString
struct FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromBoolAndStringAndString
struct  FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromBoolAndStringAndString::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_FunctionDelegate easyar.Detail_FunctorOfVoidFromBoolAndStringAndString::_func
	FunctionDelegate_tCA3602C4951133C4B97C064574C5508752B54395 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_DestroyDelegate easyar.Detail_FunctorOfVoidFromBoolAndStringAndString::_destroy
	DestroyDelegate_tF0DF87394419C98CD7EEFD04AF8127B379FA62C6 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435, ____func_1)); }
	inline FunctionDelegate_tCA3602C4951133C4B97C064574C5508752B54395 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_tCA3602C4951133C4B97C064574C5508752B54395 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_tCA3602C4951133C4B97C064574C5508752B54395 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435, ____destroy_2)); }
	inline DestroyDelegate_tF0DF87394419C98CD7EEFD04AF8127B379FA62C6 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_tF0DF87394419C98CD7EEFD04AF8127B379FA62C6 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_tF0DF87394419C98CD7EEFD04AF8127B379FA62C6 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromBoolAndStringAndString
struct FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromBoolAndStringAndString
struct FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromCameraState
struct  FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromCameraState::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromCameraState_FunctionDelegate easyar.Detail_FunctorOfVoidFromCameraState::_func
	FunctionDelegate_tAE9F5819A2ED199C1C375161C8A71F04A69FB01D * ____func_1;
	// easyar.Detail_FunctorOfVoidFromCameraState_DestroyDelegate easyar.Detail_FunctorOfVoidFromCameraState::_destroy
	DestroyDelegate_t758A0260C125658E88D189A318456AD9E62ECF32 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5, ____func_1)); }
	inline FunctionDelegate_tAE9F5819A2ED199C1C375161C8A71F04A69FB01D * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_tAE9F5819A2ED199C1C375161C8A71F04A69FB01D ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_tAE9F5819A2ED199C1C375161C8A71F04A69FB01D * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5, ____destroy_2)); }
	inline DestroyDelegate_t758A0260C125658E88D189A318456AD9E62ECF32 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t758A0260C125658E88D189A318456AD9E62ECF32 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t758A0260C125658E88D189A318456AD9E62ECF32 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromCameraState
struct FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromCameraState
struct FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromCloudRecognizationResult
struct  FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromCloudRecognizationResult::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_FunctionDelegate easyar.Detail_FunctorOfVoidFromCloudRecognizationResult::_func
	FunctionDelegate_tE7D7F32814FA87DC91820A195D9678EB48FC9F81 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_DestroyDelegate easyar.Detail_FunctorOfVoidFromCloudRecognizationResult::_destroy
	DestroyDelegate_t1AD3EDFB2E4036786B440B416474CDE85F082E26 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9, ____func_1)); }
	inline FunctionDelegate_tE7D7F32814FA87DC91820A195D9678EB48FC9F81 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_tE7D7F32814FA87DC91820A195D9678EB48FC9F81 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_tE7D7F32814FA87DC91820A195D9678EB48FC9F81 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9, ____destroy_2)); }
	inline DestroyDelegate_t1AD3EDFB2E4036786B440B416474CDE85F082E26 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t1AD3EDFB2E4036786B440B416474CDE85F082E26 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t1AD3EDFB2E4036786B440B416474CDE85F082E26 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromCloudRecognizationResult
struct FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromCloudRecognizationResult
struct FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromFeedbackFrame
struct  FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromFeedbackFrame::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromFeedbackFrame_FunctionDelegate easyar.Detail_FunctorOfVoidFromFeedbackFrame::_func
	FunctionDelegate_t95EA6B135721C887565B9A7C3A9D979FB575D769 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromFeedbackFrame_DestroyDelegate easyar.Detail_FunctorOfVoidFromFeedbackFrame::_destroy
	DestroyDelegate_t35BEB98D61989D586C4B902FB7A2F58C6B3AD351 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E, ____func_1)); }
	inline FunctionDelegate_t95EA6B135721C887565B9A7C3A9D979FB575D769 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_t95EA6B135721C887565B9A7C3A9D979FB575D769 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_t95EA6B135721C887565B9A7C3A9D979FB575D769 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E, ____destroy_2)); }
	inline DestroyDelegate_t35BEB98D61989D586C4B902FB7A2F58C6B3AD351 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t35BEB98D61989D586C4B902FB7A2F58C6B3AD351 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t35BEB98D61989D586C4B902FB7A2F58C6B3AD351 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromFeedbackFrame
struct FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromFeedbackFrame
struct FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromInputFrame
struct  FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromInputFrame::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromInputFrame_FunctionDelegate easyar.Detail_FunctorOfVoidFromInputFrame::_func
	FunctionDelegate_t1E48CF3290061A12253AC07FEC9B5A2C0865FC19 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromInputFrame_DestroyDelegate easyar.Detail_FunctorOfVoidFromInputFrame::_destroy
	DestroyDelegate_tA95D741A4ECD162A9E3EC7D8085B5589D738C740 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81, ____func_1)); }
	inline FunctionDelegate_t1E48CF3290061A12253AC07FEC9B5A2C0865FC19 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_t1E48CF3290061A12253AC07FEC9B5A2C0865FC19 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_t1E48CF3290061A12253AC07FEC9B5A2C0865FC19 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81, ____destroy_2)); }
	inline DestroyDelegate_tA95D741A4ECD162A9E3EC7D8085B5589D738C740 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_tA95D741A4ECD162A9E3EC7D8085B5589D738C740 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_tA95D741A4ECD162A9E3EC7D8085B5589D738C740 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromInputFrame
struct FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromInputFrame
struct FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromLogLevelAndString
struct  FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromLogLevelAndString::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromLogLevelAndString_FunctionDelegate easyar.Detail_FunctorOfVoidFromLogLevelAndString::_func
	FunctionDelegate_t0E05B7C0F77F94A09651021DEA1DB8377A70C448 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromLogLevelAndString_DestroyDelegate easyar.Detail_FunctorOfVoidFromLogLevelAndString::_destroy
	DestroyDelegate_t2188B56261B93F5F389858154388FF5F297B0033 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB, ____func_1)); }
	inline FunctionDelegate_t0E05B7C0F77F94A09651021DEA1DB8377A70C448 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_t0E05B7C0F77F94A09651021DEA1DB8377A70C448 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_t0E05B7C0F77F94A09651021DEA1DB8377A70C448 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB, ____destroy_2)); }
	inline DestroyDelegate_t2188B56261B93F5F389858154388FF5F297B0033 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t2188B56261B93F5F389858154388FF5F297B0033 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t2188B56261B93F5F389858154388FF5F297B0033 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromLogLevelAndString
struct FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromLogLevelAndString
struct FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromOutputFrame
struct  FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromOutputFrame::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromOutputFrame_FunctionDelegate easyar.Detail_FunctorOfVoidFromOutputFrame::_func
	FunctionDelegate_tE89DEAA79C244DC519F4FF518D34772907DDBB9B * ____func_1;
	// easyar.Detail_FunctorOfVoidFromOutputFrame_DestroyDelegate easyar.Detail_FunctorOfVoidFromOutputFrame::_destroy
	DestroyDelegate_tFFAFFA1AA25A9A49F60E9F7B12BB4B41D8B015E3 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34, ____func_1)); }
	inline FunctionDelegate_tE89DEAA79C244DC519F4FF518D34772907DDBB9B * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_tE89DEAA79C244DC519F4FF518D34772907DDBB9B ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_tE89DEAA79C244DC519F4FF518D34772907DDBB9B * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34, ____destroy_2)); }
	inline DestroyDelegate_tFFAFFA1AA25A9A49F60E9F7B12BB4B41D8B015E3 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_tFFAFFA1AA25A9A49F60E9F7B12BB4B41D8B015E3 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_tFFAFFA1AA25A9A49F60E9F7B12BB4B41D8B015E3 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromOutputFrame
struct FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromOutputFrame
struct FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromPermissionStatusAndString
struct  FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromPermissionStatusAndString::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_FunctionDelegate easyar.Detail_FunctorOfVoidFromPermissionStatusAndString::_func
	FunctionDelegate_tF4D41B79691A226FEA22151FB99FEA4EC1726836 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_DestroyDelegate easyar.Detail_FunctorOfVoidFromPermissionStatusAndString::_destroy
	DestroyDelegate_tE504B96EE209B931BDC1BF72BF91B6A87A772209 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1, ____func_1)); }
	inline FunctionDelegate_tF4D41B79691A226FEA22151FB99FEA4EC1726836 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_tF4D41B79691A226FEA22151FB99FEA4EC1726836 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_tF4D41B79691A226FEA22151FB99FEA4EC1726836 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1, ____destroy_2)); }
	inline DestroyDelegate_tE504B96EE209B931BDC1BF72BF91B6A87A772209 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_tE504B96EE209B931BDC1BF72BF91B6A87A772209 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_tE504B96EE209B931BDC1BF72BF91B6A87A772209 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromPermissionStatusAndString
struct FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromPermissionStatusAndString
struct FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromRecordStatusAndString
struct  FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromRecordStatusAndString::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromRecordStatusAndString_FunctionDelegate easyar.Detail_FunctorOfVoidFromRecordStatusAndString::_func
	FunctionDelegate_tC303954CF881FABFE27597445CFEB8CEDCF13765 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromRecordStatusAndString_DestroyDelegate easyar.Detail_FunctorOfVoidFromRecordStatusAndString::_destroy
	DestroyDelegate_t3C0897F2F93EC30F5D876405102810B918CF7790 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F, ____func_1)); }
	inline FunctionDelegate_tC303954CF881FABFE27597445CFEB8CEDCF13765 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_tC303954CF881FABFE27597445CFEB8CEDCF13765 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_tC303954CF881FABFE27597445CFEB8CEDCF13765 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F, ____destroy_2)); }
	inline DestroyDelegate_t3C0897F2F93EC30F5D876405102810B918CF7790 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t3C0897F2F93EC30F5D876405102810B918CF7790 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t3C0897F2F93EC30F5D876405102810B918CF7790 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromRecordStatusAndString
struct FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromRecordStatusAndString
struct FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromTargetAndBool
struct  FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromTargetAndBool::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromTargetAndBool_FunctionDelegate easyar.Detail_FunctorOfVoidFromTargetAndBool::_func
	FunctionDelegate_t990035A4EB0D6A4E74D65EE5C45F3AF438D69119 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromTargetAndBool_DestroyDelegate easyar.Detail_FunctorOfVoidFromTargetAndBool::_destroy
	DestroyDelegate_t14935C2A4187F63C334BF87A911FBF20F1F1BFA5 * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D, ____func_1)); }
	inline FunctionDelegate_t990035A4EB0D6A4E74D65EE5C45F3AF438D69119 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_t990035A4EB0D6A4E74D65EE5C45F3AF438D69119 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_t990035A4EB0D6A4E74D65EE5C45F3AF438D69119 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D, ____destroy_2)); }
	inline DestroyDelegate_t14935C2A4187F63C334BF87A911FBF20F1F1BFA5 * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t14935C2A4187F63C334BF87A911FBF20F1F1BFA5 ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t14935C2A4187F63C334BF87A911FBF20F1F1BFA5 * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromTargetAndBool
struct FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromTargetAndBool
struct FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_FunctorOfVoidFromVideoStatus
struct  FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863 
{
public:
	// System.IntPtr easyar.Detail_FunctorOfVoidFromVideoStatus::_state
	intptr_t ____state_0;
	// easyar.Detail_FunctorOfVoidFromVideoStatus_FunctionDelegate easyar.Detail_FunctorOfVoidFromVideoStatus::_func
	FunctionDelegate_tF44FA25582324437CAD80F657DCC3970F650DE99 * ____func_1;
	// easyar.Detail_FunctorOfVoidFromVideoStatus_DestroyDelegate easyar.Detail_FunctorOfVoidFromVideoStatus::_destroy
	DestroyDelegate_t46368E6813BD066929E2C8CB5BF24211E30D5EFD * ____destroy_2;

public:
	inline static int32_t get_offset_of__state_0() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863, ____state_0)); }
	inline intptr_t get__state_0() const { return ____state_0; }
	inline intptr_t* get_address_of__state_0() { return &____state_0; }
	inline void set__state_0(intptr_t value)
	{
		____state_0 = value;
	}

	inline static int32_t get_offset_of__func_1() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863, ____func_1)); }
	inline FunctionDelegate_tF44FA25582324437CAD80F657DCC3970F650DE99 * get__func_1() const { return ____func_1; }
	inline FunctionDelegate_tF44FA25582324437CAD80F657DCC3970F650DE99 ** get_address_of__func_1() { return &____func_1; }
	inline void set__func_1(FunctionDelegate_tF44FA25582324437CAD80F657DCC3970F650DE99 * value)
	{
		____func_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____func_1), (void*)value);
	}

	inline static int32_t get_offset_of__destroy_2() { return static_cast<int32_t>(offsetof(FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863, ____destroy_2)); }
	inline DestroyDelegate_t46368E6813BD066929E2C8CB5BF24211E30D5EFD * get__destroy_2() const { return ____destroy_2; }
	inline DestroyDelegate_t46368E6813BD066929E2C8CB5BF24211E30D5EFD ** get_address_of__destroy_2() { return &____destroy_2; }
	inline void set__destroy_2(DestroyDelegate_t46368E6813BD066929E2C8CB5BF24211E30D5EFD * value)
	{
		____destroy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destroy_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/FunctorOfVoidFromVideoStatus
struct FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863_marshaled_pinvoke
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};
// Native definition for COM marshalling of easyar.Detail/FunctorOfVoidFromVideoStatus
struct FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863_marshaled_com
{
	intptr_t ____state_0;
	Il2CppMethodPointer ____func_1;
	Il2CppMethodPointer ____destroy_2;
};

// easyar.Detail_OptionalOfBuffer
struct  OptionalOfBuffer_t0757A8E4275A9BD768752AB53823BF73BE8FA7D2 
{
public:
	// System.Byte easyar.Detail_OptionalOfBuffer::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail_OptionalOfBuffer::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfBuffer_t0757A8E4275A9BD768752AB53823BF73BE8FA7D2, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfBuffer_t0757A8E4275A9BD768752AB53823BF73BE8FA7D2, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// easyar.Detail_OptionalOfFrameFilterResult
struct  OptionalOfFrameFilterResult_tE93086BAC9B4E1E870D5622412A128F3B7897E48 
{
public:
	// System.Byte easyar.Detail_OptionalOfFrameFilterResult::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail_OptionalOfFrameFilterResult::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFrameFilterResult_tE93086BAC9B4E1E870D5622412A128F3B7897E48, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFrameFilterResult_tE93086BAC9B4E1E870D5622412A128F3B7897E48, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// easyar.Detail_OptionalOfImage
struct  OptionalOfImage_t08C1B90679EB07254FAFD2EDA7C829E9C4A6A333 
{
public:
	// System.Byte easyar.Detail_OptionalOfImage::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail_OptionalOfImage::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfImage_t08C1B90679EB07254FAFD2EDA7C829E9C4A6A333, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfImage_t08C1B90679EB07254FAFD2EDA7C829E9C4A6A333, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// easyar.Detail_OptionalOfImageTarget
struct  OptionalOfImageTarget_tBA006FD8DA304929D12DC3538F60D828DABA8605 
{
public:
	// System.Byte easyar.Detail_OptionalOfImageTarget::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail_OptionalOfImageTarget::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfImageTarget_tBA006FD8DA304929D12DC3538F60D828DABA8605, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfImageTarget_tBA006FD8DA304929D12DC3538F60D828DABA8605, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// easyar.Detail_OptionalOfMatrix44F
struct  OptionalOfMatrix44F_tA3118CA79EFA5E4D61AD496CEE1EBA8E516C6F98 
{
public:
	// System.Byte easyar.Detail_OptionalOfMatrix44F::has_value_
	uint8_t ___has_value__0;
	// easyar.Matrix44F easyar.Detail_OptionalOfMatrix44F::value
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfMatrix44F_tA3118CA79EFA5E4D61AD496CEE1EBA8E516C6F98, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfMatrix44F_tA3118CA79EFA5E4D61AD496CEE1EBA8E516C6F98, ___value_1)); }
	inline Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3  get_value_1() const { return ___value_1; }
	inline Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3  value)
	{
		___value_1 = value;
	}
};


// easyar.Detail_OptionalOfObjectTarget
struct  OptionalOfObjectTarget_t9512684692C22C2C112F935A4665D3FE2EA4CC31 
{
public:
	// System.Byte easyar.Detail_OptionalOfObjectTarget::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail_OptionalOfObjectTarget::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfObjectTarget_t9512684692C22C2C112F935A4665D3FE2EA4CC31, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfObjectTarget_t9512684692C22C2C112F935A4665D3FE2EA4CC31, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// easyar.Detail_OptionalOfOutputFrame
struct  OptionalOfOutputFrame_t5A145618F1BC39E293F85696E8B22B086A84691C 
{
public:
	// System.Byte easyar.Detail_OptionalOfOutputFrame::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail_OptionalOfOutputFrame::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfOutputFrame_t5A145618F1BC39E293F85696E8B22B086A84691C, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfOutputFrame_t5A145618F1BC39E293F85696E8B22B086A84691C, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// easyar.Detail_OptionalOfString
struct  OptionalOfString_t31CB25740B77B6965DCBF8D0175D3DE3DF478729 
{
public:
	// System.Byte easyar.Detail_OptionalOfString::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail_OptionalOfString::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfString_t31CB25740B77B6965DCBF8D0175D3DE3DF478729, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfString_t31CB25740B77B6965DCBF8D0175D3DE3DF478729, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// easyar.Detail_OptionalOfTarget
struct  OptionalOfTarget_t030A332DA69FE1D407C6BEF2D195CCF15DA6EA87 
{
public:
	// System.Byte easyar.Detail_OptionalOfTarget::has_value_
	uint8_t ___has_value__0;
	// System.IntPtr easyar.Detail_OptionalOfTarget::value
	intptr_t ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfTarget_t030A332DA69FE1D407C6BEF2D195CCF15DA6EA87, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfTarget_t030A332DA69FE1D407C6BEF2D195CCF15DA6EA87, ___value_1)); }
	inline intptr_t get_value_1() const { return ___value_1; }
	inline intptr_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(intptr_t value)
	{
		___value_1 = value;
	}
};


// easyar.ImageTrackerMode
struct  ImageTrackerMode_tAF8FC364239DA46FFECA8E36E8C2D3509778643F 
{
public:
	// System.Int32 easyar.ImageTrackerMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageTrackerMode_tAF8FC364239DA46FFECA8E36E8C2D3509778643F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.LocalizationMode
struct  LocalizationMode_tD9BE1930DA3D38E458C38D1A12B06D9644659404 
{
public:
	// System.Int32 easyar.LocalizationMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LocalizationMode_tD9BE1930DA3D38E458C38D1A12B06D9644659404, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.LogLevel
struct  LogLevel_tE02C77CE6DEB9782E61C3688A9F519AFF1D73CCB 
{
public:
	// System.Int32 easyar.LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_tE02C77CE6DEB9782E61C3688A9F519AFF1D73CCB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.MotionTrackingStatus
struct  MotionTrackingStatus_tE6223E9EFFC7741D73222659A410636F441DD7EF 
{
public:
	// System.Int32 easyar.MotionTrackingStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MotionTrackingStatus_tE6223E9EFFC7741D73222659A410636F441DD7EF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.OptionalTag
struct  OptionalTag_tC75A06EE0C7E07F270104C96382A8A4993735221 
{
public:
	// System.Int32 easyar.OptionalTag::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OptionalTag_tC75A06EE0C7E07F270104C96382A8A4993735221, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.PathType
struct  PathType_t98BC7F775F03312503BA714DBAC392FCADBB5E76 
{
public:
	// System.Int32 easyar.PathType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PathType_t98BC7F775F03312503BA714DBAC392FCADBB5E76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.PermissionStatus
struct  PermissionStatus_t41F2835584F927E7DBA46FBDFA60ECA791C96C14 
{
public:
	// System.Int32 easyar.PermissionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PermissionStatus_t41F2835584F927E7DBA46FBDFA60ECA791C96C14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.PixelFormat
struct  PixelFormat_tF3F6BCCBBE484459F419E277DBA4192B590B154F 
{
public:
	// System.Int32 easyar.PixelFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PixelFormat_tF3F6BCCBBE484459F419E277DBA4192B590B154F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.PlaneType
struct  PlaneType_t92905B0D7C8F6EE9E4FDE1FA2969767774A2DA9B 
{
public:
	// System.Int32 easyar.PlaneType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlaneType_t92905B0D7C8F6EE9E4FDE1FA2969767774A2DA9B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.RecordProfile
struct  RecordProfile_tDEF5A1B1ECB5934FD436C465E3098A9F02E1E734 
{
public:
	// System.Int32 easyar.RecordProfile::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecordProfile_tDEF5A1B1ECB5934FD436C465E3098A9F02E1E734, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.RecordStatus
struct  RecordStatus_tA225F8E91E6E99FB79D371E8A79891D95C8EC045 
{
public:
	// System.Int32 easyar.RecordStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecordStatus_tA225F8E91E6E99FB79D371E8A79891D95C8EC045, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.RecordVideoOrientation
struct  RecordVideoOrientation_t3AA19F344CD1EF1E3432FE2A2C2B5E4E470A332E 
{
public:
	// System.Int32 easyar.RecordVideoOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecordVideoOrientation_t3AA19F344CD1EF1E3432FE2A2C2B5E4E470A332E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.RecordVideoSize
struct  RecordVideoSize_tB80311BD33A6FF13197E622C9F067E3168D984D1 
{
public:
	// System.Int32 easyar.RecordVideoSize::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecordVideoSize_tB80311BD33A6FF13197E622C9F067E3168D984D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.RecordZoomMode
struct  RecordZoomMode_tD68D8E1C498F9AFE4B217A09F004A27254121A0A 
{
public:
	// System.Int32 easyar.RecordZoomMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RecordZoomMode_tD68D8E1C498F9AFE4B217A09F004A27254121A0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.RefBase
struct  RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66  : public RuntimeObject
{
public:
	// System.IntPtr easyar.RefBase::cdata_
	intptr_t ___cdata__0;
	// System.Action`1<System.IntPtr> easyar.RefBase::deleter_
	Action_1_t34F8CB5772B9DDE7B1E197366ED54E43CD61052B * ___deleter__1;
	// easyar.RefBase_Retainer easyar.RefBase::retainer_
	Retainer_tF3060567DC74FEC906B36A0E3352EF3955BC7EFC * ___retainer__2;

public:
	inline static int32_t get_offset_of_cdata__0() { return static_cast<int32_t>(offsetof(RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66, ___cdata__0)); }
	inline intptr_t get_cdata__0() const { return ___cdata__0; }
	inline intptr_t* get_address_of_cdata__0() { return &___cdata__0; }
	inline void set_cdata__0(intptr_t value)
	{
		___cdata__0 = value;
	}

	inline static int32_t get_offset_of_deleter__1() { return static_cast<int32_t>(offsetof(RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66, ___deleter__1)); }
	inline Action_1_t34F8CB5772B9DDE7B1E197366ED54E43CD61052B * get_deleter__1() const { return ___deleter__1; }
	inline Action_1_t34F8CB5772B9DDE7B1E197366ED54E43CD61052B ** get_address_of_deleter__1() { return &___deleter__1; }
	inline void set_deleter__1(Action_1_t34F8CB5772B9DDE7B1E197366ED54E43CD61052B * value)
	{
		___deleter__1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deleter__1), (void*)value);
	}

	inline static int32_t get_offset_of_retainer__2() { return static_cast<int32_t>(offsetof(RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66, ___retainer__2)); }
	inline Retainer_tF3060567DC74FEC906B36A0E3352EF3955BC7EFC * get_retainer__2() const { return ___retainer__2; }
	inline Retainer_tF3060567DC74FEC906B36A0E3352EF3955BC7EFC ** get_address_of_retainer__2() { return &___retainer__2; }
	inline void set_retainer__2(Retainer_tF3060567DC74FEC906B36A0E3352EF3955BC7EFC * value)
	{
		___retainer__2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___retainer__2), (void*)value);
	}
};


// easyar.StorageType
struct  StorageType_t6281BBD9AC7F99C0FDB4923177333905BB165C7A 
{
public:
	// System.Int32 easyar.StorageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StorageType_t6281BBD9AC7F99C0FDB4923177333905BB165C7A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.TargetController_ActiveControlStrategy
struct  ActiveControlStrategy_t665C6B68999C0414E97FB445E1F20BE85A40925D 
{
public:
	// System.Int32 easyar.TargetController_ActiveControlStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActiveControlStrategy_t665C6B68999C0414E97FB445E1F20BE85A40925D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.TargetStatus
struct  TargetStatus_t4AA93D00312DE2F6165AFA4222026D5D4DEE95B3 
{
public:
	// System.Int32 easyar.TargetStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TargetStatus_t4AA93D00312DE2F6165AFA4222026D5D4DEE95B3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VIOCameraDeviceUnion_DeviceChooseStrategy
struct  DeviceChooseStrategy_tB71112240D7E8F7D672F2D2158348A00AE12110F 
{
public:
	// System.Int32 easyar.VIOCameraDeviceUnion_DeviceChooseStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceChooseStrategy_tB71112240D7E8F7D672F2D2158348A00AE12110F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VIOCameraDeviceUnion_DeviceUnion_VIODeviceType
struct  VIODeviceType_t913BBD2CF782BFEF26A06A541475B70B2CE762BD 
{
public:
	// System.Int32 easyar.VIOCameraDeviceUnion_DeviceUnion_VIODeviceType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VIODeviceType_t913BBD2CF782BFEF26A06A541475B70B2CE762BD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VideoCameraDevice_CameraDeviceOpenMethod
struct  CameraDeviceOpenMethod_t13B30F6F8BAB0FFB98006C70ED38326D6793DC00 
{
public:
	// System.Int32 easyar.VideoCameraDevice_CameraDeviceOpenMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraDeviceOpenMethod_t13B30F6F8BAB0FFB98006C70ED38326D6793DC00, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VideoRecorder_OutputPathType
struct  OutputPathType_t53F220438AA485F062E36ADF872CFF34522826A7 
{
public:
	// System.Int32 easyar.VideoRecorder_OutputPathType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OutputPathType_t53F220438AA485F062E36ADF872CFF34522826A7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VideoRecorder_VideoOrientation
struct  VideoOrientation_tA6C7B5BB5C30F24C5DDED02FABF3ABA76E6DD4EC 
{
public:
	// System.Int32 easyar.VideoRecorder_VideoOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoOrientation_tA6C7B5BB5C30F24C5DDED02FABF3ABA76E6DD4EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VideoStatus
struct  VideoStatus_t7B2C801858421B797E970E8C44F1A8FDBD7F9483 
{
public:
	// System.Int32 easyar.VideoStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoStatus_t7B2C801858421B797E970E8C44F1A8FDBD7F9483, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.VideoType
struct  VideoType_tBC431039996B85B37628AEBCA6892F551A0C5C84 
{
public:
	// System.Int32 easyar.VideoType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoType_tBC431039996B85B37628AEBCA6892F551A0C5C84, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// easyar.WorldRootController_ActiveControlStrategy
struct  ActiveControlStrategy_t3ED1A6012B248799DF552B4F32BA8CA5BC6F4927 
{
public:
	// System.Int32 easyar.WorldRootController_ActiveControlStrategy::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActiveControlStrategy_t3ED1A6012B248799DF552B4F32BA8CA5BC6F4927, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// easyar.ARCoreCameraDevice
struct  ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.ARKitCameraDevice
struct  ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.Buffer
struct  Buffer_t355D8418B778B9A4BA46CA414A3BBE006E6A78FB  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.BufferDictionary
struct  BufferDictionary_t9A04540518B992E0FE8E1B4F8E037A24DC3A1035  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.BufferPool
struct  BufferPool_tD3515A4BE8658AC4321D4D3131DE2FFA6F36D89A  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.CallbackScheduler
struct  CallbackScheduler_t3BE641305910EA95C368FA67D1A1294797B0AFB7  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.CameraDevice
struct  CameraDevice_t93B3250A54F0E0FE23EB922E2AF6D94942F84470  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.CameraParameters
struct  CameraParameters_t6C1E79E04BA6188556FE8F1E2B46360A8C3B2BD5  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.CloudRecognizationResult
struct  CloudRecognizationResult_t6494AC886F8FA4C74D70EA0CCA6BAB766525C4FB  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.CloudRecognizer
struct  CloudRecognizer_tB0655B166281C61CA885069393FCE697C226D5E1  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.DenseSpatialMap
struct  DenseSpatialMap_t66BF4FE24613E31CA467486A348E3DE5666B9C6D  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.Detail_OptionalOfFunctorOfVoid
struct  OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoid::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoid easyar.Detail_OptionalOfFunctorOfVoid::value
	FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39, ___value_1)); }
	inline FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3  get_value_1() const { return ___value_1; }
	inline FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoid
struct OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoid
struct OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3_marshaled_com ___value_1;
};

// easyar.Detail_OptionalOfFunctorOfVoidFromBool
struct  OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoidFromBool::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoidFromBool easyar.Detail_OptionalOfFunctorOfVoidFromBool::value
	FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5, ___value_1)); }
	inline FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6  get_value_1() const { return ___value_1; }
	inline FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromBool
struct OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromBool
struct OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6_marshaled_com ___value_1;
};

// easyar.Detail_OptionalOfFunctorOfVoidFromCameraState
struct  OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoidFromCameraState::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoidFromCameraState easyar.Detail_OptionalOfFunctorOfVoidFromCameraState::value
	FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085, ___value_1)); }
	inline FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5  get_value_1() const { return ___value_1; }
	inline FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromCameraState
struct OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromCameraState
struct OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5_marshaled_com ___value_1;
};

// easyar.Detail_OptionalOfFunctorOfVoidFromFeedbackFrame
struct  OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoidFromFeedbackFrame::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoidFromFeedbackFrame easyar.Detail_OptionalOfFunctorOfVoidFromFeedbackFrame::value
	FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F, ___value_1)); }
	inline FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E  get_value_1() const { return ___value_1; }
	inline FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromFeedbackFrame
struct OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromFeedbackFrame
struct OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E_marshaled_com ___value_1;
};

// easyar.Detail_OptionalOfFunctorOfVoidFromInputFrame
struct  OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoidFromInputFrame::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoidFromInputFrame easyar.Detail_OptionalOfFunctorOfVoidFromInputFrame::value
	FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C, ___value_1)); }
	inline FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81  get_value_1() const { return ___value_1; }
	inline FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromInputFrame
struct OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromInputFrame
struct OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81_marshaled_com ___value_1;
};

// easyar.Detail_OptionalOfFunctorOfVoidFromOutputFrame
struct  OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoidFromOutputFrame::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoidFromOutputFrame easyar.Detail_OptionalOfFunctorOfVoidFromOutputFrame::value
	FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857, ___value_1)); }
	inline FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34  get_value_1() const { return ___value_1; }
	inline FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromOutputFrame
struct OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromOutputFrame
struct OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34_marshaled_com ___value_1;
};

// easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString
struct  OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoidFromPermissionStatusAndString easyar.Detail_OptionalOfFunctorOfVoidFromPermissionStatusAndString::value
	FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9, ___value_1)); }
	inline FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1  get_value_1() const { return ___value_1; }
	inline FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString
struct OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromPermissionStatusAndString
struct OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1_marshaled_com ___value_1;
};

// easyar.Detail_OptionalOfFunctorOfVoidFromRecordStatusAndString
struct  OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoidFromRecordStatusAndString::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoidFromRecordStatusAndString easyar.Detail_OptionalOfFunctorOfVoidFromRecordStatusAndString::value
	FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3, ___value_1)); }
	inline FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F  get_value_1() const { return ___value_1; }
	inline FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromRecordStatusAndString
struct OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromRecordStatusAndString
struct OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F_marshaled_com ___value_1;
};

// easyar.Detail_OptionalOfFunctorOfVoidFromVideoStatus
struct  OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B 
{
public:
	// System.Byte easyar.Detail_OptionalOfFunctorOfVoidFromVideoStatus::has_value_
	uint8_t ___has_value__0;
	// easyar.Detail_FunctorOfVoidFromVideoStatus easyar.Detail_OptionalOfFunctorOfVoidFromVideoStatus::value
	FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863  ___value_1;

public:
	inline static int32_t get_offset_of_has_value__0() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B, ___has_value__0)); }
	inline uint8_t get_has_value__0() const { return ___has_value__0; }
	inline uint8_t* get_address_of_has_value__0() { return &___has_value__0; }
	inline void set_has_value__0(uint8_t value)
	{
		___has_value__0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B, ___value_1)); }
	inline FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863  get_value_1() const { return ___value_1; }
	inline FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____func_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->____destroy_2), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromVideoStatus
struct OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B_marshaled_pinvoke
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863_marshaled_pinvoke ___value_1;
};
// Native definition for COM marshalling of easyar.Detail/OptionalOfFunctorOfVoidFromVideoStatus
struct OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B_marshaled_com
{
	uint8_t ___has_value__0;
	FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863_marshaled_com ___value_1;
};

// easyar.FeedbackFrame
struct  FeedbackFrame_tEB60455EDD504A079A1780150E58F46F9EA0DB8E  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.FeedbackFrameFork
struct  FeedbackFrameFork_tF27FD489114CA7D66F4EBC1074D67A46632DCAFC  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.FeedbackFrameSink
struct  FeedbackFrameSink_tBFC092DAD2B19C13314BDF1C0D012D018713C78A  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.FeedbackFrameSource
struct  FeedbackFrameSource_t15AC3F3BD55A3EC102E1A7016A53AAD75932845E  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.FileUtil_<LoadFile>d__1
struct  U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC  : public RuntimeObject
{
public:
	// System.Int32 easyar.FileUtil_<LoadFile>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object easyar.FileUtil_<LoadFile>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action`1<System.Byte[]> easyar.FileUtil_<LoadFile>d__1::onLoad
	Action_1_t98224925EBFF210B057632BF4539B3B7AED96DC7 * ___onLoad_2;
	// System.String easyar.FileUtil_<LoadFile>d__1::filePath
	String_t* ___filePath_3;
	// easyar.PathType easyar.FileUtil_<LoadFile>d__1::filePathType
	int32_t ___filePathType_4;
	// UnityEngine.Networking.DownloadHandlerBuffer easyar.FileUtil_<LoadFile>d__1::<handle>5__2
	DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255 * ___U3ChandleU3E5__2_5;
	// UnityEngine.Networking.UnityWebRequest easyar.FileUtil_<LoadFile>d__1::<webRequest>5__3
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwebRequestU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_onLoad_2() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC, ___onLoad_2)); }
	inline Action_1_t98224925EBFF210B057632BF4539B3B7AED96DC7 * get_onLoad_2() const { return ___onLoad_2; }
	inline Action_1_t98224925EBFF210B057632BF4539B3B7AED96DC7 ** get_address_of_onLoad_2() { return &___onLoad_2; }
	inline void set_onLoad_2(Action_1_t98224925EBFF210B057632BF4539B3B7AED96DC7 * value)
	{
		___onLoad_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onLoad_2), (void*)value);
	}

	inline static int32_t get_offset_of_filePath_3() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC, ___filePath_3)); }
	inline String_t* get_filePath_3() const { return ___filePath_3; }
	inline String_t** get_address_of_filePath_3() { return &___filePath_3; }
	inline void set_filePath_3(String_t* value)
	{
		___filePath_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filePath_3), (void*)value);
	}

	inline static int32_t get_offset_of_filePathType_4() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC, ___filePathType_4)); }
	inline int32_t get_filePathType_4() const { return ___filePathType_4; }
	inline int32_t* get_address_of_filePathType_4() { return &___filePathType_4; }
	inline void set_filePathType_4(int32_t value)
	{
		___filePathType_4 = value;
	}

	inline static int32_t get_offset_of_U3ChandleU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC, ___U3ChandleU3E5__2_5)); }
	inline DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255 * get_U3ChandleU3E5__2_5() const { return ___U3ChandleU3E5__2_5; }
	inline DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255 ** get_address_of_U3ChandleU3E5__2_5() { return &___U3ChandleU3E5__2_5; }
	inline void set_U3ChandleU3E5__2_5(DownloadHandlerBuffer_tF6A73B82C9EC807D36B904A58E1DF2DDA696B255 * value)
	{
		___U3ChandleU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ChandleU3E5__2_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CwebRequestU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC, ___U3CwebRequestU3E5__3_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwebRequestU3E5__3_6() const { return ___U3CwebRequestU3E5__3_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwebRequestU3E5__3_6() { return &___U3CwebRequestU3E5__3_6; }
	inline void set_U3CwebRequestU3E5__3_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwebRequestU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwebRequestU3E5__3_6), (void*)value);
	}
};


// easyar.FrameFilterResult
struct  FrameFilterResult_t91CD8278C8A8BBEA05B38049FB74B40E2794BF4E  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.Image
struct  Image_t18C2A1EA403C254F22B4BEC76C7AFC4DE5919801  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.ImageTargetParameters
struct  ImageTargetParameters_t4534054F85DBAB01DB7E04EE1D70B00B8212D228  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.ImageTracker
struct  ImageTracker_t99D95F9A9C7422328AF4DB69652821114B0C4CB5  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFrame
struct  InputFrame_tCADC80282B08673BD5A63A4B37A662368204000D  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFrameFork
struct  InputFrameFork_t473B6DD8D6A3FB84D4F62D652A3ECFCC63FCE31B  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFramePlayer
struct  InputFramePlayer_t8C251C9015A03B9E7C2E5A19D5E6999ECB17E425  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFrameRecorder
struct  InputFrameRecorder_tE9272358FF6007B21196E4A7476C60965FFD2D94  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFrameSink
struct  InputFrameSink_t97BD9624A3E94450FC4D84DFC4A54035A82DC1D5  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFrameSource
struct  InputFrameSource_t32D6896B7E3B06DB1BD77EEF7C555CC265BE86E4  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFrameThrottler
struct  InputFrameThrottler_tAD8A518B4EA21DB8138CE11B7B31D84E1E2E7B52  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFrameToFeedbackFrameAdapter
struct  InputFrameToFeedbackFrameAdapter_tDDA77B26C793084B51844CA454EA21D26BAEC46E  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.InputFrameToOutputFrameAdapter
struct  InputFrameToOutputFrameAdapter_t074262D2812305DB57DACF4CD79FCD0BFF3CA730  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.MotionTrackerCameraDevice
struct  MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.ObjectTargetParameters
struct  ObjectTargetParameters_tDF1A158B07BFE444EFC6D003EDEBCAA0C3ECF48B  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.ObjectTracker
struct  ObjectTracker_t2DFF1FE335FC67408A37DE87779506B214562E75  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.OutputFrame
struct  OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.OutputFrameBuffer
struct  OutputFrameBuffer_tC7D6ED8C5B46AF57417402D3B6255D0645E8FB62  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.OutputFrameFork
struct  OutputFrameFork_t61E24817457114FC26180D60615CA33816015C1C  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.OutputFrameJoin
struct  OutputFrameJoin_t35A95CCFB85BA549CE10B84F8D4A21D15EC6DA26  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.OutputFrameSink
struct  OutputFrameSink_t92453CD78A4BE2375DFAFB67324318C2C5328C64  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.OutputFrameSource
struct  OutputFrameSource_t3B940D541DE63501E2E0F6CD429EFEABC072AEB2  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.PlaneData
struct  PlaneData_tA546F8CB5D20873DC3D3C0FD041B7E8CF6986EAA  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.Recorder
struct  Recorder_t8323228F81AFAA312EF2F050D621026784AA7A75  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.RecorderConfiguration
struct  RecorderConfiguration_t0C9489C7306B4BDF93CBE2E3D56A703AED82F993  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.SceneMesh
struct  SceneMesh_tD675CCC9AF83A095BBC0CB4F0C926C88DDBB3D25  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.SignalSink
struct  SignalSink_t6960E52E8BE74841137BAC36FCFDA5871BAFE88E  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.SignalSource
struct  SignalSource_t5D9E6D365066BC68C2328194CCA3FB46B8A1C1FC  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.SparseSpatialMap
struct  SparseSpatialMap_tD3A97C244427D0B13CE1B70D917300F499E9F570  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.SparseSpatialMapConfig
struct  SparseSpatialMapConfig_tA745F650FA8B470E606C0918FB31384B7A024FC8  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.SparseSpatialMapManager
struct  SparseSpatialMapManager_t3A7F5C67A6E607A0E1A65DBEE01249F850FA1FFD  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.SurfaceTracker
struct  SurfaceTracker_t5700DA0E1842A81A3E4FD2DBE1887C69B51A2BF0  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.Target
struct  Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.TargetInstance
struct  TargetInstance_tE263CFB69CC2EFEEA6C92461B7FE4EBF1E5A1FD8  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.TextureId
struct  TextureId_tFFD7E9D4242A80AF5149CE6FC5CE60FD752977DB  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// easyar.UIPopupException
struct  UIPopupException_t2827F66E5D366BC1DD2B51961B2031DDD08557C4  : public Exception_t
{
public:

public:
};


// easyar.VIOCameraDeviceUnion_DeviceUnion
struct  DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F  : public RuntimeObject
{
public:
	// easyar.MotionTrackerCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::motionTrackerCameraDevice
	MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69 * ___motionTrackerCameraDevice_0;
	// easyar.ARKitCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::arKitCameraDevice
	ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239 * ___arKitCameraDevice_1;
	// easyar.ARCoreCameraDevice easyar.VIOCameraDeviceUnion_DeviceUnion::arCoreCameraDevice
	ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49 * ___arCoreCameraDevice_2;
	// easyar.VIOCameraDeviceUnion_DeviceUnion_VIODeviceType easyar.VIOCameraDeviceUnion_DeviceUnion::<DeviceType>k__BackingField
	int32_t ___U3CDeviceTypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_motionTrackerCameraDevice_0() { return static_cast<int32_t>(offsetof(DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F, ___motionTrackerCameraDevice_0)); }
	inline MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69 * get_motionTrackerCameraDevice_0() const { return ___motionTrackerCameraDevice_0; }
	inline MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69 ** get_address_of_motionTrackerCameraDevice_0() { return &___motionTrackerCameraDevice_0; }
	inline void set_motionTrackerCameraDevice_0(MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69 * value)
	{
		___motionTrackerCameraDevice_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___motionTrackerCameraDevice_0), (void*)value);
	}

	inline static int32_t get_offset_of_arKitCameraDevice_1() { return static_cast<int32_t>(offsetof(DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F, ___arKitCameraDevice_1)); }
	inline ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239 * get_arKitCameraDevice_1() const { return ___arKitCameraDevice_1; }
	inline ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239 ** get_address_of_arKitCameraDevice_1() { return &___arKitCameraDevice_1; }
	inline void set_arKitCameraDevice_1(ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239 * value)
	{
		___arKitCameraDevice_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arKitCameraDevice_1), (void*)value);
	}

	inline static int32_t get_offset_of_arCoreCameraDevice_2() { return static_cast<int32_t>(offsetof(DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F, ___arCoreCameraDevice_2)); }
	inline ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49 * get_arCoreCameraDevice_2() const { return ___arCoreCameraDevice_2; }
	inline ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49 ** get_address_of_arCoreCameraDevice_2() { return &___arCoreCameraDevice_2; }
	inline void set_arCoreCameraDevice_2(ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49 * value)
	{
		___arCoreCameraDevice_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arCoreCameraDevice_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDeviceTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F, ___U3CDeviceTypeU3Ek__BackingField_3)); }
	inline int32_t get_U3CDeviceTypeU3Ek__BackingField_3() const { return ___U3CDeviceTypeU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDeviceTypeU3Ek__BackingField_3() { return &___U3CDeviceTypeU3Ek__BackingField_3; }
	inline void set_U3CDeviceTypeU3Ek__BackingField_3(int32_t value)
	{
		___U3CDeviceTypeU3Ek__BackingField_3 = value;
	}
};


// easyar.VideoPlayer
struct  VideoPlayer_t8CFFC18AFC95F38EC854C3DCAE7FED6F89422C2C  : public RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// easyar.DelayedCallbackScheduler
struct  DelayedCallbackScheduler_tFDA191E2FFD94E9A3AA26F25C560D8F95EA26D2A  : public CallbackScheduler_t3BE641305910EA95C368FA67D1A1294797B0AFB7
{
public:

public:
};


// easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_DestroyDelegate
struct  DestroyDelegate_t763D40EFF45F320D973DBBDBC2091903407DE9B4  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfOutputFrameFromListOfOutputFrame_FunctionDelegate
struct  FunctionDelegate_t090C1C63C0751FF32A4D0F8A7C02814712221080  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoid_DestroyDelegate
struct  DestroyDelegate_t3AA1949EC75C7221AB90BB3B883229E0CB7C31B2  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoid_FunctionDelegate
struct  FunctionDelegate_t8848D5E47212289B58FA62C1BF4A6181B20EA54F  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromBool_DestroyDelegate
struct  DestroyDelegate_t57CB1B601122C024033C7AD2F46AA139B5E75986  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromBool_FunctionDelegate
struct  FunctionDelegate_t2CFD5626EBF6B782585676A1D9BFD20E0BE31BB4  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromBoolAndString_DestroyDelegate
struct  DestroyDelegate_tFF2989B8C9F4C2E6577E10F17BC39501BEB0737D  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromBoolAndString_FunctionDelegate
struct  FunctionDelegate_tC9991BEBFE57DA2C93AE24E31B226FA0DBBCCBC4  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_DestroyDelegate
struct  DestroyDelegate_tF0DF87394419C98CD7EEFD04AF8127B379FA62C6  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromBoolAndStringAndString_FunctionDelegate
struct  FunctionDelegate_tCA3602C4951133C4B97C064574C5508752B54395  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromCameraState_DestroyDelegate
struct  DestroyDelegate_t758A0260C125658E88D189A318456AD9E62ECF32  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromCameraState_FunctionDelegate
struct  FunctionDelegate_tAE9F5819A2ED199C1C375161C8A71F04A69FB01D  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_DestroyDelegate
struct  DestroyDelegate_t1AD3EDFB2E4036786B440B416474CDE85F082E26  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromCloudRecognizationResult_FunctionDelegate
struct  FunctionDelegate_tE7D7F32814FA87DC91820A195D9678EB48FC9F81  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromFeedbackFrame_DestroyDelegate
struct  DestroyDelegate_t35BEB98D61989D586C4B902FB7A2F58C6B3AD351  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromFeedbackFrame_FunctionDelegate
struct  FunctionDelegate_t95EA6B135721C887565B9A7C3A9D979FB575D769  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromInputFrame_DestroyDelegate
struct  DestroyDelegate_tA95D741A4ECD162A9E3EC7D8085B5589D738C740  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromInputFrame_FunctionDelegate
struct  FunctionDelegate_t1E48CF3290061A12253AC07FEC9B5A2C0865FC19  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromLogLevelAndString_DestroyDelegate
struct  DestroyDelegate_t2188B56261B93F5F389858154388FF5F297B0033  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromLogLevelAndString_FunctionDelegate
struct  FunctionDelegate_t0E05B7C0F77F94A09651021DEA1DB8377A70C448  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromOutputFrame_DestroyDelegate
struct  DestroyDelegate_tFFAFFA1AA25A9A49F60E9F7B12BB4B41D8B015E3  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromOutputFrame_FunctionDelegate
struct  FunctionDelegate_tE89DEAA79C244DC519F4FF518D34772907DDBB9B  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_DestroyDelegate
struct  DestroyDelegate_tE504B96EE209B931BDC1BF72BF91B6A87A772209  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromPermissionStatusAndString_FunctionDelegate
struct  FunctionDelegate_tF4D41B79691A226FEA22151FB99FEA4EC1726836  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromRecordStatusAndString_DestroyDelegate
struct  DestroyDelegate_t3C0897F2F93EC30F5D876405102810B918CF7790  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromRecordStatusAndString_FunctionDelegate
struct  FunctionDelegate_tC303954CF881FABFE27597445CFEB8CEDCF13765  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromTargetAndBool_DestroyDelegate
struct  DestroyDelegate_t14935C2A4187F63C334BF87A911FBF20F1F1BFA5  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromTargetAndBool_FunctionDelegate
struct  FunctionDelegate_t990035A4EB0D6A4E74D65EE5C45F3AF438D69119  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromVideoStatus_DestroyDelegate
struct  DestroyDelegate_t46368E6813BD066929E2C8CB5BF24211E30D5EFD  : public MulticastDelegate_t
{
public:

public:
};


// easyar.Detail_FunctorOfVoidFromVideoStatus_FunctionDelegate
struct  FunctionDelegate_tF44FA25582324437CAD80F657DCC3970F650DE99  : public MulticastDelegate_t
{
public:

public:
};


// easyar.ImageTarget
struct  ImageTarget_tF9CE381D1F5DE5B3254CEB22DDC143DB0A47F88F  : public Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46
{
public:

public:
};


// easyar.ImmediateCallbackScheduler
struct  ImmediateCallbackScheduler_tAA6CA29C310DCE4B0ED852A7B972BC97441A851D  : public CallbackScheduler_t3BE641305910EA95C368FA67D1A1294797B0AFB7
{
public:

public:
};


// easyar.ObjectTarget
struct  ObjectTarget_tE33A3926353FAFADD9A6A79E05BCE9261538C00A  : public Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46
{
public:

public:
};


// easyar.RefBase_Retainer
struct  Retainer_tF3060567DC74FEC906B36A0E3352EF3955BC7EFC  : public MulticastDelegate_t
{
public:

public:
};


// easyar.SparseSpatialMapResult
struct  SparseSpatialMapResult_tD81E4F0CFFE0DD6DC9A989BF54880393E512ACD2  : public FrameFilterResult_t91CD8278C8A8BBEA05B38049FB74B40E2794BF4E
{
public:

public:
};


// easyar.SurfaceTrackerResult
struct  SurfaceTrackerResult_t4B5D1BBFB34A48A883238B7FDC28A6C60080B6C9  : public FrameFilterResult_t91CD8278C8A8BBEA05B38049FB74B40E2794BF4E
{
public:

public:
};


// easyar.TargetTrackerResult
struct  TargetTrackerResult_tBCEE9FE8B43EA716AEA6949B53BB38B4D94258F5  : public FrameFilterResult_t91CD8278C8A8BBEA05B38049FB74B40E2794BF4E
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// easyar.ImageTrackerResult
struct  ImageTrackerResult_tBDBCFCA4D25F2204F40B4389264F6EA1ED8C5F61  : public TargetTrackerResult_tBCEE9FE8B43EA716AEA6949B53BB38B4D94258F5
{
public:

public:
};


// easyar.ObjectTrackerResult
struct  ObjectTrackerResult_tE4ACF6058ED5682049ABB069298CE8B695643869  : public TargetTrackerResult_tBCEE9FE8B43EA716AEA6949B53BB38B4D94258F5
{
public:

public:
};


// easyar.FrameFilter
struct  FrameFilter_t17175B54AEDD30B1251CE70EEE33D6393F2DC0EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean easyar.FrameFilter::horizontalFlip
	bool ___horizontalFlip_4;

public:
	inline static int32_t get_offset_of_horizontalFlip_4() { return static_cast<int32_t>(offsetof(FrameFilter_t17175B54AEDD30B1251CE70EEE33D6393F2DC0EB, ___horizontalFlip_4)); }
	inline bool get_horizontalFlip_4() const { return ___horizontalFlip_4; }
	inline bool* get_address_of_horizontalFlip_4() { return &___horizontalFlip_4; }
	inline void set_horizontalFlip_4(bool value)
	{
		___horizontalFlip_4 = value;
	}
};


// easyar.FrameSource
struct  FrameSource_t6AA400B054712A5C45E092546880F5C55C97A110  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// easyar.InputFrameSink easyar.FrameSource::sink
	InputFrameSink_t97BD9624A3E94450FC4D84DFC4A54035A82DC1D5 * ___sink_4;
	// easyar.ARSession easyar.FrameSource::arSession
	ARSession_tB5970654DC42C1495E6D09E2904603951CCEB03C * ___arSession_5;

public:
	inline static int32_t get_offset_of_sink_4() { return static_cast<int32_t>(offsetof(FrameSource_t6AA400B054712A5C45E092546880F5C55C97A110, ___sink_4)); }
	inline InputFrameSink_t97BD9624A3E94450FC4D84DFC4A54035A82DC1D5 * get_sink_4() const { return ___sink_4; }
	inline InputFrameSink_t97BD9624A3E94450FC4D84DFC4A54035A82DC1D5 ** get_address_of_sink_4() { return &___sink_4; }
	inline void set_sink_4(InputFrameSink_t97BD9624A3E94450FC4D84DFC4A54035A82DC1D5 * value)
	{
		___sink_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sink_4), (void*)value);
	}

	inline static int32_t get_offset_of_arSession_5() { return static_cast<int32_t>(offsetof(FrameSource_t6AA400B054712A5C45E092546880F5C55C97A110, ___arSession_5)); }
	inline ARSession_tB5970654DC42C1495E6D09E2904603951CCEB03C * get_arSession_5() const { return ___arSession_5; }
	inline ARSession_tB5970654DC42C1495E6D09E2904603951CCEB03C ** get_address_of_arSession_5() { return &___arSession_5; }
	inline void set_arSession_5(ARSession_tB5970654DC42C1495E6D09E2904603951CCEB03C * value)
	{
		___arSession_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arSession_5), (void*)value);
	}
};


// easyar.GUIPopup
struct  GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Queue`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Single>> easyar.GUIPopup::messageQueue
	Queue_1_tF033B03B6B4B6A87650DC344F9DB27113C444AFF * ___messageQueue_5;
	// System.Boolean easyar.GUIPopup::isShowing
	bool ___isShowing_6;
	// System.Boolean easyar.GUIPopup::isDisappearing
	bool ___isDisappearing_7;
	// UnityEngine.GUISkin easyar.GUIPopup::skin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ___skin_8;

public:
	inline static int32_t get_offset_of_messageQueue_5() { return static_cast<int32_t>(offsetof(GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7, ___messageQueue_5)); }
	inline Queue_1_tF033B03B6B4B6A87650DC344F9DB27113C444AFF * get_messageQueue_5() const { return ___messageQueue_5; }
	inline Queue_1_tF033B03B6B4B6A87650DC344F9DB27113C444AFF ** get_address_of_messageQueue_5() { return &___messageQueue_5; }
	inline void set_messageQueue_5(Queue_1_tF033B03B6B4B6A87650DC344F9DB27113C444AFF * value)
	{
		___messageQueue_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___messageQueue_5), (void*)value);
	}

	inline static int32_t get_offset_of_isShowing_6() { return static_cast<int32_t>(offsetof(GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7, ___isShowing_6)); }
	inline bool get_isShowing_6() const { return ___isShowing_6; }
	inline bool* get_address_of_isShowing_6() { return &___isShowing_6; }
	inline void set_isShowing_6(bool value)
	{
		___isShowing_6 = value;
	}

	inline static int32_t get_offset_of_isDisappearing_7() { return static_cast<int32_t>(offsetof(GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7, ___isDisappearing_7)); }
	inline bool get_isDisappearing_7() const { return ___isDisappearing_7; }
	inline bool* get_address_of_isDisappearing_7() { return &___isDisappearing_7; }
	inline void set_isDisappearing_7(bool value)
	{
		___isDisappearing_7 = value;
	}

	inline static int32_t get_offset_of_skin_8() { return static_cast<int32_t>(offsetof(GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7, ___skin_8)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get_skin_8() const { return ___skin_8; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of_skin_8() { return &___skin_8; }
	inline void set_skin_8(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		___skin_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skin_8), (void*)value);
	}
};

struct GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7_StaticFields
{
public:
	// easyar.GUIPopup easyar.GUIPopup::popup
	GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7 * ___popup_4;

public:
	inline static int32_t get_offset_of_popup_4() { return static_cast<int32_t>(offsetof(GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7_StaticFields, ___popup_4)); }
	inline GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7 * get_popup_4() const { return ___popup_4; }
	inline GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7 ** get_address_of_popup_4() { return &___popup_4; }
	inline void set_popup_4(GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7 * value)
	{
		___popup_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___popup_4), (void*)value);
	}
};


// easyar.TargetController
struct  TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// easyar.TargetController_ActiveControlStrategy easyar.TargetController::ActiveControl
	int32_t ___ActiveControl_4;
	// System.Boolean easyar.TargetController::HorizontalFlip
	bool ___HorizontalFlip_5;
	// System.Boolean easyar.TargetController::firstFound
	bool ___firstFound_6;
	// System.Action easyar.TargetController::TargetFound
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___TargetFound_7;
	// System.Action easyar.TargetController::TargetLost
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___TargetLost_8;
	// System.Boolean easyar.TargetController::<IsTracked>k__BackingField
	bool ___U3CIsTrackedU3Ek__BackingField_9;
	// System.Boolean easyar.TargetController::<IsLoaded>k__BackingField
	bool ___U3CIsLoadedU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_ActiveControl_4() { return static_cast<int32_t>(offsetof(TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798, ___ActiveControl_4)); }
	inline int32_t get_ActiveControl_4() const { return ___ActiveControl_4; }
	inline int32_t* get_address_of_ActiveControl_4() { return &___ActiveControl_4; }
	inline void set_ActiveControl_4(int32_t value)
	{
		___ActiveControl_4 = value;
	}

	inline static int32_t get_offset_of_HorizontalFlip_5() { return static_cast<int32_t>(offsetof(TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798, ___HorizontalFlip_5)); }
	inline bool get_HorizontalFlip_5() const { return ___HorizontalFlip_5; }
	inline bool* get_address_of_HorizontalFlip_5() { return &___HorizontalFlip_5; }
	inline void set_HorizontalFlip_5(bool value)
	{
		___HorizontalFlip_5 = value;
	}

	inline static int32_t get_offset_of_firstFound_6() { return static_cast<int32_t>(offsetof(TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798, ___firstFound_6)); }
	inline bool get_firstFound_6() const { return ___firstFound_6; }
	inline bool* get_address_of_firstFound_6() { return &___firstFound_6; }
	inline void set_firstFound_6(bool value)
	{
		___firstFound_6 = value;
	}

	inline static int32_t get_offset_of_TargetFound_7() { return static_cast<int32_t>(offsetof(TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798, ___TargetFound_7)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_TargetFound_7() const { return ___TargetFound_7; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_TargetFound_7() { return &___TargetFound_7; }
	inline void set_TargetFound_7(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___TargetFound_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetFound_7), (void*)value);
	}

	inline static int32_t get_offset_of_TargetLost_8() { return static_cast<int32_t>(offsetof(TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798, ___TargetLost_8)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_TargetLost_8() const { return ___TargetLost_8; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_TargetLost_8() { return &___TargetLost_8; }
	inline void set_TargetLost_8(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___TargetLost_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetLost_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsTrackedU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798, ___U3CIsTrackedU3Ek__BackingField_9)); }
	inline bool get_U3CIsTrackedU3Ek__BackingField_9() const { return ___U3CIsTrackedU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsTrackedU3Ek__BackingField_9() { return &___U3CIsTrackedU3Ek__BackingField_9; }
	inline void set_U3CIsTrackedU3Ek__BackingField_9(bool value)
	{
		___U3CIsTrackedU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CIsLoadedU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798, ___U3CIsLoadedU3Ek__BackingField_10)); }
	inline bool get_U3CIsLoadedU3Ek__BackingField_10() const { return ___U3CIsLoadedU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CIsLoadedU3Ek__BackingField_10() { return &___U3CIsLoadedU3Ek__BackingField_10; }
	inline void set_U3CIsLoadedU3Ek__BackingField_10(bool value)
	{
		___U3CIsLoadedU3Ek__BackingField_10 = value;
	}
};


// easyar.VideoRecorder
struct  VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// easyar.RecordProfile easyar.VideoRecorder::Profile
	int32_t ___Profile_4;
	// easyar.VideoRecorder_VideoOrientation easyar.VideoRecorder::Orientation
	int32_t ___Orientation_5;
	// easyar.RecordZoomMode easyar.VideoRecorder::RecordZoomMode
	int32_t ___RecordZoomMode_6;
	// easyar.VideoRecorder_OutputPathType easyar.VideoRecorder::FilePathType
	int32_t ___FilePathType_7;
	// System.String easyar.VideoRecorder::FilePath
	String_t* ___FilePath_8;
	// easyar.Recorder easyar.VideoRecorder::recorder
	Recorder_t8323228F81AFAA312EF2F050D621026784AA7A75 * ___recorder_9;
	// System.Action`2<easyar.RecordStatus,System.String> easyar.VideoRecorder::StatusUpdate
	Action_2_t637B3D6E59757721381E91C325A4FF48667ED83B * ___StatusUpdate_10;
	// System.Boolean easyar.VideoRecorder::<IsReady>k__BackingField
	bool ___U3CIsReadyU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_Profile_4() { return static_cast<int32_t>(offsetof(VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443, ___Profile_4)); }
	inline int32_t get_Profile_4() const { return ___Profile_4; }
	inline int32_t* get_address_of_Profile_4() { return &___Profile_4; }
	inline void set_Profile_4(int32_t value)
	{
		___Profile_4 = value;
	}

	inline static int32_t get_offset_of_Orientation_5() { return static_cast<int32_t>(offsetof(VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443, ___Orientation_5)); }
	inline int32_t get_Orientation_5() const { return ___Orientation_5; }
	inline int32_t* get_address_of_Orientation_5() { return &___Orientation_5; }
	inline void set_Orientation_5(int32_t value)
	{
		___Orientation_5 = value;
	}

	inline static int32_t get_offset_of_RecordZoomMode_6() { return static_cast<int32_t>(offsetof(VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443, ___RecordZoomMode_6)); }
	inline int32_t get_RecordZoomMode_6() const { return ___RecordZoomMode_6; }
	inline int32_t* get_address_of_RecordZoomMode_6() { return &___RecordZoomMode_6; }
	inline void set_RecordZoomMode_6(int32_t value)
	{
		___RecordZoomMode_6 = value;
	}

	inline static int32_t get_offset_of_FilePathType_7() { return static_cast<int32_t>(offsetof(VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443, ___FilePathType_7)); }
	inline int32_t get_FilePathType_7() const { return ___FilePathType_7; }
	inline int32_t* get_address_of_FilePathType_7() { return &___FilePathType_7; }
	inline void set_FilePathType_7(int32_t value)
	{
		___FilePathType_7 = value;
	}

	inline static int32_t get_offset_of_FilePath_8() { return static_cast<int32_t>(offsetof(VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443, ___FilePath_8)); }
	inline String_t* get_FilePath_8() const { return ___FilePath_8; }
	inline String_t** get_address_of_FilePath_8() { return &___FilePath_8; }
	inline void set_FilePath_8(String_t* value)
	{
		___FilePath_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilePath_8), (void*)value);
	}

	inline static int32_t get_offset_of_recorder_9() { return static_cast<int32_t>(offsetof(VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443, ___recorder_9)); }
	inline Recorder_t8323228F81AFAA312EF2F050D621026784AA7A75 * get_recorder_9() const { return ___recorder_9; }
	inline Recorder_t8323228F81AFAA312EF2F050D621026784AA7A75 ** get_address_of_recorder_9() { return &___recorder_9; }
	inline void set_recorder_9(Recorder_t8323228F81AFAA312EF2F050D621026784AA7A75 * value)
	{
		___recorder_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___recorder_9), (void*)value);
	}

	inline static int32_t get_offset_of_StatusUpdate_10() { return static_cast<int32_t>(offsetof(VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443, ___StatusUpdate_10)); }
	inline Action_2_t637B3D6E59757721381E91C325A4FF48667ED83B * get_StatusUpdate_10() const { return ___StatusUpdate_10; }
	inline Action_2_t637B3D6E59757721381E91C325A4FF48667ED83B ** get_address_of_StatusUpdate_10() { return &___StatusUpdate_10; }
	inline void set_StatusUpdate_10(Action_2_t637B3D6E59757721381E91C325A4FF48667ED83B * value)
	{
		___StatusUpdate_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StatusUpdate_10), (void*)value);
	}

	inline static int32_t get_offset_of_U3CIsReadyU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443, ___U3CIsReadyU3Ek__BackingField_11)); }
	inline bool get_U3CIsReadyU3Ek__BackingField_11() const { return ___U3CIsReadyU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsReadyU3Ek__BackingField_11() { return &___U3CIsReadyU3Ek__BackingField_11; }
	inline void set_U3CIsReadyU3Ek__BackingField_11(bool value)
	{
		___U3CIsReadyU3Ek__BackingField_11 = value;
	}
};


// easyar.WorldRootController
struct  WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// easyar.WorldRootController_ActiveControlStrategy easyar.WorldRootController::ActiveControl
	int32_t ___ActiveControl_4;
	// System.Boolean easyar.WorldRootController::trackingStarted
	bool ___trackingStarted_5;
	// System.Action`1<easyar.MotionTrackingStatus> easyar.WorldRootController::TrackingStatusChanged
	Action_1_tBCED0B9AD0B042D7EA7FC7C908253547CF7D9FB6 * ___TrackingStatusChanged_6;
	// easyar.MotionTrackingStatus easyar.WorldRootController::<TrackingStatus>k__BackingField
	int32_t ___U3CTrackingStatusU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_ActiveControl_4() { return static_cast<int32_t>(offsetof(WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B, ___ActiveControl_4)); }
	inline int32_t get_ActiveControl_4() const { return ___ActiveControl_4; }
	inline int32_t* get_address_of_ActiveControl_4() { return &___ActiveControl_4; }
	inline void set_ActiveControl_4(int32_t value)
	{
		___ActiveControl_4 = value;
	}

	inline static int32_t get_offset_of_trackingStarted_5() { return static_cast<int32_t>(offsetof(WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B, ___trackingStarted_5)); }
	inline bool get_trackingStarted_5() const { return ___trackingStarted_5; }
	inline bool* get_address_of_trackingStarted_5() { return &___trackingStarted_5; }
	inline void set_trackingStarted_5(bool value)
	{
		___trackingStarted_5 = value;
	}

	inline static int32_t get_offset_of_TrackingStatusChanged_6() { return static_cast<int32_t>(offsetof(WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B, ___TrackingStatusChanged_6)); }
	inline Action_1_tBCED0B9AD0B042D7EA7FC7C908253547CF7D9FB6 * get_TrackingStatusChanged_6() const { return ___TrackingStatusChanged_6; }
	inline Action_1_tBCED0B9AD0B042D7EA7FC7C908253547CF7D9FB6 ** get_address_of_TrackingStatusChanged_6() { return &___TrackingStatusChanged_6; }
	inline void set_TrackingStatusChanged_6(Action_1_tBCED0B9AD0B042D7EA7FC7C908253547CF7D9FB6 * value)
	{
		___TrackingStatusChanged_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrackingStatusChanged_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTrackingStatusU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B, ___U3CTrackingStatusU3Ek__BackingField_7)); }
	inline int32_t get_U3CTrackingStatusU3Ek__BackingField_7() const { return ___U3CTrackingStatusU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CTrackingStatusU3Ek__BackingField_7() { return &___U3CTrackingStatusU3Ek__BackingField_7; }
	inline void set_U3CTrackingStatusU3Ek__BackingField_7(int32_t value)
	{
		___U3CTrackingStatusU3Ek__BackingField_7 = value;
	}
};


// easyar.CameraSource
struct  CameraSource_tBBCEDFB01276E741750E3B3CF9FD360D69CCEE13  : public FrameSource_t6AA400B054712A5C45E092546880F5C55C97A110
{
public:
	// System.Int32 easyar.CameraSource::bufferCapacity
	int32_t ___bufferCapacity_6;

public:
	inline static int32_t get_offset_of_bufferCapacity_6() { return static_cast<int32_t>(offsetof(CameraSource_tBBCEDFB01276E741750E3B3CF9FD360D69CCEE13, ___bufferCapacity_6)); }
	inline int32_t get_bufferCapacity_6() const { return ___bufferCapacity_6; }
	inline int32_t* get_address_of_bufferCapacity_6() { return &___bufferCapacity_6; }
	inline void set_bufferCapacity_6(int32_t value)
	{
		___bufferCapacity_6 = value;
	}
};


// easyar.SurfaceTrackerFrameFilter
struct  SurfaceTrackerFrameFilter_t1793B6C17B711CDFC2E16D2C51E1EC8CC9AAAD7D  : public FrameFilter_t17175B54AEDD30B1251CE70EEE33D6393F2DC0EB
{
public:
	// easyar.SurfaceTracker easyar.SurfaceTrackerFrameFilter::<Tracker>k__BackingField
	SurfaceTracker_t5700DA0E1842A81A3E4FD2DBE1887C69B51A2BF0 * ___U3CTrackerU3Ek__BackingField_5;
	// System.Boolean easyar.SurfaceTrackerFrameFilter::isStarted
	bool ___isStarted_6;

public:
	inline static int32_t get_offset_of_U3CTrackerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SurfaceTrackerFrameFilter_t1793B6C17B711CDFC2E16D2C51E1EC8CC9AAAD7D, ___U3CTrackerU3Ek__BackingField_5)); }
	inline SurfaceTracker_t5700DA0E1842A81A3E4FD2DBE1887C69B51A2BF0 * get_U3CTrackerU3Ek__BackingField_5() const { return ___U3CTrackerU3Ek__BackingField_5; }
	inline SurfaceTracker_t5700DA0E1842A81A3E4FD2DBE1887C69B51A2BF0 ** get_address_of_U3CTrackerU3Ek__BackingField_5() { return &___U3CTrackerU3Ek__BackingField_5; }
	inline void set_U3CTrackerU3Ek__BackingField_5(SurfaceTracker_t5700DA0E1842A81A3E4FD2DBE1887C69B51A2BF0 * value)
	{
		___U3CTrackerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTrackerU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_isStarted_6() { return static_cast<int32_t>(offsetof(SurfaceTrackerFrameFilter_t1793B6C17B711CDFC2E16D2C51E1EC8CC9AAAD7D, ___isStarted_6)); }
	inline bool get_isStarted_6() const { return ___isStarted_6; }
	inline bool* get_address_of_isStarted_6() { return &___isStarted_6; }
	inline void set_isStarted_6(bool value)
	{
		___isStarted_6 = value;
	}
};


// easyar.VIOCameraDeviceUnion
struct  VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B  : public CameraSource_tBBCEDFB01276E741750E3B3CF9FD360D69CCEE13
{
public:
	// easyar.VIOCameraDeviceUnion_DeviceUnion easyar.VIOCameraDeviceUnion::<Device>k__BackingField
	DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F * ___U3CDeviceU3Ek__BackingField_7;
	// easyar.VIOCameraDeviceUnion_DeviceChooseStrategy easyar.VIOCameraDeviceUnion::DeviceStrategy
	int32_t ___DeviceStrategy_8;
	// System.Action easyar.VIOCameraDeviceUnion::deviceStart
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___deviceStart_9;
	// System.Action easyar.VIOCameraDeviceUnion::deviceStop
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___deviceStop_10;
	// System.Action easyar.VIOCameraDeviceUnion::deviceClose
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___deviceClose_11;
	// System.Action`1<System.Int32> easyar.VIOCameraDeviceUnion::deviceSetBufferCapacity
	Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134 * ___deviceSetBufferCapacity_12;
	// System.Func`1<System.Int32> easyar.VIOCameraDeviceUnion::deviceGetBufferCapacity
	Func_1_t3ACD5BFE4C52F1FE4426CAE02589E0F6F4ABCB40 * ___deviceGetBufferCapacity_13;
	// System.Action`1<easyar.InputFrameSink> easyar.VIOCameraDeviceUnion::deviceConnect
	Action_1_tF1A7104D18700C90ACE10E78CF50AE21367A5D3B * ___deviceConnect_14;
	// System.Boolean easyar.VIOCameraDeviceUnion::willOpen
	bool ___willOpen_15;
	// System.Action easyar.VIOCameraDeviceUnion::DeviceCreated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DeviceCreated_16;
	// System.Action easyar.VIOCameraDeviceUnion::DeviceOpened
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DeviceOpened_17;
	// System.Action easyar.VIOCameraDeviceUnion::DeviceClosed
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DeviceClosed_18;

public:
	inline static int32_t get_offset_of_U3CDeviceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___U3CDeviceU3Ek__BackingField_7)); }
	inline DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F * get_U3CDeviceU3Ek__BackingField_7() const { return ___U3CDeviceU3Ek__BackingField_7; }
	inline DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F ** get_address_of_U3CDeviceU3Ek__BackingField_7() { return &___U3CDeviceU3Ek__BackingField_7; }
	inline void set_U3CDeviceU3Ek__BackingField_7(DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F * value)
	{
		___U3CDeviceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDeviceU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceStrategy_8() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___DeviceStrategy_8)); }
	inline int32_t get_DeviceStrategy_8() const { return ___DeviceStrategy_8; }
	inline int32_t* get_address_of_DeviceStrategy_8() { return &___DeviceStrategy_8; }
	inline void set_DeviceStrategy_8(int32_t value)
	{
		___DeviceStrategy_8 = value;
	}

	inline static int32_t get_offset_of_deviceStart_9() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___deviceStart_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_deviceStart_9() const { return ___deviceStart_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_deviceStart_9() { return &___deviceStart_9; }
	inline void set_deviceStart_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___deviceStart_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceStart_9), (void*)value);
	}

	inline static int32_t get_offset_of_deviceStop_10() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___deviceStop_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_deviceStop_10() const { return ___deviceStop_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_deviceStop_10() { return &___deviceStop_10; }
	inline void set_deviceStop_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___deviceStop_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceStop_10), (void*)value);
	}

	inline static int32_t get_offset_of_deviceClose_11() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___deviceClose_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_deviceClose_11() const { return ___deviceClose_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_deviceClose_11() { return &___deviceClose_11; }
	inline void set_deviceClose_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___deviceClose_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceClose_11), (void*)value);
	}

	inline static int32_t get_offset_of_deviceSetBufferCapacity_12() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___deviceSetBufferCapacity_12)); }
	inline Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134 * get_deviceSetBufferCapacity_12() const { return ___deviceSetBufferCapacity_12; }
	inline Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134 ** get_address_of_deviceSetBufferCapacity_12() { return &___deviceSetBufferCapacity_12; }
	inline void set_deviceSetBufferCapacity_12(Action_1_t6576D96DC217B48E1C6C6A86EE61D92BA1F89134 * value)
	{
		___deviceSetBufferCapacity_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceSetBufferCapacity_12), (void*)value);
	}

	inline static int32_t get_offset_of_deviceGetBufferCapacity_13() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___deviceGetBufferCapacity_13)); }
	inline Func_1_t3ACD5BFE4C52F1FE4426CAE02589E0F6F4ABCB40 * get_deviceGetBufferCapacity_13() const { return ___deviceGetBufferCapacity_13; }
	inline Func_1_t3ACD5BFE4C52F1FE4426CAE02589E0F6F4ABCB40 ** get_address_of_deviceGetBufferCapacity_13() { return &___deviceGetBufferCapacity_13; }
	inline void set_deviceGetBufferCapacity_13(Func_1_t3ACD5BFE4C52F1FE4426CAE02589E0F6F4ABCB40 * value)
	{
		___deviceGetBufferCapacity_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceGetBufferCapacity_13), (void*)value);
	}

	inline static int32_t get_offset_of_deviceConnect_14() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___deviceConnect_14)); }
	inline Action_1_tF1A7104D18700C90ACE10E78CF50AE21367A5D3B * get_deviceConnect_14() const { return ___deviceConnect_14; }
	inline Action_1_tF1A7104D18700C90ACE10E78CF50AE21367A5D3B ** get_address_of_deviceConnect_14() { return &___deviceConnect_14; }
	inline void set_deviceConnect_14(Action_1_tF1A7104D18700C90ACE10E78CF50AE21367A5D3B * value)
	{
		___deviceConnect_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deviceConnect_14), (void*)value);
	}

	inline static int32_t get_offset_of_willOpen_15() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___willOpen_15)); }
	inline bool get_willOpen_15() const { return ___willOpen_15; }
	inline bool* get_address_of_willOpen_15() { return &___willOpen_15; }
	inline void set_willOpen_15(bool value)
	{
		___willOpen_15 = value;
	}

	inline static int32_t get_offset_of_DeviceCreated_16() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___DeviceCreated_16)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DeviceCreated_16() const { return ___DeviceCreated_16; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DeviceCreated_16() { return &___DeviceCreated_16; }
	inline void set_DeviceCreated_16(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DeviceCreated_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceCreated_16), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceOpened_17() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___DeviceOpened_17)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DeviceOpened_17() const { return ___DeviceOpened_17; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DeviceOpened_17() { return &___DeviceOpened_17; }
	inline void set_DeviceOpened_17(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DeviceOpened_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceOpened_17), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceClosed_18() { return static_cast<int32_t>(offsetof(VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B, ___DeviceClosed_18)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DeviceClosed_18() const { return ___DeviceClosed_18; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DeviceClosed_18() { return &___DeviceClosed_18; }
	inline void set_DeviceClosed_18(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DeviceClosed_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceClosed_18), (void*)value);
	}
};


// easyar.VideoCameraDevice
struct  VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E  : public CameraSource_tBBCEDFB01276E741750E3B3CF9FD360D69CCEE13
{
public:
	// easyar.CameraDevice easyar.VideoCameraDevice::<Device>k__BackingField
	CameraDevice_t93B3250A54F0E0FE23EB922E2AF6D94942F84470 * ___U3CDeviceU3Ek__BackingField_7;
	// easyar.CameraDeviceFocusMode easyar.VideoCameraDevice::FocusMode
	int32_t ___FocusMode_8;
	// UnityEngine.Vector2 easyar.VideoCameraDevice::CameraSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___CameraSize_9;
	// easyar.VideoCameraDevice_CameraDeviceOpenMethod easyar.VideoCameraDevice::CameraOpenMethod
	int32_t ___CameraOpenMethod_10;
	// easyar.CameraDeviceType easyar.VideoCameraDevice::CameraType
	int32_t ___CameraType_11;
	// System.Int32 easyar.VideoCameraDevice::CameraIndex
	int32_t ___CameraIndex_12;
	// easyar.CameraDevicePreference easyar.VideoCameraDevice::cameraPreference
	int32_t ___cameraPreference_13;
	// easyar.CameraParameters easyar.VideoCameraDevice::parameters
	CameraParameters_t6C1E79E04BA6188556FE8F1E2B46360A8C3B2BD5 * ___parameters_14;
	// System.Boolean easyar.VideoCameraDevice::willOpen
	bool ___willOpen_15;
	// System.Action easyar.VideoCameraDevice::DeviceCreated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DeviceCreated_16;
	// System.Action easyar.VideoCameraDevice::DeviceOpened
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DeviceOpened_17;
	// System.Action easyar.VideoCameraDevice::DeviceClosed
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___DeviceClosed_18;

public:
	inline static int32_t get_offset_of_U3CDeviceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___U3CDeviceU3Ek__BackingField_7)); }
	inline CameraDevice_t93B3250A54F0E0FE23EB922E2AF6D94942F84470 * get_U3CDeviceU3Ek__BackingField_7() const { return ___U3CDeviceU3Ek__BackingField_7; }
	inline CameraDevice_t93B3250A54F0E0FE23EB922E2AF6D94942F84470 ** get_address_of_U3CDeviceU3Ek__BackingField_7() { return &___U3CDeviceU3Ek__BackingField_7; }
	inline void set_U3CDeviceU3Ek__BackingField_7(CameraDevice_t93B3250A54F0E0FE23EB922E2AF6D94942F84470 * value)
	{
		___U3CDeviceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDeviceU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_FocusMode_8() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___FocusMode_8)); }
	inline int32_t get_FocusMode_8() const { return ___FocusMode_8; }
	inline int32_t* get_address_of_FocusMode_8() { return &___FocusMode_8; }
	inline void set_FocusMode_8(int32_t value)
	{
		___FocusMode_8 = value;
	}

	inline static int32_t get_offset_of_CameraSize_9() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___CameraSize_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_CameraSize_9() const { return ___CameraSize_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_CameraSize_9() { return &___CameraSize_9; }
	inline void set_CameraSize_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___CameraSize_9 = value;
	}

	inline static int32_t get_offset_of_CameraOpenMethod_10() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___CameraOpenMethod_10)); }
	inline int32_t get_CameraOpenMethod_10() const { return ___CameraOpenMethod_10; }
	inline int32_t* get_address_of_CameraOpenMethod_10() { return &___CameraOpenMethod_10; }
	inline void set_CameraOpenMethod_10(int32_t value)
	{
		___CameraOpenMethod_10 = value;
	}

	inline static int32_t get_offset_of_CameraType_11() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___CameraType_11)); }
	inline int32_t get_CameraType_11() const { return ___CameraType_11; }
	inline int32_t* get_address_of_CameraType_11() { return &___CameraType_11; }
	inline void set_CameraType_11(int32_t value)
	{
		___CameraType_11 = value;
	}

	inline static int32_t get_offset_of_CameraIndex_12() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___CameraIndex_12)); }
	inline int32_t get_CameraIndex_12() const { return ___CameraIndex_12; }
	inline int32_t* get_address_of_CameraIndex_12() { return &___CameraIndex_12; }
	inline void set_CameraIndex_12(int32_t value)
	{
		___CameraIndex_12 = value;
	}

	inline static int32_t get_offset_of_cameraPreference_13() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___cameraPreference_13)); }
	inline int32_t get_cameraPreference_13() const { return ___cameraPreference_13; }
	inline int32_t* get_address_of_cameraPreference_13() { return &___cameraPreference_13; }
	inline void set_cameraPreference_13(int32_t value)
	{
		___cameraPreference_13 = value;
	}

	inline static int32_t get_offset_of_parameters_14() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___parameters_14)); }
	inline CameraParameters_t6C1E79E04BA6188556FE8F1E2B46360A8C3B2BD5 * get_parameters_14() const { return ___parameters_14; }
	inline CameraParameters_t6C1E79E04BA6188556FE8F1E2B46360A8C3B2BD5 ** get_address_of_parameters_14() { return &___parameters_14; }
	inline void set_parameters_14(CameraParameters_t6C1E79E04BA6188556FE8F1E2B46360A8C3B2BD5 * value)
	{
		___parameters_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parameters_14), (void*)value);
	}

	inline static int32_t get_offset_of_willOpen_15() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___willOpen_15)); }
	inline bool get_willOpen_15() const { return ___willOpen_15; }
	inline bool* get_address_of_willOpen_15() { return &___willOpen_15; }
	inline void set_willOpen_15(bool value)
	{
		___willOpen_15 = value;
	}

	inline static int32_t get_offset_of_DeviceCreated_16() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___DeviceCreated_16)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DeviceCreated_16() const { return ___DeviceCreated_16; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DeviceCreated_16() { return &___DeviceCreated_16; }
	inline void set_DeviceCreated_16(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DeviceCreated_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceCreated_16), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceOpened_17() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___DeviceOpened_17)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DeviceOpened_17() const { return ___DeviceOpened_17; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DeviceOpened_17() { return &___DeviceOpened_17; }
	inline void set_DeviceOpened_17(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DeviceOpened_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceOpened_17), (void*)value);
	}

	inline static int32_t get_offset_of_DeviceClosed_18() { return static_cast<int32_t>(offsetof(VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E, ___DeviceClosed_18)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_DeviceClosed_18() const { return ___DeviceClosed_18; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_DeviceClosed_18() { return &___DeviceClosed_18; }
	inline void set_DeviceClosed_18(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___DeviceClosed_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeviceClosed_18), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (U3CU3Ec__DisplayClass64_0_t2447ADF55FF732F7A04F5897C33974CBFB472F55), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2075[3] = 
{
	U3CU3Ec__DisplayClass64_0_t2447ADF55FF732F7A04F5897C33974CBFB472F55::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass64_0_t2447ADF55FF732F7A04F5897C33974CBFB472F55::get_offset_of_controller_1(),
	U3CU3Ec__DisplayClass64_0_t2447ADF55FF732F7A04F5897C33974CBFB472F55::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (U3CU3Ec__DisplayClass65_0_t8B0FAECA16A92C91E17B2A15748D81263ED2045C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2076[3] = 
{
	U3CU3Ec__DisplayClass65_0_t8B0FAECA16A92C91E17B2A15748D81263ED2045C::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass65_0_t8B0FAECA16A92C91E17B2A15748D81263ED2045C::get_offset_of_controller_1(),
	U3CU3Ec__DisplayClass65_0_t8B0FAECA16A92C91E17B2A15748D81263ED2045C::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2077[4] = 
{
	U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC::get_offset_of_name_0(),
	U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC::get_offset_of_controller_2(),
	U3CU3Ec__DisplayClass66_0_tDAAD603B8779405E971A165EB71AB3A1F81EEBDC::get_offset_of_callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (SurfaceTrackerFrameFilter_t1793B6C17B711CDFC2E16D2C51E1EC8CC9AAAD7D), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2078[2] = 
{
	SurfaceTrackerFrameFilter_t1793B6C17B711CDFC2E16D2C51E1EC8CC9AAAD7D::get_offset_of_U3CTrackerU3Ek__BackingField_5(),
	SurfaceTrackerFrameFilter_t1793B6C17B711CDFC2E16D2C51E1EC8CC9AAAD7D::get_offset_of_isStarted_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2079[7] = 
{
	TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798::get_offset_of_ActiveControl_4(),
	TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798::get_offset_of_HorizontalFlip_5(),
	TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798::get_offset_of_firstFound_6(),
	TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798::get_offset_of_TargetFound_7(),
	TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798::get_offset_of_TargetLost_8(),
	TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798::get_offset_of_U3CIsTrackedU3Ek__BackingField_9(),
	TargetController_t1CAEBBD5B94766022D915CB8FE7ECE692E7E5798::get_offset_of_U3CIsLoadedU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (ActiveControlStrategy_t665C6B68999C0414E97FB445E1F20BE85A40925D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2080[4] = 
{
	ActiveControlStrategy_t665C6B68999C0414E97FB445E1F20BE85A40925D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (APIExtend_t5293A6E69A0F431427FA960410D09AC2F130610C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (Display_tEA466D953E94BF739413BEBF395FA0F3913A32C5), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2082[1] = 
{
	Display_tEA466D953E94BF739413BEBF395FA0F3913A32C5::get_offset_of_rotations_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (PathType_t98BC7F775F03312503BA714DBAC392FCADBB5E76)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2083[4] = 
{
	PathType_t98BC7F775F03312503BA714DBAC392FCADBB5E76::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (FileUtil_tB84023BE2B43EE962475F97316CF60D0B83D89B4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (U3CU3Ec__DisplayClass0_0_t9FC5F8D2234B3AC9254C75A9E79027A839C6021B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2085[1] = 
{
	U3CU3Ec__DisplayClass0_0_t9FC5F8D2234B3AC9254C75A9E79027A839C6021B::get_offset_of_onLoad_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2086[7] = 
{
	U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC::get_offset_of_U3CU3E1__state_0(),
	U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC::get_offset_of_U3CU3E2__current_1(),
	U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC::get_offset_of_onLoad_2(),
	U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC::get_offset_of_filePath_3(),
	U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC::get_offset_of_filePathType_4(),
	U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC::get_offset_of_U3ChandleU3E5__2_5(),
	U3CLoadFileU3Ed__1_t0C50D1EC6BF7F464E4931E37417140D59E2515FC::get_offset_of_U3CwebRequestU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7), -1, sizeof(GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2087[5] = 
{
	GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7_StaticFields::get_offset_of_popup_4(),
	GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7::get_offset_of_messageQueue_5(),
	GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7::get_offset_of_isShowing_6(),
	GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7::get_offset_of_isDisappearing_7(),
	GUIPopup_t10884388A8FE67C53C6F745D609703FB56A884D7::get_offset_of_skin_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2088[4] = 
{
	U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1::get_offset_of_U3CU3E1__state_0(),
	U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1::get_offset_of_U3CU3E2__current_1(),
	U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1::get_offset_of_U3CU3E4__this_2(),
	U3CShowMessageU3Ed__8_tD94E87EDB9C4F5720E15918CB4DBAABCBD0EA9D1::get_offset_of_U3CtimeU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (UIPopupException_t2827F66E5D366BC1DD2B51961B2031DDD08557C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (ThreadWorker_t884565929A86E905E099E91E3ADF13BA90CD7C9E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2090[3] = 
{
	ThreadWorker_t884565929A86E905E099E91E3ADF13BA90CD7C9E::get_offset_of_thread_0(),
	ThreadWorker_t884565929A86E905E099E91E3ADF13BA90CD7C9E::get_offset_of_finished_1(),
	ThreadWorker_t884565929A86E905E099E91E3ADF13BA90CD7C9E::get_offset_of_queue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (TransformUtil_t1E80C9C7DBA8482ECD9F374EFDB0771D77C496BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2092[12] = 
{
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_U3CDeviceU3Ek__BackingField_7(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_DeviceStrategy_8(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_deviceStart_9(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_deviceStop_10(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_deviceClose_11(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_deviceSetBufferCapacity_12(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_deviceGetBufferCapacity_13(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_deviceConnect_14(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_willOpen_15(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_DeviceCreated_16(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_DeviceOpened_17(),
	VIOCameraDeviceUnion_t40269EAD0AA2E55039ECDB6231C247249DBC3B9B::get_offset_of_DeviceClosed_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (DeviceChooseStrategy_tB71112240D7E8F7D672F2D2158348A00AE12110F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2093[5] = 
{
	DeviceChooseStrategy_tB71112240D7E8F7D672F2D2158348A00AE12110F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2094[4] = 
{
	DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F::get_offset_of_motionTrackerCameraDevice_0(),
	DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F::get_offset_of_arKitCameraDevice_1(),
	DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F::get_offset_of_arCoreCameraDevice_2(),
	DeviceUnion_tBAAA657DACA2656B513F307B1CEF03DD9CBEBA7F::get_offset_of_U3CDeviceTypeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (VIODeviceType_t913BBD2CF782BFEF26A06A541475B70B2CE762BD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2095[4] = 
{
	VIODeviceType_t913BBD2CF782BFEF26A06A541475B70B2CE762BD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (U3CU3Ec__DisplayClass35_0_t3E7F517A55F291899B812B3B126710ECBD47D6C1), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2096[1] = 
{
	U3CU3Ec__DisplayClass35_0_t3E7F517A55F291899B812B3B126710ECBD47D6C1::get_offset_of_device_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (U3CU3Ec__DisplayClass36_0_t51241EBB9A57AC8C14A97E8AD7C0E4A042093B77), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2097[1] = 
{
	U3CU3Ec__DisplayClass36_0_t51241EBB9A57AC8C14A97E8AD7C0E4A042093B77::get_offset_of_device_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (U3CU3Ec__DisplayClass37_0_t99023F1F073B643F7F59F61C460EA2A2319F5F5C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2098[1] = 
{
	U3CU3Ec__DisplayClass37_0_t99023F1F073B643F7F59F61C460EA2A2319F5F5C::get_offset_of_device_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2099[12] = 
{
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_U3CDeviceU3Ek__BackingField_7(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_FocusMode_8(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_CameraSize_9(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_CameraOpenMethod_10(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_CameraType_11(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_CameraIndex_12(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_cameraPreference_13(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_parameters_14(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_willOpen_15(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_DeviceCreated_16(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_DeviceOpened_17(),
	VideoCameraDevice_t2A0ED8D00DA46E27050354FCCD94502137259F6E::get_offset_of_DeviceClosed_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (CameraDeviceOpenMethod_t13B30F6F8BAB0FFB98006C70ED38326D6793DC00)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2100[3] = 
{
	CameraDeviceOpenMethod_t13B30F6F8BAB0FFB98006C70ED38326D6793DC00::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2101[8] = 
{
	VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443::get_offset_of_Profile_4(),
	VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443::get_offset_of_Orientation_5(),
	VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443::get_offset_of_RecordZoomMode_6(),
	VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443::get_offset_of_FilePathType_7(),
	VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443::get_offset_of_FilePath_8(),
	VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443::get_offset_of_recorder_9(),
	VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443::get_offset_of_StatusUpdate_10(),
	VideoRecorder_t1E575853F5DC3F5E90FEB24BB95CDC2E610E0443::get_offset_of_U3CIsReadyU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (OutputPathType_t53F220438AA485F062E36ADF872CFF34522826A7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2102[3] = 
{
	OutputPathType_t53F220438AA485F062E36ADF872CFF34522826A7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (VideoOrientation_tA6C7B5BB5C30F24C5DDED02FABF3ABA76E6DD4EC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2103[4] = 
{
	VideoOrientation_tA6C7B5BB5C30F24C5DDED02FABF3ABA76E6DD4EC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2104[4] = 
{
	WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B::get_offset_of_ActiveControl_4(),
	WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B::get_offset_of_trackingStarted_5(),
	WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B::get_offset_of_TrackingStatusChanged_6(),
	WorldRootController_tE1DF065FD582F3A760B9A4D1AA16810FD9E8D53B::get_offset_of_U3CTrackingStatusU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (ActiveControlStrategy_t3ED1A6012B248799DF552B4F32BA8CA5BC6F4927)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2105[4] = 
{
	ActiveControlStrategy_t3ED1A6012B248799DF552B4F32BA8CA5BC6F4927::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (Detail_t747CF47D738753813EA98188F036B1FDF753ECA5), -1, sizeof(Detail_t747CF47D738753813EA98188F036B1FDF753ECA5_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2106[2] = 
{
	0,
	Detail_t747CF47D738753813EA98188F036B1FDF753ECA5_StaticFields::get_offset_of_TypeNameToConstructor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2107[1] = 
{
	AutoRelease_t2613C741E716A0CED23B2899BF8C37A7B7917B67::get_offset_of_actions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2108[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (OptionalOfBuffer_t0757A8E4275A9BD768752AB53823BF73BE8FA7D2)+ sizeof (RuntimeObject), sizeof(OptionalOfBuffer_t0757A8E4275A9BD768752AB53823BF73BE8FA7D2 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2109[2] = 
{
	OptionalOfBuffer_t0757A8E4275A9BD768752AB53823BF73BE8FA7D2::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfBuffer_t0757A8E4275A9BD768752AB53823BF73BE8FA7D2::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3)+ sizeof (RuntimeObject), sizeof(FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2110[3] = 
{
	FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoid_t6F72A5858D93433EBBA8AC14D375BDDD54EF55F3::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (FunctionDelegate_t8848D5E47212289B58FA62C1BF4A6181B20EA54F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (DestroyDelegate_t3AA1949EC75C7221AB90BB3B883229E0CB7C31B2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (OptionalOfObjectTarget_t9512684692C22C2C112F935A4665D3FE2EA4CC31)+ sizeof (RuntimeObject), sizeof(OptionalOfObjectTarget_t9512684692C22C2C112F935A4665D3FE2EA4CC31 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2113[2] = 
{
	OptionalOfObjectTarget_t9512684692C22C2C112F935A4665D3FE2EA4CC31::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfObjectTarget_t9512684692C22C2C112F935A4665D3FE2EA4CC31::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (OptionalOfTarget_t030A332DA69FE1D407C6BEF2D195CCF15DA6EA87)+ sizeof (RuntimeObject), sizeof(OptionalOfTarget_t030A332DA69FE1D407C6BEF2D195CCF15DA6EA87 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2114[2] = 
{
	OptionalOfTarget_t030A332DA69FE1D407C6BEF2D195CCF15DA6EA87::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfTarget_t030A332DA69FE1D407C6BEF2D195CCF15DA6EA87::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (OptionalOfOutputFrame_t5A145618F1BC39E293F85696E8B22B086A84691C)+ sizeof (RuntimeObject), sizeof(OptionalOfOutputFrame_t5A145618F1BC39E293F85696E8B22B086A84691C ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2115[2] = 
{
	OptionalOfOutputFrame_t5A145618F1BC39E293F85696E8B22B086A84691C::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfOutputFrame_t5A145618F1BC39E293F85696E8B22B086A84691C::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (OptionalOfFrameFilterResult_tE93086BAC9B4E1E870D5622412A128F3B7897E48)+ sizeof (RuntimeObject), sizeof(OptionalOfFrameFilterResult_tE93086BAC9B4E1E870D5622412A128F3B7897E48 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2116[2] = 
{
	OptionalOfFrameFilterResult_tE93086BAC9B4E1E870D5622412A128F3B7897E48::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFrameFilterResult_tE93086BAC9B4E1E870D5622412A128F3B7897E48::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2117[2] = 
{
	OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoidFromOutputFrame_tE40E97A0CF7D6EA7EFB50890E00337418490A857::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2118[3] = 
{
	FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromOutputFrame_t1E702E95053C59E939E15F3A20C64D97BDAD3E34::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (FunctionDelegate_tE89DEAA79C244DC519F4FF518D34772907DDBB9B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (DestroyDelegate_tFFAFFA1AA25A9A49F60E9F7B12BB4B41D8B015E3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2121[3] = 
{
	FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromTargetAndBool_t18CDE1C515FEC02BAA41230EDDABC854E074227D::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (FunctionDelegate_t990035A4EB0D6A4E74D65EE5C45F3AF438D69119), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (DestroyDelegate_t14935C2A4187F63C334BF87A911FBF20F1F1BFA5), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (OptionalOfImageTarget_tBA006FD8DA304929D12DC3538F60D828DABA8605)+ sizeof (RuntimeObject), sizeof(OptionalOfImageTarget_tBA006FD8DA304929D12DC3538F60D828DABA8605 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2124[2] = 
{
	OptionalOfImageTarget_tBA006FD8DA304929D12DC3538F60D828DABA8605::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfImageTarget_tBA006FD8DA304929D12DC3538F60D828DABA8605::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (OptionalOfString_t31CB25740B77B6965DCBF8D0175D3DE3DF478729)+ sizeof (RuntimeObject), sizeof(OptionalOfString_t31CB25740B77B6965DCBF8D0175D3DE3DF478729 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2125[2] = 
{
	OptionalOfString_t31CB25740B77B6965DCBF8D0175D3DE3DF478729::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfString_t31CB25740B77B6965DCBF8D0175D3DE3DF478729::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2126[3] = 
{
	FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromCloudRecognizationResult_t9A58A3F7C1FBBC99399302202CADFD4391A87EB9::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (FunctionDelegate_tE7D7F32814FA87DC91820A195D9678EB48FC9F81), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (DestroyDelegate_t1AD3EDFB2E4036786B440B416474CDE85F082E26), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2129[2] = 
{
	OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoidFromInputFrame_t9CAB8ADF9826E088AB615BDDE30FD54EB891E53C::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2130[3] = 
{
	FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromInputFrame_tC18AF29745E855646FC492D85C8ABE9EE31F6E81::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (FunctionDelegate_t1E48CF3290061A12253AC07FEC9B5A2C0865FC19), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (DestroyDelegate_tA95D741A4ECD162A9E3EC7D8085B5589D738C740), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2133[2] = 
{
	OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoidFromCameraState_t7D4432FF31B47A8EDA6C9CF0271F09CD13535085::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2134[3] = 
{
	FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromCameraState_t02C9C9230F8F57DDC7B51F0F148D924D6A1B3CF5::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (FunctionDelegate_tAE9F5819A2ED199C1C375161C8A71F04A69FB01D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (DestroyDelegate_t758A0260C125658E88D189A318456AD9E62ECF32), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2137[2] = 
{
	OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoidFromPermissionStatusAndString_t03AFBAD33246C922A6C0A75138118A36FDEED3A9::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2138[3] = 
{
	FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromPermissionStatusAndString_t71269E9ADECE7B1BC5B2D706C1037769DD0DA7E1::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (FunctionDelegate_tF4D41B79691A226FEA22151FB99FEA4EC1726836), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (DestroyDelegate_tE504B96EE209B931BDC1BF72BF91B6A87A772209), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2141[3] = 
{
	FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromLogLevelAndString_tF8F03E5D11AD668B923210391152DFCD72DD1EEB::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (FunctionDelegate_t0E05B7C0F77F94A09651021DEA1DB8377A70C448), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (DestroyDelegate_t2188B56261B93F5F389858154388FF5F297B0033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2144[2] = 
{
	OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoidFromRecordStatusAndString_tB935B825733F73692C361260F2CD3C0D83F773A3::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2145[3] = 
{
	FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromRecordStatusAndString_t4EDAB990AB133CD4E7AEB3279A9D96AA09F3CC9F::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (FunctionDelegate_tC303954CF881FABFE27597445CFEB8CEDCF13765), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (DestroyDelegate_t3C0897F2F93EC30F5D876405102810B918CF7790), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (OptionalOfMatrix44F_tA3118CA79EFA5E4D61AD496CEE1EBA8E516C6F98)+ sizeof (RuntimeObject), sizeof(OptionalOfMatrix44F_tA3118CA79EFA5E4D61AD496CEE1EBA8E516C6F98 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2148[2] = 
{
	OptionalOfMatrix44F_tA3118CA79EFA5E4D61AD496CEE1EBA8E516C6F98::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfMatrix44F_tA3118CA79EFA5E4D61AD496CEE1EBA8E516C6F98::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2149[2] = 
{
	OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoidFromBool_t46C53C5FF1B383AD3FBCD6B36F9ABD18937925F5::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2150[3] = 
{
	FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromBool_t220D66838E55B79A023DC49B1ED415D3F5B213C6::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (FunctionDelegate_t2CFD5626EBF6B782585676A1D9BFD20E0BE31BB4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (DestroyDelegate_t57CB1B601122C024033C7AD2F46AA139B5E75986), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (OptionalOfImage_t08C1B90679EB07254FAFD2EDA7C829E9C4A6A333)+ sizeof (RuntimeObject), sizeof(OptionalOfImage_t08C1B90679EB07254FAFD2EDA7C829E9C4A6A333 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2153[2] = 
{
	OptionalOfImage_t08C1B90679EB07254FAFD2EDA7C829E9C4A6A333::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfImage_t08C1B90679EB07254FAFD2EDA7C829E9C4A6A333::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2154[3] = 
{
	FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromBoolAndStringAndString_t5D875886AD2FBE308AEBA60934A2A8D7C2FA6435::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (FunctionDelegate_tCA3602C4951133C4B97C064574C5508752B54395), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (DestroyDelegate_tF0DF87394419C98CD7EEFD04AF8127B379FA62C6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2157[3] = 
{
	FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromBoolAndString_t982F0956D884A98DFE73D62CDF9FEA56C429BFFC::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (FunctionDelegate_tC9991BEBFE57DA2C93AE24E31B226FA0DBBCCBC4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (DestroyDelegate_tFF2989B8C9F4C2E6577E10F17BC39501BEB0737D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2160[2] = 
{
	OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoidFromVideoStatus_t5950173B47879EFEC2395C609AF9C911C206051B::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2161[3] = 
{
	FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromVideoStatus_tF189B7E9F7DF7E55145B8117CE7FF7F5E2E7A863::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (FunctionDelegate_tF44FA25582324437CAD80F657DCC3970F650DE99), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (DestroyDelegate_t46368E6813BD066929E2C8CB5BF24211E30D5EFD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2164[2] = 
{
	OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoid_tC65B3C006E3F9CB88A28F5576F24B200CE9FDF39::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F)+ sizeof (RuntimeObject), sizeof(OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2165[2] = 
{
	OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F::get_offset_of_has_value__0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OptionalOfFunctorOfVoidFromFeedbackFrame_tEE6DB82CA4E5DD55E0384032329A1D2AFF902A7F::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E)+ sizeof (RuntimeObject), sizeof(FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2166[3] = 
{
	FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfVoidFromFeedbackFrame_t361B12639E959C38EC48DE3F8C86C9E4E310840E::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (FunctionDelegate_t95EA6B135721C887565B9A7C3A9D979FB575D769), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (DestroyDelegate_t35BEB98D61989D586C4B902FB7A2F58C6B3AD351), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D)+ sizeof (RuntimeObject), sizeof(FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D_marshaled_pinvoke), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2169[3] = 
{
	FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D::get_offset_of__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D::get_offset_of__func_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FunctorOfOutputFrameFromListOfOutputFrame_tACB4DB5B20F8330DD81B946E3EB9C785B08F607D::get_offset_of__destroy_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (FunctionDelegate_t090C1C63C0751FF32A4D0F8A7C02814712221080), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (DestroyDelegate_t763D40EFF45F320D973DBBDBC2091903407DE9B4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5), -1, sizeof(U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2172[11] = 
{
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__670_0_1(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__672_0_2(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__676_1_3(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__676_0_4(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__677_0_5(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__688_0_6(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__691_0_7(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__698_0_8(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__725_0_9(),
	U3CU3Ec_t240A72A101A8DCA4E161AD4603F1BE55E85E05C5_StaticFields::get_offset_of_U3CU3E9__756_0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (U3CU3Ec__DisplayClass681_0_tE8CF45153C0065CE9D9B6A53DF2774AB870DD61F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2173[1] = 
{
	U3CU3Ec__DisplayClass681_0_tE8CF45153C0065CE9D9B6A53DF2774AB870DD61F::get_offset_of_sarg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (U3CU3Ec__DisplayClass685_0_t6C9AC89FAADD442EE9AB2EF01DA23B308931B1F8), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2174[1] = 
{
	U3CU3Ec__DisplayClass685_0_t6C9AC89FAADD442EE9AB2EF01DA23B308931B1F8::get_offset_of_sarg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (U3CU3Ec__DisplayClass695_0_t447BC46E3E8D5F5B62EF132843402E6000A6435B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2175[1] = 
{
	U3CU3Ec__DisplayClass695_0_t447BC46E3E8D5F5B62EF132843402E6000A6435B::get_offset_of_sarg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (U3CU3Ec__DisplayClass702_0_tB19BD97E688E04F6F33F27FC3A4BFCE771702D8F), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2176[1] = 
{
	U3CU3Ec__DisplayClass702_0_tB19BD97E688E04F6F33F27FC3A4BFCE771702D8F::get_offset_of_sarg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (U3CU3Ec__DisplayClass749_0_t7190C5F90C0EE6A77F2435983A3474FFE5A4BD07), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2177[1] = 
{
	U3CU3Ec__DisplayClass749_0_t7190C5F90C0EE6A77F2435983A3474FFE5A4BD07::get_offset_of_sarg0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (U3CU3Ec__DisplayClass753_0_t91BFD741FB69E6370FA8F9D21B31FB47940BDA48), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2178[1] = 
{
	U3CU3Ec__DisplayClass753_0_t91BFD741FB69E6370FA8F9D21B31FB47940BDA48::get_offset_of_ar_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (U3CU3Ec__DisplayClass753_1_t1C1253CE6211B9425815D04D5B9CE6EF986FBCDF), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2179[1] = 
{
	U3CU3Ec__DisplayClass753_1_t1C1253CE6211B9425815D04D5B9CE6EF986FBCDF::get_offset_of__v0__0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (Unit_t3673CF84D960209286D3322DFEFA8817003B66A0)+ sizeof (RuntimeObject), sizeof(Unit_t3673CF84D960209286D3322DFEFA8817003B66A0 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (OptionalTag_tC75A06EE0C7E07F270104C96382A8A4993735221)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2181[3] = 
{
	OptionalTag_tC75A06EE0C7E07F270104C96382A8A4993735221::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { 0, 0, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2182[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2183[3] = 
{
	RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66::get_offset_of_cdata__0(),
	RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66::get_offset_of_deleter__1(),
	RefBase_t21B3DD4D67A668AFFDFCD8EF5A15E84C95F4FF66::get_offset_of_retainer__2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (Retainer_tF3060567DC74FEC906B36A0E3352EF3955BC7EFC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (ObjectTargetParameters_tDF1A158B07BFE444EFC6D003EDEBCAA0C3ECF48B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (ObjectTarget_tE33A3926353FAFADD9A6A79E05BCE9261538C00A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921), -1, sizeof(U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2187[3] = 
{
	U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_tA0E96DEA212E45459DF79188CE43B4F9C7C2D921_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (ObjectTrackerResult_tE4ACF6058ED5682049ABB069298CE8B695643869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (ObjectTracker_t2DFF1FE335FC67408A37DE87779506B214562E75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (CloudRecognizationStatus_tCCBA3D4F61D18616929D1E995BD5CECF4B861CD8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2190[6] = 
{
	CloudRecognizationStatus_tCCBA3D4F61D18616929D1E995BD5CECF4B861CD8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (CloudRecognizationResult_t6494AC886F8FA4C74D70EA0CCA6BAB766525C4FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC), -1, sizeof(U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2192[2] = 
{
	U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7F545E5D4EFAAE79DAB022AD1ABB137CED9299DC_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (U3CU3Ec__DisplayClass5_0_t215BB4C54B46D7533B4EF30FD3E40FAA89BBB0DE), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2193[1] = 
{
	U3CU3Ec__DisplayClass5_0_t215BB4C54B46D7533B4EF30FD3E40FAA89BBB0DE::get_offset_of_ar_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (CloudRecognizer_tB0655B166281C61CA885069393FCE697C226D5E1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (Buffer_t355D8418B778B9A4BA46CA414A3BBE006E6A78FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (U3CU3Ec__DisplayClass11_0_t34C2759F7AAD82551ABA6DBF020B91CFC379C568), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2196[1] = 
{
	U3CU3Ec__DisplayClass11_0_t34C2759F7AAD82551ABA6DBF020B91CFC379C568::get_offset_of_h_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44), -1, sizeof(U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2197[2] = 
{
	U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tF32E127AAC5085560BC715C7D5D6B218E5452B44_StaticFields::get_offset_of_U3CU3E9__12_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (U3CU3Ec__DisplayClass13_0_t1FD40E9A43A2BD116FC4ED588F635D5A4493425A), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2198[2] = 
{
	U3CU3Ec__DisplayClass13_0_t1FD40E9A43A2BD116FC4ED588F635D5A4493425A::get_offset_of_h_0(),
	U3CU3Ec__DisplayClass13_0_t1FD40E9A43A2BD116FC4ED588F635D5A4493425A::get_offset_of_deleter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (BufferDictionary_t9A04540518B992E0FE8E1B4F8E037A24DC3A1035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804), -1, sizeof(U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2200[2] = 
{
	U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t9A1B4AE817C17E354995CD83FBFA39F1CB4D9804_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (BufferPool_tD3515A4BE8658AC4321D4D3131DE2FFA6F36D89A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914), -1, sizeof(U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2202[2] = 
{
	U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t518C8D0666355837FCF42A45B2163A51F0F43914_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (CameraDeviceType_t3085F22241377C20BE28D68740E0D32EB4B6ADFA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2203[4] = 
{
	CameraDeviceType_t3085F22241377C20BE28D68740E0D32EB4B6ADFA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (MotionTrackingStatus_tE6223E9EFFC7741D73222659A410636F441DD7EF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2204[4] = 
{
	MotionTrackingStatus_tE6223E9EFFC7741D73222659A410636F441DD7EF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (CameraParameters_t6C1E79E04BA6188556FE8F1E2B46360A8C3B2BD5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (PixelFormat_tF3F6BCCBBE484459F419E277DBA4192B590B154F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2206[11] = 
{
	PixelFormat_tF3F6BCCBBE484459F419E277DBA4192B590B154F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (Image_t18C2A1EA403C254F22B4BEC76C7AFC4DE5919801), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3)+ sizeof (RuntimeObject), sizeof(Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2208[16] = 
{
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_4_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_5_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_6_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_7_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_8_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_9_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_10_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_11_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_12_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_13_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_14_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix44F_t5C86FB25D3DE97538308AC6FDE3C67F2DA98ECA3::get_offset_of_data_15_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634)+ sizeof (RuntimeObject), sizeof(Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2209[9] = 
{
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_4_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_5_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_6_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_7_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Matrix33F_t1AB2D9B00D7C98EAD51C1B6808A72301EC2C3634::get_offset_of_data_8_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612)+ sizeof (RuntimeObject), sizeof(Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2210[4] = 
{
	Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612::get_offset_of_data_0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612::get_offset_of_data_1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612::get_offset_of_data_2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec4F_tFC373FCB1887B00754D7189CBE11F8E943537612::get_offset_of_data_3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E)+ sizeof (RuntimeObject), sizeof(Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2211[3] = 
{
	Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E::get_offset_of_data_0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E::get_offset_of_data_1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec3F_t72AAC2BDA1E973CB3408FDB8976D3C33B49F7B7E::get_offset_of_data_2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (Vec2F_tBB4EB623E12BF4D1A4027EBAEA291CF8D8037227)+ sizeof (RuntimeObject), sizeof(Vec2F_tBB4EB623E12BF4D1A4027EBAEA291CF8D8037227 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2212[2] = 
{
	Vec2F_tBB4EB623E12BF4D1A4027EBAEA291CF8D8037227::get_offset_of_data_0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec2F_tBB4EB623E12BF4D1A4027EBAEA291CF8D8037227::get_offset_of_data_1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B)+ sizeof (RuntimeObject), sizeof(Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2213[4] = 
{
	Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B::get_offset_of_data_0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B::get_offset_of_data_1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B::get_offset_of_data_2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec4I_t79AEF28FFC83BB3F8A18102B67C9FA2D3C64959B::get_offset_of_data_3_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (Vec2I_t01C0457CB2DB321F0AB349CAC888D0FD6E73BBD2)+ sizeof (RuntimeObject), sizeof(Vec2I_t01C0457CB2DB321F0AB349CAC888D0FD6E73BBD2 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2214[2] = 
{
	Vec2I_t01C0457CB2DB321F0AB349CAC888D0FD6E73BBD2::get_offset_of_data_0_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec2I_t01C0457CB2DB321F0AB349CAC888D0FD6E73BBD2::get_offset_of_data_1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (DenseSpatialMap_t66BF4FE24613E31CA467486A348E3DE5666B9C6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09)+ sizeof (RuntimeObject), sizeof(BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09 ), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2216[8] = 
{
	BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09::get_offset_of_numOfVertex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09::get_offset_of_startPointOfVertex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09::get_offset_of_numOfIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09::get_offset_of_startPointOfIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlockInfo_tD23F9C1AB5795C392C543B5C395F15DCD554BB09::get_offset_of_version_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (SceneMesh_tD675CCC9AF83A095BBC0CB4F0C926C88DDBB3D25), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (ARCoreCameraDevice_t87B96650F9F627475A010CB1510DA70DF4025B49), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (ARKitCameraDevice_tDE48E4831A384798886BC428FCAFA1FD6A0D0239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (CameraDeviceFocusMode_t7CB13E84AF8DE3D7D946A1DBCAA2105F4952953E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2220[6] = 
{
	CameraDeviceFocusMode_t7CB13E84AF8DE3D7D946A1DBCAA2105F4952953E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (AndroidCameraApiType_t4131D8FB0C923BC130B759AC51AC12A27BD36325)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2221[3] = 
{
	AndroidCameraApiType_t4131D8FB0C923BC130B759AC51AC12A27BD36325::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (CameraDevicePresetProfile_tC5D271617B3555C299330A1AEE5FF46A59374F9C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2222[5] = 
{
	CameraDevicePresetProfile_tC5D271617B3555C299330A1AEE5FF46A59374F9C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (CameraState_t41D0938A1158181E7B0C9E509CF05C61D9CEF658)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2223[4] = 
{
	CameraState_t41D0938A1158181E7B0C9E509CF05C61D9CEF658::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (CameraDevice_t93B3250A54F0E0FE23EB922E2AF6D94942F84470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50), -1, sizeof(U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2225[3] = 
{
	U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
	U3CU3Ec_t67E98EB941CA522D8F92701707BEB8DDC106FE50_StaticFields::get_offset_of_U3CU3E9__11_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (CameraDevicePreference_t4BEF9FCA55102557DAA1A9F6BE552CE11AD049AB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2226[4] = 
{
	CameraDevicePreference_t4BEF9FCA55102557DAA1A9F6BE552CE11AD049AB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (CameraDeviceSelector_t49ED72EBEF29AA08FD21A7D7EF9EEC7832118F62), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (SurfaceTrackerResult_t4B5D1BBFB34A48A883238B7FDC28A6C60080B6C9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (SurfaceTracker_t5700DA0E1842A81A3E4FD2DBE1887C69B51A2BF0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (MotionTrackerCameraDevice_t3D363429CFB19C80594943F2E147458C57D03A69), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (InputFrameRecorder_tE9272358FF6007B21196E4A7476C60965FFD2D94), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (InputFramePlayer_t8C251C9015A03B9E7C2E5A19D5E6999ECB17E425), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (CallbackScheduler_t3BE641305910EA95C368FA67D1A1294797B0AFB7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (DelayedCallbackScheduler_tFDA191E2FFD94E9A3AA26F25C560D8F95EA26D2A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (ImmediateCallbackScheduler_tAA6CA29C310DCE4B0ED852A7B972BC97441A851D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (JniUtility_t368D91DE5D5E59933210A9B9C9985CEC9115AD1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (LogLevel_tE02C77CE6DEB9782E61C3688A9F519AFF1D73CCB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2237[4] = 
{
	LogLevel_tE02C77CE6DEB9782E61C3688A9F519AFF1D73CCB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (Log_t04DE32FFC173FCC192BE8481C75B27AF85CCBC0A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (ImageTargetParameters_t4534054F85DBAB01DB7E04EE1D70B00B8212D228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (ImageTarget_tF9CE381D1F5DE5B3254CEB22DDC143DB0A47F88F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6), -1, sizeof(U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2241[5] = 
{
	U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
	U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields::get_offset_of_U3CU3E9__6_0_3(),
	U3CU3Ec_tF3FEF2C6D97C63B396D081A1ED1B0334D86DD6D6_StaticFields::get_offset_of_U3CU3E9__8_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (ImageTrackerMode_tAF8FC364239DA46FFECA8E36E8C2D3509778643F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2242[3] = 
{
	ImageTrackerMode_tAF8FC364239DA46FFECA8E36E8C2D3509778643F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (ImageTrackerResult_tBDBCFCA4D25F2204F40B4389264F6EA1ED8C5F61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (ImageTracker_t99D95F9A9C7422328AF4DB69652821114B0C4CB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (Recorder_t8323228F81AFAA312EF2F050D621026784AA7A75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247), -1, sizeof(U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2246[3] = 
{
	U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_t5F1B76D27FA02A70977C1CA8DB3327AEB4861247_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (RecordProfile_tDEF5A1B1ECB5934FD436C465E3098A9F02E1E734)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2247[11] = 
{
	RecordProfile_tDEF5A1B1ECB5934FD436C465E3098A9F02E1E734::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (RecordVideoSize_tB80311BD33A6FF13197E622C9F067E3168D984D1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2248[4] = 
{
	RecordVideoSize_tB80311BD33A6FF13197E622C9F067E3168D984D1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (RecordZoomMode_tD68D8E1C498F9AFE4B217A09F004A27254121A0A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2249[3] = 
{
	RecordZoomMode_tD68D8E1C498F9AFE4B217A09F004A27254121A0A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (RecordVideoOrientation_t3AA19F344CD1EF1E3432FE2A2C2B5E4E470A332E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2250[3] = 
{
	RecordVideoOrientation_t3AA19F344CD1EF1E3432FE2A2C2B5E4E470A332E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (RecordStatus_tA225F8E91E6E99FB79D371E8A79891D95C8EC045)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2251[8] = 
{
	RecordStatus_tA225F8E91E6E99FB79D371E8A79891D95C8EC045::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (RecorderConfiguration_t0C9489C7306B4BDF93CBE2E3D56A703AED82F993), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (SparseSpatialMapResult_tD81E4F0CFFE0DD6DC9A989BF54880393E512ACD2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982), -1, sizeof(U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2254[3] = 
{
	U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_t33F4920CB477207AD794B60B936633799C01F982_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (PlaneType_t92905B0D7C8F6EE9E4FDE1FA2969767774A2DA9B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2255[3] = 
{
	PlaneType_t92905B0D7C8F6EE9E4FDE1FA2969767774A2DA9B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (PlaneData_tA546F8CB5D20873DC3D3C0FD041B7E8CF6986EAA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (LocalizationMode_tD9BE1930DA3D38E458C38D1A12B06D9644659404)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2257[5] = 
{
	LocalizationMode_tD9BE1930DA3D38E458C38D1A12B06D9644659404::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (SparseSpatialMapConfig_tA745F650FA8B470E606C0918FB31384B7A024FC8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (SparseSpatialMap_tD3A97C244427D0B13CE1B70D917300F499E9F570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007), -1, sizeof(U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2260[2] = 
{
	U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t13997028FAD603A51A50C763882C1ADB245A2007_StaticFields::get_offset_of_U3CU3E9__16_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (SparseSpatialMapManager_t3A7F5C67A6E607A0E1A65DBEE01249F850FA1FFD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D), -1, sizeof(U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2262[2] = 
{
	U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB6D42427320FAA4D9C067A7251C26C2B9E16227D_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (Engine_t935A20D4E1FBBCBBAED27EA5C43F42152E77F67E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (VideoStatus_t7B2C801858421B797E970E8C44F1A8FDBD7F9483)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2264[4] = 
{
	VideoStatus_t7B2C801858421B797E970E8C44F1A8FDBD7F9483::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (VideoType_tBC431039996B85B37628AEBCA6892F551A0C5C84)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2265[4] = 
{
	VideoType_tBC431039996B85B37628AEBCA6892F551A0C5C84::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (VideoPlayer_t8CFFC18AFC95F38EC854C3DCAE7FED6F89422C2C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C), -1, sizeof(U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2267[2] = 
{
	U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t60542070629A5427C7AA60925EEF959A5B7DA51C_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (ImageHelper_t30C212450B16C4C02A3EA93BB7801CEB459E9E5A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD), -1, sizeof(U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2269[2] = 
{
	U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t83DD4644B65EE5630129E23B08F53DEF3993DDDD_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (SignalSink_t6960E52E8BE74841137BAC36FCFDA5871BAFE88E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (SignalSource_t5D9E6D365066BC68C2328194CCA3FB46B8A1C1FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3), -1, sizeof(U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2272[2] = 
{
	U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA3A918C6D7C80560C3F9EB1C42F2841EC5D5A0F3_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (InputFrameSink_t97BD9624A3E94450FC4D84DFC4A54035A82DC1D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (InputFrameSource_t32D6896B7E3B06DB1BD77EEF7C555CC265BE86E4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1), -1, sizeof(U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2275[2] = 
{
	U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1775B3D7F94F7DBAA71BD2CC13DB834D61D4F7C1_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (OutputFrameSink_t92453CD78A4BE2375DFAFB67324318C2C5328C64), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (OutputFrameSource_t3B940D541DE63501E2E0F6CD429EFEABC072AEB2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27), -1, sizeof(U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2278[2] = 
{
	U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1D4D6F0AF0F90F545E036F2F0B630B06165A7B27_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (FeedbackFrameSink_tBFC092DAD2B19C13314BDF1C0D012D018713C78A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (FeedbackFrameSource_t15AC3F3BD55A3EC102E1A7016A53AAD75932845E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905), -1, sizeof(U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2281[2] = 
{
	U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7E23F8A01200D8A3FCCF6D7928E27AFE25079905_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (InputFrameFork_t473B6DD8D6A3FB84D4F62D652A3ECFCC63FCE31B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (OutputFrameFork_t61E24817457114FC26180D60615CA33816015C1C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (OutputFrameJoin_t35A95CCFB85BA549CE10B84F8D4A21D15EC6DA26), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (FeedbackFrameFork_tF27FD489114CA7D66F4EBC1074D67A46632DCAFC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (InputFrameThrottler_tAD8A518B4EA21DB8138CE11B7B31D84E1E2E7B52), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (OutputFrameBuffer_tC7D6ED8C5B46AF57417402D3B6255D0645E8FB62), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4), -1, sizeof(U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2288[2] = 
{
	U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t509C01411C9E7F2B5EE5B25B11D6B083C82CDDC4_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (InputFrameToOutputFrameAdapter_t074262D2812305DB57DACF4CD79FCD0BFF3CA730), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (InputFrameToFeedbackFrameAdapter_tDDA77B26C793084B51844CA454EA21D26BAEC46E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (InputFrame_tCADC80282B08673BD5A63A4B37A662368204000D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (FrameFilterResult_t91CD8278C8A8BBEA05B38049FB74B40E2794BF4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (OutputFrame_tEF80B011998FD5A6C87F59FA075C4B01CED6C9A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (FeedbackFrame_tEB60455EDD504A079A1780150E58F46F9EA0DB8E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79), -1, sizeof(U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2295[3] = 
{
	U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
	U3CU3Ec_t0F16169D4A93B64BC3A4351CA75180F5D25EEA79_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (PermissionStatus_t41F2835584F927E7DBA46FBDFA60ECA791C96C14)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2296[4] = 
{
	PermissionStatus_t41F2835584F927E7DBA46FBDFA60ECA791C96C14::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (StorageType_t6281BBD9AC7F99C0FDB4923177333905BB165C7A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2297[4] = 
{
	StorageType_t6281BBD9AC7F99C0FDB4923177333905BB165C7A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (Target_t9C4497B1723D9B3FF4C9E4880762FCFAD59D1E46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (TargetStatus_t4AA93D00312DE2F6165AFA4222026D5D4DEE95B3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2299[5] = 
{
	TargetStatus_t4AA93D00312DE2F6165AFA4222026D5D4DEE95B3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (TargetInstance_tE263CFB69CC2EFEEA6C92461B7FE4EBF1E5A1FD8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA), -1, sizeof(U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable2301[2] = 
{
	U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t283E079BDBF6D87BF21836F8E9376F6ED19A6EFA_StaticFields::get_offset_of_U3CU3E9__5_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (TargetTrackerResult_tBCEE9FE8B43EA716AEA6949B53BB38B4D94258F5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (TextureId_tFFD7E9D4242A80AF5149CE6FC5CE60FD752977DB), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
